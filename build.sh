#!/bin/bash
cd -- "$(dirname -- "$0")"

cachedBuild=false
useNinja=true
buildConfig="Debug"
install=false
systemInstall=true

while  [[ $# > 0 ]]; do
    case $1 in
        "--cached")
            cachedBuild=true
            ;;
        "--no-default-ninja")
            useNinja=false
            ;;
        "--debug")
            buildConfig="Debug"
            ;;
        "--release")
            buildConfig="Release"
            ;;
        "--install")
            install=true
            ;;
        "--user")
            systemInstall=false
            ;;
    esac
    shift 1
done

if [[ "$useNinja" == true ]]; then
    if command -v ninja &> /dev/null; then
        export CMAKE_GENERATOR=Ninja
    fi
fi

CMAKEARGS=""
if [[ -f "cmake/args" ]]; then
    CMAKEARGS="$(cat cmake/args)"
fi

if [[ "$cachedBuild" != true && -d build ]]; then
    ./clean.sh
fi

mkdir -p build
cd build || exit 1

if [[ "$1" != "--cached" ]]; then
    cmake -DKRAKEN_ENABLE_TESTS=ON "-DCMAKE_BUILD_TYPE=$buildConfig" $CMAKEARGS ..
fi

cmake --build . --config "$buildConfig" "-j$(nproc)"

if [[ "$install" == true ]]; then
    if [[ "$systemInstall" == true ]]; then
        sudo cmake --install . --config "$buildConfig"
        sudo ldconfig
    else
        cmake --install . --config "$buildConfig"
    fi
fi
