#!/bin/bash
CMAKEARGS=""
if [[ -f "cmake/args" ]]; then
    CMAKEARGS="$(cat cmake/args)"
fi

./clean.sh

mkdir build
cd build || exit 1
export CXXFLAGS="-g -O0 -Wall -W -Wshadow -Wunused-variable \
 -Wunused-parameter -Wunused-function -Wunused -Wno-system-headers \
 -Wno-deprecated -Woverloaded-virtual -Wwrite-strings -fprofile-arcs -ftest-coverage"
export CFLAGS="-g -O0 -Wall -W -fprofile-arcs -ftest-coverage"
export LDFLAGS="-fprofile-arcs -ftest-coverage"
cmake -DKRAKEN_ENABLE_TESTS=ON $CMAKEARGS ..
make "-j$(nproc)"
ctest -j$(nproc) --timeout 60
#gcovr -r ../ . --exclude-directories CMakeFiles/ --exclude-directories example/ --exclude-unreachable-branches --exclude-throw-branches "$@"
# We exclude all graphical stuff for now, since it is really difficult to test
# OpenGL rendering and other window-related stuff automatically, since that by
# its very nature requires user interaction.
gcovr -r ../ . --exclude-directories CMakeFiles/ --exclude-directories example/ --exclude-directories src/kraken/graphics/ --exclude-directories include/kraken/graphics/ --exclude-directories src/kraken/window/ --exclude-directories include/kraken/window/ --exclude-unreachable-branches --exclude-throw-branches "$@"
