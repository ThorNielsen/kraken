#include <kraken/graphics/mesh.hpp>
#include <kraken/graphics/shader.hpp>
#include <kraken/graphics/texture.hpp>
#include <kraken/graphics/texturesampler.hpp>
#include <kraken/graphics/videodriver.hpp>
#include <kraken/graphics/vertex.hpp>
#include <kraken/window/window.hpp>
#include <kraken/math/transform.hpp>

#include <kraken/utility/timer.hpp>

#include <kraken/graphics/font.hpp>
#include <kraken/graphics/typesetting.hpp>

#include <iostream>

#include <kraken/graphics/import.hpp>
#include <kraken/graphics/export.hpp>
#include <fstream>
#include <sstream>
#include <kraken/graphics/framebuffer.hpp>
#include <kraken/graphics/opengl/window.hpp>
#include <kraken/utility/timer.hpp>

int main()
{
    namespace kg = kraken::graphics;
    namespace kw = kraken::window;
    namespace math = kraken::math;
    kw::WindowCreationSettings ws;
    kg::opengl::ContextSettings cs;
    ws.width = 640;
    ws.height = 480;
    kw::Window window;
    if (!window.open(ws, kg::opengl::surfaceCreator(cs)))
    {
        throw std::runtime_error("Couldn't open window.");
    }
    window.setKeyPressCallback([&window]
        (kw::Key key, kw::Scancode, kw::KeyModifiers)
        {
            if (key == kw::Key::Escape)
            {
                window.setCloseFlag();
            }
        });

    auto* driver = kg::VideoDriver::driver(window);

    std::string vertexShaderCode =
R"***(#version 330 core

uniform mat4 transform;

in vec3 position;
in vec3 normal;
in vec2 texcoord;

out vec3 vsOutCol;
out vec3 vsOutNormal;

void main()
{
    vsOutCol = vec3(texcoord, 0.);
    vsOutNormal = (transform * vec4(normal, 0)).xyz;
    gl_Position = transform * vec4(position, 1);
})***";

    std::string fragmentShaderCode =
R"***(#version 330 core

in vec3 vsOutCol;
in vec3 vsOutNormal;
out vec4 colour;

void main()
{
    //colour = vec4(0.f, 0.f, 0.f, 1.f);
    colour = vec4(-vsOutNormal.z * vsOutCol, 1.f);
})***";

    auto* regular = driver->loadFont("/usr/share/fonts/truetype/gentium/Gentium-R.ttf");
    auto* italic = driver->loadFont("/usr/share/fonts/truetype/gentium/Gentium-I.ttf");

    if (!regular || !italic)
    {
            return 1;
    }

    auto* currFont = regular;

    std::string text = R"(A little text to show that text-rendering
works!)";

    auto metrics = kg::typeset(text,
                               kg::TextAlignment::Justified,
                               [&currFont, regular, italic, &text]
                               (size_t i)
                               {
                                    if (text[i] == '\n') currFont = currFont == regular ? italic : regular;
                                    return currFont;
                               },
                               [](size_t){ return 32; },
                               600);

    auto shader = driver->createShader();
    if (!shader->addShader(vertexShaderCode, kg::Shader::Vertex))
    {
        std::cerr << "Error in adding vertex shader.\n";
    }
    if (!shader->addShader(fragmentShaderCode, kg::Shader::Fragment))
    {
        std::cerr << "Error in adding fragment shader.\n";
    }
    if (!shader->compile())
    {
        std::cerr << "Shader compilation error.\n";
    }

    std::string postVertexShaderCode =
R"***(#version 330 core

in vec4 position;
in vec2 texcoord;

out vec2 texCoord;

void main()
{
    texCoord = texcoord;
    gl_Position = position;
})***";

    std::string postFragmentShaderCode =
R"***(#version 330 core

in vec2 texCoord;
uniform ivec2 blur;
uniform vec2 pixelSize;
uniform sampler2D fbTex;
uniform sampler2D depthStencil;

out vec4 colour;

vec4 computeBlur()
{
    vec4 col = vec4(0, 0, 0, 1);
    for (int x = -blur.x; x <= blur.x; ++x)
    {
        for (int y = -blur.y; y <= blur.y; ++y)
        {
            col += texture(fbTex, texCoord+pixelSize*vec2(x, y));
        }
    }
    return col/((2*blur.x+1)*(2*blur.y+1));
}

void main()
{
    vec2 coords = (sin((2.f*texCoord-vec2(1.f, 1.f))*1.570796327f)+vec2(1.f, 1.f))*0.5f;
    vec4 c = texture(fbTex, coords);
    vec4 ds = texture(depthStencil, coords);
    //colour = c;// c*vec4((vec3(1.f, 1.f, 1.f) - texture(depthStencil, coords).rrr)*10.f, 1.f);
    //*
    colour = 0.25f * (texture(fbTex, coords + pixelSize*vec2(3, 3))
                      + texture(fbTex, coords + pixelSize*vec2(3, -3))
                      + texture(fbTex, coords + pixelSize*vec2(-3, 3))
                      + texture(fbTex, coords + pixelSize*vec2(-3, -3)));//*/
    //colour = computeBlur();
    //colour = vec4(vec3(1.f, 1.f, 1.f) - texture(fbTex, texCoord).rgb, 1.f);
})***";

    auto postShader = driver->createShader();
    if (!postShader->addShader(postVertexShaderCode, kg::Shader::Vertex))
    {
        std::cerr << "Error in adding post vertex shader.\n";
    }
    if (!postShader->addShader(postFragmentShaderCode, kg::Shader::Fragment))
    {
        std::cerr << "Error in adding post fragment shader.\n";
    }
    if (!postShader->compile())
    {
        std::cerr << "Post shader compilation error.\n";
    }

    auto screenRect = driver->createMesh();
    {
        math::Vector<8, float> screenRectVertices[4] =
        {
            math::Vector<8, float>({-1.00f, -1.00f, 0.f,  0.f, 0.f,  0.f, 1.f, 0.f}),
            math::Vector<8, float>({ 1.00f, -1.00f, 0.f,  1.f, 0.f,  0.f, 1.f, 0.f}),
            math::Vector<8, float>({-1.00f,  1.00f, 0.f,  0.f, 1.f,  0.f, 1.f, 0.f}),
            math::Vector<8, float>({ 1.00f,  1.00f, 0.f,  1.f, 1.f,  1.f, 0.f, 0.f}),
        };

        kraken::U16 indices[6] =
        {
            0, 1, 2, 1, 3, 2
        };

        kg::VertexType vt;
        vt.push_back({"position", kg::VertexDataType::Float, 3});
        vt.push_back({"texcoord", kg::VertexDataType::Float, 2});
        vt.push_back({"colour", kg::VertexDataType::Float, 3});
        screenRect->construct(screenRectVertices, 4, vt, kg::PrimitiveType::Triangles,
                              indices, 2, 6);
    }

    bool framebufferResized = false;
    window.setFramebufferResizeCallback([&framebufferResized](kraken::U32, kraken::U32)
    {
        framebufferResized = true;
    });

    auto mesh = driver->createMesh();
    std::ifstream input("resources/stump.ply");

    {
        kraken::ScopeTimer plytimer("plytimer");
        auto importResult = kg::importPLY(input, mesh.get());
        std::cerr << "Import success? " << std::boolalpha << importResult << "\n";
    }

    //std::ofstream output("resources/stump-reexport.ply");
    //kg::exportPLY(output, mesh.get());
    driver->setCullMode(kg::CullMode::None);
    driver->enable(kg::Capability::DepthTest);

    kraken::Timer timer;
    auto framebuffer = driver->createFramebuffer({driver->width(),
                                                  driver->height()});
    framebuffer->createAttachment(0, kg::FramebufferFormat::ColourRGBA8);
    framebuffer->createAttachment(framebuffer->depthStencilAttachmentID(),
                                  kg::FramebufferFormat::Depth24Stencil8);

    double prevTime = timer.duration();
    while (!window.closeRequested())
    {
        auto now = timer.duration();
        auto dt = now - prevTime;
        prevTime = now;
        kw::Window::pollEvents();
        if (framebufferResized)
        {
            framebuffer->resize(driver->width(), driver->height());
            framebufferResized = false;
        }
        auto aspect = (float)driver->width()/(float)driver->height();
        float t = timer.duration() * 0.15f;
        math::vec3 camPos{5.f*std::cos(5.f*t), 2.5f*std::cos(3.f*t), 1.4f};
        math::vec3 camUp{0.f, .5f + .5f*std::cos(t), .5f + .5f*std::sin(t)};
        math::vec3 lookingAt{0.f, 0.f, 0.f};
        math::mat4 worldToCam = math::lookat(camPos, lookingAt, camUp);
        math::mat4 persp = math::perspective(1.f, aspect, 0.1f, 1000.f);
        driver->beginDraw(true, true, 0x6495edff);
        driver->use(shader.get());
        driver->useFramebuffer(framebuffer.get(), true, true);
        auto clearCol = kraken::image::unpack(0x6495edff, kraken::image::RGBA8);
        framebuffer->clearAttachment(0, clearCol.data());
        framebuffer->clearAttachment(framebuffer->depthStencilAttachmentID(),
                                     1.f, 0);

        math::vec2 pixelSize{1.f/driver->width(),
                             1.f/driver->height()};

        math::mat4 modelToWorld(1.f);
        shader->setUniform(shader->uniformID("transform"), persp * worldToCam * modelToWorld);
        driver->draw(mesh);

        driver->useFramebuffer(nullptr);

        driver->use(postShader.get());
        postShader->useTexture(postShader->textureID("fbTex"),
                               framebuffer->attachmentTexture(0));
        postShader->useTexture(postShader->textureID("depthStencil"),
                               framebuffer->attachmentTexture(framebuffer->depthStencilAttachmentID()));
        postShader->setUniform(postShader->uniformID("pixelSize"), pixelSize);
        postShader->setUniform(postShader->uniformID("blur"), math::ivec2{60, 5});
        driver->draw(screenRect);
        driver->draw(metrics, {16, 9});

        std::stringstream debugInfo;
        debugInfo << "FPS: " << 1.f / dt << "\n";
        debugInfo << "Frametime: " << (int)(1000*dt) << "ms\n";
        auto dbgmetrics = kg::typeset(debugInfo.str(),
                                      kg::TextAlignment::Right,
                                      [&regular](size_t)
                                      { return regular; },
                                      [](size_t){ return 32; });
        driver->draw(dbgmetrics, {(int)driver->width(), 0}, kg::Alignment::TopRight);
        driver->endDraw();
    }
}
