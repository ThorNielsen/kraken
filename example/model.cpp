#include <kraken/graphics/mesh.hpp>
#include <kraken/graphics/shader.hpp>
#include <kraken/graphics/texture.hpp>
#include <kraken/graphics/texturesampler.hpp>
#include <kraken/graphics/videodriver.hpp>
#include <kraken/graphics/vertex.hpp>
#include <kraken/window/window.hpp>
#include <kraken/math/transform.hpp>

#include <kraken/utility/timer.hpp>

#include <kraken/graphics/font.hpp>
#include <kraken/graphics/typesetting.hpp>

#include <kraken/graphics/import.hpp>
#include <kraken/graphics/export.hpp>
#include <fstream>

#include <kraken/graphics/opengl/window.hpp>

#include <kraken/geometry/mesh.hpp>
#include <kraken/geometry/convexity.hpp>
#include <kraken/geometry/conversion.hpp>
#include <kraken/geometry/import.hpp>

int main()
{
    namespace kg = kraken::graphics;
    namespace kw = kraken::window;
    namespace math = kraken::math;
    kw::WindowCreationSettings ws;
    kg::opengl::ContextSettings cs;
    ws.width = 640;
    ws.height = 480;
    kw::Window window;
    if (!window.open(ws, kg::opengl::surfaceCreator(cs)))
    {
        throw std::runtime_error("Couldn't open window.");
    }
    window.setKeyPressCallback([&window]
        (kw::Key key, kw::Scancode, kw::KeyModifiers)
        {
            if (key == kw::Key::Escape)
            {
                window.setCloseFlag();
            }
        });
    auto* driver = kg::VideoDriver::driver(window);

    std::string vertexShaderCode =
R"***(#version 330 core

uniform mat4 transform;

in vec3 position;
in vec3 normal;

out vec3 vsOutCol;

void main()
{
    vsOutCol = normal*0.5f + vec3(0.5f, 0.5f, 0.5f);
    gl_Position = transform * vec4(position, 1);
})***";

    std::string fragmentShaderCode =
R"***(#version 330 core

in vec3 vsOutCol;
out vec4 colour;

void main()
{
    //colour = vec4(0.f, 0.f, 0.f, 1.f);
    colour = vec4(vsOutCol, 1.f);
})***";

    auto* regular = driver->loadFont("/usr/share/fonts/truetype/gentium/Gentium-R.ttf");
    auto* italic = driver->loadFont("/usr/share/fonts/truetype/gentium/Gentium-I.ttf");

    if (!regular || !italic)
    {
            return 1;
    }

    auto* currFont = regular;

    std::string text = R"(Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.)";

    auto metrics = kg::typeset(text,
                               kg::TextAlignment::Justified,
                               [&currFont, regular, italic, &text]
                               (size_t i)
                               {
                                    if (text[i] == '\n') currFont = currFont == regular ? italic : regular;
                                    return currFont;
                               },
                               [](size_t){ return 32; },
                               600);

    auto shader = driver->createShader();
    if (!shader->addShader(vertexShaderCode, kg::Shader::Vertex))
    {
        std::cerr << "Error in adding vertex shader.\n";
    }
    if (!shader->addShader(fragmentShaderCode, kg::Shader::Fragment))
    {
        std::cerr << "Error in adding fragment shader.\n";
    }
    if (!shader->compile())
    {
        std::cerr << "Shader compilation error.\n";
    }

    kg::VertexType vt;

    auto mesh = driver->createMesh();
    std::ifstream input("resources/stump.ply");
    kraken::geometry::Mesh geoMesh;
    if (!kraken::geometry::importPLY(input, geoMesh))
    {
        std::cerr << "Failed to import PLY.\n";
        return 1;
    }
    kraken::geometry::toFlatRenderMesh(geoMesh,
                                       [&mesh](size_t bytes)
                                       {
                                           mesh->resizeVertexBuffer(bytes);
                                           return mesh->vertices();
                                       },
                                       vt);
    driver->setCullMode(kg::CullMode::None);
    driver->enable(kg::Capability::DepthTest);

    mesh->setVertexType(vt);
    mesh->update();

    std::vector<math::vec3> vertices;
    for (auto v = geoMesh.vbegin(); v != geoMesh.vend(); ++v)
    {
        vertices.push_back(v->pos);
    }

    auto chull = kraken::geometry::convexHull(vertices.data(), vertices.size());

    auto convHullMesh = driver->createMesh();

    kg::VertexType chVType;


    kraken::geometry::toFlatRenderMesh(chull,
                                       [&convHullMesh](size_t bytes)
                                       {
                                           convHullMesh->resizeVertexBuffer(bytes);
                                           return convHullMesh->vertices();
                                       },
                                       chVType);
    convHullMesh->setVertexType(chVType);
    convHullMesh->setPrimitiveType(kg::PrimitiveType::Triangles);
    convHullMesh->update();

    kraken::Timer timer;

    while (!window.closeRequested())
    {
        kw::Window::pollEvents();
        auto aspect = (float)driver->width()/(float)driver->height();
        float t = timer.duration() * 0.15f;
        math::vec3 camPos{5.f*std::cos(t), 1.25f + .75f*std::cos(5*t), 5.f*std::sin(t)};
        //camPos *= 0.01f;
        camPos *= 0.25f;
        math::vec3 camUp{0.f, 1.f, 0.f};
        math::vec3 lookingAt{0.f, 1.25f, 0.f};
        math::mat4 worldToCam = math::lookat(3.f*camPos, lookingAt, camUp);
        math::mat4 persp = math::perspective(1.f, aspect, 0.1f, 1000.f);
        driver->beginDraw(true, true, 0x6495edff);
        driver->use(shader.get());

        math::mat4 modelToWorld(1.f);
        shader->setUniform(shader->uniformID("transform"), persp * worldToCam * modelToWorld);
        driver->setCullMode(kg::CullMode::Back);
        driver->setRenderMode(kg::RenderMode::Solid);
        driver->draw(mesh);
        driver->setCullMode(kg::CullMode::None);
        driver->setRenderMode(window.isPressed(kw::Key::W) ? kg::RenderMode::Wireframe : kg::RenderMode::Solid);
        driver->draw(convHullMesh);

        driver->draw(metrics, {16, 9});
        driver->endDraw();
    }
}
