#include <kraken/graphics/mesh.hpp>
#include <kraken/graphics/shader.hpp>
#include <kraken/graphics/texture.hpp>
#include <kraken/graphics/texturesampler.hpp>
#include <kraken/graphics/videodriver.hpp>
#include <kraken/graphics/vertex.hpp>
#include <kraken/window/window.hpp>
#include <kraken/math/transform.hpp>

#include <kraken/utility/timer.hpp>

#include <kraken/graphics/font.hpp>
#include <kraken/graphics/typesetting.hpp>

#include <iostream>

#include <kraken/graphics/import.hpp>
#include <kraken/graphics/export.hpp>
#include <fstream>

#include <kraken/graphics/opengl/window.hpp>

#include <kraken/geometry/mesh.hpp>

int main()
{
    namespace kg = kraken::graphics;
    namespace kw = kraken::window;
    namespace math = kraken::math;
    kw::WindowCreationSettings ws;
    kg::opengl::ContextSettings cs;
    ws.width = 640;
    ws.height = 480;
    kw::Window window;
    if (!window.open(ws, kg::opengl::surfaceCreator(cs)))
    {
        throw std::runtime_error("Couldn't open window.");
    }
    window.setKeyPressCallback([&window]
        (kw::Key key, kw::Scancode, kw::KeyModifiers)
        {
            if (key == kw::Key::Escape)
            {
                window.setCloseFlag();
            }
        });
    auto* driver = kg::VideoDriver::driver(window);

    std::string vertexShaderCode =
R"***(#version 330 core

uniform mat4 transform;

in vec3 position;
in vec3 colour;

out vec3 vsOutCol;

void main()
{
    vsOutCol = colour;
    gl_Position = transform * vec4(position, 1);
})***";

    std::string fragmentShaderCode =
R"***(#version 330 core

in vec3 vsOutCol;
out vec4 colour;

void main()
{
    //colour = vec4(0.f, 0.f, 0.f, 1.f);
    colour = vec4(vsOutCol, 1.f);
})***";

    auto* regular = driver->loadFont("/usr/share/fonts/truetype/gentium/Gentium-R.ttf");
    auto* italic = driver->loadFont("/usr/share/fonts/truetype/gentium/Gentium-I.ttf");

    if (!regular || !italic)
    {
            return 1;
    }

    auto* currFont = regular;

    std::string text = R"(Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.)";

    auto metrics = kg::typeset(text,
                               kg::TextAlignment::Justified,
                               [&currFont, regular, italic, &text]
                               (size_t i)
                               {
                                    if (text[i] == '\n') currFont = currFont == regular ? italic : regular;
                                    return currFont;
                               },
                               [](size_t){ return 32; },
                               600);

    auto shader = driver->createShader();
    if (!shader->addShader(vertexShaderCode, kg::Shader::Vertex))
    {
        std::cerr << "Error in adding vertex shader.\n";
    }
    if (!shader->addShader(fragmentShaderCode, kg::Shader::Fragment))
    {
        std::cerr << "Error in adding fragment shader.\n";
    }
    if (!shader->compile())
    {
        std::cerr << "Shader compilation error.\n";
    }

    math::Vector<6, float> positions[12] =
    {
        math::Vector<6, float>({0.f, 0.f, 0.f,  1.f, 0.f, 0.f}),
        math::Vector<6, float>({0.f, 1.f, 0.f,  1.f, 0.f, 0.f}),
        math::Vector<6, float>({1.f, 0.f, 0.f,  1.f, 0.f, 0.f}),

        math::Vector<6, float>({0.f, 0.f, 0.f,  0.f, 1.f, 0.f}),
        math::Vector<6, float>({1.f, 0.f, 0.f,  0.f, 1.f, 0.f}),
        math::Vector<6, float>({0.f, 0.f, 1.f,  0.f, 1.f, 0.f}),

        math::Vector<6, float>({0.f, 0.f, 0.f,  0.f, 0.f, 1.f}),
        math::Vector<6, float>({0.f, 0.f, 1.f,  0.f, 0.f, 1.f}),
        math::Vector<6, float>({0.f, 1.f, 0.f,  0.f, 0.f, 1.f}),

        math::Vector<6, float>({1.f, 0.f, 0.f,  1.f, 1.f, 0.f}),
        math::Vector<6, float>({0.f, 1.f, 0.f,  1.f, 1.f, 0.f}),
        math::Vector<6, float>({0.f, 0.f, 1.f,  1.f, 1.f, 0.f}),
    };

    kg::VertexType vt;
    vt.push_back({"position", kg::VertexDataType::Float, 3});
    vt.push_back({"colour", kg::VertexDataType::Float, 3});

    auto mesh = driver->createMesh();
    size_t vertexComponents = 6;
    size_t xRes = 251;
    size_t yRes = 251;
    math::vec2 upperLeft{(1-(float)xRes)/2, (1-(float)yRes)/2};
    math::vec2 lowerRight{((float)xRes-1)/2, ((float)yRes-1)/2};
    mesh->setVertexType(vt);
    mesh->setPrimitiveType(kg::PrimitiveType::Triangles);
    mesh->resizeVertexBuffer(4 * vertexComponents * xRes * yRes * 12);
    float* writeAt = (float*)mesh->vertices();
    for (size_t xIdx = 0; xIdx < xRes; ++xIdx)
    {
        for (size_t yIdx = 0; yIdx < yRes; ++yIdx)
        {
            math::vec2 xy = lowerRight - upperLeft;
            xy.x /= xRes;
            xy.y /= yRes;
            float xOff = upperLeft.x + xIdx*xy.x;
            float yOff = upperLeft.y + yIdx*xy.y;
            for (size_t i = 0; i < 12; ++i)
            {
                *writeAt++ = positions[i](0)+xOff;
                *writeAt++ = positions[i](1)+yOff;
                *writeAt++ = positions[i](2);
                *writeAt++ = positions[i](3);
                *writeAt++ = positions[i](4);
                *writeAt++ = positions[i](5);
            }
        }
    }
    mesh->update();

    kraken::Timer timer;

    driver->enable(kg::Capability::DepthTest);
    while (!window.closeRequested())
    {
        kw::Window::pollEvents();
        auto aspect = (float)driver->width()/(float)driver->height();
        float t = timer.duration() * 0.15f;
        math::vec3 camPos{5.f*std::cos(t), 2.5f*std::cos(5*t), 5.f*std::sin(t)};
        //camPos *= 0.01f;
        camPos *= 0.5f;
        math::vec3 camUp{0.f, 1.f, 0.f};
        math::vec3 lookingAt{0.f, 0.f, 0.f};
        math::mat4 worldToCam = math::lookat(3.f*camPos, lookingAt, camUp);
        math::mat4 persp = math::perspective(1.f, aspect, 0.1f, 1000.f);
        driver->beginDraw(true, true, 0x6495edff);
        driver->use(shader.get());
        for (float x = -3.f; x <= 3.01f; ++x)
        {
            for (float y = -3.f; y <= 3.01f; ++y)
            {
                math::mat4 modelToWorld(1.f);
                modelToWorld(0,3) = x*xRes;
                modelToWorld(1,3) = y*yRes;
                shader->setUniform(shader->uniformID("transform"), persp * worldToCam * modelToWorld);
                driver->setCullMode(kg::CullMode::Back);
                driver->setRenderMode(kg::RenderMode::Solid);
                driver->draw(mesh);
            }
        }
        driver->draw(metrics, {16, 9});
        driver->endDraw();
    }
}
