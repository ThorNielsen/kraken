#include <kraken/graphics/mesh.hpp>
#include <kraken/graphics/shader.hpp>
#include <kraken/graphics/texture.hpp>
#include <kraken/graphics/texturesampler.hpp>
#include <kraken/graphics/videodriver.hpp>
#include <kraken/graphics/vertex.hpp>
#include <kraken/window/window.hpp>
#include <kraken/graphics/opengl/window.hpp>

#include <iostream>

int main()
{
    namespace kg = kraken::graphics;
    namespace kw = kraken::window;
    namespace math = kraken::math;
    kw::WindowCreationSettings ws;
    kg::opengl::ContextSettings cs;
    ws.width = 640;
    ws.height = 480;
    kw::Window window;
    if (!window.open(ws, kg::opengl::surfaceCreator(cs)))
    {
        throw std::runtime_error("Couldn't open window.");
    }
    window.setKeyPressCallback([&window]
        (kw::Key key, kw::Scancode, kw::KeyModifiers)
        {
            if (key == kw::Key::Escape)
            {
                window.setCloseFlag();
            }
        });

    std::string vertexShaderCode =
R"***(#version 330 core

uniform mat3 transform;
in vec3 position;
in vec2 texcoord;
in vec3 colourIn;

out vec2 tc;
out vec4 colOut;

void main()
{
    colOut = vec4(colourIn, 1.f);
    tc = texcoord;
    gl_Position = vec4(transform * position, 1);
})***";

    std::string fragmentShaderCode =
R"***(#version 330 core

in vec2 tc;
in vec4 colOut;

out vec4 colour;

uniform sampler2D colTex;
uniform float mixFac;

void main()
{
    colour = mix(texture(colTex, tc), colOut, mixFac);
})***";

    auto driver = kg::VideoDriver::driver(window);

    auto shader = driver->createShader();
    if (!shader->addShader(vertexShaderCode, kg::Shader::Vertex))
    {
        std::cerr << "Error in adding vertex shader.\n";
    }
    if (!shader->addShader(fragmentShaderCode, kg::Shader::Fragment))
    {
        std::cerr << "Error in adding fragment shader.\n";
    }
    if (!shader->compile())
    {
        std::cerr << "Shader compilation error.\n";
    }

    math::Vector<8, float> positions[4] =
    {
        math::Vector<8, float>({-1.00f, -1.00f, 0.f,  0.f, 1.f,  0.f, 1.f, 0.f}),
        math::Vector<8, float>({ 1.00f, -1.00f, 0.f,  1.f, 1.f,  0.f, 1.f, 0.f}),
        math::Vector<8, float>({-1.00f,  1.00f, 0.f,  0.f, 0.f,  0.f, 1.f, 0.f}),
        math::Vector<8, float>({ 1.00f,  1.00f, 0.f,  1.f, 0.f,  1.f, 0.f, 0.f}),
    };

    kraken::U16 indices[6] =
    {
        0, 1, 2, 1, 3, 2
    };

    kg::VertexType vt;
    vt.push_back({"position", kg::VertexDataType::Float, 3});
    vt.push_back({"texcoord", kg::VertexDataType::Float, 2});
    vt.push_back({"colourIn", kg::VertexDataType::Float, 3});

    auto mesh = driver->createMesh();
    mesh->construct(positions, 4, vt, kg::PrimitiveType::Triangles,
                    indices, 2, 6);

    auto texture = driver->create2DTexture();
    auto sampler = driver->createTextureSampler();
    sampler->setMagFilter(kg::InterpolationFilter::Nearest);
    sampler->setMinFilter(kg::InterpolationFilter::Linear);
    kraken::image::ColourType ct;
    ct.bitDepth = 8;
    ct.channels = 3;
    kraken::image::Image img(255, 255, ct);
    img.fill([](unsigned x, unsigned y)
             {
                 return math::uvec4{(x+y)&0xff, 255*(1&(x^y)), x&0xff, 255};
             });
    texture->createFromImage(img);
    texture->update();

    float frame = 0.f;
    while (!window.closeRequested())
    {
        kw::Window::pollEvents();
        driver->beginDraw(true, true, 0x6495edff);
        driver->use(shader.get());
        shader->setUniform(shader->uniformID("mixFac"), 0.5f + 0.5f*std::cos(0.01f*frame));
        shader->setUniform(shader->uniformID("transform"), math::mat3(0.7f + 0.3f*std::sin(0.01f*frame++)));
        shader->useTexture(shader->textureID("colTex"),
                           texture.get(),
                           sampler.get());
        mesh->draw(driver);
        driver->endDraw();
    }
}
