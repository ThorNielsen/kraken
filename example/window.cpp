#include <kraken/graphics/mesh.hpp>
#include <kraken/graphics/shader.hpp>
#include <kraken/graphics/videodriver.hpp>
#include <kraken/graphics/vertex.hpp>
#include <kraken/graphics/opengl/window.hpp>
#include <kraken/image/image.hpp>
#include <kraken/window/window.hpp>

#include <iostream>


int main()
{
    namespace kg = kraken::graphics;
    namespace ki = kraken::image;
    namespace kw = kraken::window;
    namespace math = kraken::math;
    kw::WindowCreationSettings ws;
    ws.title = "This is certainly a title befitting a window.";
    kg::opengl::ContextSettings cs;
    ws.width = 640;
    ws.height = 480;

    std::vector<kw::Window> windows(2);
    bool close = false;
    enum Movement : size_t
    {
        Up = 0, Down, Left, Right, In, Out,
    };
    bool move[6] = {false, false, false, false, false, false};
    unsigned int maxIt = 64;
    math::vec2 mp{0.f, 0.f};
    for (auto& window : windows)
    {
        if (!window.open(ws, kg::opengl::surfaceCreator(cs)))
        {
            throw std::runtime_error("Couldn't open window.");
        }
        std::cerr << kg::VideoDriver::driver(window) << "\n";
        window.setKeyPressCallback([&close, &move, &maxIt, &window]
            (kw::Key key, kw::Scancode, kw::KeyModifiers)
            {
                if (key == kw::Key::Escape)
                    close = true;
                if (key == kw::Key::Up) move[Up] = true;
                if (key == kw::Key::Down) move[Down] = true;
                if (key == kw::Key::Left) move[Left] = true;
                if (key == kw::Key::Right) move[Right] = true;
                if (key == kw::Key::I) move[In] = true;
                if (key == kw::Key::O) move[Out] = true;
                if (key == kw::Key::W) ++maxIt;
                if (key == kw::Key::D) maxIt += 10;
                if (key == kw::Key::F) maxIt += 1000;
                if (key == kw::Key::S && maxIt) --maxIt;
                if (key == kw::Key::A && maxIt >= 10) maxIt -= 10;
                if (key == kw::Key::Key1)
                {
                    window.setDecorated(!window.decorated());
                }
                if (key == kw::Key::Key2)
                {
                    window.setTopmost(!window.topmost());
                }
                if (key == kw::Key::Key3)
                {
                    window.setResizable(!window.resizable());
                }
            });
        window.setKeyRepeatCallback([&close, &move, &maxIt]
            (kw::Key key, kw::Scancode, kw::KeyModifiers)
            {
                if (key == kw::Key::W) ++maxIt;
                if (key == kw::Key::D) maxIt += 10;
                if (key == kw::Key::F) maxIt += 1000;
                if (key == kw::Key::S && maxIt) --maxIt;
                if (key == kw::Key::A && maxIt >= 10) maxIt -= 10;
            });
        window.setKeyReleaseCallback([&move]
            (kw::Key key, kw::Scancode, kw::KeyModifiers)
            {
                if (key == kw::Key::Up) move[Up] = false;
                if (key == kw::Key::Down) move[Down] = false;
                if (key == kw::Key::Left) move[Left] = false;
                if (key == kw::Key::Right) move[Right] = false;
                if (key == kw::Key::I) move[In] = false;
                if (key == kw::Key::O) move[Out] = false;
            });
        window.setMousePressCallback([&close]
            (kw::Mouse button, kw::KeyModifiers)
            {
                if (button == kw::Mouse::Middle)
                {
                    close = true;
                }
            });
        window.setCursorPositionCallback([&mp]
            (kraken::F64 x, kraken::F64 y)
            {
                mp = {(float)x, (float)y};
            });
        window.setUTFCharCallback([]
            (kraken::U32 codepoint)
            {
                kraken::String s(codepoint);
                std::cout << s << std::flush;
            });
        kg::VideoDriver::driver(window)->beginDraw();
        kg::VideoDriver::driver(window)->endDraw();
    }
    windows[0].setTitle("Window #1");
    //windows[1].setTitle("Window #2");

    ki::Image icon(64, 64, ki::RGBA8);
    icon.fill([](kraken::U32 x, kraken::U32 y)
        {
            return math::vec4{x/64.f, y/64.f, 0.f, 1.f};
        });

    windows[0].setIcon(icon);
    windows[0].setCursor(kw::Cursor(kw::CursorType::Crosshair));

    windows[1].setCursor(kw::Cursor(icon, {32, 32}));


    unsigned col = 0;

    std::string vertexShaderCode =
R"***(#version 330 core

in vec3 position;

out vec2 interp;

void main()
{
    interp = position.xy;
    gl_Position = vec4(position, 1);
})***";

    std::string fragmentShaderCode =
R"***(#version 330 core
/*
#extension GL_ARB_gpu_shader_fp64 : enable

#define Prec double
#define Prec2 dvec2
/*/
#define Prec float
#define Prec2 vec2

uniform vec2 center;
uniform vec2 extents;

uniform vec2 mp;

in vec2 interp;
out vec4 colour;

uniform uint maxIterations;

int mandel(Prec x, Prec y, int maxIt)
{
    Prec cx = x;
    Prec cy = y;
    int i;
    for (i = 0; i < maxIt && x*x+y*y <= 4.; ++i)
    {
        Prec tmp = 2*x*y+cy;
        x = (x*x - y*y+cx);
        y = (tmp);
    }
    return i;
}

void main()
{
    Prec2 complexPos = Prec2(center) + Prec2(interp) * Prec2(extents);
    int maxIt = int(maxIterations);

    Prec2 pos = Prec2(complexPos.x, -complexPos.y);
    vec2 diff = mp-gl_FragCoord.xy;

    int it = mandel(pos.x, pos.y, maxIt);
    vec4 bg = vec4(0, 0, 0, 1);
    vec4 fg = vec4(0, 1, 1, 1);
    if (diff.x < 0)
    {
        fg = vec4(0, 1, 0, 1);
    }
    fg = mix(vec4(0, 1, 1, 1), vec4(0, 1, 0, 1), 1.-clamp(float(mandel(diff.x/500, diff.y/500, maxIt))/float(maxIt), 0., 1.));
    fg = mix(fg, vec4(1, 1, 0, 1), 1.-clamp(length(diff)/500, 0., 1.));
    colour = mix(bg, fg, log(float(it%maxIt+1))/log(maxIt+1));
    //if (dot(diff,diff) < 900) colour = vec4(1,1,1,0)-colour;
})***";
    auto shader = kg::VideoDriver::driver(windows[0])->createShader();
    if (!shader->addShader(vertexShaderCode, kg::Shader::Vertex))
    {
        std::cerr << "Error in adding vertex shader.\n";
    }
    if (!shader->addShader(fragmentShaderCode, kg::Shader::Fragment))
    {
        std::cerr << "Error in adding fragment shader.\n";
    }
    if (!shader->compile())
    {
        std::cerr << "Shader compilation error.\n";
    }

    math::vec2 center{0,0};
    math::vec2 extents{1.777777778f,1};

    math::vec3 positions[4] =
    {
        math::vec3{-1.00f, -1.00f, 0.f},
        math::vec3{ 1.00f, -1.00f, 0.f},
        math::vec3{-1.00f,  1.00f, 0.f},
        math::vec3{ 1.00f,  1.00f, 0.f},
    };

    kraken::U16 indices[6] =
    {
        0, 1, 2, 1, 3, 2
    };

    kg::VertexType vt;
    vt.push_back({"position", kg::VertexDataType::Float, 3});

    auto mesh = kg::VideoDriver::driver(windows[0])->createMesh();
    mesh->construct(positions, 4, vt, kg::PrimitiveType::Triangles,
                    indices, 2, 6);

    bool window0Deleted = false;
    auto centerID = shader->uniformID("center");
    auto extentsID = shader->uniformID("extents");
    auto maxItID = shader->uniformID("maxIterations");
    auto mpID = shader->uniformID("mp");

    while (!windows.empty() && !close)
    {
        float xMov = extents.x / 250;
        float yMov = extents.y / 250;
        center.x += xMov * (move[Right] - (int)move[Left]);
        center.y += yMov * (move[Up] - (int)move[Down]);
        float zoom = 1.0025f;// + 0.01f;
        if (move[Out]) extents *= zoom;
        if (move[In]) extents /= zoom;
        col = (col + 1) & 0xffffff;
        kw::Window::pollEvents();
        for (size_t i = 0; i < windows.size();)
        {
            kg::VideoDriver* driver = kg::VideoDriver::driver(windows[i]);
            driver->beginDraw(true, true, (col << 8) | 0xff | (i << 31));
            if (i == 0 && !window0Deleted)
            {
                extents.x = extents.y*driver->width()/driver->height();
                driver->use(shader.get());
                shader->setUniform(centerID, center);
                shader->setUniform(extentsID, extents);
                shader->setUniform(maxItID, maxIt);
                shader->setUniform(mpID, math::vec2{mp.x, driver->height()-mp.y});
                mesh->draw(driver);
            }
            driver->endDraw();
            if (windows[i].closeRequested())
            {
                if (!i) window0Deleted = true;
                std::cerr << "Window[" << i << "]: CLOSE REQUESTED!\n";
                windows[i].close();
                windows.erase(windows.begin() + i);
                std::cerr << "Closed; new size " << windows.size() << "\n";
            }
            else
            {
                ++i;
            }
        }
    }
}
