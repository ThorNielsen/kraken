#ifndef KRAKEN_GEOMETRY_ALGORITHM_HPP_INCLUDED
#define KRAKEN_GEOMETRY_ALGORITHM_HPP_INCLUDED

#include <kraken/geometry/distance.hpp>

#include <utility>

namespace kraken::geometry
{

template <typename Iterator, typename Reference>
auto closestPoint(Iterator begin, Iterator end, Reference ref)
-> std::pair<Iterator, decltype(closestPoint(*begin, ref))>
{
    if (begin == end) return {end, {}};

    auto bestPoint = closestPoint(*begin, ref);
    auto bestDist = squaredDistance(bestPoint, ref);
    auto bestIt = begin++;

    while (begin != end)
    {
        auto currPoint = closestPoint(*begin, ref);
        auto dist = squaredDistance(currPoint, ref);
        if (dist < bestDist)
        {
            bestDist = dist;
            bestIt = begin;
            bestPoint = currPoint;
        }
        ++begin;
    }
    return {bestIt, bestPoint};
}

} // namespace kraken::geometry

#endif // KRAKEN_GEOMETRY_ALGORITHM_HPP_INCLUDED

