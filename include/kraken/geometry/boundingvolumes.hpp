#ifndef KRAKEN_GEOMETRY_BOUNDINGVOLUMES_HPP_INCLUDED
#define KRAKEN_GEOMETRY_BOUNDINGVOLUMES_HPP_INCLUDED

#include "kraken/math/constants.hpp"
#include "kraken/math/matrix.hpp"
#include <cstddef>
#include <kraken/geometry/primitives.hpp>

#include <algorithm>
#include <array>
#include <limits>

namespace kraken::geometry
{

/// This represents a ball in three-dimensional space; namely all the points
/// which have distance ≤ 'radius' from 'origin'.
template <typename Prec>
struct Sphere
{
    using scalar_type = Prec;

    Vector3<Prec> origin;
    Prec radius; ///< Must be ≥0.

    // Warning: Not tight; uses average of points to determine midpoint.
    static Sphere<Prec> bound(const Vector3<Prec>* points,
                              size_t pointCount) noexcept
    {
        Sphere<Prec> sphere;
        sphere.origin = Vector3<Prec>{0, 0, 0};
        for (size_t i = 0; i < pointCount; ++i)
        {
            sphere.origin += points[i];
        }
        if (pointCount) sphere.origin *= Prec(1) / static_cast<Prec>(pointCount);
        sphere.radius = 0;
        for (size_t i = 0; i < pointCount; ++i)
        {
            sphere.radius = std::max(sphere.radius,
                                     squaredLength(sphere.origin-points[i]));
        }
        sphere.radius = std::sqrt(sphere.radius);
        return sphere;
    }

    bool overlaps(const Sphere<Prec>& other) const noexcept
    {
        return length(origin-other.origin) <= radius + other.radius;
    }

    bool contains(Vector3<Prec> point) const noexcept
    {
        return squaredLength(point-origin) <= radius * radius;
    }

    Sphere<Prec>& operator|=(const Sphere<Prec>& other) noexcept
    {
        auto originToOtherOrigin = other.origin - origin;
        auto d = length(originToOtherOrigin);
        if (d < 20*math::Constant<Prec>::epsilon)
        {
            radius = std::max(radius, other.radius) + d;
            return *this;
        }
        Prec parMin = std::min(-radius, d-other.radius);
        Prec parMax = std::max(radius, d+other.radius);
        auto finalDiameter = parMax - parMin;

        origin += (Prec(.5) / d) * (parMin + parMax) * originToOtherOrigin;
        radius = Prec(.5) * finalDiameter;
        return *this;
    }
};

template <typename Prec>
Sphere<Prec> operator|(Sphere<Prec> a, const Sphere<Prec>& b)
{
    return a |= b;
}

/// This represents an axis-aligned bounding box in three-dimensional space; its
/// volume is exactly everything which is contained between the two corners;
/// more precisely any point (x, y, z)∈ℝ for which minCorner.x ≤ x ≤ maxCorner.x
/// and identical statements for the y and z coordinates.
template <typename Prec>
struct Aabb
{
    using scalar_type = Prec;

    Vector3<Prec> center;
    Vector3<Prec> halfExtents; ///< Each component must be nonnegative if this
                               ///  box represents a nonempty set.

    static Aabb<Prec> bound(const Vector3<Prec>* points,
                            size_t pointCount)
    {
        Vector3<Prec> minCorner{std::numeric_limits<Prec>::max(),
                                std::numeric_limits<Prec>::max(),
                                std::numeric_limits<Prec>::max()};
        auto maxCorner = -minCorner;
        for (size_t i = 0; i < 3; ++i)
        {
            for (size_t vert = 0; vert < pointCount; ++vert)
            {
                minCorner[i] = std::min(minCorner[i], points[vert][i]);
                maxCorner[i] = std::max(maxCorner[i], points[vert][i]);
            }
        }

        Aabb<Prec> aabb;
        aabb.center = (maxCorner + minCorner) * Prec(.5);
        aabb.halfExtents = (maxCorner - minCorner) * Prec(.5);
        return aabb;
    }

    bool overlaps(const Aabb<Prec>& other) const noexcept
    {
        for (size_t i = 0; i < 3; ++i)
        {
            if (std::abs(center[i]-other.center[i])
                > halfExtents[i]+other.halfExtents[i])
            {
                return false;
            }
        }
        return true;
    }

    bool overlapsApproximate(const Aabb<Prec>& other, Prec maxDist) const noexcept
    {
        for (size_t i = 0; i < 3; ++i)
        {
            if (std::abs(center[i]-other.center[i])
                > halfExtents[i]+other.halfExtents[i]+maxDist)
            {
                return false;
            }
        }
        return true;
    }

    static std::array<Vector3<Prec>, 15>
    computeTestAxes(math::Matrix<3, 3, Prec> otherTransform) noexcept
    {
        std::array<Vector3<Prec>, 15> axes;
        axes[0] = {Prec(1), Prec(0), Prec(0)};
        axes[1] = {Prec(0), Prec(1), Prec(0)};
        axes[2] = {Prec(0), Prec(0), Prec(1)};
        axes[3] = otherTransform.col(0);
        axes[4] = otherTransform.col(1);
        axes[5] = otherTransform.col(2);
        size_t currIndex = 6;
        for (size_t i = 0; i < 3; ++i)
        {
            for (size_t j = 0; j < 3; ++j)
            {
                axes[currIndex++] = cross(axes[i], axes[3+j]);
            }
        }
        return axes;
    }

    static std::array<Vector3<Prec>, 15>
    computeNonzeroTestAxes(math::Matrix<3, 3, Prec> otherTransform,
                           size_t& nonzeroCountOut,
                           Prec epsilon = 50*math::Constant<Prec>::epsilon) noexcept
    {
        auto axes = computeTestAxes(otherTransform);
        auto sqEps = epsilon * epsilon;
        nonzeroCountOut = 15;
        // First six axes are the local coordinate system axes; so they are
        // always nonzero.
        for (size_t i = 6; i < nonzeroCountOut; ++i)
        {
            auto sqLen = squaredLength(axes[i]);
            if (sqLen < sqEps) std::swap(axes[i], axes[--nonzeroCountOut]);
        }
        return axes;
    }

    static std::array<Vector3<Prec>, 15>
    computeNormalisedTestAxes(math::Matrix<3, 3, Prec> otherTransform,
                              size_t* nonzeroAxeCountOut = nullptr,
                              Prec epsilon = 50*math::Constant<Prec>::epsilon) noexcept
    {
        auto axes = computeTestAxes(otherTransform);
        auto sqEps = epsilon * epsilon;
        size_t nonzeroAxeCount = 15;
        // First six axes are the local coordinate system axes; so they are
        // always nonzero.
        for (size_t i = 6; i < nonzeroAxeCount; ++i)
        {
            auto sqLen = squaredLength(axes[i]);
            if (sqLen < sqEps)
            {
                std::swap(axes[i], axes[--nonzeroAxeCount]);
            }
            else
            {
                auto invLen = Prec(1)/std::sqrt(sqLen);
                axes[i] *= invLen;
            }
        }

        if (nonzeroAxeCountOut) *nonzeroAxeCountOut = nonzeroAxeCount;
        return axes;
    }

    /// Tests overlap with a transformed AABB.
    ///
    /// This tests for overlap between this AABB and the box defined by
    /// otherTransform * other + otherOffset.
    ///
    /// \param other Other AABB.
    /// \param otherTransform The transform to be applied to the other AABB.
    /// \param otherOffset Offset to add to the other box.
    /// \param pAxes Pointer to precomputed array of axes to test (for
    ///              separating axis test).
    /// \param axeCount The number of axes to test (in the general case, 15).
    ///
    /// \returns Whether the two boxes overlap or not.
    bool overlaps(const Aabb<Prec>& other,
                  math::Matrix<3, 3, Prec> otherTransform,
                  Vector3<Prec> otherOffset,
                  const Vector3<Prec>* pAxes,
                  size_t axeCount) const noexcept
    {
        auto diff = (otherTransform * other.center + otherOffset) - center;
        for (size_t i = 0; i < axeCount; ++i)
        {
            auto sumLength = std::abs(dot(halfExtents, pAxes[i]))
                           + std::abs(dot(other.halfExtents, pAxes[i]));
            if (sumLength < std::abs(dot(diff, pAxes[i]))) return false;
        }
        return true;
    }

    /// Tests overlap with a transformed AABB.
    ///
    /// This tests for overlap between this AABB and the box defined by
    /// otherTransform * other + otherOffset.
    ///
    /// \param other Other AABB.
    /// \param otherTransform The transform to be applied to the other AABB.
    /// \param otherOffset Offset to add to the other box.
    ///
    /// \returns Whether the two boxes overlap or not.
    bool overlaps(const Aabb<Prec>& other,
                  math::Matrix<3, 3, Prec> otherTransform,
                  Vector3<Prec> otherOffset) const noexcept
    {
        size_t nonzeroAxeCount;
        auto epsilon = Prec(20) * math::Constant<Prec>::epsilon;
        auto axes = computeNonzeroTestAxes(otherTransform,
                                           nonzeroAxeCount,
                                           epsilon);

        return overlaps(other,
                        otherTransform,
                        otherOffset,
                        axes.data(),
                        nonzeroAxeCount);
    }

    bool contains(Vector3<Prec> point) const noexcept
    {
        for (size_t i = 0; i < 3; ++i)
        {
            if (std::abs(point[i]-center[i]) > halfExtents[i]) return false;
        }
        return true;
    }

    Aabb<Prec>& operator|=(const Aabb<Prec>& other) noexcept
    {
        auto minCorner = center-halfExtents;
        auto maxCorner = center+halfExtents;
        auto otherMinCorner = other.center-other.halfExtents;
        auto otherMaxCorner = other.center+other.halfExtents;

        for (size_t i = 0; i < 3; ++i)
        {
            minCorner[i] = std::min(minCorner[i], otherMinCorner[i]);
            maxCorner[i] = std::max(maxCorner[i], otherMaxCorner[i]);
        }

        halfExtents = (maxCorner - minCorner) * Prec(.5);
        center = (maxCorner + minCorner) * Prec(.5);

        return *this;
    }

    Aabb<Prec>& expand(Vector3<Prec> point) noexcept
    {
        auto minCorner = center-halfExtents;
        auto maxCorner = center+halfExtents;

        for (size_t i = 0; i < 3; ++i)
        {
            minCorner[i] = std::min(minCorner[i], point[i]);
            maxCorner[i] = std::max(maxCorner[i], point[i]);
        }

        halfExtents = (maxCorner - minCorner) * Prec(.5);
        center = (maxCorner + minCorner) * Prec(.5);

        return *this;
    }
};

template <typename Prec>
Aabb<Prec> operator|(Aabb<Prec> a, const Aabb<Prec>& b)
{
    return a |= b;
}

/// This is an oriented bounding box in ℝ³; with its orientation specified by
/// its 'orientation' quaternion, and with its size being 2*'extents', as the
/// extents stored here are the half-extents, measured from the center to the
/// corner(s).
template <typename Prec>
struct Obb
{
    using scalar_type = Prec;

    math::Quaternion<Prec> orientation;
    Vector3<Prec> extents; ///< The extents measured from the center and out.
    Vector3<Prec> center;
};

template <typename BoundingVolume>
BoundingVolume bound(const Vector3<typename BoundingVolume::scalar_type>* points,
                     size_t pointCount)
{
    return BoundingVolume::bound(points, pointCount);
}

template <typename BoundingVolume>
BoundingVolume bound(const Triangle3d<typename BoundingVolume::scalar_type>* triangles,
                     size_t triangleCount)
{
    // Triangle3d<Prec> is just an array of three Vector3<Prec>, and everything
    // is standard layout. So we just call the point-computing version:
    using Prec = typename BoundingVolume::scalar_type;
    const auto* ptr = reinterpret_cast<const Vector3<Prec>*>(triangles);
    return BoundingVolume::bound(ptr, triangleCount * 3);
}

} // namespace kraken::geometry

#endif // KRAKEN_GEOMETRY_BOUNDINGVOLUMES_HPP_INCLUDED
