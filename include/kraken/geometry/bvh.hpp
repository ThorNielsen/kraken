#ifndef KRAKEN_GEOMETRY_BVH_HPP_INCLUDED
#define KRAKEN_GEOMETRY_BVH_HPP_INCLUDED

#include "kraken/geometry/boundingvolumes.hpp"
#include "kraken/math/constants.hpp"
#include "kraken/types.hpp"
#include <cstddef>
#include <kraken/geometry/distance.hpp>
#include <kraken/geometry/primitivequeries.hpp>
#include <kraken/geometry/primitives.hpp>

#include <algorithm>
#include <array>
#include <functional>
#include <limits>
#include <memory>
#include <stack>
#include <stdexcept>
#include <type_traits>
#include <utility>
#include <vector>

namespace kraken::geometry
{

// Todo: Maybe unionise 'right' and 'elementOffset'.
template <typename BoundingElement, typename IndexType>
struct BVHNode
{
    BoundingElement bounding;
    IndexType left;
    IndexType right;
    IndexType elementOffset;
    IndexType elementCount;

    [[nodiscard]] bool isLeaf() const noexcept { return elementCount; }
};

/// \todo Make some way of identifying where the elements came from.
template <typename Element, typename BoundingElement, typename IndexType = U32>
struct Bvh
{
    // Instead of allocating BVH nodes as pointers all over the memory, all are
    // allocated in this single array, and the nodes simply index into here.
    std::vector<BVHNode<BoundingElement, IndexType>> nodes;
    // All elements are copied into here so that they are actually accessible
    // whenever necessary.
    std::vector<Element> elements;

    // Add "traversal scratch space" member for nonrecursive traversal?
};

/// Constructs a BVH from the given elements in a configurable way.
///
/// Given the various parameters, this constructs a BVH containing the
/// appropriate geometric elements along with enclosing bounding elements.
/// Although any depth is supported in maxDepth (since construction is done
/// without native recursion), care should be taken with deeper trees, as they
/// may not be traversable recursively. A few hundred layers is probably fine,
/// but more than that is likely problematic; not to mention inefficient, as
/// that is likely highly unbalanced.
/// Note, however, that this is an 'offline' algorithm -- this is not suitable
/// for updating an already constructed BVH, as everything will be recomputed.
///
/// \remark This is an offline algorithm, and thus unsuitable for adding
///         elements one by one. This function will always recompute the entire
///         BVH, even if given elements from some BVH.
///
/// \remark This copies all given elements; to ensure that they are available at
///         any time. This is currently not configurable.
///
/// \remark The leaf nodes of the generated BVH may contain more than one
///         element -- this can happen if the maximum depth is reached or some
///         range is unpartitionable.
///
/// \param elementBegin Forward iterator to the beginning of the elements.
/// \param elementEnd End iterator to the elements to construct the BVH from.
/// \param partition Let 'Elem' denote the element type. This should be a call-
///                  able object with the function signature
///                  const Elem* partition(Elem* begin, Elem* end)
///                  (the returned pointer is allowed to be non-const as well),
///                  whose function is to partition the range [begin, end) into
///                  [begin, returned), [returned, end), where 'returned' is the
///                  returned pointer. Each of the two ranges will then be used
///                  for constructing a BVH subtree.
///                  Note that if either of the ranges are empty, then all
///                  elements in that range will be put into a single BVH node.
/// \param boundingElement Let 'Elem' denote the element type. This should be a
///                        callable object with the signature
///                        BoundingElement boundingElement(const Elem* begin,
///                                                        const Elem* end)
///                        which constructs a bounding element containing all
///                        elements in the range [begin, end).
/// \param combine This should be a callable object of the signature
///                BoundingElement combine(BoundingElement a, BoundingElement b)
///                where the returned element is one containing both a and b.
/// \param maxDepth Maximum allowed depth of the generated BVH. Zero will be
///                 treated as (virtually) unlimited.
///
/// \returns A BVH generated using the specified parameters. The parent node has
///          index 0.
template <class ElementIterator,
          class PartitionFunction,
          class BoundingElementFunction,
          class BoundingElementCombiner,
          typename IndexType = U32>
auto buildBVHByMedian(ElementIterator elementBegin,
                      ElementIterator elementEnd,
                      PartitionFunction partition,
                      BoundingElementFunction boundingElement,
                      BoundingElementCombiner combine,
                      IndexType maxDepth = 256)
-> Bvh<std::remove_cv_t<std::remove_reference_t<decltype(*elementBegin)>>,
       std::remove_cv_t<std::remove_reference_t<typename decltype(std::function{boundingElement})::result_type>>,
       IndexType>
{
    constexpr static IndexType maxIndex = std::numeric_limits<IndexType>::max();
    if (!maxDepth) maxDepth = maxIndex;

    using BoundingElement = std::remove_cv_t<std::remove_reference_t<typename decltype(std::function{boundingElement})::result_type>>;

    Bvh<std::remove_cv_t<std::remove_reference_t<decltype(*elementBegin)>>,
        BoundingElement,
        IndexType> bvh;
    for (auto it = elementBegin; it != elementEnd; ++it)
    {
        bvh.elements.push_back(*it);
    }
    auto maxElementCount = 2*bvh.elements.size();
    if (maxElementCount >= static_cast<size_t>(maxIndex))
    {
        // Note: We use IndexType to also represent the indices into bvh.nodes,
        // and in the worst case, where all elements are single leaf nodes, we
        // have 2*bvh.elements.size()-1 nodes in total.
        throw std::domain_error("Too small index type for BVH.");
    }

    if (bvh.elements.empty()) return bvh;

    auto indexToParent = std::make_unique<IndexType[]>(maxElementCount);

    // Scope reducer, to deallocate all stack memory immediately.
    {
        struct ElementRange
        {
            IndexType begin;
            IndexType end;
            IndexType parent;
            IndexType depth;
            bool isLeft;
        };

        std::stack<ElementRange> unprocessed;
        unprocessed.push({IndexType{0},
                          static_cast<IndexType>(bvh.elements.size()),
                          maxIndex,
                          IndexType{0},
                          false});

        while (!unprocessed.empty())
        {
            IndexType currNodeIndex{static_cast<IndexType>(bvh.nodes.size())};
            if (currNodeIndex >= maxElementCount)
            {
                throw std::logic_error("Tried to construct too many BVH elements.");
            }
            auto [begin, end, parent, depth, isLeft] = unprocessed.top();
            unprocessed.pop();

            indexToParent[currNodeIndex] = parent;

            auto* beginPtr = bvh.elements.data() + begin;
            auto* endPtr = bvh.elements.data() + end;
            decltype(beginPtr) midPtr;
            if (depth >= maxDepth ||
                (midPtr = partition(beginPtr, endPtr)) == beginPtr
                || midPtr == endPtr)
            {
                bvh.nodes.push_back({boundingElement(beginPtr, endPtr),
                                     maxIndex,
                                     maxIndex,
                                     begin,
                                     end-begin});
            }
            else
            {
                auto mid = begin + static_cast<IndexType>(midPtr - beginPtr);

                bvh.nodes.push_back({BoundingElement{},
                                     maxIndex,
                                     maxIndex,
                                     maxIndex,
                                     0});
                unprocessed.push({begin, mid, currNodeIndex, depth+1, true});
                unprocessed.push({mid, end, currNodeIndex, depth+1, false});
            }
            if (parent != maxIndex)
            {
                if (isLeft) bvh.nodes[parent].left = currNodeIndex;
                else bvh.nodes[parent].right = currNodeIndex;
            }
        }
    }

    // If there's only a single node, then we don't need to combine bounding
    // boxes.
    if (bvh.nodes.size() == 1) return bvh;

    // Now as there's more than one node, ALL leaf nodes (i.e. those containing
    // actual elements) have a well-defined parent node.
    // Another scope reduction as we again have some single-use structures.
    {
        std::vector<bool> constructed(bvh.nodes.size(), false);
        std::stack<IndexType> unprocessed;
        unprocessed.emplace(0);

        while (!unprocessed.empty())
        {
            auto element = unprocessed.top();
            unprocessed.pop();
            auto& node = bvh.nodes[element];
            if (node.elementCount)
            {
                constructed[element] = true;
            }
            else if (!constructed[element])
            {
                if (!constructed[node.left]) unprocessed.push(node.left);
                if (!constructed[node.right]) unprocessed.push(node.right);

                if (constructed[node.left] && constructed[node.right])
                {
                    node.bounding = combine(bvh.nodes[node.left].bounding,
                                            bvh.nodes[node.right].bounding);
                    constructed[element] = true;
                }
            }

            if (constructed[element] && indexToParent[element] != maxIndex)
            {
                unprocessed.emplace(indexToParent[element]);
            }
        }
    }
    return bvh;
}

/// Finds a minimal element of a BVH.
///
/// This supports finding a minimal element in any sense, so apart from being
/// useful for closest point computations, this can also be used for raycasts
/// and similar. However, this is not an exhaustive list -- there are many more
/// potential applications with an appropriate choice of value functions.
///
/// \remark This finds *a* minimal element -- if there's no unique minimal
///         element, then any element may be returned.
///
/// \param elemValue A function taking a const reference to an element (or
///                  simply by copy), and returning the value of that element.
/// \param boundingValue A function which takes a (const reference to a)
///                      bounding element and returns the minimal possible value
///                      within that. It must hold that if elem∈bounding, then
///                      boundingValue(bounding) ≤ elemValue(elem); i.e. this
///                      function must be conservative.
/// \param bvh A BVH.
/// \param startNode The node of the BVH to start the search in.
///
/// \returns A pair, consisting of a const pointer to a minimal element along
///          with the value of that element.
template <class BEValueFunction,
          class ElemValueFunction,
          class Element,
          class BoundingElement,
          typename IndexType>
auto minValueElement(const ElemValueFunction& elemValue,
                     const BEValueFunction& boundingValue,
                     const Bvh<Element, BoundingElement, IndexType>& bvh,
                     IndexType startNode = 0)
-> std::pair<const Element*, decltype(elemValue(bvh.elements[0]))>
{
    const auto& node = bvh.nodes[startNode];
    if (node.isLeaf())
    {
        const auto* bestElement = &bvh.elements[node.elementOffset];
        auto bestValue = elemValue(*bestElement);

        for (IndexType i = 1; i < node.elementCount; ++i)
        {
            const auto& currElement = bvh.elements[i+node.elementOffset];
            auto value = elemValue(currElement);
            if (value < bestValue)
            {
                bestValue = value;
                bestElement = &currElement;
            }
        }
        return {bestElement, bestValue};
    }

    auto leftBound = boundingValue(bvh.nodes[node.left].bounding);
    auto rightBound = boundingValue(bvh.nodes[node.right].bounding);

    bool left = leftBound <= rightBound;

    auto initial = minValueElement(elemValue,
                                   boundingValue,
                                   bvh,
                                   left ? node.left : node.right);
    if (left && initial.second <= rightBound) return initial;
    if (!left && initial.second <= leftBound) return initial;

    auto other = minValueElement(elemValue,
                                 boundingValue,
                                 bvh,
                                 left ? node.right : node.left);
    return initial.second < other.second ? initial : other;
}

/// Projects a point onto a set of primitives, accelerated using a BVH.
///
/// \remark This is recursive, and care should therefore be taken to limit the
///         depth of the BVH.
///
/// \param point The point to project.
/// \param bvh The BVH to use for projection.
///
/// \returns The projection of 'point' onto the set of elements.
template <typename Prec,
          class Element,
          class BoundingElement,
          typename IndexType>
Vector3<Prec> closestPoint(Vector3<Prec> point,
                           const Bvh<Element, BoundingElement, IndexType>& bvh)
{
    const auto* closestElem = minValueElement
    (
        [point](const Element& primitive)
        {
            auto closest = closestPoint(primitive, point);
            return squaredDistance(closest, point);
        },
        [point](const BoundingElement& elem)
        {
            return squaredDistance(elem, point);
        },
        bvh
    ).first;
    return closestPoint(*closestElem, point);
}

template <class Element,
          class BoundingElement,
          typename IndexType>
std::pair<Vector3<typename BoundingElement::scalar_type>,
          Vector3<typename BoundingElement::scalar_type>>
closestPoints(const Bvh<Element, BoundingElement, IndexType>& firstBVH,
              const Bvh<Element, BoundingElement, IndexType>& secondBVH,
              IndexType firstNodeIndex = 0,
              IndexType secondNodeIndex = 0)
{
    using Prec = typename BoundingElement::scalar_type;
    auto bestSqDist = std::numeric_limits<Prec>::max();
    std::pair<Vector3<Prec>, Vector3<Prec>> globalBest;

    const auto& firstNode = firstBVH.nodes[firstNodeIndex];
    const auto& secondNode = secondBVH.nodes[secondNodeIndex];

    if (firstNode.isLeaf() && secondNode.isLeaf())
    {
        for (IndexType i = 0; i < firstNode.elementCount; ++i)
        {
            for (IndexType j = 0; j < secondNode.elementCount; ++j)
            {
                auto best = closestPoints(firstBVH.elements[i+firstNode.elementOffset],
                                          secondBVH.elements[j+secondNode.elementOffset]);
                auto sqd = squaredLength(best.first-best.second);
                if (sqd < bestSqDist)
                {
                    bestSqDist = sqd;
                    globalBest = best;
                }
            }
        }
        return globalBest;
    }

    struct PairReference
    {
        IndexType first;
        IndexType second;
        Prec minSqDistance;

        bool operator<(const PairReference& other) const noexcept
        {
            return minSqDistance < other.minSqDistance;
        }
    };

    std::array<PairReference, 4> refs;
    size_t usedRefs = 2;
    if (!firstNode.isLeaf() && !secondNode.isLeaf())
    {
        refs[0] = {firstNode.left, secondNode.left, Prec(0)};
        refs[1] = {firstNode.left, secondNode.right, Prec(0)};
        refs[2] = {firstNode.right, secondNode.left, Prec(0)};
        refs[3] = {firstNode.right, secondNode.right, Prec(0)};
        usedRefs = 4;
    }
    else if (!firstNode.isLeaf())
    {
        refs[0] = {firstNode.left, secondNodeIndex, Prec(0)};
        refs[1] = {firstNode.right, secondNodeIndex, Prec(0)};
    }
    else
    {
        refs[0] = {firstNodeIndex, secondNode.left, Prec(0)};
        refs[1] = {firstNodeIndex, secondNode.right, Prec(0)};
    }

    for (size_t i = 0; i < usedRefs; ++i)
    {
        refs[i].minSqDistance = squaredDistance(firstBVH.nodes[refs[i].first].bounding,
                                                secondBVH.nodes[refs[i].second].bounding);
    }

    // Maybe a bit much to invoke this; possibly large overhead ... but that can
    // always be changed later.
    std::sort(refs.begin(), refs.begin() + usedRefs);

    for (size_t i = 0; i < usedRefs; ++i)
    {
        auto [first, second, dist] = refs[i];
        if (dist >= bestSqDist) break;

        auto best = closestPoints(firstBVH, secondBVH, first, second);
        auto sqd = squaredLength(best.first-best.second);
        if (sqd * (Prec(1) + Prec(10) * math::Constant<Prec>::epsilon)
                * (Prec(1) + Prec(10) * math::Constant<Prec>::epsilon)
            < dist)
        {
            throw std::logic_error("Bad distance computation: Found closer "
                                   "actual distance than enclosing bounding "
                                   "element.");
        }
        if (sqd < bestSqDist)
        {
            bestSqDist = sqd;
            globalBest = best;
        }
    }

    return globalBest;
}

/// \todo Document this.
/// \todo Maybe convert to stack-based traversal?
template <class OverlapProcessFunction,
          class Element,
          class BoundingElement,
          typename IndexType,
          class OverlapDetectionFunction
          = std::function<bool(BoundingElement, BoundingElement)>>
void processOverlaps(const Bvh<Element, BoundingElement, IndexType>& firstBVH,
                     const Bvh<Element, BoundingElement, IndexType>& secondBVH,
                     const OverlapProcessFunction& processOverlap,
                     const OverlapDetectionFunction& detectOverlap =
                     [](const BoundingElement& a, const BoundingElement& b)
                     {
                         return a.overlaps(b);
                     },
                     IndexType firstNodeIndex = 0,
                     IndexType secondNodeIndex = 0)
{
    const auto& firstNode = firstBVH.nodes[firstNodeIndex];
    const auto& secondNode = secondBVH.nodes[secondNodeIndex];

    if (firstNode.isLeaf() && secondNode.isLeaf())
    {
        for (IndexType i = 0; i < firstNode.elementCount; ++i)
        {
            auto iElem = i+firstNode.elementOffset;
            for (IndexType j = 0; j < secondNode.elementCount; ++j)
            {
                auto jElem = j+secondNode.elementOffset;
                processOverlap(firstBVH.elements[iElem],
                               secondBVH.elements[jElem]);
            }
        }
        return;
    }

    std::array<std::pair<IndexType, IndexType>, 4> refs;
    size_t usedRefs = 0;
    if (!firstNode.isLeaf() && !secondNode.isLeaf())
    {
        refs[usedRefs++] = {firstNode.left, secondNode.left};
        refs[usedRefs++] = {firstNode.left, secondNode.right};
        refs[usedRefs++] = {firstNode.right, secondNode.left};
        refs[usedRefs++] = {firstNode.right, secondNode.right};
    }
    else if (!firstNode.isLeaf())
    {
        refs[usedRefs++] = {firstNode.left, secondNodeIndex};
        refs[usedRefs++] = {firstNode.right, secondNodeIndex};
    }
    else // firstNode.isLeaf()
    {
        refs[usedRefs++] = {firstNodeIndex, secondNode.left};
        refs[usedRefs++] = {firstNodeIndex, secondNode.right};
    }

    for (size_t i = 0; i < usedRefs; ++i)
    {
        if (detectOverlap(firstBVH.nodes[refs[i].first].bounding,
                          secondBVH.nodes[refs[i].second].bounding))
        {
            processOverlaps(firstBVH, secondBVH,
                            processOverlap, detectOverlap,
                            refs[i].first, refs[i].second);
        }
    }
}

// Similarly to the above, we can compute the intersection of two triangle sets
// (or just a single triangle) by checking which boxes overlaps, and then choose
// the appropriate branches.

template <typename Prec>
Aabb<Prec> computeAABB(const Vector3<Prec>* points, size_t pointCount)
{
    Vector3<Prec> minCorner{std::numeric_limits<Prec>::max(),
                            std::numeric_limits<Prec>::max(),
                            std::numeric_limits<Prec>::max()};
    auto maxCorner = -minCorner;
    for (size_t i = 0; i < 3; ++i)
    {
        for (size_t vert = 0; vert < pointCount; ++vert)
        {
            minCorner[i] = std::min(minCorner[i], points[vert][i]);
            maxCorner[i] = std::max(maxCorner[i], points[vert][i]);
        }
    }

    Aabb<Prec> aabb;
    aabb.center = (maxCorner + minCorner) * Prec(.5);
    aabb.halfExtents = (maxCorner - minCorner) * Prec(.5);
    return aabb;
}

template <typename Prec>
Aabb<Prec> computeAABB(const Triangle3d<Prec>* triangles,
                       size_t triangleCount)
{
    // Triangle3d<Prec> is just an array of three Vector3<Prec>, and everything
    // is standard layout. So we just call the point-computing version:
    return computeAABB(reinterpret_cast<const Vector3<Prec>*>(triangles),
                       3*triangleCount);
}

} // namespace kraken::geometry

#endif // KRAKEN_GEOMETRY_BVH_HPP_INCLUDED
