#ifndef KRAKEN_GEOMETRY_BVHCONSTRUCTION_HPP_INCLUDED
#define KRAKEN_GEOMETRY_BVHCONSTRUCTION_HPP_INCLUDED

#include "kraken/math/constants.hpp"
#include "kraken/types.hpp"
#include <cstddef>
#include <kraken/geometry/boundingvolumes.hpp>
#include <kraken/geometry/bvh.hpp>
#include <kraken/geometry/primitives.hpp>
#include <kraken/utility/template_support.hpp>

#include <utility>

namespace kraken::geometry
{

template <typename BoundingVolume, typename It>
auto createTriangleBVH(It begin, It end)
-> Bvh<remove_cvr<decltype(*begin)>, BoundingVolume>
{
    using Prec = kraken::remove_cvr<decltype(begin->a[0])>;
    return buildBVHByMedian(begin, end,
    [](Triangle3d<Prec>* triBegin, Triangle3d<Prec>* triEnd)
    {
        auto triangleCount = static_cast<size_t>(triEnd-triBegin);
        auto aabb = bound<Aabb<Prec>>(triBegin, triangleCount);
        auto halfExtents = aabb.halfExtents;
        auto maximum = std::max_element(halfExtents.data(),
                                        halfExtents.data()+3);
        Vector3<Prec> direction{0, 0, 0};
        direction[maximum - halfExtents.data()] = Prec(1);
        std::nth_element(triBegin,
                         triBegin + (triangleCount>>1),
                         triBegin + triangleCount,
        [direction](const Triangle3d<Prec>& t1, const Triangle3d<Prec>& t2)
        {
            return dot(t1.a + t1.b + t1.c, direction)
                 < dot(t2.a + t2.b + t2.c, direction);
        });
        return triBegin + (triangleCount>>1);
    },
    [](const Triangle3d<Prec>* boundBegin, const Triangle3d<Prec>* boundEnd)
    {
        return bound<BoundingVolume>(boundBegin, boundEnd-boundBegin);
    },
    [](const BoundingVolume& a, const BoundingVolume& b)
    {
        return a | b;
    });
}

template <typename BoundingVolume, typename Prec, typename IndexPrec, typename IndexIt>
auto createTriangleBVH(const void* pVertices,
                       size_t vertexStride,
                       IndexIt indexBegin,
                       IndexIt indexEnd)
{
    using TriangleAndIndex = std::pair<Triangle3d<Prec>, IndexPrec>;

    struct TriangleIterator
    {
        const void* pVertices;
        size_t stride;
        IndexIt indices;
        IndexPrec currIndex;

        const Vector3<Prec>& getElement(size_t index) const noexcept
        {
            const auto* startAsU8 = static_cast<const U8*>(pVertices);
            const auto* loc = startAsU8 + index * stride;
            return *reinterpret_cast<const Vector3<Prec>*>(loc);
        }

        TriangleAndIndex operator*() const noexcept
        {
            auto index = indices;
            TriangleAndIndex tai;
            tai.first.a = getElement(*index++);
            tai.first.b = getElement(*index++);
            tai.first.c = getElement(*index++);
            tai.second = currIndex;
            return tai;
        }

        // Prefix
        TriangleIterator& operator++() noexcept
        {
            ++currIndex;
            ++(++(++indices));
            return *this;
        }

        // Postfix
        TriangleIterator operator++(int) noexcept
        {
            TriangleIterator returned{*this};
            ++(*this);
            return returned;
        }

        bool operator==(const TriangleIterator& other) const noexcept
        {
            return this->indices == other.indices;
        }

        bool operator!=(const TriangleIterator& other) const noexcept
        {
            return this->indices != other.indices;
        }
    };

    TriangleIterator triBegin;
    triBegin.pVertices = pVertices;
    triBegin.currIndex = 0;
    triBegin.stride = vertexStride;
    triBegin.indices = indexBegin;
    auto triEnd = triBegin;
    triEnd.indices = indexEnd;

    return buildBVHByMedian(triBegin, triEnd,
    [](TriangleAndIndex* begin, TriangleAndIndex* end)
    {
        auto triangleCount = static_cast<size_t>(end-begin);

        Vector3<Prec> minPoint{math::Constant<Prec>::maximum,
                               math::Constant<Prec>::maximum,
                               math::Constant<Prec>::maximum};
        auto maxPoint = -minPoint;
        for (auto it = begin; it != end; ++it)
        {
            minPoint = min(minPoint, it->first.a);
            maxPoint = max(maxPoint, it->first.a);
            minPoint = min(minPoint, it->first.b);
            maxPoint = max(maxPoint, it->first.b);
            minPoint = min(minPoint, it->first.c);
            maxPoint = max(maxPoint, it->first.c);
        }
        auto halfExtents = Prec(.5) * (maxPoint - minPoint);

        auto maximum = std::max_element(halfExtents.data(),
                                        halfExtents.data()+3);
        Vector3<Prec> direction{0, 0, 0};
        direction[maximum - halfExtents.data()] = Prec(1);
        std::nth_element(begin,
                         begin + (triangleCount>>1),
                         begin + triangleCount,
        [direction](TriangleAndIndex& t1, TriangleAndIndex& t2)
        {
            return dot(t1.first.a + t1.first.b + t1.first.c, direction)
                 < dot(t2.first.a + t2.first.b + t2.first.c, direction);
        });
        return begin + (triangleCount>>1);
    },
    [](const TriangleAndIndex* begin, const TriangleAndIndex* end)
    {
        Vector3<Prec> minPoint{math::Constant<Prec>::maximum,
                               math::Constant<Prec>::maximum,
                               math::Constant<Prec>::maximum};
        auto maxPoint = -minPoint;
        for (auto it = begin; it != end; ++it)
        {
            minPoint = min(minPoint, it->first.a);
            maxPoint = max(maxPoint, it->first.a);
            minPoint = min(minPoint, it->first.b);
            maxPoint = max(maxPoint, it->first.b);
            minPoint = min(minPoint, it->first.c);
            maxPoint = max(maxPoint, it->first.c);
        }
        Aabb<Prec> box;
        box.center = Prec(.5) * (maxPoint + minPoint);
        box.halfExtents = Prec(.5) * (maxPoint - minPoint);
        return box;
    },
    [](const BoundingVolume& a, const BoundingVolume& b)
    {
        return a | b;
    });
}


template <typename BoundingVolume, typename IndexType, typename TriangleIt>
auto createIdentifiableTriangleBVH(TriangleIt begin, TriangleIt end)
{
    using Prec = kraken::remove_cvr<decltype(begin->a[0])>;
    using TriangleAndIndex = std::pair<Triangle3d<Prec>, IndexType>;

    struct TriangleIterator
    {
        TriangleIt iterator;
        IndexType index;

        TriangleAndIndex operator*() const noexcept
        {
            return {*iterator, index};
        }

        // Prefix
        TriangleIterator& operator++() noexcept
        {
            ++iterator;
            ++index;
            return *this;
        }

        // Postfix
        TriangleIterator operator++(int) noexcept
        {
            TriangleIterator returned{*this};
            ++(*this);
            return returned;
        }

        bool operator==(const TriangleIterator& other) const noexcept
        {
            return this->iterator == other.iterator;
        }

        bool operator!=(const TriangleIterator& other) const noexcept
        {
            return this->iterator != other.iterator;
        }
    };

    TriangleIterator triBegin, triEnd;
    triBegin.index = 0;
    triBegin.iterator = begin;
    triEnd.iterator = end;

    return buildBVHByMedian(triBegin, triEnd,
    [](TriangleAndIndex* taiBegin, TriangleAndIndex* taiEnd)
    {
        auto triangleCount = static_cast<size_t>(taiEnd-taiBegin);

        Vector3<Prec> minPoint{math::Constant<Prec>::maximum,
                               math::Constant<Prec>::maximum,
                               math::Constant<Prec>::maximum};
        auto maxPoint = -minPoint;
        for (auto it = taiBegin; it != taiEnd; ++it)
        {
            minPoint = min(minPoint, it->first.a);
            maxPoint = max(maxPoint, it->first.a);
            minPoint = min(minPoint, it->first.b);
            maxPoint = max(maxPoint, it->first.b);
            minPoint = min(minPoint, it->first.c);
            maxPoint = max(maxPoint, it->first.c);
        }
        auto halfExtents = Prec(.5) * (maxPoint - minPoint);

        auto maximum = std::max_element(halfExtents.data(),
                                        halfExtents.data()+3);
        Vector3<Prec> direction{0, 0, 0};
        direction[maximum - halfExtents.data()] = Prec(1);
        std::nth_element(taiBegin,
                         taiBegin + (triangleCount>>1),
                         taiBegin + triangleCount,
        [direction](TriangleAndIndex& t1, TriangleAndIndex& t2)
        {
            return dot(t1.first.a + t1.first.b + t1.first.c, direction)
                 < dot(t2.first.a + t2.first.b + t2.first.c, direction);
        });
        return taiBegin + (triangleCount>>1);
    },
    [](const TriangleAndIndex* taiBegin, const TriangleAndIndex* taiEnd)
    {
        Vector3<Prec> minPoint{math::Constant<Prec>::maximum,
                               math::Constant<Prec>::maximum,
                               math::Constant<Prec>::maximum};
        auto maxPoint = -minPoint;
        for (auto it = taiBegin; it != taiEnd; ++it)
        {
            minPoint = min(minPoint, it->first.a);
            maxPoint = max(maxPoint, it->first.a);
            minPoint = min(minPoint, it->first.b);
            maxPoint = max(maxPoint, it->first.b);
            minPoint = min(minPoint, it->first.c);
            maxPoint = max(maxPoint, it->first.c);
        }
        Aabb<Prec> box;
        box.center = Prec(.5) * (maxPoint + minPoint);
        box.halfExtents = Prec(.5) * (maxPoint - minPoint);
        return box;
    },
    [](const BoundingVolume& a, const BoundingVolume& b)
    {
        return a | b;
    });
}

} // namespace kraken::geometry

#endif // KRAKEN_GEOMETRY_BVHCONSTRUCTION_HPP_INCLUDED
