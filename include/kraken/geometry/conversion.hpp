#ifndef KRAKEN_GEOMETRY_CONVERSION_HPP_INCLUDED
#define KRAKEN_GEOMETRY_CONVERSION_HPP_INCLUDED

#include "kraken/geometry/vertex.hpp"
#include <cstddef>
#include <functional>
#include <kraken/geometry/mesh.hpp>

namespace kraken
{

namespace geometry
{

void toFlatRenderMesh(const Mesh& mesh,
                      const std::function<void*(size_t)>& vertexOutput,
                      VertexType& vertexTypeOut);

} // namespace geometry

} // namespace kraken

#endif // KRAKEN_GEOMETRY_CONVERSION_HPP_INCLUDED
