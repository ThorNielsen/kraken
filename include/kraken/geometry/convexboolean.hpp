#ifndef KRAKEN_GEOMETRY_CONVEXBOOLEAN_HPP_INCLUDED
#define KRAKEN_GEOMETRY_CONVEXBOOLEAN_HPP_INCLUDED

#include "kraken/geometry/primitives.hpp"
#include "kraken/math/constants.hpp"
#include <cstddef>
#include <kraken/geometry/intersection.hpp>

namespace kraken::geometry
{

/// Constructs a line with unnormalised direction vector given a start index.
///
/// \param pVertices Pointer to the vertices.
/// \param vertexCount Number of vertices.
/// \param startVertex Vertex index to start line at.
///
/// \returns The line with origin pVertices[startVertex], and direction vector
///          going to pVertices[(startVertex+1) % vertexCount].
template <typename Prec>
Line2d<Prec> buildLineFromStartWrap(const Vector2<Prec>* pVertices,
                                    size_t vertexCount,
                                    size_t startVertex)
{
    auto next = (startVertex+1) % vertexCount;
    return Line2d<Prec>{pVertices[startVertex],
                        pVertices[next]-pVertices[startVertex]};
}

/// Constructs a line with unnormalised direction vector given an end index.
///
/// \param pVertices Pointer to the vertices.
/// \param vertexCount Number of vertices.
/// \param endVertex Vertex index to end line at.
///
/// \returns The line with origin pVertices[(endVertex-1)%vertexCount], and
///          direction vector going to pVertices[endVertex].
template <typename Prec>
Line2d<Prec> buildLineFromEndWrap(const Vector2<Prec>* pVertices,
                                  size_t vertexCount,
                                  size_t endVertex)
{
    auto start = (endVertex + (vertexCount - 1)) % vertexCount;
    return Line2d<Prec>{pVertices[start],
                        pVertices[endVertex]-pVertices[start]};
}

/// Performs a boolean operation on two convex 2d polygons.
///
/// \todo Finalise interface.
///
/// This can perform one of two boolean operations; either intersection or
/// union. The main reason for not supporting difference is that that does not
/// necessarily result in a connected shape even if the shapes are connected to
/// begin with.
///
/// \remark Note that the algorithm is currently of suboptimal complexity, which
///         is a fancy way of saying "this is slow on large cases".
///
/// \remark Note that while convex polygons are stable under intersections, they
///         are NOT stable under unions; and thus the output of any union
///         operation does not a priori result in a convex shape.
///
/// \param pFirstPolygon Pointer to the points of the first polygon. These must
///                      be ordered either clockwisely or counterclockwisely.
/// \param firstPolygonSize Number of vertices in the first polygon.
/// \param pSecondPolygon Pointer to the points of the second polygon.
/// \param secondPolygonSize Number of vertices in the second polygon.
/// \param intersection If true, perform an intersection of the two polygons,
///                     otherwise perform a union.
/// \param polygonOut Output iterator to write the vertices of the resulting
///                   polygon. They will be ordered with the same winding as the
///                   first polygon, and the maximal number of elements that can
///                   be written by this is firstPolygonSize*secondPolygonSize.
/// \param maxVertices Maximal amount of vertices that can be written to
///                    polygonOut.
/// \param bufferTooSmallOut If not null, then this will write to the pointed-to
///                          value whether the buffer was too small (and more
///                          vertices were attempted output).
/// \param epsilon Threshold to use for comparisons (equality, etc.).
///
/// \returns An iterator to the end (i.e. one-past-the-last) of where the points
///          were written.
template <typename Prec, typename OutIt>
OutIt performBooleanOperation(const Vector2<Prec>* pFirstPolygon,
                              size_t firstPolygonSize,
                              const Vector2<Prec>* pSecondPolygon,
                              size_t secondPolygonSize,
                              bool intersection,
                              OutIt polygonOut,
                              size_t maxVertices = ~(size_t)0,
                              bool* bufferTooSmallOut = nullptr,
                              Prec epsilon = 20*math::Constant<Prec>::epsilon)
{
    bool dummy;

    bool& bufferOverflow = bufferTooSmallOut ? *bufferTooSmallOut : dummy;

    bufferOverflow = false;

    auto sqEps = epsilon * epsilon;

    size_t firstStart = 0;
    size_t secondStart = 0;
    Prec parFirst, parSecond;

    bool hasLineIntersections = nextIntersection(pFirstPolygon,
                                                 firstPolygonSize,
                                                 pSecondPolygon,
                                                 secondPolygonSize,
                                                 0,
                                                 0,
                                                 true,
                                                 true,
                                                 &parFirst,
                                                 &parSecond,
                                                 &firstStart,
                                                 &secondStart,
                                                 epsilon);

    if (!hasLineIntersections)
    {
        bool firstInsideSecond = false;
        bool secondInsideFirst = false;

        // Here, we *may* think that neither is inside the other, and as they do
        // not intersect, then we conclude that they are separate.
        // However, it might be that the points we chose before *just* exactly
        // lies on the edge of the other polygon (or indeed, that they are even
        // the same polygon), and so we need a more robust check. That is done
        // by midpoints -- if one midpoint is inside the other, then by
        // convexity one has to lie inside the other, otherwise there's no
        // problem.
        auto firstMid = polygonCentre(pFirstPolygon,
                                      pFirstPolygon+firstPolygonSize);
        auto secondMid = polygonCentre(pSecondPolygon,
                                       pSecondPolygon+secondPolygonSize);

        firstInsideSecond = isInsideConvexPolygon(firstMid,
                                                  pSecondPolygon,
                                                  pSecondPolygon+secondPolygonSize);
        secondInsideFirst = isInsideConvexPolygon(secondMid,
                                                  pFirstPolygon,
                                                  pFirstPolygon+firstPolygonSize);

        if (!firstInsideSecond && !secondInsideFirst)
        {
            // No intersection, and we do not (yet) support unions of
            // disconnected polygons.
            return polygonOut;
        }

        if (firstInsideSecond && secondInsideFirst)
        {
            // In this case, one polygon is inside the other. We determine which
            // case by simply computing areas and comparing those.
            auto firstArea = computeGaussArea(pFirstPolygon,
                                              pFirstPolygon+firstPolygonSize);
            auto secondArea = computeGaussArea(pSecondPolygon,
                                               pSecondPolygon+secondPolygonSize);
            firstInsideSecond = false;
            secondInsideFirst = false;

            if (firstArea <= secondArea) firstInsideSecond = true;
            else secondInsideFirst = true;
        }

        // We copy the first polygon if exactly one of these statements is true:
        // * The first is inside the second.
        // * We are performing a union and not intersection operation.
        bool const copyFirst = firstInsideSecond ^ !intersection;

        const auto* copySrc = copyFirst ? pFirstPolygon : pSecondPolygon;
        auto copyCount = copyFirst ? firstPolygonSize : secondPolygonSize;
        if (copyCount > maxVertices)
        {
            copyCount = maxVertices;
            bufferOverflow = true;
        }

        return std::copy(copySrc, copySrc + copyCount, polygonOut);
    }

#ifdef KRAKEN_INSERT_BUFFER_LIMIT_CHECK
#error "Internal macro name KRAKEN_INSERT_BUFFER_LIMIT_CHECK already defined."
#endif // KRAKEN_INSERT_BUFFER_LIMIT_CHECK
#define KRAKEN_INSERT_BUFFER_LIMIT_CHECK\
    if (!maxVertices--)\
    {\
        bufferOverflow = true;\
        return polygonOut;\
    }

    auto initialFirst = firstStart;
    auto initialSecond = secondStart;

    auto firstLine = buildLineFromEndWrap(pFirstPolygon,
                                          firstPolygonSize,
                                          firstStart);
    auto secondLine = buildLineFromEndWrap(pSecondPolygon,
                                           secondPolygonSize,
                                           secondStart);

    auto lastVertex = firstLine.origin + parFirst * firstLine.direction;
    auto firstVertex = lastVertex;

    KRAKEN_INSERT_BUFFER_LIMIT_CHECK

    *polygonOut++ = lastVertex;


    bool isFirst = dot(perp(firstLine.direction), secondLine.direction)
                   >= Prec(0);
    isFirst = isFirst ^ intersection;

    size_t nextFirst, nextSecond;

    while (nextIntersection(pFirstPolygon,
                            firstPolygonSize,
                            pSecondPolygon,
                            secondPolygonSize,
                            firstStart,
                            secondStart,
                            false,
                            isFirst ^! intersection,
                            &parFirst,
                            &parSecond,
                            &nextFirst,
                            &nextSecond,
                            epsilon))
    {
        // WARNING: If input polygons are non-convex, this condition may cause
        // the algorithm to become stuck in a loop -- since if it finds an
        // intersection loop not including the very first intersection, it will
        // just continue to circle around that.
        if (nextFirst == initialFirst && nextSecond == initialSecond)
        {
            // We are done.
            break;
        }
        firstLine = buildLineFromEndWrap(pFirstPolygon,
                                         firstPolygonSize,
                                         nextFirst);
        auto intersectionVertex = firstLine.origin
                                  + parFirst * firstLine.direction;

        auto totalCount = isFirst ? firstPolygonSize : secondPolygonSize;
        const auto* src = isFirst ? pFirstPolygon : pSecondPolygon;
        auto beginOffset = isFirst ? firstStart : secondStart;
        auto endOffset = (isFirst ? nextFirst : nextSecond);

        if (squaredLength(lastVertex-src[beginOffset]) < sqEps)
        {
            ++beginOffset;
        }
        // Handle specific case: *if* the very line segment we start at for one
        // of the polygons is intersected twice, and we thus need to copy the
        // entire polygon.
        if (beginOffset == endOffset && !intersection)
        {
            for (size_t i = 0; i < totalCount; ++i)
            {
                KRAKEN_INSERT_BUFFER_LIMIT_CHECK
                *polygonOut++ = lastVertex
                              = src[(beginOffset+i) % totalCount];
            }
        }
        else
        {
            for (size_t i = beginOffset; i != endOffset; i = (i+1)%totalCount)
            {
                KRAKEN_INSERT_BUFFER_LIMIT_CHECK
                *polygonOut++ = lastVertex = src[i];
            }
        }
        if (squaredLength(lastVertex-intersectionVertex) >= sqEps)
        {
            KRAKEN_INSERT_BUFFER_LIMIT_CHECK
            *polygonOut++ = lastVertex = intersectionVertex;
        }

        firstLine = buildLineFromEndWrap(pFirstPolygon,
                                         firstPolygonSize,
                                         firstStart);
        secondLine = buildLineFromEndWrap(pSecondPolygon,
                                          secondPolygonSize,
                                          secondStart);
        firstStart = nextFirst;
        secondStart = nextSecond;
        isFirst = dot(perp(firstLine.direction), secondLine.direction)
                  < Prec(0);
        isFirst = isFirst ^ intersection;
    }

    const auto* src = isFirst ? pFirstPolygon : pSecondPolygon;
    auto beginOffset = isFirst ? firstStart : secondStart;
    auto endOffset = isFirst ? initialFirst : initialSecond;
    auto lim = isFirst ? firstPolygonSize : secondPolygonSize;
    bool ignoreCondition = beginOffset == endOffset && !intersection;
    for (size_t i = beginOffset;
         i != endOffset || ignoreCondition;
         i = (i+1) % lim)
    {
        ignoreCondition = false;
        if (squaredLength(src[i]-firstVertex) < sqEps
            || squaredLength(src[i]-lastVertex) < sqEps)
        {
            continue;
        }
        KRAKEN_INSERT_BUFFER_LIMIT_CHECK
        *polygonOut++ = src[i];
    }

#undef KRAKEN_INSERT_BUFFER_LIMIT_CHECK

    return polygonOut;
}

} // namespace kraken::geometry

#endif // KRAKEN_GEOMETRY_CONVEXBOOLEAN_HPP_INCLUDED
