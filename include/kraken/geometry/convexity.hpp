#ifndef KRAKEN_GEOMETRY_CONVEXITY_HPP_INCLUDED
#define KRAKEN_GEOMETRY_CONVEXITY_HPP_INCLUDED

#include "kraken/geometry/mesh.hpp"
#include "kraken/math/matrix.hpp"
#include <cstddef>
#include <kraken/geometry/quickhull.hpp>

#include <vector>



namespace kraken::geometry
{

size_t extremalPoint(math::vec3 direction, const std::vector<math::vec3>& vertices);

inline Mesh convexHull(const math::vec3* vertices,
                       size_t vertexCount,
                       size_t vertexStride = sizeof(math::vec3))
{
    return quickhull(vertices, vertexCount, vertexStride);
}

VertexIndex convexExtremalPoint(math::vec3 direction, const Mesh& mesh,
                                FaceIndex startSearch = invalidFace);

/// \brief Finds the feature of a given mesh closest to 'point', which is
/// allowed to be inside the mesh.
///
/// \param point Any point in space.
/// \param mesh A convex mesh.
/// \param innerPoint Any point inside the mesh which is NOT 'point'. It should
/// be far from 'point' while still being clearly inside 'mesh'.
/// \param startSearch (Optionally) the feature from which the search should be
/// started in order to exploit coherency between several queries.
/// \return Feature of 'mesh' closest to 'point'.
Feature convexClosestFeature(
    math::vec3 point,
    const Mesh &mesh,
    math::vec3 innerPoint,
    Feature startSearch = {invalidVertex, invalidEdge, invalidFace, FeatureType::None});

} // namespace kraken::geometry



#endif // KRAKEN_GEOMETRY_CONVEXITY_HPP_INCLUDED
