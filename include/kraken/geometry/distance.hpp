#ifndef KRAKEN_GEOMETRY_DISTANCE_HPP_INCLUDED
#define KRAKEN_GEOMETRY_DISTANCE_HPP_INCLUDED

#include <kraken/geometry/boundingvolumes.hpp>
#include <kraken/geometry/primitives.hpp>

#include <cmath>

namespace kraken::geometry
{

/// Finds the squared distance between two points in ℝ³.
///
/// \param a A point.
/// \param b Another point.
///
/// \returns The squared distance.
template <typename Prec>
Prec squaredDistance(Vector3<Prec> a, Vector3<Prec> b) noexcept
{
    auto ab = a-b;
    return dot(ab, ab);
}

/// Finds the squared distance from a point to an axis-aligned bounding box.
///
/// \remark This has no requirements other than the point being in ℝ³ (and thus
///         not being nan, infinite or ugly stuff like that), and the bounding
///         box being valid.
///
/// \remark This treats the box as solid, so points inside are considered to
///         have zero distance to the box.
///
/// \param aabb An axis-aligned bounding box.
/// \param point A point in ℝ³.
///
/// \returns The minimal distance from the point to the box.
template <typename Prec>
Prec squaredDistance(Aabb<Prec> aabb, Vector3<Prec> point) noexcept
{
    return squaredDistance(closestPoint(aabb, point), point);
}

/// Finds the squared distance from a point to a sphere.
///
/// \remark This treats the sphere as solid, so points inside are considered to
///         have zero distance to the sphere.
///
/// \param sphere A sphere.
/// \param point A point in ℝ³.
///
/// \returns The minimal distance from the point to the sphere.
template <typename Prec>
Prec squaredDistance(Sphere<Prec> sphere, Vector3<Prec> point) noexcept
{
    auto l = squaredLength(point - sphere.origin);
    if (l <= sphere.radius * sphere.radius) return 0.;
    l = std::sqrt(l) - sphere.radius;
    return l*l;
}

/// Finds the squared distance from one axis-aligned bounding box to another.
///
/// \param aabb An axis-aligned bounding box.
/// \param aabb2 Another axis-aligned bounding box.
///
/// \returns The distance between the two boxes.
template <typename Prec>
Prec squaredDistance(Aabb<Prec> aabb, Aabb<Prec> aabb2) noexcept
{
    auto centerDists = math::abs(aabb.center - aabb2.center);
    auto d = centerDists - aabb.halfExtents - aabb2.halfExtents;
    if (d.x <= Prec(0) || d.y <= Prec(0) || d.z <= Prec(0)) return Prec(0);
    return squaredLength(d);
}

/// Computes min_t ||line.origin+t·line.direction - point||².
///
/// \param line An infinite line.
/// \param point A point in ℝ³.
///
/// \returns The square of the minimum distance between the line and the point.
template <typename Prec>
Prec squaredDistance(Line3d<Prec> line, Vector3<Prec> point) noexcept
{
    return squaredLength(cross(point-line.origin, line.direction));
}

/// Computes min_t ||line.origin+t·line.direction - point||².
///
/// \remark This allows the direction to be unnormalised; however, it is NOT
///         allowed to be zero.
///
/// \param line An infinite line; with the direction allowed to be unnormalised.
/// \param point A point in ℝ³.
///
/// \returns The square of the minimum distance between the line and the point.
template <typename Prec>
Prec squaredDistanceUnnormalised(Line3d<Prec> line, Vector3<Prec> point) noexcept
{
    return squaredLength(cross(point-line.origin, line.direction))
           / squaredLength(line.direction);
}

/// Computes min_t ||line.origin+t·line.direction - point||.
///
/// \param line An infinite line.
/// \param point A point in ℝ³.
///
/// \returns The minimum distance between the line and the point.
template <typename Prec>
Prec distance(Line3d<Prec> line, Vector3<Prec> point) noexcept
{
    return std::sqrt(squaredDistance(line, point));
}

/// Computes the signed distance from a plane to a point.
///
/// The sign of the distance determines if the point is above the plane (in that
/// case, positive), or below (negative). The direction of the plane is
/// considered to be outwards along the normal, so the 'above' side is that
/// which the normal sticks out of.
///
/// \param plane An infinite plane.
/// \param point A point in ℝ³.
///
/// \returns The signed distance between the plane and point.
template <typename Prec>
Prec signedDistance(Plane<Prec> plane, Vector3<Prec> point) noexcept
{
    return dot(plane.normal, point - plane.origin);
}

/// Computes the distance from a plane to a point.
///
/// \param plane An infinite plane.
/// \param point A point in ℝ³.
///
/// \returns The distance from the plane to the point.
template <typename Prec>
Prec distance(Plane<Prec> plane, Vector3<Prec> point) noexcept
{
    return std::abs(signedDistance(plane, point));
}

/// Computes the distance between two triangles.
///
/// \param a A triangle.
/// \param b Another triangle.
///
/// \returns The distance between the two triangles.
template <typename Prec>
Prec distance(Triangle3d<Prec> a, Triangle3d<Prec> b) noexcept;

/// Finds the square of the distance between two 3d triangles.
///
/// \param a A triangle.
/// \param b Another triangle.
///
/// \returns The square of the shortest distance between two 3d triangles, or
///          zero if they intersect.
template <typename Prec>
Prec squaredDistance(Triangle3d<Prec> a, Triangle3d<Prec> b) noexcept;

/// Finds the distance between two 3d triangles.
///
/// \param a A triangle.
/// \param b Another triangle.
///
/// \returns The shortest distance between two 3d triangles, or zero if they
///          intersect.
template <typename Prec>
Prec distance(Triangle3d<Prec> a, Triangle3d<Prec> b) noexcept
{
    return std::sqrt(squaredDistance(a, b));
}

} // namespace kraken::geometry

#endif // KRAKEN_GEOMETRY_DISTANCE_HPP_INCLUDED
