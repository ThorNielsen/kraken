#ifndef KRAKEN_GEOMETRY_IMPORT_HPP_INCLUDED
#define KRAKEN_GEOMETRY_IMPORT_HPP_INCLUDED

#include <kraken/geometry/mesh.hpp>

#include <istream>
#include <optional>

namespace kraken::geometry
{

bool importPLY(std::istream& input, Mesh& meshOut);

std::optional<Mesh> importPLY(std::istream& input);

} // namespace kraken::geometry

#endif // KRAKEN_GEOMETRY_IMPORT_HPP_INCLUDED

