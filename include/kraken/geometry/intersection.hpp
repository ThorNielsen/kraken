#ifndef KRAKEN_GEOMETRY_INTERSECTION_HPP_INCLUDED
#define KRAKEN_GEOMETRY_INTERSECTION_HPP_INCLUDED

#include "kraken/math/constants.hpp"
#include "kraken/math/matrix.hpp"
#include <cstddef>
#include <kraken/geometry/primitivequeries.hpp>
#include <kraken/geometry/primitives.hpp>

#include <array>
#include <utility>

namespace kraken::geometry
{

/// Compute the intersection parameters between two lines in ℝ².
///
/// \remark This does not require the direction of the lines to be of unit
///         length -- the only restriction is that the line directions have
///         nonzero length, and thus do, in fact, represent a direction.
///
/// \param l1 A line in ℝ².
/// \param l2 A line in ℝ².
/// \param epsilon Epsilon to use for parallelity determination.
///
/// \returns A pair of parameters corresponding to the intersection point of the
///          lines, or (max, max) if the lines are parallel.
template <typename Prec>
std::pair<Prec, Prec> intersect(Line2d<Prec> l1,
                                Line2d<Prec> l2,
                                Prec epsilon = 20*math::Constant<Prec>::epsilon) noexcept
{
    Vector2<Prec> pars{math::Constant<Prec>::maximum,
                       math::Constant<Prec>::maximum};
    math::Matrix<2, 2, Prec> mat;
    mat.setCol(0, l1.direction);
    mat.setCol(1, -l2.direction);
    if (std::abs(det(mat)) < epsilon) return {pars.x, pars.y};
    pars = inverse(mat)*(l2.origin - l1.origin);
    return {pars.x, pars.y};
}

/// Compute the intersection point between two lines in ℝ².
///
/// \remark This does not require the direction of the lines to be of unit
///         length -- the only restriction is that the line directions have
///         nonzero length, and thus do, in fact, represent a direction.
///
/// \param l1 A line in ℝ².
/// \param l2 A line in ℝ².
/// \param pointOut Variable to write the resulting intersection point to; note
///                 that this will ONLY be written to *if* there is an
///                 intersection.
/// \param epsilon Epsilon to use for parallelity determination.
///
/// \returns True if there is an intersection, otherwise false (meaning the
///          lines are considered parallel).
template <typename Prec>
bool intersect(Line2d<Prec> l1,
               Line2d<Prec> l2,
               Vector2<Prec>& pointOut,
               Prec epsilon = 20*math::Constant<Prec>::epsilon) noexcept
{
    auto [p1, p2] = intersect(l1, l2, epsilon);
    if (p1 >= math::Constant<Prec>::maximum) return false;
    pointOut = l1.origin + p1 * l1.direction;
    return true;
}

/// Compute the intersection between a plane and a line.
///
/// \remark Neither the plane's normal nor the line's direction needs to be
///         normalised for this function to work correctly -- they are
///         explicitly allowed to be unnormalised. However, they must not be
///         zero.
///
/// \param plane The plane to use.
/// \param line The line to use.
/// \param resultOut Variable to write the resulting intersection to. Note that
///                  this is ONLY written to if the function returns true;
///                  otherwise it will be untouched.
/// \param epsilon Parallelity threshold for the plane and line.
///
/// \returns True if an intersection was found, false otherwise (i.e. if the
///          plane and line are parallel).
template <typename Prec>
bool intersectionParameterOnLine(const Plane<Prec>& plane,
                                 const Line3d<Prec>& line,
                                 Vector3<Prec>& resultOut,
                                 Prec epsilon = 50*math::Constant<Prec>::epsilon) noexcept
{
    auto rn = dot(plane.normal, line.direction);
    if (std::abs(rn) < epsilon) return false;
    auto par = dot(line.origin-plane.origin, plane.normal) / rn;
    resultOut = plane.origin + par * plane.normal;
    return true;
}

/// Compute the intersection between a plane and a line.
///
/// \remark Neither the plane's normal nor the line's direction needs to be
///         normalised for this function to work correctly -- they are
///         explicitly allowed to be unnormalised. However, they must not be
///         zero.
///
/// \param plane The plane to use.
/// \param line The line to use.
/// \param resultOut Variable to write the resulting intersection to. Note that
///                  this is ONLY written to if the function returns true;
///                  otherwise it will be untouched.
/// \param epsilon Parallelity threshold for the plane and line.
///
/// \returns True if an intersection was found, false otherwise (i.e. if the
///          plane and line are parallel).
template <typename Prec>
bool intersect(const Plane<Prec>& plane,
               const Line3d<Prec>& line,
               Vector3<Prec>& resultOut,
               Prec epsilon = 50*math::Constant<Prec>::epsilon) noexcept
{
    auto rn = dot(plane.normal, line.direction);
    if (std::abs(rn) < epsilon) return false;
    auto par = dot(line.origin-plane.origin, plane.normal) / rn;
    resultOut = plane.origin + par * plane.normal;
    return true;
}

/// Trims a line segment by a plane.
///
/// This takes a line segment, and modifies its start and end parameters such
/// that the resulting segment will be equal to the part of the original one on
/// the positive side of the plane. To see exactly what this means, consider the
/// following diagram:
///            / ->
///           /
/// a--------c------b
///         /
///        /
/// Here, the slashes are the plane, the arrow indicates the plane's normal's
/// direction, and the line from a to b is the input segment, i.e. a corresponds
/// to the segment's begin, and b to the segment's end. In the above case, the
/// segment's begin will be changed to c instead.
/// In the case where no part of the line segment lies on the positive part of
/// the plane, this will return false.
///
/// \param segment A line segment, it is allowed to be unnormalised.
/// \param plane A plane whose direction determines what to trim away.
/// \param epsilon Epsilon for parallelity.
template <typename Prec>
void trimSegment(LineSegment3d<Prec>& segment,
                 Plane<Prec> plane,
                 Prec epsilon = 50*math::Constant<Prec>::epsilon) noexcept
{
    auto rn = dot(plane.normal, segment.direction);
    if (std::abs(rn) < epsilon)
    {
        return; // So parallel that we can just as well not not trim.
    }
    auto par = dot(plane.origin-segment.origin, plane.normal) / rn;
    if (rn < Prec(0))
    {
        // We are modifying the end point.
        segment.maxPar = std::min(segment.maxPar, par);
    }
    else
    {
        segment.minPar = std::max(segment.minPar, par);
    }
}

/// Compute the intersection between a triangle and a line.
///
/// \remark The line's direction does not need to be normalised for this
///         function to work correctly -- it is explicitly allowed to be
///         unnormalised. However, it must not be zero.
///
/// \param triangle The triangle to use.
/// \param line The line to use.
/// \param resultOut Variable to write the resulting intersection to. Note that
///                  this is ONLY written to if the function returns true;
///                  otherwise it will be untouched.
/// \param epsilon Parallelity threshold for the triangle and line.
///
/// \returns True if an intersection was found, false otherwise (i.e. if the
///          plane and line are parallel).
template <typename Prec>
bool intersect(const Triangle3d<Prec>& triangle,
               const Line3d<Prec>& line,
               Vector3<Prec>& resultOut,
               Prec epsilon = 50*math::Constant<Prec>::epsilon) noexcept
{
    Plane<Prec> triPlane;
    triPlane.origin = triangle.a;
    triPlane.normal = cross(triangle.b - triangle.a, triangle.c - triangle.a);

    Vector3<Prec> result;
    if (!intersect(triPlane, line, result, epsilon)) return false;

    const auto* triBegin = reinterpret_cast<const Vector3<Prec>*>(&triangle);
    if (isInsideConvexPolygon(result, triPlane.normal, triBegin, triBegin+3))
    {
        resultOut = result;
        return true;
    }
    return false;
}

/// Computes the amount one needs to sweep one triangle to reach the other.
///
/// The sweep is assumed to be along 'direction' that neither needs to be
/// parallel to the plane defined by 'toSweep' nor the plane defined by 'other'.
///
/// \param toSweep A triangle to sweep in a direction.
/// \param other The other triangle to compute the intersection time to.
/// \param direction A direction (which MUST be normalised) to sweep 'toSweep'
///                  along.
/// \param firstResultOut Destination to write the first intersection along this
///                       direction to.
/// \param lastResultOut The variable to write the last intersection time to.
/// \param epsilon Parallelity/degeneracy threshold to use for such computations
///                (e.g. for determining if the sweep direction is parallel to
///                the plane defined by 'other').
/// \returns Whether 'toSweep' swept along 'direction' intersects 'other' at any
///          point; in the affirmative case the two parameters are written to
///          firstResultOut and lastResultOut. The value of the parameters is
///          such that t∈[firstResultOut, lastResultOut] if and only if
///          {x+t*direction | x∈toSweep} has a non-empty intersection with
///          'other'.
template <typename Prec>
bool intersectSweptTriangle(const Triangle3d<Prec>& toSweep,
                            const Triangle3d<Prec>& other,
                            Vector3<Prec> direction,
                            Prec& firstResultOut,
                            Prec& lastResultOut,
                            Prec epsilon = 50*math::Constant<Prec>::epsilon)
{
    firstResultOut = math::Constant<Prec>::maximum;
    lastResultOut = -math::Constant<Prec>::maximum;
    // Centre is included to avoid degenerate situation where the two triangles
    // match *exactly*.
    auto sweepCentre = (toSweep.a + toSweep.b + toSweep.c) * (Prec(1) / Prec(3));
    // Case 1: See if vertices from sweep intersects other triangle.
    for (const auto* point : {&toSweep.a, &toSweep.b, &toSweep.c, &sweepCentre})
    {
        Vector3<Prec> intersectResult;
        if (intersect(other, {*point, direction}, intersectResult, epsilon))
        {
            auto par = dot(intersectResult-*point, direction);
            firstResultOut = std::min(firstResultOut, par);
            lastResultOut = std::max(lastResultOut, par);
        }
    }
    // Case 2: See if vertices from other intersects sweep (we do not need the
    //         centre since that only applies if the vertices all line up, and
    //         so is already covered above.
    for (const auto* point : {&other.a, &other.b, &other.c})
    {
        Vector3<Prec> intersectResult;
        if (intersect(other, {*point, direction}, intersectResult, epsilon))
        {
            // (result-point)·direction gives how much we need to move 'other'
            // to get to 'toSweep'; the translation is simply to negate this
            // which can be achieved by just reversing the subtraction.
            auto par = dot(*point-intersectResult, direction);
            firstResultOut = std::min(firstResultOut, par);
            lastResultOut = std::max(lastResultOut, par);
        }
    }
    // Case 3: See if sweeping a line segment hits another one first. That is
    //         equivalent to just taking the plane defined by the segment from
    //         'toSweep' and the direction, and intersecting that with all
    //         segments from 'other'.
    Vector3<Prec> outerPrev = toSweep[2];
    for (size_t i = 0; i < 3; ++i)
    {
        auto outerCurr = toSweep[i];
        auto dir1 = outerCurr - outerPrev;
        auto d1len = length(dir1);
        if (d1len < epsilon) continue;
        dir1 *= Prec(1) / d1len;

        Vector3<Prec> innerPrev = other[2];
        for (size_t j = 0; j < 3; ++j)
        {
            auto innerCurr = other[j];
            auto dir2 = innerCurr - innerPrev;
            auto d2len = length(dir2);
            if (d2len >= epsilon)
            {
                dir2 *= Prec(1)/d2len;

                math::Matrix<3, 3, Prec> solver(dir1, direction, dir2);
                auto d = det(solver);
                if (std::abs(d) > epsilon)
                {
                    auto pars = inverse(solver) * (innerPrev - outerPrev);
                    // The pars are:
                    // [0] - parameter on segment from 'toSweep'
                    // [1] - parameter on 'direction'
                    // [2] - minus parameter on line
                    if (Prec(-1) <= pars[2] && pars[2] <= Prec(0)
                        && Prec(0) <= pars[0] && pars[0] <= Prec(1))
                    {
                        firstResultOut = std::min(firstResultOut, pars[1]);
                        lastResultOut = std::max(lastResultOut, pars[1]);
                    }
                }
                else
                {
                    // The degenerate case where dir1 and dir2 are parallel is
                    // implicitly handled by the vertices, but if dir1 and dir2
                    // are not parallel, then 'direction' is parallel to one of
                    // them; in which case we need only check if dir1 and dir2
                    // intersect in some way; translating the result to use
                    // direction as necessary.
                    if (!parallel(dir1, dir2, epsilon))
                    {
                        if (parallel(dir1, direction, epsilon))
                        {
                            auto [pd, par2] = closestPointParameters(Line3d{outerPrev, direction},
                                                                     Line3d{innerPrev, dir2},
                                                                     epsilon);
                            if (Prec(0) <= pd && pd <= d1len
                                && Prec(0) <= dir2 && dir2 <= d2len)
                            {
                                firstResultOut = std::min(firstResultOut, pd);
                                lastResultOut = std::max(lastResultOut, pd);
                            }
                        }
                        else if (parallel(dir2, direction, epsilon))
                        {
                            auto [par1, pd] = closestPointParameters(Line3d{outerPrev, dir1},
                                                                     Line3d{innerPrev, direction},
                                                                     epsilon);
                            if (Prec(0) <= par1 && par1 <= d1len
                                && Prec(0) <= pd && pd <= d2len)
                            {
                                firstResultOut = std::min(firstResultOut, pd);
                                lastResultOut = std::max(lastResultOut, pd);
                            }
                        }
                    }
                }
            }

            innerPrev = innerCurr;
        }

        outerPrev = outerCurr;
    }
    return firstResultOut <= lastResultOut;
}

/// Compute the intersection between two planes.
///
/// \param firstPlane One plane.
/// \param secondPlane Another plane.
/// \param resultOut Variable to write the resulting intersection to. Note that
///                  this is ONLY written to if the function returns true;
///                  otherwise it will be untouched.
/// \param epsilon Parallelity threshold for the planes.
///
/// \returns True if an intersection was found, false otherwise (i.e. if the
///          planes are parallel).
template <typename Prec>
bool intersect(const Plane<Prec>& firstPlane,
               const Plane<Prec>& secondPlane,
               Line3d<Prec>& resultOut,
               Prec epsilon = 50*math::Constant<Prec>::epsilon) noexcept;

/// Compute the intersection between two planes.
///
/// \remark Neither of the normals of the planes are required to be normalised,
///         but neither are they allowed to be zero.
///
/// \remark The returned line will NOT have normalised direction.
///
/// \param firstPlane One plane.
/// \param secondPlane Another plane.
/// \param resultOut Variable to write the resulting intersection to. Note that
///                  this is ONLY written to if the function returns true;
///                  otherwise it will be untouched. The direction will NOT be
///                  normalised.^
/// \param epsilon Parallelity threshold for the planes.
///
/// \returns True if an intersection was found, false otherwise (i.e. if the
///          planes are parallel).
template <typename Prec>
bool intersectUnnormalised(const Plane<Prec>& firstPlane,
                           const Plane<Prec>& secondPlane,
                           Line3d<Prec>& resultOut,
                           Prec epsilon = 50*math::Constant<Prec>::epsilon) noexcept;

/// Finds the next intersection of the given polygons.
///
/// Primarily for internal use by the boolean operations (but perfectly usable
/// outside, though), this function finds the next intersection of two closed
/// polygons, or determines if there are no intersections at all.
/// Note that this uses a slow algorithm -- it is O(n·m), where n is the number
/// of vertices in the first polygon, and m the number of vertices in the
/// second.
///
/// \remark The indices for the intersections are to the vertex at the *end* of
///         the respective line segment, meaning that if this gives index (i, j)
///         as the intersection indices, then the segment is the one from
///         pFirstPolygon[i-1] to pFirstPolygon[i] (wrapping around to last if
///         i = 0).
///
/// \param pFirstPolygon Pointer to first element of first polygon.
/// \param firstPolygonSize Number of vertices in the first polygon.
/// \param pSecondPolygon Pointer to the first element of the second polygon.
/// \param secondPolygonSize Number of vertices in the second polygon.
/// \param firstStart Vertex to start search from in the first polygon.
/// \param secondStart Vertex to start search from in the second polygon.
/// \param includeStart Whether to include the very first line segment in the
///                     search.
/// \param alongFirst Whether to search for an intersection going along segments
///                   from the first or second polygon (so if this is set to
///                   true, and this is started at an intersection, then it is
///                   guaranteed that there is no intersection between the start
///                   point of the first polygon and the one returned from here,
///                   i.e. this controls which polygon 'next' refers to).
/// \param firstParOut If not null, the first parameter of the intersection will
///                    be written here, i.e. if the intersection index is i,
///                    then this will give the value of t such that the
///                    following gives the point of intersection:
///                    firstPoly[i-1]+t*(firstPoly[i]-firstPoly[i-1]),
///                    where firstPoly refers to pFirstPolygon with wraparound.
/// \param secondParOut Like firstParOut, but for the second polygon instead.
/// \param firstOut If not null, the index to the end point of intersecting
///                 segment into pFirstPolygon will be written here.
/// \param secondOut Like firstOut, but for the second polygon.
/// \param epsilon Tolerance determining collinearity, equality of points, etc.
///
/// \returns True if an intersection was found, otherwise false. In case this
///          returns false, nothing is written to the pointed-to values.
template <typename Prec>
bool nextIntersection(const Vector2<Prec>* pFirstPolygon,
                      size_t firstPolygonSize,
                      const Vector2<Prec>* pSecondPolygon,
                      size_t secondPolygonSize,
                      size_t firstStart = 0,
                      size_t secondStart = 0,
                      bool includeStart = true,
                      bool alongFirst = true,
                      Prec* firstParOut = nullptr,
                      Prec* secondParOut = nullptr,
                      size_t* firstOut = nullptr,
                      size_t* secondOut = nullptr,
                      Prec epsilon = 20*math::Constant<Prec>::epsilon)
{
    if (!alongFirst)
    {
        std::swap(pFirstPolygon, pSecondPolygon);
        std::swap(firstPolygonSize, secondPolygonSize);
        std::swap(firstStart, secondStart);
        std::swap(firstParOut, secondParOut);
        std::swap(firstOut, secondOut);
    }

    auto sqEps = epsilon * epsilon;

    size_t secondOffset = !includeStart;

    auto secondInitIndex = (secondStart + secondOffset) % secondPolygonSize;

    auto fmin1 = (firstStart ? firstStart : firstPolygonSize) - 1;
    auto smin1 = (secondInitIndex ? secondInitIndex : secondPolygonSize) - 1;

    auto fp0 = pFirstPolygon[fmin1];
    auto sp0 = pSecondPolygon[smin1];
    // Now we iterate with first polygon as the outer one.
    for (size_t f = 0; f < firstPolygonSize; ++f)
    {
        auto first = (f+firstStart)%firstPolygonSize;
        auto fp1 = pFirstPolygon[first];
        Line2d<Prec> firstLine{fp0, fp1-fp0};
        fp0 = fp1;

        for (size_t s = secondOffset; s < secondPolygonSize; ++s)
        {
            auto second = (s+secondStart)%secondPolygonSize;
            auto sp1 = pSecondPolygon[second];
            Line2d<Prec> secondLine{sp0, sp1-sp0};
            // Note: The above lines are allowed to have zero length
            sp0 = sp1;

            auto [parFirst, parSecond] = intersect(firstLine,
                                                   secondLine,
                                                   epsilon);

            if (Prec(0) <= parFirst && parFirst <= Prec(1)
                && Prec(0) <= parSecond && parSecond <= Prec(1))
            {
                auto fdsq = squaredLength(firstLine.direction);
                auto sdsq = squaredLength(secondLine.direction);

                if (fdsq * parFirst <= sqEps
                    || fdsq * (Prec(1)-parFirst) <= sqEps
                    || sdsq * parSecond <= sqEps
                    || sdsq * (Prec(1)-parSecond) <= sqEps) continue;

                if (firstParOut) *firstParOut = parFirst;
                if (secondParOut) *secondParOut = parSecond;
                if (firstOut) *firstOut = first;
                if (secondOut) *secondOut = second;
                return true;
            }
        }
        secondOffset = 0;
    }
    return false;
}

/// Computes the intersection of two triangles.
///
/// Special care must be taken for this function, since the intersection set can
/// have a variable number of dimensions. The following cases are possible;
/// enumerated by the number of returned vertices:
/// * 0: No intersection. The most common case for random triangles.
/// * 1: Zero-dimensional intersection; only a single point. Happens only if a
///      vertex from one triangle just exactly touches the other *or* if two
///      edges of non-coplanar triangles intersects. Extremely rare case for
///      random triangles.
/// * 2: One-dimensional intersection; a line segment. This happens either if
///      the triangles are coplanar and have collinear edges (which is very rare
///      for random triangles), or if the triangles are non-coplanar and their
///      respective planes intersect (and the triangles as well). The latter
///      case is by far the most common for random triangles.
/// * 3-6: Two-dimensional intersection; a polygon. This only happens if the
///        triangles are coplanar.
///
/// \param triA One triangle.
/// \param triB Another triangle.
///
/// \returns An array of up to six 3d-vectors and a number which tells the exact
///          number of vertices of the intersection.
template <typename Prec>
std::pair<std::array<Vector3<Prec>, 6>, size_t>
intersect(Triangle3d<Prec> triA, Triangle3d<Prec> triB,
          Prec epsilon = Prec(20)*math::Constant<Prec>::epsilon);

/// Computes the parameters for the projection of a set of vertices on an axis.
///
/// Note that this doesn't compute the actual projection, but rather one scaled
/// by the length of the axis, so if the limits for the actual projection is
/// preferred, one should normalise the axis before calling this.
///
/// \param axis An axis.
/// \param vertices Pointer to the first vertex to project.
/// \param vertexCount Number of vertices to project. Must be nonzero.
///
/// \returns [pMin, pMax] giving the interval of projection onto the axis.
template <typename Prec>
std::pair<Prec, Prec> parametersOfAxisProjection(Vector3<Prec> axis,
                                                 const Vector3<Prec>* vertices,
                                                 size_t vertexCount)
{
    auto minProj = math::Constant<Prec>::maximum;
    auto maxProj = -minProj;
    for (size_t i = 0; i < vertexCount; ++i)
    {
        auto proj = dot(axis, vertices[i]);
        minProj = std::min(minProj, proj);
        maxProj = std::max(maxProj, proj);
    }
    return {minProj, maxProj};
}

template <typename Prec>
bool isSeparatingAxis(Vector3<Prec> axis,
                      const Vector3<Prec>* pFirstVertices,
                      size_t firstVertexCount,
                      const Vector3<Prec>* pSecondVertices,
                      size_t secondVertexCount)
{
    auto [fl, fh] = parametersOfAxisProjection(axis,
                                               pFirstVertices,
                                               firstVertexCount);
    auto [sl, sh] = parametersOfAxisProjection(axis,
                                               pSecondVertices,
                                               secondVertexCount);
    return fh < sl || sh < fl;
}

/// Determines if two convex polygons intersects.
///
/// This is done using the separating axis test; and as thus has complexity
/// O(n·m²+n²·m), where n is the number of vertices of the first polygon,
/// and m is those of the second polygon. Thus, while this is perfectly fine for
/// small polygons (like triangles and quadrilaterals), this is probably not
/// fast enough for larger polygons, and one therefore needs another algorithm
/// in that case.
///
/// \remark It says convex for a reason; ensure that both polygons are convex.
///         Otherwise the result may be wrong, although only wrong in one way:
///         If non-convex polygons are reported as not intersecting, that is
///         correct, however if they are reported as intersecting, that is not
///         necessarily true. However, make no mistake: This doesn't magically
///         compute a convex hull and/or reorder points -- they must be ordered
///         so that traversing them in their order yields a convex polygon.
///
/// \param pFirstPolygon Pointer to the first vertex of the first polygon.
/// \param firstPolygonSize Number of vertices in the first polygon.
/// \param pSecondPolygon Pointer to the first vertex of the second polygon.
/// \param secondPolygonSize Number of vertices in the second polygon.
///
/// \returns True if the polygons intersect, otherwise false.
template <typename Prec>
bool convexIntersects(const Vector3<Prec>* pFirstPolygon,
                      size_t firstPolygonSize,
                      const Vector3<Prec>* pSecondPolygon,
                      size_t secondPolygonSize)
{
    auto n1 = computeNormal(pFirstPolygon, firstPolygonSize);
    auto n2 = computeNormal(pSecondPolygon, secondPolygonSize);

    if (isSeparatingAxis(n1,
                         pFirstPolygon, firstPolygonSize,
                         pSecondPolygon, secondPolygonSize)) return false;
    if (isSeparatingAxis(n2,
                         pFirstPolygon, firstPolygonSize,
                         pSecondPolygon, secondPolygonSize)) return false;

    auto prevFirst = pFirstPolygon[firstPolygonSize-1];
    for (size_t i = 0; i < firstPolygonSize; ++i)
    {
        auto currFirst = pFirstPolygon[i];
        auto firstAxis = currFirst - prevFirst;
        prevFirst = currFirst;

        auto prevSecond = pSecondPolygon[secondPolygonSize-1];
        for (size_t j = 0; j < secondPolygonSize; ++j)
        {
            auto currSecond = pSecondPolygon[j];
            auto secondAxis = currSecond - prevSecond;
            prevSecond = currSecond;

            if (isSeparatingAxis(cross(firstAxis, secondAxis),
                                 pFirstPolygon,
                                 firstPolygonSize,
                                 pSecondPolygon,
                                 secondPolygonSize)) return false;
        }
    }

    return true;
}

} // namespace kraken::geometry

#endif // KRAKEN_GEOMETRY_INTERSECTION_HPP_INCLUDED
