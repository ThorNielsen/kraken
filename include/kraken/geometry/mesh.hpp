#ifndef KRAKEN_GEOMETRY_MESH_HPP_INCLUDED
#define KRAKEN_GEOMETRY_MESH_HPP_INCLUDED

#include "kraken/math/matrix.hpp"
#include <cstddef>
#include <kraken/types.hpp>

#include <array>
#include <limits>
#include <map>
#include <ostream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

namespace kraken
{

namespace geometry
{

using VertexIndex = U32;
using EdgeIndex = U32;
using FaceIndex = U32;

// The max-1 is not a bug -- max is used to mark deleted elements.
constexpr VertexIndex invalidVertex = std::numeric_limits<VertexIndex>::max()-1;
constexpr EdgeIndex invalidEdge = std::numeric_limits<EdgeIndex>::max()-1;
constexpr FaceIndex invalidFace = std::numeric_limits<FaceIndex>::max()-1;

struct Vertex
{
    union
    {
        math::vec3 pos;
        VertexIndex _nextFree;
    };
    EdgeIndex edge; // Edge which has this vertex as its origin.
};

struct Edge
{
    VertexIndex origin;
    EdgeIndex twin;
    EdgeIndex prev;
    union
    {
        EdgeIndex next;
        EdgeIndex _nextFree;
    };
    FaceIndex face;
};

struct Face
{
    union
    {
        math::vec3 center;
        FaceIndex _nextFree;
    };
    math::vec3 normal;
    EdgeIndex outer;
};

enum class FeatureType
{
    Vertex, Edge, Face, None
};

struct Feature
{
    VertexIndex vertex;
    EdgeIndex edge;
    FaceIndex face;
    FeatureType type;
};

class Mesh;

namespace priv
{

constexpr VertexIndex deletedVertex = std::numeric_limits<VertexIndex>::max();
constexpr EdgeIndex deletedEdge = std::numeric_limits<EdgeIndex>::max();
constexpr FaceIndex deletedFace = std::numeric_limits<FaceIndex>::max();

inline bool deleted(const Vertex& v)
{
    return v.edge == deletedEdge;
}

inline void markDeleted(Vertex& v, VertexIndex next)
{
    v.edge = deletedEdge;
    v._nextFree = next;
}

inline bool deleted(const Edge& e)
{
    return e.prev == deletedEdge;
}

inline void markDeleted(Edge& e, EdgeIndex next)
{
    e.prev = deletedEdge;
    e._nextFree = next;
}

inline bool deleted(const Face& f)
{
    return f.outer == deletedEdge;
}

inline void markDeleted(Face& f, FaceIndex next)
{
    f.outer = deletedEdge;
    f._nextFree = next;
}

template <typename Type, typename TypeIndex, typename TypeContainer>
class MeshIterator
{
public:
    MeshIterator() = default;
    MeshIterator(const MeshIterator&) = default;
    MeshIterator& operator=(const MeshIterator&) = default;

    MeshIterator& operator++()
    {
        do
        {
            ++m_pos;
        } while (m_pos < m_container->size()
                 && deleted((*m_container)[m_pos]));
        return *this;
    }
    MeshIterator operator++(int)
    {
        MeshIterator cpy(*this);
        operator++();
        return cpy;
    }
    MeshIterator& operator--()
    {
        do
        {
            --m_pos;
        } while (deleted((*m_container)[m_pos]));
    }
    MeshIterator operator--(int)
    {
        MeshIterator cpy(*this);
        operator--();
        return cpy;
    }

    Type& operator*() const
    {
        return (*m_container)[m_pos];
    }

    TypeIndex index() const
    {
        return m_pos;
    }

    Type* operator->() const
    {
        return &*(*this);
    }

    bool operator<(const MeshIterator& o) const
    {
        return m_pos < o.m_pos;
    }
    bool operator<=(const MeshIterator& o) const
    {
        return m_pos <= o.m_pos;
    }
    bool operator>(const MeshIterator& o) const
    {
        return m_pos > o.m_pos;
    }
    bool operator>=(const MeshIterator& o) const
    {
        return m_pos >= o.m_pos;
    }
    bool operator==(const MeshIterator& o) const
    {
        return m_pos == o.m_pos;
    }
    bool operator!=(const MeshIterator& o) const
    {
        return m_pos != o.m_pos;
    }
private:
    friend class ::kraken::geometry::Mesh;
    TypeContainer* m_container;
    TypeIndex m_pos;
};

} // namespace priv


inline std::ostream& operator<<(std::ostream& ost, const Vertex& vert)
{
    return ost << "Vertex[" << vert.pos << " | " << vert.edge << "]";
}

inline std::ostream& operator<<(std::ostream& ost, const Edge& edge)
{
    return ost << "Edge[origin " << edge.origin << " | twin " << edge.twin
               << " | " << edge.prev << " p->n " << edge.next << " (face "
               << edge.face << ")]";
}

inline std::ostream& operator<<(std::ostream& ost, const Face& face)
{
    return ost << "Face[c" << face.center << " | n" << face.normal
               << " | outer " << face.outer << "]";
}

class Mesh
{
public:
    using VertexIterator = priv::MeshIterator<Vertex,
                                              VertexIndex,
                                              std::vector<Vertex>>;
    using ConstVertexIterator = priv::MeshIterator<const Vertex,
                                                   VertexIndex,
                                                   const std::vector<Vertex>>;
    using EdgeIterator = priv::MeshIterator<Edge, EdgeIndex, std::vector<Edge>>;
    using ConstEdgeIterator = priv::MeshIterator<const Edge,
                                                 EdgeIndex,
                                                 const std::vector<Edge>>;
    using FaceIterator = priv::MeshIterator<Face, FaceIndex, std::vector<Face>>;
    using ConstFaceIterator = priv::MeshIterator<const Face,
                                                 FaceIndex,
                                                 const std::vector<Face>>;


    VertexIndex createVertex()
    {
        Vertex v{math::vec3{0.f, 0.f, 0.f}, invalidEdge};
        return insert(v);
    }
    EdgeIndex createEdge()
    {
        Edge e{invalidVertex, invalidEdge, invalidEdge, invalidEdge,
               invalidFace};
        return insert(e);
    }
    FaceIndex createFace()
    {
        Face f{math::vec3{0.f, 0.f, 0.f}, math::vec3{0.f, 0.f, 0.f}, invalidEdge};
        return insert(f);
    }

    VertexIndex insert(Vertex v)
    {
        ++vCount;
        if (vNext == priv::deletedVertex)
        {
            m_verts.push_back(v);
            return static_cast<VertexIndex>(m_verts.size()-1);
        }
        auto cv = vNext;
        vNext = m_verts[cv]._nextFree;
        m_verts[cv] = v;
        return cv;
    }
    EdgeIndex insert(Edge e)
    {
        ++eCount;
        if (eNext == priv::deletedEdge)
        {
            m_edges.push_back(e);
            return static_cast<EdgeIndex>(m_edges.size()-1);
        }
        auto ce = eNext;
        eNext = m_edges[ce]._nextFree;
        m_edges[ce] = e;
        return ce;
    }
    FaceIndex insert(Face f)
    {
        ++fCount;
        if (fNext == priv::deletedFace)
        {
            m_faces.push_back(f);
            return static_cast<FaceIndex>(m_faces.size()-1);
        }
        auto cf = fNext;
        fNext = m_faces[cf]._nextFree;
        m_faces[cf] = f;
        return cf;
    }

    Vertex& vertexAt(VertexIndex i)
    {
        if (!isValidVertex(i))
            throw std::domain_error("Vertex not found: " + std::to_string(i));
        return m_verts[i];
    }
    const Vertex& vertexAt(VertexIndex i) const
    {
        if (!isValidVertex(i))
            throw std::domain_error("Vertex not found: " + std::to_string(i));
        return m_verts[i];
    }
    Edge& edgeAt(EdgeIndex i)
    {
        if (!isValidEdge(i))
            throw std::domain_error("Edge not found: " + std::to_string(i));
        return m_edges[i];
    }
    const Edge& edgeAt(EdgeIndex i) const
    {
        if (!isValidEdge(i))
            throw std::domain_error("Edge not found: " + std::to_string(i));
        return m_edges[i];
    }
    Face& faceAt(FaceIndex i)
    {
        if (!isValidFace(i))
            throw std::domain_error("Face not found: " + std::to_string(i));
        return m_faces[i];
    }
    const Face& faceAt(FaceIndex i) const
    {
        if (!isValidFace(i))
            throw std::domain_error("Face not found: " + std::to_string(i));
        return m_faces[i];
    }

    //*
    Vertex& vertex(VertexIndex i) { return vertexAt(i); }
    const Vertex& vertex(VertexIndex i) const { return vertexAt(i); }
    Edge& edge(EdgeIndex i) { return edgeAt(i); }
    const Edge& edge(EdgeIndex i) const { return edgeAt(i); }
    Face& face(FaceIndex i) { return faceAt(i); }
    const Face& face(FaceIndex i) const { return faceAt(i); }
    /*/
    Vertex& vertex(VertexIndex i) { return m_verts[i]; }
    const Vertex& vertex(VertexIndex i) const { return m_verts[i]; }
    Edge& edge(EdgeIndex i) { return m_edges[i]; }
    const Edge& edge(EdgeIndex i) const { return m_edges[i]; }
    Face& face(FaceIndex i) { return m_faces[i]; }
    const Face& face(FaceIndex i) const { return m_faces[i]; } //*/

    size_t vertices() const { return vCount; }
    size_t edges() const { return eCount; }
    size_t faces() const { return fCount; }

    void deleteVertex(VertexIndex i)
    {
        if (priv::deleted(m_verts[i])) return;
        --vCount;
        priv::markDeleted(m_verts[i], vNext);
        vNext = i;
    }
    void deleteEdge(EdgeIndex i)
    {
        if (priv::deleted(m_edges[i])) return;
        --eCount;
        priv::markDeleted(m_edges[i], eNext);
        eNext = i;
    }
    void deleteFace(FaceIndex i)
    {
        if (priv::deleted(m_faces[i])) return;
        --fCount;
        priv::markDeleted(m_faces[i], fNext);
        fNext = i;
    }

    bool isValidVertex(VertexIndex i) const
    {
        if (i >= m_verts.size()) return false;
        return !priv::deleted(m_verts[i]);
    }
    bool isValidEdge(EdgeIndex i) const
    {
        if (i >= m_edges.size()) return false;
        return !priv::deleted(m_edges[i]);
    }
    bool isValidFace(FaceIndex i) const
    {
        if (i >= m_faces.size()) return false;
        return !priv::deleted(m_faces[i]);
    }

    void updateFace(FaceIndex i);

    S64 eulerCharacteristic() const
    {
        return (S64)vertices() - (S64)edges() / 2 + (S64)faces();
    }

    bool isPolyhedron() const
    {
        return eulerCharacteristic() == 2
               && edges() % 2 == 0;
    }

    void compactify();
    void clear();

    void dump() const;
    void checkRefs() const;
    void checkTopology() const;

    template <typename IndexType>
    static Mesh fromTriangleMesh(const math::vec3* vertices, size_t vertexCount,
                                 size_t vertexStride,
                                 const IndexType* indices, size_t indexCount)
    {
        if (indexCount % 3 != 0)
        {
            throw std::domain_error("Indices contains non-triangular faces.");
        }

        Mesh mesh;
        std::vector<VertexIndex> vIdx;
        for (size_t i = 0; i < vertexCount; ++i)
        {
            auto pos = *reinterpret_cast<const math::vec3*>(reinterpret_cast<const U8*>(vertices)+vertexStride*i);
            vIdx.push_back(mesh.insert(Vertex{pos, invalidEdge}));
        }
        std::map<std::pair<VertexIndex, VertexIndex>, EdgeIndex> edges;
        for (size_t i = 0; i < indexCount; i += 3)
        {
            FaceIndex cFace = mesh.createFace();

            std::array<EdgeIndex, 3> faceEdgeIndices
                = {invalidEdge, invalidEdge, invalidEdge};

            for (size_t s = 0; s < 3; ++s)
            {
                auto v0 = vIdx[indices[i+s]];
                auto v1 = vIdx[indices[i+(s+1)%3]];
                auto currEdge = edges.find({v0, v1});
                if (currEdge == edges.end())
                {
                    auto edgeIdx = mesh.createEdge();
                    auto twinIdx = mesh.createEdge();
                    auto& edge = mesh.edge(edgeIdx);
                    auto& twin = mesh.edge(twinIdx);
                    // Fix vertex 'pointers'
                    mesh.vertex(v0).edge = edgeIdx;
                    mesh.vertex(v1).edge = twinIdx;

                    // Fix edge 'pointers'
                    edge.twin = twinIdx;
                    edge.origin = v0;
                    edge.face = cFace;
                    twin.twin = edgeIdx;
                    twin.origin = v1;
                    // We don't know the face of the twin edge yet.

                    edges[{v0, v1}] = edgeIdx;
                    edges[{v1, v0}] = twinIdx;
                    faceEdgeIndices[s] = edgeIdx;
                }
                else
                {
                    faceEdgeIndices[s] = currEdge->second;
                }
            }

            for (size_t s = 0; s < 3; ++s)
            {
                auto& cEdge = mesh.edge(faceEdgeIndices[s]);
                cEdge.prev = faceEdgeIndices[(s+2)%3];
                cEdge.next = faceEdgeIndices[(s+1)%3];
                cEdge.face = cFace;
            }

            mesh.face(cFace).outer = faceEdgeIndices[0];
            // For optimisation later (if needed), this can most likely be
            // replaced by a cross product and average of the vertices since we
            // only have vertices.
            mesh.updateFace(cFace);
        }

        return mesh;
    }

    VertexIterator vbegin()
    {
        return createBeginIterator<VertexIterator>(&m_verts);
    }
    VertexIterator vend()
    {
        return createEndIterator<VertexIterator>(&m_verts);
    }
    ConstVertexIterator vbegin() const
    {
        return createBeginIterator<ConstVertexIterator>(&m_verts);
    }
    ConstVertexIterator vend() const
    {
        return createEndIterator<ConstVertexIterator>(&m_verts);
    }
    EdgeIterator ebegin()
    {
        return createBeginIterator<EdgeIterator>(&m_edges);
    }
    EdgeIterator eend()
    {
        return createEndIterator<EdgeIterator>(&m_edges);
    }
    ConstEdgeIterator ebegin() const
    {
        return createBeginIterator<ConstEdgeIterator>(&m_edges);
    }
    ConstEdgeIterator eend() const
    {
        return createEndIterator<ConstEdgeIterator>(&m_edges);
    }
    FaceIterator fbegin()
    {
        return createBeginIterator<FaceIterator>(&m_faces);
    }
    FaceIterator fend()
    {
        return createEndIterator<FaceIterator>(&m_faces);
    }
    ConstFaceIterator fbegin() const
    {
        return createBeginIterator<ConstFaceIterator>(&m_faces);
    }
    ConstFaceIterator fend() const
    {
        return createEndIterator<ConstFaceIterator>(&m_faces);
    }

private:
    template <typename Iterator, typename Container>
    static Iterator createBeginIterator(Container* container)
    {
        Iterator it;
        it.m_container = container;
        it.m_pos = 0;
        while (it.m_pos < container->size()
               && priv::deleted((*container)[it.m_pos]))
        {
            ++it.m_pos;
        }
        return it;
    }

    template <typename Iterator, typename Container>
    static Iterator createEndIterator(Container* container)
    {
        Iterator it;
        it.m_container = container;
        it.m_pos = static_cast<decltype(it.m_pos)>(container->size());
        return it;
    }

    std::vector<Vertex> m_verts;
    std::vector<Edge> m_edges;
    std::vector<Face> m_faces;
    VertexIndex vCount = 0;
    VertexIndex vNext = priv::deletedVertex;
    EdgeIndex eCount = 0;
    EdgeIndex eNext = priv::deletedEdge;
    FaceIndex fCount = 0;
    FaceIndex fNext = priv::deletedFace;
};


inline float signedDistance(const Face& f, math::vec3 p)
{
    return math::dot(f.normal, p) - math::dot(f.center, f.normal);
}

} // namespace geometry

} // namespace kraken

#endif // KRAKEN_GEOMETRY_MESH_HPP_INCLUDED

