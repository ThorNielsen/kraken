#ifndef KRAKEN_GEOMETRY_MESHREPAIR_HPP_INCLUDED
#define KRAKEN_GEOMETRY_MESHREPAIR_HPP_INCLUDED

#include "kraken/geometry/boundingvolumes.hpp"
#include "kraken/geometry/primitives.hpp"
#include "kraken/math/constants.hpp"
#include "kraken/types.hpp"
#include <algorithm>
#include <cstddef>
#include <cstring>
#include <functional>
#include <kraken/geometry/bvh.hpp>
#include <kraken/geometry/bvhconstruction.hpp>
#include <kraken/utility/memory.hpp>

#include <array>
#include <limits>
#include <memory>
#include <set>
#include <stack>
#include <stdexcept>
#include <unordered_map>
#include <utility>
#include <vector>

namespace kraken::geometry
{

namespace priv
{

template <typename IndexType>
using Edge = std::pair<IndexType, IndexType>;

template <typename IndexType>
struct EdgeHasher
{
    size_t operator()(const Edge<IndexType>& edge) const noexcept
    {
        return (std::hash<IndexType>()(edge.first) << 3)
                ^ std::hash<IndexType>()(edge.second);
    }
};

} // namespace priv

/// This determines the indices of all unused vertices.
///
/// \param vertexCount Number of vertices in the corresponding vertex array.
/// \param indexCount Number of indices in the index array.
/// \param pIndices Pointer to the index array.
///
/// \returns A vector of indices, each of which referring to an unused vertex.
template <typename IndexType>
[[nodiscard]] std::vector<IndexType>
getUnusedIndices(size_t vertexCount,
                 size_t indexCount,
                 const IndexType* pIndices)
{
    std::vector<bool> used(vertexCount, false);
    while (indexCount --> 0)
    {
        used[*pIndices++] = true;
    }
    std::vector<IndexType> unused;
    for (size_t i = 0; i < vertexCount; ++i)
    {
        if (!used[i]) unused.push_back(static_cast<IndexType>(i));
    }
    return unused;
}

/// Removes all vertices of the mesh not present in the index buffer.
///
/// \param vertexCount Number of vertices.
/// \param pVertices Pointer to the start of the vertex array.
/// \param vertexSize The size (in bytes) of each individual vertex.
/// \param indexCount Number of indices.
/// \param pIndices Pointer to the start of the index array.
/// \param pMappingOut Optional, but if not nullptr, it will be resized to
///                    exactly as many entries as there are vertices left; the
///                    i'th element gives the original position of that vertex;
///                    i.e. if (*pMappingOut)[i] = j, then pVertices[j] has been
///                    mapped to pVertices[i].
///
/// \returns The number of remaining vertices.
template <typename IndexType>
size_t removeUnusedVertices(size_t vertexCount,
                            void* pVertices,
                            size_t vertexSize,
                            size_t indexCount,
                            const IndexType* pIndices,
                            std::vector<IndexType>* pMappingOut = nullptr)
{
    auto unused = getUnusedIndices(vertexCount, indexCount, pIndices);
    removeUntypedArrayElements(unused.begin(),
                               unused.end(),
                               vertexCount,
                               pVertices,
                               vertexSize);

    size_t numVerticesLeft = vertexCount - unused.size();

    if (pMappingOut)
    {
        pMappingOut->resize(numVerticesLeft);
        size_t currUnused = 0;
        size_t mapTo = 0;
        for (size_t i = 0; i < vertexCount; ++i)
        {
            if (currUnused < unused.size() && unused[currUnused] == i)
            {
                ++currUnused;
            }
            else
            {
                (*pMappingOut)[mapTo++] = i;
            }
        }
    }

    return numVerticesLeft;
}

/// This computes the minimum number of indices in a corresponding vertex array.
///
/// Note that this doesn't count the number of unique vertices, but rather
/// assumes that the indices index into a contiguous vertex array and therefore
/// simply returns the maximally encountered index plus one (or zero, in case
/// there are no vertices).
///
/// \param indexCount Number of indices.
/// \param pIndices Pointer to the index array.
/// \returns The minimal number of vertices in a vertex array that the given
///          index array can refer into.
template <typename IndexType>
[[nodiscard]] size_t
getVertexCountFromIndices(size_t indexCount, const IndexType* pIndices)
{
    if (!indexCount) return 0;
    size_t vertexCount = 0;
    for (size_t i = 0; i < indexCount; ++i)
    {
        vertexCount = std::max<size_t>(vertexCount, pIndices[i]);
    }
    return vertexCount+1;
}

/// Counts the number of times each vertex is used in an index array.
///
/// \tparam ReturnIndexType The returned type that should contain the occurences
///                         (the underlying type for the returned array).
/// \param vertexCount Number of vertices in the corresponding vertex array. May
///                    be zero in which case it is computed from the index
///                    array; note that this carries a performance penalty,
///                    though.
/// \param indexCount Number of indices.
/// \param pIndices Pointer to the index array.
/// \returns A vector with the same size as the number of vertices, where the
///          i'th element gives the number of occurences of vertex i.
template <typename IndexType, typename ReturnIndexType = U32>
[[nodiscard]] std::vector<ReturnIndexType>
countVertexUsages(size_t vertexCount,
                  size_t indexCount,
                  const IndexType* pIndices)
{
    if (!vertexCount)
    {
        vertexCount = getVertexCountFromIndices(indexCount, pIndices);
    }
    std::vector<ReturnIndexType> usages(vertexCount, 0);
    while (indexCount --> 0) ++usages[*pIndices++];
    return usages;
}

/// Computes connectivity components solely by considering indices.
///
/// By default this returns the connected *triangles*, where the returned array
/// will thus have size indexCount/3. Optionally it is possible to get the
/// connected vertices instead. In either case, the returned array will contain
/// integers from 0 to the number of connected components minus one; and each
/// entry with the same number indicates that the triangle/vertex of that index
/// are connected.
/// Connectivity is defined naturally: Two vertices are connected iff they are
/// part of the same triangle; note that this does not take into account any
/// geometry (so two different vertices in the exact same position will NOT be
/// considered connected unless there is a path from one to the other through
/// some triangles described by the indices). This is also the rationale for
/// returning connectivity on a per-triangle basis, as triangles are a much more
/// natural unit of connectivity than vertices.
///
/// \param vertexCount The number of vertices; if zero will be computed from the
///                    indices (not recommended as it will require traversing
///                    the entire index array).
/// \param indexCount The number of indices.
/// \param pIndices Pointer to the beginning of the index array.
/// \param returnPerVertex Whether to return per-vertex or per-triangle
///                        connectivity; note that per-vertex necessitates a
///                        conversion, which in low-memory scenarios may not be
///                        desirable or even possible (it requires storing both
///                        the per-triangle and per-vertex connectivity arrays).
///
/// \returns An array describing the connected components.
template <typename IndexType>
[[nodiscard]] std::vector<size_t>
getConnectedComponents(size_t vertexCount,
                       size_t indexCount,
                       const IndexType* pIndices,
                       bool returnPerVertex = false)
{
    if (!vertexCount)
    {
        vertexCount = getVertexCountFromIndices(indexCount, pIndices);
    }
    if (!vertexCount) return {};

    constexpr size_t invalidIndex = ~(size_t)0;
    auto usages = countVertexUsages<IndexType, size_t>(vertexCount+1,
                                                       indexCount,
                                                       pIndices);

    if (usages.empty()) return {};

    // Now we offset usages in order to be able to pack neighbours tightly.
    for (size_t i = 1; i < vertexCount; ++i)
    {
        usages[i] += usages[i-1];
    }

    usages.pop_back();
    usages.insert(usages.begin(), 0);

    std::vector<size_t> vertexTriangleConnections(indexCount, invalidIndex);

    for (size_t i = 0; i < indexCount; ++i)
    {
        vertexTriangleConnections.at(usages[pIndices[i]]++) = i/3;
    }

    // We have added to each usage exactly as many times as it is used, in
    // effect shifting the array by one element. So we shift it back.
    for (size_t i = vertexCount; i; --i)
    {
        usages[i] = usages[i-1];
    }
    usages[0] = 0;

    // Note: Now we have usages[vertexCount] == indexCount, and so for vertex i,
    // we always have that its neighbours are placed in usages[i..(i+1)].

    auto triCount = indexCount / 3;
    std::vector<size_t> components(triCount, invalidIndex);

    size_t nextComponent = 0;

    std::stack<IndexType> unprocessed;
    for (size_t i = 0; i < triCount; ++i)
    {
        if (components[i] != invalidIndex) continue;
        unprocessed.push(i);
        while (!unprocessed.empty())
        {
            auto curr = unprocessed.top();
            unprocessed.pop();
            if (components[curr] != invalidIndex) continue;
            components[curr] = nextComponent;

            for (size_t v = 0; v < 3; ++v)
            {
                auto currVertex = pIndices[3*curr+v];
                auto from = usages[currVertex];
                auto to = usages[currVertex+1];
                for (auto k = from; k < to; ++k)
                {
                    auto tri = vertexTriangleConnections[k];
                    if (components[tri] == invalidIndex) unprocessed.push(tri);
                }
            }
        }
        ++nextComponent;
    }

    if (returnPerVertex)
    {
        // Hopefully make the implementation free up the arrays to avoid a low-
        // memory scenario.
        unprocessed = {};
        vertexTriangleConnections = {};

        std::vector<size_t> perVertexComponents(vertexCount);
        size_t currIndex = 0;
        for (size_t currTriangle = 0;
             currTriangle < triCount;
             ++currTriangle, currIndex += 3)
        {
            auto triComp = components[currTriangle];
            perVertexComponents[pIndices[currIndex  ]] = triComp;
            perVertexComponents[pIndices[currIndex+1]] = triComp;
            perVertexComponents[pIndices[currIndex+2]] = triComp;
        }
        return perVertexComponents;
    }

    return components;
}

/// Partially splits all non-manifold edges into separate unconnected edges.
///
/// The partial part of this operation is that the indices are modified, but the
/// vertices are not duplicated; for each element in the returned vector that
/// vertex should be duplicated and pushed onto the vertex array in order to
/// finalise the splitting.
///
/// \remark This may overflow, and WILL generate corrupt indices if so. This can
///         happen if the IndexType is too small to hold all the split edges;
///         one can compute exactly how many vertices this will add by computing
///         the sum of max(usages[i]-2, 0) over all i∈{0, ..., vertexCount-1},
///         where usages[i] denotes the number of times i occurs in the index
///         array.
///
/// \param vertexCount Number of vertices.
/// \param indexCount Number of indices.
/// \param pIndices Pointer to the index data. WILL BE MODIFIED!
///
/// \returns A vector of vertices to append, as described above.
template <typename IndexType>
[[nodiscard]] std::vector<IndexType>
partialNonManifoldEdgeSplit(size_t vertexCount,
                            size_t indexCount,
                            IndexType* pIndices)
{
    auto usages = countVertexUsages<IndexType>(vertexCount,
                                               indexCount,
                                               pIndices);
    std::vector<bool> taken(vertexCount, false);
    if (!vertexCount) vertexCount = usages.size();
    IndexType next = vertexCount;
    std::vector<size_t> verticesToAdd;
    auto triCount = indexCount / 3;
    for (size_t i = 0; i < triCount; ++i)
    {
        auto& i0 = pIndices[3*i];
        auto& i1 = pIndices[3*i+1];
        auto& i2 = pIndices[3*i+2];
        auto ni0 = i0;
        auto ni1 = i1;
        auto ni2 = i2;
        if (int(usages[i0] > 2) + int(usages[i1] > 2) + int(usages[i2] > 2) > 1)
        {
            ni0 = taken[i0] ? next++ : i0;
            ni1 = taken[i1] ? next++ : i1;
            ni2 = taken[i2] ? next++ : i2;
            if (ni0 >= vertexCount) verticesToAdd.push_back(i0);
            if (ni1 >= vertexCount) verticesToAdd.push_back(i1);
            if (ni2 >= vertexCount) verticesToAdd.push_back(i2);
        }
        taken[i0] = true; i0 = ni0;
        taken[i1] = true; i1 = ni1;
        taken[i2] = true; i2 = ni2;
    }
    return verticesToAdd;
}

/// Fixes T-vertices in a mesh by splitting opposite triangles.
///
/// \remark The position attribute inside the vertices must be contiguous and
///         each coordinate consist of three numbers each matching the template
///         type 'Prec'. Other than this they can be laid out arbitrarily.
///
/// \param pVertices Pointer to the vertices.
/// \param vertexSize Size of each vertex.
/// \param positionOffset Offset of the position data member within each vertex.
/// \param indexCount Number of indices.
/// \param pIndices The indices forming the triangles. WILL BE MODIFIED!
/// \param maxSegmentDistance Maximum distance to a segment to allow welding as
///                           opposed to just having a hole.
///
/// \returns A list of indices to add to the index data in order to apply the
///          fixes performed here.
template <typename Prec, typename IndexType>
[[nodiscard]] std::vector<IndexType>
fixTVertices(const void* pVertices,
             size_t vertexSize,
             size_t positionOffset,
             size_t indexCount,
             IndexType* pIndices,
             Prec maxSegmentDistance = 50 * math::Constant<Prec>::epsilon)
{
    // TODO: Replace the second BVH with a point-BVH.
    auto bvh = createTriangleBVH<Aabb<Prec>, Prec, IndexType>
                                (pointeradd(pVertices, positionOffset),
                                 vertexSize,
                                 pIndices,
                                 pIndices+indexCount);

    using TriangleAndIndex = std::pair<Triangle3d<Prec>, IndexType>;

    auto sqMaxDist = maxSegmentDistance * maxSegmentDistance;

    // Done in this cumbersome way to avoid problems with modifying the BVH
    // while traversing it.

    struct EdgeSplit
    {
        IndexType offset;
        IndexType vertexToAdd;
        Prec proj;

        bool operator<(const EdgeSplit& other) const
        {
            if (offset != other.offset) return offset < other.offset;
            if (proj != other.proj) return proj < other.proj;
            return vertexToAdd < other.vertexToAdd;
        }
    };

    std::unordered_map<size_t, std::set<EdgeSplit>> toSplit;

    // Takes a triangle index, an offset into that triangle's edges, a vertex
    // index and a projection length and marks that triangle for splitting at
    // that edge with the corresponding point.
    auto splitTriangle = [&toSplit]
    (size_t triangleIndex, IndexType offset, IndexType v, Prec proj)
    {
        toSplit[triangleIndex].insert({offset, v, proj});
    };

    processOverlaps(bvh, bvh,
    [pIndices, sqMaxDist, &splitTriangle]
    (const TriangleAndIndex& a, const TriangleAndIndex& b)
    {
        if (a.second == b.second) return;
        std::array<Vector3<Prec>, 3> points =
        {
            b.first.a, b.first.b, b.first.c,
        };
        const auto& ta = a.first;
        std::array<Line3d<Prec>, 3> lines =
        {
            Line3d<Prec>{ta.a, ta.b-ta.a},
            Line3d<Prec>{ta.b, ta.c-ta.b},
            Line3d<Prec>{ta.c, ta.a-ta.c},
        };
        for (size_t p = 0; p < 3; ++p)
        {
            auto pointIndex = 3*b.second+p;
            for (size_t l = 0; l < 3; ++l)
            {
                auto lStartIndex = 3*a.second+l;
                auto lEndIndex = 3*a.second+(l+1)%3;
                if (pIndices[lStartIndex] == pIndices[pointIndex] ||
                    pIndices[lEndIndex] == pIndices[pointIndex])
                {
                    continue;
                }
                auto par = dot(points[p]-lines[l].origin, lines[l].direction);
                par /= squaredLength(lines[l].direction);
                if (par < Prec(0) || par > Prec(1))
                {
                    continue;
                }
                auto pointOnLine = lines[l].origin + par * lines[l].direction;

                if (squaredLength(points[p]-pointOnLine) >= sqMaxDist)
                {
                    continue;
                }

                // We now have a T-Vertex.
                splitTriangle(a.second,
                              l,
                              pIndices[pointIndex],
                              par);
            }
        }
    },
    [maxSegmentDistance](const Aabb<Prec>& a, const Aabb<Prec>& b)
    {
        return a.overlapsApproximate(b, maxSegmentDistance);
    });

    std::vector<IndexType> toAdd;

    const void* posStart = pointeradd(pVertices, positionOffset);
    auto positionOf = [posStart, vertexSize](IndexType i)
    {
        using Vector3 = const Vector3<Prec>;
        return reinterpret_cast<const Vector3*>(pointeradd(posStart,
                                                           i*vertexSize));
    };

    for (auto& [triangleIndex, splitList] : toSplit)
    {
        // Last one duplicated to avoid having to mod out by 3.
        std::array<IndexType, 4> triangle =
        {
            pIndices[3*triangleIndex  ], pIndices[3*triangleIndex+1],
            pIndices[3*triangleIndex+2], pIndices[3*triangleIndex  ],
        };
        std::vector<IndexType> polygon;
        IndexType last = 0;
        polygon.push_back(triangle[last]);
        for (auto [offset, vertexToAdd, proj] : splitList)
        {
            while (last < offset)
            {
                polygon.push_back(triangle[++last]);
            }
            polygon.push_back(vertexToAdd);
        }
        while (last < 2) polygon.push_back(triangle[++last]);

        std::vector<Prec> segmentDotp;
        for (size_t i = 0; i < polygon.size(); ++i)
        {
            auto prev = (i+polygon.size()-1) % polygon.size();
            auto next = (i+1) % polygon.size();
            auto prevPoint = *positionOf(polygon[prev]);
            auto currPoint = *positionOf(polygon[i]);
            auto nextPoint = *positionOf(polygon[next]);

            auto pc = currPoint - prevPoint;
            auto np = nextPoint - currPoint;

            segmentDotp.push_back(dot(normalise(pc), normalise(np)));
        }

        Prec bestValue = std::numeric_limits<Prec>::max();
        size_t bestIndex = 0;
        for (size_t i = 0; i < polygon.size(); ++i)
        {
            auto prev = (i+polygon.size()-1) % polygon.size();
            auto next = (i+1) % polygon.size();
            auto currVal = std::max(std::abs(segmentDotp[prev]),
                                    std::abs(segmentDotp[next]));
            if (currVal < bestValue)
            {
                bestValue = currVal;
                bestIndex = i;
            }
        }

        bool modified = false;
        for (size_t i = 1; i <= polygon.size(); ++i)
        {
            auto prevI = i-1;
            auto currI = i % polygon.size();
            if (prevI == bestIndex || currI == bestIndex) continue;
            if (!modified)
            {
                pIndices[3*triangleIndex  ] = polygon[prevI];
                pIndices[3*triangleIndex+1] = polygon[currI];
                pIndices[3*triangleIndex+2] = polygon[bestIndex];
                modified = true;
            }
            else
            {
                toAdd.push_back(polygon[prevI]);
                toAdd.push_back(polygon[currI]);
                toAdd.push_back(polygon[bestIndex]);
            }
        }
    }

    return toAdd;
}

template <typename Prec, typename IndexType, typename IndexResizeFunction>
void fixTVertices(const void* pVertices,
                  size_t vertexSize,
                  size_t positionOffset,
                  size_t indexCount,
                  IndexType* pIndices,
                  const IndexResizeFunction& indexResizer,
                  Prec maxSegmentDistance = 50 * math::Constant<Prec>::epsilon)
{
    auto toFix = fixTVertices(pVertices,
                              vertexSize,
                              positionOffset,
                              indexCount,
                              pIndices,
                              maxSegmentDistance);
    if (toFix.empty()) return;
    auto appendAt = indexResizer(indexCount + toFix.size()) + indexCount;
    for (auto elem : toFix)
    {
        *appendAt++ = elem;
    }
}

/// Finds various types of non-manifold edges.
///
/// Given the indices to a triangular mesh (each triplet of indices
/// corresponding to one triangle) this finds all edges which is used in either
/// more or fewer than 2 triangles, and/or those used in two triangles, but
/// where the triangles have opposite winding order.
///
/// \param indexCount The number of indices of the mesh.
/// \param pIndices Pointer to the indices; with each successive triplet forming
///                 one triangle.
/// \param includeMultipleEdges Whether to return information about edges where
///                             three or more are shared.
/// \param includeDualEdges Whether to return information about edges where one
///                         is shared between two triangles BUT is in the same
///                         direction (this indicates that the two triangles are
///                         of opposite winding order).
/// \param includeSingleEdges Whether to include edges that are only referenced
///                           by a single triangle each.
///
/// \returns A vector of indices containing the resulting non-manifold edges
///          packed such that the first element determines the number N of edges
///          in that block, and the next 2*N entries are then indices into the
///          N edges in pIndices, laid out one after each other. So e.g. a
///          return vector of {1, 0, 1, 3, 1, 2, 3, 4, 5, 6} should be 'split'
///          into the two blocks {0, 1} and {1, 2,  3, 4,  5, 6}, meaning that
///          the edge from pIndices[0] to pIndices[1] occurs once only, and that
///          that from pIndices[1] to pIndices[2] is identical to that from
///          pIndices[3] to pIndices[4] which is again identical to that from
///          pIndices[5] to pIndices[6].
template <typename IndexType>
[[nodiscard]] std::vector<IndexType>
findNonManifoldEdges(size_t indexCount,
                     const IndexType* pIndices,
                     bool includeMultipleEdges,
                     bool includeDualEdges,
                     bool includeSingleEdges)
{
    using MEdge = priv::Edge<IndexType>;
    using MEdgeHasher = priv::EdgeHasher<IndexType>;
    struct EdgePairContainer
    {
        MEdge first{0, 0};
        MEdge second{0, 1};
        std::unique_ptr<std::vector<MEdge>> extra;
        IndexType count;

        void append(MEdge e)
        {
            if (count == 0) first = e;
            else if (count == 1) second = e;
            else
            {
                if (count == 2) extra = std::make_unique<std::vector<MEdge>>();
                extra->push_back(e);
            }
            ++count;
        }

        auto operator[](IndexType index) const
        {
            if (index == 0) return first;
            if (index == 1) return second;
            return (*extra)[index-2];
        }
    };

    std::unordered_map<MEdge, EdgePairContainer, MEdgeHasher> edges;

    struct EdgeUtil
    {
        static MEdge createIndexed(IndexType a, IndexType b) noexcept
        {
            return a < b ? MEdge{a, b} : MEdge{b, a};
        }

        static void add(decltype(edges)& edges,
                        const IndexType* pIndices,
                        IndexType i,
                        IndexType j)
        {
            edges[createIndexed(pIndices[i], pIndices[j])].append({i, j});
        }

        static IndexType getUsages(decltype(edges)& edges,
                                   const IndexType* pIndices,
                                   IndexType i,
                                   IndexType j) noexcept
        {
            return edges[EdgeUtil::createIndexed(pIndices[i], pIndices[j])].count;
        }
    };

    for (size_t i = 0; i+2 < indexCount; i += 3)
    {
        EdgeUtil::add(edges, pIndices, i, i+1);
        EdgeUtil::add(edges, pIndices, i+1, i+2);
        EdgeUtil::add(edges, pIndices, i+2, i);
    }

    std::vector<IndexType> returned;
    for (const auto& [index, epc] : edges)
    {
        if (epc.count == 1 && !includeSingleEdges) continue;
        if (epc.count == 2)
        {
            if (!includeDualEdges) continue;
            if (pIndices[epc[0].first] != pIndices[epc[1].first]) continue;
        }
        if (epc.count > 2 && !includeMultipleEdges) continue;
        returned.push_back(epc.count);
        for (IndexType i = 0; i < epc.count; ++i)
        {
            auto p = epc[i];
            returned.push_back(p.first);
            returned.push_back(p.second);
        }
    }

    return returned;
}

/// Applies edge splits and returns the indices of the vertices to duplicate.
///
/// \param vertexCount Number of vertices *BEFORE* splitting. MUST BE NONZERO!
/// \param pIndices Pointer to the indices. The pointed-to array will be
///                 modified, and may refer past the end of the vertex array
///                 until the vertices this returns the indices of has been
///                 duplicated appropriately.
/// \param splits A split list detailing which edges to split; in the same
///               format as generated by findNonManifoldEdges.
///
/// \returns A vector containing the indices of each vertex to duplicate. These
///          must be duplicated in this order; otherwise pIndices will be
///          invalid.
template <typename IndexType>
[[nodiscard]] std::vector<IndexType>
applySimpleEdgeSplit(size_t vertexCount,
                     IndexType* pIndices,
                     const std::vector<IndexType>& splits)
{
    std::vector<IndexType> toAppend;
    for (size_t i = 0; i < splits.size(); ++i)
    {
        auto count = splits[i];
        i += 2; // Ignore the first two -- they get to keep the non-split edge.
        for (size_t j = 1; j < count; ++j)
        {
            // Yes, ++i is correct here since i in the loop starts at the end of
            // the previous edge.
            toAppend.push_back(pIndices[splits[++i]]);
            pIndices[splits[i]] = vertexCount++;
            toAppend.push_back(pIndices[splits[++i]]);
            pIndices[splits[i]] = vertexCount++;
        }
    }
    return toAppend;
}

/// Splits non-manifold edges.
///
/// \todo Implement a nicer algorithm which handles ears etc. in a much nicer
///       way, also taking connectivity into account.
/// \bug In the current implementation, this splits a vertex into new copies for
///      for EACH of the non-manifold edges it is part of. This should be fixed.
///
/// \param vertexCount Number of vertices. Will be computed from pIndices if 0.
/// \param indexCount Number of indices; must be divisible by 3.
/// \param pIndices Pointer to the index array; will be modified by this.
///
/// \returns A vector of vertex indices, which indicates which vertices should
///          be duplicated in the original vertex data in order to make the
///          modified index array valid. Note that the order is important; the
///          vertices duplicated MUST match this order. So e.g. if this returns
///          {3, 0, 2}, it means that to make this valid, one MUST append vertex
///          #3, then #0 and finally #2 onto the end of the vertex array.
template <typename IndexType>
[[nodiscard]] std::vector<IndexType>
splitNonManifoldEdges(size_t vertexCount,
                      size_t indexCount,
                      IndexType* pIndices)
{
    if (!vertexCount)
    {
        vertexCount = getVertexCountFromIndices(indexCount, pIndices);
    }
    auto nonManifold = findNonManifoldEdges(indexCount,
                                            pIndices,
                                            true,
                                            false,
                                            false);
    return applySimpleEdgeSplit(vertexCount, pIndices, nonManifold);
}

template <typename Prec, typename IndexType>
[[nodiscard]] std::vector<IndexType>
splitNonManifoldEdgesContinuously(size_t vertexCount,
                                  const void* pVertices,
                                  size_t vertexSize,
                                  size_t positionOffset,
                                  size_t indexCount,
                                  IndexType* pIndices)
{
    if (!vertexCount)
    {
        vertexCount = getVertexCountFromIndices(indexCount, pIndices);
    }
    // To fix the nonmanifoldness, we just need to know which edges to re-weld.
    // Suppose we have edges e_1, ..., e_n all between the two vertices v and w.
    // Then, we first split them to separate edges, and let
    // C : E×E → {true, false} be such that C(f,g) = true iff f and g are
    // connected after the splitting. Furthermore let S : E → ℕ give the size
    // (in number of vertices) of the connected component of a given edge, and
    // A : E → ℝ the area of the connected component of a given edge.
    // Then, e_1, ..., e_n should be processed by the following rules (applying
    // the first matching one):
    // * If there exists indices i,j ∈ {1, ..., n}, with i ≠ j such that
    //   C(e_i, e_j) = true, but C(e_i, e_k) = false = C(j, k) for all
    //   k ∈ {1, ..., n}\{i, j}, then e_i and e_j should be merged to a single
    //   edge, removed from the set of edges and these rules applied to the now
    //   smaller set of edges.
    // * Suppose there are two indices i, j ∈ {1, ..., n}, i ≠ j such that for
    //   all k ∈ {1, ..., n}\{i, j}, it holds that
    //   (1)  A(i), A(j) >> A(k), and
    //   (2)  S(i), S(j) >> S(k).
    //   Then e_i and e_j should be merged, and these rules applied to the now
    //   smaller set of edges.
    // * Same rule as above, except only condition (2) should hold.
    // * Same rule as above, except only condition (1) should hold.
    // * Suppose there are two indices i, j ∈ {1, ..., n}, i ≠ j such that for
    //   all k ∈ {1, ..., n}\{i, j}, at least one edge in the connected
    //   component of e_k has no "twin" edge (so no other edge connects the same
    //   two vertices as e_k), but there are no such edges in either of the
    //   connected components of e_i and e_j, then e_i and e_j should be merged
    //   and these rules should be applied to the now smaller set of edges.
    // * No edges should be merged.

    // Do we also want to correct winding order problems? If so, set the dual
    // edge detection parameter below to true.
    // But maybe that's a function in itself.

    auto nonManifold = findNonManifoldEdges(indexCount,
                                            pIndices,
                                            true,
                                            false,
                                            true);
    if (nonManifold.empty()) return {};

    auto vertsToDuplicate = applySimpleEdgeSplit(vertexCount,
                                                 pIndices,
                                                 nonManifold);

    auto fullVertexCount = vertexCount + vertsToDuplicate.size();

    auto components = getConnectedComponents(fullVertexCount,
                                             indexCount,
                                             pIndices,
                                             true);
    size_t largestComponent = 0;
    for (auto c : components)
    {
        largestComponent = std::max(c, largestComponent);
    }

    std::vector<size_t> componentCount(largestComponent+1, 0);
    for (auto c : components)
    {
        ++componentCount[c];
    }

    const void* pPositions = pointeradd(pVertices, positionOffset);

    // TODO: Only compute these if actually necessary.
    // Note: These areas are double the actual areas, because it doesn't really
    // make sense to multiply by 1/2 everywhere just to compare relatively in
    // any case.
    std::vector<Prec> componentAreas(largestComponent+1, Prec(0));
    for (IndexType i = 0; i < indexCount;)
    {
        auto ci = i;
        Vector3<Prec> triverts[3];
        for (IndexType j = 0; j < 3; ++i, ++j)
        {
            size_t index = pIndices[i] < vertexCount
                         ? pIndices[i]
                         : vertsToDuplicate[pIndices[i]-vertexCount];
            auto ptr = pointeradd(pPositions, index * vertexSize);
            triverts[j] = *static_cast<const Vector3<Prec>*>(ptr);
        }
        // 2*area
        auto area2 = length(cross(triverts[2]-triverts[0],
                                  triverts[1]-triverts[0]));
        componentAreas[components[pIndices[ci]]] += area2;
    }

    std::vector<bool> hasSingleEdge(largestComponent+1, false);
    for (size_t i = 0; i < nonManifold.size();)
    {
        auto numCoincidentEdges = nonManifold[i++];
        if (i >= nonManifold.size())
        {
            throw std::logic_error("Corrupt non-manifold array (off-by-one).");
        }
        if (i+2*numCoincidentEdges > nonManifold.size())
        {
            throw std::logic_error("Corrupt non-manifold array (too short).");
        }
        if (!numCoincidentEdges)
        {
            throw std::logic_error("Corrupt non-manifold array (0 CE).");
        }
        if (numCoincidentEdges == 1)
        {
            hasSingleEdge[components[pIndices[nonManifold[i]]]] = true;
        }
        i += 2*numCoincidentEdges;
    }

    std::vector<bool> removedVertices(vertsToDuplicate.size(), false);

    // NOTE: Modifies the nonManifold array!
    for (size_t i = 0; i < nonManifold.size();)
    {
        auto numCoincidentEdges = nonManifold[i++];
        size_t nextI = i+(numCoincidentEdges<<1);

        // The indices this takes should be relative to the current set of non-
        // manifold edges (i.e. within [0, numCoincidentEdges-1]).
        using EdgeCondition = std::function<bool(size_t, size_t)>;

        // Given an edge, this gives the indices into the *triangulation* of the
        // edge's vertices.
        auto getEdgeIndices = [&nonManifold, pIndices, i]
        (size_t edgeNumber) -> std::pair<size_t, size_t>
        {
            auto start = i+(edgeNumber<<1);
            auto end = start+1;
            return {pIndices[nonManifold[start]], pIndices[nonManifold[end]]};
        };

        // If this returns true, there is an edge pair satisfying the given
        // condition, and that is then hoisted to the beginning of the current
        // position into the edge array.
        // Note: This does O(n²) evaluations of the condition, with n being the
        // number of edges in the current group (which is usually a *very* small
        // number, so complexity isn't really important).
        auto findEdgePairSatisfying = [&numCoincidentEdges, &nonManifold, i]
        (const EdgeCondition& condition)
        {
            for (size_t j = 0; j < numCoincidentEdges; ++j)
            {
                for (size_t k = j+1; k < numCoincidentEdges; ++k)
                {
                    if (condition(j, k))
                    {
                        auto ji = i+(j<<1);
                        auto ki = i+(k<<1);
                        std::swap(nonManifold[i], nonManifold[ji]);
                        std::swap(nonManifold[i+1], nonManifold[ji+1]);
                        std::swap(nonManifold[i+2], nonManifold[ki]);
                        std::swap(nonManifold[i+3], nonManifold[ki+1]);
                        return true;
                    }
                }
            }
            return false;
        };

        // Determine if the two edges are of the same component and if so,
        // whether they are the only ones in that component.
        auto inUniqueComponent = [&](size_t firstEdge, size_t secondEdge)
        {
            auto vIdx0 = getEdgeIndices(firstEdge).first;
            auto vIdx1 = getEdgeIndices(secondEdge).first;
            if (components[vIdx0] != components[vIdx1]) return false;
            for (size_t k = 0; k < numCoincidentEdges; ++k)
            {
                if (k == firstEdge || k == secondEdge) continue;
                if (components[getEdgeIndices(k).first]
                    == components[vIdx0])
                {
                    return false;
                }
            }
            return true;
        };

        auto hasLargestComponentCount = [&](size_t firstEdge, size_t secondEdge)
        {
            auto firstComp = components[getEdgeIndices(firstEdge).first];
            auto secondComp = components[getEdgeIndices(secondEdge).first];
            auto cc = std::max(componentCount[firstComp],
                               componentCount[secondComp]);
            for (size_t k = 0; k < numCoincidentEdges; ++k)
            {
                if (k == firstEdge || k == secondEdge) continue;
                if (componentCount[components[getEdgeIndices(k).first]] > 5 * cc)
                {
                    return false;
                }
            }
            return true;
        };

        auto hasLargestComponentArea = [&](size_t firstEdge, size_t secondEdge)
        {
            auto firstComp = components[getEdgeIndices(firstEdge).first];
            auto secondComp = components[getEdgeIndices(secondEdge).first];
            auto ca = std::max(componentAreas[firstComp],
                               componentAreas[secondComp]);
            for (size_t k = 0; k < numCoincidentEdges; ++k)
            {
                if (k == firstEdge || k == secondEdge) continue;
                if (componentAreas[components[getEdgeIndices(k).first]] > 5 * ca)
                {
                    return false;
                }
            }
            return true;
        };

        auto hasBestComponent = [&](size_t firstEdge, size_t secondEdge)
        {
            return hasLargestComponentCount(firstEdge, secondEdge)
                   && hasLargestComponentArea(firstEdge, secondEdge);
        };

        auto onlyPairWithoutSingleEdges =
        [&](size_t firstEdge, size_t secondEdge)
        {
            if (hasSingleEdge[components[getEdgeIndices(firstEdge).first]]
                || hasSingleEdge[components[getEdgeIndices(secondEdge).first]])
            {
                return false;
            }
            for (size_t k = 0; k < numCoincidentEdges; ++k)
            {
                if (k == firstEdge || k == secondEdge) continue;
                if (!hasSingleEdge[components[getEdgeIndices(k).first]])
                {
                    return false;
                }
            }
            return true;
        };

        while (numCoincidentEdges >= 2)
        {
            if (findEdgePairSatisfying(inUniqueComponent)
                || findEdgePairSatisfying(hasBestComponent)
                || findEdgePairSatisfying(hasLargestComponentCount)
                || findEdgePairSatisfying(hasLargestComponentArea)
                || findEdgePairSatisfying(onlyPairWithoutSingleEdges))
            {
                auto& toMerge0 = pIndices[nonManifold[i+2]];
                auto& toMerge1 = pIndices[nonManifold[i+3]];
                if (toMerge0 < vertexCount || toMerge1 < vertexCount)
                {
                    throw std::logic_error("Invalid edge split: Index too "
                                           "small.");
                }
                // Note: nonManifold[0] and nonManifold[1] can be either
                // original indices or nonoriginal ones.

                vertsToDuplicate[toMerge0-vertexCount] = fullVertexCount;
                vertsToDuplicate[toMerge1-vertexCount] = fullVertexCount;

                // Note: Merging like this is okay since findNonManifoldEdges
                // explicitly ensures that they are laid out in the same
                // direction.
                removedVertices.at(toMerge0-vertexCount) = true;
                removedVertices[toMerge1-vertexCount] = true;
                pIndices[nonManifold[i+2]] = pIndices[nonManifold[i+0]];
                pIndices[nonManifold[i+3]] = pIndices[nonManifold[i+1]];
                toMerge0 = pIndices[nonManifold[i+0]];
                toMerge1 = pIndices[nonManifold[i+0]];
                i += 4;
                numCoincidentEdges -= 2;
            }
            else break;
        }
        i = nextI;
    }

    // Now we can erase the vertices from vertsToDuplicate which weren't
    // necessary after all. To do that, we create a relocation map, mapping
    // an old index to a new one.
    constexpr auto invalidIndex = std::numeric_limits<size_t>::max();
    std::vector<size_t> relocations(vertsToDuplicate.size(), invalidIndex);

    size_t numToReturn = 0;
    for (size_t i = 0; i < vertsToDuplicate.size(); ++i)
    {
        if (vertsToDuplicate[i] != fullVertexCount)
        {
            auto newPosition = relocations[i] = numToReturn++;
            vertsToDuplicate[newPosition] = vertsToDuplicate[i];
        }
    }

    // Simultaneously we have also compacted the vertsToDuplicate map.
    vertsToDuplicate.resize(numToReturn);

    // Now we'll apply the relocations to the indices; note that we need only do
    // so for any of the non-manifold edges, and since we have only swapped
    // around the indices of the edges in a bijective manner (using std::swap
    // for all elements).
    for (size_t i = 0; i < nonManifold.size();)
    {
        IndexType groupSize = nonManifold[i++] << IndexType{1};
        for (size_t k = 0; k < groupSize; ++k)
        {
            auto& currIndex = pIndices[nonManifold[i+k]];
            if (currIndex >= vertexCount)
            {
                currIndex = relocations.at(currIndex-vertexCount);
            }
        }
        i += groupSize;
    }

    return vertsToDuplicate;
}

/// Splits non-manifold edges.
///
/// This is simply a nicer wrapper around the other function of the same name,
/// which doesn't take a vertex resizing function but rather just returns a list
/// of what is essentially copying commands.
///
/// \param vertexCount Number of vertices (initially).
/// \param pVertices Pointer to the vertices.
/// \param vertexSize The size of each vertex in bytes.
/// \param indexCount Number of indices; must be divisible by 3.
/// \param pIndices Pointer to the first index. WILL BE MODIFIED!
/// \param vertexResizer Something callable which should take the number of
///                      vertices to resize to (in total), and return a pointer
///                      to the first vertex.
template <typename IndexType, typename VertexResizeFunction>
void splitNonManifoldEdges(size_t vertexCount,
                           void* pVertices,
                           size_t vertexSize,
                           size_t indexCount,
                           IndexType* pIndices,
                           const VertexResizeFunction& vertexResizer)
{
    auto toAdd = splitNonManifoldEdges(vertexCount, indexCount, pIndices);
    if (toAdd.empty()) return;
    pVertices = vertexResizer(vertexCount + toAdd.size());
    auto appendAt = pointeradd(pVertices, vertexSize*vertexCount);
    for (auto elem : toAdd)
    {
        std::memcpy(appendAt,
                    pointeradd(pVertices, vertexSize*elem),
                    vertexSize);
        appendAt = pointeradd(appendAt, vertexSize);
    }
}

/// Splits non-manifold edges continuously.
///
/// This is simply a nicer wrapper around the other function of the same name,
/// which doesn't take a vertex resizing function but rather just returns a list
/// of what is essentially copying commands.
///
/// \tparam Prec The underlying data type for the positions (usually float or
///              double, though non-floating point is supported).
/// \tparam IndexType The type of the indices.
/// \param vertexCount Number of vertices (initially).
/// \param pVertices Pointer to the vertices.
/// \param vertexSize The size of each vertex in bytes.
/// \param positionOffset The offset into each vertex where the position is
///                       located.
/// \param indexCount Number of indices; must be divisible by 3.
/// \param pIndices Pointer to the first index. WILL BE MODIFIED!
/// \param vertexResize Something callable which should take the number of
///                     vertices to resize to (in total), and return a pointer
///                     to the first vertex.
template <typename Prec, typename IndexType, typename VertexResizeFunction>
void splitNonManifoldEdgesContinuously(size_t vertexCount,
                                       void* pVertices,
                                       size_t vertexSize,
                                       size_t positionOffset,
                                       size_t indexCount,
                                       IndexType* pIndices,
                                       const VertexResizeFunction& vertexResize)
{
    auto toAdd = splitNonManifoldEdgesContinuously<Prec>(vertexCount,
                                                         pVertices,
                                                         vertexSize,
                                                         positionOffset,
                                                         indexCount,
                                                         pIndices);
    if (toAdd.empty()) return;
    pVertices = vertexResize(vertexCount + toAdd.size());
    auto appendAt = pointeradd(pVertices, vertexSize*vertexCount);
    for (auto elem : toAdd)
    {
        std::memcpy(appendAt,
                    pointeradd(pVertices, vertexSize*elem),
                    vertexSize);
        appendAt = pointeradd(appendAt, vertexSize);
    }
}

/// Finds all edges of a triangular mesh not shared by any other triangles.
///
/// \param indexCount Number of indices in the mesh; must be divisble by 3.
/// \param pIndices Pointer to the index array.
///
/// \returns A vector of index pairs, each of which representing a directed edge
///          that only occurs in the mesh once (and its reversal DOES NOT
///          appear either). The indices refers to the *index* array; NOT to the
///          corresponding vertex array (if any).
template <typename IndexType>
[[nodiscard]] std::vector<std::pair<IndexType, IndexType>>
findSingleEdges(size_t indexCount,
                const IndexType* pIndices)
{
    auto nonManifold = findNonManifoldEdges(indexCount, pIndices,
                                            false, false, true);
    if (nonManifold.empty()) return {};
    std::vector<std::pair<IndexType, IndexType>> singleEdges;
    for (size_t i = 0; i < nonManifold.size();)
    {
        auto numEdges = nonManifold[i++];
        for (size_t j = 0; j < numEdges; ++j)
        {
            auto eFrom = nonManifold[i++];
            auto eTo = nonManifold[i++];
            singleEdges.emplace_back(eFrom, eTo);
        }
    }
    return singleEdges;
}

/// Finds all outer edge loops of a given triangular mesh.
///
/// \param indexCount Number of indices in the mesh; must be divisble by 3.
/// \param pIndices Pointer to the index array.
///
/// \remark Every single contour this returns is *closed* and as thus, the first
///         and last index of every contour is the same.
/// \remark A vertex can occur multiple times in the same contour, and this is
///         not a problem per se, though it only occurs if two parts of the mesh
///         is connected through a vertex (and not an edge, as a manifold mesh
///         would do).
/// \remark Note that the returned indices indexes into the *indices* and not
///         the corresponding vertex array.
///
/// \returns A vector of contours / outer edge loops. Each entry is a triplet of
///          indices (a, b, c) such that the edge in question is from
///          pIndices[a] to pIndices[b]; while pIndices[c] gives the third
///          index of the triangle that the edge in question is part of.
///          There is no guarantee that each edge loop is closed; sometimes it
///          is possible to have a single exposed outer edge without it being
///          part of a loop (this is only for non-manifold meshes, though).
template <typename IndexType>
[[nodiscard]] std::vector<std::vector<IndexType>>
extractContours(size_t indexCount,
                const IndexType* pIndices)
{
    // Ensure we have triangles!
    if (indexCount % 3) indexCount -= indexCount % 3;

    auto singleEdges = findSingleEdges(indexCount, pIndices);
    if (singleEdges.empty()) return {};

    std::unordered_multimap<IndexType, priv::Edge<IndexType>> connections;
    IndexType maxIndex{0};
    for (auto [from, to] : singleEdges)
    {
        connections.emplace(pIndices[from], priv::Edge<IndexType>{from, to});
        maxIndex = std::max(std::max(maxIndex, from), to);
    }

    std::vector<std::vector<IndexType>> contours;
    std::vector<bool> visited(size_t(maxIndex) + 1, false);

    for (auto [start, startNext] : singleEdges)
    {
        if (visited[start]) continue;
        contours.push_back({});
        auto& currContour = contours.back();
        priv::Edge<IndexType> currEdge = {start, startNext};
        while (!visited[currEdge.first])
        {
            visited[currEdge.first] = true;
            currContour.push_back(currEdge.first);
            currContour.push_back(currEdge.second);
            auto remainder = currEdge.first % 3 + currEdge.second % 3;
            auto otherIndex = 3 * (currEdge.first / 3) + (3 - (remainder));
            currContour.push_back(otherIndex);

            auto nextVIndex = pIndices[currEdge.second];

            auto [connBegin, connEnd] = connections.equal_range(nextVIndex);
            for (auto it = connBegin; it != connEnd; ++it)
            {
                if (!visited[it->second.first])
                {
                    currEdge = it->second;
                    break;
                }
            }
        }
    }

    return contours;
}

/// Finds cavities in a contour (i.e. where there's no local convexity).
///
/// This can be used to find indents in the outer edge of a mesh for subsequent
/// repair or other diagnostic. To see what this means, consider a box like the
/// following:
/// 0   1
/// |\ /|
/// | 2 |
/// 3---4
/// Supposing CCW winding, this contour is 0→3→4→1→2 (or any cyclic shift
/// thereof), and one concave vertex; namely #2. This will return 1→2→0;
/// indicating that these edges form a concave indent.
///
/// \remark While this seems obvious to use extractContours to extract the
///         contours; one must take care: extractContours returns indices into
///         the index array; while this expects indices into the vertex array.
///         Thus, one can feed any contour from extractContours into this by
///         simply performing the substitution c ← pIndices[c] for all c in the
///         returned contours.
///
/// \tparam Prec Data type used for the positions of the vertices; since
///              positions must be in ℝ³; then this should be the actual type
///              that the 3d vector uses for each component. Usually float, but
///              can be double.
/// \param pVertices Pointer to the vertices of the mesh.
/// \param vertexSize Size (in bytes) of each vertex.
/// \param positionOffset Offset (in bytes) of the position attribute within
///                       each vertex.
/// \param indexCount Number of indices in the given contour.
/// \param pIndices Pointer to the indices of the contour. Note that this should
///                 take the indices as *triplets*; for each triplet the first
///                 two elements gives the edge's from and to vertex; and the
///                 last element of the triplet gives the final index of the
///                 last vertex of the triangle this edge originates from.
///                 Note that they must be contiguous.
/// \param minConcaveAngle Minimal angle between two edges to consider for
///                        concavity.
/// \param epsilon Epsilon to use for degeneracy computation.
///
/// \returns Vectors of concavity strips -- each of these is a number of indices
///          i_0, ..., i_n into pVertices such that the line strip
///          pVertices[i_0] → pVertices[i_1] → ... → pVertices[i_n] gives a
///          strip of purely concave edges; however only the 'middle' indices
///          are of actual, concave vertices.
template <typename Prec, typename IndexType>
[[nodiscard]] std::vector<std::vector<IndexType>>
findContourCavities(const void* pVertices,
                    size_t vertexSize,
                    size_t positionOffset,
                    size_t indexCount,
                    const IndexType* pIndices,
                    Prec minConcaveAngle = Prec(0),
                    Prec epsilon = 50 * math::Constant<Prec>::epsilon)
{
    // Ensure we have triangles!
    if (indexCount % 3) indexCount -= indexCount % 3;
    auto triCount = indexCount / 3;
    pVertices = pointeradd(pVertices, positionOffset);

    if (triCount <= 1) return {};

    auto minCAdotp = std::cos(std::max(Prec(0), minConcaveAngle));
    auto sqEps = epsilon * epsilon;

    auto getPosition = [pVertices, pIndices, vertexSize](size_t i)
    {
        using Vector3 = const Vector3<Prec>;
        auto index = pIndices[i];
        return *reinterpret_cast<const Vector3*>(pointeradd(pVertices,
                                                            index*vertexSize));
    };

    auto getTriangleNormal =
    [&getPosition](size_t i0, size_t i1, size_t i2) -> Vector3<Prec>
    {
        auto v0 = getPosition(i0);
        auto v1 = getPosition(i1);
        auto v2 = getPosition(i2);
        return cross(v1-v0, v2-v0);
    };

    auto isConcaveEnough =
    [&getPosition, pIndices, sqEps, minCAdotp]
    (size_t i0, size_t i1, size_t j0, size_t j1, const Vector3<Prec>& normal)
    -> bool
    {
        if (pIndices[i1] != pIndices[j0])
        {
            throw std::runtime_error("Uncontiguous edge; cannot get angle!");
        }
        auto shared = getPosition(i1);
        auto edgeI = getPosition(i0) - shared;
        auto edgeJ = getPosition(j1) - shared;
        auto eiLen2 = squaredLength(edgeI);
        auto ejLen2 = squaredLength(edgeJ);
        if (eiLen2 < sqEps || ejLen2 < sqEps) return false; // One is too small.
        edgeI /= std::sqrt(eiLen2);
        edgeJ /= std::sqrt(ejLen2);

        auto crossed = cross(edgeI, edgeJ);
        if (squaredLength(crossed) < sqEps) return false; // Parallel.
        if (dot(normal, crossed) < Prec(0)) return false; // Convex.

        return std::abs(dot(edgeI, edgeJ)) < minCAdotp;
    };

    std::vector<bool> isCavity(triCount, false);
    auto firstCavity = triCount;
    for (size_t i = 0; i < triCount; ++i)
    {
        auto nextI = (i+1)%triCount;
        auto normal = getTriangleNormal(3*i, 3*i+1, 3*i+2);
        isCavity[i] = isConcaveEnough(3*i, 3*i+1,
                                      3*nextI, 3*nextI+1, normal);
        if (isCavity[i]) firstCavity = std::min(firstCavity, i);
    }

    if (firstCavity == triCount) return {};
    std::rotate(isCavity.begin(), isCavity.begin()+firstCavity, isCavity.end());
    bool prev = true;

    std::vector<std::vector<IndexType>> cavities;

    std::vector<IndexType> current;
    for (size_t i = 0; i < isCavity.size(); ++i)
    {
        if (prev == isCavity[i])
        {
            if (prev) current.push_back(i);
        }
        else if (prev)
        {
            current.push_back(i);
            cavities.emplace_back(std::move(current));
            current.clear();
        }
        prev = isCavity[i];
    }
    if (!current.empty()) cavities.emplace_back(std::move(current));
    for (auto& cavity : cavities)
    {
        std::vector<IndexType> actualIndices;
        size_t lastIndex = 0;
        for (auto tri : cavity)
        {
            auto iStart = 3*((tri + firstCavity) % triCount);
            actualIndices.push_back(pIndices[iStart]);
            lastIndex = iStart;
        }
        actualIndices.push_back(pIndices[lastIndex+1]);
        std::swap(actualIndices, cavity);
    }

    return cavities;
}

/// Fill in concavities/indents in rims of a triangular mesh.
///
/// For a more thorough description of concavities/indents, see the
/// documentation of findContourCavities.
///
/// \tparam Prec Data type used for the positions of the vertices; since
///              positions must be in ℝ³; then this should be the actual type
///              that the 3d vector uses for each component. Usually float, but
///              can be double.
/// \param pVertices Pointer to the vertices of the mesh.
/// \param vertexSize Size (in bytes) of each vertex.
/// \param positionOffset Offset (in bytes) of the position attribute within
///                       each vertex.
/// \param indexCount Number of indices in the given contour.
/// \param pIndices Pointer to the indices of the contour. Note that this should
///                 take the indices as *triplets*; for each triplet the first
///                 two elements gives the edge's from and to vertex; and the
///                 last element of the triplet gives the final index of the
///                 last vertex of the triangle this edge originates from.
///                 Note that they must be contiguous.
/// \param maxConcaveVertices The maximal number of vertices in a given concave
///                           section (excluding the end points, so only the
///                           number of vertices with actual concave angles)
///                           that should be filled in.
/// \param minConcaveAngle Minimal angle between two edges to consider for
///                        concavity.
/// \param epsilon Epsilon to use for degeneracy computation.
///
/// \returns A vector of indices that must be appended to the index array of
///          the original (triangular) mesh in order to fill in the concavities.
template <typename Prec, typename IndexType>
[[nodiscard]] std::vector<IndexType>
fillConcavities(const void* pVertices,
                size_t vertexSize,
                size_t positionOffset,
                size_t indexCount,
                const IndexType* pIndices,
                size_t maxConcaveVertices = std::numeric_limits<size_t>::max(),
                Prec minConcaveAngle = Prec(0),
                Prec epsilon = 50 * math::Constant<Prec>::epsilon)
{
    auto contours = extractContours(indexCount, pIndices);

    std::vector<IndexType> output;
    for (auto& contour : contours)
    {
        for (auto& c : contour)
        {
            c = pIndices[c];
        }
        auto cavities = findContourCavities(pVertices,
                                            vertexSize,
                                            positionOffset,
                                            contour.size(),
                                            contour.data(),
                                            minConcaveAngle,
                                            epsilon);
        for (const auto& cavity : cavities)
        {
            if (cavity.size() < 3)
            {
                throw std::logic_error("Impossibly sized cavity.");
            }
            if (cavity.size() - 2 > maxConcaveVertices) continue;
            for (size_t i = 1; i + 1 < cavity.size(); ++i)
            {
                output.push_back(cavity.front());
                output.push_back(cavity.back());
                output.push_back(cavity[i]);
            }
        }
    }
    return output;
}


} // namespace kraken::geometry

#endif // KRAKEN_GEOMETRY_MESHREPAIR_HPP_INCLUDED
