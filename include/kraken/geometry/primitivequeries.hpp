#ifndef KRAKEN_GEOMETRY_PRIMITIVEQUERIES_HPP_INCLUDED
#define KRAKEN_GEOMETRY_PRIMITIVEQUERIES_HPP_INCLUDED

#include "kraken/math/constants.hpp"
#include <cstddef>
#include <kraken/geometry/boundingvolumes.hpp>
#include <kraken/geometry/primitives.hpp>

#include <cmath>
#include <limits>
#include <type_traits>
#include <utility>

namespace kraken::geometry
{

/// Computes an unnormalised normal of a triangle, pointing outwards.
///
/// \remark The triangle is assumed to be oriented counter-clockwisely; if not
///         one can reverse the order by swapping two vertices.
///
/// \param tri A triangle.
///
/// \returns A normal; if the triangle is degenerate, the zero vector.
template <typename Prec>
Vector3<Prec> computeNormal(const Triangle3d<Prec>& tri) noexcept
{
    return cross(tri.b-tri.a, tri.c-tri.a);
}

/// Computes an unnormalised normal of a CCW polygon, pointing outwards.
///
/// The normal is computed using Newell's method.
///
/// \param pPolygon Pointer to the first vertex of the polygon.
/// \param polygonSize Number of elements in the polygon. MUST BE NON-ZERO!
///
/// \returns A normal; if the polygon is degenerate, the zero vector.
template <typename Prec>
Vector3<Prec> computeNormal(const Vector3<Prec>* pPolygon,
                            size_t polygonSize) noexcept
{
    auto prev = pPolygon[polygonSize-1];
    Vector3<Prec> normal{Prec(0), Prec(0), Prec(0)};
    for (size_t i = 0; i < polygonSize; ++i)
    {
        auto curr = pPolygon[i];
        normal.x += (prev.y - curr.y) * (prev.z + curr.z);
        normal.y += (prev.z - curr.z) * (prev.x + curr.x);
        normal.z += (prev.x - curr.x) * (prev.y + curr.y);
        prev = curr;
    }
    return normal;
}

/// Finds the point on a line (segment) closest to the given point.
///
/// Note that 'minPar' and 'maxPar' can each be either finite or infinite; they
/// are solely treated as delimiters for the line, which can thus either be
/// interpreted as a line, ray or line segment. The only restriction is that
/// minPar ≤ maxPar.
///
/// \remark The line must have a normalised direction, otherwise this will yield
///         a wrong result.
///
/// \param line The underlying line to find the closest point on.
/// \param point The point that should be projected onto the line (segment).
/// \param minPar Optionally, the minimum parameter delimiting the line.
/// \param maxPar Optionally, the maximum parameter delimiting the line.
///
/// \returns The closest point to the line/ray/line segment.
template <typename Prec>
Vector3<Prec> closestPoint(Line3d<Prec> line,
                           Vector3<Prec> point,
                           Prec minPar = -std::numeric_limits<Prec>::max(),
                           Prec maxPar = std::numeric_limits<Prec>::max()) noexcept
{
    auto par = dot(point-line.origin, line.direction);
    return line.origin + math::clamp(par, minPar, maxPar) * line.direction;
}

/// Finds the point on a line (segment) closest to the given point.
///
/// Note that 'minPar' and 'maxPar' can each be either finite or infinite; they
/// are solely treated as delimiters for the line, which can thus either be
/// interpreted as a line, ray or line segment. The only restriction is that
/// minPar ≤ maxPar.
///
/// \remark This function, as opposed to 'closestPoint', does allow the line not
///         to be normalised; making this useful for one-off queries for lines
///         it doesn't make sense to normalise.
///
/// \param line The underlying line to find the closest point on.
/// \param point The point that should be projected onto the line (segment).
/// \param minPar Optionally, the minimum parameter delimiting the line.
/// \param maxPar Optionally, the maximum parameter delimiting the line.
///
/// \returns The closest point to the line/ray/line segment.
template <typename Prec>
Vector3<Prec> closestPointUnnormalised(Line3d<Prec> line,
                                       Vector3<Prec> point,
                                       Prec minPar = -std::numeric_limits<Prec>::max(),
                                       Prec maxPar = std::numeric_limits<Prec>::max()) noexcept
{
    auto par = dot(point-line.origin, line.direction)
               / squaredLength(line.direction);
    return line.origin + math::clamp(par, minPar, maxPar) * line.direction;
}

/// Finds the closest point on a given triangle.
///
/// \remark There are practically no requirements for this function to work; the
///         only real requirements is that all points have finite norm. In any
///         such case, projection cannot fail.
///
/// \param triangle The triangle to project the point onto.
/// \param point Any point in ℝ³.
///
/// \returns The closest point to 'point' on 'triangle', as measured by the
///          usual Euclidean metric.
template <typename Prec>
Vector3<Prec>
closestPoint(Triangle3d<Prec> triangle, Vector3<Prec> point) noexcept;

/// Finds the closest point on a given tetrahedron.
///
/// \remark There are practically no requirements for this function to work; the
///         only real requirements is that all points have finite norm. In any
///         such case, projection cannot fail.
///
/// \param tetrahedron The tetrahedron to project the point onto.
/// \param point Any point in ℝ³.
///
/// \returns The closest point to 'point' on 'triangle', as measured by the
///          usual Euclidean metric.
template <typename Prec>
Vector3<Prec>
closestPoint(Tetrahedron<Prec> tetrahedron, Vector3<Prec> point) noexcept;

/// Finds the closest point on an axis-aligned bounding box.
///
/// \remark This has no requirements other than the point being in ℝ³ (and thus
///         not being nan, infinite or ugly stuff like that), and the bounding
///         box being valid.
///
/// \remark This treats the box as solid, and so if the point is inside the box,
///         this will simply return the point.
///
/// \param aabb An axis-aligned bounding box.
/// \param point The point to project onto the box.
///
/// \returns The point on the AABB closest to 'point'.
template <typename Prec>
Vector3<Prec>
closestPoint(Aabb<Prec> aabb, Vector3<Prec> point) noexcept
{
    for (size_t i = 0; i < 3; ++i)
    {
        point[i] = std::max(aabb.center[i] - aabb.halfExtents[i],
                            std::min(aabb.center[i] + aabb.halfExtents[i],
                                     point[i]));
    }
    return point;
}

/// Computes the parameters on the two lines where they are closest.
///
/// \remark The lines should not be parallel (or very close to); if so, the
///         returned parameters are (∞,∞); even if the lines are opposite.
///
/// \param a An infinite line.
/// \param b Another infinite line.
/// \param epsilon Epsilon to use for parallelity determination.
///
/// \returns Their closest point.
template <typename Prec>
std::pair<Prec, Prec>
closestPointParameters(Line3d<Prec> a,
                       Line3d<Prec> b,
                       Prec epsilon = 50*math::Constant<Prec>::epsilon) noexcept;

/// Computes the parameters on the two lines where they are closest.
///
/// \remark The lines should not be parallel (or very close to); if so, the
///         returned parameters are (∞,∞); even if the lines are opposite.
/// \remark This version allows unnormalised (but not zero) direction vectors;
///         which can be useful for intersections of transient line segments.
///
/// \param a An infinite line.
/// \param b Another infinite line.
/// \param epsilon Epsilon to use for parallelity determination.
///
/// \returns Their closest point.
template <typename Prec>
std::pair<Prec, Prec>
closestPointParametersUnnormalised(Line3d<Prec> a,
                                   Line3d<Prec> b,
                                   Prec epsilon = 50*math::Constant<Prec>::epsilon) noexcept;

/// Computes the closest points on two wires of line segments.
///
/// This is done by simply testing all wires of the line segments against all
/// others; resulting in a runtime of O(n·m), where n is the number of vertices
/// of the first wire, and m is those of the second polygon.
///
/// \param pFirstVertices Pointer to the first vertex of the first polygon.
/// \param firstVertexCount Number of vertices in the first polygon.
/// \param pSecondVertices Pointer to the first vertex of the second polygon.
/// \param secondVertexCount Number of vertices in the second polygon.
/// \param firstClosed Whether to close the first wire by connecting the last
///                    and first point.
/// \param secondClosed Whether to close the second wire by connecting its last
///                     and first point.
/// \param epsilon Epsilon to use for parallelity detection.
///
/// \returns A point on each wire attaining the minimal distance between the two
///          wires. The first returned point lies on the first wire, and the
///          second point on the second wire.
template <typename Prec>
std::pair<Vector3<Prec>, Vector3<Prec>>
closestPointsOnWires(const Vector3<Prec>* pFirstVertices,
                     size_t firstVertexCount,
                     const Vector3<Prec>* pSecondVertices,
                     size_t secondVertexCount,
                     bool firstClosed = true,
                     bool secondClosed = true,
                     Prec epsilon = 20*math::Constant<Prec>::epsilon);

/// Finds the closest points on the two given lines.
///
/// \remark If the lines are parallel, the result is implementation-defined.
///
/// \param a An infinite line.
/// \param b Another infinite line.
///
/// \returns The two points minimising the distance between the lines.
template <typename Prec>
std::pair<Vector3<Prec>, Vector3<Prec>>
closestPoints(Line3d<Prec> a, Line3d<Prec> b) noexcept
{
    auto [first, second] = closestPointParameters(a, b);
    return {a.origin + first * a.direction,
            b.origin + second * b.direction};
}

/// Finds the closest points on the two unnormalised line segments.
///
/// \remark If the lines are parallel, the result is implementation-defined.
///
/// \param a An infinite line.
/// \param aMin Minimum parameter for the first line.
/// \param aMax Maximum parameter for the first line.
/// \param b Another infinite line.
/// \param bMin Minimum parameter for the second line.
/// \param bMax Maximum parameter for the second line.
///
/// \returns The two points minimising the distance between the line segments
///          defined by the lines and their parameter restriction.
template <typename Prec>
std::pair<Vector3<Prec>, Vector3<Prec>>
closestPointsUnnormalised(Line3d<Prec> a, Prec aMin, Prec aMax,
                          Line3d<Prec> b, Prec bMin, Prec bMax) noexcept
{
    auto [first, second] = closestPointParametersUnnormalised(a, b);
    return {a.origin + std::max(std::min(first, aMax), aMin) * a.direction,
            b.origin + std::max(std::min(second, bMax), bMin) * b.direction};
}

/// \todo Document this.
template <typename Prec>
std::pair<Vector3<Prec>, Vector3<Prec>>
closestPoints(Triangle3d<Prec> a, Triangle3d<Prec> b); // noexcept

/// Computes the orthogonal projection of the point onto the plane.
///
/// \param plane The plane to project onto.
/// \param point The point to project onto the plane.
///
/// \returns The closest point on the plane.
template <typename Prec>
Vector3<Prec> closestPoint(Plane<Prec> plane, Vector3<Prec> point) noexcept
{
    return point - dot(plane.normal, point - plane.origin) * plane.normal;
}

/// Determines if a point is inside a convex polygon in three-dimneions.
///
/// \remark Care should be exercised around edges, as a point lying *very* close
///         to an edge may be mistakenly considered on the opposite side.
///         However, detection of this is easy -- one simply checks the
///         projection of the point onto the polygon contour, and if it is
///         sufficiently close, this function doesn't need to be called. Note,
///         though, that this function does *NOT* perform that test, but is
///         quite a bit more naïve.
///
/// \param point The point to test.
/// \param normal The polygon normal.
/// \param polygonBegin Iterator to the first vertex of the polygon.
/// \param polygonEnd Iterator to the end of the polygon; the polygon is assumed
///                   to be constructed from the points as:
///        *fromBegin → *(fromBegin+1) → ... → *(polygonEnd-1) → *fromBegin
///
/// \returns True if the point is inside the polygon, otherwise false.
template <typename Prec, typename It>
bool isInsideConvexPolygon(Vector3<Prec> point,
                           Vector3<Prec> normal,
                           It polygonBegin,
                           It polygonEnd)
{
    int test = 0;
    auto it = polygonBegin;
    auto nextIt = it;
    ++nextIt;
    bool atEnd = false;
    while (!atEnd)
    {
        if (nextIt == polygonEnd)
        {
            nextIt = polygonBegin;
            atEnd = true;
        }

        auto from = *it;
        auto to = *nextIt;

        auto dotp = dot(cross(point-from, to-from), normal);
        test |= 1<<(int)(dotp > Prec(0));

        it = nextIt;
        ++nextIt;
    }
    return test <= 2;
}

/// Determines if a point is inside a convex polygon.
///
/// \remark Care should be exercised around edges, as a point lying *very* close
///         to an edge may be mistakenly considered on the opposite side.
///         However, detection of this is easy -- one simply checks the
///         projection of the point onto the polygon contour, and if it is
///         sufficiently close, this function doesn't need to be called. Note,
///         though, that this function does *NOT* perform that test, but is
///         quite a bit more naïve.
///
/// \param point The point to test.
/// \param polygonBegin Iterator to the first vertex of the polygon.
/// \param polygonEnd Iterator to the end of the polygon; the polygon is assumed
///                   to be constructed from the points as:
///        *fromBegin → *(fromBegin+1) → ... → *(polygonEnd-1) → *fromBegin
///
/// \returns True if the point is inside the polygon, otherwise false.
template <typename Prec, typename It>
bool isInsideConvexPolygon(Vector2<Prec> point,
                           It polygonBegin,
                           It polygonEnd)
{
    int test = 0;
    auto it = polygonBegin;
    auto nextIt = it;
    ++nextIt;
    bool atEnd = false;
    while (!atEnd && test <= 2)
    {
        if (nextIt == polygonEnd)
        {
            nextIt = polygonBegin;
            atEnd = true;
        }

        auto from = *it;
        auto to = *nextIt;

        auto dotp = dot(point-from, perp(to-from));
        test |= 1<<(int)(dotp > Prec(0));

        it = nextIt;
        ++nextIt;
    }
    return test <= 2;
}

/// This computes the area of a polygon using the Gaussian formula (shoelace).
///
/// This can be used to compute the area of any simple polygon, i.e. one where
/// there are no self-intersections in the edges. Note that the area is signed,
/// and the sign gives the winding order: A positive area corresponds to a
/// counterclockwise winding, while a negative corresponds to a clockwise
/// winding.
/// This can also be used for non-simple polygons, however the interpretation
/// here is murkier. The resulting area is now the sum of the area of the
/// various enclosed areas, each scaled by their winding number. Roughly, this
/// means that every counter-clockwisely wound polygon has its area added,
/// while every clockwisely wound polygon has its area subtracted.
///
/// \remark Read the description carefully. This has several pitfalls/caveats.
///
/// \param begin Forward iterator to the first polygon vertex.
/// \param end Iterator to the end of the polygon vertices.
///
/// \returns A signed area with meaning defined as in the description above.
template <typename It>
auto computeGaussArea(It begin, It end)
-> decltype(begin->x)
{
    decltype(begin->x) area{0};
    auto nextIt = begin;
    ++nextIt;
    for (auto it = begin; nextIt != end; it = nextIt++)
    {
        area += it->x*nextIt->y - nextIt->x*it->y;
    }
    area += nextIt->x*begin->y - begin->x*nextIt->y;
    return area;
}

/// Computes the midpoint of a polygon.
///
/// \param begin Iterator to first vertex.
/// \param end Iterator to last vertex.
///
/// \returns The midpoint.
template <typename It>
auto polygonCentre(It begin, It end) noexcept ->
typename std::remove_cv_t<std::remove_reference_t<decltype(*begin)>>
{
    using Vector = typename std::remove_cv_t<std::remove_reference_t<decltype(*begin)>>;
    using Prec = typename std::remove_cv_t<std::remove_reference_t<decltype(*begin->data())>>;

    Vector midpoint{};
    size_t count = 0;
    while (begin != end)
    {
        ++count;
        midpoint += *begin++;
    }
    return midpoint * (Prec{1} / Prec(count));
}

/// Collapses a triangle to an unnormalised line segment.
///
/// Given a triangle, this returns a line parametrised such that the segment
/// obtained for t∈[0, 1] is a good approximation of the triangle. In
/// particular, for triangles with very low area, this eessentially removes the
/// extraneous vertex.
///
/// \param tri A triangle.
///
/// \returns A line which approximates the triangle well, *if* the triangle is
///          highly degenerate.
template <typename Prec>
Line3d<Prec> collapseToLine(Triangle3d<Prec> tri) noexcept
{
    auto lenAB = squaredLength(tri.a-tri.b);
    auto lenAC = squaredLength(tri.a-tri.c);
    auto lenBC = squaredLength(tri.b-tri.c);
    return lenAB > lenAC
           ? (lenAB > lenBC
              ? Line3d<Prec>{tri.a, tri.b-tri.a}
              : Line3d<Prec>{tri.b, tri.c-tri.b})
           : (lenAC > lenBC
              ? Line3d<Prec>{tri.a, tri.c-tri.a}
              : Line3d<Prec>{tri.b, tri.c-tri.b});
}

} // namespace kraken::geometry

#endif // KRAKEN_GEOMETRY_PRIMITIVEQUERIES_HPP_INCLUDED
