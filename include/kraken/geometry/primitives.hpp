#ifndef KRAKEN_GEOMETRY_PRIMITIVES_HPP_INCLUDED
#define KRAKEN_GEOMETRY_PRIMITIVES_HPP_INCLUDED

#include "kraken/math/matrix.hpp"
#include <cstddef>

#include <ostream>
#include <type_traits>

namespace kraken::geometry
{

template <typename Prec>
using Vector3 = math::Vector<3, Prec>;

template <typename Prec>
using Vector2 = math::Vector<2, Prec>;

template <typename Prec>
using Point3d = Vector3<Prec>;

template <typename Prec>
using Point2d = Vector2<Prec>;

/// This represents a line in three-dimensional space; the points on the line
/// are given by origin+t*direction, for t∈ℝ.
template <typename Prec>
struct Line3d
{
    Vector3<Prec> origin; ///< Can be any point on the line.
    Vector3<Prec> direction; ///< Must be normalised.
};

/// This represents a line in three-dimensional space; the points on the line
/// are given by origin+t*direction, for t∈ℝ.
template <typename Prec>
struct LineSegment3d
{
    Vector3<Prec> origin; ///< Can be any point on the line.
    Vector3<Prec> direction; ///< Must be normalised.
    Prec minPar; ///< Minimum parameter for t.
    Prec maxPar; ///< Maximum parameter for t.
};


template <typename Prec>
struct Line2d
{
    Vector2<Prec> origin; ///< Can be any point on the line.
    Vector2<Prec> direction; ///< Must be normalised.
};

template <typename Prec>
struct Plane
{
    Plane() = default;
    Plane(Vector3<Prec> o, Vector3<Prec> n)
        : origin{o}, normal{n} {}
    Plane(Vector3<Prec> a, Vector3<Prec> b, Vector3<Prec> c)
        : origin{a}, normal{normalise(cross(b-a, c-a))} {}

    Vector3<Prec> origin; ///< Any point on the plane.
    Vector3<Prec> normal; ///< A normal to the plane -- must be normalised!
};

static_assert(std::is_trivial<Plane<float>>::value);
static_assert(std::is_standard_layout<Plane<float>>::value);

/// Describes a triangle embedded in ℝ³ specified by its vertices. It is
/// implicitly oriented counter-clockwise; so a normal pointing outwards from
/// the triangle (in positive direction) is given by (b-a)×(c-a). (Note: that
/// also gives an easy way to compute the area; namely the area is
/// ½|(b-a)×(c-a)|.)
template <typename Prec>
struct Triangle3d
{
    Vector3<Prec> a;
    Vector3<Prec> b;
    Vector3<Prec> c;

    Vector3<Prec>& operator[](size_t i) noexcept
    {
        return reinterpret_cast<Vector3<Prec>*>(this)[i];
    }
    const Vector3<Prec>& operator[](size_t i) const noexcept
    {
        return reinterpret_cast<Vector3<Prec>*>(this)[i];
    }
};

static_assert(std::is_trivial<Triangle3d<float>>::value);
static_assert(std::is_standard_layout<Triangle3d<float>>::value);

static_assert(std::is_trivial<Triangle3d<double>>::value);
static_assert(std::is_standard_layout<Triangle3d<double>>::value);

/// Describes a tetrahedron in ℝ³ specified by its vertices, and as opposed to
/// a triangle, the ordering of the vertices does not have any specific meaning
/// a priori, since it has a well-defined inside and outside; namely the finite
/// and infinite volume its shell implicitly partitions the space into.
template <typename Prec>
struct Tetrahedron
{
    Vector3<Prec> a;
    Vector3<Prec> b;
    Vector3<Prec> c;
    Vector3<Prec> d;
};

template <typename Prec>
std::ostream& operator<<(std::ostream& ost, const Triangle3d<Prec>& tri)
{
    return ost << "{" << tri.a << ", " << tri.b << ", " << tri.c << "}";
}

} // namespace kraken::geometry

#endif // KRAKEN_GEOMETRY_PRIMITIVES_HPP_INCLUDED
