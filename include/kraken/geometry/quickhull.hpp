#ifndef KRAKEN_GEOMETRY_QUICKHULL_HPP_INCLUDED
#define KRAKEN_GEOMETRY_QUICKHULL_HPP_INCLUDED

#include "kraken/math/matrix.hpp"
#include <cstddef>
#include <kraken/geometry/mesh.hpp>

namespace kraken
{

namespace geometry
{

/// \brief Calculates the convex hull of a point cloud.
///
/// Please keep the following points in mind when using quickhull:
/// * Quickhull internally uses an automatically generated epsilon value in
///   tests like whether two faces are coplanar and should be merged. This is
///   calculated using the absolute value of the coordinates of the vertices.
///   Therefore vertices should be centred around the origin since otherwise
///   vertices might be considered equal (for example, if one has a number of
///   vertices where the greatest distance between them is .0001, they will all
///   be considered equal if they are places 1000 units from the origin while
///   this is not the case if they are placed very near the origin).
/// * Because the previous epsilon value is used in all computations models
///   which are extremely long in one direction compared to another (e.g. a
///   cylinder where height >> width) may fail hull generation (since all
///   endpoints may be collapsed). However, this should not be a problem if
///   models are reasonably sized - if the largest dimension of the object is at
///   most 250 times the smallest dimension, there shouldn't be any problems
///   (even up to about 800 times larger seems fine).
/// * Aside from the aforementioned epsilon two other ones are used:
///   One is the triangle epsilon, which determines whether a given point can be
///   added to the hull in the following way: If the addition of this point
///   would cause a triangle with area less than triangleEpsilon * epsilon
///   (where epsilon is the internally created epsilon), that point will be
///   rejected, since such small triangles are both numerically unstable and
///   mean that the point lies so close to the hull that its addition would not
///   alter the shape significantly.
///   The other epsilon is again a multiplier which determines whether two
///   neighbouring faces should be merged. Please note that increasing this may
///   introduce larger concavities which can cause hull generation to either
///   return a somewhat concave hull (best case), return a hull with random
///   interior triangles and/or tunnels or simply fail (by constructing bad
///   geometry such that some queries may run "forever" or (more likely) simply
///   throwing exceptions).
///
/// \param vertices Vertices to calculate quickhull from.
/// \param vertexCount Number of vertices pointed to by 'vertices'.
/// \param vertexStride Distance (in bytes) of two consecutive vertices.
/// \param triangleEpsilon Epsilon multiplier to accept/reject triangles.
/// \param mergeEpsilon Epsilon multiplier to determine whether to merge faces.
/// \return An approximately convex hull of the given vertices.
Mesh quickhull(const math::vec3* vertices,
               size_t vertexCount,
               size_t vertexStride = sizeof(math::vec3),
               float triangleEpsilon = 1.875e2f,
               float mergeEpsilon = 1.1e2f);

} // namespace geometry

} // namespace kraken

#endif // KRAKEN_GEOMETRY_QUICKHULL_HPP_INCLUDED

