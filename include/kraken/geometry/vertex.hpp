#ifndef KRAKEN_GEOMETRY_VERTEX_HPP_INCLUDED
#define KRAKEN_GEOMETRY_VERTEX_HPP_INCLUDED

#include "kraken/types.hpp"
#include <cstddef>

#include <ostream>
#include <string>
#include <string_view>
#include <vector>

namespace kraken::geometry
{

enum class VertexDataType : U32
{
    Padding = 0x0, // Padding size == 1 byte.
    Byte    = 0x1,
    UByte   = 0x2,
    Short   = 0x3,
    UShort  = 0x4,
    Int     = 0x5,
    UInt    = 0x6,
    Float   = 0x7,

    TypeMask = 0xff,

    // By default, data is assumed to need normalisation - that is it needs to
    // be converted to floats in the range [0, 1] by dividing with the maximum
    // value (or for signed integer types to the range [-1, 1]).
    Unnormalised = 0x100, // Instead of normalising, cast to float.
    Integer      = 0x200, // Pass the data directly as the integer type.
};

std::ostream& operator<<(std::ostream& ost, VertexDataType vdt);

size_t dataTypeSize(VertexDataType);

inline VertexDataType operator&(VertexDataType a, const VertexDataType& b)
{
    return static_cast<VertexDataType>(static_cast<U32>(a)
                                       & static_cast<U32>(b));
}
inline VertexDataType operator&=(VertexDataType& a, const VertexDataType& b)
{
    return a = a&b;
}

inline VertexDataType operator|(VertexDataType a, const VertexDataType& b)
{
    return static_cast<VertexDataType>(static_cast<U32>(a)
                                       | static_cast<U32>(b));
}

inline VertexDataType operator|=(VertexDataType& a, const VertexDataType& b)
{
    return a = a|b;
}

inline bool operator!(VertexDataType v)
{
    return static_cast<bool>(v);
}

struct Attribute
{
    // Maybe normalise "position"/"pos", "normal"/"nml", "texcoord"/"uv", etc.
    bool operator==(std::string_view sv) const noexcept { return name == sv; }

    std::string name;
    VertexDataType type;
    size_t count;
};

std::ostream& operator<<(std::ostream& ost, const Attribute& attr);

using VertexType = std::vector<Attribute>;

bool operator==(const VertexType& a, const VertexType& b);
inline bool operator!=(const VertexType& a, const VertexType& b)
{
    return !(a == b);
}

std::ostream& operator<<(std::ostream& ost, const VertexType& vt);

VertexType normalise(const VertexType& vType);

enum class PrimitiveType
{
    Triangles, TriangleFan, TriangleStrip,
    Lines, LineStrip, Points,
};

template <typename It>
size_t vertexSize(It begin, It end)
{
    size_t size = 0;
    while (begin != end)
    {
        size += dataTypeSize(begin->type)*begin->count;
        ++begin;
    }
    return size;
}

inline size_t vertexSize(const VertexType& vt)
{
    return vertexSize(vt.begin(), vt.end());
}

} // namespace kraken::geometry

#endif // KRAKEN_GEOMETRY_VERTEX_HPP_INCLUDED
