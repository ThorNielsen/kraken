#ifndef KRAKEN_GRAPHICS_EXPORT_HPP_INCLUDED
#define KRAKEN_GRAPHICS_EXPORT_HPP_INCLUDED

#include <cstddef>
#include <kraken/graphics/vertex.hpp>
#include <kraken/types.hpp>
#include <ostream>

namespace kraken
{

namespace graphics
{

class Mesh;

struct PLYExportOptions
{
    bool signedIndices = false;
};

bool exportPLY(std::ostream& output, const Mesh* mesh,
               PLYExportOptions opts = {});
bool exportPLY(std::ostream& output,
               const void* vertexBegin,
               size_t vertexCount,
               const void* indexBegin,
               size_t indexCount,
               const VertexType& vertexType,
               U32 indexByteWidth,
               PLYExportOptions opts = {});

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_EXPORT_HPP_INCLUDED

