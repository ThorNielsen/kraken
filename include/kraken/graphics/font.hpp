#ifndef KRAKEN_GRAPHICS_FONT_HPP_INCLUDED
#define KRAKEN_GRAPHICS_FONT_HPP_INCLUDED

#include <kraken/types.hpp>
#include <kraken/graphics/shader.hpp>
#include <kraken/graphics/typesetting.hpp>
#include <kraken/graphics/videodriver.hpp>
#include <kraken/math/matrix.hpp>
#include <kraken/utility/string.hpp>

#include <vector>

namespace kraken
{

namespace graphics
{

namespace freetype
{

struct FaceWrapper;
struct GlyphMap;

} // namespace freetype

struct GlyphRectangle
{
    U16 x;
    U16 y;
    U16 width;
    U16 height;
};

struct Glyph
{
    GlyphRectangle texCoords;
    U32 glyphIndex;
    S16 xOffset;
    S16 yOffset;
    S16 xAdvance;
    S16 yAdvance;
};

class Font
{
public:
    Font(VideoDriver*);
    ~Font();

    bool load(const std::string& path);
    bool load(const U8* data, U64 length);

    void clear();

    void enableKerning();
    void disableKerning()
    {
        m_usekerning = false;
    }

    bool isKerningEnabled() const
    {
        return m_usekerning;
    }

    freetype::FaceWrapper* getInternalFace() const
    {
        return m_face;
    }

    void update();

    Glyph getGlyph(U32 codepoint, U32 height) const;

    int lineHeight() const
    {
        return m_height + m_height / 4;
    }

    /// \brief Renders text given its metrics.
    ///
    /// The text must be typeset before it can be rendered, as this function
    /// does not typeset anything. No attempt is made at containing all text
    /// within the renderable window - text can (and if care isn't taken, will)
    /// be rendered offscreen. This implies a performance hit if large amounts
    /// of text are offscreen, as geometry is generated for each letter.
    /// \param tm Metrics of the text.
    /// \param driver Driver driving display text should be rendered on.
    /// \param textShader A text rendering shader.
    /// \param position Origin point of the text. Note that +Y is down!
    /// \param align How the position should be interpreted - e.g. TopLeft means
    ///        that text should be rendered such that the top left of the text
    ///        is positioned at 'position'.
    void render(const TextMetrics&,
                VideoDriver* driver,
                Shader* textShader,
                math::ivec2 position,
                Alignment align = Alignment::TopLeft);

    // Note that this method is ~33% faster than the previous one that renders
    // directly from TextMetrics for large texts (several thousand glyphs).
    /// \brief Renders text given pre-processed geometry.
    ///
    /// \param geometry Pre-processed render geometry.
    /// \param driver Driver driving display to render on.
    /// \param textShader Text rendering shader.
    void render(const TextGeometry& geometry,
                VideoDriver* driver,
                Shader* textShader,
                math::ivec2 position,
                Alignment align = Alignment::TopLeft);

    void setupRendering(Shader* shader) const;

    void dump();
private:
    friend TextMetrics typeset(const String&,
                               TextAlignment,
                               Font&,
                               U32,
                               U32);

    struct GlyphMapWrapper
    {
        GlyphMapWrapper() { glyphmap = nullptr; }
        ~GlyphMapWrapper();
        freetype::GlyphMap* glyphmap;
    };

    U8* dataBuffer(U64 length);
    bool createFace();
    void deleteFace();

    void setHeight(U32 height) const;

    std::vector<U8> m_fontdata;
    GlyphMapWrapper m_glyphmap;
    freetype::FaceWrapper* m_face;
    VideoDriver* m_driver;
    mutable U32 m_height;
    bool m_usekerning;
    bool m_faceInit;
};

void render(const TextGeometry& geometry,
            VideoDriver* driver,
            Shader* textShader,
            math::ivec2 position,
            Alignment align);


} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_FONT_HPP_INCLUDED

