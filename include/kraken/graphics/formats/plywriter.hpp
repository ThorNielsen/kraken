#ifndef KRAKEN_GRAPHICS_FORMATS_PLYWRITER_HPP_INCLUDED
#define KRAKEN_GRAPHICS_FORMATS_PLYWRITER_HPP_INCLUDED

#include "kraken/types.hpp"
#include <cstddef>
#include <kraken/graphics/export.hpp>
#include <kraken/graphics/vertex.hpp>
#include <ostream>
#include <string>
#include <vector>

namespace kraken
{

namespace graphics
{

class PLYWriter
{
public:
    PLYWriter();
    ~PLYWriter();

    bool write(std::ostream& output,
               const void* vertexBegin,
               size_t vertexCount,
               const void* indexBegin,
               size_t indexCount,
               const VertexType& vertexType,
               U32 indexByteWidth,
               PLYExportOptions opts);
private:
    void reset();
    bool createHeader(const VertexType& vType);
    bool createIndices(const void* currIndices, U32 indexByteWidth);
    bool copyExistingIndices(const void* currIndices, U32 indexByteWidth);
    void createNewIndices();
    bool appendAttribute(const std::string& name, VertexDataType dataType,
                         size_t count);

    template <typename Numeric>
    std::string toString(Numeric arg)
    {
        return std::to_string(arg);
    }

    PLYExportOptions m_options;
    std::vector<U8> m_indices;
    std::string m_header;
    size_t m_vCount;
    size_t m_iCount;
    size_t m_padNumber;
    U32 m_indexOutWidth;
    char m_lineEnd;
    bool m_hasIndices;
};

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_FORMATS_PLYWRITER_HPP_INCLUDED

