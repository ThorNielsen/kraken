#ifndef KRAKEN_GRAPHICS_FRAMEBUFFER_HPP_INCLUDED
#define KRAKEN_GRAPHICS_FRAMEBUFFER_HPP_INCLUDED

#include <kraken/types.hpp>
#include <kraken/image/image.hpp>

namespace kraken
{

namespace graphics
{

class FramebufferTexture;

enum class FramebufferFormat
{
    ColourRGBA8,
    Depth24Stencil8,
};

class Framebuffer
{
protected:
    // Ugly to have protected first, but it is the only way to be able to
    // implement clearAttachment inline.
    enum class AttachmentClearType
    {
        Unsigned, Int, Float, DepthStencil
    };

public:
    virtual ~Framebuffer() {}

    virtual void resize(U32 newWidth, U32 newHeight) = 0;
    virtual U32 width() const = 0;
    virtual U32 height() const = 0;

    virtual bool createAttachment(U32 location, FramebufferFormat fmt) = 0;
    virtual const FramebufferTexture* attachmentTexture(U32 location) const = 0;
    virtual bool isAttached(U32 location) const = 0;
    virtual void removeAttachment(U32 location) = 0;
    virtual U32 maxColourAttachments() const = 0;
    virtual U32 maxSimultaneousDrawingAttachments() const = 0;
    virtual U32 depthAttachmentID() const = 0;
    virtual U32 stencilAttachmentID() const = 0;
    virtual U32 depthStencilAttachmentID() const = 0;
    virtual U32 attachedBufferMask() const = 0;

    void clearAttachment(U32 id, const U32* value)
    {
        clearAttachment(id, value, 0, AttachmentClearType::Unsigned);
    }
    void clearAttachment(U32 id, const S32* value)
    {
        clearAttachment(id, value, 0, AttachmentClearType::Int);
    }
    void clearAttachment(U32 id, const F32* value)
    {
        clearAttachment(id, value, 0, AttachmentClearType::Float);
    }
    void clearAttachment(U32 id, F32 depth, S32 stencil)
    {
        clearAttachment(id, &depth, stencil, AttachmentClearType::DepthStencil);
    }

protected:
    virtual void clearAttachment(U32 id, const void* values, S32 stencil,
                                 AttachmentClearType type) = 0;
    virtual void setupRendering(U32 attachMask, bool useAsInput,
                                bool useAsOutput) = 0;
    virtual void endRendering() = 0;
};

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_FRAMEBUFFER_HPP_INCLUDED

