#ifndef KRAKEN_GRAPHICS_FREETYPE_FREETYPE_HPP_INCLUDED
#define KRAKEN_GRAPHICS_FREETYPE_FREETYPE_HPP_INCLUDED

#include "freetype/config/ftheader.h"
#include "freetype/fttypes.h"

#include <string>

#include FT_FREETYPE_H

namespace kraken
{

namespace graphics
{

namespace freetype
{

void throwFTError(FT_Error fterr, std::string func, int line);

#define checkFTError(fterr) do { if (fterr) { kraken::graphics::freetype::throwFTError(fterr, __func__, __LINE__); } } while(false)

struct FaceWrapper
{
    FaceWrapper() {}
    ~FaceWrapper() {}
    FT_Face face;
};

} // namespace freetype


} // namespace graphics

} // namespace kraken


#endif // KRAKEN_GRAPHICS_FREETYPE_FREETYPE_HPP_INCLUDED

