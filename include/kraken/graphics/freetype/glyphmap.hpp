#ifndef KRAKEN_GRAPHICS_FREETYPE_GLYPHMAP_HPP_INCLUDED
#define KRAKEN_GRAPHICS_FREETYPE_GLYPHMAP_HPP_INCLUDED

#include <kraken/types.hpp>
#include <kraken/graphics/font.hpp>
#include <kraken/graphics/freetype/freetype.hpp>
#include <kraken/graphics/texture.hpp>
#include <kraken/graphics/texturesampler.hpp>
#include <kraken/graphics/videodriver.hpp>
#include <kraken/math/matrix.hpp>

#include <map>
#include <unordered_map>

namespace std
{

template<> struct hash<std::pair<kraken::U32, kraken::U32>>
{
    size_t operator()(const std::pair<kraken::U32, kraken::U32>& s) const
    {
        return std::hash<kraken::U32>{}(s.first) ^ (std::hash<kraken::U32>{}(~s.second));
    }
};

}

namespace kraken
{

namespace graphics
{

namespace freetype
{

class GlyphMap
{
public:
    GlyphMap(VideoDriver*);
    ~GlyphMap() {}

    void addGlyph(U32 codepoint, U32 height, U32 glyphIdx, FT_GlyphSlot gslot);
    void clear();

    void resizeTexture(U32 height);

    FT_GlyphSlot renderGlyph(FT_Face& face, U32 codepoint);

    Glyph getGlyph(FT_Face& face, U32 codepoint, U32 height)
    {
        auto pos = m_positions.find({codepoint, height});
        if (pos == m_positions.end())
        {
            m_dirty = true;
            // It is the calling functions responsibility to ensure that face's
            // height is 'height'.
            U32 glyph = FT_Get_Char_Index(face, codepoint);
            addGlyph(codepoint, height, glyph, renderGlyph(face, glyph));
            pos = m_positions.find({codepoint, height});
        }
        return pos->second;
    }
    const Texture2D* getTexture() const
    {
        return m_texture.get();
    }
    const TextureSampler* getSampler() const
    {
        return m_sampler.get();
    }
    void updateMap()
    {
        m_texture->update();
        m_dirty = false;
    }
    bool dirty() const { return m_dirty; }

    U32 texWidth() const { return m_width; }
    U32 texHeight() const { return m_height; }

private:
    // Performance measurements show that unordered_map is faster than map.
    // Test procedure:
    //  Call getGlyph() repeatedly to get a glyph, copy texCoords and do nothing
    //  with the result. This is done on an English text with 4197 glyphs giving
    //  4197 lookups (all letters and some punctuation is present in the text).
    // Results (average over ~400-500 repetitions):
    //  map:           1.190970ms
    //  unordered_map: 0.900656ms    ~75% of plain map.
    // It is expected that texts with a larger number of glyphs will benefit
    // even more from unordered_map's O(1) lookup time, although this should be
    // measured.
    std::unordered_map<std::pair<U32, U32>, Glyph> m_positions;
    std::unique_ptr<TextureSampler> m_sampler;
    std::unique_ptr<Texture2D> m_texture;
    U32 m_xpos;
    U32 m_ypos;
    U32 m_lineheight;
    U32 m_width;
    U32 m_height;
    bool m_dirty;
};

} // namespace freetype

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_FREETYPE_GLYPHMAP_HPP_INCLUDED

