#ifndef KRAKEN_GRAPHICS_IMPORT_HPP_INCLUDED
#define KRAKEN_GRAPHICS_IMPORT_HPP_INCLUDED

#include <istream>

namespace kraken::graphics
{

class Mesh;

bool importPLY(std::istream& input, Mesh* meshOut);

bool importSTL(std::istream& input, Mesh* meshOut);

} // namespace kraken::graphics

#endif // KRAKEN_GRAPHICS_IMPORT_HPP_INCLUDED
