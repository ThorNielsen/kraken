#ifndef KRAKEN_GRAPHICS_MESH_HPP_INCLUDED
#define KRAKEN_GRAPHICS_MESH_HPP_INCLUDED

#include "kraken/types.hpp"
#include <kraken/graphics/vertex.hpp>

namespace kraken
{

namespace graphics
{

class VideoDriver;

class Mesh
{
public:
    virtual ~Mesh() {}

    virtual void construct(const void* vertices_,
                           U64 vertexCount_,
                           const VertexType& vertexType_,
                           PrimitiveType primitiveType_,
                           const void* indices_ = nullptr,
                           U64 indexByteWidth_ = 0,
                           U64 indexCount_ = 0) = 0;

    virtual void append(const void* vertices_,
                        U64 vertexCount_,
                        const void* indices_ = nullptr,
                        U64 indexByteWidth_ = 0,
                        U64 indexCount_ = 0,
                        bool indexIntoPrevious = false) = 0;

    virtual void resizeVertexBuffer(U64 bytes) = 0;
    virtual void resizeIndexBuffer(U64 bytes) = 0;

    virtual U64 vertexBufferSize() const = 0;
    virtual U64 indexBufferSize() const = 0;

    virtual U64 vertexCount() const = 0;
    virtual U64 indexCount() const = 0;

    virtual const VertexType& vertexType() const = 0;
    virtual PrimitiveType primitiveType() const = 0;
    virtual U64 indexByteWidth() const = 0;

    virtual void setVertexType(const VertexType&) = 0;
    virtual void setPrimitiveType(PrimitiveType) = 0;
    virtual void setIndexByteWidth(U64 bytes) = 0;

    virtual const void* vertices() const = 0;
    virtual void* vertices() = 0;
    virtual const void* indices() const = 0;
    virtual void* indices() = 0;

    virtual void clear() = 0;
    virtual void update() = 0;

    virtual void draw(VideoDriver*, U32 count = 0) = 0;
};

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_MESH_HPP_INCLUDED

