#ifndef KRAKEN_GRAPHICS_OPENGL_DRIVER_HPP_INCLUDED
#define KRAKEN_GRAPHICS_OPENGL_DRIVER_HPP_INCLUDED

#include <kraken/graphics/videodriver.hpp>
#include <kraken/graphics/opengl/opengl.hpp>
#include <kraken/graphics/opengl/shader.hpp>

#include <functional>
#include <map>
#include <vector>
#include <utility>

namespace kraken
{

namespace graphics
{

class Framebuffer;
class Window;

namespace opengl
{

class Framebuffer;

class Driver : public VideoDriver
{
public:
    Driver(void* handle);
    ~Driver();
    bool beginDraw(bool clearCBuffer,
                   bool clearZBuffer,
                   image::Colour background,
                   image::ColourType colourType) override;
    bool endDraw() override;

    std::unique_ptr<graphics::Framebuffer> createFramebuffer(math::uvec2 dim) override;
    std::unique_ptr<graphics::Mesh> createMesh() override;
    std::unique_ptr<graphics::Shader> createShader() override;
    std::unique_ptr<graphics::Texture1D> create1DTexture() override;
    std::unique_ptr<graphics::Texture2D> create2DTexture() override;
    std::unique_ptr<graphics::Texture3D> create3DTexture() override;
    std::unique_ptr<graphics::TextureSampler> createTextureSampler() override;

    void drawVertices(const void* vertices,
                      VertexType vertexType,
                      U64 vertexCount,
                      const void* indices,
                      U64 indexByteWidth,
                      U64 indexCount,
                      PrimitiveType pt) override;

    void drawVertices(const void* vertices,
                      VertexType vType,
                      U64 vCount,
                      PrimitiveType pt) override;

    bool use(graphics::Shader* shader) override
    {
        m_currShader = static_cast<Shader*>(shader);
        if (!shader) return true;
        if (&m_currShader->m_driver != this)
        {
            throw std::runtime_error("Trying to use shader from different driver.");
        }
        return m_currShader->use();
    }

    const graphics::Shader* currentShader() const override
    {
        return m_currShader;
    }

    graphics::Shader* currentShader() override
    {
        return m_currShader;
    }

    bool useFramebuffer(graphics::Framebuffer* buffer,
                        bool useAsInput, bool useAsOutput) override;

    const graphics::Framebuffer* currentReadFramebuffer() const override;
    graphics::Framebuffer* currentReadFramebuffer() override;
    const graphics::Framebuffer* currentWriteFramebuffer() const override;
    graphics::Framebuffer* currentWriteFramebuffer() override;

    U32 width() const override
    {
        return m_width;
    }

    U32 height() const override
    {
        return m_height;
    }

    void enable(Capability) override;
    void disable(Capability) override;
    bool isEnabled(Capability) const override;

    void setWindingOrder(WindingOrder) override;
    void setRenderMode(RenderMode) override;
    void setCullMode(CullMode) override;

    WindingOrder windingOrder() const override { return m_windingorder; }
    RenderMode renderMode() const override { return m_rendermode; }
    CullMode cullMode() const override { return m_cullmode; }

    void onFramebufferResize(U32 newX, U32 newY);

protected:
    void ensureContextCurrent();


    void setupTextRendering() override;
    void drawText(const TextGeometry& geom, math::ivec2 offset) override;
    void onEndTextRendering() override;

private:
    friend class Shader;
    Driver(Window*);


    // Finds the binding point of texture - if it isn't bound then 0 is
    // returned.
    GLint getTextureUnit(GLuint texture);

    /// \brief Transfers ownership of a texture binding point to the shader.
    ///
    /// \param shader The shader that wants ownership of the texture unit.
    /// \param shaderLoc Location of the texture in the shader.
    /// \param textureUnit Texture unit to take ownership of.
    void takeOwnership(GLuint shader, GLuint shaderLoc, GLuint textureUnit);

    /// \brief Finds a vacant texture unit.
    ///
    /// This looks for a texture unit where 'texture' can be bound to. It also
    /// makes sure that the next time 'shader' requests a new texture unit this
    /// one cannot be returned unless the next request's 'shaderLoc' is the same
    /// as this one's (because then the new texture will replace this one in the
    /// shader which makes this one unnecessary).
    ///
    /// \param shader The shader program that should have ownership of the unit.
    /// \param shaderLoc Location of texture that will be bound to the returned
    ///                  texture unit in shader.
    /// \param texture The texture that is to be bound to the returned unit.
    /// \return Texture unit if any are available or stealable, otherwise an
    ///         exception is thrown.
    GLint getTextureUnit(GLuint shader, GLuint shaderLoc, GLuint texture);


    // Clears texture unit where texture is bound to (if any).
    void clearSingleTextureUnit(GLuint texture);

    // Clears all texture units owned by shader (if any).
    void clearAllTextureUnits(GLuint shader);

    // Clears all texture units.
    void clearAllTextureUnits();


    struct TextureBinding
    {
        GLuint shader;
        GLuint loc;
        GLuint texture;
    };

    // Oh look, a magic number. Well, it is required that there's at least 48
    // texture image units in OpenGL 3.3 implementations. However, one of these
    // units (the first one) has been reserved (in this application) such that
    // any temporary texture binding can use GL_TEXTURE0 without overwriting a
    // non-temporary binding. Therefore, we get at least 47 usable texture
    // units. It is possible to use a vector and at runtime resize it to fit the
    // amount of available texture units but this is not done for two reasons:
    // 1. If the number of texture units become too large binding a texture
    //    will take too long as a linear search is used to find a (somewhat)
    //    free unit. Whether this cost is large compared to texture binding time
    //    remains to be tested/measured.
    // 2. If someone develops on a machine with more than 48 units and uses more
    //    than 47 they may assume that any machine can handle N simultaneous
    //    textures, and then their program simply won't work on some conforming
    //    implementations.
    std::array<TextureBinding, 47> m_texbinds;

    void* m_windowHandle;

    Shader* m_currShader;
    Framebuffer* m_currReadFramebuffer;
    Framebuffer* m_currWriteFramebuffer;
    std::unique_ptr<Shader> m_textShader;

    std::map<Capability, bool> m_capability;

    std::function<void(void)> m_fontRestore;
    GLuint m_buffers[2];
    RenderMode m_rendermode;
    WindingOrder m_windingorder;
    CullMode m_cullmode;
    GLuint m_vao;
    U32 m_width;
    U32 m_height;
    bool m_isCulling;
};

} // namespace opengl

} // namespace graphics

} // namespace kraken

#endif // GRAPHICS_OPENGL_DRIVER_HPP_INCLUDED

