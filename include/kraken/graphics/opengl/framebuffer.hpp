#ifndef KRAKEN_GRAPHICS_OPENGL_FRAMEBUFFER_HPP_INCLUDED
#define KRAKEN_GRAPHICS_OPENGL_FRAMEBUFFER_HPP_INCLUDED

#include <kraken/graphics/framebuffer.hpp>
#include <kraken/graphics/opengl/opengl.hpp>
#include <kraken/graphics/opengl/texture.hpp>
#include <vector>

namespace kraken
{

namespace graphics
{

namespace opengl
{

class Driver;

class Framebuffer final : public graphics::Framebuffer
{
public:
    ~Framebuffer();
    void resize(U32 newWidth, U32 newHeight) override;
    U32 width() const override { return m_dims.x; }
    U32 height() const override { return m_dims.y; }

    bool createAttachment(U32 location, FramebufferFormat fmt) override;
    const graphics::FramebufferTexture* attachmentTexture(U32 location) const override;
    bool isAttached(U32 location) const override
    {
        return !!(m_attachMask & (U32(1) << location));
    }
    void removeAttachment(U32 location) override;
    U32 maxColourAttachments() const override { return m_maxColbufs; }
    U32 maxSimultaneousDrawingAttachments() const override { return m_maxDraw; }
    U32 depthStencilAttachmentID() const override { return m_maxColbufs; }
    U32 depthAttachmentID() const override { return m_maxColbufs+1; }
    U32 stencilAttachmentID() const override { return m_maxColbufs+2; }
    U32 attachedBufferMask() const override { return m_attachMask; }

protected:
    void clearAttachment(U32 id, const void* values, S32 stencil,
                         AttachmentClearType type) override;
    void setupRendering(U32 attachMask, bool asInput, bool asOutput) override;
    void endRendering() override;

private:
    friend class Driver;

    Framebuffer(Driver* driver, math::uvec2 dims);

    struct AttachedTexture
    {
        GLuint id;
        GLint internalFormat;
        GLenum format;
        GLenum type;
    };

    void updateTexture2D(const AttachedTexture& params);
    GLenum locationToAttachmentTarget(U32 location) const;

    std::vector<AttachedTexture> m_attachments;
    mutable std::vector<FramebufferTexture> m_fbTextures;

    math::uvec2 m_dims;
    Driver* m_driver;
    U32 m_attachMask;
    U32 m_maxColbufs;
    U32 m_maxDraw;
    GLuint m_fbo;
    GLenum m_target;
};

} // namespace opengl

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_OPENGL_FRAMEBUFFER_HPP_INCLUDED
