#ifndef KRAKEN_GRAPHICS_OPENGL_MESH_HPP_INCLUDED
#define KRAKEN_GRAPHICS_OPENGL_MESH_HPP_INCLUDED

#include <kraken/graphics/opengl/opengl.hpp>
#include <kraken/graphics/mesh.hpp>
#include <kraken/graphics/videodriver.hpp>
#include <kraken/graphics/vertex.hpp>
#include <kraken/utility/memory.hpp>

namespace kraken
{

namespace graphics
{

namespace opengl
{

class Driver;

class Mesh : public graphics::Mesh
{
public:
    ~Mesh();

    Mesh(const Mesh&) = delete;
    Mesh(Mesh&& buf) { moveConstruct(std::move(buf)); }

    Mesh& operator=(const Mesh&) = delete;
    Mesh& operator=(Mesh&& buf)
    {
        moveConstruct(std::move(buf));
        return *this;
    }

    void construct(const void* vertices_,
                   U64 vertexCount_,
                   const VertexType& vertexType_,
                   PrimitiveType primitiveType_,
                   const void* indices_,
                   U64 indexByteWidth_,
                   U64 indexCount_);

    void append(const void* vertices_,
                U64 vertexCount_,
                const void* indices_,
                U64 indexByteWidth_,
                U64 indexCount_,
                bool indexIntoPrevious);

    void resizeVertexBuffer(U64 bytes) override
    {
        resizeBuffer(bytes, m_vData, m_vAlloc, m_vCount, m_vElemSize);
    }
    void resizeIndexBuffer(U64 bytes) override
    {
        resizeBuffer(bytes, m_iData, m_iAlloc, m_iCount, m_iElemSize);
    }

    U64 vertexBufferSize() const override { return m_vAlloc; }
    U64 indexBufferSize() const override { return m_iAlloc; }

    U64 vertexCount() const override { return m_vCount; }
    U64 indexCount() const override { return m_iCount; }

    const VertexType& vertexType() const override { return m_vType; }
    PrimitiveType primitiveType() const override { return m_pType; }
    U64 indexByteWidth() const override { return m_iElemSize; }

    void setVertexType(const VertexType&) override;
    void setPrimitiveType(PrimitiveType pt) override { m_pType = pt; }
    void setIndexByteWidth(U64 bytes) override;

    const void* vertices() const override { return m_vData; }
    void* vertices() override { return m_vData; }
    const void* indices() const override { return m_iData; }
    void* indices() override { return m_iData; }

    void clear() override;
    void update() override;

    void draw(VideoDriver*, U32) override;

private:
    friend class Driver;

    Mesh();


    bool usingIndices() const { return m_iData != nullptr; }
    void constructIndices();

    void resizeBuffer(U64 newSz, U8*& data, U64& allocCnt,
                      U64& elemCount, U64 elemSize);

    void moveConstruct(Mesh&&);

    void copyByteShort(U16* newBuf);
    void copyByteInt(U32* newBuf);
    void copyShortInt(U32* newBuf);
    void widenIndices(U64 newByteWidth);

    void widenIndicesToFit(U64 index);

    void appendIndex(const void* src, U64 srcWidth, void* dest,
                     U64 idx, U64 add);

    U64 maxIndex() const;

    void drawSimple(Driver*, U32);
    void drawInstanced(Driver*, U32);

    VertexType m_vType;
    GLuint m_buf[2];
    U8* m_vData;
    U8* m_iData;
    U64 m_vAlloc;
    U64 m_iAlloc;
    U64 m_vElemSize;
    U64 m_iElemSize;
    U64 m_vCount;
    U64 m_iCount;
    PrimitiveType m_pType;
    bool initialised;
};

} // namespace opengl

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_OPENGL_MESH_HPP_INCLUDED
