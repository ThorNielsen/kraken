#ifndef KRAKEN_GRAPHICS_OPENGL_OPENGL_HPP_INCLUDED
#define KRAKEN_GRAPHICS_OPENGL_OPENGL_HPP_INCLUDED

#include <kraken/graphics/opengl/gl_core_3_3.h>

#include <stdexcept>
#include <string>

namespace kraken
{

namespace graphics
{

namespace opengl
{

void printGLError(GLint err, std::string func, std::string file, int line);

#define CHECK_GL_ERRORS

#ifdef CHECK_GL_ERRORS
#define checkGLError()\
do {\
    GLint err = glGetError();\
    if (err)\
    {\
        printGLError(err, __func__, __FILE__, __LINE__);\
        throw std::runtime_error("OpenGL error.");\
    }\
} while (false)

#else
#define checkGLError()
#endif // CHECK_GL_ERRORS

} // namespace opengl

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_OPENGL_OPENGL_HPP_INCLUDED

