#ifndef KRAKEN_GRAPHICS_OPENGL_SHADER_HPP_INCLUDED
#define KRAKEN_GRAPHICS_OPENGL_SHADER_HPP_INCLUDED

#include <kraken/graphics/shader.hpp>
#include <kraken/graphics/opengl/opengl.hpp>
#include <kraken/graphics/opengl/texture.hpp>
#include <kraken/graphics/opengl/texturesampler.hpp>
#include <kraken/utility/string.hpp>

#include <vector>
#include <map>

namespace kraken
{

namespace graphics
{

namespace opengl
{

class Driver;

class Shader : public graphics::Shader
{
public:
    ~Shader();
    Shader(const Shader&) = delete;
    Shader& operator=(const Shader&) = delete;

    bool addShader(const std::string& code, Stage stage) override;
    bool compile() override;

    AttributeID attributeID(const std::string& name) const override
    {
        for (size_t i = 0; i < m_attrinfo.size(); ++i)
        {
            if (m_attrinfo[i].name == name) return static_cast<AttributeID>(i);
        }
        return -1;
    }
    const AttributeInformation& attribute(AttributeID i) const override
    {
        return m_attrinfo[i];
    }
    AttributeID attributeCount() const override
    {
        return static_cast<AttributeID>(m_attrinfo.size());
    }

    UniformID uniformID(const std::string& name) const override
    {
        for (size_t i = 0; i < m_unifinfo.size(); ++i)
        {
            if (m_unifinfo[i].name == name) return static_cast<UniformID>(i);
        }
        return uniformCount(); // Invalid uniform!
    }
    const UniformInformation& uniform(UniformID i) const override
    {
        return m_unifinfo[i];
    }
    UniformID uniformCount() const override
    {
        return static_cast<UniformID>(m_unifinfo.size());
    }

    void useTexture(TextureID,
                    const graphics::Texture1D*,
                    const graphics::TextureSampler*) override;

    void useTexture(TextureID,
                    const graphics::Texture2D*,
                    const graphics::TextureSampler*) override;

    void useTexture(TextureID,
                    const graphics::Texture3D*,
                    const graphics::TextureSampler*) override;

    void useTexture(TextureID, const graphics::FramebufferTexture*) override;

    void setUniform(UniformID, math::mat4) override;
    void setUniform(UniformID, math::mat3) override;
    void setUniform(UniformID, math::mat2) override;
    void setUniform(UniformID, math::vec4) override;
    void setUniform(UniformID, math::vec3) override;
    void setUniform(UniformID, math::vec2) override;
    void setUniform(UniformID, float) override;

    void setUniform(UniformID, math::ivec4) override;
    void setUniform(UniformID, math::ivec3) override;
    void setUniform(UniformID, math::ivec2) override;
    void setUniform(UniformID, S32) override;

    void setUniform(UniformID, math::uvec4) override;
    void setUniform(UniformID, math::uvec3) override;
    void setUniform(UniformID, math::uvec2) override;
    void setUniform(UniformID, U32) override;

private:
    friend class Driver;

    Shader(graphics::opengl::Driver&);
    bool use();
    void readAttributes();
    void readUniforms();

    bool isValidUniformID(UniformID id) const
    {
        return id >= 0 && static_cast<size_t>(id) < m_unifinfo.size();
    }
    GLint toLocation(UniformID id) const
    {
        return m_unifinfo[id]._index;
    }

    void useTexture(TextureID tid, GLuint oglTextureID,
                    GLuint sampler, GLenum textureType);

    std::vector<GLuint> m_shaders;
    std::vector<AttributeInformation> m_attrinfo;
    std::vector<UniformInformation> m_unifinfo;
    Driver& m_driver;
    GLuint m_program;
    bool m_linked;
};

} // namespace opengl

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_OPENGL_SHADER_HPP_INCLUDED
