#ifndef KRAKEN_GRAPHICS_OPENGL_TEXTURE_HPP_INCLUDED
#define KRAKEN_GRAPHICS_OPENGL_TEXTURE_HPP_INCLUDED

#include <kraken/graphics/texture.hpp>
#include <kraken/graphics/opengl/opengl.hpp>

namespace kraken
{

namespace graphics
{

namespace opengl
{

GLenum getStorageFormat(graphics::TexelStorageFormat);
GLenum getChannelFormat(graphics::ChannelType);
GLenum getTexelFormat(graphics::TexelType);


class Texture
{
public:
    Texture();
    ~Texture();

    Texture& operator=(const Texture& other) = delete;
    Texture& operator=(Texture&& other);
    Texture(Texture&& other)
    {
        *this = std::move(other);
    }

    GLuint texture() const { return m_texture; }

private:
    GLuint m_texture;
};

class FramebufferTexture : public graphics::FramebufferTexture
{
public:
    GLuint texture;
};

class Texture1D : public graphics::Texture1D
{
public:
    Texture1D();
    ~Texture1D();

    void update() override;
    void updateMipmap(U32 mipmap) override;

    void setSize(U32 width_) override;

    U32 width() const override { return m_width; }

    opengl::Texture& getGLTexture() { return m_texture; }
    const opengl::Texture& getGLTexture() const { return m_texture; }

protected:
    //bool onGenerateMipmaps(U32 oldLevel, U32 newMaxLevel) override;
    void onAllocateMipmaps(U32 oldLevel, U32 newMaxLevel) override;
    //void onMipmapRemoval(U32 first) override;
    void onClear() override;

private:
    void internalUpdateMipmap(U32 mipmap,
                              U32 mipmapwidth,
                              GLenum oglStorageFormat,
                              GLenum oglChannelFormat,
                              GLenum oglTexelFormat);

    opengl::Texture m_texture;
    U32 m_width;
};


class Texture2D : public graphics::Texture2D
{
public:
    Texture2D();
    ~Texture2D();

    void update() override;
    void updateMipmap(U32 mipmap) override;

    void setSize(U32 width_, U32 height_) override;

    U32 width() const override { return m_width; }
    U32 height() const override { return m_height; }

    opengl::Texture& getGLTexture() { return m_texture; }
    const opengl::Texture& getGLTexture() const { return m_texture; }

protected:
    //bool onGenerateMipmaps(U32 oldLevel, U32 newMaxLevel) override;
    void onAllocateMipmaps(U32 oldLevel, U32 newMaxLevel) override;
    //void onMipmapRemoval(U32 first) override;
    void onClear() override;

private:
    void internalUpdateMipmap(U32 mipmap, U32 mipmapwidth, U32 mipmapheight,
                              GLenum oglInternalFormat, GLenum oglChannelFormat,
                              GLenum oglTexelFormat);

    opengl::Texture m_texture;
    U32 m_width;
    U32 m_height;
};


class Texture3D : public graphics::Texture3D
{
public:
    Texture3D();
    ~Texture3D();

    void update() override;
    void updateMipmap(U32 mipmap) override;

    void setSize(U32 width_, U32 height_, U32 depth_) override;

    U32 width() const override { return m_width; }
    U32 height() const override { return m_height; }
    U32 depth() const override { return m_depth; }

    opengl::Texture& getGLTexture() { return m_texture; }
    const opengl::Texture& getGLTexture() const { return m_texture; }

protected:
    //bool onGenerateMipmaps(U32 oldLevel, U32 newMaxLevel) override;
    void onAllocateMipmaps(U32 oldLevel, U32 newMaxLevel) override;
    //void onMipmapRemoval(U32 first) override;
    void onClear() override;

private:
    void internalUpdateMipmap(U32 mipmap, U32 mipmapwidth, U32 mipmapheight,
                              U32 mipmapdepth, GLenum oglInternalFormat,
                              GLenum oglChannelFormat, GLenum oglTexelFormat);

    opengl::Texture m_texture;
    U32 m_width;
    U32 m_height;
    U32 m_depth;
};

} // namespace opengl

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_OPENGL_TEXTURE_HPP_INCLUDED

