#ifndef KRAKEN_GRAPHICS_OPENGL_TEXTURESAMPLER_HPP_INCLUDED
#define KRAKEN_GRAPHICS_OPENGL_TEXTURESAMPLER_HPP_INCLUDED

#include <kraken/graphics/texturesampler.hpp>

#include <kraken/graphics/opengl/opengl.hpp>

namespace kraken
{

namespace graphics
{

namespace opengl
{

using graphics::InterpolationFilter;
using graphics::WrapMode;

class Shader;
class Driver;

class TextureSampler : graphics::TextureSampler
{
public:
    ~TextureSampler();

    void setMinFilter(InterpolationFilter) override;
    void setMagFilter(InterpolationFilter) override;
    void setMinMipmap(U32 mipmap) override;
    void setMaxMipmap(U32 mipmap) override;
    void setSWrapMode(WrapMode) override;
    void setTWrapMode(WrapMode) override;
    void setPWrapMode(WrapMode) override;

    InterpolationFilter minFilter() const override { return m_minfilter; }
    InterpolationFilter magFilter() const override { return m_magfilter; }
    U32 minMipmap() const override { return m_minmm; }
    U32 maxMipmap() const override { return m_maxmm; }
    WrapMode sWrapMode() const override { return m_swrap; }
    WrapMode tWrapMode() const override { return m_swrap; }
    WrapMode pWrapMode() const override { return m_swrap; }

private:
    TextureSampler();
    friend class Shader;
    friend class Driver;
    InterpolationFilter m_minfilter;
    InterpolationFilter m_magfilter;
    WrapMode m_swrap;
    WrapMode m_twrap;
    WrapMode m_pwrap;
    U32 m_minmm;
    U32 m_maxmm;
    GLuint m_sampler;
};

} // namespace opengl

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_OPENGL_TEXTURESAMPLER_HPP_INCLUDED

