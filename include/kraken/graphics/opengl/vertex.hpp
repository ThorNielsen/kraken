#ifndef KRAKEN_GRAPHICS_OPENGL_VERTEX_HPP_INCLUDED
#define KRAKEN_GRAPHICS_OPENGL_VERTEX_HPP_INCLUDED

#include <kraken/graphics/vertex.hpp>
#include <kraken/graphics/opengl/opengl.hpp>

namespace kraken
{

namespace graphics
{

namespace opengl
{

class Driver;

GLenum vertexDataTypeToGL(VertexDataType d);
GLenum primitiveTypeToGL(PrimitiveType pType);

void setupVertexRendering(const VertexType&, Driver*);

void clearVertexRendering(const VertexType&, Driver*);

} // namespace opengl

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_OPENGL_VERTEX_HPP_INCLUDED

