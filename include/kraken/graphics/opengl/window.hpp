#ifndef KRAKEN_GRAPHICS_OPENGL_WINDOW_HPP_INCLUDED
#define KRAKEN_GRAPHICS_OPENGL_WINDOW_HPP_INCLUDED

#include <kraken/window/window.hpp>
#include <memory>

namespace kraken
{

namespace graphics
{

namespace opengl
{

struct ContextSettings
{
    U32 majorVersion = 3;
    U32 minorVersion = 3;
    bool coreProfile = true;
    bool debugContext = false;

    U32 redBits = 8;
    U32 greenBits = 8;
    U32 blueBits = 8;
    U32 alphaBits = 8;
    U32 depthBits = 24;
    U32 stencilBits = 8;
    U32 samples = 0; // Samples for multisampled rendering. Zero disables this.
    bool stereo = false;
    bool doubleBuffer = true;
};

class GLSurfaceCreationInfo : public window::SurfaceCreationInfo
{
public:
    GLSurfaceCreationInfo()
        : m_cs{}, m_driver{nullptr}
    {}
    GLSurfaceCreationInfo(ContextSettings cs)
        : m_cs{cs}, m_driver{nullptr}
    {}
    GLSurfaceCreationInfo(const GLSurfaceCreationInfo&) = delete;
    GLSurfaceCreationInfo& operator=(const GLSurfaceCreationInfo&) = delete;

    ~GLSurfaceCreationInfo();

    ContextSettings& contextSettings() { return m_cs; }
    const ContextSettings& contextSettings() const { return m_cs; }

    void beforeWindowOpen() override;
    void* afterWindowOpen(void*) override;
    void beforeWindowClose(void*) override;
    void onFramebufferResize(U32, U32) override;

private:
    ContextSettings m_cs;
    void* m_driver;
};

std::unique_ptr<window::SurfaceCreationInfo> surfaceCreator(ContextSettings cs);

} // namespace opengl

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_OPENGL_WINDOW_HPP_INCLUDED

