#ifndef KRAKEN_GRAPHICS_SHADER_HPP_INCLUDED
#define KRAKEN_GRAPHICS_SHADER_HPP_INCLUDED

#include <kraken/math/matrix.hpp>
#include <kraken/types.hpp>

#include <istream>
#include <string>

namespace kraken
{

namespace graphics
{

enum class AttributeDataType : unsigned
{
    Float, Vector2, Vector3, Vector4,
    Matrix2, Matrix3, Matrix4,
    Integer, IVector2, IVector3, IVector4,
    Unsigned, UVector2, UVector3, UVector4
};

enum class UniformDataType : unsigned
{
    Float, Vector2, Vector3, Vector4,
    Integer, IVector2, IVector3, IVector4,
    Unsigned, UVector2, UVector3, UVector4,
    // Bools unsupported since they can be implemented with different sizes in
    // different compilers and architectures.
    //Bool, BVector2, BVector3, BVector4,
    Matrix2, Matrix3, Matrix4,
    Sampler1D, Sampler2D, Sampler3D,
    ISampler1D, ISampler2D, ISampler3D,
    USampler1D, USampler2D, USampler3D,
};

using AttributeID = int;
using TextureID = int;
using UniformID = int;

struct AttributeInformation
{
    std::string name;
    AttributeDataType type;
    unsigned count;
    int _index;
};

struct UniformInformation
{
    std::string name;
    UniformDataType type;
    unsigned count;
    int _index;
};

class FramebufferTexture;
class Texture1D;
class Texture2D;
class Texture3D;
class TextureSampler;

// How should shaders be passed here? Since we may have multiple backends, they
// might use different languages, and thus a general interface should either be
// able to translate between them (very hard, since it requires implementing a
// transpiler), or accept/reject when given a shader in some language.

class Shader
{
public:
    enum Stage
    {
        Vertex, Geometry, Fragment,
    };
    virtual ~Shader() {}

    // Currently all shaders are assumed to be GLSL shaders. Furthermore they
    // are assumed to be UTF-8-encoded.
    bool addShader(std::istream& input, Stage stage);
    virtual bool addShader(const std::string& code, Stage stage) = 0;
    virtual bool compile() = 0;

    virtual AttributeID attributeID(const std::string& name) const = 0;
    virtual const AttributeInformation& attribute(AttributeID) const = 0;
    virtual AttributeID attributeCount() const = 0;

    virtual UniformID uniformID(const std::string& name) const = 0;
    virtual const UniformInformation& uniform(UniformID) const = 0;
    virtual UniformID uniformCount() const = 0;

    virtual TextureID textureID(const std::string& name) const
    {
        return uniformID(name);
    }

    virtual void useTexture(TextureID,
                            const Texture1D*,
                            const TextureSampler*) = 0;
    virtual void useTexture(TextureID,
                            const Texture2D*,
                            const TextureSampler*) = 0;
    virtual void useTexture(TextureID,
                            const Texture3D*,
                            const TextureSampler*) = 0;
    virtual void useTexture(TextureID, const FramebufferTexture*) = 0;

    virtual void setUniform(UniformID, math::mat4) = 0;
    virtual void setUniform(UniformID, math::mat3) = 0;
    virtual void setUniform(UniformID, math::mat2) = 0;
    virtual void setUniform(UniformID, math::vec4) = 0;
    virtual void setUniform(UniformID, math::vec3) = 0;
    virtual void setUniform(UniformID, math::vec2) = 0;
    virtual void setUniform(UniformID, float) = 0;

    virtual void setUniform(UniformID, math::ivec4) = 0;
    virtual void setUniform(UniformID, math::ivec3) = 0;
    virtual void setUniform(UniformID, math::ivec2) = 0;
    virtual void setUniform(UniformID, S32) = 0;

    virtual void setUniform(UniformID, math::uvec4) = 0;
    virtual void setUniform(UniformID, math::uvec3) = 0;
    virtual void setUniform(UniformID, math::uvec2) = 0;
    virtual void setUniform(UniformID, U32) = 0;
};

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_SHADER_HPP_INCLUDED

