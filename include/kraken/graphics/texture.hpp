#ifndef KRAKEN_GRAPHICS_TEXTURE_HPP_INCLUDED
#define KRAKEN_GRAPHICS_TEXTURE_HPP_INCLUDED

#include <kraken/types.hpp>
#include <kraken/image/image.hpp>
#include <cstdlib>
#include <vector>

namespace kraken
{

namespace graphics
{

// Specifies how channels are laid out in memory - BGRA means that we first have
// component #2, then #1, then #0 and finally #3.
enum class ChannelType
{
    Red, R = Red,
    RG, RGB, BGR, RGBA, BGRA, DepthStencil,
};

// Specifies what type each individual channel is - an RGB-texture with UShort
// is laid out in memory as rrggbbRRGGBB where
enum class TexelType
{
    UByte, Byte, UShort, Short, UInt, Int, Float, UInt24UInt8,
};

// This is the format that will be used to store the texture data and controls
// how it can be read.
enum class TexelStorageFormat
{
    R8, RG8, RGB8, RGBA8, SRGB8, SRGBA8,
    R32I, RG32I, RGB32I, RGBA32I,
    R32U, RG32U, RGB32U, RGBA32U,
    R32F, RG32F, RGB32F, RGBA32F,

    CompressedRed,
    CompressedRG,
    CompressedRGB,
    CompressedRGBA,
    CompressedSRGB,
    CompressedSRGBA,

    Depth24Stencil8,

    R = R8,
    Red = R,
    RG = RG8,
    RGB = RGB8,
    RGBA = RGBA8,
    SRGB = SRGB8,
    SRGBA = SRGBA8,
};

class FramebufferTexture {};

class Texture
{
public:
    Texture() :
        m_data{},
        m_dirty{true},
        m_channeltype{ChannelType::RGBA},
        m_texeltype{TexelType::UByte},
        m_storageformat{TexelStorageFormat::RGBA8}
    {
        m_data.resize(1);
    }
    virtual ~Texture() {}

    ChannelType channelType() const noexcept
    {
        return m_channeltype;
    }
    TexelType texelType() const noexcept
    {
        return m_texeltype;
    }
    TexelStorageFormat texelStorageFormat() const noexcept
    {
        return m_storageformat;
    }
    size_t texelSize() const noexcept
    {
        size_t channels = 1;
        switch (channelType())
        {
        default: case ChannelType::R:
            break;
        case ChannelType::RG:
            channels = 2;
            break;
        case ChannelType::RGB: case ChannelType::BGR:
            channels = 3;
            break;
        case ChannelType::RGBA: case ChannelType::BGRA:
            channels = 4;
            break;
        }
        size_t bytes = 1;
        switch (texelType())
        {
        default: case TexelType::UByte: case TexelType::Byte:
            break;
        case TexelType::UShort: case TexelType::Short:
            bytes = 2;
            break;
        case TexelType::UInt: case TexelType::Int: case TexelType::Float:
            bytes = 4;
            break;
        }
        return channels * bytes;
    }
    U32 mipmapCount() const noexcept
    {
        return m_data.size();
    }
    size_t size(U32 mipmap = 0) const
    {
        return m_data.at(mipmap).size();
    }
    const void* data(U32 mipmap = 0) const
    {
        return m_data.at(mipmap).data();
    }
    void* data(U32 mipmap = 0)
    {
        m_dirty.at(mipmap) = true;
        return m_data.at(mipmap).data();
    }

    void setFormat(ChannelType ct, TexelType tt, TexelStorageFormat tsf)
    {
        std::fill(m_dirty.begin(), m_dirty.end(), true);
        m_channeltype = ct;
        m_texeltype = tt;
        m_storageformat = tsf;
    }

    /*
    bool generateMipmaps(U32 maxLevel = 64)
    {
        U32 oldLevel = mipmapCount();
        allocateMipmaps(maxLevel);
        return onGenerateMipmaps(oldLevel, maxLevel);
    }*/
    void allocateMipmaps(U32 maxLevel = 64)
    {
        U32 oldLevel = mipmapCount();
        m_data.resize(maxLevel);
        m_dirty.resize(maxLevel, true);
        onAllocateMipmaps(oldLevel, maxLevel);
    }

    void clear()
    {
        onClear();
        m_data.clear();
        m_dirty = {true};
        m_data.resize(1);
    }

    void removeMipmaps(U32 first = 1)
    {
        if (!first)
        {
            return clear();
        }
        onMipmapRemoval(first);
        if (mipmapCount() >= first)
        {
            m_data.erase(m_data.begin()+first);
        }
    }

    virtual void update() = 0;
    virtual void updateMipmap(U32 mipmap) = 0;

protected:
    bool isDirty(U32 mipmap)
    {
        return m_dirty.at(mipmap);
    }

    void markUpdated(U32 mipmap)
    {
        m_dirty.at(mipmap) = false;
    }

    std::vector<U8>& mipmapData(U32 idx) { return m_data.at(idx); }

    //virtual bool onGenerateMipmaps(U32 oldLevel, U32 newMaxLevel) = 0;
    virtual void onAllocateMipmaps(U32 oldLevel, U32 newMaxLevel) = 0;

    virtual void onMipmapRemoval(U32 /*first*/) {}
    virtual void onClear() {}

private:
    std::vector<std::vector<U8>> m_data;
    std::vector<bool> m_dirty;
    ChannelType m_channeltype;
    TexelType m_texeltype;
    TexelStorageFormat m_storageformat;
};

class Texture1D : public Texture
{
public:
    virtual ~Texture1D() {}

    virtual void setSize(U32 width_) = 0;
    virtual U32 width() const = 0;
};

class Texture2D : public Texture
{
public:
    virtual ~Texture2D() {}

    bool createFromImage(const image::Image& img);

    virtual void setSize(U32 width_, U32 height_) = 0;
    virtual U32 width() const = 0;
    virtual U32 height() const = 0;
};

class Texture3D : public Texture
{
public:
    virtual ~Texture3D() {}

    virtual void setSize(U32 width_, U32 height_, U32 depth_) = 0;
    virtual U32 width() const = 0;
    virtual U32 height() const = 0;
    virtual U32 depth() const = 0;
};


} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_TEXTURE_HPP_INCLUDED

