#ifndef KRAKEN_GRAPHICS_TEXTURESAMPLER_HPP_INCLUDED
#define KRAKEN_GRAPHICS_TEXTURESAMPLER_HPP_INCLUDED

#include <kraken/types.hpp>

namespace kraken
{

namespace graphics
{

enum class InterpolationFilter
{
    Nearest,
    Linear,
    NearestTexelNearestMipmap,
    LinearTexelNearestMipmap,
    NearestTexelLinearMipmap,
    LinearTexelLinearMipmap
};

enum class WrapMode
{
    EdgeClamp,
    MirrorRepeat,
    Repeat
};

class TextureSampler
{
public:
    virtual ~TextureSampler() {}

    virtual void setMinFilter(InterpolationFilter) = 0;
    virtual void setMagFilter(InterpolationFilter) = 0;
    virtual void setMinMipmap(U32 mipmap) = 0;
    virtual void setMaxMipmap(U32 mipmap) = 0;
    virtual void setSWrapMode(WrapMode) = 0;
    virtual void setTWrapMode(WrapMode) = 0;
    virtual void setPWrapMode(WrapMode) = 0;
    void setWrapMode(WrapMode wm)
    {
        setSWrapMode(wm);
        setTWrapMode(wm);
        setPWrapMode(wm);
    }

    virtual InterpolationFilter minFilter() const = 0;
    virtual InterpolationFilter magFilter() const = 0;
    virtual U32 minMipmap() const = 0;
    virtual U32 maxMipmap() const = 0;
    virtual WrapMode sWrapMode() const = 0;
    virtual WrapMode tWrapMode() const = 0;
    virtual WrapMode pWrapMode() const = 0;
};

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_TEXTURESAMPLER_HPP_INCLUDED

