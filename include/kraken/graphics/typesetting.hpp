#ifndef KRAKEN_GRAPHICS_TYPESETTING_HPP_INCLUDED
#define KRAKEN_GRAPHICS_TYPESETTING_HPP_INCLUDED

#include <kraken/graphics/mesh.hpp>
#include <kraken/graphics/typesetting_fwd.hpp>
#include <kraken/graphics/videodriver.hpp>
#include <kraken/math/matrix.hpp>
#include <kraken/utility/string.hpp>

#include <functional>
#include <map>
#include <limits>

namespace kraken
{

namespace graphics
{

namespace opengl
{

class Driver;

} // namespace opengl

class Font;
using FontList = std::vector<std::pair<size_t, Font*>>;

class TextGeometry
{
public:
    struct Geometry
    {
        std::unique_ptr<Mesh> mesh;
        const Font* font;
        U32 count;
    };

    TextGeometry(VideoDriver* driver) { m_driver = driver; }
    void append(const TextMetrics& metrics, math::ivec2 position,
                Alignment anchor = Alignment::TopLeft);

    std::vector<Geometry> geometry;

    math::ivec2 offset() const { return m_topLeft; }

private:
    math::ivec2 m_topLeft{0, 0};
    math::ivec2 m_bottomRight{0, 0};
    VideoDriver* m_driver;
};


// Passing raw integer positions and sizes is about 10-15% faster than
// passing two float rectangles (render quad + texCoordinates).
struct GlyphData
{
    math::ivec2 pos;
    math::uvec2 texData;
};
static_assert(sizeof(GlyphData) == 16, "Size.");
static_assert(sizeof(GlyphData[2]) == 2*sizeof(GlyphData), "Padding.");

class TextMetrics
{
public:
    struct Metric
    {
        math::ivec2 pos;
        U32 codepoint;
        U16 fontHeight;
        U16 width;
        U16 height;
    };
    struct Range
    {
        size_t begin; // First element index.
        size_t end; // Last element index + 1
    };

    static constexpr size_t npos = std::numeric_limits<size_t>::max();

    size_t size() const
    {
        return m_positions.size();
    }

    std::map<Font*, std::vector<Range>>::iterator fontBegin()
    {
        return m_fonts.begin();
    }

    std::map<Font*, std::vector<Range>>::iterator fontEnd()
    {
        return m_fonts.end();
    }

    std::map<Font*, std::vector<Range>>::const_iterator fontBegin() const
    {
        return m_fonts.begin();
    }

    std::map<Font*, std::vector<Range>>::const_iterator fontEnd() const
    {
        return m_fonts.end();
    }

    void moveRelative(size_t idx, math::ivec2 offset_)
    {
        m_positions[idx].pos += offset_;
    }

    Metric metrics(size_t idx) const
    {
        return m_positions[idx];
    }

    const math::ivec2& operator[](size_t idx) const { return m_positions[idx].pos; }

    U32 width() const { return m_width; }
    U32 height() const { return m_height; }

    math::ivec2 offset() const
    {
        return m_offset;
    }

private:
    friend TextMetrics typeset(const String& text,
                               TextAlignment align,
                               std::function<Font*(size_t)> fontfunc,
                               std::function<U32(size_t)> height,
                               U32 maxWidth);

    void setFont(Font* font, size_t begin, size_t end)
    {
        m_fonts[font].push_back({begin, end});
    }
    void append(U32 codepoint, math::ivec2 pos, U16 fHeight, U16 w, U16 h)
    {
        m_positions.push_back({pos, codepoint, fHeight, w, h});
    }

    void updateWidth(U32 w)
    {
        m_width = w;
    }
    void updateHeight(U32 h)
    {
        m_height = h;
    }

    void setOffset(math::ivec2 offset_)
    {
        m_offset = offset_;
    }

    // pair of {position, glyph}
    std::vector<Metric> m_positions;
    std::map<Font*, std::vector<Range>> m_fonts;
    math::ivec2 m_offset = math::ivec2{0, 0};
    U32 m_width = 0;
    U32 m_height = 0;
};

/// \brief Typesets a text (positions every character). Currently only typesets
/// horizontal text left-to-right.
///
/// \param text Text to typeset. Note that only "\n" signifies a newline.
/// \param align Text alignment. E.g. left-aligned (giving a ragged right).
/// \param font Function that given an index to a character in the text returns
///        a pointer to the font that will be used for rendering that portion of
///        the text (used for measurement, kerning distances etc.). Do note that
///        this function will be called in a weakly increasing manner - that is,
///        if the function is called with argument x, then it will only ever be
///        called with an argument larger than or equal to x.
/// \param height Function that returns line height in pixels. Affects glyph
///        sizes. Subject to the same conditions as the font function (except
///        for return type).
/// \param maxWidth Maximum width of text. 0 means unlimited (no line-wrapping).
/// \return Metrics describing positions of every glyph along with any auxiliary
///         information that may be needed for rendering.
TextMetrics typeset(const String& text,
                    TextAlignment align,
                    std::function<Font*(size_t)> font,
                    std::function<U32(size_t)> height,
                    U32 maxWidth = 0);

math::ivec2 getOffset(Alignment align, math::ivec2 size);

//TextGeometry createRenderGeometry(const TextMetrics&, VideoDriver*);

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_TYPESETTING_HPP_INCLUDED

