#ifndef KRAKEN_GRAPHICS_TYPESETTING_FWD_HPP_INCLUDED
#define KRAKEN_GRAPHICS_TYPESETTING_FWD_HPP_INCLUDED

namespace kraken
{

namespace graphics
{

enum class TextAlignment
{
    Left,
    Center,
    Right,
    Justified
};

enum class Alignment
{
    TopLeft,    TopCenter,    TopRight,
    CenterLeft, Center,       CenterRight,
    BottomLeft, BottomCenter, BottomRight,
};

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_TYPESETTING_FWD_HPP_INCLUDED

