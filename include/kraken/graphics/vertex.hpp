#ifndef KRAKEN_GRAPHICS_VERTEX_HPP_INCLUDED
#define KRAKEN_GRAPHICS_VERTEX_HPP_INCLUDED

#include "kraken/math/matrix.hpp"
#include <kraken/geometry/vertex.hpp>

namespace kraken::graphics
{

struct Vertex
{
    math::vec3 position;
    math::vec3 normal;
    math::vec2 tcoord;
};

using ::kraken::geometry::VertexDataType;
using ::kraken::geometry::Attribute;
using ::kraken::geometry::VertexType;
using ::kraken::geometry::PrimitiveType;

} // namespace kraken::graphics

#endif // KRAKEN_GRAPHICS_VERTEX_HPP_INCLUDED

