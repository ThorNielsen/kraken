#ifndef KRAKEN_GRAPHICS_VIDEODRIVER_HPP_INCLUDED
#define KRAKEN_GRAPHICS_VIDEODRIVER_HPP_INCLUDED

#include <kraken/graphics/typesetting_fwd.hpp>
#include <kraken/graphics/vertex.hpp>
#include <kraken/window/window.hpp>
#include <kraken/image/colour.hpp>
#include <kraken/math/matrix.hpp>
#include <kraken/utility/string.hpp>

#include <memory>

namespace kraken
{

namespace graphics
{

class Font;
class TextMetrics;
class TextGeometry;

class Framebuffer;
class Mesh;
class Shader;
class Texture1D;
class Texture2D;
class Texture3D;
class TextureSampler;

enum class Capability
{
    DepthTest,
    AlphaTest
};

enum class CullMode
{
    None,
    Front,
    Back,
    FrontAndBack
};

enum class RenderMode
{
    Solid,
    Wireframe,
    Points
};

enum class WindingOrder
{
    CW, CCW,
    Clockwise = CW,
    Counterclockwise = CCW
};

class VideoDriver
{
public:
    static VideoDriver* driver(const window::Window& wnd)
    {
        return static_cast<VideoDriver*>(wnd.windowExtra());
    }

    virtual ~VideoDriver() {}

    /// \brief Begins rendering of the current frame.
    ///
    /// \param clearCBuffer Whether the colours from last frame should be reset.
    /// \param clearZBuffer Whether the last frame's depth should be cleared.
    /// \param background Background colour to use for clearing.
    /// \return If everything succeeded, true, otherwise false.
    virtual bool beginDraw(bool clearCBuffer = true,
                           bool clearZBuffer = true,
                           image::Colour background = 0x000000ff,
                           image::ColourType colourType = image::RGBA8) = 0;

    /// \brief Displays currently rendered frame.
    ///
    /// \return If everything succeeded, true, otherwise false.
    virtual bool endDraw() = 0;

    virtual std::unique_ptr<Framebuffer> createFramebuffer(math::uvec2 dim) = 0;
    virtual std::unique_ptr<Mesh> createMesh() = 0;
    virtual std::unique_ptr<Shader> createShader() = 0;
    virtual std::unique_ptr<Texture1D> create1DTexture() = 0;
    virtual std::unique_ptr<Texture2D> create2DTexture() = 0;
    virtual std::unique_ptr<Texture3D> create3DTexture() = 0;
    virtual std::unique_ptr<TextureSampler> createTextureSampler() = 0;

    virtual void drawVertices(const void* vertices,
                              VertexType vertexType,
                              U64 vertexCount,
                              const void* indices,
                              U64 indexByteWidth,
                              U64 indexCount,
                              PrimitiveType pt) = 0;

    virtual void drawVertices(const void* vertices,
                              VertexType vType,
                              U64 vCount,
                              PrimitiveType pt) = 0;

    virtual bool use(Shader*) = 0;
    virtual const Shader* currentShader() const = 0;
    virtual Shader* currentShader() = 0;

    virtual bool useFramebuffer(Framebuffer*,
                                bool useAsInput = true,
                                bool useAsOutput = true) = 0;
    virtual const Framebuffer* currentReadFramebuffer() const = 0;
    virtual Framebuffer* currentReadFramebuffer() = 0;
    virtual const Framebuffer* currentWriteFramebuffer() const = 0;
    virtual Framebuffer* currentWriteFramebuffer() = 0;

    virtual U32 width() const = 0;
    virtual U32 height() const = 0;

    virtual void enable(Capability) = 0;
    virtual void disable(Capability) = 0;
    virtual bool isEnabled(Capability) const = 0;

    virtual void setWindingOrder(WindingOrder) = 0;
    virtual void setRenderMode(RenderMode) = 0;
    virtual void setCullMode(CullMode) = 0;

    virtual WindingOrder windingOrder() const = 0;
    virtual RenderMode renderMode() const = 0;
    virtual CullMode cullMode() const = 0;

    template <typename Object>
    void draw(const Object& object)
    {
        if (m_fontRenderMode)
        {
            endTextRendering();
            m_fontRenderMode = false;
        }
        object->draw(this);
    }

    void draw(const TextMetrics& metrics, math::ivec2 offset = {0, 0},
              Alignment align = Alignment::TopLeft);
    void draw(const TextGeometry& geom, math::ivec2 offset = {0, 0})
    {
        if (!m_fontRenderMode)
        {
            setupTextRendering();
            m_fontRenderMode = true;
        }
        drawText(geom, offset);
    }

    // Todo: Get fonts by name, prevent duplicate fonts, etc.

    Font* loadFont(const std::string& path);
    Font* loadFont(const U8* data, U64 length);

    void destroyFont(Font* font);

protected:
    virtual void setupTextRendering() = 0;
    virtual void drawText(const TextGeometry& geom, math::ivec2 offset) = 0;
    void endTextRendering()
    {
        m_fontRenderMode = false;
        onEndTextRendering();
    }
    virtual void onEndTextRendering() = 0;

    bool isRenderingText() const { return m_fontRenderMode; }

private:
    struct FontWrapper
    {
        FontWrapper() : font{nullptr} {}
        FontWrapper(FontWrapper&& other)
        {
            font = other.font;
            other.font = nullptr;
        }
        FontWrapper& operator=(FontWrapper&& other)
        {
            font = other.font;
            other.font = nullptr;
            return *this;
        }
        ~FontWrapper();
        Font* font;
    };
    std::vector<FontWrapper> m_fonts;
    bool m_fontRenderMode = false;
};

} // namespace graphics

} // namespace kraken

#endif // KRAKEN_GRAPHICS_VIDEODRIVER_HPP_INCLUDED

