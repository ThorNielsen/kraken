#ifndef KRAKEN_IMAGE_COLOUR_HPP_INCLUDED
#define KRAKEN_IMAGE_COLOUR_HPP_INCLUDED

#include "kraken/math/matrix.hpp"
#include <kraken/types.hpp>

#include <iosfwd>

namespace kraken::image
{

// Note that valid colour types are those with 1 <= channels <= 4 and
// 1 <= bitDepth <= 16. However, note that in many cases, it is beneficial to
// restrict oneself to using one of the following combinations:
// 1 channel: Bit depths of 1, 2, 4, 8 and 16
// 2 channels: Bit depths of 1, 2, 4, 8 and 16
// 3 channels: Bit depths of 8 and 16
// 4 channels: Bit depths of 1, 2, 4, 8 and 16
// The reason for these constraints is that it allows tight and efficient
// packing of many colours with these values while still allowing a single
// colour to be stored in a machine word (assuming 64 bits, which most modern
// processors support). The reason for not supporting depths of >16 bits is that
// this would remove many shortcuts in conversion between different formats and
// also extraction of pixels in an image.
struct ColourType
{
    U8 channels;
    U8 bitDepth; // Per-channel bit depth
};

std::ostream& operator<<(std::ostream&, const ColourType&);

inline bool operator==(const ColourType& a, const ColourType& b) noexcept
{
    return a.channels == b.channels && a.bitDepth == b.bitDepth;
}

inline bool operator!=(const ColourType& a, const ColourType& b) noexcept
{
    return a.channels != b.channels || a.bitDepth != b.bitDepth;
}

// The allowed colour types are also enumerated here for convenience:
static constexpr ColourType R1{1,1};
static constexpr ColourType R2{1,2};
static constexpr ColourType R4{1,4};
static constexpr ColourType R8{1,8};
static constexpr ColourType R16{1,16};
static constexpr ColourType RG1{2,1};
static constexpr ColourType RG2{2,2};
static constexpr ColourType RG4{2,4};
static constexpr ColourType RG8{2,8};
static constexpr ColourType RG16{2,16};
static constexpr ColourType RGB{3,8};
static constexpr ColourType RGB1{3,1};
static constexpr ColourType RGB2{3,2};
static constexpr ColourType RGB4{3,4};
static constexpr ColourType RGB8{3,8};
static constexpr ColourType RGB16{3,16};
static constexpr ColourType RGBA{4,8};
static constexpr ColourType RGBA1{4,1};
static constexpr ColourType RGBA2{4,2};
static constexpr ColourType RGBA4{4,4};
static constexpr ColourType RGBA8{4,8};
static constexpr ColourType RGBA16{4,16};

using Colour = U64;

Colour convert(Colour c, ColourType from, ColourType to) noexcept;

inline Colour pack(math::vec4 colour, ColourType desiredType) noexcept
{
    U32 mul = (1 << desiredType.bitDepth) - 1;
    U64 res = (U16)(colour.r*mul);
    res = (res << desiredType.bitDepth) | (U16)(colour.g*mul);
    res = (res << desiredType.bitDepth) | (U16)(colour.b*mul);
    res = (res << desiredType.bitDepth) | (U16)(colour.a*mul);

    // Since we do not necessarily need to output all channels, we simply
    // discard the superfluous channels. This way this function becomes branch-
    // less, *hopefully* speeding it up. Furthermore, it is expected that 3 and
    // 4 is the most used number of colour channels, thus most of the work will
    // not be wasted.
    return res >> ((4-desiredType.channels) * desiredType.bitDepth);
}

inline Colour pack(math::ivec4 colour, ColourType desiredType) noexcept
{
    U64 res = (U16)(colour.r);
    res = (res << desiredType.bitDepth) | (U16)(colour.g);
    res = (res << desiredType.bitDepth) | (U16)(colour.b);
    res = (res << desiredType.bitDepth) | (U16)(colour.a);

    // Same comments as the above pack()-function.
    return res >> ((4-desiredType.channels) * desiredType.bitDepth);
}

inline Colour pack(math::uvec4 colour, ColourType desiredType) noexcept
{
    U64 res = (U16)(colour.r);
    res = (res << desiredType.bitDepth) | (U16)(colour.g);
    res = (res << desiredType.bitDepth) | (U16)(colour.b);
    res = (res << desiredType.bitDepth) | (U16)(colour.a);

    // Same comments as the above pack()-function.
    return res >> ((4-desiredType.channels) * desiredType.bitDepth);
}

inline math::vec4 unpack(Colour colour, ColourType fmt) noexcept
{
    float mul = 1.f/((1 << fmt.bitDepth)-1);
    U16 mask = (1 << fmt.bitDepth) - 1;
    math::vec4 col{0.f, 0.f, 0.f, 1.f};
    switch (fmt.channels)
    {
    case 4:
        col.a = mul*(colour&mask);
        colour >>= fmt.bitDepth;
        // Fallthrough.
    case 3:
        col.b = mul*(colour&mask);
        colour >>= fmt.bitDepth;
        // Fallthrough.
    case 2:
        col.g = mul*(colour&mask);
        colour >>= fmt.bitDepth;
        // Fallthrough.
    case 1:
        col.r = mul*(colour&mask);
        //colour >>= fmt.bitDepth;
        // Fallthrough (to nothing).
    default:
        ;
    }
    return col;
}

inline math::ivec4 iunpack(Colour colour, ColourType fmt) noexcept
{
    U16 mask = (1 << fmt.bitDepth) - 1;
    math::ivec4 col{0, 0, 0, mask};
    switch (fmt.channels)
    {
    case 4:
        col.a = colour&mask;
        colour >>= fmt.bitDepth;
        // Fallthrough.
    case 3:
        col.b = colour&mask;
        colour >>= fmt.bitDepth;
        // Fallthrough.
    case 2:
        col.g = colour&mask;
        colour >>= fmt.bitDepth;
        // Fallthrough.
    case 1:
        col.r = colour&mask;
        //colour >>= fmt.bitDepth;
        // Fallthrough (to nothing).
    default:
        ;
    }
    return col;
}

inline math::uvec4 uunpack(Colour colour, ColourType fmt) noexcept
{
    U16 mask = (1 << fmt.bitDepth) - 1;
    math::uvec4 col{0, 0, 0, mask};
    switch (fmt.channels)
    {
    case 4:
        col.a = colour&mask;
        colour >>= fmt.bitDepth;
        // Fallthrough.
    case 3:
        col.b = colour&mask;
        colour >>= fmt.bitDepth;
        // Fallthrough.
    case 2:
        col.g = colour&mask;
        colour >>= fmt.bitDepth;
        // Fallthrough.
    case 1:
        col.r = colour&mask;
        //colour >>= fmt.bitDepth;
        // Fallthrough (to nothing).
    default:
        ;
    }
    return col;
}

} // namespace kraken::image

#endif // KRAKEN_IMAGE_COLOUR_HPP_INCLUDED
