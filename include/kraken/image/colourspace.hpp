#ifndef KRAKEN_IMAGE_COLOURSPACE_HPP_INCLUDED
#define KRAKEN_IMAGE_COLOURSPACE_HPP_INCLUDED

#include <kraken/math/comparison.hpp>
#include <kraken/types.hpp>

namespace kraken::image
{

enum class ColourSpace : U32
{
    LinearRGB = 0,
    sRGB,
    YCbCrJPEG,
};

/// Converts a colour from one space to another.
///
/// This is the "mother" conversion function whose only purpose is to define an
/// interface for specialisations; so it will be a compile-time error if one
/// attempts to perform an invalid conversion, or one that hasn't been
/// implemented yet.
///
/// \remark This function only deals with 3-component to 3-component conversion,
///         so exotic stuff like RGB to CMYK is not supported by this function.
///         (And for CMYK in particular there isn't even a well-defined single
///         version of it; it has merely been included here for convenience.
///
/// \remark The channel naming here is generic; in specialisations it may differ
///         for obvious reasons.
///
/// \tparam CSFrom Source colour space.
/// \tparam CSTo Destination colour space.
/// \tparam Prec The data type used by the colour channels.
///
/// \param c0 First input channel.
/// \param c1 Second input channel.
/// \param c2 Third input channel.
/// \param o0 First output channel.
/// \param o1 Second output channel.
/// \param o2 Third output channel.
template <ColourSpace CSFrom, ColourSpace CSTo, typename Prec>
void convert(Prec c0, Prec c1, Prec c2,
             Prec& o0, Prec& o1, Prec& o2) noexcept = delete;


template<>
inline void convert<ColourSpace::YCbCrJPEG, ColourSpace::sRGB, U8>
(U8 y, U8 cb, U8 cr, U8& r, U8& g, U8& b) noexcept
{
    auto luma = (F32)y;
    auto cbn = (F32)cb - 128.f;
    auto crn = (F32)cr - 128.f;

    r = math::clamp(luma                 +    1.402f*crn+.5f, 0.f, 255.f);
    g = math::clamp(luma - 0.344136f*cbn - 0.714136f*crn+.5f, 0.f, 255.f);
    b = math::clamp(luma +    1.772f*cbn                +.5f, 0.f, 255.f);
}

} // namespace kraken::image

#endif // KRAKEN_IMAGE_COLOUR_HPP_INCLUDED
