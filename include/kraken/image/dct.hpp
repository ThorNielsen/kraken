#ifndef KRAKEN_IMAGE_DCT_HPP_INCLUDED
#define KRAKEN_IMAGE_DCT_HPP_INCLUDED

#include <kraken/types.hpp>

namespace kraken::image
{

/// Computes the JPEG inverse DCT of the input block naively.
///
/// \remark This is NOT meant for performance-critical work, nor indeed anywhere
///         where performance is even slightly important. Instead it is meant as
///         a dead-simple easy-to-verify-correctness-of function that can be
///         used as a basis for comparison when doing further tests.
///
/// \param inputBuf Pointer to the array containing the 8x8 DCT coefficients;
///                 laid out row-by-row (so the first eight elements constitute
///                 the first row, the next eight the second row, etc.). Note
///                 that this MUST have exactly 64 elements (more is fine, but
///                 won't be used), as otherwise this WILL access invalid
///                 memory.
/// \param outputBuf Destination for the decoded data; will be level shifted and
///                  rounded appropriately. Layout is identical to inputBuf;
///                  note that there must be 64 writable elements for this not
///                  to access invalid memory.
void jpeg8x8IDCTNaive(const F32* inputBuf, U8* outputBuf);

/// Computes the JPEG inverse DCT of the input block in a much faster way.
///
/// The "much faster" algorithm used here is not that magical -- it is merely
/// using a simple factorisation of the 2d DCT into 2 1D DCTs of all rows.
///
/// \param inputBuf Pointer to the array containing the 8x8 DCT coefficients;
///                 laid out row-by-row (so the first eight elements constitute
///                 the first row, the next eight the second row, etc.). Note
///                 that this MUST have exactly 64 elements (more is fine, but
///                 won't be used), as otherwise this WILL access invalid
///                 memory.
/// \param outputBuf Destination for the decoded data; will be level shifted and
///                  rounded appropriately. Layout is identical to inputBuf;
///                  note that there must be 64 writable elements for this not
///                  to access invalid memory.
void jpeg8x8IDCTFactoredNaive(const F32* inputBuf, U8* outputBuf);

/// Functionally identical to the other JPEG IDCTs.
///
/// This explicitly uses a factorisation into 1D IDCTs computed separately, but
/// also very naïvely.
///
/// \param inputBuf As in jpeg8x8IDCTNaive.
/// \param outputBuf As in jpeg8x8IDCTNaive.
void jpeg8x8IDCTBy1DFactorisationNaive(const F32* inputBuf, U8* outputBuf);

/// Functionally identical to the other JPEG IDCTs.
///
/// This explicitly uses a factorisation into 1D IDCTs computed separately,
/// where each of the 1D computations are done using the algorithm of Loeffler
/// et al.
///
/// \param inputBuf As in jpeg8x8IDCTNaive.
/// \param outputBuf As in jpeg8x8IDCTNaive.
void jpeg8x8IDCTBy1DFactorisationFast(const F32* inputBuf, U8* outputBuf);

} // namespace kraken::image

#endif // KRAKEN_IMAGE_DCT_HPP_INCLUDED
