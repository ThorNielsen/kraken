#ifndef KRAKEN_IMAGE_IMAGE_HPP_INCLUDED
#define KRAKEN_IMAGE_IMAGE_HPP_INCLUDED

#include <kraken/image/colour.hpp>
#include <kraken/image/colourspace.hpp>

#include <kraken/utility/string.hpp>

#include <functional>
#include <iosfwd>
#include <map>
#include <optional>
#include <vector>

namespace kraken
{

namespace image
{

class Image
{
public:
    Image()
        : Image(0, 0, RGB)
    {}
    Image(U32 desiredWidth,
          U32 desiredHeight,
          ColourType ctype,
          ColourSpace space = ColourSpace::sRGB)
        : m_colSpace{space}
    {
        resize(desiredWidth, desiredHeight, ctype);
    }
    Image(const Image&);
    Image(Image&&);
    Image& operator=(const Image&);
    Image& operator=(Image&&);

    void resize(U32 newWidth, U32 newHeight, ColourType newCType);
    Image crop(U32 newWidth, U32 newHeight,
               U32 xOffset = 0, U32 yOffset = 0, Colour replacementCol = 0);

    [[nodiscard]] U32 width() const noexcept { return m_width; }
    [[nodiscard]] U32 height() const noexcept { return m_height; }
    [[nodiscard]] ColourType colourType() const noexcept { return m_ctype; }
    [[nodiscard]] ColourSpace colourSpace() const noexcept { return m_colSpace; }

    void fill(const std::function<Colour(U32, U32)>& fillfunc);
    void fill(const std::function<math::vec4(U32, U32)>& fillfunc);
    void fill(const std::function<math::ivec4(U32, U32)>& fillfunc);
    void fill(const std::function<math::uvec4(U32, U32)>& fillfunc);

    // Note: This always gives/sets the raw representation in the image.
    // That is, if the image uses a palette, then the given/returned values are
    // always to be considered as indexes into the palette.
    [[nodiscard]] Colour getPixel(U32 x, U32 y) const noexcept
    {
        U64 bitColOffset = m_bpp * x;
        U64 offset = rowBytes() * y + (bitColOffset >> 3);
        Colour read = 0;
        switch (m_bpp)
        {
        case 8*8:
            read = m_data[offset++];
            read = (read << 8) | m_data[offset++];
            // Fallthrough
        case 6*8:
            read = (read << 8) | m_data[offset++];
            read = (read << 8) | m_data[offset++];
            // Fallthrough
        case 4*8:
            read = (read << 8) | m_data[offset++];
            // Fallthrough
        case 3*8:
            read = (read << 8) | m_data[offset++];
            // Fallthrough
        case 2*8:
            read = (read << 8) | m_data[offset++];
            // Fallthrough
        case 1*8:
            read = (read << 8) | m_data[offset++];
            return read;
        case 1: case 2: case 4:
        default: // 1, 2, 4:
            U8 insideOffset = bitColOffset & 7;
            return (m_data[offset] >> (8-m_bpp-insideOffset)) & m_mask;
        }
    }

    void setPixel(U32 x, U32 y, Colour col)
    {
        U64 bitColOffset = m_bpp * x;
        U64 offset = rowBytes() * y + (bitColOffset >> 3);
        U64 modOffset = offset + pixelBytes() - 1;
        switch (m_bpp)
        {
        case 8*8:
            m_data[modOffset--] = col & 0xff; col >>= 8;
            m_data[modOffset--] = col & 0xff; col >>= 8;
            // Fallthrough
        case 6*8:
            m_data[modOffset--] = col & 0xff; col >>= 8;
            m_data[modOffset--] = col & 0xff; col >>= 8;
            // Fallthrough
        case 4*8:
            m_data[modOffset--] = col & 0xff; col >>= 8;
            // Fallthrough
        case 3*8:
            m_data[modOffset--] = col & 0xff; col >>= 8;
            // Fallthrough
        case 2*8:
            m_data[modOffset--] = col & 0xff; col >>= 8;
            // Fallthrough
        case 1*8:
            m_data[modOffset--] = col & 0xff;// col >>= 8;
            break;
        case 1: case 2: case 4:
        default: // 1, 2, 4:
            U8 shift = 8-m_bpp-(bitColOffset & 7);
            U8 mask = m_mask << shift;
            m_data[offset] = (col << shift) | (m_data[offset] & ~mask);
        }
    }

    void clear()
    {
        resize(width(), height(), colourType());
        removeMetadata();
        removePalette();
    }

    bool read(std::istream& input);
    bool write(std::ostream& output, std::string fmt) const;

    [[nodiscard]] U64 rowBytes() const noexcept
    {
        return (m_ctype.bitDepth * m_ctype.channels * U64(m_width) + 7) >> 3;
    }

    void setMetadata(String key, String value);
    [[nodiscard]] bool hasMetadata(String key) const noexcept;
    [[nodiscard]] const String& getMetadata(String key) const;
    void removeMetadata();

    [[nodiscard]] U8* data() noexcept { return m_data.data(); }
    [[nodiscard]] const U8* data() const noexcept { return m_data.data(); }

    // Gives size of image in bytes.
    [[nodiscard]] U64 size() const noexcept { return m_data.size(); }

    void setGamma(F32 newGamma) noexcept { m_gamma = newGamma; }
    [[nodiscard]] F32 gamma() const noexcept { return m_gamma; }

    [[nodiscard]] bool usesPalette() const noexcept;
    void removePalette() noexcept;
    [[nodiscard]] size_t paletteCount() const noexcept;
    void createPalette(size_t elemCount);
    void setPaletteElement(size_t idx, Colour col);
    [[nodiscard]] Colour getPaletteElement(size_t idx) const;
    [[nodiscard]] ColourType paletteColourType() const noexcept;
    void setPaletteColourType(ColourType cType) noexcept;
    void convertPaletteColourType(ColourType cType) noexcept;

    void applyPalette();

    /// Sets the colour space completely disregarding the previous one.
    ///
    /// \remark Currently this is *very* lax and allows nonsensical colour
    ///         spaces, but in the future it might throw exceptions on absurd
    ///         ones, such as YCbCr on a 1-component image, etc.
    void setColourSpace(ColourSpace space) noexcept
    {
        m_colSpace = space;
    }

    // This converts the entire image to the new colour type specified. Note
    // that the returned image is never paletted; the palette is thrown away.
    [[nodiscard]] Image convert(ColourType newCType) const;

    /// Transforms the image from the current colour space to the given one.
    ///
    /// The transformation happens in-place, and this means it might fail due to
    /// that simply not being possible if the number of components are changed.
    /// However, another reason can also be that the appropriate conversion has
    /// simply not been implemented yet.
    ///
    /// \remark It is legal to call this with the same colour space as the
    ///         current; in which case the conversion turns into a no-op, and
    ///         this becomes equivalent to setGamma.
    ///
    /// \remark If conversion is not possible, then no data is modified. This
    ///         includes gamma, so if conversion is not possible, the gamma
    ///         won't be modified.
    ///
    /// \param dest Colour space to transform into.
    /// \param newGamma Desired new gamma (if applicable).
    ///
    /// \returns Whether the conversion was possible (and thus were performed)
    ///          or not.
    bool transform(ColourSpace dest,
                   std::optional<F32> newGamma = std::nullopt) noexcept;

private:
    void copyTriviallyDestructibleVariables(const Image& img);

    bool readBMP(std::istream& input);
    bool readJPEG(std::istream& input);
    bool readPNG(std::istream& input);
    bool reconstructPNGRow(std::vector<U8>& currRow,
                           const std::vector<U8>& prevRow,
                           U32 row,
                           U32 interlacePass,
                           U32 subrow,
                           bool interlaced);

    bool readPNM(std::istream& input, U8 variant);
    bool readPlainPNM(std::istream& input, U8 variant);
    bool readBinaryPNM(std::istream& input, U8 variant, bool rawReadPossible);

    bool writeBMP(std::ostream& output) const;
    bool writeBMPColourdata(std::ostream& output, U32 padBytes,
                            U32 rowWidth) const;
    void fillBMPPalette(U8* begin, U32 entries) const;
    bool writePNG(std::ostream& output) const;
    void writePNGScanline(U8* currLine, const U8* prevLine, U32 row,
                          U8 filterMethod = 0xff) const;
    bool writePNGChunk(std::ostream& output, const char name[4],
                       const U8* begin, const U8* end) const;

    bool writePNM(std::ostream& output) const;

    U64 pixelBytes() const
    {
        return (m_ctype.bitDepth * m_ctype.channels + 7) >> 3;
    }
    bool isColourTypeSupported(ColourType ctype) const noexcept;

    std::map<String, String> m_metadata;
    std::vector<Colour> m_palette;
    std::vector<U8> m_data;
    U64 m_bpp;
    U64 m_mask;
    U32 m_width;
    U32 m_height;
    F32 m_gamma;
    ColourType m_ctype;
    ColourType m_palCtype;
    ColourSpace m_colSpace;
};

} // namespace image

} // namespace kraken

#endif // KRAKEN_IMAGE_IMAGE_HPP_INCLUDED
