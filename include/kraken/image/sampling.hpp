#ifndef KRAKEN_IMAGE_SAMPLING_HPP_INCLUDED
#define KRAKEN_IMAGE_SAMPLING_HPP_INCLUDED

#include <kraken/image/image.hpp>

namespace kraken::image
{

enum class UpsamplingMethod : U32
{
    NearestNeighbour = 0,
    Nearest = NearestNeighbour,
    Linear = 1,
    Cubic = 2,
};

/// This upsamples a single component of the given image in-place.
///
/// As the image is not rescaled (but only the component), it is obviously
/// necessary to pack the samples inside the image, but this can be done in
/// several ways. This expects the samples in the appropriate component to be
/// spaced regularly within the image with the frequency specified by the
/// stretch factors. So for example, if we have a 23x5 image with the stretch
/// factors (3, 2), then the following samples are the ones to be filled in:
/// \code
///     X..X..X..X..X..X..X..X.
///     .......................
///     X..X..X..X..X..X..X..X.
///     .......................
///     X..X..X..X..X..X..X..X.
/// \endcode
/// Obviously the above image solely shows the samples of a single component,
/// which is itself possibly interleaved with other components. It is guaranteed
/// that this does not *read* from any of the unused samples, and it is thus
/// fine for those to contain uninitialised memeory, except if the sample size
/// of the component is not an integer number of bytes (as widening and
/// and subsequent narrowing will then be performed for *all* samples).
///
/// \remark This can solely be used for upsampling by an integer multiple; for
///         example to scale a 7x3 image to a 14x9 one. Observe, though, that it
///         DOES NOT rescale the actual image, but rather interpolates a certain
///         subset of the already contained components.
///
/// \remark To enable an optimised implementation, the component in question
///         must be byte-sized (i.e. its bit width must be divisible by 8), and
///         aligned to byte boundaries. If it isn't, the image will undergo a
///         widening conversion, then the upsampling followed by a narrowing
///         conversion.
///
/// \param img An image with the samples of the given component arranged as
///            described in the general section.
/// \param method The upscaling method to use for this component.
/// \param component Index of the component.
/// \param stretchFactorX Number of pixels a new sample should occupy.
/// \param stretchFactorY As stretchFactorX.
void upsampleComponentInplace(Image& img,
                              UpsamplingMethod method,
                              size_t component,
                              size_t stretchFactorX,
                              size_t stretchFactorY);

} // namespace kraken::image

#endif // KRAKEN_IMAGE_SAMPLING_HPP_INCLUDED
