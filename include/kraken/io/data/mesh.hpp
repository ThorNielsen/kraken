#ifndef KRAKEN_IO_DATA_MESH_HPP_INCLUDED
#define KRAKEN_IO_DATA_MESH_HPP_INCLUDED

#include <kraken/types.hpp>

#include <kraken/io/data/range.hpp>
#include <kraken/io/data/types.hpp>

#include <functional>
#include <map>
#include <numeric>
#include <optional>
#include <string>
#include <tuple>

namespace kraken::io
{

// The special type “Polygons” is intended to hold any general polygon without
// a fixed number of edges. While the remainder is trivially usable for GPU
// rendering, “Polygons” is intended for avoiding automatic triangulation and
// instead being able to process the input data (in case it is not triangulated
// originally).
enum class MeshPrimitiveType : U32
{
    Points = 0,
    LineSegments,
    LineStrips,
    Triangles,
    TriangleStrips,
    TriangleFans,
    Polygons,
};

// This identifies different usage types for a vertex component (e.g. if it is a
// position, normal, etc.).
enum class VertexAttributeUsageType : U32
{
    // These initially follow the glTF specification, and the meanings in that
    // specification are also intended here.
    Position = 0,
    Normal = 1,
    Tangent = 2,
    Texcoord = 3,
    Colour = 4,
    Joint = 5,
    Weight = 6,

    KnownTypeCount,
    // If this bit is set (and this is NOT equal to Invalid nor Unknown, then
    // the numeric value is equal to ExtendedBit | Index, where Index is the
    // index in some external control structure describing the usages not
    // enumerated here.
    ExtendedBit = 0x8000'0000,

    // Indicates that the component's usage is valid (i.e. this is not padding),
    // but the exact meaning is unknown, and no extended meaning is available.
    Unknown = 0xffff'fffe,

    // Invalid is used for densely packed component usage types to represent
    // padding, etc.
    Invalid = 0xffff'ffff,
};

[[nodiscard]] constexpr VertexAttributeUsageType makeExtendedAttributeType(U32 index) noexcept
{
    using UsageType = VertexAttributeUsageType;
    return static_cast<UsageType>(static_cast<U32>(UsageType::ExtendedBit)
                                  | index);
}

[[nodiscard]] constexpr U32 getExtendedIndex(VertexAttributeUsageType usage) noexcept
{
    return static_cast<U32>(usage)
         &~ static_cast<U32>(VertexAttributeUsageType::ExtendedBit);
}

/// Finds the canonical name for the usage type if available.
///
/// \remark If there is no name available, nullptr is returned, which,
///         crucially, is distinct from the empty string (the empty string is
///         a dereferencable pointer to a null byte, nullptr is decidedly NOT).
///
/// \remark Extended types are not supported.
///
/// \returns A pointer to a zero-terminated string IF there is a known canonical
///          name otherwise nullptr.
[[nodiscard]] const char* getCanonicalName(VertexAttributeUsageType usageType) noexcept;

[[nodiscard]] constexpr bool isKnownType(VertexAttributeUsageType tp) noexcept
{
    return static_cast<U32>(tp) < static_cast<U32>(VertexAttributeUsageType::KnownTypeCount);
}

class ExtendedAttributeNames
{
public:
    VertexAttributeUsageType makeExtendedUsage(std::string_view name);

    std::optional<std::string_view>
    extendedName(VertexAttributeUsageType attrType) const noexcept;

private:
    std::vector<std::string> m_names;
    std::map<std::string, U32, std::less<>> m_indexToName;
};

// Describes a vertex attribute, potentially with multiple components.
struct VertexAttribute
{
    using enum VertexAttributeUsageType;

    PrimitiveDataType dataType;
    VertexAttributeUsageType usage;
    U8 componentCount;
    U8 setIndex;

    [[nodiscard]] std::size_t byteSize() const noexcept
    {
        return componentCount * ::kraken::io::byteSize(dataType);
    }

    [[nodiscard]] auto operator<=>(const VertexAttribute& other) const noexcept
    {
        return std::tie(usage, componentCount, setIndex, dataType)
           <=> std::tie(other.usage, other.componentCount, other.setIndex, other.dataType);
    }
    [[nodiscard]] bool operator==(const VertexAttribute&) const noexcept = default;
    [[nodiscard]] bool operator!=(const VertexAttribute&) const noexcept = default;
};

// The potentially extended names are external to this.
struct VertexLayout
{
    // Attributes; padding is represented with “Invalid” usages.
    std::vector<VertexAttribute> attributes;

    std::size_t attributeCount() const noexcept { return attributes.size(); }

    std::size_t vertexStride() const noexcept
    {
        return std::accumulate(attributes.begin(),
                               attributes.end(),
                               std::size_t{0},
                               [](auto offset, const auto& attr) noexcept
                               {
                                   return offset + attr.byteSize();
                               });
    }

    [[nodiscard]] auto operator<=>(const VertexLayout& other) const noexcept = default;

    template <typename OutIt>
    OutIt writeOffsets(OutIt* firstOutput) const
    {
        std::size_t offset{0};
        for (const auto& attr : attributes)
        {
            *firstOutput++ = offset;
            offset += attr.byteSize();
        }
        return firstOutput;
    }
};

// By default, if something has primitive restarts, the largest possible index
// for the given index byte width is
struct PrimitiveLayout
{
    MeshPrimitiveType type;
    U32 count;
    U32 restartValue;
    bool isIndexed;

    [[nodiscard]] auto operator<=>(const PrimitiveLayout&) const noexcept = default;
};

struct VertexAttributeComponentLocation
{
    DataRange byteRange;
    std::size_t attributeIndex; // Index into the attribute list.
    U8 component; // Which component of the attribute is this?
};

struct VertexAttributeIdentification
{
    // A flat list of attributes in the order they are encountered; note that
    // this may not be identical to the order they are in the file if components
    // are interleaved in a weird way.
    std::vector<VertexAttribute> attributes;
    // This contains a sorted list (by input offsets) of each input component,
    // with an index into the "attributes" list above.
    std::vector<VertexAttributeComponentLocation> locations;
};

// This describes how vertex attributes are mapped between the input and output
// specification. It is intended for an entire vertex that a number of these are
// stored in an array, representing the input order (with any padding in the
// input represented by empty output mappings).
struct VertexAttributeComponentMapping
{
    PrimitiveDataType dataType;
    std::size_t outputOffset;
    bool shouldOutput;
};

} // namespace kraken::io

#endif // KRAKEN_IO_DATA_MESH_HPP_INCLUDED
