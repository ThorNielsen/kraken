#ifndef KRAKEN_IO_DATA_RANGE_HPP_INCLUDED
#define KRAKEN_IO_DATA_RANGE_HPP_INCLUDED

#include <compare>
#include <cstddef>

namespace kraken::io
{

struct DataRange
{
    std::size_t offset;
    std::size_t length;

    [[nodiscard]] auto rangeBegin() const noexcept { return offset; }
    [[nodiscard]] auto rangeEnd() const noexcept { return offset + length; }

    [[nodiscard]] auto operator<=>(const DataRange& other) const noexcept
    {
        auto result = offset <=> other.offset;
        if (result != std::strong_ordering::equal) return result;
        return length <=> other.length;
    }
    [[nodiscard]] bool operator==(const DataRange&) const noexcept = default;
    [[nodiscard]] bool operator!=(const DataRange&) const noexcept = default;
};

} // namespace kraken::io

#endif // KRAKEN_IO_DATA_RANGE_HPP_INCLUDED
