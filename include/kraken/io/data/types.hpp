#ifndef KRAKEN_IO_DATA_TYPES_HPP_INCLUDED
#define KRAKEN_IO_DATA_TYPES_HPP_INCLUDED

#include <cstddef>

#include <kraken/types.hpp>

namespace kraken::io
{

// Enumerates primitive types that occurs in files, etc.
enum class PrimitiveDataType : U32
{
    Byte = 0, // S8
    UByte,    // U8

    Short,    // S16
    UShort,   // U16

    Int,      // S32
    UInt,     // U32

    Long,     // S64
    ULong,    // U64

    Float,    // F32
    Double,   // F64
};

[[nodiscard]] std::size_t byteSize(PrimitiveDataType) noexcept;
[[nodiscard]] std::size_t requiredAlignment(PrimitiveDataType) noexcept;
[[nodiscard]] bool isInteger(PrimitiveDataType) noexcept;
[[nodiscard]] bool isFloatingPoint(PrimitiveDataType) noexcept;

} // namespace kraken::io

#endif // KRAKEN_IO_DATA_TYPES_HPP_INCLUDED
