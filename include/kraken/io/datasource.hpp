#ifndef KRAKEN_IO_DATASOURCE_HPP_INCLUDED
#define KRAKEN_IO_DATASOURCE_HPP_INCLUDED

#include <kraken/types.hpp>

#if __has_include(<fast_float/fast_float.h>)
    #include <fast_float/fast_float.h>
#define USE_FAST_FLOAT_PARSING
#endif

#include <charconv>
#include <iosfwd>
#include <memory>
#include <optional>

namespace kraken::io
{

template <typename T>
concept HasAvailableFastFloatImpl =
#ifdef USE_FAST_FLOAT_PARSING
    requires { fast_float::from_chars("begin", "end", std::ref<T>); };
#else
    false;
#endif

template <typename T>
concept MemcpyConstructible
    = std::is_trivially_copyable_v<T> && std::is_standard_layout_v<T>;

template <typename Function>
concept ByteTestFunction = requires (Function f)
{
    { f(std::byte{}) } -> std::convertible_to<bool>;
};

static_assert(std::endian::native == std::endian::big
              || std::endian::native == std::endian::little,
              "Mixed-endian platforms are unsupported, if they even exist.");

template <typename OutputIterator>
concept IsAssignableToByte = std::indirectly_writable<OutputIterator, std::byte>;

template <typename OutputIterator>
OutputIterator copyToOutputIterator(OutputIterator outIt, std::byte byte) requires IsAssignableToByte<OutputIterator>
{
    *outIt++ = byte;
    return outIt;
}

template <typename OutputIterator>
OutputIterator copyToOutputIterator(OutputIterator outIt, std::byte byte) requires (!IsAssignableToByte<OutputIterator>)
{
    *outIt++ = static_cast<U8>(byte);
    return outIt;
}

// Represents a line end. All non-esoteric newline encodings uses at most two
// bytes (in UTF-8), and the reasonable ones uses a single one. Thus, there is
// only provision for storing at most two bytes.
struct LineEnd
{
    std::byte lineEnd; // Leading line end byte.
    std::byte secondLineEndChar; // = 0x0 if line ending is single-byte.

    [[nodiscard]] static constexpr LineEnd singleByte(std::byte b) noexcept { return {b, std::byte{0}}; };
    [[nodiscard]] static constexpr LineEnd singleByte(U8 b) noexcept { return {std::byte{b}, std::byte{0}}; };

    [[nodiscard]] static constexpr LineEnd cr() noexcept { return {std::byte{0x0d}, std::byte{0}}; }
    [[nodiscard]] static constexpr LineEnd lf() noexcept { return {std::byte{0x0a}, std::byte{0}}; }
    [[nodiscard]] static constexpr LineEnd crlf() noexcept { return {std::byte{0x0d}, std::byte{0x0a}}; }
};

struct DataSource
{
public:
    DataSource() : m_maxSize{0}, m_position{0} {}
    DataSource(const void* begin, size_t byteCount);
    DataSource(std::istream& input);

    void init(const void* begin, size_t byteCount);
    void init(std::istream& input);

    void clear();

    [[nodiscard]] bool readBinary(void* dst, size_t byteCount, std::endian storedEndian) noexcept
    {
        if (m_position + byteCount > m_maxSize) return false;
        std::memcpy(dst, m_data.get() + m_position, byteCount);
        if (storedEndian != std::endian::native)
        {
            std::reverse(static_cast<std::byte*>(dst),
                         static_cast<std::byte*>(dst) + byteCount);
        }
        m_position += byteCount;
        return true;
    }

    template <typename Result>
    [[nodiscard]] bool readBinary(Result& dst, std::endian storedEndian) noexcept requires MemcpyConstructible<Result>
    {
        return readBinary(static_cast<void*>(&dst), sizeof(Result), storedEndian);
    }

    [[nodiscard]] bool readSingleByte(std::byte& result) noexcept
    {
        if (m_position >= m_maxSize) return false;
        result = m_data[m_position++];
        return true;
    }

    template <typename Number>
    [[nodiscard]] bool readAscii(Number& dst) noexcept requires (!HasAvailableFastFloatImpl<Number>)
    {
        auto currPos = reinterpret_cast<const char*>(m_data.get()) + m_position;
        auto end = reinterpret_cast<const char*>(m_data.get()) + m_maxSize;
        auto parseResult = std::from_chars(currPos, end, dst);
        if (parseResult.ec == std::errc{})
        {
            m_position += parseResult.ptr - currPos;
            return true;
        }
        return false;
    }

#ifdef USE_FAST_FLOAT_PARSING
    template <typename Number>
    [[nodiscard]] bool readAscii(Number& dst) noexcept requires HasAvailableFastFloatImpl<Number>
    {
        auto currPos = reinterpret_cast<const char*>(m_data.get()) + m_position;
        auto end = reinterpret_cast<const char*>(m_data.get()) + m_maxSize;
        auto parseResult = fast_float::from_chars(currPos, end, dst);
        if (parseResult.ec == std::errc{})
        {
            m_position += parseResult.ptr - currPos;
            return true;
        }
        return false;
    }
#endif // USE_FAST_FLOAT_PARSING

    // Will read up to outputLength bytes and place those in output. It will
    // NOT write a null terminator!
    template <typename Testfunction, typename OutputIterator>
    bool readString(Testfunction toInclude,
                    OutputIterator output,
                    size_t* outputLength = nullptr)
    requires ByteTestFunction<Testfunction>
    {
        size_t dummy = std::numeric_limits<size_t>::max();
        if (!outputLength) outputLength = &dummy;

        while (true)
        {
            // Could be semi-controversial, since when this is refactored to a
            // chunked reader, end-of-chunk is not the same as end-of-stream/
            // end-of-file.
            if (m_position >= m_maxSize) return true;

            // However, if this returns, we have certainly scanned the string
            // correctly.
            if (!toInclude(m_data[m_position])) return true;

            // In this case we cannot output anything more, and as thus, we do
            // NOT increment m_position.
            if (!outputLength) return false;

            --outputLength;

            output = copyToOutputIterator(output, m_data[m_position++]);
        }
    }

    template <typename Testfunction>
    void skip(Testfunction toSkip) requires ByteTestFunction<Testfunction>
    {
        while (m_position < m_maxSize && toSkip(m_data[m_position]))
        {
            ++m_position;
        }
    }

    [[nodiscard]] std::size_t memcpy(void* dst, std::size_t bytes) noexcept
    {
        bytes = std::min(bytes, bytesLeft().value());
        std::memcpy(dst, m_data.get() + m_position, bytes);
        m_position += bytes;
        return bytes;
    }

    void skip(size_t byteCount)
    {
        // Note: We assume no overflow, which is reasonable in most cases on
        // 64-bit machines.
        m_position = std::min(m_position + byteCount, m_maxSize);
    }

    // Wrapped in optional to support chunked reader with the same interface.
    [[nodiscard]] std::optional<std::size_t> bytesLeft() const noexcept
    {
        return m_maxSize - m_position;
    }

    // Note: This is at the end of ALL data this can provide; will not return
    // true for (future) chunked implementation where one is at the end of a
    // chunk.
    [[nodiscard]] bool atEnd() const noexcept
    {
        return m_position >= m_maxSize;
    }

    [[nodiscard]] bool putback(std::size_t bytes)
    {
        if (m_position >= bytes)
        {
            m_position -= bytes;
            return true;
        }
        return false;
    }

private:
    std::unique_ptr<std::byte[]> m_data;
    size_t m_maxSize;
    size_t m_position;
};

} // namespace kraken::io

#endif // KRAKEN_IO_DATASOURCE_HPP_INCLUDED

