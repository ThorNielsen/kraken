#ifndef KRAKEN_IO_FORMATS_PLY_HPP_INCLUDED
#define KRAKEN_IO_FORMATS_PLY_HPP_INCLUDED

#include <kraken/io/data/types.hpp>
#include <kraken/io/datasource.hpp>

#include <bit>
#include <cstddef>
#include <optional>
#include <string>
#include <vector>

namespace kraken::io::formats::ply
{

enum class StoredFormat : U8
{
    Ascii,
    BinaryLittleEndian,
    BinaryBigEndian
};

/// Describes a single property.
struct Property
{
    /// Raw name of the property in question.
    std::string name;
    /// Underlying type of each element of the property; for variable-length
    /// properties (like face indices), this is the main type.
    PrimitiveDataType type;
    /// This is set iff the property is a list, and indicates the type
    /// describing the list size itself. If set, this is necessarily an integer
    /// type.
    std::optional<PrimitiveDataType> listIndexType;

    /// Determines if this property is a list.
    [[nodiscard]] bool isList() const noexcept
    {
        return listIndexType.has_value();
    }
};

/// Describes a single element consisting of properties.
struct Element
{
    /// Name of this element (NOT the properties).
    std::string name;

    /// Actual properties.
    std::vector<Property> properties;

    [[nodiscard]] auto containsList() const noexcept
    {
        for (const auto& prop : properties)
        {
            if (prop.listIndexType) return true;
        }
        return false;
    }
};

struct ElementWithCount
{
    Element element;
    std::size_t count;
};

struct Comment
{
    std::string text;
    // Positional information: These holds the index of the immediately
    // following elements / properties; observe that if the comments are after
    // all elements and properties, these indices will be one-past-the end for
    // all elements/properties.
    std::size_t beforeElementIndex;
    std::size_t beforePropertyIndex;
};

/// Describes the layout of an entire PLY file.
struct Header
{
    StoredFormat format;
    std::vector<ElementWithCount> elements;
    std::vector<Comment> comments;

    // Returns nullptr if it does not exist.
    ElementWithCount* getElementByName(std::string_view name);
    const ElementWithCount* getElementByName(std::string_view name) const;

    LineEnd lineEnd;
};

[[nodiscard]] constexpr bool isFormatNativeEndian(StoredFormat fmt) noexcept
{
    if constexpr (std::endian::native == std::endian::little)
    {
        return fmt == StoredFormat::BinaryLittleEndian;
    }
    if constexpr (std::endian::native == std::endian::big)
    {
        return fmt == StoredFormat::BinaryBigEndian;
    }
    return false;
}

[[nodiscard]] constexpr std::optional<std::endian>
getEndian(formats::ply::StoredFormat storedFormat) noexcept
{
    switch (storedFormat)
    {
    case formats::ply::StoredFormat::BinaryBigEndian: return std::endian::big;
    case formats::ply::StoredFormat::BinaryLittleEndian: return std::endian::little;
    case formats::ply::StoredFormat::Ascii: default:
        return std::nullopt;
    }
}

const char* getVertexElementName() noexcept;
const char* getFaceElementName() noexcept;

} // namespace kraken::io::formats::ply

#endif // KRAKEN_IO_FORMATS_PLY_HPP_INCLUDED

