#ifndef KRAKEN_IO_MESHOUTPUT_HPP_INCLUDED
#define KRAKEN_IO_MESHOUTPUT_HPP_INCLUDED

#include <kraken/graphics/vertex.hpp>
#include <kraken/io/data/mesh.hpp>
#include <kraken/io/data/range.hpp>
#include <kraken/io/data/types.hpp>
#include <kraken/types.hpp>
#include <kraken/utility/memory.hpp>

#include <cstddef>
#include <optional>

namespace kraken::io
{

enum class MemoryMapStatus
{
    Success,
    MemoryAllocationFailed,
    OutOfRangeAccess,
    UnsupportedType,

    Unknown
};

class MeshOutputWriter;

class WritableMemoryMapWithStatus
{
public:
    WritableMemoryMapWithStatus(const void* data);

    // Returns nullptr if there is no data.
    operator void*() const noexcept { return m_data; }
    operator MemoryMapStatus() const noexcept { return m_status; }

private:
    void* m_data;
    size_t size;
    MemoryMapStatus m_status;
};

enum class MeshOutputWriterStatus
{
    Success,

    // Input errors
    NoVertexSizeDefined,
    TooSmallVertexStride,
    InvalidIndexType,
    InvalidListIndexType,
    BadPrimitiveType,
    TooLittleInputData,

    // Output errors
    MemoryAllocationFailed,

    // Generic error, if something goes REALLY wrong:
    UnknownError
};

#if 0

class MeshOutputWriter
{
public:
    virtual ~MeshOutputWriter() = default;

    /// Sets the description of the vertex layout.
    ///
    /// \param spec A map describing where each component of the vertex is
    ///             located, and what that will be used for.
    /// \todo Replace with a simple vector of packed VertexComponentUsageTypes.
    /// \throws An exception if the size is not supported as output. For example
    ///         if the output writer is supposed to write directly to GPU-
    ///         visible memory, but the vertex size is greater than the maximal
    ///         input attribute size supported by the device.
    void setVertexLayout(const std::map<VertexComponent, DataRange>& spec);

    /// Provides a hint of the number of vertices that will be written.
    ///
    /// Note that this function purely works for optimisation purposes to tweak
    /// the allocation behaviour of the output writer (if possible), and no call
    /// to this can be considered binding.
    ///
    /// \param count Number of vertices in total that can currently be
    ///              determined will be written.
    /// \param isFinal Whether this number is the final expected number or if it
    ///                is just an intermediate. This allows the allocator to
    ///                select whether to resize to exactly "count" elements or
    ///                whether to reserve more space for amortised linear-time
    ///                allocation.
    virtual void reserveVertices(std::size_t count, bool isFinal = false);

    /// Writes vertices to the given output.
    ///
    /// While this can be called with just a single vertex, it is not
    /// recommended to do so due to the overhead of virtual function call
    /// dispatching -- unless storing multiple vertices for some reason incurs
    /// a higher overhead.
    ///
    /// \param source Pointer to read from.
    /// \param count Number of vertices to read.
    /// \param srcStride Stride (i.e. byte distance between consecutive
    ///                  elements) of input vertices.
    /// \returns Either success, or a code describing an (unrecoverable) error
    ///          condition.
    virtual MeshOutputWriterStatus writeVertices(const void* source,
                                                 std::size_t count,
                                                 std::size_t srcStride = 0) = 0;

    /// Writes a number of primitives to the output.
    ///
    /// \param sourceBegin Pointer to the initial location of the primitives.
    /// \param sourceEnd Pointer to the end.
    /// \param primitiveType The type of primitives to write.
    /// \param indexType The type of the indices. Note that this MUST be the
    ///                  same type at all times, and that it MUST be able to
    ///                  represent any valid index.
    /// \param listIndexType This is relevant for "non-primitive" primitive
    ///                      types, i.e. anything which contains a variable
    ///                      number of elements. If this has a value, all
    ///                      primitives must be stored in the pointed-to data as
    ///                      the size (of type *listIndexType) of the current
    ///                      primitive, and then that number of indices (of type
    ///                      indexType), i.e. repetitions of the following:
    ///                      count, primIdx_0, ..., primIdx_{count-1}
    ///
    /// \remark For now there is no support for switching primitive types, so
    ///         all calls to this function are CURRENTLY assumed to give the
    ///         same primitive type, and to output to a single mesh only.
    /// \remark For performance reason there is no guaranteed bounds checking on
    ///         the primitive indexes, and so one should both ensure they refer
    ///         to valid vertices, and especially in the case of lists, that
    ///         none of them are malformed.´
    ///
    /// \returns A status indicating whether the operation was successful, or
    ///          the first-encountered error type.
    virtual MeshOutputWriterStatus writePrimitives(const void* sourceBegin,
                                                   const void* sourceEnd,
                                                   MeshPrimitiveType primitiveType,
                                                   PrimitiveDataType indexType,
                                                   std::optional<PrimitiveDataType> listIndexType) = 0;

protected:
    // Can optionally be overridden to implement custom handling for handling
    // whenever setVertexLayout is called.
    virtual void setVertexLayoutImpl(const std::map<VertexComponent, DataRange>& spec);

    [[nodiscard]] const std::map<VertexComponent, DataRange>& getFormatSpec() const noexcept
    {
        return m_vertexFormatSpec;
    }

    [[nodiscard]] std::optional<std::size_t> vertexSize() const noexcept
    {
        return m_vertexSize;
    }

private:
    // Replace with flat vector instead! Maybe even just a pointer.
    std::map<VertexComponent, DataRange> m_vertexFormatSpec;
    std::optional<std::size_t> m_vertexSize;
};


class SimpleMeshOutputWriter : public MeshOutputWriter
{
public:
    SimpleMeshOutputWriter(ExternalMemoryAllocation::MemoryAllocationFunction vertexAlloc,
                           ExternalMemoryAllocation::MemoryAllocationFunction indexAlloc);

    [[nodiscard]] graphics::VertexType vertexType() const;
    [[nodiscard]] std::optional<U32> indexByteWidth() const;

    MeshOutputWriterStatus writeVertices(const void* source,
                                         std::size_t count,
                                         std::size_t srcStride) override;

    MeshOutputWriterStatus writePrimitives(const void* sourceBegin,
                                           const void* sourceEnd,
                                           MeshPrimitiveType primitiveType,
                                           PrimitiveDataType indexType,
                                           std::optional<PrimitiveDataType> listIndexType) override;

private:
    ExternalMemoryAllocation m_vertexAlloc;
    ExternalMemoryAllocation m_indexAlloc;

    std::optional<U32> m_indexByteWidth;
};

#endif

} // namespace kraken::io

#endif // KRAKEN_IO_MESHOUTPUT_HPP_INCLUDED

