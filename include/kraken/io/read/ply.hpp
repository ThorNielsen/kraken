#ifndef KRAKEN_IO_READ_PLY_HPP_INCLUDED
#define KRAKEN_IO_READ_PLY_HPP_INCLUDED

#include <kraken/io/datasource.hpp>
#include <kraken/io/data/mesh.hpp>
#include <kraken/io/data/types.hpp>
#include <kraken/io/meshoutput.hpp>

#include <kraken/io/formats/ply.hpp>

#include <kraken/graphics/vertex.hpp>
#include <kraken/types.hpp>

#include <cctype>
#include <cstddef>
#include <cstdlib>
#include <cstring>
#include <functional>

namespace kraken::io::formats::ply
{

// This function is responsible for parsing an element of a specific type,
// storing the result if necessary, and returning the parsing status.
using ElementParser = std::function<bool(DataSource&,
                                         const Header&, // Header describing the elements.
                                         std::size_t)>; // Index into the header describing the current element.

// Describes how to parse elements with specific names.
// Observe that the key "" is special -- if it is present, that function will be
// run on any unknown element.
using ElementParserFunctions = std::map<std::string, ElementParser>;

/// Parses a PLY header from the given data source.
///
/// \todo Make reentrant/continuable by passing workdata.
/// \todo Return actual status.
///
/// \param source A data source.
///
/// \returns A header iff that was parsable.
std::optional<Header>
readHeader(DataSource& source);

/// Parses the remainder of a PLY file.
///
/// \param source Data source, where just the header has been read.
/// \param header The parsed header.
/// \param parsers Functions to use for parsing specific elements.
///
/// \returns Whether the parsing was successful or not.
bool readBody(DataSource& source,
              const Header& header,
              const ElementParserFunctions& parsers);

/// Parses a PLY file.
///
/// \param source Data source containing the file.
/// \param parsers Functions determining how to handle each element in the file.
///                Observe that unknown elements will be skipped by default. The
///                parser corresponding to an empty element name will be treated
///                specially; namely that will be used for any element not
///                parsed by another parser.
///
/// \returns Whether the parsing was successful.
bool read(DataSource& source, const ElementParserFunctions& parsers);

struct TriangleInfo
{
    std::size_t triangleCount;
    std::size_t indexByteWidth;
};

ElementParser createDefaultVertexParser(ExternalMemoryAllocation& dataOut,
                                        VertexLayout& layoutOut,
                                        std::size_t& vertexCountOut,
                                        ExtendedAttributeNames* attrNames = nullptr);

ElementParser createRelocatingVertexParser(ExternalMemoryAllocation& dataOut,
                                           const VertexLayout& desiredLayout,
                                           VertexLayout& resultinglayoutOut,
                                           std::size_t& vertexCountOut,
                                           ExtendedAttributeNames* attrNames = nullptr);

ElementParser createTriangulatingFaceParser(ExternalMemoryAllocation& dataOut,
                                            TriangleInfo& triangleInfoOut);

} // namespace kraken::io::formats::ply

#endif // KRAKEN_IO_READ_PLY_HPP_INCLUDED

