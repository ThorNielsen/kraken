#ifndef KRAKEN_IO_READ_PLY_ATTRIBUTEIDENTIFICATION_HPP_INCLUDED
#define KRAKEN_IO_READ_PLY_ATTRIBUTEIDENTIFICATION_HPP_INCLUDED

#include <kraken/io/data/mesh.hpp>
#include <kraken/io/data/types.hpp>
#include <kraken/io/formats/ply.hpp>
#include <kraken/io/utility/attributeidentification.hpp>

#include <optional>

namespace kraken::io::read::ply
{

[[nodiscard]] std::optional<PrimitiveDataType>
toPrimitiveDataType(const char* s);

// Returns nullopt iff the element has variable-length elements.
[[nodiscard]] std::optional<VertexAttributeIdentification>
identifyAttributes(const formats::ply::Element& element,
                   ExtendedAttributeNames* extAttrNames = nullptr);

} // namespace kraken::io::read::ply

#endif // KRAKEN_IO_READ_PLY_ATTRIBUTEIDENTIFICATION_HPP_INCLUDED
