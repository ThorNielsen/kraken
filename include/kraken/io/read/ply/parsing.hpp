#ifndef KRAKEN_IO_READ_PLY_PARSING_HPP_INCLUDED
#define KRAKEN_IO_READ_PLY_PARSING_HPP_INCLUDED

#include <kraken/io/data/mesh.hpp>
#include <kraken/io/datasource.hpp>
#include <kraken/io/formats/ply.hpp>
#include <kraken/io/utility/binary.hpp>
#include <kraken/io/utility/tokeniser.hpp>

namespace kraken::io::read::ply
{

inline bool readInteger(U32& valueOut,
                        DataSource& source,
                        std::size_t size, // Zero iff plain text
                        std::optional<std::endian> endian) // nullopt iff plain text
{
    switch (size)
    {
    case 0:
        advanceToNextToken(source);
        return source.readAscii(valueOut);
    case 1: return readBinary<U8>(valueOut, source, *endian);
    case 2: return readBinary<U16>(valueOut, source, *endian);
    case 4: return readBinary<U32>(valueOut, source, *endian);
    default: return false;
    }
}

inline void skipToLineEnd(DataSource& source,
                          const formats::ply::Header& header) noexcept
{
    skipRestOfLine(source, header.lineEnd);
}

inline void skipPlainProperty(DataSource& source,
                              const formats::ply::Property& property)
{
    if (!property.isList()) skipNextToken(source);
    else
    {
        auto listCount = parseNext<std::size_t>(source);
        while (listCount --> 0) skipNextToken(source);
    }
}

inline void skipBinaryProperty(DataSource& source,
                               const formats::ply::Property& property,
                               std::endian endian)
{
    if (!property.isList())
    {
        source.skip(byteSize(property.type));
    }
    else
    {
        auto bytesToDiscard = byteSize(property.type);
        switch (byteSize(property.listIndexType.value()))
        {
        case 1:
            bytesToDiscard *= std::size_t(readBinary<U8>(source, endian));
            break;
        case 2:
            bytesToDiscard *= std::size_t(readBinary<U16>(source, endian));
            break;
        case 4:
            bytesToDiscard *= std::size_t(readBinary<U32>(source, endian));
            break;
        default:
            ;
        }
        source.skip(bytesToDiscard);
    }
}

void skipElementWithList(DataSource& source,
                         const formats::ply::Header& header,
                         std::size_t elementIndex);

bool skipElement(DataSource& source,
                 const formats::ply::Header& header,
                 std::size_t elementIndex);

struct FaceInfo
{
    std::optional<std::size_t> vertexCount; // If known. Otherwise byte width
        // will be forced to 4.
    std::array<std::size_t, 3> desiredIndexWidths; // Should contain 1, 2 and/or
        // 4 in the order they are
        // desired, and then zeroes.
    std::size_t chosenIndexWidth = 0;
};

/// \param vertexSize Contains the size of one vertex (also equal to stride).
///                   If too small, it will be enlarged exactly to fit all
///                   mapped components. Input 0 to ensure vertices are tightly
///                   packed, and nullptr if you do not care about the vertex
///                   size at all.
bool readVertices(DataSource& source,
                  formats::ply::StoredFormat format,
                  ExternalMemoryAllocation& destination,
                  std::size_t vertexCount,
                  const VertexAttributeComponentMapping* pMappings,
                  std::size_t mappingCount,
                  std::size_t* vertexSize = nullptr);

bool readFacesTriangulated(DataSource& source,
                           const formats::ply::Element& element,
                           std::size_t faceCount,
                           formats::ply::StoredFormat format,
                           ExternalMemoryAllocation& destination,
                           FaceInfo& auxInfo);

} // namespace kraken::io::read::ply

#endif // KRAKEN_IO_READ_PLY_PARSING_HPP_INCLUDED
