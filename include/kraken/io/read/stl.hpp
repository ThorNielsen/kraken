#ifndef KRAKEN_IO_READ_STL_HPP_INCLUDED
#define KRAKEN_IO_READ_STL_HPP_INCLUDED

#include <kraken/io/datasource.hpp>
#include <kraken/io/data/mesh.hpp>
#include <kraken/io/data/types.hpp>
#include <kraken/utility/memory.hpp>

namespace kraken::io::formats::stl
{

bool readPlain(DataSource& source,
               ExternalMemoryAllocation& verticesOut,
               std::size_t& vertexCountOut,
               VertexLayout& layoutOut,
               const VertexLayout* desiredLayout = nullptr);

bool readBinary(DataSource& source,
                ExternalMemoryAllocation& verticesOut,
                std::size_t& vertexCountOut,
                VertexLayout& layoutOut,
                const VertexLayout* desiredLayout = nullptr);

// Vertex merging not yet implemented, and essentially just copies data.
bool read(DataSource& source,
          ExternalMemoryAllocation& verticesOut,
          std::size_t& vertexCountOut,
          VertexLayout& layoutOut,
          const VertexLayout* desiredLayout = nullptr);

} // namespace kraken::io::formats::stl

#endif // KRAKEN_IO_READ_STL_HPP_INCLUDED
