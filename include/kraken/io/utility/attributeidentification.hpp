#ifndef KRAKEN_IO_UTILITY_ATTRIBUTEIDENTIFICATION_HPP_INCLUDED
#define KRAKEN_IO_UTILITY_ATTRIBUTEIDENTIFICATION_HPP_INCLUDED

#include <kraken/io/data/mesh.hpp>
#include <kraken/io/data/types.hpp>

namespace kraken::io
{

/// Computes a mapping that one can use to read vertices described by the
/// identification, and output them to the attributes given.
///
/// Observe that to add padding, one must add a dummy attribute with invalid/
/// padding usage.
[[nodiscard]]
std::vector<VertexAttributeComponentMapping>
computeComponentMapping(const VertexAttributeIdentification& identification,
                        const VertexAttribute* pAttributes,
                        std::size_t attributeCount);

} // namespace kraken::io

#endif // KRAKEN_IO_UTILITY_ATTRIBUTEIDENTIFICATION_HPP_INCLUDED
