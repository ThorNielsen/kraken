#ifndef KRAKEN_IO_UTILITY_BINARY_HPP_INCLUDED
#define KRAKEN_IO_UTILITY_BINARY_HPP_INCLUDED

#include <kraken/io/datasource.hpp>
#include <kraken/utility/memory.hpp>

#include <stdexcept>

namespace kraken::io
{

template <typename Type>
[[nodiscard]] Type readBinary(DataSource& source, std::endian storedEndian)
{
    Type result;
    if (!source.readBinary(result, storedEndian))
    {
        throw std::logic_error("Out of bounds.");
    }
    return result;
}

template <typename TypeToRead, typename ConvertTo>
[[nodiscard]] auto readBinary(ConvertTo& resultOut,
                              DataSource& source,
                              std::endian storedEndian) requires std::convertible_to<TypeToRead, ConvertTo>
{
    TypeToRead tmp;
    auto status = source.readBinary(tmp, storedEndian);
    if (!status) return status;
    resultOut = static_cast<ConvertTo>(tmp);
    return status;
}

// Used for writing the indices of a triangle to a potentially different format
// by exploiting automatic conversion for parameters in function calls.
template <typename IndexType>
[[nodiscard]] bool writeIndices(ExternalMemoryAllocation& destination,
                                IndexType i0, IndexType i1, IndexType i2) noexcept
{
    IndexType bufToWrite[3]{i0, i1, i2};
    return destination.memcpy_back(bufToWrite, 3*sizeof(IndexType), 4);
}

} // namespace kraken::io

#endif // KRAKEN_IO_UTILITY_BINARY_HPP_INCLUDED

