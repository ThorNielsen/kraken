#ifndef KRAKEN_IO_UTILITY_TOKENISER_HPP_INCLUDED
#define KRAKEN_IO_UTILITY_TOKENISER_HPP_INCLUDED

#include <kraken/io/datasource.hpp>
#include <kraken/io/data/types.hpp>
#include <kraken/utility/range.hpp>

#include <cstddef>
#include <stdexcept>
#include <string>

namespace kraken::io
{

namespace detail
{

[[nodiscard]] constexpr bool isPartOfToken(std::byte elem) noexcept
{
    return !std::isspace(std::to_integer<char>(elem));
}

[[nodiscard]] constexpr bool isNotPartOfToken(std::byte elem) noexcept
{
    return std::isspace(std::to_integer<char>(elem));
}

} // namespace detail

// Also skips the newline, if any.
inline void skipRestOfLine(DataSource& source, LineEnd lineEnd) noexcept
{
    if (lineEnd.secondLineEndChar == std::byte{0x0})
    {
        source.skip([to = lineEnd.lineEnd](std::byte b) noexcept
                    -> bool {return b != to; });
        source.skip(1);
    }
    else
    {
        bool lastIsInitialLineEnd = false;
        source.skip([&lastIsInitialLineEnd, lineEnd](std::byte b) noexcept -> bool
        {
            if (lastIsInitialLineEnd && b == lineEnd.secondLineEndChar)
            {
                return false;
            }
            lastIsInitialLineEnd = b == lineEnd.lineEnd;
            return true;
        });
        source.skip(1);
    }
}

inline void advanceToNextToken(DataSource& source) noexcept
{
    source.skip(detail::isNotPartOfToken);
}

inline void skipNextToken(DataSource& source) noexcept
{
    advanceToNextToken(source);
    source.skip(detail::isPartOfToken);
}

[[nodiscard]] inline std::string nextToken(DataSource& source)
{
    advanceToNextToken(source);
    std::string result;
    if (!source.readString(detail::isPartOfToken,
                           std::back_inserter(result),
                           nullptr))
    {
        throw std::logic_error("Back-insertion should never fail to read.");
    }
    return result;
}

[[nodiscard]] inline bool nextToken(DataSource& source, std::string& resultOut)
{
    advanceToNextToken(source);
    resultOut.clear();
    return source.readString(detail::isPartOfToken,
                             std::back_inserter(resultOut),
                             nullptr);
}

// Todo: Fix bug with multi-byte line ends where e.g.
// <text> <CR> <extra text> <CR> <LF>
// gets split at first CR and gobbles one byte of the extra text.
// REQUIRES data source to add lookahead of one byte.
[[nodiscard]] inline std::string getline(DataSource& source, LineEnd lineEnd)
{
    std::string result;
    if (!source.readString([lineEnd](std::byte elem)->bool{return elem != lineEnd.lineEnd;},
                           std::back_inserter(result),
                           nullptr))
    {
        throw std::logic_error("Back-insertion should never fail to read.");
    }
    source.skip(1+(lineEnd.secondLineEndChar != std::byte{0})); // Skips the delimiting byte(s).
    return result;
}

template <typename Type>
[[nodiscard]] Type parseNext(DataSource& source)
{
    advanceToNextToken(source);
    Type result;
    if (!source.readAscii(result))
    {
        throw std::logic_error("Failed to parse plain text.");
    }
    return result;
}

template <typename Number>
bool parseAsciiAndWriteBinary(DataSource& reader,
                              void* buffer,
                              size_t& bufPosToIncrement)
{
    void* bufPos = static_cast<void*>(static_cast<U8*>(buffer) + bufPosToIncrement);
    Number result;
    advanceToNextToken(reader);
    if (!reader.readAscii(result)) return false;
    bufPosToIncrement += sizeof(Number);
    std::memcpy(bufPos, &result, sizeof(Number));
    return true;
}

template <typename Number>
bool parseAsciiAndWriteBinary(DataSource& reader, void* dstBuf)
{
    return reader.readAscii(*reinterpret_cast<Number*>(dstBuf));
}

inline bool parseAscii(DataSource& source, void* dstBuf, PrimitiveDataType type)
{
    switch (type)
    {
    case PrimitiveDataType::Byte: return parseAsciiAndWriteBinary<S8>(source, dstBuf);
    case PrimitiveDataType::UByte: return parseAsciiAndWriteBinary<U8>(source, dstBuf);
    case PrimitiveDataType::Short: return parseAsciiAndWriteBinary<S16>(source, dstBuf);
    case PrimitiveDataType::UShort: return parseAsciiAndWriteBinary<U16>(source, dstBuf);
    case PrimitiveDataType::Int: return parseAsciiAndWriteBinary<S32>(source, dstBuf);
    case PrimitiveDataType::UInt: return parseAsciiAndWriteBinary<U32>(source, dstBuf);
    case PrimitiveDataType::Float: return parseAsciiAndWriteBinary<F32>(source, dstBuf);
    case PrimitiveDataType::Double: return parseAsciiAndWriteBinary<F64>(source, dstBuf);
    default:
        return false;
    }
}

// Returns number of successfully parsed elements.
template <typename Type>
inline std::size_t parseSpaceSeparatedElements(DataSource& source,
                                               Type* pOutput,
                                               std::size_t toRead)
{
    for (auto i : range(toRead))
    {
        advanceToNextToken(source);
        if (!source.readAscii(*pOutput++)) return i;
    }
    return toRead;
}

} // namespace kraken::io

#endif // KRAKEN_IO_UTILITY_TOKENISER_HPP_INCLUDED
