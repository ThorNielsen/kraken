#ifndef KRAKEN_MATH_ALGORITHM_HPP_INCLUDED
#define KRAKEN_MATH_ALGORITHM_HPP_INCLUDED

#include <cstddef>
#include <kraken/math/constants.hpp>
#include <kraken/math/matrix.hpp>
#include <type_traits>

namespace kraken
{

namespace math
{

template <size_t Rows, size_t Cols, typename Prec>
Matrix<Rows, Cols, Prec> rowreduce(Matrix<Rows, Cols, Prec> mat,
                                   bool stopAtUpperTriangular = false,
                                   Prec epsilon = 100*Constant<Prec>::epsilon,
                                   size_t* flipsOut = nullptr)
{
    Matrix<1, Cols, Prec> tmprow;
    size_t cRow = 0;
    size_t flipCount = 0;
    for (size_t col = 0; col < Cols; ++col)
    {
        Prec maxVal(0);
        size_t maxIdx = cRow;
        for (size_t row = cRow; row < Rows; ++row)
        {
            auto cVal = std::abs(mat(row, col));
            if (cVal > maxVal)
            {
                maxVal = cVal;
                maxIdx = row;
            }
        }
        if (maxVal < epsilon) continue;
        if (maxIdx != cRow)
        {
            ++flipCount;
            tmprow = mat.row(maxIdx);
            if (!stopAtUpperTriangular) tmprow *= Prec(1)/mat(maxIdx, col);
            mat.setRow(maxIdx, mat.row(cRow));
            mat.setRow(cRow, tmprow);
        }
        Prec invPivot = Prec(1)/mat(cRow, col);
        if (maxIdx == cRow && !stopAtUpperTriangular)
        {
            for (size_t j = 0; j < Cols; ++j)
            {
                mat(cRow, j) *= invPivot;
            }
            invPivot = Prec(1);
        }
        for (size_t row = 0; row < Rows; ++row)
        {
            if (row == cRow) continue;
            tmprow = mat.row(row) - mat.row(cRow) * (mat(row, col) * invPivot);
            mat.setRow(row, tmprow);
        }
        ++cRow;
    }
    if (flipsOut) *flipsOut = flipCount;
    return mat;
}

template <size_t Size, typename Prec>
Prec determinant(const Matrix<Size, Size, Prec>& mat,
                 Prec epsilon = 100*Constant<Prec>::epsilon)
{
    size_t flipCount;
    auto reduced = rowreduce(mat, true, epsilon, &flipCount);
    Prec prod(1);
    for (size_t i = 0; i < Size; ++i)
    {
        prod *= reduced(i, i);
    }
    if (flipCount % 2) prod = -prod;
    return prod;
}

template <size_t Size, typename Prec>
Matrix<Size, Size, Prec> inverse(Matrix<Size, Size, Prec> mat,
                                 Prec epsilon = 100*Constant<Prec>::epsilon)
{
    Matrix<Size, 2*Size, Prec> reducer(mat);
    for (size_t i = 0; i < Size; ++i)
    {
        reducer(i, Size+i) = Prec(1);
    }
    reducer = rowreduce(reducer, false, epsilon);
    for (size_t row = 0; row < Size; ++row)
    {
        for (size_t col = 0; col < Size; ++col)
        {
            mat(row, col) = reducer(row, Size+col);
        }
    }
    return mat;
}

template <size_t Rows, size_t Cols, typename Prec>
typename std::enable_if<Rows <= Cols, Matrix<Rows, Cols, Prec>>::type
orthonormaliseRows(Matrix<Rows, Cols, Prec> mat)
{
    Matrix<1, Cols, Prec> tmprow;
    Matrix<1, Cols, Prec> currRow;
    for (size_t row = 0; row < Rows; ++row)
    {
        tmprow = mat.row(row);
        currRow = tmprow;
        for (size_t i = 0; i < row; ++i)
        {
            auto rowI = mat.row(i);
            tmprow -= dot(currRow, rowI) * rowI;
        }
        mat.setRow(row, normalise(tmprow));
    }
    return mat;
}

template <size_t Rows, size_t Cols, typename Prec>
typename std::enable_if<Rows <= Cols, Matrix<Rows, Cols, Prec>>::type
orthonormaliseColumns(Matrix<Rows, Cols, Prec> mat)
{
    Matrix<Rows, 1, Prec> tmpcol;
    Matrix<Rows, 1, Prec> currCol;
    for (size_t col = 0; col < Cols; ++col)
    {
        tmpcol = mat.col(col);
        currCol = tmpcol;
        for (size_t i = 0; i < col; ++i)
        {
            auto colI = mat.col(i);
            tmpcol -= dot(currCol, colI) * colI;
        }
        mat.setCol(col, normalise(tmpcol));
    }
    return mat;
}

template <size_t Rows, size_t Cols, typename Prec, typename OutIt>
OutIt computeImageBasis(const Matrix<Rows, Cols, Prec>& mat, OutIt output,
                        Prec epsilon = 100*Constant<Prec>::epsilon)
{
    auto reduced = rowreduce(mat, true, epsilon);
    size_t row = 0;
    size_t col = 0;
    while (col < Cols)
    {
        size_t zeroesBegin = Rows;
        while (zeroesBegin && std::abs(reduced(zeroesBegin-1, col)) < epsilon)
        {
            --zeroesBegin;
        }
        if (zeroesBegin <= row)
        {
            ++col;
            continue;
        }
        *output++ = mat.col(col);
        row = zeroesBegin;
        ++col;
    }
    return output;
}

template <size_t Rows, size_t Cols, typename Prec, typename OutIt>
OutIt computeKernelBasis(const Matrix<Rows, Cols, Prec>& mat, OutIt output,
                         Prec epsilon = 100*Constant<Prec>::epsilon)
{
    Matrix<Cols, Rows + Cols, Prec> reduced(transpose(mat));
    for (size_t col = 0; col < Cols; ++col)
    {
        for (size_t row = 0; row < Cols; ++row)
        {
            reduced(row, Rows+col) = (row == col) * Prec(1);
        }
    }
    reduced = rowreduce(reduced, false, epsilon);

    for (size_t row = 0; row < Cols; ++row)
    {
        bool isZero = true;
        for (size_t col = 0; col < Rows; ++col)
        {
            if (std::abs(reduced(row, col)) >= epsilon)
            {
                isZero = false;
                break;
            }
        }
        if (isZero)
        {
            Vector<Cols, Prec> out(0);
            for (size_t col = 0; col < Cols; ++col)
            {
                out(col) = reduced(row, Rows+col);
            }
            *output++ = out;
        }
    }
    return output;
}

} // namespace math

} // namespace kraken

#endif // KRAKEN_MATH_ALGORITHM_HPP_INCLUDED
