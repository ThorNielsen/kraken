// This file is part of the Kraken project, and in addition to the license for
// that project (if any), this file itself is also available under the MIT
// License, the text of which follows below.

// MIT License

// Copyright (c) 2022 Thor Gabelgaard Nielsen

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef KRAKEN_MATH_COMPARISON_HPP_INCLUDED
#define KRAKEN_MATH_COMPARISON_HPP_INCLUDED

#include <algorithm>
#include <cmath>

#include <kraken/math/constants.hpp>

namespace kraken
{

namespace math
{

template <typename Prec>
bool zero(Prec real, Prec epsilon = 100*Constant<Prec>::epsilon) noexcept
{
    return std::abs(real) <= epsilon;
}

template <typename Prec>
bool flt_eq(Prec a, Prec b, Prec epsilon = 100*Constant<Prec>::epsilon) noexcept
{
    return zero(a-b, epsilon);
}

template <typename Prec>
bool flt_releq(Prec a, Prec b, Prec epsilon = 50*Constant<Prec>::epsilon) noexcept
{
    return std::abs(a-b) <= epsilon * (std::abs(a) + std::abs(b) + 2.f);
}

template <typename T>
inline T square(T a) noexcept
{
    return a*a;
}

template <typename T>
inline bool in_closed_range(T value, T from, T to) noexcept
{
    return value >= from && value <= to;
}

template <typename T>
inline T clamp(T val, T min, T max) noexcept
{
    return std::max(std::min(val, max), min);
}

template <typename Prec>
Prec radians(Prec deg) noexcept
{
    return Constant<Prec>::rad_per_deg * deg;
}

template <typename Prec>
Prec degrees(Prec rad) noexcept
{
    return Constant<Prec>::deg_per_rad * rad;
}

} // namespace math

} // namespace kraken

#endif // KRAKEN_MATH_COMPARISON_HPP_INCLUDED
