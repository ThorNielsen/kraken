// This file is part of the Kraken project, and in addition to the license for
// that project (if any), this file itself is also available under the MIT
// License, the text of which follows below.

// MIT License

// Copyright (c) 2022 Thor Gabelgaard Nielsen

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef KRAKEN_MATH_CONSTANTS_HPP_INCLUDED
#define KRAKEN_MATH_CONSTANTS_HPP_INCLUDED

#include <limits>

namespace kraken
{

namespace math
{

template <typename Prec>
class Constant
{
public:
    static constexpr Prec e = static_cast<Prec>(2.7182818284590452353602874713526625L);
    static constexpr Prec pi = static_cast<Prec>(3.1415926535897932384626433832795029L);
    static constexpr Prec two_pi = static_cast<Prec>(6.2831853071795864769252867665590058L);
    static constexpr Prec half_pi = static_cast<Prec>(1.5707963267948966192313216916397514L);
    static constexpr Prec infinity = std::numeric_limits<Prec>::infinity();
    static constexpr Prec maximum = std::numeric_limits<Prec>::max();
    static constexpr Prec rad_per_deg = static_cast<Prec>(0.0174532930056254069004769780351638L);
    static constexpr Prec deg_per_rad = static_cast<Prec>(57.295777918682046735837642970068373L);
    // epsilon = distance from 1 to smallest number strictly larger than 1
    // representable by the given type (except for non-floating point types;
    // for these it is equal to 0).
    static constexpr Prec epsilon = std::numeric_limits<Prec>::epsilon();
};

} // namespace math

} // namespace kraken

#endif // KRAKEN_MATH_CONSTANTS_HPP_INCLUDED
