#ifndef KRAKEN_MATH_INTEGER_HPP_INCLUDED
#define KRAKEN_MATH_INTEGER_HPP_INCLUDED

#include <kraken/types.hpp>

namespace kraken
{

// Note: ilog2(0) is explicitly defined to equal zero.
inline U64 ilog2(U64 n)
{
    U64 res = 0;
    if (n >= 0x100000000ull) { res += 32; n >>= 32; }
    if (n >= 0x10000ull) { res += 16; n >>= 16; }
    if (n >= 0x100ull) { res += 8; n >>= 8; }
    if (n >= 0x10ull) { res += 4; n >>= 4; }
    if (n >= 0x4ull) { res += 2; n >>= 2; }
    if (n >= 0x2ull) { res += 1; /*n >>= 1;*/ } // <-- Unnecessary
    return res;
}

inline U64 gcd(U64 a, U64 b)
{
    while (b)
    {
        U64 t = b;
        b = a%b;
        a = t;
    }
    return a;
}

inline U64 lcm(U64 a, U64 b)
{
    return (a/gcd(a, b))*b;
}

inline bool isprime(U64 n)
{
    if (n <= 2) return n == 2;
    if (n % 2 == 0) return false;
    for (U64 test = 3; test*test <= n; test += 2)
    {
        if (n % test == 0) return false;
    }
    return true;
}

inline U64 isqrt(U64 n)
{
    U64 shiftCount = 1;
    U64 nCpy = n;
    while (nCpy)
    {
        ++shiftCount;
        nCpy >>= 2;
    }

    U64 res = 0;
    U64 shift = (shiftCount-1) << 1;
    for (U64 dummy = 0; dummy < shiftCount; ++dummy, shift -= 2)
    {
        res <<= 1;
        U64 high = res + 1;
        if (high * high <= (n>>shift)) ++res;
    }
    return res;
}

template <typename T, T identity = 1>
inline T ipow(T value, U64 exponent)
{
    T res = identity;
    while (exponent)
    {
        if (exponent & 1) res *= value;
        value *= value;
        exponent >>= 1;
    }
    return res;
}

inline U64 modpow(U64 value, U64 exponent, U64 mod)
{
    U64 res = 1;
    while (exponent)
    {
        if (exponent & 1) res = (res * value) % mod;
        value = (value * value) % mod;
        exponent >>= 1;
    }
    return res;
}

inline U64 maskpow(U64 value, U64 exponent, U64 mask)
{
    U64 res = 1;
    while (exponent)
    {
        if (exponent & 1) res = (res * value) & mask;
        value = (value * value) & mask;
        exponent >>= 1;
    }
    return res;
}

} // namespace kraken

#endif // KRAKEN_MATH_INTEGER_HPP_INCLUDED
