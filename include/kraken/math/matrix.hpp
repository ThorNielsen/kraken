// This file is part of the Kraken project, and in addition to the license for
// that project (if any), this file itself is also available under the MIT
// License, the text of which follows below.

// MIT License

// Copyright (c) 2022-2024 Thor Gabelgaard Nielsen

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef KRAKEN_MATH_MATRIX_HPP_INCLUDED
#define KRAKEN_MATH_MATRIX_HPP_INCLUDED

#include "kraken/math/constants.hpp"
#include <algorithm>
#include <cmath>
#include <cstddef>
#include <cwctype>
#include <initializer_list>
#include <ios>
#include <istream>
#include <kraken/math/comparison.hpp>
#include <kraken/types.hpp>
#include <ostream>
#include <stdexcept>
#include <type_traits>

namespace kraken::math
{

template <size_t Rows, size_t Cols, typename Prec>
class Matrix
{
public:
    static constexpr size_t RowCount = std::max<size_t>(Rows, 1);
    static constexpr size_t ColumnCount = std::max<size_t>(Cols, 1);

    explicit Matrix(Prec diag) noexcept;
    Matrix() = default;
    Matrix(std::initializer_list<Prec> list)
    {
        if (list.size() == 1)
        {
            for (size_t i = 0; i < RowCount; ++i)
            {
                for (size_t j = 0; j < ColumnCount; ++j)
                {
                    (*this)(i, j) = (i == j) * *list.begin();
                }
            }
            return;
        }
        if (list.size() != RowCount * ColumnCount)
        {
            throw std::runtime_error("Initializer list bad size.");
        }
        auto beg = list.begin();
        auto* writeAt = m_data;
        while (beg != list.end())
        {
            *writeAt++ = *beg++;
        }
    }
    template <size_t DummyRows = Rows>
    Matrix(typename std::enable_if_t<(DummyRows > 1), std::initializer_list<Matrix<Rows, 1, Prec>>> list)
    {
        if (list.size() != ColumnCount)
        {
            throw std::runtime_error("Initializer list bad size.");
        }
        auto beg = list.begin();
        for (size_t i = 0; i < ColumnCount; ++i)
        {
            setCol(i, *beg++);
        }
    }

    template <size_t DummyRows = Rows>
    Matrix(typename std::enable_if_t<(Cols > 1 && DummyRows <= 1), std::initializer_list<Matrix<1, Cols, Prec>>> list)
    {
        if (list.size() != RowCount)
        {
            throw std::runtime_error("Initializer list bad size.");
        }
        auto beg = list.begin();
        for (size_t i = 0; i < RowCount; ++i)
        {
            setRow(i, *beg++);
        }
    }

    template <size_t ORows, size_t OCols, typename OPrec>
    explicit Matrix(const Matrix<ORows, OCols, OPrec>& other, Prec diag = Prec(1)) noexcept
    {
        for (size_t i = 0; i < RowCount; ++i)
        {
            for (size_t j = 0; j < ColumnCount; ++j)
            {
                if (i < std::max<size_t>(ORows, 1) && j < std::max<size_t>(OCols, 1))
                {
                    (*this)(i, j) = other(i, j);
                }
                else
                {
                    (*this)(i, j) = (i == j)*diag;
                }
            }
        }
    }

    // Initialises a rotation matrix from a quaternion.
    template <typename OPrec, typename DummyPrec = typename std::enable_if_t<(Rows >= 3 && Cols >= 3), OPrec>>
    Matrix(const Matrix<4, 0, OPrec>& q)
        : Matrix(Prec(1))
    {
        (*this)(0, 0) = Prec(1) - Prec(2) * (q[1] * q[1] + q[2] * q[2]);
        (*this)(0, 1) = Prec(2)           * (q[0] * q[1] - q[3] * q[2]);
        (*this)(0, 2) = Prec(2)           * (q[0] * q[2] + q[3] * q[1]);
        (*this)(1, 0) = Prec(2)           * (q[0] * q[1] + q[3] * q[2]);
        (*this)(1, 1) = Prec(1) - Prec(2) * (q[0] * q[0] + q[2] * q[2]);
        (*this)(1, 2) = Prec(2)           * (q[1] * q[2] - q[3] * q[0]);
        (*this)(2, 0) = Prec(2)           * (q[0] * q[2] - q[3] * q[1]);
        (*this)(2, 1) = Prec(2)           * (q[1] * q[2] + q[3] * q[0]);
        (*this)(2, 2) = Prec(1) - Prec(2) * (q[0] * q[0] + q[1] * q[1]);
    }

    [[nodiscard]] Prec* data() noexcept { return m_data; }
    [[nodiscard]] const Prec* data() const noexcept { return m_data; }

    [[nodiscard]] size_t rowCount() const noexcept { return RowCount; }
    [[nodiscard]] size_t colCount() const noexcept { return ColumnCount; }

    template <class Return = Prec>
    [[nodiscard]]
    typename std::enable_if<(Cols <= 1 || Rows <= 1), Return>::type
    operator[](size_t i) const noexcept
    {
        return m_data[i];
    }

    template <class Return = Prec>
    [[nodiscard]]
    typename std::enable_if<(Cols <= 1 || Rows <= 1), Return&>::type
    operator[](size_t i) noexcept
    {
        return m_data[i];
    }

    template <class Return = Prec>
    [[nodiscard]]
    typename std::enable_if<(Cols <= 1 || Rows <= 1), Return>::type
    operator()(size_t i) const noexcept
    {
        return m_data[i];
    }

    template <class Return = Prec>
    [[nodiscard]]
    typename std::enable_if<(Cols <= 1 || Rows <= 1), Return&>::type
    operator()(size_t i) noexcept
    {
        return m_data[i];
    }

    [[nodiscard]] const Prec& operator()(size_t i, size_t j) const noexcept
    {
        return m_data[toIndex(i, j)];
    }

    [[nodiscard]] Prec& operator()(size_t i, size_t j) noexcept
    {
        return m_data[toIndex(i, j)];
    }

    [[nodiscard]] const Prec& at(size_t i, size_t j) const
    {
        return m_data[toIndexBoundChecked(i, j)];
    }

    Prec& at(size_t i, size_t j)
    {
        return m_data[toIndexBoundChecked(i, j)];
    }

    template <typename NewPrec>
    [[nodiscard]] auto cast() const noexcept
    {
        Matrix<Rows, Cols, NewPrec> newmat;
        for (size_t i = 0;
             i < std::max<size_t>(Rows, 1)*std::max<size_t>(Cols, 1);
             ++i)
        {
            newmat.data()[i] = static_cast<NewPrec>(data()[i]);
        }
        return newmat;
    }

#if !defined(KRAKEN_ALLOW_DEGENERATE_MATRICES)
    template <size_t DummyRowCount = RowCount,
              typename Row = typename std::enable_if_t<(DummyRowCount > 1), Matrix<1, Cols, Prec>>>
#else
    template <size_t DummyRowCount = RowCount, typename Row = Matrix<1, Cols, Prec>>
#endif // !defined(KRAKEN_ALLOW_DEGENERATE_MATRICES)
    [[nodiscard]]
    Row row(size_t i) const noexcept
    {
        Row r;
        for (size_t j = 0; j < ColumnCount; ++j)
        {
            r(j) = (*this)(i, j);
        }
        return r;
    }

#if !defined(KRAKEN_ALLOW_DEGENERATE_MATRICES)
    template <class Return = const Prec&>
    [[nodiscard]]
    typename std::enable_if<(Rows <= 1), Return>::type
    row(size_t j) const noexcept
    {
        return m_data[j];
    }
#endif // !defined(KRAKEN_ALLOW_DEGENERATE_MATRICES)

    void setRow(size_t i, const Matrix<1, ColumnCount, Prec>& rowVal) noexcept
    {
        for (size_t j = 0; j < ColumnCount; ++j)
        {
            (*this)(i, j) = rowVal(j);
        }
    }

    // Allow setting rows using column vectors; this avoids the overhead of
    // having to transpose those. While it may appear to work around the type
    // system enforcing correct dimensions, it is explicit on calls, since this
    // is named "setRow".
    template <size_t DummyCols = Cols>
    void setRow(size_t i,
                typename std::enable_if_t<(DummyCols > 1), const Matrix<ColumnCount, 1, Prec>&> rowVal) noexcept
    {
        for (size_t j = 0; j < ColumnCount; ++j)
        {
            (*this)(i, j) = rowVal(j);
        }
    }

    template <size_t Length = ColumnCount>
    typename std::enable_if_t<Length == 1, void>
    setRow(size_t i, const Prec& rowVal) noexcept
    {
        m_data[i] = rowVal;
    }

#if !defined(KRAKEN_ALLOW_DEGENERATE_MATRICES)
    template <size_t DummyColCount = ColumnCount,
              typename Column = typename std::enable_if_t<(DummyColCount > 1), Matrix<Rows, 1, Prec>>>
#else
    template <size_t DummyColCount = ColumnCount, typename Column = Matrix<Rows, 1, Prec>>
#endif // !defined(KRAKEN_ALLOW_DEGENERATE_MATRICES)
    Column col(size_t j) const noexcept
    {
        Column c;
        for (size_t i = 0; i < RowCount; ++i)
        {
            c(i) = (*this)(i, j);
        }
        return c;
    }

#if !defined(KRAKEN_ALLOW_DEGENERATE_MATRICES)
    template <class Return = const Prec&>
    typename std::enable_if<(Cols <= 1), Return>::type
    col(size_t j) const noexcept
    {
        return m_data[j];
    }
#endif // !defined(KRAKEN_ALLOW_DEGENERATE_MATRICES)

    void setCol(size_t j, const Matrix<RowCount, 1, Prec>& c)
    {
        for (size_t i = 0; i < RowCount; ++i)
        {
            (*this)(i, j) = c(i);
        }
    }
    // Allow setting columns using row vectors; this avoids the overhead of
    // having to transpose those.
    template <size_t DummyRows = Rows>
    void setCol(size_t j,
                typename std::enable_if_t<(DummyRows > 1), const Matrix<1, RowCount, Prec>&> c) noexcept
    {
        for (size_t i = 0; i < RowCount; ++i)
        {
            (*this)(i, j) = c(i);
        }
    }

    template <size_t Length = RowCount>
    typename std::enable_if<Length == 1, void>::type
    setCol(size_t j, const Matrix<1, Rows, Prec>& c)
    {
        m_data[j] = c;
    }


    Matrix<Rows, Cols, Prec>& operator+=(const Matrix<Rows, Cols, Prec>& other);
    Matrix<Rows, Cols, Prec>& operator-=(const Matrix<Rows, Cols, Prec>& other);
    Matrix<Rows, Cols, Prec>& operator*=(const Matrix<Cols, Cols, Prec>& other);

    template <typename OPrec>
    Matrix<Rows, Cols, Prec>& operator*=(OPrec scalar)
    {
        for (size_t i = 0; i < RowCount*ColumnCount; ++i)
        {
            m_data[i] *= scalar;
        }
        return *this;
    }
    template <typename OPrec>
    Matrix<Rows, Cols, Prec>& operator/=(OPrec scalar)
    {
        for (size_t i = 0; i < RowCount*ColumnCount; ++i)
        {
            m_data[i] /= scalar;
        }
        return *this;
    }

    [[nodiscard]] Matrix<Rows, Cols, Prec> operator+() const
    {
        Matrix<Rows, Cols, Prec> m;
        for (size_t i = 0; i < RowCount*ColumnCount; ++i)
        {
            m.m_data[i] = +m_data[i];
        }
        return m;
    }

    [[nodiscard]] Matrix<Rows, Cols, Prec> operator-() const
    {
        Matrix<Rows, Cols, Prec> m;
        for (size_t i = 0; i < RowCount*ColumnCount; ++i)
        {
            m.m_data[i] = -m_data[i];
        }
        return m;
    }

    [[nodiscard]] Matrix<Cols, Rows, Prec> transpose() const
    {
        Matrix<Cols, Rows, Prec> mat;
        for (size_t i = 0; i < RowCount; ++i)
        {
            for (size_t j = 0; j < ColumnCount; ++j)
            {
                mat(j, i) = (*this)(i, j);
            }
        }
        return mat;
    }

private:
    [[nodiscard]] constexpr static size_t toIndex(size_t i, size_t j) noexcept
    {
        return i * ColumnCount + j;
    }
    [[nodiscard]] constexpr static size_t toIndexBoundChecked(size_t i, size_t j)
    {
        if (i*ColumnCount + j > RowCount * ColumnCount) throw std::domain_error("Out of bounds read.");
        return i * ColumnCount + j;
    }

    Prec m_data[RowCount*ColumnCount];
};

////////////////////////////////////////////////////////////////////////////////
/// Basic operations (implementations).
////////////////////////////////////////////////////////////////////////////////

template <size_t Rows, size_t Cols, typename Prec>
inline Matrix<Rows, Cols, Prec>::Matrix(Prec diag) noexcept
{
    for (size_t i = 0; i < RowCount; ++i)
    {
        for (size_t j = 0; j < ColumnCount; ++j)
        {
            m_data[toIndex(i, j)] = i == j ? diag : Prec(0);
        }
    }
}

template <size_t Rows, size_t Cols, typename Prec>
inline Matrix<Rows, Cols, Prec> operator+(Matrix<Rows, Cols, Prec> first,
                                          const Matrix<Rows, Cols, Prec>& second)
{
    return first += second;
}

template <size_t Rows, size_t Cols, typename Prec>
inline Matrix<Rows, Cols, Prec> operator-(Matrix<Rows, Cols, Prec> first,
                                          const Matrix<Rows, Cols, Prec>& second)
{
    return first -= second;
}

#ifdef KRAKEN_ALLOW_DEGENERATE_MATRICES
template <size_t RowsA, size_t SharedSize, size_t ColsB, typename Prec>
inline Matrix<RowsA, ColsB, Prec>
operator*(Matrix<RowsA, SharedSize, Prec> first,
         const Matrix<SharedSize, ColsB, Prec>& second)
{
   Matrix<RowsA, ColsB, Prec> result(Prec(0));
   for (size_t i = 0; i < RowsA; ++i)
   {
       for (size_t j = 0; j < ColsB; ++j)
       {
           for (size_t k = 0; k < SharedSize; ++k)
           {
               result(i, j) += first(i, k) * second(k, j);
           }
       }
   }
   return result;
}
#else
template <size_t RowsA, size_t SharedSize, size_t ColsB, typename Prec>
inline typename std::enable_if<(RowsA > 1) || (ColsB > 1),
                               Matrix<RowsA, ColsB, Prec>>::type
operator*(Matrix<RowsA, SharedSize, Prec> first,
          const Matrix<SharedSize, ColsB, Prec>& second)
{
    Matrix<RowsA, ColsB, Prec> result(Prec(0));
    for (size_t i = 0; i < std::max<size_t>(RowsA, 1); ++i)
    {
        for (size_t j = 0; j < std::max<size_t>(ColsB, 1); ++j)
        {
            for (size_t k = 0; k < SharedSize; ++k)
            {
                result(i, j) += first(i, k) * second(k, j);
            }
        }
    }
    return result;
}

template <size_t RowsA, size_t SharedSize, size_t ColsB, typename Prec>
inline typename std::enable_if<(RowsA <= 1) && (ColsB <= 1), Prec>::type
operator*(Matrix<RowsA, SharedSize, Prec> first,
          const Matrix<SharedSize, ColsB, Prec>& second)
{
    Prec result(0);
    for (size_t k = 0; k < std::max<size_t>(SharedSize, 1); ++k)
    {
        result += first(k) * second(k);
    }
    return result;
}
#endif // KRAKEN_ALLOW_DEGENERATE_MATRICES

template <typename Prec>
inline Matrix<4, 0, Prec>
operator*(Matrix<4, 0, Prec> first,
          const Matrix<4, 0, Prec>& second)
{
    Matrix<4, 0, Prec> result(Prec(0));
    result[0] = first[3] * second[0] + first[0] * second[3]
              + first[1] * second[2] - first[2] * second[1];
    result[1] = first[3] * second[1] - first[0] * second[2]
              + first[1] * second[3] + first[2] * second[0];
    result[2] = first[3] * second[2] + first[0] * second[1]
              - first[1] * second[0] + first[2] * second[3];
    result[3] = first[3] * second[3] - first[0] * second[0]
              - first[1] * second[1] - first[2] * second[2];
    return result;
}

template <size_t Rows, size_t Cols, typename Prec>
inline Matrix<Rows, Cols, Prec>&
Matrix<Rows, Cols, Prec>::operator+=(const Matrix<Rows, Cols, Prec>& other)
{
    for (size_t i = 0; i < std::max<size_t>(Rows, 1); ++i)
    {
        for (size_t j = 0; j < std::max<size_t>(Cols, 1); ++j)
        {
            (*this)(i, j) += other(i, j);
        }
    }
    return *this;
}

template <size_t Rows, size_t Cols, typename Prec>
inline Matrix<Rows, Cols, Prec>&
Matrix<Rows, Cols, Prec>::operator-=(const Matrix<Rows, Cols, Prec>& other)
{
    for (size_t i = 0; i < std::max<size_t>(Rows, 1); ++i)
    {
        for (size_t j = 0; j < std::max<size_t>(Cols, 1); ++j)
        {
            (*this)(i, j) -= other(i, j);
        }
    }
    return *this;
}

template <size_t Rows, size_t Cols, typename Prec>
inline Matrix<Rows, Cols, Prec>&
Matrix<Rows, Cols, Prec>::operator*=(const Matrix<Cols, Cols, Prec>& other)
{
    return (*this = *this * other);
}

template <size_t Rows, size_t Cols, typename Prec, typename OPrec>
inline typename
std::enable_if_t<std::is_arithmetic<OPrec>::value, Matrix<Rows, Cols, Prec>>
operator*(Matrix<Rows, Cols, Prec> mat, OPrec scalar)
{
    return mat *= scalar;
}

template <size_t Rows, size_t Cols, typename Prec, typename OPrec>
inline Matrix<Rows, Cols, Prec>
operator*(OPrec scalar, Matrix<Rows, Cols, Prec> mat)
{
    return mat *= scalar;
}

template <size_t Rows, size_t Cols, typename Prec, typename OPrec>
inline Matrix<Rows, Cols, Prec>
operator/(Matrix<Rows, Cols, Prec> mat, OPrec scalar)
{
    return mat /= scalar;
}


template <size_t Rows, size_t Cols, typename Prec>
inline typename std::enable_if<(Rows > 1 && Cols > 1), std::ostream&>::type
operator<<(std::ostream& ost, const Matrix<Rows, Cols, Prec>& mat)
{
    ost << "[";
    for (size_t i = 0; i < std::max<size_t>(Rows, 1); ++i)
    {
        ost << "[";
        for (size_t j = 0; j < std::max<size_t>(Cols, 1); ++j)
        {
            ost << mat(i,j);
            if (j+1 < std::max<size_t>(Cols, 1)) ost << ", ";
        }
        ost << "]";
        if (i+1 < std::max<size_t>(Rows, 1)) ost << ", ";
    }
    ost << "]";
    return ost;
}

template <size_t Rows, size_t Cols, typename Prec>
inline typename std::enable_if<(Rows <= 1) || (Cols <= 1), std::ostream&>::type
operator<<(std::ostream& ost, const Matrix<Rows, Cols, Prec>& mat)
{
    constexpr static size_t Length = std::max<size_t>(Rows, 1)*std::max<size_t>(Cols, 1);
    ost << "[";
    for (size_t i = 0; i < Length; ++i)
    {
        ost << mat(i);
        if (i+1 < Length) ost << ", ";
    }
    ost << "]";
    return ost;
}

template <size_t Rows, size_t Cols, typename Prec>
inline typename std::enable_if_t<(Rows <= 1) || (Cols <= 1), std::istream&>
operator>>(std::istream& ist, Matrix<Rows, Cols, Prec>& mat)
{
    char curr = '\0';
    while (ist.good() && curr != '[')
    {
        curr = (char)ist.get();
    }
    if (!ist.good()) return ist;

    constexpr static size_t Length = std::max<size_t>(std::max(Rows, Cols), 1);

    for (size_t i = 0; i < Length; ++i)
    {
        ist >> mat[i];
        if (!ist.good()) return ist;
        while (ist.good() && ((curr = (char)ist.peek()) == ',' || std::iswspace(curr)))
            ist.get();
        if (i + 1 == Length)
        {
            if (curr == ']') return ist;
        }
    }
    ist.setstate(std::ios::failbit);
    return ist;
}

template <size_t Rows, size_t Cols, typename Prec>
inline typename std::enable_if_t<(Rows > 1) && (Cols > 1), std::istream&>
operator>>(std::istream& ist, Matrix<Rows, Cols, Prec>& mat)
{
    char curr = '\0';
    while (ist.good() && curr != '[')
    {
        curr = (char)ist.get();
    }
    if (!ist.good()) return ist;

    Matrix<1, Cols, Prec> reader;

    for (size_t i = 0; i < Rows; ++i)
    {
        ist >> reader;
        if (!ist.good()) return ist;
        mat.setRow(i, reader);
        while (ist.good() && ((curr = (char)ist.peek()) == ',' || std::iswspace(curr)))
            ist.get();
        if (i + 1 == Rows)
        {
            if (curr == ']') return ist;
        }
    }
    ist.setstate(std::ios::failbit);
    return ist;
}

template <size_t Rows, size_t Cols, typename Prec>
[[nodiscard]] inline Matrix<Cols, Rows, Prec>
transpose(const Matrix<Rows, Cols, Prec>& mat)
{
    return mat.transpose();
}

template <size_t Dim, typename Prec>
[[nodiscard]] Prec trace(const Matrix<Dim, Dim, Prec>& mat)
{
    auto tr = Prec(0);
    for (size_t i = 0; i < std::max<size_t>(Dim, 1); ++i)
    {
        tr += mat(i, i);
    }
    return tr;
}

template <size_t Rows, size_t Cols, typename Prec>
[[nodiscard]] inline Matrix<Rows, Cols, Prec>
abs(const Matrix<Rows, Cols, Prec>& mat)
{
    Matrix<Rows, Cols, Prec> res;
    for (size_t i = 0; i < std::max<size_t>(Rows, 1); ++i)
    {
        for (size_t j = 0; j < std::max<size_t>(Cols, 1); ++j)
        {
            res(i, j) = std::abs(mat(i, j));
        }
    }
    return res;
}

template <size_t Rows, size_t Cols, typename Prec>
[[nodiscard]] inline Matrix<Rows, Cols, Prec>
min(const Matrix<Rows, Cols, Prec>& m1,
    const Matrix<Rows, Cols, Prec>& m2)
{
    Matrix<Rows, Cols, Prec> res;
    for (size_t i = 0; i < std::max<size_t>(Rows, 1); ++i)
    {
        for (size_t j = 0; j < std::max<size_t>(Cols, 1); ++j)
        {
            res(i, j) = std::min(m1(i, j), m2(i, j));
        }
    }
    return res;
}

template <size_t Rows, size_t Cols, typename Prec>
[[nodiscard]] inline Matrix<Rows, Cols, Prec>
max(const Matrix<Rows, Cols, Prec>& m1,
    const Matrix<Rows, Cols, Prec>& m2)
{
    Matrix<Rows, Cols, Prec> res;
    for (size_t i = 0; i < std::max<size_t>(Rows, 1); ++i)
    {
        for (size_t j = 0; j < std::max<size_t>(Cols, 1); ++j)
        {
            res(i, j) = std::max(m1(i, j), m2(i, j));
        }
    }
    return res;
}

template <size_t Rows, size_t Cols, typename Prec>
[[nodiscard]] inline bool
operator==(const Matrix<Rows, Cols, Prec>& a,
           const Matrix<Rows, Cols, Prec>& b)
{
    for (size_t i = 0; i < std::max<size_t>(Rows, 1)*std::max<size_t>(Cols, 1); ++i)
    {
        if (a.data()[i] != b.data()[i]) return false;
    }
    return true;
}

template <size_t Rows, size_t Cols, typename Prec>
[[nodiscard]] inline bool
operator!=(const Matrix<Rows, Cols, Prec>& a,
           const Matrix<Rows, Cols, Prec>& b)
{
    return !(a == b);
}

template <size_t Length, size_t ZeroOne, typename Prec>
[[nodiscard]]
inline typename std::enable_if<(Length > 1) && (ZeroOne <= 1), bool>::type
operator==(const Matrix<Length, ZeroOne, Prec>& a,
           const Matrix<ZeroOne, Length, Prec>& b)
{
    for (size_t i = 0; i < Length; ++i)
    {
        if (a.data()[i] != b.data()[i]) return false;
    }
    return true;
}

template <size_t Length, size_t ZeroOne, typename Prec>
[[nodiscard]]
inline typename std::enable_if<(Length > 1) && (ZeroOne <= 1), bool>::type
operator!=(const Matrix<Length, ZeroOne, Prec>& a,
           const Matrix<ZeroOne, Length, Prec>& b)
{
    return !(a == b);
}

template <size_t Length, size_t ZeroOne, typename Prec>
[[nodiscard]]
inline typename std::enable_if<(Length > 1) && (ZeroOne <= 1), bool>::type
operator==(const Matrix<ZeroOne, Length, Prec>& a,
           const Matrix<Length, ZeroOne, Prec>& b)
{
    for (size_t i = 0; i < Length; ++i)
    {
        if (a.data()[i] != b.data()[i]) return false;
    }
    return true;
}

template <size_t Length, size_t ZeroOne, typename Prec>
[[nodiscard]]
inline typename std::enable_if<(Length > 1) && (ZeroOne <= 1), bool>::type
operator!=(const Matrix<ZeroOne, Length, Prec>& a,
           const Matrix<Length, ZeroOne, Prec>& b)
{
    return !(a == b);
}

template <size_t Rows, size_t Cols, typename Prec>
[[nodiscard]]
inline bool zero(const Matrix<Rows, Cols, Prec>& mat,
                 Prec epsilon = 20 * Constant<Prec>::epsilon)
{
    for (size_t i = 0;
         i < std::max<size_t>(Rows, 1) * std::max<size_t>(Cols, 1);
         ++i)
    {
        if (!zero(std::abs(mat.data()[i]), epsilon)) return false;
    }
    return true;
}

template <typename NewPrec, size_t Rows, size_t Cols, typename Prec>
[[nodiscard]]
inline auto prec_cast(const Matrix<Rows, Cols, Prec>& mat) noexcept
{
    return mat.template cast<NewPrec>();
}

template <size_t Rows, size_t Cols, typename Prec>
[[nodiscard]]
inline auto lowp_cast(const Matrix<Rows, Cols, Prec>& mat)
{
    return prec_cast<float>(mat);
}

template <size_t Rows, size_t Cols, typename Prec>
[[nodiscard]]
inline auto highp_cast(const Matrix<Rows, Cols, Prec>& mat)
{
    return prec_cast<double>(mat);
}

////////////////////////////////////////////////////////////////////////////////
/// Vectors
////////////////////////////////////////////////////////////////////////////////
template <typename Prec>
class Matrix<2, 1, Prec>
{
public:
    static constexpr size_t RowCount = 2;
    static constexpr size_t ColumnCount = 1;

    union {Prec x, r, s;};
    union {Prec y, g, t;};

    Matrix() = default;
    explicit Matrix(Prec elem0, Prec elem1 = Prec(0)) noexcept
    {
        x = elem0;
        y = elem1;
    }
    Matrix(std::initializer_list<Prec> list)
    {
        if (list.size() != 2)
        {
            throw std::runtime_error("Initializer list bad size.");
        }
        auto beg = list.begin();
        x = *beg++;
        y = *beg;
    }

    // Note: Vectors WILL behave differently from matrices here as any
    // additional values will NOT be a diagonal, but rather the next coordinate.
    template <size_t ORows, size_t OCols, typename OPrec>
    explicit Matrix(const Matrix<ORows, OCols, OPrec>& other,
                    Prec coord1 = Prec(0)) noexcept
    {
        x = other.data()[0];
        y = ORows > 1 ? other(1, 0) : coord1;
    }

    // The below cast is well-defined as long as this is a standard-layout type.
    [[nodiscard]] Prec* data() noexcept { return reinterpret_cast<Prec*>(this); }
    [[nodiscard]] const Prec* data() const noexcept { return reinterpret_cast<const Prec*>(this); }

    [[nodiscard]] size_t rowCount() const noexcept { return RowCount; }
    [[nodiscard]] size_t colCount() const noexcept { return ColumnCount; }

    [[nodiscard]] Prec& operator()(size_t i) noexcept { return data()[i]; }
    [[nodiscard]] const Prec& operator()(size_t i) const noexcept { return data()[i]; }
    [[nodiscard]] Prec& operator[](size_t i) noexcept { return data()[i]; }
    [[nodiscard]] const Prec& operator[](size_t i) const noexcept { return data()[i]; }

    [[nodiscard]] const Prec& operator()(size_t i, size_t) const noexcept
    {
        return data()[i];
    }

    [[nodiscard]] Prec& operator()(size_t i, size_t) noexcept
    {
        return data()[i];
    }

    template <typename NewPrec>
    [[nodiscard]] auto cast() const noexcept
    {
        return Matrix<2, 1, NewPrec>
        {
            static_cast<NewPrec>(x),
            static_cast<NewPrec>(y)
        };
    }

    [[nodiscard]] Prec row(size_t i) const noexcept
    {
        return data()[i];
    }

    void setRow(size_t i, Prec rowVal) noexcept
    {
        data()[i] = rowVal;
    }

    [[nodiscard]] const Matrix<2, 1, Prec>& col(size_t) const noexcept
    {
        return *this;
    }

    [[nodiscard]] Matrix<2, 1, Prec>& col(size_t) noexcept
    {
        return *this;
    }

    void setCol(size_t, const Matrix<2, 1, Prec>& c) noexcept
    {
        *this = c;
    }
    void setCol(size_t, const Matrix<1, 2, Prec>& c) noexcept
    {
        x = c(0);
        y = c(1);
    }

    Matrix<2, 1, Prec>& operator+=(const Matrix<2, 1, Prec>& other) noexcept
    {
        x += other.x;
        y += other.y;
        return *this;
    }

    Matrix<2, 1, Prec>& operator-=(const Matrix<2, 1, Prec>& other) noexcept
    {
        x -= other.x;
        y -= other.y;
        return *this;
    }

    template <typename OPrec>
    Matrix<2, 1, Prec>& operator*=(OPrec scalar) noexcept
    {
        x *= scalar;
        y *= scalar;
        return *this;
    }

    template <typename OPrec>
    Matrix<2, 1, Prec>& operator/=(OPrec scalar) noexcept
    {
        x /= scalar;
        y /= scalar;
        return *this;
    }

    [[nodiscard]] Matrix<2, 1, Prec> operator+() const noexcept
    {
        return {+x, +y};
    }

    [[nodiscard]] Matrix<2, 1, Prec> operator-() const noexcept
    {
        return {-x, -y};
    }

    [[nodiscard]] Matrix<1, 2, Prec> transpose() const noexcept
    {
        return {x, y};
    }
};

template <typename Prec>
class Matrix<3, 1, Prec>
{
public:
    static constexpr size_t RowCount = 3;
    static constexpr size_t ColumnCount = 1;

    union {Prec x, r, s;};
    union {Prec y, g, t;};
    union {Prec z, b, p;};

    Matrix() = default;
    explicit Matrix(Prec elem0,
                    Prec elem1 = Prec(0),
                    Prec elem2 = Prec(0)) noexcept
    {
        x = elem0;
        y = elem1;
        z = elem2;
    }
    Matrix(std::initializer_list<Prec> list)
    {
        if (list.size() != 3)
        {
            throw std::runtime_error("Initializer list bad size.");
        }
        auto beg = list.begin();
        x = *beg++;
        y = *beg++;
        z = *beg;
    }

    // Note: To implement the vector-widening with additional arguments, we need
    // to add explicit constructors for multiple different sizes.
    // First a generic one for truncation.
    template <size_t ORows, size_t OCols, typename OPrec, typename = std::enable_if_t<(ORows >= 3)>>
    explicit Matrix(const Matrix<ORows, OCols, OPrec>& other) noexcept
    {
        x = other.data()[0];
        y = other(1, 0);
        z = other(2, 0);
    }
    // And then for expansion.
    template <size_t OCols, typename OPrec>
    explicit Matrix(const Matrix<2, OCols, OPrec>& other,
                    Prec coord2 = Prec(0)) noexcept
    {
        x = other.data()[0];
        y = other(1, 0);
        z = coord2;
    }

    template <size_t OCols, typename OPrec>
    explicit Matrix(const Matrix<1, OCols, OPrec>& other,
                    Prec coord1 = Prec(0),
                    Prec coord2 = Prec(0)) noexcept
    {
        x = other.data()[0];
        y = coord1;
        z = coord2;
    }

    // The below cast is well-defined as long as this is a standard-layout type.
    [[nodiscard]] Prec* data() noexcept { return reinterpret_cast<Prec*>(this); }
    [[nodiscard]] const Prec* data() const noexcept { return reinterpret_cast<const Prec*>(this); }

    [[nodiscard]] size_t rowCount() const noexcept { return RowCount; }
    [[nodiscard]] size_t colCount() const noexcept { return ColumnCount; }

    [[nodiscard]] Prec& operator()(size_t i) noexcept { return data()[i]; }
    [[nodiscard]] const Prec& operator()(size_t i) const noexcept { return data()[i]; }
    [[nodiscard]] Prec& operator[](size_t i) noexcept { return data()[i]; }
    [[nodiscard]] const Prec& operator[](size_t i) const noexcept { return data()[i]; }

    [[nodiscard]] const Prec& operator()(size_t i, size_t) const noexcept
    {
        return data()[i];
    }

    [[nodiscard]] Prec& operator()(size_t i, size_t) noexcept
    {
        return data()[i];
    }

    template <typename NewPrec>
    [[nodiscard]] auto cast() const noexcept
    {
        return Matrix<3, 1, NewPrec>
        {
            static_cast<NewPrec>(x),
            static_cast<NewPrec>(y),
            static_cast<NewPrec>(z)
        };
    }

    [[nodiscard]] Prec row(size_t i) const noexcept
    {
        return data()[i];
    }

    void setRow(size_t i, Prec rowVal) noexcept
    {
        data()[i] = rowVal;
    }

    [[nodiscard]] const Matrix<3, 1, Prec>& col(size_t) const noexcept
    {
        return *this;
    }

    [[nodiscard]] Matrix<3, 1, Prec>& col(size_t) noexcept
    {
        return *this;
    }

    void setCol(size_t, const Matrix<3, 1, Prec>& c) noexcept
    {
        *this = c;
    }

    void setCol(size_t, const Matrix<1, 3, Prec>& c) noexcept
    {
        x = c(0);
        y = c(1);
        z = c(2);
    }

    Matrix<3, 1, Prec>& operator+=(const Matrix<3, 1, Prec>& other) noexcept
    {
        x += other.x;
        y += other.y;
        z += other.z;
        return *this;
    }

    Matrix<3, 1, Prec>& operator-=(const Matrix<3, 1, Prec>& other) noexcept
    {
        x -= other.x;
        y -= other.y;
        z -= other.z;
        return *this;
    }

    template <typename OPrec>
    Matrix<3, 1, Prec>& operator*=(OPrec scalar) noexcept
    {
        x *= scalar;
        y *= scalar;
        z *= scalar;
        return *this;
    }

    template <typename OPrec>
    Matrix<3, 1, Prec>& operator/=(OPrec scalar) noexcept
    {
        x /= scalar;
        y /= scalar;
        z /= scalar;
        return *this;
    }

    [[nodiscard]] Matrix<3, 1, Prec> operator+() const noexcept
    {
        return {+x, +y, +z};
    }

    [[nodiscard]] Matrix<3, 1, Prec> operator-() const noexcept
    {
        return {-x, -y, -z};
    }

    [[nodiscard]] Matrix<1, 3, Prec> transpose() const noexcept
    {
        return {x, y, z};
    }
};

template <typename Prec>
class Matrix<4, 1, Prec>
{
public:
    static constexpr size_t RowCount = 4;
    static constexpr size_t ColumnCount = 1;

    union {Prec x, r, s;};
    union {Prec y, g, t;};
    union {Prec z, b, p;};
    union {Prec w, a, q;};

    Matrix() = default;
    explicit Matrix(Prec elem0,
                    Prec elem1 = Prec(0),
                    Prec elem2 = Prec(0),
                    Prec elem3 = Prec(0))
    {
        x = elem0;
        y = elem1;
        z = elem2;
        w = elem3;
    }
    Matrix(std::initializer_list<Prec> list)
    {
        if (list.size() != 4)
        {
            throw std::runtime_error("Initializer list bad size.");
        }
        auto beg = list.begin();
        x = *beg++;
        y = *beg++;
        z = *beg++;
        w = *beg;
    }

    template <size_t ORows, size_t OCols, typename OPrec>
    explicit Matrix(const Matrix<ORows, OCols, OPrec>& other, Prec /*diag*/ = Prec(1)) noexcept
    {
        x = other.data()[0];
        y = ORows > 1 ? other(1, 0) : Prec(0);
        z = ORows > 2 ? other(2, 0) : Prec(0);
        w = ORows > 3 ? other(3, 0) : Prec(0);
    }

    // Note: To implement the vector-widening with additional arguments, we need
    // to add explicit constructors for multiple different sizes.
    template <size_t ORows, size_t OCols, typename OPrec, typename = std::enable_if_t<(ORows >= 4)>>
    explicit Matrix(const Matrix<ORows, OCols, OPrec>& other) noexcept
    {
        x = other.data()[0];
        y = other(1, 0);
        z = other(2, 0);
        w = other(3, 0);
    }
    template <size_t OCols, typename OPrec>
    explicit Matrix(const Matrix<1, OCols, OPrec>& other,
                    Prec coord1 = Prec(0),
                    Prec coord2 = Prec(0),
                    Prec coord3 = Prec(0)) noexcept
    {
        x = other.data()[0];
        y = coord1;
        z = coord2;
        w = coord3;
    }
    template <size_t OCols, typename OPrec>
    explicit Matrix(const Matrix<2, OCols, OPrec>& other,
                    Prec coord2 = Prec(0),
                    Prec coord3 = Prec(0)) noexcept
    {
        x = other.data()[0];
        y = other(1, 0);
        z = coord2;
        w = coord3;
    }
    template <size_t OCols, typename OPrec>
    explicit Matrix(const Matrix<3, OCols, OPrec>& other,
                    Prec coord3 = Prec(0)) noexcept
    {
        x = other.data()[0];
        y = other(1, 0);
        z = other(2, 0);
        w = coord3;
    }

    template <typename NewPrec>
    [[nodiscard]] auto cast() const noexcept
    {
        return Matrix<4, 1, NewPrec>
        {
            static_cast<NewPrec>(x),
            static_cast<NewPrec>(y),
            static_cast<NewPrec>(z),
            static_cast<NewPrec>(w)
        };
    }

    // The below cast is well-defined as long as this is a standard-layout type.
    [[nodiscard]] Prec* data() noexcept { return reinterpret_cast<Prec*>(this); }
    [[nodiscard]] const Prec* data() const noexcept { return reinterpret_cast<const Prec*>(this); }

    [[nodiscard]] size_t rowCount() const noexcept { return RowCount; }
    [[nodiscard]] size_t colCount() const noexcept { return ColumnCount; }

    [[nodiscard]] Prec& operator()(size_t i) noexcept { return data()[i]; }
    [[nodiscard]] const Prec& operator()(size_t i) const noexcept { return data()[i]; }
    [[nodiscard]] Prec& operator[](size_t i) noexcept { return data()[i]; }
    [[nodiscard]] const Prec& operator[](size_t i) const noexcept { return data()[i]; }

    [[nodiscard]] const Prec& operator()(size_t i, size_t) const noexcept
    {
        return data()[i];
    }

    [[nodiscard]] Prec& operator()(size_t i, size_t) noexcept
    {
        return data()[i];
    }

    [[nodiscard]] Prec row(size_t i) const noexcept
    {
        return data()[i];
    }

    void setRow(size_t i, Prec rowVal) noexcept
    {
        data()[i] = rowVal;
    }

    [[nodiscard]] const Matrix<4, 1, Prec>& col(size_t) const noexcept
    {
        return *this;
    }

    [[nodiscard]] Matrix<4, 1, Prec>& col(size_t) noexcept
    {
        return *this;
    }

    template <size_t OCols>
    typename std::enable_if_t<(OCols <= 1), void>
    setCol(size_t, const Matrix<4, OCols, Prec>& c) noexcept
    {
        *this = {c(0), c(1), c(2), c(3)};
    }

    template <size_t OCols>
    typename std::enable_if_t<(OCols <= 1), void>
    setCol(size_t, const Matrix<OCols, 4, Prec>& c) noexcept
    {
        *this = {c(0), c(1), c(2), c(3)};
    }

    Matrix<4, 1, Prec>& operator+=(const Matrix<4, 1, Prec>& other) noexcept
    {
        x += other.x;
        y += other.y;
        z += other.z;
        w += other.w;
        return *this;
    }

    Matrix<4, 1, Prec>& operator-=(const Matrix<4, 1, Prec>& other) noexcept
    {
        x -= other.x;
        y -= other.y;
        z -= other.z;
        w -= other.w;
        return *this;
    }

    template <typename OPrec>
    Matrix<4, 1, Prec>& operator*=(OPrec scalar) noexcept
    {
        x *= scalar;
        y *= scalar;
        z *= scalar;
        w *= scalar;
        return *this;
    }

    template <typename OPrec>
    Matrix<4, 1, Prec>& operator/=(OPrec scalar) noexcept
    {
        x /= scalar;
        y /= scalar;
        z /= scalar;
        w /= scalar;
        return *this;
    }

    [[nodiscard]] Matrix<4, 1, Prec> operator+() const noexcept
    {
        return {+x, +y, +z, +w};
    }

    [[nodiscard]] Matrix<4, 1, Prec> operator-() const noexcept
    {
        return {-x, -y, -z, -w};
    }

    [[nodiscard]] Matrix<1, 4, Prec> transpose() const noexcept
    {
        return {x, y, z, w};
    }
};

// Quaternion
template <typename Prec>
class Matrix<4, 0, Prec>
{
public:
    static constexpr size_t RowCount = 4;
    static constexpr size_t ColumnCount = 1;

    Prec x;
    Prec y;
    Prec z;
    Prec w;

    Matrix() = default;
    explicit Matrix(Prec diag)
    {
        x = Prec(0);
        y = Prec(0);
        z = Prec(0);
        w = diag;
    }
    Matrix(std::initializer_list<Prec> list)
    {
        if (list.size() != 4)
        {
            throw std::runtime_error("Initializer list bad size.");
        }
        auto beg = list.begin();
        x = *beg++;
        y = *beg++;
        z = *beg++;
        w = *beg;
    }

    template <size_t ORows, size_t OCols, typename OPrec>
    explicit Matrix(const Matrix<ORows, OCols, OPrec>& other) noexcept
    {
        x = other.data()[0];
        y = ORows > 1 ? other(1, 0) : Prec(0);
        z = ORows > 2 ? other(2, 0) : Prec(0);
        w = ORows > 3 ? other(3, 0) : Prec(0);
    }

    template <typename OPrec>
    explicit Matrix(Prec real, const Matrix<3, 1, OPrec>& imag) noexcept
    {
        x = imag.x;
        y = imag.y;
        z = imag.z;
        w = real;
    }

    template <typename OPrec>
    explicit Matrix(const Matrix<3, 1, OPrec>& imag, Prec real) noexcept
    {
        x = imag.x;
        y = imag.y;
        z = imag.z;
        w = real;
    }

    [[nodiscard]] static Matrix<4, 0, Prec> rotation(Matrix<3, 1, Prec> axis, Prec angle) noexcept
    {
        angle *= Prec(.5);
        Prec s = std::sin(angle);
        return Matrix<4, 0, Prec>{axis.x * s, axis.y * s, axis.z * s,
                                  std::cos(angle)};
    }

    // The below cast is well-defined as long as this is a standard-layout type.
    [[nodiscard]] Prec* data() noexcept { return reinterpret_cast<Prec*>(this); }
    [[nodiscard]] const Prec* data() const noexcept { return reinterpret_cast<const Prec*>(this); }

    [[nodiscard]] size_t rowCount() const noexcept { return RowCount; }
    [[nodiscard]] size_t colCount() const noexcept { return ColumnCount; }

    [[nodiscard]] Prec& operator()(size_t i) noexcept { return data()[i]; }
    [[nodiscard]] const Prec& operator()(size_t i) const noexcept { return data()[i]; }
    [[nodiscard]] Prec& operator[](size_t i) noexcept { return data()[i]; }
    [[nodiscard]] const Prec& operator[](size_t i) const noexcept { return data()[i]; }

    [[nodiscard]] const Prec& operator()(size_t i, size_t) const noexcept
    {
        return data()[i];
    }

    [[nodiscard]] Prec& operator()(size_t i, size_t) noexcept
    {
        return data()[i];
    }

    template <typename NewPrec>
    [[nodiscard]] auto cast() const noexcept
    {
        return Matrix<4, 0, NewPrec>
        {
            static_cast<NewPrec>(x),
            static_cast<NewPrec>(y),
            static_cast<NewPrec>(z),
            static_cast<NewPrec>(w)
        };
    }

    [[nodiscard]] Prec row(size_t i) const noexcept
    {
        return data()[i];
    }

    void setRow(size_t i, Prec rowVal) noexcept
    {
        data()[i] = rowVal;
    }

    [[nodiscard]] const Matrix<4, 1, Prec>& col(size_t) const noexcept
    {
        return {x, y, z, w};
    }

    template <size_t OCols>
    typename std::enable_if_t<(OCols <= 1), void>
    setCol(size_t, const Matrix<4, OCols, Prec>& c) noexcept
    {
        *this = {c(0), c(1), c(2), c(3)};
    }

    template <size_t OCols>
    typename std::enable_if_t<(OCols <= 1), void>
    setCol(size_t, const Matrix<OCols, 4, Prec>& c) noexcept
    {
        *this = {c(0), c(1), c(2), c(3)};
    }

    Matrix<4, 0, Prec>& operator+=(const Matrix<4, 0, Prec>& other) noexcept
    {
        x += other.x;
        y += other.y;
        z += other.z;
        w += other.w;
        return *this;
    }

    Matrix<4, 0, Prec>& operator-=(const Matrix<4, 0, Prec>& other) noexcept
    {
        x -= other.x;
        y -= other.y;
        z -= other.z;
        w -= other.w;
        return *this;
    }

    template <typename OPrec>
    Matrix<4, 0, Prec>& operator*=(OPrec scalar) noexcept
    {
        x *= scalar;
        y *= scalar;
        z *= scalar;
        w *= scalar;
        return *this;
    }

    template <typename OPrec>
    Matrix<4, 0, Prec>& operator/=(OPrec scalar) noexcept
    {
        x /= scalar;
        y /= scalar;
        z /= scalar;
        w /= scalar;
        return *this;
    }

    [[nodiscard]] Matrix<4, 0, Prec> operator+() const noexcept
    {
        return {+x, +y, +z, +w};
    }

    [[nodiscard]] Matrix<4, 0, Prec> operator-() const noexcept
    {
        return {-x, -y, -z, -w};
    }

    [[nodiscard]] Matrix<0, 4, Prec> transpose() const noexcept
    {
        return {x, y, z, w};
    }

    [[nodiscard]] Prec real() const noexcept
    {
        return w;
    }

    [[nodiscard]] Matrix<3, 1, Prec> imag() const noexcept
    {
        return {x, y, z};
    }

    [[nodiscard]] Matrix<4, 0, Prec> conjugate() const noexcept
    {
        return {-x, -y, -z, w};
    }

    [[nodiscard]] Matrix<4, 0, Prec> inverse() const noexcept
    {
        Prec invLengthSquared = Prec(1) / (x*x+y*y+z*z+w*w);
        return conjugate() * invLengthSquared;
    }

    void normalise() noexcept
    {
        Prec invLength = Prec(1) / std::sqrt(x*x+y*y+z*z+w*w);
        *this *= invLength;
    }

    [[nodiscard]] Prec pitch() const noexcept
    {
        return std::atan2(Prec(2) * (w * y + x * z),
                          w * w - x * x - y * y + z * z);
    }

    [[nodiscard]] Prec yaw() const noexcept
    {
        return std::asin(clamp(Prec(2) * (w * x - z * y), Prec(-1), Prec(1)));
    }

    [[nodiscard]] Prec roll() const noexcept
    {
        return std::atan2(Prec(2) * (w * z + x * y),
                          w * w - x * x + y * y - z * z);
    }

    [[nodiscard]] Matrix<3, 1, Prec> eulerAngles() const noexcept
    {
        return {pitch(), yaw(), roll()};
    }

    [[nodiscard]] Matrix<3, 1, Prec> rotateVector(const Matrix<3, 1, Prec>& v) const noexcept
    {
        auto vAsQuat = Matrix<4, 0, Prec>(v, 0);
        return (*this * vAsQuat * conjugate()).imag();
    }
};

template <typename Prec>
using Quaternion = Matrix<4, 0, Prec>;

template <size_t Length, typename Prec>
using Vector = Matrix<Length, 1, Prec>;

template <size_t Length, typename Prec>
using RowVector = Matrix<1, Length, Prec>;

////////////////////////////////////////////////////////////////////////////////
/// Vector operations
////////////////////////////////////////////////////////////////////////////////

template <size_t Length, size_t Cols, typename Prec>
[[nodiscard]] inline typename std::enable_if_t<(Cols <= 1), Prec>
dot(const Matrix<Length, Cols, Prec>& a, const Matrix<Length, Cols, Prec>& b) noexcept
{
    Prec val(0);
    for (size_t i = 0; i < Length; ++i)
    {
        val += a(i) * b(i);
    }
    return val;
}

template <size_t Length, size_t Cols, typename Prec>
[[nodiscard]] inline typename std::enable_if_t<(Cols <= 1), Prec>
squaredLength(const Matrix<Length, Cols, Prec>& a) noexcept
{
    return dot(a, a);
}

template <size_t Length, size_t Cols, typename Prec>
[[nodiscard]] inline typename std::enable_if_t<(Cols <= 1), Prec>
length(const Matrix<Length, Cols, Prec>& a) noexcept
{
    return std::sqrt(dot(a, a));
}

template <size_t Length, size_t Cols, typename Prec>
[[nodiscard]] inline typename std::enable_if_t<(Cols <= 1), Matrix<Length, Cols, Prec>>
normalise(Matrix<Length, Cols, Prec> a) noexcept
{
    auto mul = Prec(1) / length(a);
    return a *= mul;
}

template <size_t Length, size_t Cols, typename Prec>
[[nodiscard]] inline typename std::enable_if_t<(Cols <= 1), Matrix<Length, Cols, Prec>>
project(const Matrix<Length, Cols, Prec>& a,
        const Matrix<Length, Cols, Prec>& b) noexcept
{
    return dot(a, b)/dot(b,b)*b;
}

template <size_t Length, size_t Cols, typename Prec>
[[nodiscard]] inline typename std::enable_if_t<(Cols <= 1), Matrix<Length, Cols, Prec>>
projectUnit(const Matrix<Length, Cols, Prec>& a,
            const Matrix<Length, Cols, Prec>& b) noexcept
{
    return dot(a, b) * b;
}

template <typename Prec>
[[nodiscard]] inline Vector<3, Prec>
cross(const Vector<3, Prec>& a, const Vector<3, Prec>& b) noexcept
{
    return
    {
        a.y*b.z-a.z*b.y,
        a.z*b.x-a.x*b.z,
        a.x*b.y-a.y*b.x
    };
}

template <typename Prec>
[[nodiscard]] inline Vector<2, Prec> perp(const Vector<2, Prec>& a) noexcept
{
    return {-a.y, a.x};
}

template <size_t Length, size_t Cols, typename Prec>
[[nodiscard]] inline typename std::enable_if_t<(Cols <= 1), bool>
parallel(const Matrix<Length, Cols, Prec>& a,
         const Matrix<Length, Cols, Prec>& b,
         Prec epsilon = 20 * Constant<Prec>::epsilon) noexcept
{
    // WARNING: This is incorrect for complex vector spaces (needs conjugation).
    // TODO: Fix that.
    auto d = dot(a, b);
    d *= d;
    auto al = dot(a, a);
    auto bl = dot(b, b);
    return d >= al*bl*(Prec(1)-epsilon);
}

////////////////////////////////////////////////////////////////////////////////
/// Row vector operations
////////////////////////////////////////////////////////////////////////////////


template <size_t Length, size_t Rows, typename Prec>
[[nodiscard]] inline typename std::enable_if<(Length > 1 && Rows <= 1), Prec>::type
squaredLength(const Matrix<Rows, Length, Prec>& a) noexcept
{
   return dot(a, a);
}

template <size_t Length, size_t Rows, typename Prec>
[[nodiscard]] inline typename std::enable_if<(Length > 1 && Rows <= 1), Prec>::type
length(const Matrix<Rows, Length, Prec>& a) noexcept
{
   return std::sqrt(dot(a, a));
}

template <size_t Length, size_t Rows, typename Prec>
[[nodiscard]] inline typename std::enable_if<(Length > 1 && Rows <= 1),
                                             Matrix<Rows, Length, Prec>>::type
normalise(Matrix<Rows, Length, Prec> a) noexcept
{
   auto mul = Prec(1) / length(a);
   return a *= mul;
}

template <size_t Length, size_t Rows, typename Prec>
[[nodiscard]] inline typename std::enable_if<(Length > 1 && Rows <= 1),
                                             Matrix<Rows, Length, Prec>>::type
project(const Matrix<Rows, Length, Prec>& a,
        const Matrix<Rows, Length, Prec>& b) noexcept
{
   return dot(a, b)/dot(b,b)*b;
}

template <size_t Length, size_t Rows, typename Prec>
[[nodiscard]] inline typename std::enable_if<(Length > 1 && Rows <= 1), Prec>::type
dot(const Matrix<Rows, Length, Prec>& a, const Matrix<Rows, Length, Prec>& b) noexcept
{
    Prec val(0);
    for (size_t i = 0; i < Length; ++i)
    {
        val += a(i) * b(i);
    }
    return val;
}

////////////////////////////////////////////////////////////////////////////////
/// Basic quaternion operations
////////////////////////////////////////////////////////////////////////////////

template <typename Prec>
[[nodiscard]] Prec real(const Quaternion<Prec>& q) noexcept
{
    return q.real();
}

template <typename Prec>
[[nodiscard]] Vector<3, Prec> imag(const Quaternion<Prec>& q) noexcept
{
    return q.imag();
}

template <typename Prec>
[[nodiscard]] Quaternion<Prec> conjugate(const Quaternion<Prec>& q) noexcept
{
    return q.conjugate();
}

template <typename Prec>
[[nodiscard]] Quaternion<Prec> inverse(const Quaternion<Prec>& q) noexcept
{
    return q.inverse();
}

template <typename Prec>
[[nodiscard]] Quaternion<Prec> normalise(Quaternion<Prec> q) noexcept
{
    q.normalise();
    return q;
}

////////////////////////////////////////////////////////////////////////////////
/// Basic matrix operations
////////////////////////////////////////////////////////////////////////////////
template <typename Prec>
[[nodiscard]] inline Prec det(const Matrix<2,2,Prec>& m) noexcept
{
    return m(0, 0) * m(1, 1) - m(0, 1) * m(1, 0);
}

template <typename Prec>
[[nodiscard]] inline Matrix<2,2,Prec> inverse(const Matrix<2,2,Prec>& m) noexcept
{
    auto d = Prec(1)/det(m);
    Matrix<2,2,Prec> a;
    a(0, 0) =  m(1, 1)*d;
    a(0, 1) = -m(0, 1)*d;
    a(1, 0) = -m(1, 0)*d;
    a(1, 1) =  m(0, 0)*d;
    return a;
}


template <typename Prec>
[[nodiscard]] inline Prec det(const Matrix<3,3,Prec>& m) noexcept
{
    return  m(0, 0)*(m(1, 1) * m(2, 2) - m(1, 2) * m(2, 1))
           -m(1, 0)*(m(0, 1) * m(2, 2) - m(2, 1) * m(0, 2))
           +m(2, 0)*(m(0, 1) * m(1, 2) - m(1, 1) * m(0, 2));
}

template <typename Prec>
[[nodiscard]] inline Matrix<3,3,Prec> inverse(const Matrix<3,3,Prec>& m) noexcept
{
    Matrix<3,3,Prec> a;
    auto d00 = m(1, 1) * m(2, 2) - m(1, 2) * m(2, 1);
    auto d10 = m(2, 1) * m(0, 2) - m(0, 1) * m(2, 2);
    auto d20 = m(0, 1) * m(1, 2) - m(1, 1) * m(0, 2);

    auto d = Prec(1)/(m(0, 0) * d00 + m(1, 0) * d10 + m(2, 0) * d20);
    a(0, 0) = d00 * d;
    a(1, 0) = (m(2, 0) * m(1, 2) - m(1, 0) * m(2, 2)) * d;
    a(2, 0) = (m(1, 0) * m(2, 1) - m(2, 0) * m(1, 1)) * d;
    a(0, 1) = d10 * d;
    a(1, 1) = (m(0, 0) * m(2, 2) - m(2, 0) * m(0, 2)) * d;
    a(2, 1) = (m(2, 0) * m(0, 1) - m(0, 0) * m(2, 1)) * d;
    a(0, 2) = d20 * d;
    a(1, 2) = (m(1, 0) * m(0, 2) - m(0, 0) * m(1, 2)) * d;
    a(2, 2) = (m(0, 0) * m(1, 1) - m(1, 0) * m(0, 1)) * d;
    return a;
}


// Are the below too complicated / would it be quicker or more stable
// numerically to just use the general algorithms?
template <typename Prec>
[[nodiscard]] inline Prec det(const Matrix<4,4,Prec>& m) noexcept
{
    auto d00 =  m(1, 1)*m(2, 2)*m(3, 3)-m(1, 1)*m(3, 2)*m(2, 3)
               -m(2, 1)*m(1, 2)*m(3, 3)+m(2, 1)*m(3, 2)*m(1, 3)
               +m(3, 1)*m(1, 2)*m(2, 3)-m(3, 1)*m(2, 2)*m(1, 3);
    auto d01 =  m(1, 0)*m(3, 2)*m(2, 3)-m(1, 0)*m(2, 2)*m(3, 3)
               +m(2, 0)*m(1, 2)*m(3, 3)-m(2, 0)*m(3, 2)*m(1, 3)
               -m(3, 0)*m(1, 2)*m(2, 3)+m(3, 0)*m(2, 2)*m(1, 3);
    auto d02 =  m(1, 0)*m(2, 1)*m(3, 3)-m(1, 0)*m(3, 1)*m(2, 3)
               -m(2, 0)*m(1, 1)*m(3, 3)+m(2, 0)*m(3, 1)*m(1, 3)
               +m(3, 0)*m(1, 1)*m(2, 3)-m(3, 0)*m(2, 1)*m(1, 3);
    auto d03 =  m(1, 0)*m(3, 1)*m(2, 2)-m(1, 0)*m(2, 1)*m(3, 2)
               +m(2, 0)*m(1, 1)*m(3, 2)-m(2, 0)*m(3, 1)*m(1, 2)
               -m(3, 0)*m(1, 1)*m(2, 2)+m(3, 0)*m(2, 1)*m(1, 2);
    return m(0, 0) * d00 + m(0, 1) * d01 + m(0, 2) * d02 + m(0, 3) * d03;
}

template <typename Prec>
[[nodiscard]] inline Matrix<4,4,Prec> inverse(const Matrix<4,4,Prec>& m) noexcept
{
    auto d00 =  m(1, 1)*m(2, 2)*m(3, 3)-m(1, 1)*m(3, 2)*m(2, 3)
               -m(2, 1)*m(1, 2)*m(3, 3)+m(2, 1)*m(3, 2)*m(1, 3)
               +m(3, 1)*m(1, 2)*m(2, 3)-m(3, 1)*m(2, 2)*m(1, 3);
    auto d01 =  m(1, 0)*m(3, 2)*m(2, 3)-m(1, 0)*m(2, 2)*m(3, 3)
               +m(2, 0)*m(1, 2)*m(3, 3)-m(2, 0)*m(3, 2)*m(1, 3)
               -m(3, 0)*m(1, 2)*m(2, 3)+m(3, 0)*m(2, 2)*m(1, 3);
    auto d02 =  m(1, 0)*m(2, 1)*m(3, 3)-m(1, 0)*m(3, 1)*m(2, 3)
               -m(2, 0)*m(1, 1)*m(3, 3)+m(2, 0)*m(3, 1)*m(1, 3)
               +m(3, 0)*m(1, 1)*m(2, 3)-m(3, 0)*m(2, 1)*m(1, 3);
    auto d03 =  m(1, 0)*m(3, 1)*m(2, 2)-m(1, 0)*m(2, 1)*m(3, 2)
               +m(2, 0)*m(1, 1)*m(3, 2)-m(2, 0)*m(3, 1)*m(1, 2)
               -m(3, 0)*m(1, 1)*m(2, 2)+m(3, 0)*m(2, 1)*m(1, 2);
    auto d = Prec(1)/(m(0, 0) * d00 + m(0, 1) * d01
                      + m(0, 2) * d02 + m(0, 3) * d03);
    Matrix<4,4,Prec> a;
    a(0, 0) = d00;
    a(1, 0) = d01;
    a(2, 0) = d02;
    a(3, 0) = d03;

    a(0, 1) =  m(0, 1)*m(3, 2)*m(2, 3)-m(0, 1)*m(2, 2)*m(3, 3)
              +m(2, 1)*m(0, 2)*m(3, 3)-m(2, 1)*m(3, 2)*m(0, 3)
              -m(3, 1)*m(0, 2)*m(2, 3)+m(3, 1)*m(2, 2)*m(0, 3);
    a(0, 2) =  m(0, 1)*m(1, 2)*m(3, 3)-m(0, 1)*m(3, 2)*m(1, 3)
              -m(1, 1)*m(0, 2)*m(3, 3)+m(1, 1)*m(3, 2)*m(0, 3)
              +m(3, 1)*m(0, 2)*m(1, 3)-m(3, 1)*m(1, 2)*m(0, 3);
    a(0, 3) =  m(0, 1)*m(2, 2)*m(1, 3)-m(0, 1)*m(1, 2)*m(2, 3)
              +m(1, 1)*m(0, 2)*m(2, 3)-m(1, 1)*m(2, 2)*m(0, 3)
              -m(2, 1)*m(0, 2)*m(1, 3)+m(2, 1)*m(1, 2)*m(0, 3);

    a(1, 1) =  m(0, 0)*m(2, 2)*m(3, 3)-m(0, 0)*m(3, 2)*m(2, 3)
              -m(2, 0)*m(0, 2)*m(3, 3)+m(2, 0)*m(3, 2)*m(0, 3)
              +m(3, 0)*m(0, 2)*m(2, 3)-m(3, 0)*m(2, 2)*m(0, 3);
    a(1, 2) =  m(0, 0)*m(3, 2)*m(1, 3)-m(0, 0)*m(1, 2)*m(3, 3)
              +m(1, 0)*m(0, 2)*m(3, 3)-m(1, 0)*m(3, 2)*m(0, 3)
              -m(3, 0)*m(0, 2)*m(1, 3)+m(3, 0)*m(1, 2)*m(0, 3);
    a(1, 3) =  m(0, 0)*m(1, 2)*m(2, 3)-m(0, 0)*m(2, 2)*m(1, 3)
              -m(1, 0)*m(0, 2)*m(2, 3)+m(1, 0)*m(2, 2)*m(0, 3)
              +m(2, 0)*m(0, 2)*m(1, 3)-m(2, 0)*m(1, 2)*m(0, 3);

    a(2, 1) =  m(0, 0)*m(3, 1)*m(2, 3)-m(0, 0)*m(2, 1)*m(3, 3)
              +m(2, 0)*m(0, 1)*m(3, 3)-m(2, 0)*m(3, 1)*m(0, 3)
              -m(3, 0)*m(0, 1)*m(2, 3)+m(3, 0)*m(2, 1)*m(0, 3);
    a(2, 2) =  m(0, 0)*m(1, 1)*m(3, 3)-m(0, 0)*m(3, 1)*m(1, 3)
              -m(1, 0)*m(0, 1)*m(3, 3)+m(1, 0)*m(3, 1)*m(0, 3)
              +m(3, 0)*m(0, 1)*m(1, 3)-m(3, 0)*m(1, 1)*m(0, 3);
    a(2, 3) =  m(0, 0)*m(2, 1)*m(1, 3)-m(0, 0)*m(1, 1)*m(2, 3)
              +m(1, 0)*m(0, 1)*m(2, 3)-m(1, 0)*m(2, 1)*m(0, 3)
              -m(2, 0)*m(0, 1)*m(1, 3)+m(2, 0)*m(1, 1)*m(0, 3);

    a(3, 1) =  m(0, 0)*m(2, 1)*m(3, 2)-m(0, 0)*m(3, 1)*m(2, 2)
              -m(2, 0)*m(0, 1)*m(3, 2)+m(2, 0)*m(3, 1)*m(0, 2)
              +m(3, 0)*m(0, 1)*m(2, 2)-m(3, 0)*m(2, 1)*m(0, 2);
    a(3, 2) =  m(0, 0)*m(3, 1)*m(1, 2)-m(0, 0)*m(1, 1)*m(3, 2)
              +m(1, 0)*m(0, 1)*m(3, 2)-m(1, 0)*m(3, 1)*m(0, 2)
              -m(3, 0)*m(0, 1)*m(1, 2)+m(3, 0)*m(1, 1)*m(0, 2);
    a(3, 3) =  m(0, 0)*m(1, 1)*m(2, 2)-m(0, 0)*m(2, 1)*m(1, 2)
              -m(1, 0)*m(0, 1)*m(2, 2)+m(1, 0)*m(2, 1)*m(0, 2)
              +m(2, 0)*m(0, 1)*m(1, 2)-m(2, 0)*m(1, 1)*m(0, 2);
    return a * d;
}

////////////////////////////////////////////////////////////////////////////////
/// Typedefs
////////////////////////////////////////////////////////////////////////////////
using  vec2 = Vector<2,F32>;
using dvec2 = Vector<2,F64>;
using ivec2 = Vector<2,S32>;
using uvec2 = Vector<2,U32>;
using  vec3 = Vector<3,F32>;
using dvec3 = Vector<3,F64>;
using ivec3 = Vector<3,S32>;
using uvec3 = Vector<3,U32>;
using  vec4 = Vector<4,F32>;
using dvec4 = Vector<4,F64>;
using ivec4 = Vector<4,S32>;
using uvec4 = Vector<4,U32>;

using  mat2 = Matrix<2,2,F32>;
using dmat2 = Matrix<2,2,F64>;
using imat2 = Matrix<2,2,S32>;
using umat2 = Matrix<2,2,U32>;
using  mat3 = Matrix<3,3,F32>;
using dmat3 = Matrix<3,3,F64>;
using imat3 = Matrix<3,3,S32>;
using umat3 = Matrix<3,3,U32>;
using  mat4 = Matrix<4,4,F32>;
using dmat4 = Matrix<4,4,F64>;
using imat4 = Matrix<4,4,S32>;
using umat4 = Matrix<4,4,U32>;

using quat = Quaternion<F32>;
using dquat = Quaternion<F64>;

////////////////////////////////////////////////////////////////////////////////
/// Static assertions
////////////////////////////////////////////////////////////////////////////////
// Assert that assumptions about the type are correct at compile-time:
// * The sizes are correct.
// * Arrays are tightly packed / there is no padding.
static_assert(std::is_standard_layout_v< vec2>);
static_assert(std::is_standard_layout_v<dvec2>);
static_assert(std::is_standard_layout_v<ivec2>);
static_assert(std::is_standard_layout_v<uvec2>);
static_assert(std::is_trivial_v< vec2>);
static_assert(std::is_trivial_v<dvec2>);
static_assert(std::is_trivial_v<ivec2>);
static_assert(std::is_trivial_v<uvec2>);
static_assert(sizeof( vec2) == size_t(4*2));
static_assert(sizeof(dvec2) == size_t(8*2));
static_assert(sizeof(ivec2) == size_t(4*2));
static_assert(sizeof(uvec2) == size_t(4*2));
static_assert(sizeof( vec2[2]) == sizeof( vec2)*2);
static_assert(sizeof(dvec2[2]) == sizeof(dvec2)*2);
static_assert(sizeof(ivec2[2]) == sizeof(ivec2)*2);
static_assert(sizeof(uvec2[2]) == sizeof(uvec2)*2);

static_assert(std::is_standard_layout_v< vec3>);
static_assert(std::is_standard_layout_v<dvec3>);
static_assert(std::is_standard_layout_v<ivec3>);
static_assert(std::is_standard_layout_v<uvec3>);
static_assert(std::is_trivial_v< vec3>);
static_assert(std::is_trivial_v<dvec3>);
static_assert(std::is_trivial_v<ivec3>);
static_assert(std::is_trivial_v<uvec3>);
static_assert(sizeof( vec3) == size_t(4*3));
static_assert(sizeof(dvec3) == size_t(8*3));
static_assert(sizeof(ivec3) == size_t(4*3));
static_assert(sizeof(uvec3) == size_t(4*3));
static_assert(sizeof( vec3[2]) == sizeof( vec2)*3);
static_assert(sizeof(dvec3[2]) == sizeof(dvec2)*3);
static_assert(sizeof(ivec3[2]) == sizeof(ivec2)*3);
static_assert(sizeof(uvec3[2]) == sizeof(uvec2)*3);

static_assert(std::is_standard_layout_v< vec4>);
static_assert(std::is_standard_layout_v<dvec4>);
static_assert(std::is_standard_layout_v<ivec4>);
static_assert(std::is_standard_layout_v<uvec4>);
static_assert(std::is_trivial_v< vec4>);
static_assert(std::is_trivial_v<dvec4>);
static_assert(std::is_trivial_v<ivec4>);
static_assert(std::is_trivial_v<uvec4>);
static_assert(sizeof( vec4) == size_t(4*4));
static_assert(sizeof(dvec4) == size_t(8*4));
static_assert(sizeof(ivec4) == size_t(4*4));
static_assert(sizeof(uvec4) == size_t(4*4));
static_assert(sizeof( vec4[2]) == sizeof( vec4)*2);
static_assert(sizeof(dvec4[2]) == sizeof(dvec4)*2);
static_assert(sizeof(ivec4[2]) == sizeof(ivec4)*2);
static_assert(sizeof(uvec4[2]) == sizeof(uvec4)*2);

static_assert(std::is_standard_layout_v< mat2>);
static_assert(std::is_standard_layout_v<dmat2>);
static_assert(std::is_standard_layout_v<imat2>);
static_assert(std::is_standard_layout_v<umat2>);
static_assert(std::is_trivial_v< mat2>);
static_assert(std::is_trivial_v<dmat2>);
static_assert(std::is_trivial_v<imat2>);
static_assert(std::is_trivial_v<umat2>);
static_assert(sizeof( mat2) == sizeof( vec2)*2);
static_assert(sizeof(dmat2) == sizeof(dvec2)*2);
static_assert(sizeof(imat2) == sizeof(ivec2)*2);
static_assert(sizeof(umat2) == sizeof(uvec2)*2);
static_assert(sizeof( mat2[2]) == sizeof( mat2)*2);
static_assert(sizeof(dmat2[2]) == sizeof(dmat2)*2);
static_assert(sizeof(imat2[2]) == sizeof(imat2)*2);
static_assert(sizeof(umat2[2]) == sizeof(umat2)*2);

static_assert(std::is_standard_layout_v< mat3>);
static_assert(std::is_standard_layout_v<dmat3>);
static_assert(std::is_standard_layout_v<imat3>);
static_assert(std::is_standard_layout_v<umat3>);
static_assert(std::is_trivial_v< mat3>);
static_assert(std::is_trivial_v<dmat3>);
static_assert(std::is_trivial_v<imat3>);
static_assert(std::is_trivial_v<umat3>);
static_assert(sizeof( mat3) == sizeof( vec3)*3);
static_assert(sizeof(dmat3) == sizeof(dvec3)*3);
static_assert(sizeof(imat3) == sizeof(ivec3)*3);
static_assert(sizeof(umat3) == sizeof(uvec3)*3);
static_assert(sizeof( mat3[2]) == sizeof( mat3)*2);
static_assert(sizeof(dmat3[2]) == sizeof(dmat3)*2);
static_assert(sizeof(imat3[2]) == sizeof(imat3)*2);
static_assert(sizeof(umat3[2]) == sizeof(umat3)*2);

static_assert(std::is_standard_layout_v< mat4>);
static_assert(std::is_standard_layout_v<dmat4>);
static_assert(std::is_standard_layout_v<imat4>);
static_assert(std::is_standard_layout_v<umat4>);
static_assert(std::is_trivial_v< mat4>);
static_assert(std::is_trivial_v<dmat4>);
static_assert(std::is_trivial_v<imat4>);
static_assert(std::is_trivial_v<umat4>);
static_assert(sizeof( mat4) == sizeof( vec4)*4);
static_assert(sizeof(dmat4) == sizeof(dvec4)*4);
static_assert(sizeof(imat4) == sizeof(ivec4)*4);
static_assert(sizeof(umat4) == sizeof(uvec4)*4);
static_assert(sizeof( mat4[2]) == sizeof( mat4)*2);
static_assert(sizeof(dmat4[2]) == sizeof(dmat4)*2);
static_assert(sizeof(imat4[2]) == sizeof(imat4)*2);
static_assert(sizeof(umat4[2]) == sizeof(umat4)*2);

static_assert(std::is_standard_layout_v< quat>);
static_assert(std::is_standard_layout_v<dquat>);
static_assert(std::is_trivial_v< quat>);
static_assert(std::is_trivial_v<dquat>);
static_assert(sizeof( quat) == size_t(4*4));
static_assert(sizeof(dquat) == size_t(8*4));
static_assert(sizeof( quat[2]) == sizeof( quat)*2);
static_assert(sizeof(dquat[2]) == sizeof(dquat)*2);

} // namespace kraken::math

#endif // KRAKEN_MATH_MATRIX_HPP_INCLUDED
