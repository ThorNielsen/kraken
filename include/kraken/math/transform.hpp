#ifndef KRAKEN_MATH_TRANSFORM_HPP_INCLUDED
#define KRAKEN_MATH_TRANSFORM_HPP_INCLUDED

#include <kraken/math/matrix.hpp>
#include <cmath>

namespace kraken
{

namespace math
{

template <typename Prec>
inline Matrix<4, 4, Prec> lookat(const Vector<3, Prec>& eye,
                                 const Vector<3, Prec>& point,
                                 const Vector<3, Prec>& up)
{
    auto d = normalise(point - eye);
    auto s = normalise(cross(d, up));
    auto u = cross(s, d);
    Matrix<4, 4, Prec> m(Prec(1));
    m(0, 0) = s.x;
    m(0, 1) = s.y;
    m(0, 2) = s.z;
    m(1, 0) = u.x;
    m(1, 1) = u.y;
    m(1, 2) = u.z;
    m(2, 0) = -d.x;
    m(2, 1) = -d.y;
    m(2, 2) = -d.z;
    m(0, 3) = -dot(s, eye);
    m(1, 3) = -dot(u, eye);
    m(2, 3) =  dot(d, eye);
    return m;
}

template <typename Prec>
inline Matrix<4, 4, Prec> perspective(Prec fovy, Prec aspect,
                                      Prec near, Prec far)
{
    auto scale = std::tan(fovy/Prec(2));
    Matrix<4, 4, Prec> m(Prec(0));
    m(0, 0) = Prec(1) / (scale * aspect);
    m(1, 1) = Prec(1) / scale;
    m(2, 2) = (near + far) / (near - far);
    m(3, 2) = Prec(-1);
    m(2, 3) = Prec(2) * far * near / (near - far);
    return m;
}

template <typename Prec>
inline Matrix<4, 4, Prec> perspective(Prec left, Prec right, Prec top,
                                      Prec bottom, Prec near, Prec far)
{
    Matrix<4, 4, Prec> m(Prec(0));
    m(0, 0) = Prec(2) * near / (right - left);
    m(1, 1) = Prec(2) * near / (top - bottom);
    m(0, 2) = (right + left) / (right - left);
    m(1, 2) = (top + bottom) / (top - bottom);
    m(2, 2) = (near + far) / (near - far);
    m(3, 2) = Prec(-1);
    m(2, 3) = Prec(2) * near * far / (near - far);
    return m;
}

template <typename Prec>
inline Matrix<3,3,Prec> rotation(Vector<3, Prec> axis, Prec angle)
{
    Prec c = std::cos(angle);
    Prec oc = Prec(1)-c;
    Prec s = std::sin(angle);
    Prec m00 = c + oc*axis.x*axis.x;
    Prec m11 = c + oc*axis.y*axis.y;
    Prec m22 = c + oc*axis.z*axis.z;
    Prec subxy = oc*axis.x*axis.y;
    Prec subxz = oc*axis.x*axis.z;
    Prec subyz = oc*axis.y*axis.z;
    axis *= s;
    return Matrix<3,3,Prec>{m00,          subxy-axis.z, subxz+axis.y,
                            subxy+axis.z, m11,          subyz-axis.x,
                            subxz-axis.y, subyz+axis.x, m22};
}

} // namespace math

} // namespace kraken


#endif // KRAKEN_MATH_TRANSFORM_HPP_INCLUDED
