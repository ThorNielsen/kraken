// This file is part of the Kraken project, and in addition to the license for
// that project (if any), this file itself is also available under the MIT
// License, the text of which follows below.

// MIT License

// Copyright (c) 2022 Thor Gabelgaard Nielsen

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef KRAKEN_TYPES_HPP_INCLUDED
#define KRAKEN_TYPES_HPP_INCLUDED

#include <cstdint>

namespace kraken
{

// Exactly specified size.
using U64 = uint64_t;
using U32 = uint32_t;
using U16 = uint16_t;
using U8  = uint8_t;

using S64 = int64_t;
using S32 = int32_t;
using S16 = int16_t;
using S8  = int8_t;

// At least specified size.
using UL64 = uint_fast64_t;
using UL32 = uint_fast32_t;
using UL16 = uint_fast16_t;
using UL8  = uint_fast8_t;

using SL64 = int_fast64_t;
using SL32 = int_fast32_t;
using SL16 = int_fast16_t;
using SL8  = int_fast8_t;

using F64 = double;
using F32 = float;

// Tests only needed for floats as u?int[0-9]+_t are guaranteed to be the
// correct sizes (or not be defined at all) and tests would therefore be
// redundant. No such guarantees exists for floats, however.
static_assert(sizeof(F32) == 4, "F32 size is wrong.");
static_assert(sizeof(F64) == 8, "F64 size is wrong.");

} // namespace kraken

#endif // KRAKEN_TYPES_HPP_INCLUDED

