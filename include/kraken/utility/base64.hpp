#ifndef KRAKEN_UTILITY_BASE64_HPP_INCLUDED
#define KRAKEN_UTILITY_BASE64_HPP_INCLUDED

#include <cstddef>
#include <iterator>
#include <kraken/types.hpp>
#include <string>

namespace kraken
{

namespace priv
{

inline U32 fromBase64Value(U8 val)
{
    // Switch to switch statement instead?
    if (0x41 <= val && val <= 0x5a) return val - 0x41;
    if (0x61 <= val && val <= 0x7a) return val - (0x61 - 26);
    if (0x30 <= val && val <= 0x39) return val + (52-0x30);
    if (val == '+' || val == '-') return 62;
    if (val == '/' || val == '_') return 63;
    if (val == '=') return 64;
    return 65;
}

inline U8 toBase64Value(U8 sixbits, bool urlsafe)
{
    const char* alphabet = urlsafe
        ? "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"
        : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    return alphabet[sixbits&0x3f];
}

template <typename OutputIterator>
inline OutputIterator outputBase64Decoded(U32 val, U32 bytesRead,
                                          OutputIterator out)
{
    switch (bytesRead)
    {
    case 4:
        *out++ = val >> 16;
        *out++ = (val >> 8) & 0xff;
        *out++ = val & 0xff;
        break;
    case 3:
        *out++ = val >> 10;
        *out++ = (val >> 2) & 0xff;
        break;
    case 2:
        *out++ = val >> 4;
        break;
    default:
        ;
    }
    return out;
}

template <typename OutputIterator>
inline OutputIterator encodeBase64Block(U32 val, U32 storedBytes,
                                        OutputIterator out,
                                        bool urlsafe)
{
    switch (storedBytes)
    {
    case 3:
        *out++ = toBase64Value( val >> 18        , urlsafe);
        *out++ = toBase64Value((val >> 12) & 0x3f, urlsafe);
        *out++ = toBase64Value((val >>  6) & 0x3f, urlsafe);
        *out++ = toBase64Value((val      ) & 0x3f, urlsafe);
        break;
    case 2:
        *out++ = toBase64Value( val >> 10        , urlsafe);
        *out++ = toBase64Value((val >>  4) & 0x3f, urlsafe);
        *out++ = toBase64Value((val <<  2) & 0x3f, urlsafe);
        *out++ = '=';
        break;
    case 1:
        *out++ = toBase64Value( val >> 2        , urlsafe);
        *out++ = toBase64Value((val << 4) & 0x3f, urlsafe);
        *out++ = '=';
        *out++ = '=';
    default:
        ;
    }
    return out;
}

} // namespace priv

template <typename InputIterator, typename OutputIterator>
inline InputIterator base64Decode(InputIterator begin, InputIterator end,
                                  OutputIterator out) noexcept
{
    U32 cVal = 0;
    U32 byteCount = 0;
    while (begin != end)
    {
        auto append = priv::fromBase64Value(*begin++);
        if (append < 64)
        {
            cVal = append | (cVal << 6);
            ++byteCount;
        }
        else if (append == 64)
        {
            // Erroneous padding.
            if (!byteCount) return begin;

            // Absorb padding.
            for (size_t i = 0; i < 3-byteCount; ++i)
            {
                if (begin != end) ++begin;
            }
            // Having eaten the padding, we can now break out of the loop, and
            // then the remaining data will be written.
            break;
        }

        if (byteCount == 4)
        {
            out = priv::outputBase64Decoded(cVal, byteCount, out);
            byteCount = 0;
            cVal = 0;
        }
    }
    out = priv::outputBase64Decoded(cVal, byteCount, out);
    return begin;
}

inline std::string base64Decode(const std::string& str)
{
    std::string output;
    base64Decode(str.begin(), str.end(), std::back_inserter(output));
    return output;
}

template <typename InputIterator, typename OutputIterator>
inline InputIterator base64Encode(InputIterator begin, InputIterator end,
                                  OutputIterator output, bool urlsafe = false)
{
    U32 cVal = 0;
    U32 byteCount = 0;
    while (begin != end)
    {
        cVal = (cVal << 8) | *begin++;
        ++byteCount;
        if (byteCount == 3)
        {
            output = priv::encodeBase64Block(cVal, byteCount, output, urlsafe);
            byteCount = 0;
            cVal = 0;
        }
    }
    output = priv::encodeBase64Block(cVal, byteCount, output, urlsafe);
    return begin;
}

inline std::string base64Encode(const std::string& str, bool urlsafe = false)
{
    std::string output;
    base64Encode(str.begin(), str.end(), std::back_inserter(output), urlsafe);
    return output;
}

} // namespace kraken

#endif // KRAKEN_UTILITY_BASE64_HPP_INCLUDED
