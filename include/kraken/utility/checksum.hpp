#ifndef KRAKEN_UTILITY_CHECKSUM_HPP_INCLUDED
#define KRAKEN_UTILITY_CHECKSUM_HPP_INCLUDED

#include <cstddef>
#include <kraken/types.hpp>
#include <kraken/utility/template_support.hpp>
#include <string>
#include <type_traits>

namespace kraken
{

class CRC32sum
{
public:
    CRC32sum();
    void clear();


    void append(const U8* begin, const U8* end);

    template <typename T>
    auto append(const T* begin, const T* end)
    -> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, void>::type
    {
        append(reinterpret_cast<const U8*>(begin),
               reinterpret_cast<const U8*>(end));
    }

    template <typename T>
    void append(const T* data, size_t length)
    {
        append(data, data+length);
    }


    U32 sum() const;
    operator U32() const;

    operator std::string() const;

private:
    U32 m_crc;
};

class ADLER32sum
{
public:
    ADLER32sum();
    void clear();
    void append(const U8* begin, const U8* end);

    template <typename T>
    auto append(const T* begin, const T* end)
    -> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, void>::type
    {
        append(reinterpret_cast<const U8*>(begin),
               reinterpret_cast<const U8*>(end));
    }

    template <typename T>
    void append(const T* data, size_t length)
    {
        append(data, data+length);
    }

    U32 sum() const;
    operator U32() const;

    operator std::string() const;
private:
    U32 m_s1;
    U32 m_s2;
};

class SHA256sum
{
public:
    SHA256sum();
    void clear();


    void append(const U8* begin, const U8* end);

    template <typename T>
    auto append(const T* begin, const T* end)
    -> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, void>::type
    {
        append(reinterpret_cast<const U8*>(begin),
               reinterpret_cast<const U8*>(end));
    }

    template <typename T>
    void append(const T* data, size_t length)
    {
        append(data, data+length);
    }

    std::string sum() const;
    operator std::string() const;
private:
    void hashBlock(const U32 block[16], U32 hash[8]) const;
    void hashBlock(const U8 block[64], U32 hash[8]) const;
    void updateCached() const;

    // Hash of previous blocks
    U32 m_hash[8];

    // Partial (unhashed) blocks are held here.
    U8 m_currBlockData[64];

    // Length (in bytes) of data processed so far. That is, len(hashed)
    // + len(data in m_currBlockData).
    // Note that as blocks are 64 bytes (512 bits) long, m_dataLen % 64 gives
    // the number of used bytes in the current block. Note furthermore that:
    // x % 64 == x & 0x3f
    // x / 64 == x >> 6
    U64 m_dataLen;

    // When m_cachedValid is true, this holds the SHA256 hash of all data given
    // since tha last call to clear().
    mutable U32 m_cachedHash[8];
    mutable bool m_cachedValid;
};

// Warning: Does not conform exactly to spec -- fails if number of bits are greater than or equal to 2^64 (2^61 bytes).
class SHA512sum
{
public:
    SHA512sum();
    void clear();


    void append(const U8* begin, const U8* end);

    template <typename T>
    auto append(const T* begin, const T* end)
    -> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, void>::type
    {
        append(reinterpret_cast<const U8*>(begin),
               reinterpret_cast<const U8*>(end));
    }

    template <typename T>
    void append(const T* data, size_t length)
    {
        append(data, data+length);
    }

    std::string sum() const;
    operator std::string() const;
private:
    void hashBlock(const U64 block[16], U64 hash[8]) const;
    void hashBlock(const U8 block[128], U64 hash[8]) const;
    void updateCached() const;

    // Almost exactly the same variables as SHA256.
    U64 m_hash[8];
    U8 m_currBlockData[128];
    U64 m_dataLen;

    mutable U64 m_cachedHash[8];
    mutable bool m_cachedValid;
};

} // namespace kraken

#endif // KRAKEN_UTILITY_CHECKSUM_HPP_INCLUDED
