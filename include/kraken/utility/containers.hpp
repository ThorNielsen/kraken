#ifndef KRAKEN_UTILITY_CONTAINERS_HPP_INCLUDED
#define KRAKEN_UTILITY_CONTAINERS_HPP_INCLUDED

#include <cstddef>
#include <functional> // For std::less
#include <kraken/types.hpp>
#include <vector>

namespace kraken
{

template <class Elem, class Compare = std::less<Elem>>
class PriorityQueue
{
public:
    PriorityQueue() = default;
    explicit PriorityQueue(const Compare& comparator)
        : m_comparator{comparator} {}

    ~PriorityQueue() = default;

    PriorityQueue(const PriorityQueue& other)
    {
        m_elems = other.m_elems;
        m_comparator = other.m_comparator;
    }
    PriorityQueue(PriorityQueue&& other) noexcept
    {
        m_elems = std::move(other.m_elems);
        m_comparator = std::move(other.m_comparator);
    }
    PriorityQueue& operator=(const PriorityQueue& other)
    {
        m_elems = other.m_elems;
        m_comparator = other.m_comparator;
        return *this;
    }
    PriorityQueue& operator=(PriorityQueue&& other) noexcept
    {
        m_elems = std::move(other.m_elems);
        m_comparator = std::move(other.m_comparator);
        return *this;
    }


    template <class ... Args>
    void emplace(Args&&... args)
    {
        m_elems.emplace_back(std::forward<Args>(args)...);
        fixupInsertion();
    }
    void push(Elem elem)
    {
        m_elems.push_back(elem);
        fixupInsertion();
    }
    const Elem& top() const { return m_elems[0]; }
    void pop();

    [[nodiscard]] bool empty() const noexcept { return m_elems.empty(); }

private:
    using Index = size_t;
    Index left(Index i) { return i << 1; }
    Index right(Index i) { return (i << 1) | 1; }
    Index parent(Index i) { return i >> 1; }
    void heapify(Index i);
    void fixupInsertion();

    std::vector<Elem> m_elems;
    Compare m_comparator;
};

template <class Key, class Compare = std::less<Key>>
class RedBlackTree
{
public:
    using NodeID = U32;
    struct Node
    {
        Key data;
        NodeID parent;
        NodeID left;
        NodeID right;
    };

    NodeID insert(const Key& elem);
    NodeID find(const Key& elem);
    void erase(NodeID node);
    NodeID minimum();
    NodeID maximum();
    NodeID predecessor(NodeID node);
    NodeID successor(NodeID node);

    Node* get(NodeID id)
    {
        if (id < m_nodes.size()) return &m_nodes[id];
        return nullptr;
    }
    NodeID id(Node* node);

    bool empty() const { return m_nodes.empty(); }
    size_t size() const { return m_nodes.size(); }


#ifdef KRAKEN_UTILITY_REDBLACK_INTERNAL_TESTING
    void performConsistencyCheck();
    bool checkRBCorrect()
    {
        return doRBCheck() >= 0;
    }
    // Returns black height.
    int doRBCheck(NodeID toTest = 0, bool prevBlack = 1);
#endif // KRAKEN_UTILITY_REDBLACK_INTERNAL_TESTING

private:
    NodeID fixupInsertion(NodeID inserted);
    void fixupDeletion(NodeID toFixup, NodeID fixupParent, bool isRight);

    void setParent(Node* node, NodeID parent);
    NodeID parentID(Node* node) const { return node->parent & 0x7fffffff; }

    // Colours: 0 = Red, 1 = Black
    void setColour(Node* node, bool col)
    {
        node->parent = (col << 31) | parentID(node);
    }
    bool isBlack(Node* node) const { return node->parent >> 31; }

    // Deletes a node. It is assumed that the node has no children.
    // If its parent point to it, that parent will have its reference removed.
    void removeNode(NodeID);
    // The same as above, but here the two extra parameters will have their IDs
    // updated if they change (i.e. if it moves preserve2 around in memory, then
    // it will also update the parameter).
    void removeNode(NodeID del, NodeID& preserve1, NodeID& preserve2);

    void transplant(NodeID replacement, NodeID old);

    // Moves "replacement" directly into the position occupied by "toDestroy"
    // and then deletes "toDestroy", if this is possible. However in some cases
    // that position may not be availiable and then the new position of
    // "replacement" is returned. It is assumed that all children are
    // detached from "toDestroy" in the sense that they no longer point to it
    // as a parent. However, the parent of "toDestroy" (if any) is still assumed
    // to point to it.
    // Note: Return value is always the new position of replacement, which is
    // highly likely to be identical to "toDestroy" (but not guaranteed).
    NodeID moveReplace(NodeID replacement, NodeID toDestroy);

    // These return the ID the node which replaces "node", that is, the new top
    // node.
    // y := rotateLeft(x)  ==>  y top, y->left = x
    NodeID rotateLeft(NodeID node); /// OK
    // y := rotateRight(x)  ==>  y top, y->right = x
    NodeID rotateRight(NodeID node); /// OK

    // Swaps the position of nodes a and b in memory. These are the only ones
    // whose memory location change.
    void memswap(NodeID a, NodeID b); /// OK

    constexpr static NodeID invalidID = 0xffffffff;
    std::vector<Node> m_nodes;
};

} // namespace kraken

#include <kraken/utility/priv/priorityqueue.tcc>
#include <kraken/utility/priv/redblacktree.tcc>

#endif // KRAKEN_UTILITY_CONTAINERS_HPP_INCLUDED
