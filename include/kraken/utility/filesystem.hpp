#ifndef KRAKEN_UTILITY_FILESYSTEM_HPP_INCLUDED
#define KRAKEN_UTILITY_FILESYSTEM_HPP_INCLUDED

#include <algorithm>
#include <cstddef>
#include <ios>
#include <kraken/types.hpp>

#include <filesystem>
#include <fstream>
#include <limits>
#include <memory>
#include <string>
#include <utility>
#include <vector>

namespace kraken
{

/// This constructs a path for a temporary file and cleans up automatically.
///
/// The natural usage of this is to generate a path to a file which with
/// exceedingly high probability is unique (this is checked at generation time,
/// but obviously other software might create a file at that exact path, though
/// that is unlikely).
/// Note that the referred file is cleaned up in the destructor (i.e. removed),
/// unless specifically released.
///
/// \threadsafe It is safe to construct multiple of these objects on multiple
///             threads simultaneously; the generated file names will not clash.
///             However, access to a single object must be synchronised.
class TemporaryFile
{
public:
    TemporaryFile(const std::filesystem::path& directory = "");
    ~TemporaryFile();

    TemporaryFile(const TemporaryFile&) = delete;
    TemporaryFile& operator=(const TemporaryFile&) = delete;

    TemporaryFile(TemporaryFile&& other)
    {
        *this = std::move(other);
    }
    TemporaryFile& operator=(TemporaryFile&& other);

    const std::filesystem::path& path() const noexcept { return m_path; }

    std::filesystem::path release() noexcept;

    operator const std::filesystem::path&() const noexcept { return m_path; }
    operator std::string() const noexcept { return m_path.string(); }

private:
    std::filesystem::path m_path;
};

/// Reads an entire file into memory.
///
/// Note that this reads the file in binary mode, so there is no conversion of
/// line endings, etc.
///
/// \tparam BufferAllocationFunction A function taking a number of bytes and
///                                  returns a pointer to an array where that
///                                  many bytes can be written; a null pointer
///                                  otherwise (in which case this function will
///                                  fail).
/// \param path The path to the file.
/// \param bufAlloc A function which takes a number of bytes and returns a
///                 pointer to this many bytes which this function will then
///                 write to. This may be called multiple times with different
///                 sizes if 'maxBufferSize' is smaller than the file in
///                 question. If a null pointer is returned, this function will
///                 merely return false. As far as possible this will avoid
///                 allocating too much memory, but if the file is truncated
///                 while being read (by another process), this may have already
///                 requested a too large amount of memory, so readBytesOut
///                 should be used for verification.
/// \param readBytesOut If not null, this will write how many bytes were in fact
///                     read by this function.
/// \param maxBufferSize Sets the maximal size of the buffer to request from
///                      'bufAlloc'. If unset (or set to the maximal possible),
///                      the entire file will be read in a single operation.
///
/// \returns Whether the entire operation succeeded or not. Note that there is
///          one slightly surprising special case: If this is given an empty
///          file, a successful read of that will return true yet bufAlloc is
///          not necessarily invoked.
template <typename BufferAllocationFunction>
bool readEntireFile(const std::filesystem::path& path,
                    const BufferAllocationFunction& bufAlloc,
                    size_t* readBytesOut = nullptr,
                    size_t maxBufferSize = std::numeric_limits<size_t>::max())
{
    size_t storage;
    if (!readBytesOut) readBytesOut = &storage;
    *readBytesOut = 0;
    std::ifstream input(path, std::ios::binary | std::ios::ate);
    if (!input.is_open()) return false;
    auto endPos = input.tellg();
    input.seekg(0);
    auto beginPos = input.tellg();
    if (endPos < beginPos) return false;
    if (beginPos == endPos) return true;
    auto byteSize = static_cast<size_t>(endPos-beginPos);
    while (byteSize)
    {
        auto chunkSize = std::min(maxBufferSize, byteSize);
        void* writeTo = bufAlloc(chunkSize);
        if (!writeTo) return false;
        if (!input.read(static_cast<char*>(writeTo), chunkSize)) return false;
        byteSize -= chunkSize;
        *readBytesOut += static_cast<size_t>(input.gcount());
    }
    return true;
}

/// Reads an entire file.
///
/// \throws An exception in case the file cannot be read or some other error
///         occurs.
///
/// \param path Path of the file.
///
/// \returns The content of the file stored in a vector.
std::vector<U8> readEntireFile(const std::filesystem::path& path);

/// Reads an entire file.
///
/// \throws An exception if the file cannot be read or some other error occurs.
///
/// \param path The path of the file.
/// \param sizeOut In case this succeeds, the size of the returned array will be
///                written here. Note that technically it is possible for the
///                array to have more storage than what is returned here;
///                however that is highly unlikely in practise.
///
/// \returns The content of the file; or a null pointer in case the file is
///          empty.
std::unique_ptr<U8[]> readEntireFile(const std::filesystem::path& path,
                                     size_t& sizeOut);

} // namespace kraken

#endif // KRAKEN_UTILITY_FILESYSTEM_HPP_INCLUDED
