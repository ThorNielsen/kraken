#ifndef KRAKEN_UTILITY_HUFFTREE_HPP_INCLUDED
#define KRAKEN_UTILITY_HUFFTREE_HPP_INCLUDED

#include <cstddef>
#include <kraken/types.hpp>
#include <utility>
#include <vector>

namespace kraken
{

namespace priv
{

/// A class for building a Huffman tree and decoding the corresponding codes.
///
/// Note that this is NOT capable of decoding general Huffman-encoded data; in
/// particular it only supports codes up to (and including) 16 bits of length.
/// Furthermore, this only supports a very specific subset of potential Huffman
/// codes; namely those with the property that a code precedes another
/// lexicographically if and only if it is of at most the same length (i.e. so
/// if code A is strictly shorter than code B, code A must (when ordered
/// lexicographically) come before code B).
class HufftreeDecoder
{
public:
    struct DecodeResult
    {
        U16 decoded;
        U8 length;
    };

    HufftreeDecoder();
    HufftreeDecoder(const HufftreeDecoder& other)
    {
        *this = other;
    }
    HufftreeDecoder(HufftreeDecoder&& other)
    {
        *this = std::move(other);
    }

    HufftreeDecoder& operator=(const HufftreeDecoder& other);
    HufftreeDecoder& operator=(HufftreeDecoder&& other);

    /// Creates a Huffman tree decoder from the specified code lengths.
    ///
    /// \param pBitLengths Pointer to the bit lengths of the different codes.
    /// \param bitlenCount Number of bit lengths / codes.
    /// \param pValues Optional, may be null. If not null, this MUST for every
    ///                value of pBitLengths have a corresponding code; and then
    ///                this class will be constructed such that subsequent
    ///                decoding operations returns pValues[i] whenever it sees
    ///                the code that pBitLengths[i] corresponds to.
    /// \param startFromLSB If true, the generated Huffman tree will assume
    ///                     codes are to be decoded starting from the LSB of the
    ///                     input numbers. Otherwise they will start from the
    ///                     MSB.
    void create(const U8* pBitLengths,
                size_t bitlenCount,
                const U16* pValues = nullptr,
                bool startFromLSB = true);

    void create(const std::vector<U8>& bitlens);

    /// Decodes a symbol inside 'encoded'.
    ///
    /// \param encoded The next 16 bits of the stream that one decodes from (if
    ///                any) packed such that the most significant bit of this
    ///                variable is the first bit to start the decoding from. If
    ///                less than 16 bits are available, the remainder can be
    ///                padded with arbitrary bits, though zeroes is
    ///                conventional.
    ///
    /// \returns The decoded symbol along with the code length; that can be used
    ///          to determine how far to advance the bit position within a
    ///          bitstream. If decoding fails, a length of zero is returned.
    [[nodiscard]] DecodeResult decodeSymbol(U16 encoded) const noexcept;

    [[nodiscard]] DecodeResult decodeSymbolLSB(U16 encoded) const noexcept;

    [[nodiscard]] DecodeResult decodeSymbolMSB(U16 encoded) const noexcept;

    [[nodiscard]] size_t suggestedInitialBits() const noexcept
    {
        return m_cachebits;
    }

    void clear();

    [[nodiscard]] bool empty() const noexcept { return m_cache.empty(); }

private:
    constexpr static U16 codeBitCount = 11;
    constexpr static U16 codeMask = 0x7ff;

    // Adds a symbol where the decoded value is 'value', the encoded value is
    // 'code' and the code has bit length 'bitlen'.
    void addSymbol(U16 value, U16 code, U8 codeLength);

    void setCacheBits(U8 count);

    std::vector<U16> m_cache;
    std::vector<U16> m_extraCacheIndices;
    std::vector<std::vector<U16>> m_extraCaches;
    U16 m_cacheBitMask;
    U8 m_cachebits;
    bool m_startsFromLSB;
};

} // namespace priv

} // namespace kraken

#endif // KRAKEN_UTILITY_HUFFTREE_HPP_INCLUDED
