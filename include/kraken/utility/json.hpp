#ifndef KRAKEN_UTILITY_JSON_HPP_INCLUDED
#define KRAKEN_UTILITY_JSON_HPP_INCLUDED

#include <initializer_list>
#include <kraken/types.hpp>

#include <cstring>
#include <iosfwd>
#include <map>
#include <optional>
#include <stdexcept>
#include <string>
#include <string_view>
#include <type_traits>
#include <vector>

namespace kraken
{

namespace detail
{

template <typename Numeric>
[[nodiscard]] std::optional<Numeric>
tryConvertTo(const std::string& contents) noexcept
{
    // Double is a reasonable default if no specialisation is available.
    if (auto result = tryConvertTo<double>(contents)) return *result;
    return std::nullopt;
}

template <>
[[nodiscard]] std::optional<double>
tryConvertTo<double>(const std::string& contents) noexcept;

} // namespace detail

enum class JSONType
{
    Null,
    Boolean,
    Double, // = 64-bit floating-point element.
    Number = Double,
    SignedInt, // = 64-bit signed integer
    UnsignedInt, // = 64-bit unsigned integer
    String,
    Array,
    Object,
};

struct JSONValue
{
    using ObjectType = std::map<std::string, JSONValue>;
    using ArrayType = std::vector<JSONValue>;

    JSONValue()
        : m_type{JSONType::Null} {}

    JSONValue(std::string str)
    {
        setToString(std::move(str));
    }

    JSONValue(std::string_view view)
        : JSONValue(std::string(view)) {}

    JSONValue(std::nullptr_t)
        : m_type{JSONType::Null} {}

    JSONValue(const char* str)
    {
        setToString(str);
    }

    template <typename Number>
    JSONValue(Number val) requires (std::is_floating_point_v<Number>)
    {
        setToDouble(val);
    }

    template <typename Number>
    JSONValue(Number val) requires (std::is_integral_v<Number>
                                    && std::is_signed_v<Number>)
    {
        setToSignedInt(val);
    }
    template <typename Number>
    JSONValue(Number val) requires (std::is_integral_v<Number>
                                    && !std::is_signed_v<Number>)
    {
        setToUnsignedInt(val);
    }

    JSONValue(bool b)
    {
        setToBool(b);
    }

    explicit JSONValue(ObjectType&& obj)
    {
        setToObject(std::move(obj));
    }

    explicit JSONValue(ArrayType&& arr)
    {
        setToArray(std::move(arr));
    }

    explicit JSONValue(const ObjectType& obj)
    {
        setToObject(obj);
    }

    explicit JSONValue(const ArrayType& arr)
    {
        setToArray(arr);
    }


    template <typename T>
    JSONValue(std::initializer_list<T> vals)
    {
        m_type = JSONType::Null;
        if (vals.size() == 1)
        {
            *this = JSONValue(*vals.begin());
            return;
        }
        std::size_t idx = 0;
        for (auto v : vals)
        {
            (*this)[idx++] = v;
        }
    }
    ~JSONValue()
    {
        destroy();
    }

    static JSONValue Array()
    {
        JSONValue value;
        value.setToArray({});
        return value;
    }
    static JSONValue Array(std::initializer_list<JSONValue> values)
    {
        JSONValue value;
        value.setToArray({});
        std::size_t idx = 0;
        for (auto&& v : values)
        {
            value[idx++] = std::move(v);
        }
        return value;
    }
    static JSONValue Object()
    {
        JSONValue value;
        value.setToObject({});
        return value;
    }
    static JSONValue Object(std::initializer_list<std::pair<std::string, JSONValue>> values)
    {
        JSONValue value;
        value.setToObject({});
        for (auto&& v : values)
        {
            value.asObject().emplace(std::move(v.first), std::move(v.second));
        }
        return value;
    }
    static JSONValue Object(std::pair<std::string, std::string> v)
    {
        JSONValue value;
        value.setToObject({});
        value.asObject().emplace(std::move(v.first), std::move(v.second));
        return value;
    }

    [[nodiscard]] bool isNull()        const noexcept { return m_type == JSONType::Null; }
    [[nodiscard]] bool isBoolean()     const noexcept { return m_type == JSONType::Boolean; }
    [[nodiscard]] bool isNumeric()     const noexcept { return isDouble() || isSignedInt() || isUnsignedInt(); }
    [[nodiscard]] bool isDouble()      const noexcept { return m_type == JSONType::Double; }
    [[nodiscard]] bool isSignedInt()   const noexcept { return m_type == JSONType::SignedInt; }
    [[nodiscard]] bool isUnsignedInt() const noexcept { return m_type == JSONType::UnsignedInt; }
    [[nodiscard]] bool isString()      const noexcept { return m_type == JSONType::String; }
    [[nodiscard]] bool isObject()      const noexcept { return m_type == JSONType::Object; }
    [[nodiscard]] bool isArray()       const noexcept { return m_type == JSONType::Array; }

    [[deprecated("isNumber() will either be removed entirely or take on a "
                 "different meaning, use isDouble()/isSignedInt()/isUnsignedInt()"
                 " for the old functionality.")]]
    [[nodiscard]] bool isNumber()      const noexcept { return m_type == JSONType::Number; }

    [[nodiscard]] std::string& asString()      noexcept { return *m_data.asString; }
    [[nodiscard]] F64&         asDouble()      noexcept { return m_data.asDouble; }
    [[nodiscard]] S64&         asSignedInt()   noexcept { return m_data.asSInt; }
    [[nodiscard]] U64&         asUnsignedInt() noexcept { return m_data.asUInt; }
    [[nodiscard]] bool&        asBoolean()     noexcept { return m_data.asBoolean; }
    [[nodiscard]] ObjectType&  asObject()      noexcept { return *m_data.asObject; }
    [[nodiscard]] ArrayType&   asArray()       noexcept { return *m_data.asArray; }

    [[deprecated("This will be removed; use asDouble() for the old functionality.")]]
    [[nodiscard]] F64&         asNumber()  noexcept { return asDouble(); }

    [[nodiscard]] const std::string& asString()      const noexcept { return *m_data.asString; }
    [[nodiscard]] const F64&         asDouble()      const noexcept { return m_data.asDouble; }
    [[nodiscard]] const S64&         asSignedInt()   const noexcept { return m_data.asSInt; }
    [[nodiscard]] const U64&         asUnsignedInt() const noexcept { return m_data.asUInt; }
    [[nodiscard]] const bool&        asBoolean()     const noexcept { return m_data.asBoolean; }
    [[nodiscard]] const ObjectType&  asObject()      const noexcept { return *m_data.asObject; }
    [[nodiscard]] const ArrayType&   asArray()       const noexcept { return *m_data.asArray; }

    [[deprecated("This will be removed; use asDouble() for the old functionality.")]]
    [[nodiscard]] const F64&         asNumber()  const noexcept { return asDouble(); }

    JSONValue(const JSONValue& other)
    {
        m_type = JSONType::Null; // Avoid destroying this.
        copy(other);
    }
    JSONValue(JSONValue&& other)
    {
        m_type = JSONType::Null; // Avoid destroying this.
        (*this) = std::move(other);
    }

    JSONValue& operator=(const JSONValue& other)
    {
        destroy();
        copy(other);
        return *this;
    }
    JSONValue& operator=(JSONValue&& other)
    {
        destroy();
        m_type = other.m_type;
        std::memcpy(&m_data, &other.m_data, sizeof(m_data));
        other.m_data.asObject = nullptr;
        other.m_type = JSONType::Null;
        return *this;
    }
    JSONValue& operator=(const std::string& str)
    {
        destroy();
        setToString(str);
        return *this;
    }
    JSONValue& operator=(const char* str)
    {
        destroy();
        if (str) setToString(str);
        else m_type = JSONType::Null;
        return *this;
    }
    template <typename Number>
    JSONValue& operator=(Number val) requires (std::is_floating_point_v<Number>)
    {
        destroy();
        setToDouble(val);
        return *this;
    }

    template <typename Number>
    JSONValue& operator=(Number val) requires (std::is_integral_v<Number>
                                               && std::is_signed_v<Number>)
    {
        destroy();
        setToSignedInt(val);
        return *this;
    }
    template <typename Number>
    JSONValue& operator=(Number val) requires (std::is_integral_v<Number>
                                               && !std::is_signed_v<Number>)
    {
        destroy();
        setToUnsignedInt(val);
        return *this;
    }
    JSONValue& operator=(bool b)
    {
        destroy();
        setToBool(b);
        return *this;
    }
    JSONValue& operator=(const ObjectType& obj)
    {
        destroy();
        setToObject(obj);
        return *this;
    }
    JSONValue& operator=(const ArrayType& arr)
    {
        destroy();
        setToArray(arr);
        return *this;
    }

    [[nodiscard]] JSONType type() const { return m_type; }

    [[nodiscard]] bool operator==(const JSONValue& other) const;

    [[nodiscard]] bool operator!=(const JSONValue& other) const
    {
        return !(*this == other);
    }

    [[nodiscard]] explicit operator bool() const
    {
        if (m_type == JSONType::Boolean)
        {
            return asBoolean();
        }
        else if (m_type == JSONType::Null)
        {
            return false;
        }
        throw std::runtime_error("Cannot convert non-bool to bool.");
    }

    // Prevent this from clashing with operator Number() by ensuring String is
    // not an arithmetic type. Then, we can call this if const char* can
    // directly be converted to String.
    template <typename String>
    explicit operator String() const requires (!std::is_arithmetic_v<String>
                                               && std::is_constructible_v<String, const char*>)
    {
        switch (m_type)
        {
        case JSONType::Null: return String("null");
        case JSONType::Boolean: return asBoolean() ? String("true") : String("false");
        case JSONType::Double:
        case JSONType::SignedInt:
        case JSONType::UnsignedInt:
            return stringifyNumber(); // Technically incorrect as we get a string
                                      // and not const char*...
        case JSONType::String: return asString();
        case JSONType::Array: case JSONType::Object: default:
            throw std::runtime_error("Cannot convert array/object to string implicitly.");
        }
    }

    template <typename Number>
    [[nodiscard]] explicit operator Number() const requires std::is_arithmetic_v<Number>
    {
        if (auto result = tryConvertTo<Number>())
        {
            return *result;
        }
        throw std::runtime_error("Element cannot be converted to number.");
    }

    void clear()
    {
        destroy();
    }

    [[nodiscard]] JSONValue& operator[](const std::string& str)
    {
        if (m_type == JSONType::Object)
        {
            return asObject()[str];
        }
        if (m_type == JSONType::Null)
        {
            setToObject({});
            return (*this)[str];
        }
        throw std::domain_error("Cannot index non-object by string.");
    }

    [[nodiscard]] JSONValue& operator[](std::size_t idx)
    {
        if (m_type == JSONType::Array)
        {

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
            // Looks horribly inefficient as compared to asArray.resize(idx+1),
            // but using push_back() gives the same asymptotic complexity, and,
            // futhermore, this also makes sure that if this is called
            // repeatedly with increasing indices, the runtime is linear. If we
            // used resize, it might reallocate the array every time, giving
            // quadratic runtime instead.
            while (idx >= asArray().size())
            {
                asArray().emplace_back();
            }
            return asArray()[idx];
#pragma GCC diagnostic pop

        }
        if (m_type == JSONType::Null)
        {
            setToArray({});
            return (*this)[idx];
        }
        throw std::domain_error("Cannot index non-array by integer.");
    }

    // This gives read-only indexing which never fails (but returns null
    // instead), which enables one to write expressions like
    // json("some")("nested")("expression")
    // which then returns null if one of these does not exist or if it exists
    // but is null.
    [[nodiscard]] const JSONValue& operator()(const std::string& str) const
    {
        if (m_type == JSONType::Object)
        {
            const auto& obj = asObject();
            auto it = obj.find(str);
            if (it != obj.end()) return it->second;
        }
        return nullvalue();
    }

    [[nodiscard]] const JSONValue& operator()(std::size_t idx) const
    {
        if (m_type == JSONType::Array)
        {
            if (idx < asArray().size())
            {
                return asArray()[idx];
            }
        }
        return nullvalue();
    }

    void push_back(const JSONValue& obj)
    {
        if (m_type == JSONType::Array)
        {
            asArray().push_back(obj);
        }
        else if (m_type == JSONType::Null)
        {
            setToArray({});
            asArray().push_back(obj);
        }
        else
        {
            throw std::runtime_error("Cannot push back on non-null non-array.");
        }
    }

    void setType(JSONType newType)
    {
        clear();
        switch (newType)
        {
        case JSONType::String:
            m_data.asString = new std::string;
            break;
        case JSONType::Array:
            m_data.asArray = new std::vector<JSONValue>;
            break;
        case JSONType::Object:
            m_data.asObject = new std::map<std::string, JSONValue>;
            break;
        case JSONType::Null: case JSONType::Boolean: case JSONType::Double:
        case JSONType::SignedInt: case JSONType::UnsignedInt:
        default:
            ;
        }
        m_type = newType;
    }

    [[nodiscard]] std::size_t size() const noexcept
    {
        switch (m_type)
        {
        default:
        case JSONType::Null: case JSONType::Boolean: case JSONType::Double:
        case JSONType::SignedInt: case JSONType::UnsignedInt:
            return 0; // There is nothing to index, so 0 is the only reasonable
                      // value.
        case JSONType::String: return asString().size();
        case JSONType::Array:  return asArray().size();
        case JSONType::Object: return asObject().size();
        }
    }

    [[nodiscard]] std::string serialise(bool compact = false) const;

    [[nodiscard]] static JSONValue parse(std::string_view view);

private:
    void setToString(std::string str)
    {
        m_data.asString = new std::string(std::move(str));
        m_type = JSONType::String;
    }
    void setToDouble(F64 val) noexcept
    {
        m_data.asDouble = val;
        m_type = JSONType::Double;
    }

    void setToSignedInt(S64 val) noexcept
    {
        m_data.asSInt = val;
        m_type = JSONType::SignedInt;
    }

    void setToUnsignedInt(U64 val) noexcept
    {
        m_data.asUInt = val;
        m_type = JSONType::UnsignedInt;
    }

    void setToBool(bool b) noexcept
    {
        m_data.asBoolean = b;
        m_type = JSONType::Boolean;
    }

    void setToObject(ObjectType obj)
    {
        m_data.asObject = new ObjectType(std::move(obj));
        m_type = JSONType::Object;
    }

    void setToArray(ArrayType arr)
    {
        m_data.asArray = new ArrayType(std::move(arr));
        m_type = JSONType::Array;
    }

    void copy(const JSONValue& other)
    {
        destroy();
        m_type = other.m_type;
        switch (m_type)
        {
        case JSONType::String:
            setToString(other.asString());
            break;
        case JSONType::Double:
            setToDouble(other.asDouble());
            break;
        case JSONType::SignedInt:
            setToSignedInt(other.asSignedInt());
            break;
        case JSONType::UnsignedInt:
            setToUnsignedInt(other.asUnsignedInt());
            break;
        case JSONType::Boolean:
            setToBool(other.asBoolean());
            break;
        case JSONType::Object:
            setToObject(other.asObject());
            break;
        case JSONType::Array:
            setToArray(other.asArray());
            break;
        case JSONType::Null: default:
            ;
        }
    }
    void destroy() noexcept
    {
        switch (m_type)
        {
        case JSONType::String:
            delete m_data.asString;
            break;
        case JSONType::Object:
            delete m_data.asObject;
            break;
        case JSONType::Array:
            delete m_data.asArray;
            break;
        case JSONType::Number:
        case JSONType::Boolean:
        case JSONType::Null:
        default:
            ;
        }
        m_type = JSONType::Null;
    }

    template <typename Numeric>
    std::optional<Numeric> tryConvertTo() const
    {
        switch (m_type)
        {
        case JSONType::Null: return 0.;
        case JSONType::Boolean: return +asBoolean();
        case JSONType::Double: return asDouble();
        case JSONType::SignedInt: return asSignedInt();
        case JSONType::UnsignedInt: return asUnsignedInt();
        case JSONType::String: return detail::tryConvertTo<Numeric>(asString());
        case JSONType::Array:
        case JSONType::Object:
        default:
            return std::nullopt;
        }
        return std::nullopt;
    }

    [[nodiscard]] std::string stringifyNumber() const; // Assumes this is numeric

    [[nodiscard]] static const JSONValue& nullvalue()
    {
        static thread_local JSONValue val;
        return val;
    }

    union
    {
        std::string* asString;
        F64 asDouble;
        S64 asSInt;
        U64 asUInt;
        bool asBoolean;
        ObjectType* asObject;
        ArrayType* asArray;
    } m_data;
    JSONType m_type;
};

std::ostream& prettyPrint(std::ostream& ost, const JSONValue& obj);
std::ostream& compactPrint(std::ostream& ost, const JSONValue& obj);

std::ostream& operator<<(std::ostream& ost, const JSONValue& obj);

[[nodiscard]] JSONValue parseJSON(std::istream& input);
std::istream& operator>>(std::istream& input, JSONValue& object);

const U8* parseJSON(const U8* begin, const U8* end, JSONValue& jsonOut);
[[nodiscard]] JSONValue parseJSON(std::string_view);

template <typename T>
std::enable_if<sizeof(T) == 1, const T*>
parseJSON(const T* begin, const T* end, JSONValue& jsonOut)
{
    auto ptr = parseJSON(reinterpret_cast<const U8*>(begin),
                         reinterpret_cast<const U8*>(end),
                         jsonOut);
    return reinterpret_cast<const T*>(ptr);
}

} // namespace kraken

#endif // KRAKEN_UTILITY_JSON_HPP_INCLUDED
