#ifndef KRAKEN_UTILITY_MEMORY_HPP_INCLUDED
#define KRAKEN_UTILITY_MEMORY_HPP_INCLUDED

#include <kraken/types.hpp>

#include <cstddef>
#include <cstring>
#include <functional>
#include <memory>
#include <type_traits>

namespace kraken
{

/// Advances the given pointer by the number of bytes.
///
/// This is useful for two scenarios: One is to advance a void pointer by a
/// certain amount of bytes without having to cast to some byte type and back;
/// the other is to advance a non-void pointer by a specific number of bytes.
/// The latter may seem erroneous, but in case one has an array of untyped data
/// (say, a binary file in memory), where one wishes to access the next element
/// a number of bytes away which is not a multiple of the element size (say, if
/// one has an array of (F64, S32)-pairs and want to access the next double/F64)
/// then this can be used for that purpose.
///
/// \remark Read the description carefully; this makes it very easy to invoke
///         undefined behaviour, read past array bounds, etc.
/// \param curr The current pointer.
/// \param bytes The number of bytes to advance the pointer.
/// \returns A pointer of same type as 'curr' pointing to an address exactly
///          'bytes' bytes farther.
template <typename Type>
Type* pointeradd(Type* curr, std::size_t bytes) noexcept
{
    using ByteType = std::conditional_t<std::is_const_v<Type>,
                                        const U8*,
                                        U8*>;
    return reinterpret_cast<Type*>(reinterpret_cast<ByteType>(curr)+bytes);
}

template <typename Type>
std::size_t byteDistance(const Type* first, const Type* second)
{
    return static_cast<std::size_t>(static_cast<const U8*>(second)
                                    -static_cast<const U8*>(first));
}

/// Removes elements of an untyped array by overwriting with other elements.
///
/// This works by taking the element indices given by *removeBegin,
/// *(removeBegin+1), ..., *(removeEnd-1), and overwriting these by elements
/// from the array. Note that the indices must be in ascending order, and after
/// this operation there will be free space at the end of the array.
/// As an example, if the array is e.g. 'ABCDEFGHIJ', and the indices are 0, 1,
/// 5, 7; then the resulting array becomes 'CDEGIJ____', where the underscores
/// indicate undefined content.
///
/// \remark This function obviously requires standard-layout entries which are
///         trivially constructible, copyable and destructible. If this is not
///         the case, bad things will happen.
///
/// \param removeBegin Iterator to the first index to remove.
/// \param removeEnd End iterator for indices to remove.
/// \param entryCount Number of elements in the array.
/// \param pEntries Pointer to the start of the array.
/// \param entrySize Size (in bytes) of each array element.
template <typename It, std::size_t bufSize = 0>
void removeUntypedArrayElements(It removeBegin,
                                It removeEnd,
                                std::size_t entryCount,
                                void* pEntries,
                                std::size_t entrySize)
{
    if (removeBegin == removeEnd) return;
    std::size_t nextRemove = *removeBegin++;
    std::size_t dest = 0;
    for (std::size_t i = 0; i < entryCount; ++i)
    {
        if (i != nextRemove)
        {
            if (i != dest)
            {
                std::memcpy(pointeradd(pEntries, entrySize*dest),
                            pointeradd(pEntries, entrySize*i),
                            entrySize);
            }
            ++dest;
        }
        else if (removeBegin != removeEnd)
        {
            nextRemove = *removeBegin++;
        }
    }
}

template <typename T, typename... Ts>
typename std::enable_if<!std::is_array<T>::value, std::unique_ptr<T>>::type
make_unique(Ts&&... args)
{
    return std::unique_ptr<T>(new T(std::forward<Ts>(args)...));
}

template <typename T>
typename std::enable_if<std::is_array<T>::value, std::unique_ptr<T>>::type
make_unique(std::size_t len)
{
    return std::unique_ptr<T>(new typename std::remove_extent<T>::type[len]);
}

void* allocateUntyped(std::size_t bytes,
                      std::size_t alignment = alignof(max_align_t));

void deleteUntyped(void* pointer);

template <typename Deriv, typename Base>
Deriv* downcast(Base ptr)
{
    return static_cast<Deriv*>(static_cast<void*>(ptr));
}

template <typename Deriv, typename Base, typename Deleter>
std::unique_ptr<Deriv, Deleter> downcast(std::unique_ptr<Base, Deleter>&& ptr)
{
    auto p = static_cast<Deriv*>(ptr.release());
    return std::unique_ptr<Deriv, Deleter>(p, std::move(ptr.get_deleter()));
}

inline bool isBigEndian()
{
    union
    {
        U32 val;
        U8 bytes[4];
    } tester;
    tester.val = 0xac000000;
    return tester.bytes[0] == 0xac;
}

namespace priv
{

inline U16 swapEndian16(U16 v)
{
    return static_cast<U16>((v >> 8) | (v << 8));
}

inline U32 swapEndian32(U32 v)
{
    auto lower = static_cast<U16>(v&0xfffful);
    auto upper = static_cast<U16>(v>>16);
    return (static_cast<U32>(swapEndian16(lower)) << 16)
           | static_cast<U32>(swapEndian16(upper));
}

inline U64 swapEndian64(U64 v)
{
    auto lower = static_cast<U32>(v&0xffffffffull);
    auto upper = static_cast<U32>(v>>32);
    return (static_cast<U64>(swapEndian32(lower)) << 32)
           | static_cast<U64>(swapEndian32(upper));
}

template <typename T, std::size_t bytes>
struct EndianSwapper
{
    static T swap(T);
};

template <typename T>
struct EndianSwapper<T, 1>
{
    static T swap(T v)
    {
        return v;
    }
};

template <typename T>
struct EndianSwapper<T, 2>
{
    static T swap(T v)
    {
        union
        {
            T orig;
            U16 swapper;
        } swapper;
        swapper.orig = v;
        swapper.swapper = swapEndian16(swapper.swapper);
        return swapper.orig;
    }
};

template <typename T>
struct EndianSwapper<T, 4>
{
    static T swap(T v)
    {
        union
        {
            T orig;
            U32 swapper;
        } swapper;
        swapper.orig = v;
        swapper.swapper = swapEndian32(swapper.swapper);
        return swapper.orig;
    }
};

template <typename T>
struct EndianSwapper<T, 8>
{
    static T swap(T v)
    {
        union
        {
            T orig;
            U64 swapper;
        } swapper;
        swapper.orig = v;
        swapper.swapper = swapEndian64(swapper.swapper);
        return swapper.orig;
    }
};

} // namespace priv

template <typename T>
T swapEndian(T val)
{
    return priv::EndianSwapper<T, sizeof(T)>::swap(val);
}

// Swaps endian in-place
template <typename T>
void swapEndianIP(T& val)
{
    val = swapEndian(val);
}

// Parses a given type from a sequence of bytes; this will read exactly
// sizeof(T) bytes and advance begin by that amount.
template <typename T,
          typename = typename std::enable_if_t<std::is_integral_v<T>>>
T readLittleEndian(const U8*& begin) noexcept
{
    T t{0};
    T shift{0};
    for (size_t i = 0; i < sizeof(T); ++i)
    {
        t |= *begin++ << shift;
        shift += 8;
    }
    return t;
}

template <typename T,
          typename = typename std::enable_if_t<std::is_integral_v<T>>>
T readBigEndian(const U8*& begin) noexcept
{
    T t{0};
    for (std::size_t i = 0; i < sizeof(T); ++i)
    {
        t = (t << 8) | *begin++;
    }
    return t;
}

class ExternalMemoryAllocation
{
public:
    /// This describes a function for allocating memory. The first input
    /// argument describes the number of bytes it should allocate, while the
    /// second argument describes the required alignment of the initial pointer.
    ///
    /// The function must return a pointer to the memory with the given
    /// alignment on success, and nullptr on failure.
    using MemoryAllocationFunction = std::function<void*(std::size_t,
                                                         std::size_t)>;

    ExternalMemoryAllocation() noexcept;
    ExternalMemoryAllocation(MemoryAllocationFunction func, bool isStableOnError = false) noexcept;
    ExternalMemoryAllocation(const ExternalMemoryAllocation&) = delete;
    ExternalMemoryAllocation(ExternalMemoryAllocation&&) noexcept;
    ~ExternalMemoryAllocation() = default;

    ExternalMemoryAllocation& operator=(const ExternalMemoryAllocation&) noexcept = delete;
    ExternalMemoryAllocation& operator=(ExternalMemoryAllocation&&) noexcept;

    operator MemoryAllocationFunction();

    /// Sets the callback function for allocating memory.
    ///
    /// \param func The function for allocating memory.
    /// \param isStableOnError Whether the function allocating memory is stable
    ///                        on errors, in the sense that the old data is
    ///                        invalidated only if the memory allocation is
    ///                        successful.
    ///
    /// \remark The function is copied and so the scope of that function is the
    ///         same as this object. Thus, if one passes in a lambda function
    ///         referencing local variables, there is a risk of those becoming
    ///         dangling if this object's lifetime is longer than the referenced
    ///         variables.
    void setAllocator(MemoryAllocationFunction func, bool isStableOnError) noexcept;

    // Primitive access functions.
    /// Yields a pointer to the beginning of the current data.
    [[nodiscard]] void* data() const noexcept { return m_data; }

    [[nodiscard]] std::size_t capacity() const noexcept { return m_capacity; }
    [[nodiscard]] std::size_t size() const noexcept { return m_size; }

    // Utility functions for accessing data at specific positions / current.
    [[nodiscard]] void* data(std::size_t offset) const noexcept
    {
        return pointeradd(m_data, offset);
    }
    [[nodiscard]] void* currDataPos() const noexcept { return data(size()); }
    [[nodiscard]] std::size_t unusedCapacity() const noexcept { return capacity() - size();}

    // Marks data as being used.
    void addUsed(std::size_t extraBytes) noexcept
    {
        m_size += extraBytes;
    }

    /// Range-checked memcpy.
    ///
    /// This copies the given data and adds the number of copied bytes to the
    /// stored size.
    ///
    /// \remark The range check is solely on the destination, and nothing is
    ///         performed on the source.
    ///
    /// \param source Pointer to where to copy from.
    /// \param bytes Number of bytes to copy.
    ///
    /// \returns Whether the copy was successful, or if the range check was
    ///          triggered.
    bool memcpy(const void* source, std::size_t bytes) noexcept
    {
        if (bytes > unusedCapacity()) return false;
        std::memcpy(currDataPos(), source, bytes);
        addUsed(bytes);
        return true;
    }

    /// Auto-resizing memcpy.
    bool memcpy_back(const void* source, std::size_t bytes, std::size_t alignment) noexcept
    {
        if (bytes > unusedCapacity()
            && !reserve(size() + bytes, alignment))
        {
            return false;
        }
        std::memcpy(currDataPos(), source, bytes);
        addUsed(bytes);
        return true;
    }

    /// Requests some memory to be reserved.
    ///
    /// As opposed to standard-library functionality, this can specifically
    /// increase the user-facing capacity to more than requested; this is done
    /// to allow various functions to just call reserve without having to
    ///
    /// \param desiredCapacity The minimum capacity to reserve, in bytes.
    /// \param alignment Required alignment of the initial pointer, in bytes.
    ///
    /// \returns Whether the allocation was successful.
    bool reserve(std::size_t desiredCapacity, std::size_t alignment) noexcept;
    bool reserveExact(std::size_t bytes, std::size_t alignment) noexcept;

private:
    void clear() noexcept;

    MemoryAllocationFunction m_memalloc;
    void* m_data;
    std::size_t m_capacity;
    std::size_t m_size;
    bool m_isStableOnError;
};

} // namespace kraken

#endif // KRAKEN_UTILITY_MEMORY_HPP_INCLUDED

