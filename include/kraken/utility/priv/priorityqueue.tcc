#ifndef KRAKEN_UTILITY_PRIV_PRIORITYQUEUE_TCC_INCLUDED
#define KRAKEN_UTILITY_PRIV_PRIORITYQUEUE_TCC_INCLUDED

#include <kraken/types.hpp>
#include <functional> // For std::less
#include <stdexcept>
#include <vector>

namespace kraken
{

template <class Elem, class Compare>
void PriorityQueue<Elem, Compare>::pop()
{
    std::swap(m_elems[0], m_elems[m_elems.size()-1]);
    m_elems.pop_back();
    heapify(0);
}

template <class Elem, class Compare>
void PriorityQueue<Elem, Compare>::heapify(Index i)
{
    auto l = left(i);
    auto r = l | 1;
    auto smallestIdx = i;
    if (l < m_elems.size() && m_comparator(m_elems[l], m_elems[i]))
    {
        smallestIdx = l;
    }
    if (r < m_elems.size() && m_comparator(m_elems[r], m_elems[smallestIdx]))
    {
        smallestIdx = r;
    }
    if (i != smallestIdx)
    {
        std::swap(m_elems[i], m_elems[smallestIdx]);
        heapify(smallestIdx);
    }
}

template <class Elem, class Compare>
void PriorityQueue<Elem, Compare>::fixupInsertion()
{
    Index i = m_elems.size() - 1;
    while (i && m_comparator(m_elems[i], m_elems[parent(i)]))
    {
        std::swap(m_elems[i], m_elems[parent(i)]);
        i = parent(i);
    }
}

} // namespace kraken

#endif // KRAKEN_UTILITY_PRIV_PRIORITYQUEUE_TCC_INCLUDED
