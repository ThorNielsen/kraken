#ifndef KRAKEN_UTILITY_PRIV_REDBLACKTREE_TCC_INCLUDED
#define KRAKEN_UTILITY_PRIV_REDBLACKTREE_TCC_INCLUDED

#include <kraken/types.hpp>
#include <functional> // For std::less
#include <stdexcept>
#include <vector>

namespace kraken
{

template <class Key, class Compare>
void RedBlackTree<Key, Compare>::setParent(Node* node, NodeID parent)
{
    if (!id(node))
    {
        node = get(0);
    }
    node->parent = parent | (node->parent & 0x80000000);
}

template <class Key, class Compare>
typename RedBlackTree<Key, Compare>::NodeID RedBlackTree<Key, Compare>::id(Node* node)
{
    if (!node) return invalidID;
    return NodeID(node-get(0));
}

template <class Key, class Compare>
typename RedBlackTree<Key, Compare>::NodeID
RedBlackTree<Key, Compare>::insert(const Key& elem)
{
    if (m_nodes.empty())
    {
        m_nodes.push_back({elem, invalidID, invalidID, invalidID});
        setColour(get(0), 1);
        return 0;
    }
    m_nodes.push_back({elem, invalidID, invalidID, invalidID});
    NodeID newID = m_nodes.size()-1;
    Node* prev = nullptr;
    Node* curr = get(0);

    Compare comparator{};

    while (curr)
    {
        prev = curr;
        if (comparator(elem, (const Key&)curr->data)) curr = get(curr->left);
        else curr = get(curr->right);
    }
    Node* inserted = get(newID);
    setParent(inserted, id(prev));
    setColour(inserted, 0);
    if (comparator(elem, (const Key&)prev->data)) prev->left = newID;
    else prev->right = newID;
    return fixupInsertion(newID);
}

template <class Key, class Compare>
typename RedBlackTree<Key, Compare>::NodeID
RedBlackTree<Key, Compare>::fixupInsertion(NodeID inserted)
{
    // Begin: Only possible red-black violation is that the inserted node may
    // be red while its parent is also red.

    // "curr" holds the node we are currently processing and "parent" is its
    // parent (if any). If the original node changes its ID (it becomes the
    // root), "inserted" is updated to reflect this.
    Node* curr = get(inserted);
    Node* parent = nullptr;

    while ((parent = get(parentID(curr))) && !isBlack(parent))
    {
        Node* grandparent = get(parentID(parent));
        if (!grandparent)
        {
            // "parent" is the root. Handle it! Do SOMETHING!
            throw grandparent;
        }
        if (id(parent) == grandparent->left)
        {
            Node* uncle = get(grandparent->right);
            if (uncle && !isBlack(uncle))
            {
                setColour(parent, 1);
                setColour(uncle, 1);
                setColour(grandparent, 0);
                curr = grandparent;
            }
            else
            {
                if (id(curr) == parent->right)
                {
                    auto currID = id(curr);
                    auto parID = id(parent);
                    auto newTopID = rotateLeft(parID);
                    if (currID == inserted) inserted = newTopID;
                    curr = get(get(newTopID)->left);
                    parent = get(parentID(curr));
                }
                setColour(parent, 1);
                setColour(grandparent, 0);
                // rotateRight cannot change the ID of the node we have inserted
                rotateRight(id(grandparent));
            }
        }
        else
        {
            // Parent is the right child of grandparent.
            Node* uncle = get(grandparent->left);
            if (uncle && !isBlack(uncle))
            {
                setColour(parent, 1);
                setColour(uncle, 1);
                setColour(grandparent, 0);
                curr = grandparent;
            }
            else
            {
                if (id(curr) == parent->left)
                {
                    auto currID = id(curr);
                    auto parID = id(parent);
                    auto newTopID = rotateRight(parID);
                    if (currID == inserted) inserted = newTopID;
                    curr = get(get(newTopID)->right);
                    parent = get(parentID(curr));
                }
                setColour(parent, 1);
                setColour(grandparent, 0);
                // rotateLeft cannot change the ID of the node we have inserted
                rotateLeft(id(grandparent));
            }
        }
    }
    setColour(get(0), 1);
    return inserted;
}

template <class Key, class Compare>
void RedBlackTree<Key, Compare>::fixupDeletion(NodeID toFixup, NodeID fixupParent,
                                             bool isRight)
{
    NodeID currID = toFixup;
    Node* curr = get(currID);
    NodeID parID = fixupParent;
    Node* parent = get(parID);
    // We need either curr to be a leaf node or if it is a proper one then it
    // should be black.
    while (currID && (!curr || isBlack(curr)))
    {
        if (!parent)
        {
            throw std::logic_error("Non-root element MUST have a parent.");
        }
        if (!isRight)//(currID == parent->left)
        {
            NodeID siblingID = parent->right;
            Node* sibling = get(siblingID);
            if (!sibling) throw std::logic_error("Malformed tree.");
            if (!isBlack(sibling))
            {
                setColour(sibling, 1);
                setColour(parent, 0);
                parID = get(rotateLeft(parID))->left;
                parent = get(parID);
                currID = parent->left;
                curr = get(currID);
                isRight = false;
                siblingID = parent->right;
                sibling = get(siblingID);
            }
            bool sibleftblack = !get(sibling->left) || isBlack(get(sibling->left));
            bool sibrightblack = !get(sibling->right) || isBlack(get(sibling->right));
            if (sibleftblack && sibrightblack)
            {
                setColour(sibling, 0);
                currID = parID;
                curr = parent;
                parID = parentID(curr);
                parent = get(parID);
            }
            else
            {
                if (sibrightblack)
                {
                    if (!get(sibling->left))
                    {
                        throw std::logic_error("And here you have it!... AN IMPOSSIBLE SITUATION!");
                    }
                    setColour(get(sibling->left), 1);
                    setColour(sibling, 0);
                    siblingID = rotateRight(siblingID);
                    sibling = get(siblingID);
                }
                if (!get(sibling->right))
                {
                    throw std::runtime_error("Cthulhu has corrupted the tree.");
                }
                setColour(sibling, isBlack(parent));
                setColour(parent, 1);
                setColour(get(sibling->right), 1);
                rotateLeft(parID);
                currID = 0;
                curr = get(currID);
            }
        }
        else
        {
            NodeID siblingID = parent->left;
            Node* sibling = get(siblingID);
            if (!sibling) throw std::logic_error("Malformed tree.");
            if (!isBlack(sibling))
            {
                setColour(sibling, 1);
                setColour(parent, 0);
                parID = get(rotateRight(parID))->right;
                parent = get(parID);
                currID = parent->right;
                curr = get(currID);
                isRight = false;
                siblingID = parent->left;
                sibling = get(siblingID);
            }
            bool sibrightblack = !get(sibling->right) || isBlack(get(sibling->right));
            bool sibleftblack = !get(sibling->left) || isBlack(get(sibling->left));
            if (sibrightblack && sibleftblack)
            {
                setColour(sibling, 0);
                currID = parID;
                curr = parent;
                parID = parentID(curr);
                parent = get(parID);
            }
            else
            {
                if (sibleftblack)
                {
                    if (!get(sibling->right))
                    {
                        throw std::logic_error("And here you have it!... AN IMPOSSIBLE SITUATION!");
                    }
                    setColour(get(sibling->right), 1);
                    setColour(sibling, 0);
                    siblingID = rotateLeft(siblingID);
                    sibling = get(siblingID);
                }
                if (!get(sibling->left))
                {
                    throw std::runtime_error("Cthulhu has corrupted the tree.");
                }
                setColour(sibling, isBlack(parent));
                setColour(parent, 1);
                setColour(get(sibling->left), 1);
                rotateRight(parID);
                currID = 0;
                curr = get(currID);
            }
        }

        if (parent && parent->left == currID) isRight = false;
        if (parent && parent->right == currID) isRight = true;
    }
    setColour(curr, 1);
    //throw std::logic_error("Deletion fixup is not implemented... YET!");
}

template <class Key, class Compare>
typename RedBlackTree<Key, Compare>::NodeID
RedBlackTree<Key, Compare>::find(const Key& elem)
{
    Node* curr = get(0);
    while (curr && curr->data != elem)
    {
        if (Compare(elem, curr->data)) curr = curr->left;
        else curr = curr->right;
    }
    if (!curr) return invalidID;
    return id(curr);
}

template <class Key, class Compare>
void RedBlackTree<Key, Compare>::removeNode(NodeID del, NodeID& preserve1, NodeID& preserve2)
{
    if (empty()) return;
    NodeID last = m_nodes.size() - 1;
    if (last == preserve1) preserve1 = del;
    if (last == preserve2) preserve2 = del;
    memswap(last, del);

    Node* parent = get(parentID(get(last)));
    if (parent && parent->left == last) parent->left = invalidID;
    if (parent && parent->right == last) parent->right = invalidID;
    m_nodes.pop_back();
}

template <class Key, class Compare>
void RedBlackTree<Key, Compare>::erase(NodeID node)
{
    Node* curr = get(node);
    if (!curr) return;

    // Handle the extremely simple case of deleting just one element.
    if (node == 0 && size() == 1)
    {
        m_nodes.pop_back();
        return;
    }

    Node* lchild = get(curr->left);
    Node* rchild = get(curr->right);

    bool fixupBlack = isBlack(curr);

    NodeID toFixup = invalidID;
    NodeID fixupParent = invalidID;
    NodeID origReplacedWith = invalidID;
    bool rightFixup = false;

    if (!lchild)
    {
        Node* par = get(parentID(curr));
        if (par && par->left == node) rightFixup = false;
        if (par && par->right == node) rightFixup = true;
        toFixup = curr->right;
        fixupParent = parentID(curr);
        transplant(curr->right, node);
        origReplacedWith = curr->right;
    }
    else if (!rchild)
    {
        Node* par = get(parentID(curr));
        if (par && par->left == node) rightFixup = false;
        if (par && par->right == node) rightFixup = true;
        toFixup = curr->left;
        fixupParent = parentID(curr);
        transplant(curr->left, node);
        origReplacedWith = curr->left;
    }
    else
    {
        // Find successor
        NodeID succID = successor(node);
        Node* succ = get(succID);
        // We want to move successor directly into the position where the
        // current node is, but we will set successor's colour to whatever the
        // current is, such that the node inserted at the erased position will
        // not violate any red-black properties, instead violations are only
        // possible in one spot, namely where we removed successor from.
        fixupBlack = isBlack(succ);
        rightFixup = true;

        toFixup = succ->right;
        fixupParent = parentID(succ);

        if (succID == curr->right)
        {
            fixupParent = succID;
        }
        else
        {
            transplant(succ->right, succID);
            succ->right = curr->right;
            if (get(succ->right)) setParent(get(succ->right), succID);
            rightFixup = false;
        }
        transplant(succID, node);
        origReplacedWith = succID;
        succ->left = curr->left;
        setParent(get(succ->left), succID);
        setColour(succ, isBlack(curr));
    }

    // Now the node we wish to delete should be completely detached from every-
    // thing. To ensure that nothing happens when using memswap, we must remove
    // all its references to other nodes.
    curr->left = invalidID;
    curr->right = invalidID;
    setParent(curr, invalidID);
    NodeID toDelete = node;
    if (node == 0)
    {
        memswap(node, origReplacedWith);
        toDelete = origReplacedWith;
        if (toFixup == node) toFixup = origReplacedWith;
        else if (toFixup == origReplacedWith) toFixup = node;
        if (fixupParent == node) fixupParent = origReplacedWith;
        else if (fixupParent == origReplacedWith) fixupParent = node;
    }
    // Now we perform the actual deletion. Note that as we pass the fixup ids,
    // they will be updated if they change.
    removeNode(toDelete, toFixup, fixupParent);
    if (fixupBlack)
    {
        fixupDeletion(toFixup, fixupParent, rightFixup);
    }
}

template <class Key, class Compare>
void RedBlackTree<Key, Compare>::transplant(NodeID replacement, NodeID old)
{
    Node* rep = get(replacement);
    Node* o = get(old);
    NodeID parID = parentID(o);
    Node* par = get(parID);
    if (rep) rep->parent = parID;
    if (par && par->left == old) par->left = replacement;
    if (par && par->right == old) par->right = replacement;
}

template <class Key, class Compare>
typename RedBlackTree<Key, Compare>::NodeID
RedBlackTree<Key, Compare>::moveReplace(NodeID replacement, NodeID toDestroy)
{
    if (!get(toDestroy))
    {
        throw std::logic_error("Bad internal moveReplace()-call.");
    }
    if (!get(replacement))
    {
        // If there is no replacement, then the node we wish to destroy MAY NOT
        // have any children.
        Node* destroy = get(toDestroy);
        if (get(destroy->left) || get(destroy->right))
        {
            throw std::logic_error("Children must be destroyed before their parents.");
        }
        removeNode(toDestroy);
        return invalidID;
    }

    std::swap(m_nodes[replacement], m_nodes[toDestroy]);
    NodeID replacedID = toDestroy;
    toDestroy = replacement;
    // Now toDestroy holds the new position of the node to be destroyed
    // and replacedID holds the position of where the replacement ended up.
    Node* replaced = get(replacedID);
    Node* left = get(replaced->left);
    Node* right = get(replaced->right);
    if (left) setParent(left, replacedID);
    if (right) setParent(right, replacedID);
    setParent(replaced, parentID(get(toDestroy)));

    Node* destroyMe = get(toDestroy);
    destroyMe->left = invalidID;
    destroyMe->right = invalidID;

    bool wasLast = replacedID == m_nodes.size()-1;

    removeNode(toDestroy);
    return wasLast ? toDestroy : replacement;
}

template <class Key, class Compare>
void RedBlackTree<Key, Compare>::removeNode(NodeID node)
{
    auto last = m_nodes.size() - 1;
    memswap(node, last);
    // The node to delete is now positioned at "last".
    Node* parent = get(parentID(get(last)));
    if (parent && parent->left == last) parent->left = invalidID;
    if (parent && parent->right == last) parent->right = invalidID;

    m_nodes.pop_back();
}

template <class Key, class Compare>
typename RedBlackTree<Key, Compare>::NodeID RedBlackTree<Key, Compare>::minimum()
{
    Node* curr = get(0);
    while (curr && curr->left != invalidID)
    {
        curr = get(curr->left);
    }
    return id(curr);
}

template <class Key, class Compare>
typename RedBlackTree<Key, Compare>::NodeID RedBlackTree<Key, Compare>::maximum()
{
    Node* curr = get(0);
    while (curr && curr->right != invalidID)
    {
        curr = get(curr->right);
    }
    return id(curr);
}

template <class Key, class Compare>
typename RedBlackTree<Key, Compare>::NodeID
RedBlackTree<Key, Compare>::predecessor(NodeID node)
{
    Node* curr = get(node);
    if (!curr) return invalidID;
    if (curr->left != invalidID)
    {
        curr = get(curr->left);
        while (curr && curr->right != invalidID)
        {
            curr = get(curr->right);
        }
        return id(curr);
    }
    Node* ancestor = get(parentID(curr));
    while (ancestor && curr == get(ancestor->left))
    {
        curr = ancestor;
        ancestor = get(parentID(curr));
    }
    return id(ancestor);
}

template <class Key, class Compare>
typename RedBlackTree<Key, Compare>::NodeID
RedBlackTree<Key, Compare>::successor(NodeID node)
{
    Node* curr = get(node);
    if (!curr) return invalidID;
    if (curr->right != invalidID)
    {
        curr = get(curr->right);
        while (curr && curr->left != invalidID)
        {
            curr = get(curr->left);
        }
        return id(curr);
    }
    Node* ancestor = get(parentID(curr));
    while (ancestor && curr == get(ancestor->right))
    {
        curr = ancestor;
        ancestor = get(parentID(curr));
    }
    return id(ancestor);
}

template <class Key, class Compare>
typename RedBlackTree<Key, Compare>::NodeID
RedBlackTree<Key, Compare>::rotateLeft(NodeID currID)
{
    Node* curr = get(currID);
    NodeID rightID = curr->right;
    Node* right = get(rightID);
    Node* root = get(parentID(curr));

    // Parent fixup.
    if (root && root->left == currID) root->left = rightID;
    if (root && root->right == currID) root->right = rightID;
    setParent(right, parentID(curr));
    setParent(curr, rightID);

    // We now just need to move a child node.
    NodeID moveID = right->left;
    Node* move = get(moveID);
    right->left = currID;
    curr->right = moveID;
    if (move) setParent(move, currID);

    // And finally, if we change the root node, we must reposition it.
    if (currID == 0)
    {
        memswap(currID, rightID);
        // Now the top node is the root; we simply return that.
        return 0;
    }
    return rightID;
}

template <class Key, class Compare>
typename RedBlackTree<Key, Compare>::NodeID
RedBlackTree<Key, Compare>::rotateRight(NodeID currID)
{
    Node* curr = get(currID);
    NodeID leftID = curr->left;
    Node* left = get(leftID);
    Node* root = get(parentID(curr)); // Root of this part of the tree.

    // Fixup parents.
    if (root && root->left == currID) root->left = leftID;
    if (root && root->right == currID) root->right = leftID;
    setParent(left, parentID(curr));
    setParent(curr, leftID);

    // Now root points to left, and nodes have their correct parents.
    // We now need to detach left's right child, swap left & curr and then
    // attach the detached child as curr's left child.
    NodeID moveID = left->right;
    Node* move = get(moveID);
    left->right = currID;
    curr->left = moveID;
    if (move) setParent(move, currID);

    // If we change the root node, we MUST ensure that the new root node is at
    // position #0.
    if (currID == 0)
    {
        memswap(currID, leftID);
        // Now the top node is the root; we simply return that.
        return 0;
    }
    return leftID;
}

template <class Key, class Compare>
void RedBlackTree<Key, Compare>::memswap(NodeID a, NodeID b)
{
    if (a == b) return;
    Node* na = get(a);
    Node* nb = get(b);
    Node* aPar = get(parentID(na));
    Node* aLeft = get(na->left);
    Node* aRight = get(na->right);
    Node* bPar = get(parentID(nb));
    Node* bLeft = get(nb->left);
    Node* bRight = get(nb->right);
    if (aPar && aPar->left == a) aPar->left = b;
    if (aPar && aPar->right == a) aPar->right = b;
    if (aLeft && parentID(aLeft) == a) setParent(aLeft, b);
    if (aRight && parentID(aRight) == a) setParent(aRight, b);

    if (bPar && bPar->left == b) bPar->left = a;
    if (bPar && bPar->right == b) bPar->right = a;
    if (bLeft && parentID(bLeft) == b) setParent(bLeft, a);
    if (bRight && parentID(bRight) == b) setParent(bRight, a);

    std::swap(m_nodes[a], m_nodes[b]);
}

#ifdef KRAKEN_UTILITY_REDBLACK_INTERNAL_TESTING
template <class Key, class Compare>
int RedBlackTree<Key, Compare>::doRBCheck(NodeID toTest, bool prevBlack)
{
    Node* test = get(toTest);
    if (!test) return 0;
    bool black = isBlack(test);
    if (!prevBlack && !black) return -1000; // Two red nodes after each other.
    int lHeight = black+doRBCheck(test->left, black);
    int rHeight = black+doRBCheck(test->right, black);
    if (lHeight != rHeight) return -10000;
    return lHeight;
}

template <class Key, class Compare>
void RedBlackTree<Key, Compare>::performConsistencyCheck()
{
    for (size_t i = 0; i < m_nodes.size(); ++i)
    {
        Node* node = &m_nodes[i];
        Node* par = get(parentID(node));
        Node* left = get(node->left);
        Node* right = get(node->right);
        if (left && parentID(left) != i) throw std::logic_error("Bad left child.");
        if (right && parentID(right) != i) throw std::logic_error("Bad right child.");
        if (par && par->left != i && par->right != i) throw std::logic_error("Bad parent.");
        if (m_nodes.size() > 1 && !left && !right && !par) throw std::logic_error("Unconnected node.");
        if (m_nodes.size() == 1 && (left || right || par)) throw std::logic_error("Too well-connected node.");
    }
    if (!checkRBCorrect()) throw std::logic_error("Red-black properties violated.");
}
#endif // KRAKEN_UTILITY_REDBLACK_INTERNAL_TESTING

} // namespace kraken

#endif // KRAKEN_UTILITY_PRIV_REDBLACKTREE_TCC_INCLUDED
