#ifndef KRAKEN_UTILITY_RANGE_HPP_INCLUDED
#define KRAKEN_UTILITY_RANGE_HPP_INCLUDED

namespace kraken
{

namespace detail
{

template <typename T>
struct SimpleIncrementRangeProxy
{
    T minValue;
    T maxValue;
};

template <typename T>
struct RangeIterator
{
    T value;

    T operator*() { return value; }

    RangeIterator<T>& operator++()
    {
        ++value;
        return *this;
    }
};

template <typename T>
struct RangeEndSentinel
{
    T limit;
};

template <typename T>
RangeIterator<T> begin(const SimpleIncrementRangeProxy<T>& proxy)
{
    return {proxy.minValue};
}
template <typename T>
RangeEndSentinel<T> end(const SimpleIncrementRangeProxy<T>& proxy)
{
    return {proxy.maxValue};
}

template <typename T>
bool operator!=(const RangeIterator<T>& iter, const RangeEndSentinel<T>& sentinel)
{
    return iter.value < sentinel.limit;
}

template <typename T>
bool operator!=(const RangeEndSentinel<T>& sentinel, const RangeIterator<T>& iter)
{
    return iter != sentinel;
}

} // namespace detail

template <typename T>
inline detail::SimpleIncrementRangeProxy<T> range(T upTo)
{
    return {T{0}, upTo};
}

template <typename T>
inline detail::SimpleIncrementRangeProxy<T> range(T from, T upTo)
{
    return {from, upTo};
}

} // namespace kraken

#endif // KRAKEN_UTILITY_RANGE_HPP_INCLUDED
