#ifndef KRAKEN_UTILITY_STRING_HPP_INCLUDED
#define KRAKEN_UTILITY_STRING_HPP_INCLUDED

#include <kraken/utility/utf.hpp>
#include <kraken/types.hpp>

#include <iterator>
#include <istream>
#include <locale>
#include <ostream>
#include <string>

namespace kraken
{

extern const std::locale& defaultLocale;

std::locale createLocale(std::string name = "");

inline void setGlobalLocale(const std::locale& loc)
{
    std::locale::global(loc);
}

#ifdef KRAKEN_ENABLE_UNICODE_SUPPORT
enum class ComparisonLevel
{
    BaseLetters,
    BaseAccents,
    BaseCaseAccents,
    Identical,
    IdenticalCodepoints
};
#endif // KRAKEN_ENABLE_UNICODE_SUPPORT

class StringIterator
{
public:
    StringIterator() : m_str{nullptr}, m_pos{} {}
    StringIterator(const StringIterator&) = default;
    StringIterator& operator=(const StringIterator&) = default;

    StringIterator& operator++()
    {
        U32 dummy;
        m_pos = decodeUTF8(m_pos, m_str->end(), dummy);
        return *this;
    }
    StringIterator operator++(int)
    {
        StringIterator cpy(*this);
        operator++();
        return cpy;
    }
    StringIterator& operator--()
    {
        while (((*--m_pos) & 0xc0) == 0x80)
            ;
        return *this;
    }
    StringIterator operator--(int)
    {
        StringIterator cpy(*this);
        operator--();
        return cpy;
    }

    StringIterator& operator+=(const std::ptrdiff_t& val)
    {
        std::advance(*this, val);
        return *this;
    }

    StringIterator& operator-=(const std::ptrdiff_t& val)
    {
        std::advance(*this, -val);
        return *this;
    }

    U32 operator*() const
    {
        U32 codepoint;
        decodeUTF8(m_pos, m_str->end(), codepoint);
        return codepoint;
    }
    bool operator<(const StringIterator& o) const
    {
        return m_pos < o.m_pos;
    }
    bool operator<=(const StringIterator& o) const
    {
        return m_pos <= o.m_pos;
    }
    bool operator>(const StringIterator& o) const
    {
        return m_pos > o.m_pos;
    }
    bool operator>=(const StringIterator& o) const
    {
        return m_pos >= o.m_pos;
    }
    bool operator==(const StringIterator& o) const
    {
        return m_pos == o.m_pos;
    }
    bool operator!=(const StringIterator& o) const
    {
        return m_pos != o.m_pos;
    }
private:
    friend class String;
    friend std::ptrdiff_t byteDistance(StringIterator a, StringIterator b);
    StringIterator(const std::string& s, std::string::const_iterator p)
        : m_str(&s), m_pos{p} {}

    size_t pos()
    {
        return m_pos - m_str->begin();
    }

    const std::string* m_str;
    std::string::const_iterator m_pos;
};

inline StringIterator operator+(StringIterator a, std::ptrdiff_t b)
{
    return a += b;
}

inline StringIterator operator-(StringIterator a, std::ptrdiff_t b)
{
    return a -= b;
}

inline std::ptrdiff_t byteDistance(StringIterator a, StringIterator b)
{
    return std::distance(a.m_pos, b.m_pos);
}

// This has been implemented as a standalone reverse iterator instead of using
// std::reverse_iterator, as the value we get from the iterator is a temporary,
// which limited testing has shown to fail.
class ReverseStringIterator
{
public:
    explicit ReverseStringIterator(StringIterator it) : m_it{it} {}
    ReverseStringIterator(const ReverseStringIterator&) = default;
    ReverseStringIterator& operator=(const ReverseStringIterator&) = default;

    StringIterator base() const
    {
        return m_it;
    }

    StringIterator toIterator() const
    {
        StringIterator it = m_it;
        return --it;
    }

    ReverseStringIterator& operator++()
    {
        --m_it;
        return *this;
    }
    ReverseStringIterator operator++(int)
    {
        ReverseStringIterator cpy(*this);
        operator++();
        return cpy;
    }
    ReverseStringIterator& operator--()
    {
        ++m_it;
        return *this;
    }
    ReverseStringIterator operator--(int)
    {
        ReverseStringIterator cpy(*this);
        operator--();
        return cpy;
    }

    ReverseStringIterator& operator+=(const std::ptrdiff_t& val)
    {
        std::advance(*this, val);
        return *this;
    }

    ReverseStringIterator& operator-=(const std::ptrdiff_t& val)
    {
        std::advance(*this, -val);
        return *this;
    }

    U32 operator*() const
    {
        StringIterator tmp = m_it;
        return *--tmp;
    }
    bool operator<(const ReverseStringIterator& o) const
    {
        return m_it < o.m_it;
    }
    bool operator<=(const ReverseStringIterator& o) const
    {
        return m_it <= o.m_it;
    }
    bool operator>(const ReverseStringIterator& o) const
    {
        return m_it > o.m_it;
    }
    bool operator>=(const ReverseStringIterator& o) const
    {
        return m_it >= o.m_it;
    }
    bool operator==(const ReverseStringIterator& o) const
    {
        return m_it == o.m_it;
    }
    bool operator!=(const ReverseStringIterator& o) const
    {
        return m_it != o.m_it;
    }

private:
    StringIterator m_it;
};

inline ReverseStringIterator operator+(ReverseStringIterator a,std::ptrdiff_t b)
{
    return a += b;
}

inline ReverseStringIterator operator-(ReverseStringIterator a,std::ptrdiff_t b)
{
    return a -= b;
}

inline std::ptrdiff_t byteDistance(ReverseStringIterator a,
                                   ReverseStringIterator b)
{
    return byteDistance(a.base(), b.base());
}

class String
{
public:
    using iterator = StringIterator;
    using const_iterator = iterator;
    using reverse_iterator = ReverseStringIterator;
    using const_reverse_iterator = ReverseStringIterator;
    using value_type = U32;

    // Simple constructors.
    String() = default;
    String(const String&) = default;
    String& operator=(const String&) = default;
    String(String&&) = default;
    String& operator=(String&&) = default;


    // Lots of conversions.
    String(char, const std::locale& = std::locale());
    String(U32);

    String(const char*, const std::locale&);
    String(const std::string&, const std::locale&);

    String(const std::string&);
    String(const char*);
    template <typename T>
    String(const T* data, typename std::enable_if_t<(sizeof(T) > 1), void*> = nullptr)
    {
        decodeUTFStr(data, std::back_inserter(*this));
    }
    template <typename T>
    String(const T* data, typename std::enable_if_t<(sizeof(T) == 1) && !std::is_same_v<T, char>, void*> = nullptr)
        : String(reinterpret_cast<const char*>(data)) {}

    template <typename T>
    String(const std::basic_string<T>& str)
    {
        decodeUTFStr(str.data(), std::back_inserter(*this));
    }

    std::string ansi(const std::locale& = std::locale()) const;
    std::wstring wide() const;

    const std::string& str() const { return m_str; }
    const std::string& utf8() const { return m_str; }
    std::basic_string<U8> toUTF8() const;
    std::basic_string<U16> toUTF16() const;
    std::basic_string<U32> toUTF32() const;

    operator std::string() const;
    operator std::wstring() const;

    const_iterator cbegin() const
    {
        StringIterator it(m_str, m_str.cbegin());
        return it;
    }
    const_iterator cend() const
    {
        StringIterator it(m_str, m_str.cend());
        return it;
    }
    iterator begin() const { return cbegin(); }
    iterator end() const { return cend(); }


    // Iterators.
    const_reverse_iterator crbegin() const
    {
        return const_reverse_iterator(end());
    }

    const_reverse_iterator crend() const
    {
        return const_reverse_iterator(begin());
    }


    reverse_iterator rbegin() const
    {
        return crbegin();
    }
    reverse_iterator rend() const
    {
        return crend();
    }


    // Memory functions.
    bool empty() const { return m_str.empty(); }
    size_t length() const; // Number of code points NOT always equal to size().
                           // May be quite slow, as the number of codepoints
                           // is computed every time.
    size_t size() const { return m_str.size(); } // In bytes!
    size_t capacity() const { return m_str.capacity(); } // In bytes!
    void reserve(size_t); // In bytes!
    void resize(size_t); // In bytes!


    // Direct access to data. Provided to enable writing String to somewhere
    // without copying first.
    const U8* data() const
    {
        return static_cast<const U8*>(static_cast<const void*>(m_str.data()));
    }
    const char* c_str() const
    {
        return m_str.c_str();
    }


    // Not so direct data access.
    U32 front() const { return *begin(); }
    U32 back() const { return *--end(); }


    // Operations.

    String& operator+=(const String&);
    void clear();
    void push_back(U32 codepoint)
    {
        if (!isValidCodepoint(codepoint)) return;
        encodeUTF8(codepoint, std::back_inserter(m_str));
    }
    void push_back(char c, const std::locale& loc)
    {
        push_back(toWideChar(c, loc));
    }
    void pop_back()
    {
        while ((m_str.back() & 0xc0) == 0x80)
            m_str.pop_back();
        m_str.pop_back();
    }

    // Inserts str just BEFORE pos.
    void insert(const String& str, const_iterator pos);

    // Erases every element in the range [first, last)
    void erase(const_iterator first, const_iterator last);
    void erase(const_iterator first)
    {
        erase(first, end());
    }

    // Replaces [first, last) with the contents of str.
    void replace(const_iterator first, const_iterator last, const String& str);

    // Replaces [first, last) with [repf, repl)
    void replace(const_iterator first, const_iterator last,
                 const_iterator repf, const_iterator repl);

    // Returns the substring [first, last).
    String substr(const_iterator first, const_iterator last) const;
    String substr(const_iterator first) const
    {
        return substr(first, end());
    }

    void swap(String& o)
    {
        return m_str.swap(o.m_str);
    }

    // Copies elements [first, last) to dataOut. Note that dataOut must be able
    // to store at least byteDistance(last-first) bytes.
    void copy(U32* dataOut, const_iterator first, const_iterator last);

    // Copies elements [first, last) to dataOut. The capacity of dataOut must
    // be at least byteDistance(last-first).
    void copy(U8* dataOut, const_iterator first, const_iterator last);


    // Search
    const_iterator find(char c, const_iterator first,
                        const std::locale& loc = std::locale()) const
    {
        return find(toWideChar(c, loc), first);
    }
    const_iterator find(char c, const std::locale& loc = std::locale()) const
    {
        return find(c, cbegin(), loc);
    }
    const_iterator find(U32, const_iterator first) const;
    const_iterator find(U32 cp) const
    {
        return find(cp, cbegin());
    }
    const_iterator find(const String&, const_iterator first) const;
    const_iterator find(const String& s) const
    {
        return find(s, cbegin());
    }

#ifdef KRAKEN_ENABLE_UNICODE_SUPPORT
    // Compares this string with another one (like strn?cmp). The comparison
    // accuracy is determined by 'level'.
    int compare(const String&,
                const std::locale& loc = std::locale(),
                ComparisonLevel level = ComparisonLevel::BaseAccents) const;

#endif // KRAKEN_ENABLE_UNICODE_SUPPORT

    // WARNING: This should NOT be used for comparisons, as it only compares
    // whether the codepoints of the two strings are equal (meaning that e.g.
    // e-then-combining-accent is different from e-with-accent), although those
    // would be rendered in the exact same way and aren't different other than
    // their representation.
    // Use compare() instead.
    bool operator==(const String& o) const
    {
        return m_str == o.m_str;
    }
    bool operator!=(const String& o) const
    {
        return m_str != o.m_str;
    }

    // WARNING: This should NOT be used for actual comparisons as it compares
    // the UTF8-encoded strings directly. It is only provided to be able to
    // use String in a container like std::map that requires an order.
    // Use compare() instead.
    bool operator<(const String& o) const
    {
        return m_str < o.m_str;
    }

    std::size_t hash() const
    {
        return std::hash<std::string>()(m_str);
    }

    template <class CharT, class Traits> std::basic_istream<CharT, Traits>&
        getline(std::basic_istream<CharT, Traits>& in, CharT delim)
    {
        return std::getline(in, m_str, delim);
    }

    template <class CharT, class Traits> std::basic_istream<CharT, Traits>&
        getline(std::basic_istream<CharT, Traits>& in)
    {
        return std::getline(in, m_str);
    }

private:
    U32 toWideChar(char, const std::locale&) const;
    char toNarrowChar(U32, char replacement, const std::locale&) const;

    friend std::ostream& operator<<(std::ostream& os, const String& s);
    friend std::istream& operator>>(std::istream& is, String& s);

    std::string m_str;
};

#ifdef KRAKEN_ENABLE_UNICODE_SUPPORT
inline int compare(const String& a, const String& b,
                   const std::locale& loc = std::locale(),
                   ComparisonLevel level = ComparisonLevel::BaseAccents)
{
    return a.compare(b, loc, level);
}

// CONVERT ENTIRE STRING TO UPPERCASE
String uppercase(const String&);

// convert entire string to lowercase
String lowercase(const String&);

// Convert Entire String To Titlecase
String titlecase(const String&);

// convert to a case-independent representation (ß -> ss etc.)
String foldcase(const String&);


// Normalise the representation of the string.
String normalise(const String&);

// Normalise by decomposing (é -> e + ´)
String normaliseNFD(const String&);

// Normalise by decomposing then re-composing (Å -> A+° -> Å)
String normaliseNFC(const String&);

// Normalise as NFD, but remove some formatting distinctions (2⁵ -> 25)
String normaliseNFKD(const String&);

// Normalise as NFC, but remove some formatting distinctions (2⁵ -> 25)
String normaliseNFKC(const String&);


bool isAlnum(U32 codepoint);
bool isAlpha(U32 codepoint);
bool isLower(U32 codepoint);
bool isUpper(U32 codepoint);
bool isDigit(U32 codepoint);
bool isHexDigit(U32 codepoint);
bool isControl(U32 codepoint);
bool isGraphical(U32 codepoint);
bool isWhitespace(U32 codepoint);
bool isBlank(U32 codepoint);
bool isPunct(U32 codepoint);

#endif // KRAKEN_ENABLE_UNICODE_SUPPORT

inline std::ostream& operator<<(std::ostream& os, const String& s)
{
    os << s.m_str;
    return os;
}

inline std::istream& operator>>(std::istream& is, String& s)
{
    is >> s.m_str;
    return is;
}

inline String operator+(String a, const String& b)
{
    return a += b;
}

inline bool operator<=(const String& a, const String& b)
{
    // The order of operations here is important (for performance reasons):
    // If a < b then a == b won't have to be evaluated.
    // If a == b then a < b will only evaluate the first character.
    return ((a < b) || (a == b));
}

inline bool operator>(const String& a, const String& b)
{
    return !(a <= b);
}

inline bool operator>=(const String& a, const String& b)
{
    return !(a < b);
}

} // namespace kraken


namespace std
{

template <class CharT, class Traits>
basic_istream<CharT, Traits>& getline(basic_istream<CharT, Traits>& in,
                                      kraken::String& str,
                                      CharT delim)
{
    return str.getline(in, delim);
}

template <class CharT, class Traits>
basic_istream<CharT, Traits>& getline(basic_istream<CharT, Traits>& in,
                                      kraken::String& str)
{
    return str.getline(in);
}

template <>
struct iterator_traits<kraken::StringIterator>
{
    using difference_type = std::ptrdiff_t;
    using value_type = kraken::U32;
    using pointer = kraken::U32*;
    using reference = kraken::U32&;
    using iterator_category = std::bidirectional_iterator_tag;
};

template <>
struct iterator_traits<kraken::ReverseStringIterator>
{
    using difference_type = std::ptrdiff_t;
    using value_type = kraken::U32;
    using pointer = kraken::U32*;
    using reference = kraken::U32&;
    using iterator_category = std::bidirectional_iterator_tag;
};

template<> struct hash<kraken::String>
{
    using result_type = std::size_t;
    result_type operator()(const kraken::String& s) const
    {
        return s.hash();
    }
};

} // namespace std

#endif // KRAKEN_UTILITY_STRING_HPP_INCLUDED

