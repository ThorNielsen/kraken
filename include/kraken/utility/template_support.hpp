#ifndef KRAKEN_UTILITY_TEMPLATE_SUPPORT_HPP_INCLUDED
#define KRAKEN_UTILITY_TEMPLATE_SUPPORT_HPP_INCLUDED

#include <cstddef>
#include <type_traits>

namespace kraken
{

template <std::size_t a, std::size_t b>
struct equal_size
{
    static constexpr bool value = a == b;
};

template <typename Tp>
using remove_cvr = typename std::remove_cv_t<std::remove_reference_t<Tp>>;

} // namespace kraken

#endif // KRAKEN_UTILITY_TEMPLATE_SUPPORT_HPP_INCLUDED
