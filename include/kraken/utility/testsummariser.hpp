#ifndef KRAKEN_UTILITY_TESTSUMMARISER_HPP_INCLUDED
#define KRAKEN_UTILITY_TESTSUMMARISER_HPP_INCLUDED

#include <format>
#include <iterator>
#include <iostream>
#include <map>
#include <source_location>
#include <string>
#include <string_view>
#include <vector>

namespace kraken
{

struct TestSummariser
{
public:
    TestSummariser() = default;
    TestSummariser(TestSummariser&& other) = default;
    TestSummariser& operator=(TestSummariser&&) = default;

    TestSummariser& operator+=(TestSummariser&& other)
    {
        m_failures.insert(m_failures.end(),
                          std::make_move_iterator(other.m_failures.begin()),
                          std::make_move_iterator(other.m_failures.end()));
        m_successes += other.m_successes;
        other.m_failures.clear();
        other.m_successes = 0;
        other.disarm();
        return *this;
    }

    ~TestSummariser()
    {
        print();
    }

    bool expect(bool expr,
                std::string msg = "",
                std::source_location location = std::source_location::current())
    {
        if (!expr) m_failures.emplace_back(location, std::move(msg));
        else ++m_successes;
        return expr;
    }

    bool expectFalse(bool expr,
                     std::string msg = "",
                     std::source_location location = std::source_location::current())
    {
        if (expr) m_failures.emplace_back(location, std::move(msg));
        else ++m_successes;
        return !expr;
    }

    bool check(bool expr,
               std::source_location location = std::source_location::current())
    {
        return expect(expr, "Failed to ensure truthiness", location);
    }

    void assert(bool expr,
                std::source_location location = std::source_location::current())
    {
        if (!expect(expr, "Failed to assert truthiness", location))
        {
            throw std::runtime_error("Assert failed.");
        }
    }

    int returnCode() const noexcept
    {
        return !m_failures.empty();
    }

private:
    void disarm()
    {
        m_armed = false;
    }

    void print()
    {
        if (!m_successes && m_failures.empty())
        {
            if (m_armed) std::cerr << "\033[36mNo tests ran.\033[0m\n";
            return;
        }
        const char* successColour = m_successes ? "\033[32m" : "\033[30m";
        const char* failureColour = m_failures.empty() ? "\033[30m" : "\033[31m";
        std::cerr << std::format("Ran {} tests: {}{} succeded\033[0m, {}{} failed\033[0m.\n",
                                 m_successes + m_failures.size(),
                                 successColour,
                                 m_successes,
                                 failureColour,
                                 m_failures.size());

        if (m_failures.empty()) return;

        std::map
        <
            std::string_view,
            std::map
            <
                std::string_view,
                std::vector
                <
                    std::pair
                    <
                        decltype(std::source_location{}.line()),
                        std::string
                    >
                >
            >
        > failureLocations;

        for (auto& failure : m_failures)
        {
            failureLocations[failure.first.file_name()]
                            [failure.first.function_name()]
                            .emplace_back(failure.first.line(), failure.second);
        }

        for (const auto& [fileName, locationsWithFunction] : failureLocations)
        {
            std::cerr << std::format("\033[31m{}\033[0m:\n", fileName);
            for (const auto& [functionName, failures] : locationsWithFunction)
            {
                std::cerr << std::format("  {} failure{} in \033[31m{}\033[0m:\n",
                                         failures.size(),
                                         failures.size() > 1 ? "s" : "",
                                         functionName);
                for (const auto& [line, message] : failures)
                {
                    std::cerr << std::format("    * Test failed at line {}; full path: {}:{}\n",
                                             line,
                                             fileName,
                                             line);
                    if (!message.empty())
                    {
                        std::cerr << "      Message: " << message << "\n";
                    }
                }
            }
        }
    }

    std::vector<std::pair<std::source_location, std::string>> m_failures;
    std::size_t m_successes = 0;
    bool m_armed = true;
};

} // namespace kraken

#endif // KRAKEN_UTILITY_TESTSUMMARISER_HPP_INCLUDED

