// This file is part of the Kraken project, and in addition to the license for
// that project (if any), this file itself is also available under the MIT
// License, the text of which follows below.

// MIT License

// Copyright (c) 2022 Thor Gabelgaard Nielsen

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef KRAKEN_UTILITY_TIMER_HPP_INCLUDED
#define KRAKEN_UTILITY_TIMER_HPP_INCLUDED

#include <kraken/types.hpp>

#include <chrono>
#include <iostream>
#include <string>

namespace kraken
{

class Timer
{
public:
    using Seconds = F64;

    Timer()
    {
        reset();
    }
    void reset()
    {
        m_start = getCurrentTime();
        m_paused = false;
    }
    void pause()
    {
        m_pausedAt = getCurrentTime();
        m_paused = true;
    }
    void resume()
    {
        if (!m_paused) return;
        auto resume = getCurrentTime();
        m_start += resume - m_pausedAt;
        m_paused = false;

    }
    Seconds duration()
    {
        auto end = m_paused ? m_pausedAt : getCurrentTime();
        return std::chrono::duration<Seconds>(end - m_start).count();
    }

private:
    using Timepoint = decltype(std::chrono::high_resolution_clock::now());

    Timepoint getCurrentTime()
    {
        return std::chrono::high_resolution_clock::now();
    }

    Timepoint m_start;
    Timepoint m_pausedAt;
    bool m_paused = false;
};

class ScopeTimer
{
public:
    ScopeTimer(const std::string& s) : m_msg{s} {}
    ~ScopeTimer()
    {
        std::cout << "Timer \"" << m_msg << "\": " << m_timer.duration() << "s\n";
    }

private:
    Timer m_timer;
    std::string m_msg;
};

} // namespace kraken

#endif // KRAKEN_UTILITY_TIMER_HPP_INCLUDED
