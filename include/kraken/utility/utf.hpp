#ifndef KRAKEN_UTILITY_UTF_HPP_INCLUDED
#define KRAKEN_UTILITY_UTF_HPP_INCLUDED

#include <kraken/utility/template_support.hpp>
#include <kraken/types.hpp>

#include <iterator>
#include <type_traits>

namespace kraken
{

constexpr U32 invalidCodepoint = 0xffffffffu;

inline bool isValidCodepoint(kraken::U32 codepoint)
{
    return codepoint <= 0x10ffff && (codepoint <= 0xd7ff || codepoint >= 0xe000);
}

inline unsigned getUTF8SequenceByteLength(U8 firstByte) noexcept
{
    static constexpr unsigned byteLengths[32] =
    {
        1, 1, 1, 1,  1, 1, 1, 1,
        1, 1, 1, 1,  1, 1, 1, 1,
        0, 0, 0, 0,  0, 0, 0, 0, // All on this line are invalid
        2, 2, 2, 2,  3, 3, 4, 0, // Last one is invalid
    };
    return byteLengths[firstByte >> 3];
}

inline unsigned getUTF8ByteCount(U32 codepoint) noexcept
{
    if (codepoint < 0x80) return 1;
    if (codepoint < 0x800) return 2;
    if (codepoint < 0x10000) return 3;
    return 4;
}

template <typename Iterator>
inline Iterator decodeUTF8(Iterator begin, Iterator end, U32& out,
                           std::random_access_iterator_tag,
                           U32 replacement = invalidCodepoint) noexcept
{
    static constexpr U32 offset[5] =
    {
        0, 0, 0x3080, 0xe2080, 0x3c82080
    };
    static constexpr U32 minVals[5] =
    {
        0, 0, 0x3100, 0xe2880, 0x3c92080
    };

    U8 reader = static_cast<U8>(*begin);

    unsigned byteLength = getUTF8SequenceByteLength(reader);
    U8 requiredOneMask = 0;
    U8 requiredZeroMask = 0x80 >> byteLength;

    if (begin + byteLength <= end)
    {
        out = 0;
        switch (byteLength)
        {
        default:
            ++begin;
            goto decodeError;

        case 4:
            out += reader;
            out <<= 6;
            ++begin;
            requiredZeroMask = 0x40;
            requiredOneMask = 0x80;

            // Fallthrough
        case 3:
            out += reader = static_cast<U8>(*begin);
            if (((reader & requiredZeroMask)
                 | ((reader & requiredOneMask) ^ requiredOneMask)))
            {
                goto decodeError;
            }
            out <<= 6;
            ++begin;
            requiredZeroMask = 0x40;
            requiredOneMask = 0x80;

            // Fallthrough
        case 2:
            out += reader = static_cast<U8>(*begin);
            if (((reader & requiredZeroMask)
                 | ((reader & requiredOneMask) ^ requiredOneMask)))
            {
                goto decodeError;
            }
            out <<= 6;
            ++begin;
            requiredZeroMask = 0x40;
            requiredOneMask = 0x80;
            out += reader = static_cast<U8>(*begin);
            if (((reader & requiredZeroMask)
                 | ((reader & requiredOneMask) ^ requiredOneMask)))
            {
                goto decodeError;
            }
            ++begin;
            break;

        case 1:
            out += static_cast<U8>(*begin++);
        }
        if (out < minVals[byteLength]) out = replacement;
        else out -= offset[byteLength];
    }
    else
    {
        ++begin;
        goto decodeError;
    }

    return begin;
decodeError:
    // Invalid character (possibly split at a wrong point).
    out = replacement;
    return begin;
}

template <typename Iterator>
inline Iterator decodeUTF8(Iterator begin, Iterator end, U32& out,
                           std::forward_iterator_tag,
                           U32 replacement = invalidCodepoint) noexcept
{
    static constexpr U32 offset[5] =
    {
        0, 0, 0x3080, 0xe2080, 0x3c82080
    };
    static constexpr U32 minVals[5] =
    {
        0, 0, 0x3100, 0xe2880, 0x3c92080
    };

    U8 reader = static_cast<U8>(*begin);

    unsigned byteLength = getUTF8SequenceByteLength(reader);

    U8 requiredOneMask = 0;
    U8 requiredZeroMask = 0x80 >> byteLength;

    out = 0;
    switch (byteLength)
    {
    default:
        ++begin;
        goto decodeError;

    case 4:
        out += reader;
        if (begin++ == end) goto decodeError;
        out <<= 6;
        requiredZeroMask = 0x40;
        requiredOneMask = 0x80;

        // Fallthrough
    case 3:
        out += reader = static_cast<U8>(*begin);
        if (((reader & requiredZeroMask)
             | ((reader & requiredOneMask) ^ requiredOneMask))
            || begin++ == end) goto decodeError;
        out <<= 6;
        requiredZeroMask = 0x40;
        requiredOneMask = 0x80;

        // Fallthrough
    case 2:
        out += reader = static_cast<U8>(*begin);
        if (((reader & requiredZeroMask)
             | ((reader & requiredOneMask) ^ requiredOneMask))
            || begin++ == end) goto decodeError;
        out <<= 6;
        requiredZeroMask = 0x40;
        requiredOneMask = 0x80;
        out += reader = static_cast<U8>(*begin);
        if (((reader & requiredZeroMask)
             | ((reader & requiredOneMask) ^ requiredOneMask))
            || begin++ == end) goto decodeError;
        break;

    case 1:
        out += static_cast<U8>(*begin++);
    }
    if (out < minVals[byteLength]) out = replacement;
    else out -= offset[byteLength];
    return begin;

decodeError:
    // Invalid character (possibly split at a wrong point).
    out = replacement;
    return begin;
}

template <typename Iterator>
inline Iterator decodeUTF8(Iterator begin, Iterator end, U32& out,
                           U32 replacement = invalidCodepoint) noexcept
{
    using traits = std::iterator_traits<Iterator>;
    return decodeUTF8(begin, end, out, typename traits::iterator_category{},
                      replacement);
}

template <typename OutIt>
inline OutIt encodeUTF8(U32 codepoint, OutIt output) noexcept
{
    static constexpr U8 firstByte[4] =
    {
        0x0, 0xc0, 0xe0, 0xf0
    };

    int extraBytes = getUTF8ByteCount(codepoint)-1;

    if (!isValidCodepoint(codepoint))
    {
        // Invalid code point, just ignore it.
        return output;
    }

    U8 bytes[4];
    switch (extraBytes)
    {
    case 3:
        bytes[3] = static_cast<U8>(0x80 | (codepoint & 0x3f));
        codepoint >>= 6;
        // Fallthrough
    case 2:
        bytes[2] = static_cast<U8>(0x80 | (codepoint & 0x3f));
        codepoint >>= 6;
        // Fallthrough
    case 1:
        bytes[1] = static_cast<U8>(0x80 | (codepoint & 0x3f));
        codepoint >>= 6;
        // Fallthrough
    case 0: default: // Default case cannot happen.
        bytes[0] = static_cast<U8>(firstByte[extraBytes] | codepoint);
        codepoint >>= 6;
        // Fallthrough
    }

    return std::copy(bytes, bytes + extraBytes + 1, output);
}

template <typename Iterator>
inline auto decodeUTF16LE(Iterator begin, Iterator end, U32& out,
                          U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    out = *begin++;
    if (begin == end) goto decodeError;
    out = (*begin++ << 8) | out;
    if (0xd7ff < out && out < 0xe000)
    {
        out -= 0xd7c0;
        if (begin != end)
        {
            out <<= 10;
            U16 tmp = *begin++;
            if (begin == end) goto decodeError;
            tmp = (*begin++ << 8) | tmp;
            out |= (tmp - 0xdc00) & 0x3ff;
        }
        else
        {
            out = replacement;
        }
    }
    return begin;

decodeError:
    out = replacement;
    return begin;
}


template <typename Iterator>
inline auto decodeUTF16BE(Iterator begin, Iterator end, U32& out,
                          U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    out = *begin++;
    if (begin == end) goto decodeError;
    out = (*begin++) | (out << 8);
    if (0xd7ff < out && out < 0xe000)
    {
        out -= 0xd7c0;
        if (begin != end)
        {
            out <<= 10;
            U16 tmp = *begin++;
            if (begin == end) goto decodeError;
            tmp = (*begin++) | (tmp << 8);
            out |= (tmp - 0xdc00) & 0x3ff;
        }
        else
        {
            out = replacement;
        }
    }
    return begin;

decodeError:
    out = replacement;
    return begin;
}

template <typename Iterator>
inline auto decodeUTF16(Iterator begin, Iterator end, U32& out,
                        U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 2>::value, Iterator>::type
{
    out = *begin++;
    if (0xd7ff < out && out < 0xe000)
    {
        out -= 0xd7c0;
        if (begin != end)
        {
            out <<= 10;
            out |= (*begin++ - 0xdc00) & 0x3ff;
        }
        else
        {
            out = replacement;
        }
    }
    return begin;
}

template <typename OutIt>
inline OutIt encodeUTF16LE(U32 codepoint, OutIt output) noexcept
{
    if (codepoint < 0x10000)
    {
        // Trying to encode an invalid codepoint (surrogate).
        if (0xd7ff < codepoint && codepoint < 0xe000)
        {
            return output;
        }
        *output++ = static_cast<U8>(codepoint&0xff);
        *output++ = static_cast<U8>(codepoint>>8);
    }
    else
    {
        U32 cpr = codepoint - 0x10000;
        U16 enc1 = 0xd800 | ((cpr >> 10) & 0x3ff);
        U16 enc2 = 0xdc00 | (cpr & 0x3ff);
        *output++ = static_cast<U8>(enc1&0xff);
        *output++ = static_cast<U8>(enc1>>8);
        *output++ = static_cast<U8>(enc2&0xff);
        *output++ = static_cast<U8>(enc2>>8);
    }
    return output;
}

template <typename OutIt>
inline OutIt encodeUTF16BE(U32 codepoint, OutIt output) noexcept
{
    if (codepoint < 0x10000)
    {
        // Trying to encode an invalid codepoint (surrogate).
        if (0xd7ff < codepoint && codepoint < 0xe000)
        {
            return output;
        }
        *output++ = static_cast<U8>(codepoint>>8);
        *output++ = static_cast<U8>(codepoint&0xff);
    }
    else
    {
        U32 cpr = codepoint - 0x10000;
        U16 enc1 = 0xd800 | ((cpr >> 10) & 0x3ff);
        U16 enc2 = 0xdc00 | (cpr & 0x3ff);
        *output++ = static_cast<U8>(enc1>>8);
        *output++ = static_cast<U8>(enc1&0xff);
        *output++ = static_cast<U8>(enc2>>8);
        *output++ = static_cast<U8>(enc2&0xff);
    }
    return output;
}

template <typename OutIt>
inline OutIt encodeUTF16(U32 codepoint, OutIt output) noexcept
{
    if (codepoint < 0x10000)
    {
        // Trying to encode an invalid codepoint (surrogate).
        if (0xd7ff < codepoint && codepoint < 0xe000)
        {
            return output;
        }
        *output++ = static_cast<U16>(codepoint);
    }
    else
    {
        U32 cpr = codepoint - 0x10000;
        *output++ = static_cast<U16>(0xd800 | ((cpr >> 10) & 0x3ff));
        *output++ = static_cast<U16>(0xdc00 | (cpr & 0x3ff));
    }
    return output;
}

template <typename Iterator>
inline auto decodeUTF32LE(Iterator begin, Iterator end, U32& out,
                          std::forward_iterator_tag,
                          U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    out = *begin++;
    if (begin == end) goto decodeError;
    out = (*begin++ << 8)  | out;
    if (begin == end) goto decodeError;
    out = (*begin++ << 16) | out;
    if (begin == end) goto decodeError;
    out = (*begin++ << 24) | out;
    if (!isValidCodepoint(out))
    {
        out = replacement;
    }
    return begin;

decodeError:
    out = replacement;
    return begin;
}

template <typename Iterator>
inline auto decodeUTF32LE(Iterator begin, Iterator end, U32& out,
                          std::random_access_iterator_tag,
                          U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    if (begin + 3 >= end)
    {
        out = replacement;
        return end;
    }
    out = *begin++;
    out = (*begin++ << 8)  | out;
    out = (*begin++ << 16) | out;
    out = (*begin++ << 24) | out;
    if (!isValidCodepoint(out))
    {
        out = replacement;
    }
    return begin;
}

template <typename Iterator>
inline auto decodeUTF32LE(Iterator begin, Iterator end, U32& out,
                          U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    using traits = std::iterator_traits<Iterator>;
    return decodeUTF32LE(begin, end, out, typename traits::iterator_category{},
                         replacement);
}

template <typename Iterator>
inline auto decodeUTF32BE(Iterator begin, Iterator end, U32& out,
                          std::forward_iterator_tag,
                          U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    out = *begin++;
    if (begin == end) goto decodeError;
    out = (*begin++) | (out << 8);
    if (begin == end) goto decodeError;
    out = (*begin++) | (out << 8);
    if (begin == end) goto decodeError;
    out = (*begin++) | (out << 8);
    if (!isValidCodepoint(out))
    {
        out = replacement;
    }
    return begin;

decodeError:
    out = replacement;
    return begin;
}

template <typename Iterator>
inline auto decodeUTF32BE(Iterator begin, Iterator end, U32& out,
                          std::random_access_iterator_tag,
                          U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    if (begin + 3 >= end)
    {
        out = replacement;
        return end;
    }
    out = *begin++;
    out = (*begin++) | (out << 8);
    out = (*begin++) | (out << 8);
    out = (*begin++) | (out << 8);
    if (!isValidCodepoint(out))
    {
        out = replacement;
    }
    return begin;
}

template <typename Iterator>
inline auto decodeUTF32BE(Iterator begin, Iterator end, U32& out,
                          U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    using traits = std::iterator_traits<Iterator>;
    return decodeUTF32BE(begin, end, out, typename traits::iterator_category{},
                         replacement);
}

template <typename Iterator>
inline auto decodeUTF32(Iterator begin, Iterator end, U32& out) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 4>::value, Iterator>::type
{
    (void)end; // Unused
    out = *begin++;
    return begin;
}

template <typename OutIt>
inline OutIt encodeUTF32LE(U32 codepoint, OutIt output) noexcept
{
    if (!isValidCodepoint(codepoint))
    {
        return output;
    }
    *output++ = (codepoint      ) & 0xff;
    *output++ = (codepoint >>  8) & 0xff;
    *output++ = (codepoint >> 16) & 0xff;
    *output++ = (codepoint >> 24) & 0xff;
    return output;
}

template <typename OutIt>
inline OutIt encodeUTF32BE(U32 codepoint, OutIt output) noexcept
{
    if (!isValidCodepoint(codepoint))
    {
        return output;
    }
    *output++ = (codepoint >> 24) & 0xff;
    *output++ = (codepoint >> 16) & 0xff;
    *output++ = (codepoint >>  8) & 0xff;
    *output++ = (codepoint      ) & 0xff;
    return output;
}

template <typename OutIt>
inline OutIt encodeUTF32(U32 codepoint, OutIt output) noexcept
{
    *output++ = codepoint;
    return output;
}

enum class UnicodeEncoding
{
    Unknown = 0,
    UTF8,
    UTF16LE, UTF16BE,
    UTF32LE, UTF32BE,
};

template <typename Iterator>
inline auto decode(Iterator begin, Iterator end,
                   U32& output, UnicodeEncoding enc,
                   U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    switch (enc)
    {
    case UnicodeEncoding::UTF8: return decodeUTF8(begin, end, output);
    case UnicodeEncoding::UTF16LE: return decodeUTF16LE(begin, end, output);
    case UnicodeEncoding::UTF16BE: return decodeUTF16BE(begin, end, output);
    case UnicodeEncoding::UTF32LE: return decodeUTF32LE(begin, end, output);
    case UnicodeEncoding::UTF32BE: return decodeUTF32BE(begin, end, output);
    case UnicodeEncoding::Unknown: default:
        output = replacement;
        return begin;
    }
}

template <typename Iterator, typename OutIt>
inline auto decodeUTF8Str(Iterator begin, Iterator end, OutIt output,
                          U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    while (begin != end)
    {
        U32 cp;
        begin = decodeUTF8(begin, end, cp, replacement);
        *output++ = cp;
    }
    return begin;
}

template <typename Iterator, typename OutIt>
inline auto decodeUTF16LEStr(Iterator begin, Iterator end, OutIt output,
                             U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    while (begin != end)
    {
        U32 cp;
        begin = decodeUTF16LE(begin, end, cp, replacement);
        *output++ = cp;
    }
    return begin;
}

template <typename Iterator, typename OutIt>
inline auto decodeUTF16BEStr(Iterator begin, Iterator end, OutIt output,
                             U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    while (begin != end)
    {
        U32 cp;
        begin = decodeUTF16BE(begin, end, cp, replacement);
        *output++ = cp;
    }
    return begin;
}

template <typename Iterator, typename OutIt>
inline auto decodeUTF32LEStr(Iterator begin, Iterator end, OutIt output,
                             U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    while (begin != end)
    {
        U32 cp;
        begin = decodeUTF32LE(begin, end, cp, replacement);
        *output++ = cp;
    }
    return begin;
}

template <typename Iterator, typename OutIt>
inline auto decodeUTF32BEStr(Iterator begin, Iterator end, OutIt output,
                             U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    while (begin != end)
    {
        U32 cp;
        begin = decodeUTF32BE(begin, end, cp, replacement);
        *output++ = cp;
    }
    return begin;
}

template <typename Iterator, typename OutIt>
inline auto decodeUTFStr(Iterator begin, Iterator end, OutIt output,
                         UnicodeEncoding enc,
                         U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    switch (enc)
    {
    case UnicodeEncoding::UTF8:
        return decodeUTF8Str(begin, end, output, replacement);
    case UnicodeEncoding::UTF16LE:
        return decodeUTF16LEStr(begin, end, output, replacement);
    case UnicodeEncoding::UTF16BE:
        return decodeUTF16BEStr(begin, end, output, replacement);
    case UnicodeEncoding::UTF32LE:
        return decodeUTF32LEStr(begin, end, output, replacement);
    case UnicodeEncoding::UTF32BE:
        return decodeUTF32BEStr(begin, end, output, replacement);
    default:
        return begin;
    }
}

UnicodeEncoding detectUnicodeEncoding(U8 firstBytes[16]) noexcept;

// Try to auto-detect encoding of text by looking at the initial few characters.
// This favors UTF-8 in that this is assumed to be the encoding if no other can
// be conclusively determined.
// However, it may return 'Unknown' if there are characters which none of the
// encodings permit.
template <typename Iterator>
inline auto detectUnicodeEncoding(Iterator begin, Iterator end) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value,
                           UnicodeEncoding>::type
{
    U8 bytes[16] = {0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0};
    auto readCount = 0;
    while (begin != end && readCount < 16)
    {
        bytes[readCount++] = static_cast<U8>(*begin++);
    }
    return detectUnicodeEncoding(bytes);
}

template <typename Iterator, typename OutIt>
inline auto decodeUTFStr(Iterator begin, Iterator end, OutIt output,
                         U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    return decodeUTFStr(begin, end, output,
                        detectUnicodeEncoding(begin, end), replacement);
}

template <typename Iterator, typename OutIt>
inline auto decodeUTFStr(Iterator begin, Iterator end, OutIt output,
                         U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 2>::value, Iterator>::type
{
    while (begin != end)
    {
        U32 cp;
        begin = decodeUTF16(begin, end, cp, replacement);
        *output++ = cp;
    }
    return begin;
}


template <typename Iterator, typename OutIt>
inline auto decodeUTFStr(Iterator begin, Iterator end, OutIt output,
                         U32 = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 4>::value, Iterator>::type
{
    while (begin != end)
    {
        *output++ = *begin++;
    }
    return begin;
}

template <typename Iterator, typename OutIt>
inline auto decodeUTFStr(Iterator begin, OutIt output,
                         U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, Iterator>::type
{
    auto end = begin;
    while (*end++)
        ;
    // Todo: Fix broken UTF-8 input by replacing with replacement.
    // (Right now it is just assumed that valid UTF-8 is passed)
    while (begin != end)
    {
        U32 value{replacement};
        begin = decodeUTF8(begin, end, value, replacement);
        *output++ = value;
    }
    return begin;
}

template <typename Iterator, typename OutIt>
inline auto decodeUTFStr(Iterator begin, OutIt output,
                         U32 replacement = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 2>::value, Iterator>::type
{
    while (*begin)
    {
        U32 cp;
        Iterator end = begin;
        ++end;
        if (*end) ++end;
        begin = decodeUTF16(begin, end, cp, replacement);
        *output++ = cp;
    }
    return begin;
}


template <typename Iterator, typename OutIt>
inline auto decodeUTFStr(Iterator begin, OutIt output,
                         U32 = invalidCodepoint) noexcept
-> typename std::enable_if<equal_size<sizeof(*begin), 4>::value, Iterator>::type
{
    while (*begin)
    {
        *output++ = *begin++;
    }
    return begin;
}

} // namespace kraken

#endif // KRAKEN_UTILITY_UTF_HPP_INCLUDED

