#ifndef KRAKEN_UTILITY_ZLIB_HPP_INCLUDED
#define KRAKEN_UTILITY_ZLIB_HPP_INCLUDED

#include "kraken/types.hpp"
#include <algorithm>
#include <cstddef>
#include <kraken/utility/checksum.hpp>
#include <kraken/utility/hufftree.hpp>
#include <kraken/utility/memory.hpp>
#include <kraken/utility/template_support.hpp>
#include <memory>
#include <stdexcept>
#include <type_traits>
#include <vector>

namespace kraken
{

class Inflate
{
public:
    enum InflateStatus : U32
    {
        Good = 0,
        Finished = 1,
        RequiresInput = 2,
        RequiresDictionary = 4,
        RequiresOutput = 8,
        BadInput = 16,
        BadChecksum = 32
    };
    Inflate();

    void setInput(const U8* begin, const U8* end);

    template <typename T>
    auto setInput(const T* begin, const T* end)
    -> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, void>::type
    {
        setInput(reinterpret_cast<const U8*>(begin),
                 reinterpret_cast<const U8*>(end));
    }

    template <typename T>
    auto setInput(const T* begin, size_t length)
    -> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, void>::type
    {
        setInput(reinterpret_cast<const U8*>(begin),
                 reinterpret_cast<const U8*>(begin)+length);
    }

    const U8* inputAt() const { return m_inputBegin + (m_bitpos >> 3); }
    const U8* inputEnd() const { return m_inputEnd; }
    U8*       outputAt() const { return m_outputAt; }
    const U8* outputEnd() const { return m_outputEnd; }

    void setOutput(U8* begin, const U8* end);

    template <typename T>
    auto setOutput(T* begin, const T* end)
    -> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, void>::type
    {
        setOutput(reinterpret_cast<U8*>(begin),
                  reinterpret_cast<const U8*>(end));
    }

    template <typename T>
    auto setOutput(T* begin, size_t length)
    -> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, void>::type
    {
        setOutput(reinterpret_cast<U8*>(begin),
                  reinterpret_cast<const U8*>(begin)+length);
    }

    // Returns true exactly when a stream has ended and been decompressed
    // correctly.
    bool inflate();

    InflateStatus status() { return static_cast<InflateStatus>(m_status); }

    void clear();

private:
    enum class State
    {
        ExpectHeader, ExpectBlock, InsideBlock, ExpectChecksum, StreamEnd
    };

    U64 bitsLeft() const
    {
        return m_bitpos <= m_availbits ? m_availbits - m_bitpos : 0;
    }
    // Returns true iff the required number of bits is available. If not, that
    // number of bits is requested.
    bool requireBits(U64 count);
    // Returns true iff there is at least one byte available to output to,
    // otherwise it adds InflateStatus::OutputRequired to the status and returns
    // false.
    bool requireOutput();


    bool readHufftreeLengths(const priv::HufftreeDecoder& decoder,
                             U16 codeCount,
                             std::vector<U8>& clens);
    void generateFixedTrees();
    bool readDynamicTrees();
    bool processHeader();

    // Reads a symbol from tree into output. Return value is number of bits read
    // and zero bits indicate that a decoding error has occured.
    U8 readSymbol(const priv::HufftreeDecoder& tree, U16& output);

    bool processBlock();

    bool processUncompressed();
    bool processCompressed();

    bool ensureCopying();

    void appendByte(U8 byte);
    U8 getPrevByte(U32 dist);

    void clearLitblock();
    void clearDtree();
    void clearCblock();
    void clearBlockInfo();

    U16 read(U8 bits);
    U16 longRead(U8 bits);
    void ensureByteAligned();
    void unget(U32 bits);
    U8 readAlignedByte();
    bool atOutputEnd() { return m_outputAt == m_outputEnd; }
    struct
    {
        U16 remainingLength;
        bool checksumRead;
        bool lengthRead;
    } m_litblockinfo;

    struct
    {
        std::vector<U8> codelengths;
        priv::HufftreeDecoder cltree;
        U16 litcodes;
        U16 distcodes;
        U16 clcodes;
        U8 currReading;
    } m_dtree;

    struct
    {
        U16 dupLen;
        U16 dupDist;
        bool treesRead;
    } m_cblock;

    priv::HufftreeDecoder m_littree;
    priv::HufftreeDecoder m_disttree;
    std::vector<U8> m_window;
    ADLER32sum m_checksum;

    const U8* m_inputBegin;
    const U8* m_inputEnd;
    U8* m_outputAt;
    const U8* m_outputEnd;
    U64 m_bitpos;
    U64 m_availbits;
    U32 m_status;

    State m_state;
    U32 m_windowpos;
    U8 m_blocktype;
    bool m_finalBlock;
};

namespace priv
{

class Deflate;

}

class Deflate
{
public:
    enum DeflateStatus : U32
    {
        Good = 0,
        Finished = 1,
        RequiresInput = 2,
        RequiresOutput = 4
    };
    Deflate();
    ~Deflate();

    void setInput(const U8* begin, const U8* end);

    template <typename T>
    auto setInput(const T* begin, const T* end)
    -> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, void>::type
    {
        setInput(reinterpret_cast<const U8*>(begin),
                 reinterpret_cast<const U8*>(end));
    }

    template <typename T>
    auto setInput(const T* begin, size_t length)
    -> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, void>::type
    {
        setInput(reinterpret_cast<const U8*>(begin),
                 reinterpret_cast<const U8*>(begin)+length);
    }

    const U8* inputAt() const;
    const U8* inputEnd() const;
          U8* outputAt() const;
    const U8* outputEnd() const;

    void setOutput(U8* begin, const U8* end);

    template <typename T>
    auto setOutput(T* begin, const T* end)
    -> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, void>::type
    {
        setOutput(reinterpret_cast<U8*>(begin),
                  reinterpret_cast<const U8*>(end));
    }

    template <typename T>
    auto setOutput(T* begin, size_t length)
    -> typename std::enable_if<equal_size<sizeof(*begin), 1>::value, void>::type
    {
        setOutput(reinterpret_cast<U8*>(begin),
                  reinterpret_cast<const U8*>(begin)+length);
    }

    bool deflate(bool finalInput);

    DeflateStatus status() const;

    void clear();

private:
    std::unique_ptr<priv::Deflate> m_deflate;
};

template <typename OutIt>
inline bool inflate(const U8* begin, const U8* end, OutIt output)
{
    if (end <= begin) return begin == end;
    size_t blockSize = std::max<size_t>(std::min<size_t>(size_t(end-begin), 8192), 64);
    auto buf = make_unique<U8[]>(blockSize);
    Inflate inflate;
    inflate.setInput(begin, end);
    inflate.setOutput(buf.get(), buf.get() + blockSize);
    while (!inflate.inflate())
    {
        if (inflate.status() & Inflate::BadChecksum) return false;
        if (inflate.status() & Inflate::BadInput) return false;
        if (inflate.status() & Inflate::RequiresDictionary) return false;
        if (inflate.status() & Inflate::Finished) break;
        if (inflate.status() & Inflate::RequiresInput)
        {
            throw std::logic_error("Inflate requires non-existent input.");
        }
        if (inflate.status() & Inflate::RequiresOutput)
        {
            const U8* at = buf.get();
            const U8* blockEnd = inflate.outputAt();
            while (at != blockEnd)
            {
                *output++ = *at++;
            }
            inflate.setOutput(buf.get(), buf.get() + blockSize);
        }
    }
    const U8* at = buf.get();
    const U8* blockEnd = inflate.outputAt();
    while (at != blockEnd)
    {
        *output++ = *at++;
    }
    return true;
}

template <typename OutIt>
inline bool inflate(const U8* begin, size_t size, OutIt output)
{
    return inflate(begin, begin+size, output);
}

template <typename OutIt>
inline void deflate(const U8* begin, const U8* end, OutIt output)
{
    if (end <= begin) return;
    size_t blockSize = std::max<size_t>(std::min<size_t>(size_t(end-begin), 8192), 64);
    auto buf = make_unique<U8[]>(blockSize);
    Deflate deflate;
    deflate.setInput(begin, end);
    deflate.setOutput(buf.get(), buf.get() + blockSize);
    while (!deflate.deflate(true))
    {
        if (deflate.status() & Deflate::Finished) break;
        if (deflate.status() & Deflate::RequiresInput)
        {
            throw std::logic_error("Deflate requires non-existent input.");
        }
        if (deflate.status() & Deflate::RequiresOutput)
        {
            const U8* at = buf.get();
            const U8* blockEnd = deflate.outputEnd();
            while (at != blockEnd)
            {
                *output++ = *at++;
            }
            deflate.setOutput(buf.get(), buf.get() + blockSize);
        }
    }
    if (deflate.inputAt() != deflate.inputEnd())
    {
        throw std::logic_error("Deflate didn't consume all data!");
    }
    const U8* at = buf.get();
    const U8* blockEnd = deflate.outputAt();
    while (at != blockEnd)
    {
        *output++ = *at++;
    }
}

template <typename OutIt>
inline void deflate(const U8* begin, size_t size, OutIt output)
{
    deflate(begin, begin+size, output);
}

} // namespace kraken

#endif // KRAKEN_UTILITY_ZLIB_HPP_INCLUDED
