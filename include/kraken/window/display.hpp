#ifndef KRAKEN_WINDOW_DISPLAY_HPP_INCLUDED
#define KRAKEN_WINDOW_DISPLAY_HPP_INCLUDED

#include <kraken/utility/string.hpp>
#include <kraken/types.hpp>
#include <functional>
#include <vector>

namespace kraken
{

namespace window
{

class DisplayID
{
public:
    DisplayID();
    DisplayID(const DisplayID&);
    ~DisplayID();
    DisplayID& operator=(const DisplayID&);
    bool operator==(const DisplayID& other) const noexcept
    {
        return m_display == other.m_display;
    }
    bool operator!=(const DisplayID& other) const noexcept
    {
        return m_display != other.m_display;
    }
private:
    void construct();
    void destroy();

    friend void priv_markDisconnect(DisplayID&);
    friend DisplayID* priv_next(DisplayID*);
    friend void* priv_getID(const DisplayID&);
    friend void priv_setID(DisplayID&, void*);
    void* m_display;
    DisplayID* m_prev;
    DisplayID* m_next;
    bool m_disconnected = false;
};

struct VideoMode
{
    U32 width = 0;
    U32 height = 0;
    F32 refreshRate = 0;
    U32 redBits = 8;
    U32 greenBits = 8;
    U32 blueBits = 8;
};

struct DisplayInfo
{
    std::vector<VideoMode> availableModes;
    std::vector<U16> gammaRampRed;
    std::vector<U16> gammaRampGreen;
    std::vector<U16> gammaRampBlue;
    String name;
    U32 physicalWidth = 0; // In millimetres
    U32 physicalHeight = 0; // In millimetres
    S32 virtualXPos = 0;
    S32 virtualYPos = 0;
};

std::vector<DisplayID> getDisplays();

DisplayID getPrimaryDisplay();

DisplayInfo getDisplayInfo(DisplayID id);

VideoMode currentVideoMode(DisplayID id);

void setDisplayConnectCallback(std::function<void(DisplayID)>);

void setDisplayDisconnectCallback(std::function<void(DisplayID)>);

void setDisplayDisconnectCallback(DisplayID id, std::function<void(DisplayID)>);


} // namespace window

} // namespace kraken

#endif // KRAKEN_WINDOW_DISPLAY_HPP_INCLUDED
