#ifndef KRAKEN_WINDOW_GLFW_HPP_INCLUDED
#define KRAKEN_WINDOW_GLFW_HPP_INCLUDED

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <kraken/utility/string.hpp>
#include <kraken/window/input.hpp>

#include <atomic>
#include <functional>
#include <map>
#include <vector>

namespace kraken
{

namespace window
{

namespace priv
{

struct WindowCallbacks
{
    std::function<void()> close;
    std::function<void()> focus;
    std::function<void()> defocus;
    std::function<void()> minimise;
    std::function<void()> maximise;
    std::function<void(bool)> restore;
    std::function<void()> redraw;

    std::function<void(S32, S32)> position;
    std::function<void(U32, U32)> resize;
    std::function<void(U32, U32)> fbresize;

    std::function<void(Key, Scancode, KeyModifiers)> keypress;
    std::function<void(Key, Scancode, KeyModifiers)> keyrepeat;
    std::function<void(Key, Scancode, KeyModifiers)> keyrelease;
    std::function<void(U32)> utfchar;
    std::function<void(Mouse, KeyModifiers)> mousepress;
    std::function<void(Mouse, KeyModifiers)> mouserelease;
    std::function<void(F64, F64)> scroll;

    std::function<void(F64, F64)> cursorposition;
    std::function<void()> cursorenter;
    std::function<void()> cursorleave;

    std::function<void(std::vector<String>)> stringdrop;
};

extern std::atomic<size_t> g_glfwErrorCount;

WindowCallbacks emptyCallbacks();

void setEmptyCallbacks(GLFWwindow* wnd, void* callbacks);

void clearCallbacks(GLFWwindow* wnd);

void registerGLFWCallbacks(GLFWwindow* wnd, void* callbacks);

void assertOnMainThread();

void initialiseGLFW();

void registerGLFWDependent();

void deregisterGLFWDependent();

struct GLFWGuard
{
    GLFWGuard() { registerGLFWDependent(); }
    ~GLFWGuard() { deregisterGLFWDependent(); }
};

int keyToGLFW(Key key);
Key keyFromGLFW(int key);


} // namespace priv

class DisplayID;

void* priv_getID(const DisplayID& did);
void priv_setID(DisplayID& did, void* id);
void priv_markDisconnect(DisplayID& did);
DisplayID* priv_next(DisplayID* did);


} // namespace window

} // namespace kraken

#endif // KRAKEN_WINDOW_GLFW_HPP_INCLUDED
