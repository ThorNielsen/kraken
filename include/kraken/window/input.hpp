#ifndef KRAKEN_WINDOW_INPUT_HPP_INCLUDED
#define KRAKEN_WINDOW_INPUT_HPP_INCLUDED

#include <kraken/types.hpp>
#include <kraken/utility/string.hpp>

namespace kraken
{

namespace window
{

using Scancode = S32;

struct KeyModifiers
{
    bool ctrl;
    bool alt;
    bool shift;
    bool super;
    bool numLock;
    bool capsLock;
};

enum class Key : U32
{
    Key0 = 0, Key1, Key2, Key3, Key4, Key5, Key6, Key7, Key8, Key9,
    A, B, C, D, E, F, G, H, I, J, K, L, M,
    N, O, P, Q, R, S, T, U, V, W, X, Y, Z,

    Hyphen, Comma, Period, Apostrophe,

    Space, Enter, Backspace, Insert, Delete,

    NumLock, CapsLock, ScrollLock,
    Escape, PrintScreen, Pause, Menu,

    Up, Down, Left, Right,
    Home, End, PageUp, PageDown, Tab,

    LeftControl, LeftAlt, LeftShift, LeftSuper,
    RightControl, RightAlt, RightShift, RightSuper,

    F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,
    F13, F14, F15, F16, F17, F18, F19, F20, F21, F22, F23, F24, F25,

    Numpad0, Numpad1, Numpad2, Numpad3, Numpad4,
    Numpad5, Numpad6, Numpad7, Numpad8, Numpad9,
    NumpadAdd, NumpadSubtract, NumpadMultiply, NumpadDivide,
    NumpadComma, NumpadEnter,

    Unknown,

    Last = Unknown
};

enum class Mouse : U32
{
    Button1 = 0, Button2, Button3, Button4, Button5, Button6, Button7, Button8,
    Left = Button1,
    Right = Button2,
    Middle = Button3,

    Last = Button8
};

enum class CursorMode : U32
{
    Visible = 0,
    Hidden = 1,
    Locked = 2,
    LockedUnaccelerated = 3,

    Last = LockedUnaccelerated
};

String getKeyName(Key);
String getKeyName(Scancode scancode);

Scancode getScancode(Key);

} // namespace window

} // namespace kraken

#endif // KRAKEN_WINDOW_INPUT_HPP_INCLUDED
