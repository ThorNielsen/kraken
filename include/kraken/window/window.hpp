#ifndef KRAKEN_WINDOW_WINDOW_HPP_INCLUDED
#define KRAKEN_WINDOW_WINDOW_HPP_INCLUDED

#include <kraken/types.hpp>
#include <kraken/image/image.hpp>
#include <kraken/utility/memory.hpp>
#include <kraken/utility/string.hpp>
#include <kraken/window/display.hpp>
#include <kraken/window/input.hpp>

#include <functional>
#include <string>
#include <vector>

#include <vulkan/vulkan.h>

namespace kraken
{

namespace window
{

enum class DisplayMode
{
    Windowed,
    Fullscreen,
    BorderlessFullscreen
};

// These can only be set before creation/on creation.
struct WindowCreationSettings
{
    DisplayMode mode = DisplayMode::Windowed;
    String title;
    DisplayID fullscreenDisplay;
    U32 width = 640;
    U32 height = 480;
    F32 refreshRate;
    S32 xpos = 2147483647;
    S32 ypos = 2147483647;
    bool decorations = true; // Titlebar, borders etc.
    bool focused = true; // Initial focus
    bool autominimise = false; // Minimise on focus loss.
    bool maximised = false;
    bool resizable = true; // Whether the window can be user-resized.
    bool topmost = false;
    bool visible = true; // Is the window initially visible?
    bool transparent = false;
    bool centerMouseFullscreen = true;
};

enum class CursorType
{
    Arrow,
    IBeam,
    Crosshair,
    Hand,
    HResize,
    VResize,
};

class Cursor
{
public:
    Cursor(CursorType);
    Cursor(image::Image, math::ivec2 clickAt);

private:
    friend class Window;
    struct CWrap
    {
        CWrap();
        ~CWrap();

        void* handle;
    };

    std::shared_ptr<CWrap> m_cursor;
};

class SurfaceCreationInfo
{
public:
    virtual ~SurfaceCreationInfo() {}

    virtual void beforeWindowOpen() {}
    virtual void* afterWindowOpen(void*) { return nullptr; }
    virtual void beforeWindowClose(void*) {}
    virtual void afterWindowClose() {}
    virtual void onFramebufferResize(U32, U32) {}

protected:
    SurfaceCreationInfo() {}
};

class VulkanSurfaceCreationInfo : public SurfaceCreationInfo
{
public:
    VulkanSurfaceCreationInfo(VkInstance instance,
                              const VkAllocationCallbacks* allocator,
                              VkResult& resultOut,
                              VkSurfaceKHR& surfaceOut)
        : m_instance{instance},
          m_allocator{allocator},
          m_resultOut{resultOut},
          m_surfaceOut{surfaceOut}
    {}

    static bool isPresentationSupported(VkInstance instance,
                                        VkPhysicalDevice device,
                                        U32 queueFamily);
    static std::vector<std::string> requiredInstanceExtensions();

    void beforeWindowOpen() override;
    void* afterWindowOpen(void*) override;

private:
    VkInstance m_instance;
    const VkAllocationCallbacks* m_allocator;
    VkResult& m_resultOut;
    VkSurfaceKHR& m_surfaceOut;
};

class Window
{
public:
    Window();
    Window(Window&& other);
    Window& operator=(Window&& other);

    ~Window();

    Window(const Window&) = delete;
    Window& operator=(const Window&) = delete;

    U32 width() const noexcept { return m_width; }
    U32 height() const noexcept { return m_height; }

    static void pollEvents();
    static void waitEvents(double timeout = 0);

    String title() const noexcept { return m_title; }
    void setTitle(String newTitle);

    bool isOpen() const noexcept
    {
        return m_open;
    }
    bool closeRequested() const;
    void setCloseFlag();
    void clearCloseFlag();

    bool open(WindowCreationSettings, std::unique_ptr<SurfaceCreationInfo>);

    void close() noexcept;

    void* nativeWindowHandle() const { return m_windowHandle; }
    void* windowExtra() const { return m_windowExtra; }

    void setCloseCallback(std::function<void()>);
    void setFocusCallback(std::function<void()>);
    void setDefocusCallback(std::function<void()>);
    void setMinimiseCallback(std::function<void()>);
    void setMaximiseCallback(std::function<void()>);
    void setRestoreCallback(std::function<void(bool)>); // Parameter is true if
                                                        // restore was from
                                                        // minimisation.
    void setRedrawCallback(std::function<void()>);

    void setPositionCallback(std::function<void(S32, S32)>);
    void setResizeCallback(std::function<void(U32, U32)>);
    void setFramebufferResizeCallback(std::function<void(U32, U32)>);

    void setKeyPressCallback(std::function<void(Key, Scancode, KeyModifiers)>);
    void setKeyRepeatCallback(std::function<void(Key, Scancode, KeyModifiers)>);
    void setKeyReleaseCallback(std::function<void(Key, Scancode, KeyModifiers)>);
    void setUTFCharCallback(std::function<void(U32)>);
    void setMousePressCallback(std::function<void(Mouse, KeyModifiers)>);
    void setMouseReleaseCallback(std::function<void(Mouse, KeyModifiers)>);
    void setScrollCallback(std::function<void(F64, F64)>);

    // Note: Cursor position is given relative to the top-left corner of the
    // client (i.e. non-decorated) area of the window.
    void setCursorPositionCallback(std::function<void(F64, F64)>);
    void setCursorEnterCallback(std::function<void()>);
    void setCursorLeaveCallback(std::function<void()>);

    void setStringDropCallback(std::function<void(std::vector<String>)>);

    bool isPressed(Key k) const;
    bool isPressed(Mouse m) const;
    math::dvec2 mousePos() const;
    void setMousePosition(math::dvec2);

    CursorMode cursorMode() const { return m_cursorMode; }
    void setCursorMode(CursorMode newMode);
    void setCursor(Cursor);
    Cursor cursor() const { return m_cursor; }

    void setIcon(const image::Image* iconsBegin, const image::Image* iconsEnd);
    void setIcon(const image::Image& img);
    void clearIcon()
    {
        setIcon(nullptr, nullptr);
    }

    void hide();
    void unhide();
    bool isHidden() const;

    void forceFocus();
    void requestFocus();

    void setSizeLimits(math::uvec2 minSize, math::uvec2 maxSize);
    void setAspectRatio(math::uvec2 xy);

    void setSize(math::uvec2 newSize);
    math::uvec2 size() const noexcept { return {m_width, m_height}; }

    math::uvec2 framebufferSize() const;

    void setPosition(math::ivec2 pos);
    math::ivec2 position() const;

    DisplayMode currentDisplayMode() const { return m_displayMode; }
    DisplayID fullscreenDisplay() const;

    void setDisplayMode(DisplayMode newMode,
                        math::ivec2 newPos,
                        math::uvec2 newSize,
                        F32 refreshRate = 0,
                        DisplayID display = getPrimaryDisplay());

    void setDecorated(bool enable = true);
    void setAutominimise(bool enable = true);
    void setTopmost(bool enable = true);
    void setResizable(bool enable = true);

    bool decorated() const;
    bool autominimise() const;
    bool topmost() const;
    bool resizable() const;

private:
    void handleFramebufferResize(U32 w, U32 h);
    void assertOpen(const char* msg = nullptr) const;

    String m_title;
    std::unique_ptr<SurfaceCreationInfo> m_sci;

    U32 m_width;
    U32 m_height;

    Cursor m_cursor;
    CursorMode m_cursorMode;
    DisplayMode m_displayMode;
    // Don't pollute public headers with implementation libraries' names.
    void* m_windowHandle;
    void* m_windowExtra;
    void* m_callbacks;
    bool m_open;
};

} // namespace window

} // namespace kraken

#endif // KRAKEN_WINDOW_WINDOW_HPP_INCLUDED
