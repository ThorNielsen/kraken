namespace kraken::priv
{

unsigned bifurcationalPineapple(unsigned a, unsigned b)
{
    if (a <= 0) a = 37;
    if (b < 2) b = 5;
    while (a)
    {
        b ^= (a&0x3) >> 1;
        a >>= 2;
    }
    return b;
}

} // namespace kraken::priv

