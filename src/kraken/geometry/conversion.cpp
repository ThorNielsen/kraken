#include "kraken/geometry/mesh.hpp"
#include "kraken/geometry/vertex.hpp"
#include "kraken/math/matrix.hpp"
#include <cstddef>
#include <functional>
#include <kraken/geometry/conversion.hpp>

namespace kraken::geometry
{

using namespace ::kraken::math;

namespace
{

struct SimpleVertex
{
    vec3 position;
    vec3 normal;
};

}

void toFlatRenderMesh(const Mesh& mesh,
                      const std::function<void*(size_t)>& vertexOutput,
                      VertexType& vertexTypeOut)
{
    vertexTypeOut.clear();
    vertexTypeOut.push_back({"position", VertexDataType::Float, 3});
    vertexTypeOut.push_back({"normal", VertexDataType::Float, 3});
    size_t triAlloc = mesh.faces();
    auto vSize = vertexSize(vertexTypeOut);
    auto* out = static_cast<SimpleVertex*>(vertexOutput(3 * vSize * triAlloc));
    size_t tris = 0;
    for (auto face = mesh.fbegin(); face != mesh.fend(); ++face)
    {
        auto edgeIdx = face->outer;
        auto originVertexIdx = mesh.edge(edgeIdx).origin;
        SimpleVertex tri[3];
        tri[0].normal = face->normal;
        tri[1].normal = tri[0].normal;
        tri[2].normal = tri[1].normal;
        tri[0].position = mesh.vertex(originVertexIdx).pos;
        auto cEdge = mesh.edge(edgeIdx).next;
        while (cEdge != edgeIdx)
        {
            auto vert1 = mesh.edge(cEdge).origin;
            cEdge = mesh.edge(cEdge).next;
            auto vert2 = mesh.edge(cEdge).origin;
            tri[1].position = mesh.vertex(vert1).pos;
            tri[2].position = mesh.vertex(vert2).pos;
            if (tris == triAlloc)
            {
                triAlloc *= 2;
                out = static_cast<SimpleVertex*>(vertexOutput(3 * vSize * triAlloc));
            }
            out[3*tris  ] = tri[0];
            out[3*tris+1] = tri[1];
            out[3*tris+2] = tri[2];
            ++tris;
        }
    }
    if (tris != triAlloc)
    {
        vertexOutput(3 * vSize * tris);
    }
}

} // namespace kraken::geometry
