#include "kraken/geometry/mesh.hpp"
#include "kraken/geometry/primitives.hpp"
#include "kraken/math/matrix.hpp"
#include <cstddef>
#include <cstdlib>
#include <kraken/geometry/convexity.hpp>
#include <kraken/geometry/primitivequeries.hpp>
#include <kraken/math/constants.hpp>
#include <vector>

namespace kraken::geometry
{

using namespace ::kraken::math;

size_t extremalPoint(vec3 direction, const std::vector<vec3>& vertices)
{
    size_t ep = 0;
    float proj = -Constant<float>::maximum;
    for (size_t i = 0; i < vertices.size(); ++i)
    {
        float p = dot(direction, vertices[i]);
        if (p > proj)
        {
            ep = i;
            proj = p;
        }
    }
    return ep;
}

VertexIndex convexExtremalPoint(vec3 direction, const Mesh& mesh,
                                FaceIndex startSearch)
{
    if (!mesh.vertices()) return invalidVertex;
    if (startSearch == invalidFace)
    {
        startSearch = mesh.fbegin().index();
    }
    FaceIndex best = startSearch;
    FaceIndex curr = best;
    auto maxProj = dot(mesh.face(best).normal, direction);
    VertexIndex extremalVertex = invalidVertex;
    auto maxVProj = -Constant<float>::maximum;
    while (curr != invalidFace)
    {
        FaceIndex better = invalidFace;
        EdgeIndex cEdge = mesh.face(curr).outer;
        do
        {
            auto tFace = mesh.edge(mesh.edge(cEdge).twin).face;

            auto proj = dot(mesh.face(tFace).normal, direction);
            if (proj > maxProj)
            {
                maxProj = proj;
                better = tFace;
                best = better;
            }
            auto vProj = dot(mesh.vertex(mesh.edge(cEdge).origin).pos,
                             direction);
            if (vProj > maxVProj)
            {
                maxVProj = vProj;
                extremalVertex = mesh.edge(cEdge).origin;
            }

            cEdge = mesh.edge(cEdge).next;
        } while (cEdge != mesh.face(curr).outer);
        curr = better;
    }
    return extremalVertex;
}

VertexIndex getCloserVertex(vec3 point, const Mesh& mesh, EdgeIndex edge,
                            float& squaredDistanceOut)
{
    if (edge == invalidEdge) return invalidVertex;
    auto v0 = mesh.edge(edge).origin;
    auto v1 = mesh.edge(mesh.edge(edge).twin).origin;
    vec3 p0 = mesh.vertex(v0).pos;
    vec3 p1 = mesh.vertex(v1).pos;
    auto sqlen0 = squaredLength(point-p0);
    auto sqlen1 = squaredLength(point-p1);
    if (sqlen0 <= sqlen1)
    {
        squaredDistanceOut = sqlen0;
        return v0;
    }

    squaredDistanceOut = sqlen1;
    return v1;
}

EdgeIndex getCloserEdge(vec3 point, const Mesh& mesh, FaceIndex face,
                        float& squaredDistanceOut)
{
    EdgeIndex closer = invalidEdge;
    float sqDist = Constant<float>::maximum;
    const Face& cFace = mesh.face(face);
    EdgeIndex eIdx = cFace.outer;
    do
    {
        const Edge& cEdge = mesh.edge(eIdx);
        vec3 p0 = mesh.vertex(cEdge.origin).pos;
        vec3 p1 = mesh.vertex(mesh.edge(cEdge.twin).origin).pos;
        auto cp = closestPointUnnormalised(Line3d<float>{p0, p1-p0}, point, 0.f, 1.f);
        auto dist = squaredLength(point-cp);
        if (dist < sqDist)
        {
            closer = eIdx;
            sqDist = dist;
        }
        eIdx = cEdge.next;
    } while (eIdx != cFace.outer);
    squaredDistanceOut = sqDist;
    return closer;
}

Feature convexClosestFeature(vec3 point, const Mesh& mesh,
                             vec3 innerPoint,
                             Feature startSearch)
{
    Feature found{invalidVertex, invalidEdge, invalidFace, FeatureType::None};
    if (!mesh.vertices()) return found;
    if (!mesh.isPolyhedron()) return found;
    if (startSearch.type == FeatureType::None)
    {
        found.face = mesh.fbegin().index();
    }
    else if (startSearch.type == FeatureType::Vertex)
    {
        found.face = mesh.edge(mesh.vertex(startSearch.vertex).edge).face;
    }
    else if (startSearch.type == FeatureType::Edge)
    {
        found.face = mesh.edge(startSearch.edge).face;
    }
    else
    {
        found.face = startSearch.face;
    }

    vec3 dir = point - innerPoint;

    FaceIndex bestFace = found.face;
    while (bestFace != invalidFace)
    {
        bestFace = invalidFace;
        const Face& cFace = mesh.face(found.face);
        auto maxDot = dot(cFace.normal, dir);
        EdgeIndex eIdx = mesh.face(found.face).outer;
        do
        {
            auto twin = mesh.edge(eIdx).twin;
            auto face = mesh.edge(twin).face;
            auto dotp = dot(mesh.face(face).normal, dir);
            if (dotp > maxDot)
            {
                maxDot = dotp;
                bestFace = face;
            }
            eIdx = mesh.edge(eIdx).next;
        } while (eIdx != mesh.face(found.face).outer);
        if (bestFace != invalidFace)
        {
            found.face = bestFace;
        }
    }

    auto vertexSqDist = Constant<float>::maximum;
    auto edgeSqDist = vertexSqDist;

    auto faceSqDist = std::abs(signedDistance(mesh.face(found.face), point));
    faceSqDist *= faceSqDist;
    // The above is only valid if the vertex is directly above the face (we
    // simply found the squared distance from the point to the *plane* of the
    // face), and so we check this precondition -- otherwise faceSqDist is
    // exactly as large as the edgeSqDist.

    bool isFaceFeature = false;
    if (mesh.isValidFace(found.face))
    {
        isFaceFeature = true;
        auto currFace = mesh.face(found.face);
        auto startEdge = currFace.outer;

        auto currEdge = startEdge;
        do
        {
            vec3 p0 = mesh.vertex(mesh.edge(currEdge).origin).pos;
            currEdge = mesh.edge(currEdge).next;
            vec3 p1 = mesh.vertex(mesh.edge(currEdge).origin).pos;
            if (dot(cross(currFace.normal, p1-p0), point-p0) < 0.)
            {
                isFaceFeature = false;
                break;
            }
        } while (currEdge != startEdge);
    }

    found.edge = getCloserEdge(point, mesh, found.face, edgeSqDist);
    found.vertex = getCloserVertex(point, mesh, found.edge, vertexSqDist);

    if (!isFaceFeature) faceSqDist = edgeSqDist;

    if (vertexSqDist <= edgeSqDist && vertexSqDist <= faceSqDist)
    {
        found.type = FeatureType::Vertex;
    }
    else if (edgeSqDist <= vertexSqDist && edgeSqDist <= faceSqDist)
    {
        found.type = FeatureType::Edge;
    }
    else
    {
        found.type = FeatureType::Face;
    }
    return found;
}

} // namespace kraken::geometry
