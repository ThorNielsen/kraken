#include "kraken/geometry/primitives.hpp"
#include <cstddef>
#include <kraken/geometry/distance.hpp>
#include <kraken/geometry/intersection.hpp>

namespace kraken::geometry
{

using namespace ::kraken::math;

template <typename Prec>
Prec squaredDistance(Triangle3d<Prec> a, Triangle3d<Prec> b) noexcept
{
    const auto* aVerts = reinterpret_cast<const Vector3<Prec>*>(&a);
    const auto* bVerts = reinterpret_cast<const Vector3<Prec>*>(&b);
    if (convexIntersects(aVerts, 3, bVerts, 3))
    {
        return Prec(0);
    }

    auto closest = closestPointsOnWires(aVerts, 3, bVerts, 3, true, true);
    auto bestDist = squaredLength(closest.first - closest.second);

    for (size_t i = 0; i < 3; ++i)
    {
        auto pt = closestPoint(a, bVerts[i]);
        auto d = squaredLength(pt-bVerts[i]);
        if (d < bestDist)
        {
            bestDist = d;
            closest = {pt, bVerts[i]};
        }
        pt = closestPoint(b, aVerts[i]);
        d = squaredLength(pt-aVerts[i]);
        if (d < bestDist)
        {
            bestDist = d;
            closest = {aVerts[i], pt};
        }
    }
    return squaredLength(closest.first-closest.second);
}

template float squaredDistance(Triangle3d<float> a, Triangle3d<float> b) noexcept;
template double squaredDistance(Triangle3d<double> a, Triangle3d<double> b) noexcept;
template long double squaredDistance(Triangle3d<long double> a, Triangle3d<long double> b) noexcept;

} // namespace kraken::geometry
