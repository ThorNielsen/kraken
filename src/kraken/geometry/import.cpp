#include "kraken/geometry/import.hpp"

#include "kraken/geometry/primitives.hpp"
#include "kraken/io/data/mesh.hpp"
#include "kraken/io/formats/ply.hpp"
#include "kraken/io/read/ply.hpp"
#include "kraken/io/read/ply/parsing.hpp"
#include "kraken/utility/memory.hpp"
#include "kraken/utility/range.hpp"

#include <iostream>

namespace kraken::geometry
{

bool readFaces(io::DataSource& source,
               const io::formats::ply::Element& elem,
               std::size_t faceCount,
               io::formats::ply::StoredFormat format,
               geometry::Mesh& meshOut,
               std::size_t vertexIndexOffset)
{
    namespace ply = io::read::ply;
    bool isBinary = format != io::formats::ply::StoredFormat::Ascii;
    // Determine where the vertex indices are located
    std::size_t vertexIndexLocation = elem.properties.size();
    for (auto propIndex : range(elem.properties.size()))
    {
        if ((elem.properties[propIndex].name == "vertex_indices"
             || elem.properties[propIndex].name == "vertex_index")
            && elem.properties[propIndex].isList())
        {
            vertexIndexLocation = propIndex;
            break;
        }
    }
    if (elem.properties.size() == 1
        && elem.properties[0].isList())
    {
        vertexIndexLocation = 0; // Probably just weirdly named.
    }
    // No indices found.
    if (vertexIndexLocation >= elem.properties.size()) return false;
    const auto& indexProp = elem.properties[vertexIndexLocation];

    auto endian = getEndian(format);

    auto listIndexByteSize = byteSize(*indexProp.listIndexType) * isBinary;
    auto indexByteSize = byteSize(indexProp.type) * isBinary;

    std::vector<geometry::VertexIndex> edgeEndpoints;
    std::vector<U32> vertexOutgoingCount(meshOut.vertices()-vertexIndexOffset, 0);
    U64 totalOutgoing = 0;

    auto edgeOffset = meshOut.edges();

    for ([[maybe_unused]] auto faceIndex : range(faceCount))
    {
        for (auto propIdx : range(elem.properties.size()))
        {
            const auto& property = elem.properties[propIdx];
            if (propIdx != vertexIndexLocation)
            {
                if (endian) ply::skipBinaryProperty(source, property, *endian);
                else ply::skipPlainProperty(source, property);
                continue;
            }

            // Index count is assumed to be an unsigned integer since non-
            // negative counts are meaningless.
            U32 indexCount = 0;
            if (!ply::readInteger(indexCount, source, listIndexByteSize, endian))
            {
                return false;
            }

            // If there are fewer than two indices, the face is necessarily
            // degenerate, and so we skip it.
            if (indexCount < 3)
            {
                continue;
            }

            // Read the polygon.
            auto meshFaceIndex = meshOut.createFace();
            auto& face = meshOut.face(meshFaceIndex);

            auto firstMeshEdgeIndex = geometry::invalidEdge;
            auto prevMeshEdgeIndex = geometry::invalidEdge;

            // Indexes into the PLY file and NOT the mesh.
            U32 firstPLYVertexIndex = 0;

            if (!ply::readInteger(firstPLYVertexIndex, source, indexByteSize, endian))
            {
                return false;
            }

            auto prevPLYVertexIndex = firstPLYVertexIndex;

            for (auto j : range(indexCount))
            {
                auto currPLYVertexIndex = firstPLYVertexIndex;
                if (j+1 < indexCount)
                {
                    if (!ply::readInteger(currPLYVertexIndex, source, indexByteSize, endian))
                    {
                        return false;
                    }
                }
                // Otherwise the current index is correct -- we want to close
                // the edge loop.


                auto currEdgeIndex = meshOut.createEdge();
                auto& currEdge = meshOut.edge(currEdgeIndex);
                currEdge.origin = prevPLYVertexIndex + vertexIndexOffset;
                edgeEndpoints.push_back(currPLYVertexIndex + vertexIndexOffset);
                currEdge.prev = prevMeshEdgeIndex;
                currEdge.face = meshFaceIndex;

                ++vertexOutgoingCount.at(currEdge.origin);
                ++totalOutgoing;

                if (firstMeshEdgeIndex == geometry::invalidEdge)
                {
                    firstMeshEdgeIndex = currEdgeIndex;
                }
                else
                {
                    meshOut.edge(currEdge.prev).next = currEdgeIndex;
                }
                prevMeshEdgeIndex = currEdgeIndex;

                prevPLYVertexIndex = currPLYVertexIndex;
            }

            face.outer = prevMeshEdgeIndex;
            meshOut.edge(firstMeshEdgeIndex).prev = prevMeshEdgeIndex;
            meshOut.edge(prevMeshEdgeIndex).next = firstMeshEdgeIndex;
        }
    }
    if (totalOutgoing >= std::numeric_limits<geometry::EdgeIndex>::max())
    {
        return false;
    }

    // We want to build a list of outgoing edges for each vertex. These can be
    // compactly stored in a single array where the "edge" property of each
    // vertex determine the index of where the list is stored. There is no
    // terminator, but one can check for a given edge in the list whether its
    // origin is the same as the previous one, thus a terminator is unnecessary.
    geometry::EdgeIndex pos = 0;
    for (geometry::VertexIndex vertex = 0; vertex < meshOut.vertices(); ++vertex)
    {
        meshOut.vertex(vertex).edge = pos;
        pos += vertexOutgoingCount[vertex];
    }

    std::vector<geometry::EdgeIndex> outgoingEdges(totalOutgoing,
                                                   geometry::invalidEdge);
    std::vector<U32> placementOffsets(meshOut.vertices(), 0);

    // Place each edge in the "outgoingEdge" array so we can easily search all
    // outgoing edges of a particular vertex.
    for (auto edge : range(edgeOffset, meshOut.edges()))
    {
        auto& currEdge = meshOut.edge(edge);
        size_t placeAt = meshOut.vertex(currEdge.origin).edge
                       + placementOffsets[currEdge.origin]++;
        outgoingEdges[placeAt] = edge;
    }

    // Now we actually connect the edges with their twins, as we created them
    // earlier. We make use of the structures created above.
    for (auto edge : range(edgeOffset, meshOut.edges()))
    {
        auto& currEdge = meshOut.edge(edge);
        if (currEdge.twin != geometry::invalidEdge) continue;
        auto targetVertexIndex = edgeEndpoints[edge];
        auto twin = geometry::invalidEdge;

        auto searchPos = meshOut.vertex(targetVertexIndex).edge;
        while (searchPos < outgoingEdges.size() &&
               meshOut.edge(outgoingEdges[searchPos]).origin == targetVertexIndex)
        {
            if (edgeEndpoints[outgoingEdges[searchPos]] == currEdge.origin)
            {
                twin = outgoingEdges[searchPos];
                break;
            }
            ++searchPos;
        }

        if (twin == geometry::invalidEdge) continue; // No twin, then.

        auto& currTwin = meshOut.edge(twin);
        if (currTwin.twin != geometry::invalidEdge)
        {
            // Non-manifold mesh. We should probably do something to fix it
            // (such as splitting the mesh), but that is a little difficult
            // (which face does not belong?), and we can't do it now without
            // destroying the acceleration structures constructed above. Thus,
            // we should probably make a note of all those edges being non-
            // manifold so we can resolve the problems afterwards. The
            // complexity there probably don't matter much, as the mesh
            // shouldn't really be non-manifold at all, so it is probably only a
            // few edges anyways.
            continue;
        }
        currEdge.twin = twin;
        currTwin.twin = edge;
    }

    for (geometry::EdgeIndex edge = 0; edge < meshOut.edges(); ++edge)
    {
        meshOut.vertex(meshOut.edge(edge).origin).edge = edge;
    }

    return true;
}

bool importPLY(std::istream& input, Mesh& meshOut)
{
    io::DataSource dataSource(input);
    std::vector<Vector3<float>> vertexData;

    kraken::ExternalMemoryAllocation vertAlloc([&vertexData](std::size_t count, std::size_t)
                                               {
                                                   vertexData.resize(count);
                                                   return vertexData.data();
                                               });

    // First we ensure there are no "holes" ion the vertex array so that the
    // offsetting is valid (and thus that we are sure they will be allocated
    // sequentially).
    meshOut.compactify();
    auto vertexIndexOffset = meshOut.vertices();
    auto faceIndexOffset = meshOut.faces();

    namespace ply = io::formats::ply;

    auto faceParser = [&meshOut, vertexIndexOffset](io::DataSource& source,
                                                    const ply::Header& header,
                                                    std::size_t elemIndex)
    {
        return readFaces(source,
                         header.elements[elemIndex].element,
                         header.elements[elemIndex].count,
                         header.format,
                         meshOut,
                         vertexIndexOffset);
    };

    io::VertexLayout desiredLayout;
    desiredLayout.attributes.push_back
    ({
        .dataType = kraken::io::PrimitiveDataType::Float,
        .usage = kraken::io::VertexAttributeUsageType::Position,
        .componentCount = 3,
        .setIndex = 0,
    });

    kraken::io::VertexLayout resultingLayout;
    [[maybe_unused]] std::size_t vertexCount;
    auto vertexParser = ply::createRelocatingVertexParser(vertAlloc, desiredLayout, resultingLayout, vertexCount);

    auto header = ply::readHeader(dataSource);
    if (!header) return false;

    auto vertexElem = header->getElementByName(ply::getVertexElementName());
    if (!vertexElem) return false;

    for (auto i : range(vertexElem->count))
    {
        if (meshOut.createVertex() != vertexIndexOffset + i)
        {
            // Bad invariant.
            throw std::logic_error("Vertices created didn't have expected invariant.");
        }
    }

    if (!ply::readBody(dataSource,
                       *header,
                       {
                           {ply::getVertexElementName(), vertexParser},
                           {ply::getFaceElementName(), faceParser},
                       }))
    {
        return false;
    }

    for (auto i : range(vertexElem->count))
    {
        meshOut.vertexAt(vertexIndexOffset+i).pos = vertexData[i];
    }

    for (auto faceIdx : range(faceIndexOffset, meshOut.faces()))
    {
        meshOut.updateFace(faceIdx);
    }

    return true;
}

std::optional<Mesh> importPLY(std::istream& input)
{
    std::optional<Mesh> mesh;
    mesh.emplace();
    if (!importPLY(input, mesh.value())) mesh = std::nullopt;
    return mesh;
}

} // namespace kraken::geometry
