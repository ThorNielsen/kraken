#include "kraken/geometry/primitives.hpp"
#include "kraken/math/constants.hpp"
#include "kraken/math/matrix.hpp"
#include <array>
#include <cstddef>
#include <kraken/geometry/convexboolean.hpp>
#include <kraken/geometry/intersection.hpp>
#include <kraken/geometry/primitivequeries.hpp>

#include <utility>

namespace kraken::geometry
{

using namespace ::kraken::math;

template <typename Prec>
bool intersect(const Plane<Prec>& firstPlane,
               const Plane<Prec>& secondPlane,
               Line3d<Prec>& resultOut,
               Prec epsilon) noexcept
{
    auto lineDir = cross(firstPlane.normal, secondPlane.normal);
    if (squaredLength(lineDir) < epsilon*epsilon) return false;
    lineDir = normalise(lineDir);
    const Line3d<Prec> l1{firstPlane.origin, cross(lineDir, firstPlane.normal)};
    const Line3d<Prec> l2{secondPlane.origin, cross(lineDir, secondPlane.normal)};
    const auto [par1, par2] = closestPointParameters(l1, l2, epsilon);
    if (std::isinf(par1)) return false;
    // Technically, we should maybe average the two points... But that's some
    // more computation, and possibly overkill.
    resultOut = {l1.origin + par1 * l1.direction, lineDir};
    return true;
}

template bool intersect(const Plane<float>&, const Plane<float>&, Line3d<float>&, float) noexcept;
template bool intersect(const Plane<double>&, const Plane<double>&, Line3d<double>&, double) noexcept;
template bool intersect(const Plane<long double>&, const Plane<long double>&, Line3d<long double>&, long double) noexcept;

template <typename Prec>
bool intersectUnnormalised(const Plane<Prec>& firstPlane,
                           const Plane<Prec>& secondPlane,
                           Line3d<Prec>& resultOut,
                           Prec epsilon) noexcept
{
    auto lineDir = cross(firstPlane.normal, secondPlane.normal);
    if (squaredLength(lineDir) < epsilon*epsilon) return false;
    lineDir = normalise(lineDir);
    // Expected length of l1: ||lineDir||·||firstNormal||²·||secondNormal||.
    // This implies that if the plane normals are small, we should weigh the
    // resulting epsilon... To incorporate both planes, we just multiply the
    // squares of the normal lenghts together.
    auto modEps = epsilon * squaredLength(firstPlane.normal)
                          * squaredLength(secondPlane.normal);
    const Line3d<Prec> l1{firstPlane.origin, cross(lineDir, firstPlane.normal)};
    const Line3d<Prec> l2{secondPlane.origin, cross(lineDir, secondPlane.normal)};
    const auto [par1, par2] = closestPointParametersUnnormalised(l1, l2, modEps);
    if (std::isinf(par1)) return false;
    // Technically, we should maybe average the two points... But that's some
    // more computation, and possibly overkill.
    resultOut = {l1.origin + par1 * l1.direction, lineDir};
    return true;
}

template bool intersectUnnormalised(const Plane<float>&, const Plane<float>&, Line3d<float>&, float) noexcept;
template bool intersectUnnormalised(const Plane<double>&, const Plane<double>&, Line3d<double>&, double) noexcept;
template bool intersectUnnormalised(const Plane<long double>&, const Plane<long double>&, Line3d<long double>&, long double) noexcept;

template <typename Prec>
void trimByTriangle(LineSegment3d<Prec>& segment,
                    const Triangle3d<Prec>& tri,
                    Vector3<Prec> normal,
                    Prec epsilon)
{
    Plane<Prec> trimPlane;
    trimPlane.origin = tri.a;
    trimPlane.normal = cross(normal, tri.b - tri.a);
    trimSegment(segment, trimPlane, epsilon);
    trimPlane.origin = tri.b;
    trimPlane.normal = cross(normal, tri.c - tri.b);
    trimSegment(segment, trimPlane, epsilon);
    trimPlane.origin = tri.c;
    trimPlane.normal = cross(normal, tri.a - tri.c);
    trimSegment(segment, trimPlane, epsilon);
}

template <typename Prec>
LineSegment3d<Prec> infiniteSegment(Line3d<Prec> line)
{
    LineSegment3d<Prec> seg;
    seg.origin = line.origin;
    seg.direction = line.direction;
    seg.minPar = -math::Constant<Prec>::maximum;
    seg.maxPar = math::Constant<Prec>::maximum;
    return seg;
}

template <typename Prec>
std::pair<std::array<Vector3<Prec>, 6>, size_t>
intersect(Triangle3d<Prec> triA, Triangle3d<Prec> triB, Prec epsilon)
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
    std::array<Vector3<Prec>, 6> result;
    auto na = cross(triA.b - triA.a, triA.c - triA.a);
    auto nb = cross(triB.b - triB.a, triB.c - triB.a);
    if (dot(na, nb) < 0.)
    {
        nb = -nb;
        std::swap(triB.b, triB.c);
    }
    auto sqEps = epsilon * epsilon;

    auto naSq = squaredLength(na);
    auto nbSq = squaredLength(nb);
    auto aDegen = naSq < sqEps;
    auto bDegen = nbSq < sqEps;
    if (aDegen)
    {
        auto linA = collapseToLine(triA);
        if (bDegen)
        {
            auto linB = collapseToLine(triB);
            auto cp = closestPointsUnnormalised(linA, Prec(0), Prec(1),
                                                linB, Prec(0), Prec(1));
            if (squaredLength(cp.first - cp.second) < sqEps)
            {
                result[0] = Prec(.5) * (cp.first + cp.second);
                return {result, 1};
            }
            return {result, 0};
        }
        if (intersect(triB, linA, result[0], epsilon)) return {result, 1};
        // Otherwise, linA and triB are parallel, with two possible cases:
        // 1. They are disjoint, far apart.
        // 2. They are very close together, and should be intersected.
        Plane<Prec> planeB;
        planeB.origin = triB.a;
        planeB.normal = nb;

        auto seg = infiniteSegment(linA);

        auto pt = closestPoint(planeB, linA.origin);
        if (squaredLength(pt-linA.origin) >= sqEps) return {result, 0};
        trimByTriangle(seg, triB, nb, epsilon);
        if (seg.minPar > seg.maxPar) return {result, 0};
        result[0] = seg.origin + seg.minPar * seg.direction;
        if (seg.minPar < seg.maxPar)
        {
            result[1] = seg.origin + seg.maxPar * seg.direction;
            return {result, 2};
        }
        return {result, 1};
    }
    else if (bDegen)
    {
        // A is not degenerate. For simplicity, we just swap the role of A and B
        // in order to not have to write the above thing twice.
        return intersect(triB, triA, epsilon);
    }

    // Both triangles are here non-degenerate. We need only handle three cases:
    // 1. Non-parallel, giving a possible line intersection.
    // 2. Parallel, in different planes.
    // 3. Parallel, within the same plane.

    // For numerical stability, we perform a necessary normal normalisation.
    na = normalise(na);
    nb = normalise(nb);

    Line3d<Prec> lineOfIntersection;
    Plane<Prec> planA, planB;
    planA.origin = triA.a;
    planA.normal = na;
    planB.origin = triB.a;
    planB.normal = nb;
    if (intersectUnnormalised(planA, planB, lineOfIntersection, epsilon))
    {
        // Case 1: Non-parallel planes.
        auto seg = infiniteSegment(lineOfIntersection);
        trimByTriangle(seg, triA, na, epsilon);
        trimByTriangle(seg, triB, nb, epsilon);
        if (seg.minPar > seg.maxPar) return {result, 0};
        result[0] = seg.origin + seg.minPar * seg.direction;
        if (seg.minPar < seg.maxPar)
        {
            result[1] = seg.origin + seg.maxPar * seg.direction;
            return {result, 2};
        }
        return {result, 1};
    }

    // Otherwise, they are parallel. So we can now check the distance:
    if (dot(planA.origin-planB.origin, normalise(na)) < epsilon)
    {
        // Case 2: Parallel, but with different planes.
        return {result, 0};
    }

    // Case 3: Parallel, within the same plane.
    // For this, we need a projector onto their common plane, intersect the
    // resulting 2d polygons, and then return their reprojected points.

    Vector3<Prec> localX, localY, localZ, localOrigin;

    // Figure out best local directions by choosing local coordinates from the
    // largest-area triangle.
    if (naSq > nbSq)
    {
        localOrigin = triA.a;
        localX = triA.b - triA.a;
        localY = triA.c - triA.a;
        localZ = na;
    }
    else
    {
        localOrigin = triB.a;
        localX = triB.b - triB.a;
        localY = triB.c - triB.a;
        localZ = nb;
    }
    localX = normalise(localX);
    localZ = normalise(localZ);
    localY = normalise(cross(localZ, localX));
    math::Matrix<2, 3, Prec> projector;
    projector.setRow(0, localX);
    projector.setRow(1, localY);

    Vector2<Prec> polygonVertices[6];
    polygonVertices[0] = projector * (triA.a-localOrigin);
    polygonVertices[1] = projector * (triA.b-localOrigin);
    polygonVertices[2] = projector * (triA.c-localOrigin);
    polygonVertices[3] = projector * (triB.a-localOrigin);
    polygonVertices[4] = projector * (triB.b-localOrigin);
    polygonVertices[5] = projector * (triB.c-localOrigin);

    Vector2<Prec> result2d[6];
    const auto* r2dEnd = performBooleanOperation(polygonVertices,
                                                 3,
                                                 polygonVertices+3,
                                                 3,
                                                 true,
                                                 result2d,
                                                 6,
                                                 nullptr,
                                                 epsilon);

    auto resultCount = static_cast<size_t>(r2dEnd-result2d);
    for (size_t i = 0; i < resultCount; ++i)
    {
        result[i] = localOrigin
                  + localX * result2d[i].x
                  + localY * result2d[i].y;
    }

    return {result, resultCount};
#pragma GCC diagnostic pop
}

template std::pair<std::array<Vector3<float>, 6>, size_t>
intersect(Triangle3d<float> triA, Triangle3d<float> triB, float epsilon);

template std::pair<std::array<Vector3<double>, 6>, size_t>
intersect(Triangle3d<double> triA, Triangle3d<double> triB, double epsilon);

template std::pair<std::array<Vector3<long double>, 6>, size_t>
intersect(Triangle3d<long double> triA, Triangle3d<long double> triB, long double epsilon);

} // namespace kraken::geometry
