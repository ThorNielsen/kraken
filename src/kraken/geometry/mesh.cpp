#include "kraken/math/matrix.hpp"
#include <cstddef>
#include <kraken/geometry/mesh.hpp>

#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

namespace kraken::geometry
{

using namespace ::kraken::math;

// This uses Newell's method to calculate the normal properly.
void Mesh::updateFace(FaceIndex i)
{
    Face& f = face(i);
    f.center = {0.f, 0.f, 0.f};
    f.normal = {0.f, 0.f, 0.f};
    EdgeIndex edgeIdx = f.outer;
    vec3 prevPos = vertex(edge(edge(edgeIdx).prev).origin).pos;
    size_t cnt = 0;
    do
    {
        auto& cEdge = edge(edgeIdx);
        vec3 currPos = vertex(cEdge.origin).pos;
        f.center += currPos;
        f.normal.x += (prevPos.y - currPos.y) * (prevPos.z + currPos.z);
        f.normal.y += (prevPos.z - currPos.z) * (prevPos.x + currPos.x);
        f.normal.z += (prevPos.x - currPos.x) * (prevPos.y + currPos.y);

        prevPos = currPos;
        edgeIdx = cEdge.next;
        ++cnt;
        // Beware: Hiodden limit on polygon sizes...
    } while (edgeIdx != f.outer && cnt < 10000);
    f.center *= 1.f / static_cast<float>(cnt);
    f.normal = normalise(f.normal);
}

void Mesh::dump() const
{
    std::cout << "=============\n";
    std::cout << "= Mesh dump =\n";
    std::cout << "=============\n";
    std::cout << "Vertex count: " << vertices() << std::endl;
    std::cout << "Edge count: " << edges() << std::endl;
    std::cout << "Face count: " << faces() << std::endl;
    for (auto v = vbegin(); v != vend(); ++v)
    {
        std::cout << v.index() << ":" << *v << std::endl;
    }
    for (auto e = ebegin(); e != eend(); ++e)
    {
        std::cout << e.index() << ":" << *e << std::endl;
    }
    for (auto f = fbegin(); f != fend(); ++f)
    {
        std::cout << f.index() << ":" << *f << std::endl;
    }
    std::cout << "\n" << std::endl;
}

void Mesh::checkRefs() const
{
    std::cout << "\033[1;31m";
    for (auto v = vbegin(); v != vend(); ++v)
    {
        std::string msg = "Vertex " + std::to_string(v.index()) + " has invalid";
        auto osz = msg.size();
        if (!isValidEdge(v->edge))
            msg += " edge " + std::to_string(v->edge);
        if (isValidEdge(v->edge) &&
            edge(v->edge).origin != v.index())
            msg += " origin_edge " + std::to_string(v->edge);
        if (msg.size() != osz)
            std::cout << msg << std::endl;
    }
    std::cout << "\033[1;32m";
    for (auto e = ebegin(); e != eend(); ++e)
    {
        std::string msg = "Edge " + std::to_string(e.index()) + " has invalid";
        auto osz = msg.size();
        if (!isValidVertex(e->origin))
            msg += " origin " + std::to_string(e->origin);
        if (!isValidEdge(e->twin))
            msg += " twin " + std::to_string(e->twin);
        if (!isValidEdge(e->prev))
            msg += " prev " + std::to_string(e->prev);
        if (!isValidEdge(e->next))
            msg += " next " + std::to_string(e->next);
        if (!isValidFace(e->face))
            msg += " face " + std::to_string(e->face);
        if (msg.size() != osz)
            std::cout << msg << std::endl;
    }
    std::cout << "\033[1;33m";
    for (auto f = fbegin(); f != fend(); ++f)
    {
        std::string msg = "Face " + std::to_string(f.index()) + " has invalid";
        auto osz = msg.size();
        if (!isValidEdge(f->outer))
            msg += " outer " + std::to_string(f->outer);
        if (msg.size() != osz)
            std::cout << msg << std::endl;
    }
    std::cout << "\033[0m" << std::flush;
}

size_t vertexEdgeCount(VertexIndex v, const Mesh& mesh)
{
    size_t c = 0;
    const Vertex& vert = mesh.vertexAt(v);
    EdgeIndex cEdge = vert.edge;
    do
    {
        ++c;
        cEdge = mesh.edgeAt(mesh.edgeAt(cEdge).twin).next;
    } while (cEdge != vert.edge && c < mesh.edges());
    if (c == mesh.edges())
    {
        throw std::logic_error("Bad mesh detected at vertex "
                               + std::to_string(v) + ".");
    }
    size_t c2 = 0;
    for (auto e = mesh.ebegin(); e != mesh.eend(); ++e)
    {
        if (e->origin == v) ++c2;
    }
    if (c != c2)
    {
        throw std::logic_error("Connectivity is definitely wrong at vertex "
                               + std::to_string(v) + " ("
                               + std::to_string(c) + " vs. "
                               + std::to_string(c2) + ").");
    }
    return c;
}

size_t faceEdgeCount(FaceIndex f, const Mesh& mesh)
{
    auto face = mesh.face(f);
    auto cEdge = face.outer;
    size_t c = 0;
    do
    {
        cEdge = mesh.edge(cEdge).next;
        ++c;
    } while (cEdge != face.outer && c < mesh.edges());
    if (c == mesh.edges())
    {
        throw std::logic_error("Bad mesh detected at face "
                               + std::to_string(f) + ".");
    }
    size_t c2 = 0;
    for (auto e = mesh.ebegin(); e != mesh.eend(); ++e)
    {
        if (e->face == f) ++c2;
    }
    if (c != c2)
    {
        throw std::logic_error("Connectivity is definitely wrong at face "
                               + std::to_string(f) + " ("
                               + std::to_string(c) + " vs. "
                               + std::to_string(c2) + ").");
    }
    return c;
}

void Mesh::checkTopology() const
{
    try
    {
        std::cerr << "\033[1;34m";
        for (auto v = vbegin(); v != vend(); ++v)
        {
            auto ec = vertexEdgeCount(v.index(), *this);
            if (ec < 3)
            {
                std::cerr << "Vertex " << v.index()
                          << " has " << ec << " edges.\n";
            }
        }
        std::cerr << "\033[1;35m";
        for (auto e = ebegin(); e != eend(); ++e)
        {
            if (e->face == edge(e->twin).face)
            {
                std::cerr << "Edge " << e.index()
                          << " and its twin are in the same face.\n";
            }
        }
        std::cerr << "\033[1;36m";
        for (auto f = fbegin(); f != fend(); ++f)
        {
            auto ec = faceEdgeCount(f.index(), *this);
            if (ec < 3)
            {
                std::cerr << "Face " << f.index()
                          << " has " << ec << " edges.\n";
            }
        }
    }
    catch(...)
    {
        std::cerr << "\033[0m" << std::flush;
        dump();
        throw;
    }
    std::cerr << "\033[0m" << std::flush;
}

template <typename Index>
Index getDistance(Index index,
                  const std::vector<std::pair<Index, Index>>& moveBack)
{
    auto it = std::upper_bound(moveBack.begin(), moveBack.end(),
                               std::pair<Index, Index>{index, 0},
                               [](const std::pair<Index, Index>& a,
                                  const std::pair<Index, Index>& b)
                               { return a.first < b.first; });
    if (it == moveBack.end()) return moveBack.back().second;
    return (--it)->second;
}

template <typename Index, typename It, typename Cont>
std::vector<std::pair<Index, Index>> moveBack(It begin, It end, Cont& cont)
{
    std::vector<std::pair<Index, Index>> moved;
    moved.push_back({0,0});
    Index nextPos = 0;
    Index moveBack = 0;
    for (auto it = begin; it != end; ++it, ++nextPos)
    {
        if (it.index() == nextPos) continue;
        if (it.index() - nextPos != moveBack)
        {
            moveBack = it.index() - nextPos;
            moved.push_back({it.index(), moveBack});
        }
        cont[nextPos] = *it;
    }
    cont.resize(nextPos);
    return moved;
}

void Mesh::compactify()
{
    if (vertices() < m_verts.size())
    {
        auto vertexMove = moveBack<VertexIndex>(vbegin(), vend(), m_verts);
        for (auto it = ebegin(); it != eend(); ++it)
        {
            it->origin -= getDistance(it->origin, vertexMove);
        }
    }
    if (edges() < m_edges.size())
    {
        auto edgeMove = moveBack<EdgeIndex>(ebegin(), eend(), m_edges);
        for (auto it = vbegin(); it != vend(); ++it)
        {
            it->edge -= getDistance(it->edge, edgeMove);
        }
        for (auto it = ebegin(); it != eend(); ++it)
        {
            it->twin -= getDistance(it->twin, edgeMove);
            it->prev -= getDistance(it->prev, edgeMove);
            it->next -= getDistance(it->next, edgeMove);
        }
        for (auto it = fbegin(); it != fend(); ++it)
        {
            it->outer -= getDistance(it->outer, edgeMove);
        }
    }
    if (faces() < m_faces.size())
    {
        auto faceMove = moveBack<FaceIndex>(fbegin(), fend(), m_faces);
        for (auto it = ebegin(); it != eend(); ++it)
        {
            it->face -= getDistance(it->face, faceMove);
        }
    }
}

void Mesh::clear()
{
    m_verts.clear();
    m_edges.clear();
    m_faces.clear();
    vCount = 0;
    vNext = priv::deletedVertex;
    eCount = 0;
    eNext = priv::deletedEdge;
    fCount = 0;
    fNext = priv::deletedFace;
}

} // namespace kraken::geometry
