#include "kraken/geometry/primitives.hpp"
#include "kraken/math/constants.hpp"
#include <cstddef>
#include <kraken/geometry/intersection.hpp>
#include <kraken/geometry/primitivequeries.hpp>

#include <limits>
#include <stdexcept>
#include <utility>

namespace kraken::geometry
{

using namespace ::kraken::math;

template <typename Prec>
Vector3<Prec>
closestPoint(Triangle3d<Prec> triangle, Vector3<Prec> point) noexcept
{
    auto ab = triangle.b - triangle.a;
    auto ac = triangle.c - triangle.a;
    auto ap = point      - triangle.a;
    auto abap = dot(ab, ap);
    auto acap = dot(ac, ap);

    // Both |ab| and |ac| projects to some point before a. Therefore a is the
    // closest point.
    if (abap <= Prec(0) && acap <= Prec(0)) return triangle.a;

    auto bp = point - triangle.b;
    auto abbp = dot(ab, bp);
    auto acbp = dot(ac, bp);
    // Note: tn is equal to dot(c-b, p-b).
    auto tn = acbp - abbp;
    if (abbp >= Prec(0) && tn <= Prec(0)) return triangle.b;

    // Compute barycentric coordinates of p in t - see Real-Time Collision
    // Detection (Christer Ericson 2004), pp. 137+138 for derivation. Please
    // note that this is an optimised version exploiting the identity
    // (axb)*(cxd) = (a*c)(b*d)-(a*d)(b*c)
    // several times.
    auto baryC = abap * acbp - abbp * acap;
    // Note that baryC is the dot product of the normals of two triangles:
    // ABC and ABP. If P is to the same side of |AB| as C, then the cross
    // products have the same direction <=> dot product positive and otherwise
    // if P is on the other side of |AB| as C, then the dot product is negative
    // and we should project P onto |AB|. If P then doesn't project on this line
    // segment, is must lie outside of another edge.
    if (baryC <= Prec(0) && abap >= Prec(0) && abbp <= Prec(0))
    {
        return triangle.a + (abap / (abap - abbp)) * ab;
    }

    auto cp = point - triangle.c;
    auto abcp = dot(ab, cp);
    auto accp = dot(ac, cp);

    auto baryB = abcp * acap - abap * accp;
    if (baryB <= Prec(0) && acap >= Prec(0) && accp <= Prec(0))
    {
        return triangle.a + (acap / (acap - accp)) * ac;
    }

    auto td = abcp - accp;
    if (accp >= Prec(0) && td <= Prec(0)) return triangle.c;
    auto baryA = abbp * accp - abcp * acbp;
    if (baryA <= Prec(0) && tn >= Prec(0) && td >= Prec(0))
    {
        return triangle.b + tn / (tn + td) * (triangle.c - triangle.b);
    }

    // The closest point must be inside the triangle, so we use the computed
    // barycentric coordinates to find the correct position.
    auto invBaryTot = Prec(1) / (baryA + baryB + baryC);
    auto u = baryA * invBaryTot;
    auto v = baryB * invBaryTot;
    auto w = Prec(1) - u - v;
    return u * triangle.a + v * triangle.b + w * triangle.c;
}

template Vector3<float> closestPoint(Triangle3d<float>, Vector3<float>) noexcept;
template Vector3<double> closestPoint(Triangle3d<double>, Vector3<double>) noexcept;
template Vector3<long double> closestPoint(Triangle3d<long double>, Vector3<long double>) noexcept;

// Checks whether testPointA and testPointB are on opposite sides of the plane
// spanned by the first three arguments.
template <typename Prec>
bool onOppositePlaneSides(Vector3<Prec> planePointA,
                          Vector3<Prec> planePointB,
                          Vector3<Prec> planePointC,
                          Vector3<Prec> testPointA,
                          Vector3<Prec> testPointB) noexcept
{
    auto normal = cross(planePointB-planePointA, planePointC-planePointA);
    return (dot(testPointA-planePointA, normal) >= Prec(0))
        != (dot(testPointB-planePointA, normal) >= Prec(0));
}

template <typename Prec>
Vector3<Prec> closestPoint(Tetrahedron<Prec> tetra, Vector3<Prec> point) noexcept
{
    // Start off assuming p is inside and therefore the closest point.
    auto closest = point;
    auto minDist = std::numeric_limits<Prec>::max();
    // If p and d are on opposite sides of abc, the closest point might be on
    // abc.
    if (onOppositePlaneSides(tetra.a, tetra.b, tetra.c, point, tetra.d))
    {
        auto q = closestPoint(Triangle3d<Prec>{tetra.a, tetra.b, tetra.c}, point);
        auto sqlen = squaredLength(point-q);
        if (sqlen < minDist)
        {
            closest = q;
            minDist = sqlen;
        }
    }
    // But it can also be on one of the other faces.
    if (onOppositePlaneSides(tetra.a, tetra.b, tetra.d, point, tetra.c))
    {
        // So we test which face it is closest to.
        auto q = closestPoint(Triangle3d<Prec>{tetra.a, tetra.b, tetra.d}, point);
        auto sqlen = squaredLength(point-q);
        if (sqlen < minDist)
        {
            closest = q;
            minDist = sqlen;
        }
    }
    if (onOppositePlaneSides(tetra.a, tetra.c, tetra.d, point, tetra.b))
    {
        auto q = closestPoint(Triangle3d<Prec>{tetra.a, tetra.c, tetra.d}, point);
        auto sqlen = squaredLength(point-q);
        if (sqlen < minDist)
        {
            closest = q;
            minDist = sqlen;
        }
    }
    if (onOppositePlaneSides(tetra.b, tetra.c, tetra.d, point, tetra.a))
    {
        auto q = closestPoint(Triangle3d<Prec>{tetra.b, tetra.c, tetra.d}, point);
        auto sqlen = squaredLength(point-q);
        if (sqlen < minDist)
        {
            closest = q;
            minDist = sqlen;
        }
    }

    return closest;
}

template Vector3<float> closestPoint(Tetrahedron<float>, Vector3<float>) noexcept;
template Vector3<double> closestPoint(Tetrahedron<double>, Vector3<double>) noexcept;
template Vector3<long double> closestPoint(Tetrahedron<long double>, Vector3<long double>) noexcept;

template <typename Prec>
std::pair<Prec, Prec>
closestPointParameters(Line3d<Prec> a, Line3d<Prec> b, Prec epsilon) noexcept
{
    std::pair<Prec, Prec> v{Constant<Prec>::infinity, Constant<Prec>::infinity};
    auto arbr = dot(a.direction, b.direction);
    auto arbp = dot(a.direction, b.origin);
    auto apar = dot(a.origin, a.direction);

    auto denom = dot(b.direction, b.direction) - arbr * arbr;

    if (std::abs(denom) <= epsilon) return v;

    // This is a simplified result of a longer derivation.
    v.second = (dot(a.origin, b.direction)
                - dot(b.origin, b.direction)
                + arbr * (arbp - apar))
               / denom;
    v.first = arbp + arbr * v.second - apar;
    return v;
}

template std::pair<float, float> closestPointParameters(Line3d<float> a, Line3d<float> b, float epsilon) noexcept;
template std::pair<double, double> closestPointParameters(Line3d<double> a, Line3d<double> b, double epsilon) noexcept;
template std::pair<long double, long double> closestPointParameters(Line3d<long double> a, Line3d<long double> b, long double epsilon) noexcept;

template <typename Prec>
std::pair<Prec, Prec>
closestPointParametersUnnormalised(Line3d<Prec> a, Line3d<Prec> b, Prec epsilon) noexcept
{
    std::pair<Prec, Prec> v{Constant<Prec>::infinity, Constant<Prec>::infinity};
    auto arar = dot(a.direction, a.direction);
    auto arbr = dot(a.direction, b.direction);
    auto arbp = dot(a.direction, b.origin);
    auto apar = dot(a.origin, a.direction);

    auto denom = arar * dot(b.direction, b.direction) - arbr * arbr;

    if (std::abs(denom) <= epsilon) return v;

    // This is a simplified result of a longer derivation.
    v.second = ((arar * dot(a.origin, b.direction) - dot(b.origin, b.direction))
                + arbr * (arbp - apar))
               / denom;
    v.first = (arbp + arbr * v.second - apar) / arar;
    return v;
}

template std::pair<float, float> closestPointParametersUnnormalised(Line3d<float> a, Line3d<float> b, float epsilon) noexcept;
template std::pair<double, double> closestPointParametersUnnormalised(Line3d<double> a, Line3d<double> b, double epsilon) noexcept;
template std::pair<long double, long double> closestPointParametersUnnormalised(Line3d<long double> a, Line3d<long double> b, long double epsilon) noexcept;


template <typename Prec>
std::pair<Vector3<Prec>, Vector3<Prec>>
closestPointsOnWires(const Vector3<Prec>* pFirstVertices,
                     size_t firstVertexCount,
                     const Vector3<Prec>* pSecondVertices,
                     size_t secondVertexCount,
                     bool firstClosed,
                     bool secondClosed,
                     Prec epsilon)
{
    std::pair<Vector3<Prec>, Vector3<Prec>> closest{*pFirstVertices,
                                                    *pSecondVertices};
    Prec bestDist = math::Constant<Prec>::maximum;

    size_t secondStartAt = secondClosed ? secondVertexCount - 1 : 0;

    auto prevFirst = pFirstVertices[firstClosed ? firstVertexCount-1 : 0];
    for (size_t i = !firstClosed; i < firstVertexCount; ++i)
    {
        auto currFirst = pFirstVertices[i];
        Line3d<Prec> firstLine{prevFirst, currFirst-prevFirst};
        prevFirst = currFirst;

        auto prevSecond = pSecondVertices[secondStartAt];
        for (size_t j = !secondClosed; j < secondVertexCount; ++j)
        {
            auto currSecond = pSecondVertices[j];
            Line3d<Prec> secondLine{prevSecond, currSecond-prevSecond};
            prevSecond = currSecond;

            auto [pf, ps] = closestPointParametersUnnormalised(firstLine,
                                                               secondLine,
                                                               epsilon);
            pf = std::max(std::min(pf, Prec(1)), Prec(0));
            ps = std::max(std::min(ps, Prec(1)), Prec(0));
            auto vf = firstLine.origin + pf * firstLine.direction;
            auto vs = secondLine.origin + ps * secondLine.direction;
            auto d = squaredLength(vf-vs);
            if (d < bestDist)
            {
                bestDist = d;
                closest = {vf, vs};
            }
        }
    }
    return closest;
}

template
std::pair<Vector3<float>, Vector3<float>>
closestPointsOnWires(const Vector3<float>* pFirstVertices,
                     size_t firstVertexCount,
                     const Vector3<float>* pSecondVertices,
                     size_t secondVertexCount,
                     bool firstClosed,
                     bool secondClosed,
                     float epsilon);
template
std::pair<Vector3<double>, Vector3<double>>
closestPointsOnWires(const Vector3<double>* pFirstVertices,
                     size_t firstVertexCount,
                     const Vector3<double>* pSecondVertices,
                     size_t secondVertexCount,
                     bool firstClosed,
                     bool secondClosed,
                     double epsilon);
template
std::pair<Vector3<long double>, Vector3<long double>>
closestPointsOnWires(const Vector3<long double>* pFirstVertices,
                     size_t firstVertexCount,
                     const Vector3<long double>* pSecondVertices,
                     size_t secondVertexCount,
                     bool firstClosed,
                     bool secondClosed,
                     long double epsilon);

template <typename Prec>
std::pair<Vector3<Prec>, Vector3<Prec>>
closestPoints(Triangle3d<Prec> a, Triangle3d<Prec> b) // noexcept
{
    const auto* aVerts = reinterpret_cast<const Vector3<Prec>*>(&a);
    const auto* bVerts = reinterpret_cast<const Vector3<Prec>*>(&b);
    if (convexIntersects(aVerts, 3, bVerts, 3))
    {
        throw std::logic_error("Intersection, so not implemented.");
        return {{}, {}};
    }

    auto closest = closestPointsOnWires(aVerts, 3, bVerts, 3, true, true);
    auto bestDist = squaredLength(closest.first - closest.second);

    for (size_t i = 0; i < 3; ++i)
    {
        auto pt = closestPoint(a, bVerts[i]);
        auto d = squaredLength(pt-bVerts[i]);
        if (d < bestDist)
        {
            bestDist = d;
            closest = {pt, bVerts[i]};
        }
        pt = closestPoint(b, aVerts[i]);
        d = squaredLength(pt-aVerts[i]);
        if (d < bestDist)
        {
            bestDist = d;
            closest = {aVerts[i], pt};
        }
    }
    return closest;
}

template std::pair<Vector3<float>, Vector3<float>>
closestPoints(Triangle3d<float> a, Triangle3d<float> b);

template std::pair<Vector3<double>, Vector3<double>>
closestPoints(Triangle3d<double> a, Triangle3d<double> b);

template std::pair<Vector3<long double>, Vector3<long double>>
closestPoints(Triangle3d<long double> a, Triangle3d<long double> b);

} // namespace kraken::geometry
