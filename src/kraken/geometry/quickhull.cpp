#include "kraken/geometry/mesh.hpp"
#include "kraken/math/matrix.hpp"
#include "kraken/types.hpp"
#include <array>
#include <cstddef>
#include <cstdlib>
#include <kraken/geometry/quickhull.hpp>

#include <kraken/geometry/distance.hpp>
#include <kraken/geometry/primitives.hpp>
#include <kraken/math/comparison.hpp>
#include <kraken/math/constants.hpp>

#include <algorithm>
#include <map>
#include <queue>
#include <stack>
#include <stdexcept>
#include <string>
#include <vector>

namespace kraken::geometry
{

using namespace ::kraken::math;

namespace
{

inline vec3 getVertex(const vec3* vertices, size_t stride, size_t pos)
{
    return *reinterpret_cast<const vec3*>(reinterpret_cast<const U8*>(vertices)+stride*pos);
}

using FacePoints = std::map<FaceIndex, std::vector<vec3>>;

// Faces are as follows (CCW):
// ABC, BDC, DAC, ADB
struct QHTetrahedron
{
    U32 a; U32 b; U32 c; U32 d;
};

QHTetrahedron getInitialSimplex(const vec3* vertices,
                                size_t vertexCount,
                                size_t vertexStride,
                                float& epsilonOut)
{
    U32 xl = 0, xh = 0, yl = 0, yh = 0, zl = 0, zh = 0;
    for (U32 i = 0; i < vertexCount; ++i)
    {
        vec3 c = getVertex(vertices, vertexStride, i);
        if (getVertex(vertices, vertexStride, xl).x > c.x) xl = i;
        if (getVertex(vertices, vertexStride, xh).x < c.x) xh = i;
        if (getVertex(vertices, vertexStride, yl).y > c.y) yl = i;
        if (getVertex(vertices, vertexStride, yh).y < c.y) yh = i;
        if (getVertex(vertices, vertexStride, zl).z > c.z) zl = i;
        if (getVertex(vertices, vertexStride, zh).z < c.z) zh = i;
    }

    float xLen = squaredLength(getVertex(vertices, vertexStride, xh) - getVertex(vertices, vertexStride, xl));
    float yLen = squaredLength(getVertex(vertices, vertexStride, yh) - getVertex(vertices, vertexStride, yl));
    float zLen = squaredLength(getVertex(vertices, vertexStride, zh) - getVertex(vertices, vertexStride, zl));
    U32 pa = 0;
U32 pb = 0;
    if (xLen >= std::max({xLen, yLen, zLen}))
    {
        pa = xl;
        pb = xh;
    }
    if (yLen >= std::max({xLen, yLen, zLen}))
    {
        pa = yl;
        pb = yh;
    }
    if (zLen >= std::max({xLen, yLen, zLen}))
    {
        pa = zl;
        pb = zh;
    }

    if (pa == pb)
    {
        throw std::runtime_error("Point cloud degenerate (point).");
    }

    U32 pc = 0;
    float sqDist = 0.f;
    Line3d<float> l;
    l.origin = getVertex(vertices, vertexStride, pa);
    l.direction = getVertex(vertices, vertexStride, pb)-l.origin;
    for (U32 i : {xl, xh, yl, yh, zl, zh})
    {
        float cDist = squaredDistanceUnnormalised(l, getVertex(vertices, vertexStride, i));
        if (cDist >= sqDist)
        {
            sqDist = cDist;
            pc = i;
        }
    }

    if (zero(sqDist, 0.f))
    {
        throw std::runtime_error("Point cloud degenerate (line).");
    }

    float dist = 0.f;
    U32 pd = 0;
    Plane<float> p{getVertex(vertices, vertexStride, pa),
                   getVertex(vertices, vertexStride, pb),
                   getVertex(vertices, vertexStride, pc)};
    for (U32 i = 0; i < vertexCount; ++i)
    {
        float d = signedDistance(p, getVertex(vertices, vertexStride, i));
        if (std::abs(d) > std::abs(dist))
        {
            dist = d;
            pd = i;
        }
    }

    // Geometry is two-dimensional (planar).
    if (zero(dist, 0.f))
    {
        throw std::runtime_error("Point cloud degenerate (plane).");
    }

    if (dist >= 0.f)
    {
        std::swap(pa, pb);
    }

    auto vxl = getVertex(vertices, vertexStride, xl);
    auto vxh = getVertex(vertices, vertexStride, xh);
    auto vyl = getVertex(vertices, vertexStride, yl);
    auto vyh = getVertex(vertices, vertexStride, yh);
    auto vzl = getVertex(vertices, vertexStride, zl);
    auto vzh = getVertex(vertices, vertexStride, zh);

    epsilonOut = 3e-6f *
                 (std::max(std::abs(vxl.x), std::abs(vxh.x)) +
                  std::max(std::abs(vyl.y), std::abs(vyh.y)) +
                  std::max(std::abs(vzl.z), std::abs(vzh.z)));
    return {pa, pb, pc, pd};
}

// Faces are as follows (CCW):
// ABC, BDC, DAC, ADB
// 012, 132, 302, 031
Mesh buildInitialHull(const vec3* vertices, size_t vertexCount,
                      size_t vertexStride, float& epsilonOut)
{
    auto simplex = getInitialSimplex(vertices, vertexCount, vertexStride, epsilonOut);
    std::vector<vec3> verts = {getVertex(vertices, vertexStride, simplex.a),
                               getVertex(vertices, vertexStride, simplex.b),
                               getVertex(vertices, vertexStride, simplex.c),
                               getVertex(vertices, vertexStride, simplex.d)};
    std::vector<U16> indices = {0,1,2, 1,3,2, 3,0,2, 0,3,1};
    return Mesh::fromTriangleMesh(verts.data(), verts.size(), sizeof(vec3),
                                  indices.data(), indices.size());
}

vec3 getFarthestPoint(FaceIndex face, const Mesh& hull,
                      const std::vector<vec3>& facePoints)
{
    float maxDist = -Constant<float>::maximum;
    vec3 farthest{0.f, 0.f, 0.f};
    const auto& cFace = hull.faceAt(face);
    for (auto& vertex : facePoints)
    {
        float dist = signedDistance(cFace, vertex);
        if (dist > maxDist)
        {
            maxDist = dist;
            farthest = vertex;
        }
    }
    return farthest;
}

inline bool isVisible(const Face& f, vec3 point, float epsilon)
{
    return signedDistance(f, point) >= -epsilon * 10.f;
}

constexpr EdgeIndex visitMask = 0x7fffffffu;
constexpr EdgeIndex visitBit = 0x80000000u;

void setVisited(Vertex& v) { v.edge |= visitBit; }
void clearVisited(Vertex& v) { v.edge &= visitMask; }
bool visited(const Vertex& v)
{
    return (v.edge & visitBit) && v.edge < invalidEdge;
}
void setVisited(Edge& e) { e.twin |= visitBit; }
void clearVisited(Edge& e) { e.twin &= visitMask; }
bool visited(const Edge& e)
{
    return (e.twin & visitBit) && e.twin < invalidEdge;
}
void setVisited(Face& f) { f.outer |= visitBit; }
void clearVisited(Face& f) { f.outer &= visitMask; }
bool visited(const Face& f)
{
    return (f.outer & visitBit) && f.outer < invalidEdge;
}

bool isHorizonEdge(EdgeIndex edge, vec3 point, Mesh& hull, float epsilon)
{
    return isVisible(hull.faceAt(hull.edgeAt(edge).face), point, epsilon) !=
           isVisible(hull.faceAt(hull.edgeAt(hull.edgeAt(edge).twin
                                             & visitMask).face),
                     point,
                     epsilon);
}

EdgeIndex findHorizonEdge(vec3 point, EdgeIndex first,
                          Mesh& hull, float epsilon,
                          std::vector<VertexIndex>& visibleVerticesOut,
                          std::vector<EdgeIndex>& visibleEdgesOut)
{
    if (!isVisible(hull.faceAt(hull.edgeAt(first).face), point, epsilon))
    {
        throw std::logic_error("findHorizonEdge given non-visible edge-face.");
    }
    std::stack<EdgeIndex> unprocessed;
    unprocessed.push(first);
    EdgeIndex horizonEdge = invalidEdge;
    std::vector<EdgeIndex> visit;
    while (!unprocessed.empty())
    {
        auto currIdx = unprocessed.top();
        unprocessed.pop();
        Edge& curr = hull.edgeAt(currIdx);
        Vertex& vert = hull.vertexAt(curr.origin);
        if (!visited(vert))
        {
            visibleVerticesOut.push_back(curr.origin);
            setVisited(vert);
        }

        setVisited(curr);
        if (isHorizonEdge(currIdx, point, hull, epsilon))
        {
            // Make sure that we return the edge in the invisible face.
            if (isVisible(hull.faceAt(curr.face), point, epsilon))
            {
                visit.push_back(currIdx);
                horizonEdge = currIdx;
            }
            else
            {
                throw std::logic_error("Tried to return invisible edge.");
            }
        }
        else
        {
            visibleEdgesOut.push_back(currIdx);
        }
        Edge& next = hull.edgeAt(curr.next);
        if (!visited(next) && isVisible(hull.faceAt(next.face), point, epsilon))
        {
            unprocessed.push(curr.next);
        }
        Edge& twin = hull.edgeAt(curr.twin & visitMask);
        if (!visited(twin) && isVisible(hull.faceAt(twin.face), point, epsilon))
        {
            unprocessed.push(curr.twin & visitMask);
        }
    }
    if (horizonEdge == invalidEdge)
    {
        throw std::logic_error("Could not find any horizon edge.");
    }
    for (auto& e : visit)
    {
        clearVisited(hull.edgeAt(e));
    }
    return horizonEdge;
}

EdgeIndex nextHorizonEdge(EdgeIndex current, vec3 point,
                          Mesh& hull, float epsilon)
{
    size_t guard = 0;
    do
    {
        current = hull.edgeAt(hull.edgeAt(current).next).twin & visitMask;
    } while (!isHorizonEdge(current, point, hull, epsilon)
             && guard++ < hull.edges());
    if (guard > hull.edges())
    {
        throw std::logic_error("Horizon creation gives way too many edges.");
    }
    if (isVisible(hull.faceAt(hull.edgeAt(current).face), point, epsilon))
    {
        throw std::logic_error("Expected face to be invisible.");
    }
    return hull.edgeAt(current).twin & visitMask;
}

std::vector<FaceIndex> visibleFaces(FaceIndex curr, vec3 point,
                                    Mesh& hull, float epsilon)
{
    std::vector<FaceIndex> found;
    std::stack<FaceIndex> unprocessed;
    unprocessed.push(curr);
    while (!unprocessed.empty())
    {
        FaceIndex cIdx = unprocessed.top();
        unprocessed.pop();
        Face& cFace = hull.faceAt(cIdx);
        if (visited(cFace)) continue;
        setVisited(cFace);
        found.push_back(cIdx);
        auto edge = cFace.outer & visitMask;
        auto firstEdge = edge;
        do
        {
            auto nIdx = hull.edgeAt(hull.edgeAt(edge).twin & visitMask).face;
            Face& neighbour = hull.faceAt(nIdx);
            if (!visited(neighbour) && isVisible(neighbour, point, epsilon))
            {
                unprocessed.push(nIdx);
            }
            edge = hull.edgeAt(edge).next & visitMask;
        } while (edge != firstEdge);
    }
    return found;
}

std::vector<EdgeIndex> createHorizon(vec3 point, FaceIndex face,
                                     Mesh& hull, float epsilon,
                                     float triangleEpsilon,
                                     std::vector<vec3>& pointsOut,
                                     FacePoints& fPoints)
{
    std::vector<EdgeIndex> horizon;
    std::vector<EdgeIndex> visibleEdges;
    std::vector<VertexIndex> visibleVertices;
    auto firstEdge = findHorizonEdge(point, hull.faceAt(face).outer,
                                     hull, epsilon, visibleVertices,
                                     visibleEdges);
    horizon.push_back(firstEdge);
    size_t guard = 0;
    do
    {
        auto cEdge = nextHorizonEdge(horizon.back(), point, hull, epsilon);
        horizon.push_back(cEdge);
    } while (horizon.front() != horizon.back()
             && guard++ <= hull.edges());

    if (guard > hull.edges())
    {
        throw std::logic_error("Horizon creation gives way too many edges.");
    }
    horizon.pop_back();

    float minArea = Constant<float>::maximum;

    for (size_t i = 0; i < horizon.size(); ++i)
    {
        Edge& cEdge = hull.edgeAt(horizon[i]);
        Edge& cTwin = hull.edgeAt(cEdge.twin & visitMask);
        vec3 v0 = hull.vertexAt(cEdge.origin).pos;
        vec3 v1 = hull.vertexAt(cTwin.origin).pos;
        float area = length(cross(point-v0, point-v1));
        minArea = std::min(minArea, area);
    }

    // This is an experimentally derived constant which seems to give good
    // results as the triangles with larger area than this appears to be stable
    // wrt. normals.
    if (minArea < triangleEpsilon)
    {
        for (auto& vertex : visibleVertices)
        {
            clearVisited(hull.vertexAt(vertex));
        }
        for (auto& edge : visibleEdges)
        {
            clearVisited(hull.edgeAt(edge));
        }
        return {};
    }

    auto vFaces = visibleFaces(face, point, hull, epsilon);

    for (size_t i = 0; i < horizon.size(); ++i)
    {
        Edge& curr = hull.edgeAt(horizon[i]);
        auto& vert = hull.vertexAt(curr.origin);
        vert.edge = horizon[i];
        clearVisited(vert);
        curr.next = horizon[(i+1)%horizon.size()];
        curr.prev = horizon[(i+horizon.size()-1)%horizon.size()];
        clearVisited(curr);
    }

    for (auto& vertex : visibleVertices)
    {
        if (visited(hull.vertexAt(vertex)))
        {
            hull.deleteVertex(vertex);
        }
    }

    for (auto& edge : visibleEdges)
    {
        hull.deleteEdge(edge);
    }

    for (auto& f : vFaces)
    {
        pointsOut.insert(pointsOut.end(),
                         fPoints[f].begin(),
                         fPoints[f].end());
        fPoints[f].clear();
        hull.deleteFace(f);
    }

    return horizon;
}

std::vector<FaceIndex> createFaces(vec3 point,
                                   const std::vector<EdgeIndex>& horizon,
                                   Mesh& hull)
{
    if (horizon.empty()) return {};
    if (horizon.size() < 3)
    {
        throw std::runtime_error("Horizon degenerate!");
    }

    VertexIndex vert = hull.createVertex();
    hull.vertexAt(vert).pos = point;

    std::vector<FaceIndex> newFaces;

    for (auto& edge : horizon)
    {

        auto eIdx1 = hull.createEdge();
        auto eIdx2 = hull.createEdge();
        auto& edge1 = hull.edgeAt(eIdx1);
        auto& edge2 = hull.edgeAt(eIdx2);
        auto& hEdge = hull.edgeAt(edge);

        auto v1 = hull.edgeAt(hEdge.next).origin;

        edge1.origin = v1;
        edge1.prev = edge;
        edge1.next = eIdx2;

        edge2.origin = vert;
        edge2.prev = eIdx1;
        edge2.next = edge;

        hEdge.next = eIdx1;
        hEdge.prev = eIdx2;

        auto fIdx = hull.createFace();
        auto& face = hull.faceAt(fIdx);
        hEdge.face = edge1.face = edge2.face = fIdx;
        face.outer = edge; // This could in principle be any of the created
                           // edges but it is assumed in the code below that
                           // we use an edge on the horizon. Don't change it.
        hull.updateFace(fIdx);
        newFaces.push_back(fIdx);
    }


    size_t newFaceCount = newFaces.size();

    // Now we only need to link the half-edges together with their twins.
    // Note that all faces refer to an edge on the horizon and therefore we know
    // which edges we need to fix the references of.
    for (size_t i = 0; i < newFaceCount; ++i)
    {
        auto& currFace = hull.faceAt(newFaces[i]);
        auto& nextFace = hull.faceAt(newFaces[(i+1)%newFaceCount]);
        auto currEdge = hull.edgeAt(currFace.outer).next;
        auto nextEdge = hull.edgeAt(nextFace.outer).prev;
        hull.edgeAt(currEdge).twin = nextEdge;
        hull.edgeAt(nextEdge).twin = currEdge;
    }
    hull.vertexAt(vert).edge = hull.edgeAt(hull.faceAt(newFaces[0]).outer).prev;
    return newFaces;
}

bool isTriangle(Face& f, Mesh& hull)
{
    auto eIdx = hull.edgeAt(f.outer&visitMask).next;
    eIdx = hull.edgeAt(eIdx).next;
    if (eIdx == (f.outer&visitMask)) return false;
    eIdx = hull.edgeAt(eIdx).next;
    return eIdx == (f.outer&visitMask);
}

void fixEdges(FaceIndex fIdx, Mesh& hull, EdgeIndex& edgeOut)
{
    Face& face = hull.faceAt(fIdx);
    auto eIdx = face.outer;
    do
    {
        Edge* cEdge = &hull.edgeAt(eIdx);
        auto nextIdx = cEdge->next;
        Edge* nEdge = &hull.edgeAt(nextIdx);
        auto cTwinIdx = cEdge->twin;
        auto nTwinIdx = nEdge->twin;
        Edge* ctEdge = &hull.edgeAt(cTwinIdx);
        Edge* ntEdge = &hull.edgeAt(nTwinIdx);

        if (ntEdge->next == cTwinIdx)
        {
            face.outer = cEdge->prev;
            if (isTriangle(hull.faceAt(ctEdge->face), hull))
            {
                auto oIdx = ctEdge->next;
                Edge& oEdge = hull.edgeAt(oIdx);
                oEdge.face = fIdx;
                oEdge.prev = cEdge->prev;
                oEdge.next = nEdge->next;
                hull.edgeAt(oEdge.prev).next = oIdx;
                hull.edgeAt(oEdge.next).prev = oIdx;
                hull.vertexAt(oEdge.origin).edge = oIdx;
                hull.vertexAt(ntEdge->origin).edge = oEdge.next;

                edgeOut = ctEdge->next;

                hull.deleteVertex(nEdge->origin);
                hull.deleteFace(ntEdge->face);
                hull.deleteEdge(eIdx);
                hull.deleteEdge(nextIdx);
                hull.deleteEdge(cTwinIdx);
                hull.deleteEdge(nTwinIdx);

                eIdx = oIdx;
            }
            else
            {
                hull.deleteVertex(nEdge->origin);
                hull.vertexAt(ntEdge->origin).edge = cTwinIdx;
                cEdge->next = nEdge->next;
                ctEdge->prev = ntEdge->prev;
                hull.edgeAt(cEdge->next).prev = eIdx;
                hull.edgeAt(ctEdge->prev).next = cTwinIdx;
                ctEdge->origin = ntEdge->origin;
                hull.deleteEdge(nextIdx);
                hull.deleteEdge(nTwinIdx);
                hull.faceAt(ctEdge->face).outer = cTwinIdx;
                edgeOut = cEdge->next;
            }
        }
        else
        {
            eIdx = cEdge->next;
        }
    } while (eIdx != face.outer);
}

void merge(FaceIndex preserve, FaceIndex remove, EdgeIndex& edge, Mesh& hull)
{
    if (remove == preserve)
    {
        throw std::logic_error("Cannot merge face into itself.");
    }
    clearVisited(hull.faceAt(preserve));
    EdgeIndex beginEdge = hull.edgeAt(edge).twin;
    EdgeIndex cEdge = beginEdge;
    std::vector<EdgeIndex> toDelete;
    do
    {
        Edge& curr = hull.edgeAt(cEdge);
        if (hull.edgeAt(curr.twin).face != preserve)
        {
            curr.face = preserve;
        }
        else
        {
            toDelete.push_back(cEdge);
        }
        cEdge = curr.next;
    } while (cEdge != beginEdge);

    if (toDelete.empty())
    {
        throw std::logic_error("Merging could not find any shared edge.");
    }

    Edge* front = &hull.edgeAt(toDelete.front());
    Edge* fTwin = &hull.edgeAt(front->twin);

    hull.edgeAt(front->next).prev = fTwin->prev;
    hull.edgeAt(fTwin->prev).next = front->next;
    hull.vertexAt(fTwin->origin).edge = front->next;


    Edge* back = &hull.edgeAt(toDelete.back());
    Edge* bTwin = &hull.edgeAt(back->twin);

    hull.edgeAt(back->prev).next = bTwin->next;
    hull.edgeAt(bTwin->next).prev = back->prev;
    hull.vertexAt(back->origin).edge = bTwin->next;

    hull.faceAt(preserve).outer = front->next;
    edge = front->next;

    for (size_t i = 0; i < toDelete.size(); ++i)
    {
        Edge* del = &hull.edgeAt(toDelete[i]);
        if (i+1 < toDelete.size())
        {
            hull.deleteVertex(del->origin);
        }
        hull.deleteEdge(del->twin);
        hull.deleteEdge(toDelete[i]);
    }

    hull.deleteFace(remove);
    hull.updateFace(preserve);
    fixEdges(preserve, hull, edge);
    setVisited(hull.faceAt(preserve));
}

//void mergeNeighbours(FaceIndex face, Mesh& hull, float sinAngle = 1.2e-2f)
void mergeNeighbours(FaceIndex face, Mesh& hull, float mergeEpsilon)
{
    if (!hull.isValidFace(face&visitMask)) return;
    Face& f = hull.faceAt(face);
    EdgeIndex cEdge = f.outer & visitMask;
    do
    {
        FaceIndex nIdx = hull.edgeAt(hull.edgeAt(cEdge).twin).face;
        Face* n = &hull.faceAt(nIdx);
        //if (zero(cross(f.normal, n->normal), sinAngle))
        if (visited(*n) &&
            (std::abs(signedDistance(f, n->center)) < mergeEpsilon ||
             std::abs(signedDistance(*n, f.center)) < mergeEpsilon))
        {
            merge(face, nIdx, cEdge, hull);
        }
        cEdge = hull.edgeAt(cEdge).next;
    } while (cEdge != (f.outer & visitMask));
}

void mergeFaces(std::vector<FaceIndex>& faces, Mesh& hull, float mergeEpsilon)
{
    for (auto& face : faces)
    {
        setVisited(hull.faceAt(face));
    }

    for (auto& face : faces)
    {
        mergeNeighbours(face, hull, mergeEpsilon);
    }

    for (auto& face : faces)
    {
        if (hull.isValidFace(face)) clearVisited(hull.faceAt(face));
    }

    // Since we don't construct any faces but may delete some of the created
    // ones, the face indices of the deleted ones cannot be reused yet.
    // Therefore we can simply check whether any of the newly created faces have
    // been merged by checking whether its index is still valid.
    for (size_t i = 0; i < faces.size();)
    {
        if (!hull.isValidFace(faces[i]))
        {
            std::swap(faces[i], faces[faces.size()-1]);
            faces.pop_back();
        }
        else ++i;
    }
}

void resolvePoints(vec3 cPoint,
                   const std::vector<vec3>& points,
                   const std::vector<FaceIndex>& faces,
                   std::queue<FaceIndex>& unprocessed,
                   FacePoints& fPoints,
                   Mesh& hull,
                   float epsilon)
{
    for (auto point : points)
    {
        if (squaredLength(cPoint-point) < epsilon * epsilon) continue;
        float maxDist = -Constant<float>::maximum;
        size_t farthestIdx = 0;

        for (size_t f = 0; f < faces.size(); ++f)
        {
            auto dist = signedDistance(hull.faceAt(faces[f]), point);
            if (dist > maxDist)
            {
                maxDist = dist;
                farthestIdx = f;
            }
        }
        if (maxDist > epsilon * 10)
        {
            fPoints[faces[farthestIdx]].push_back(point);
        }
    }
    for (size_t i = 0; i < faces.size(); ++i)
    {
        unprocessed.push(faces[i]);
    }
}

void addPointToHull(vec3 point, FaceIndex origFace,
                    std::queue<FaceIndex>& unprocessed,
                    Mesh& hull, FacePoints& fPoints, float epsilon,
                    float triangleMul, float mergeMul)
{
    if (!hull.isPolyhedron())
    {
        hull.dump();
        hull.checkRefs();
        hull.checkTopology();
        throw std::logic_error("Bad initial hull ("
                               + std::to_string(hull.eulerCharacteristic())
                               + ")");
    }

    std::vector<vec3> orphans;

    // Create horizon around point.
    auto horizon = createHorizon(point, origFace, hull, epsilon,
                                 triangleMul * epsilon, orphans,
                                 fPoints);

    auto newFaces = createFaces(point, horizon, hull);

    if (!hull.isPolyhedron())
    {
        hull.dump();
        hull.checkRefs();
        hull.checkTopology();
        throw std::logic_error("Bad hull after horizon face creation ("
                               + std::to_string(hull.eulerCharacteristic())
                               + ")");
    }
    mergeFaces(newFaces, hull, mergeMul * epsilon);

    if (!hull.isPolyhedron())
    {
        hull.dump();
        hull.checkRefs();
        hull.checkTopology();
        throw std::logic_error("Bad hull after merging ("
                               + std::to_string(hull.eulerCharacteristic())
                               + ")");
    }
    resolvePoints(point, orphans, newFaces, unprocessed, fPoints, hull,
                  epsilon);

    if (!hull.isPolyhedron())
    {
        hull.dump();
        hull.checkRefs();
        hull.checkTopology();
        throw std::logic_error("Bad hull after point resolution ("
                               + std::to_string(hull.eulerCharacteristic())
                               + ")");
    }
}

}

Mesh quickhull(const vec3* vertices,
               size_t vertexCount,
               size_t vertexStride,
               float triangleEpsilon,
               float mergeEpsilon)
{
    float epsilon = 1e-6f;
    Mesh hull = buildInitialHull(vertices, vertexCount, vertexStride, epsilon);

    std::queue<FaceIndex> unprocessed;
    std::array<FaceIndex, 4> initialFaces;

    size_t idx = 0;
    for (auto face = hull.fbegin(); face != hull.fend(); ++face)
    {
        initialFaces[idx++] = face.index();
    }
    FacePoints fPoints;
    std::array<decltype(fPoints.begin()), 4> indices;
    for (size_t i = 0; i < 4; ++i)
    {
        fPoints[initialFaces[i]] = {};
        indices[i] = fPoints.find(initialFaces[i]);
    }

    for (size_t vert = 0; vert < vertexCount; ++vert)
    {
        auto vertex = getVertex(vertices, vertexStride, vert);
        float maxDist = -Constant<float>::maximum;
        size_t cnt = 0;
        size_t farthestIdx = 0;
        for (auto f = hull.fbegin(); f != hull.fend(); ++cnt, ++f)
        {
            auto dist = signedDistance(*f, vertex);
            if (dist > maxDist)
            {
                maxDist = dist;
                farthestIdx = cnt;
            }
        }
        if (maxDist > epsilon * 10)
        {
            indices[farthestIdx]->second.push_back(vertex);
        }
    }

    for (size_t i = 0; i < 4; ++i)
    {
        unprocessed.push(initialFaces[i]);
    }

    while (!unprocessed.empty())
    {
        auto face = unprocessed.front();
        unprocessed.pop();
        if (!hull.isValidFace(face)) continue;
        if (fPoints[face].empty()) continue;
        auto farthest = getFarthestPoint(face, hull, fPoints[face]);
        if (!isVisible(hull.faceAt(face), farthest, epsilon))
        {
            fPoints[face].clear();
            continue;
        }
        // 2 * triangleEpsilon since createHorizon calculates double the
        // triangle area.
        addPointToHull(farthest, face, unprocessed, hull, fPoints, epsilon,
                       2.f * triangleEpsilon, mergeEpsilon);
    }
    if (!hull.isPolyhedron())
    {
        throw std::logic_error("Convex hull is not a polyhedron. Euler "
                               "characteristic: "
                               + std::to_string(hull.eulerCharacteristic()));
    }

    return hull;
}

} // kraken::geometry
