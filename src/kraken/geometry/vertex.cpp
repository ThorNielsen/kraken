#include <algorithm>
#include <cstddef>
#include <kraken/geometry/vertex.hpp>

#include <ostream>
#include <stdexcept>
#include <vector>

namespace kraken::geometry
{

bool operator==(const VertexType& a, const VertexType& b)
{
    if (a.size() != b.size()) return false;
    for (size_t i = 0; i < a.size(); ++i)
    {
        if (a[i].name != b[i].name) return false;
        if (a[i].type != b[i].type) return false;
        if (a[i].count != b[i].count) return false;
    }
    return true;
}

std::ostream& operator<<(std::ostream& ost, VertexDataType vdt)
{
    if ((vdt & VertexDataType::Unnormalised) != VertexDataType::Padding)
    {
        ost << "Unnormalised";
    }
    if ((vdt & VertexDataType::Integer) != VertexDataType::Padding)
    {
        ost << "Integer";
    }
    switch (vdt&VertexDataType::TypeMask)
    {
    case VertexDataType::Padding: ost << "Padding"; break;
    case VertexDataType::Byte: ost << "Byte"; break;
    case VertexDataType::UByte: ost << "UByte"; break;
    case VertexDataType::Short: ost << "Short"; break;
    case VertexDataType::UShort: ost << "UShort"; break;
    case VertexDataType::Int: ost << "Int"; break;
    case VertexDataType::UInt: ost << "UInt"; break;
    case VertexDataType::Float: ost << "Float"; break;
    case VertexDataType::TypeMask:
    case VertexDataType::Unnormalised:
    case VertexDataType::Integer:
    default:
        break; // Impossible, but we will sate the compiler's warnings here.
    }
    return ost;
}

size_t dataTypeSize(VertexDataType d)
{
    switch (d & VertexDataType::TypeMask)
    {
    case VertexDataType::Padding:
    case VertexDataType::Byte:
    case VertexDataType::UByte:
        return 1;
    case VertexDataType::Short:
    case VertexDataType::UShort:
        return 2;
    case VertexDataType::Int:
    case VertexDataType::UInt:
    case VertexDataType::Float:
        return 4;

    // These are not valid but in order to let the compiler check for missed
    // cases in switch-statements, they have been added.
    case VertexDataType::TypeMask: case VertexDataType::Unnormalised:
    case VertexDataType::Integer:
    default:
        throw std::runtime_error("Invalid VertexDataType!");
    }
}

std::ostream& operator<<(std::ostream& ost, const Attribute& attr)
{
    return ost << "{\"name\": \"" << attr.name
               << "\", \"type\": \"" << attr.type
               << "\", \"count\": " << attr.count
               << "}";
}

std::ostream& operator<<(std::ostream& ost, const VertexType& vt)
{
    ost << "[";
    for (size_t i = 0; i < vt.size(); ++i)
    {
        if (i) ost << ", ";
        ost << vt[i];
    }
    return ost << "]";
}

VertexType normalise(const VertexType& vType)
{
    struct SortHelper
    {
        size_t value;
        decltype(vType.begin()) iterator;
    };

    std::vector<SortHelper> helpers;
    helpers.reserve(vType.size());
    size_t invalidValue = ~(size_t)0;
    for (auto it = vType.begin(); it != vType.end(); ++it)
    {
        size_t value = invalidValue;
        if (it->name == "position" || it->name == "pos") value = 0;
        else if (it->name == "normal" || it->name == "nml") value = 1;
        else if (it->name == "colour"
                 || it->name == "color" // This abominable spelling is common.
                 || it->name == "col")
        {
            value = 2;
        }
        else if (it->name == "texcoord"
                 || it->name == "tc"
                 || it->name == "uv")
        {
            value = 3;
        }
        helpers.push_back({value, it});
    }

    std::stable_sort(helpers.begin(), helpers.end(),
    [invalidValue](const SortHelper& a, const SortHelper& b)
    {
        if (a.value == invalidValue && b.value == invalidValue)
        {
            return a.iterator->name < b.iterator->name;
        }
        return a.value < b.value;
    });

    VertexType vReturn;
    vReturn.reserve(helpers.size());
    for (const auto& helper : helpers)
    {
        vReturn.push_back(*helper.iterator);
    }
    return vReturn;
}

} // namespace kraken::geometry
