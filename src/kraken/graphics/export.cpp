#include "kraken/graphics/vertex.hpp"
#include "kraken/types.hpp"
#include <cstddef>
#include <kraken/graphics/export.hpp>
#include <kraken/graphics/formats/plywriter.hpp>
#include <kraken/graphics/mesh.hpp>
#include <ostream>

namespace kraken
{

namespace graphics
{

bool exportPLY(std::ostream& output, const Mesh* mesh,
               PLYExportOptions opts)
{
    return exportPLY(output,
                     mesh->vertices(),
                     mesh->vertexCount(),
                     mesh->indices(),
                     mesh->indexCount(),
                     mesh->vertexType(),
                     mesh->indexByteWidth(),
                     opts);
}
bool exportPLY(std::ostream& output,
               const void* vertexBegin,
               size_t vertexCount,
               const void* indexBegin,
               size_t indexCount,
               const VertexType& vertexType,
               U32 indexByteWidth,
               PLYExportOptions opts)
{
    PLYWriter writer;
    return writer.write(output,
                        vertexBegin,
                        vertexCount,
                        indexBegin,
                        indexCount,
                        vertexType,
                        indexByteWidth,
                        opts);
}

} // namespace graphics

} // namespace kraken
