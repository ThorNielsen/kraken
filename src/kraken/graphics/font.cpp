#include <kraken/graphics/font.hpp>
#include <kraken/types.hpp>
#include <kraken/math/matrix.hpp>
#include <kraken/graphics/freetype/freetype.hpp>
#include <kraken/graphics/freetype/glyphmap.hpp>

#include <fstream>
#include <stdexcept>

namespace kraken
{

namespace graphics
{

namespace freetype
{

namespace
{
    FT_Library g_freeTypeLibrary;
    U32 g_FontCount = 0;
} // end anonymous namespace

} // namespace freetype


Font::Font(VideoDriver* driver)
  : m_fontdata{},
    m_driver{driver},
    m_height{0},
    m_faceInit{false}
{
    m_face = new freetype::FaceWrapper;
    m_glyphmap.glyphmap = new freetype::GlyphMap(driver);
    if (!freetype::g_FontCount) // FreeType not initialised.
    {
        checkFTError(FT_Init_FreeType(&freetype::g_freeTypeLibrary));
    }
    ++freetype::g_FontCount;
}

Font::~Font()
{
    deleteFace();
    delete m_face;

    --freetype::g_FontCount;
    if (!freetype::g_FontCount)
    {
        checkFTError(FT_Done_FreeType(freetype::g_freeTypeLibrary));
    }
}

bool Font::load(const std::string& path)
{
    std::ifstream fontfile(path);
    if (!fontfile.is_open()) return false;

    U64 start = fontfile.tellg();
    fontfile.seekg(0, std::ios_base::end);
    U64 end = fontfile.tellg();
    fontfile.seekg(start);

    deleteFace();
    m_fontdata.resize(end - start);
    fontfile.read(static_cast<char*>(static_cast<void*>(m_fontdata.data())),
                  end - start);
    return createFace();
}

bool Font::load(const U8* data, U64 length)
{
    deleteFace();
    m_fontdata.resize(length);
    for (U64 i = 0; i < length; ++i)
    {
        m_fontdata[i] = data[i];
    }
    return createFace();
}

void Font::update()
{
    m_glyphmap.glyphmap->updateMap();
}

void Font::setHeight(U32 h) const
{
    if (m_height == h) return;
    checkFTError(FT_Set_Pixel_Sizes(m_face->face,
                                    0,
                                    h));
    m_height = h;
}


bool Font::createFace()
{
    auto err = FT_New_Memory_Face(freetype::g_freeTypeLibrary,
                                  m_fontdata.data(),
                                  m_fontdata.size(),
                                  0,
                                  &(m_face->face));
    //checkFTError(err);
    m_faceInit = true;

    m_glyphmap.glyphmap->clear();
    m_usekerning = FT_HAS_KERNING(m_face->face);
    return !err;
}

void Font::enableKerning()
{
    if (m_face->face)
    {
        m_usekerning = FT_HAS_KERNING(m_face->face);
    }
}

void Font::deleteFace()
{
    if (!m_faceInit) return;
    m_faceInit = false;
    checkFTError(FT_Done_Face(m_face->face));
}

Glyph Font::getGlyph(U32 codepoint, U32 height) const
{
    setHeight(height);
    return m_glyphmap.glyphmap->getGlyph(m_face->face, codepoint, height);
}

void Font::setupRendering(Shader* shader) const
{
    if (m_glyphmap.glyphmap->dirty()) m_glyphmap.glyphmap->updateMap();
    shader->useTexture(shader->textureID("textTexture"),
                       m_glyphmap.glyphmap->getTexture(),
                       m_glyphmap.glyphmap->getSampler());

    shader->setUniform(shader->uniformID("glyphMapSize"),
                       math::vec2{static_cast<float>(m_glyphmap.glyphmap->texWidth()),
                                  static_cast<float>(m_glyphmap.glyphmap->texHeight())});
}

Font::GlyphMapWrapper::~GlyphMapWrapper()
{
    if (glyphmap) delete glyphmap;
}

} // namespace graphics

} // namespace kraken
