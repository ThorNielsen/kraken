#include "kraken/geometry/vertex.hpp"
#include "kraken/graphics/vertex.hpp"
#include "kraken/types.hpp"
#include <cstddef>
#include <kraken/graphics/export.hpp>
#include <kraken/graphics/formats/plywriter.hpp>

#include <kraken/utility/memory.hpp>
#include <ostream>
#include <stdexcept>
#include <string>

namespace kraken
{

namespace graphics
{

namespace
{

template <size_t Bytes>
void writeBytes(void* buf, const void* data)
{
    auto* cBuf = static_cast<U8*>(buf);
    const auto* cData = static_cast<const U8*>(data);
    for (size_t i = 0; i < Bytes; ++i)
    {
        *cBuf++ = *cData++;
    }
}

template <typename Type>
void writeData(void* buf, Type data)
{
    writeBytes<sizeof(Type)>(buf, static_cast<const void*>(&data));
}

void advance(void*& ptr, size_t bytes)
{
    ptr = static_cast<void*>(static_cast<U8*>(ptr) + bytes);
}

} // end anonymous namespace


PLYWriter::PLYWriter()
{
}

PLYWriter::~PLYWriter()
{
}

void PLYWriter::reset()
{
    m_indices = {};
    m_header = "";
    m_padNumber = 0;
    m_vCount = 0;
    m_iCount = 0;
    // Always use the same line endings. In the original specification by Greg
    // Turk, the line endings that should be used are CR (0x0d in ASCII) only.
    // However, some software (Blender for example) does not like CR line ends
    // and therefore LF is used even though this is not correct.
    m_lineEnd = 0x0a;
    m_hasIndices = false;
}

std::string plyType(VertexDataType dataType)
{
    switch (dataType & VertexDataType::TypeMask)
    {
    case VertexDataType::Padding: return "uchar";
    case VertexDataType::Byte: return "char";
    case VertexDataType::UByte: return "uchar";
    case VertexDataType::Short: return "short";
    case VertexDataType::UShort: return "ushort";
    case VertexDataType::Int: return "int";
    case VertexDataType::UInt: return "uint";
    case VertexDataType::Float: return "float";

    case VertexDataType::TypeMask:
    case VertexDataType::Unnormalised:
    case VertexDataType::Integer:
    default:
        return "invalid";
    }
}

bool PLYWriter::appendAttribute(const std::string& name,
                                VertexDataType dataType,
                                size_t count)
{
    auto lineStart = plyType(dataType);
    if (lineStart == "invalid") return false;
    lineStart = "property " + lineStart + " ";

    if (dataType == VertexDataType::Padding)
    {
        for (size_t i = 0; i < count; ++i)
        {
            m_header += "property uchar padding"
                        + toString(m_padNumber++)
                        + m_lineEnd;
        }
        return true;
    }

    if (name == "position")
    {
        if (count < 3 || count > 4) return false;
        for (size_t i = 0; i < count; ++i)
        {
            m_header += lineStart + "xyzw"[i] + m_lineEnd;
        }
    }
    else if (name == "normal")
    {
        if (count != 3) return false;
        for (size_t i = 0; i < count; ++i)
        {
            m_header += lineStart + "n" + "xyz"[i] + m_lineEnd;
        }
    }
    else if (name == "texcoord")
    {
        if (count < 2 || count > 4) return false;
        for (size_t i = 0; i < count; ++i)
        {
            m_header += lineStart + "stpq"[i] + m_lineEnd;
        }
    }
    else if (name == "colour")
    {
        if (count < 3 || count > 4) return false;
        std::string names[4] = {"red", "green", "blue", "alpha"};
        for (size_t i = 0; i < count; ++i)
        {
            m_header += lineStart + names[i] + m_lineEnd;
        }
    }
    else if (count == 1)
    {
        m_header += lineStart + name + m_lineEnd;
    }
    else
    {
        for (size_t i = 0; i < count; ++i)
        {
            m_header += lineStart + name + toString(count) + m_lineEnd;
        }
    }
    return true;
}

bool PLYWriter::createHeader(const VertexType& vType)
{
    if (m_iCount % 3) return false;

    m_header += "ply";
    m_header += m_lineEnd;
    m_header += "format binary_";
    m_header += (isBigEndian() ? "big" : "little");
    m_header += "_endian 1.0";
    m_header += m_lineEnd;
    m_header += "comment Exported using kraken.";
    m_header += m_lineEnd;
    m_header += "element vertex ";
    m_header += toString(m_vCount);
    m_header += m_lineEnd;
    for (size_t i = 0; i < vType.size(); ++i)
    {
        appendAttribute(vType[i].name, vType[i].type, vType[i].count);
    }
    m_header += "element face ";
    m_header += toString(m_iCount/3);
    m_header += m_lineEnd;
    m_header += "property list uchar ";

    const char* widthNames[] =
        {"uchar", "ushort", "uint", "char", "short", "int"};
    const unsigned long long widthSplits[] =
        { 256ull, 65536ull, 128ull, 32768ull };
    size_t nameOffset = 0;
    size_t splitOffset = 0;
    if (m_options.signedIndices && m_vCount < 0x80000000ull)
    {
        nameOffset = 3;
        splitOffset = 2;
    }

    if (m_vCount < widthSplits[0+splitOffset])
    {
        m_indexOutWidth = 1;
        m_header += widthNames[0+nameOffset];
    }
    else if (m_vCount < widthSplits[1+splitOffset])
    {
        m_indexOutWidth = 2;
        m_header += widthNames[1+nameOffset];
    }
    else
    {
        m_indexOutWidth = 4;
        m_header += widthNames[2+nameOffset];
    }
    m_header += " vertex_indices";
    m_header += m_lineEnd;
    m_header += "end_header";
    m_header += m_lineEnd;

    // Unreferenced vertices is not a problem -- what if someone wants to export
    // point clouds or something with only a very few triangles.
    // It could for example happen that someone created a very simple mesh of
    // e.g. 50 triangles, then generated 100000 randomly distributed points
    // inside, and then exported the resulting mesh.
    /*if (indexByteWidth < m_indexOutWidth)
    {
        throw std::runtime_error("Unreferenced vertices in mesh detected.");
    }*/

    return true;
}

template <typename ReadType, typename WriteType>
void writeTriangleIndices(void*& dest, const void* src, size_t triNum,
                          size_t vertexCount)
{
    size_t a = static_cast<const ReadType*>(src)[3*triNum  ];
    size_t b = static_cast<const ReadType*>(src)[3*triNum+1];
    size_t c = static_cast<const ReadType*>(src)[3*triNum+2];
    if (a >= vertexCount || b >= vertexCount || c >= vertexCount)
    {
        throw std::logic_error("Bad vertex index: Triangle("
                               + std::to_string(a) + ", "
                               + std::to_string(b) + ", "
                               + std::to_string(c) + "), but max is "
                               + std::to_string(vertexCount) + ".");
    }
    if (a == b && b == c)
    {
        throw std::logic_error("Degenerate face on vertex " + std::to_string(a) + ".");
    }
    writeData<U8>(dest, 3);
    advance(dest, 1);
    writeData<WriteType>(dest, a);
    advance(dest, sizeof(WriteType));
    writeData<WriteType>(dest, b);
    advance(dest, sizeof(WriteType));
    writeData<WriteType>(dest, c);
    advance(dest, sizeof(WriteType));
}

bool PLYWriter::copyExistingIndices(const void* currIndices, U32 indexByteWidth)
{
    size_t triCount = m_iCount / 3;
    m_indices.resize(m_iCount * m_indexOutWidth + triCount);
    void* dest = static_cast<void*>(m_indices.data());
    if (indexByteWidth == 1)
    {
        if (m_indexOutWidth == 1)
        {
            for (size_t i = 0; i < triCount; ++i)
            {
                writeTriangleIndices<U8, U8>(dest, currIndices, i, m_vCount);
            }
        }
        else if (m_indexOutWidth == 2)
        {
            for (size_t i = 0; i < triCount; ++i)
            {
                writeTriangleIndices<U8, U16>(dest, currIndices, i, m_vCount);
            }
        }
        else // if (m_indexOutWidth == 4)
        {
            for (size_t i = 0; i < triCount; ++i)
            {
                writeTriangleIndices<U8, U32>(dest, currIndices, i, m_vCount);
            }
        }
    }
    else if (indexByteWidth == 2)
    {
        if (m_indexOutWidth == 1)
        {
            for (size_t i = 0; i < triCount; ++i)
            {
                writeTriangleIndices<U16, U8>(dest, currIndices, i, m_vCount);
            }
        }
        else if (m_indexOutWidth == 2)
        {
            for (size_t i = 0; i < triCount; ++i)
            {
                writeTriangleIndices<U16, U16>(dest, currIndices, i, m_vCount);
            }
        }
        else // if (m_indexOutWidth == 4)
        {
            for (size_t i = 0; i < triCount; ++i)
            {
                writeTriangleIndices<U16, U32>(dest, currIndices, i, m_vCount);
            }
        }
    }
    else if (indexByteWidth == 4)
    {
        if (m_indexOutWidth == 1)
        {
            for (size_t i = 0; i < triCount; ++i)
            {
                writeTriangleIndices<U32, U8>(dest, currIndices, i, m_vCount);
            }
        }
        else if (m_indexOutWidth == 2)
        {
            for (size_t i = 0; i < triCount; ++i)
            {
                writeTriangleIndices<U32, U16>(dest, currIndices, i, m_vCount);
            }
        }
        else// if (m_indexOutWidth == 4)
        {
            for (size_t i = 0; i < triCount; ++i)
            {
                writeTriangleIndices<U32, U32>(dest, currIndices, i, m_vCount);
            }
        }
    }
    else
    {
        return false;
    }
    return true;
}

void PLYWriter::createNewIndices()
{
    size_t requiredBytes = m_indexOutWidth;
    size_t triCount = m_iCount / 3;
    m_indices.resize(m_iCount * requiredBytes + triCount);
    void* dest = static_cast<void*>(m_indices.data());
    if (requiredBytes == 1)
    {
        for (size_t i = 0; i < triCount; ++i)
        {
            writeData<U8>(dest, 3); advance(dest, 1);
            writeData<U8>(dest, 3*i  ); advance(dest, 1);
            writeData<U8>(dest, 3*i+1); advance(dest, 1);
            writeData<U8>(dest, 3*i+2); advance(dest, 1);
        }
    }
    else if (requiredBytes == 2)
    {
        for (size_t i = 0; i < triCount; ++i)
        {
            writeData<U8>(dest, 3); advance(dest, 1);
            writeData<U16>(dest, 3*i  ); advance(dest, 2);
            writeData<U16>(dest, 3*i+1); advance(dest, 2);
            writeData<U16>(dest, 3*i+2); advance(dest, 2);
        }
    }
    else
    {
        for (size_t i = 0; i < triCount; ++i)
        {
            writeData<U8>(dest, 3); advance(dest, 1);
            writeData<U32>(dest, 3*i  ); advance(dest, 4);
            writeData<U32>(dest, 3*i+1); advance(dest, 4);
            writeData<U32>(dest, 3*i+2); advance(dest, 4);
        }
    }
}

bool PLYWriter::createIndices(const void* currIndices,
                              U32 indexByteWidth)
{
    if (m_iCount % 3) return false;

    if (m_hasIndices)
    {
        return copyExistingIndices(currIndices, indexByteWidth);
    }
    else
    {
        createNewIndices();
        return true;
    }
}

bool PLYWriter::write(std::ostream& output,
                      const void* vertexBegin,
                      size_t vertexCount,
                      const void* indexBegin,
                      size_t indexCount,
                      const VertexType& vertexType,
                      U32 indexByteWidth,
                      PLYExportOptions opts)
{
    reset();

    m_options = opts;
    m_vCount = vertexCount;
    m_iCount = indexCount;
    if (!m_iCount)
    {
        m_iCount = m_vCount;
        m_hasIndices = false;
    }
    else
    {
        m_hasIndices = true;
    }

    createHeader(vertexType);
    if (!createIndices(indexBegin, indexByteWidth)) return false;
    output.write(m_header.data(), m_header.size());
    output.write(static_cast<const char*>(vertexBegin),
                 m_vCount * vertexSize(vertexType));
    output.write(static_cast<const char*>(static_cast<const void*>(m_indices.data())),
                 m_indices.size());
    return output.good();
}

} // namespace graphics

} // namespace kraken

