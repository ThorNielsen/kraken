#include "freetype/fttypes.h"
#include <kraken/graphics/freetype/freetype.hpp>
#include <stdexcept>
#include <string>

namespace kraken
{

namespace graphics
{

namespace freetype
{

void throwFTError(FT_Error fterr, std::string func, int line)
{
    throw std::runtime_error("Freetype error (in "
                             + func
                             + ", line "
                             + std::to_string(line)
                             + "): "
                             + std::to_string(fterr));
}

} // namespace freetype

} // namespace graphics

} // namespace kraken
