#include <kraken/graphics/freetype/glyphmap.hpp>

#include <kraken/graphics/freetype/freetype.hpp>

namespace kraken
{

namespace graphics
{

namespace freetype
{

GlyphMap::GlyphMap(VideoDriver* driver) :
    m_positions{},
    m_texture{nullptr},
    m_xpos{0},
    m_ypos{0},
    m_lineheight{0},
    m_width{1024},
    m_height{8},
    m_dirty{true}
{
    m_sampler = driver->createTextureSampler();
    m_sampler->setMinFilter(InterpolationFilter::Nearest);
    m_sampler->setMagFilter(InterpolationFilter::Nearest);
    m_sampler->setMinMipmap(0);
    m_sampler->setMaxMipmap(0);
    m_sampler->setWrapMode(WrapMode::MirrorRepeat);

    m_texture = driver->create2DTexture();
    m_texture->clear();
    m_texture->setFormat(ChannelType::Red, TexelType::UByte,
                         TexelStorageFormat::R8);
    resizeTexture(m_height);
}

void GlyphMap::clear()
{
    m_positions.clear();
    m_texture->clear();
    m_xpos = 0;
    m_ypos = 0;
    m_lineheight = 0;
    m_width = 1024;
    m_height = 8;
    resizeTexture(m_height);
}

void GlyphMap::resizeTexture(U32 height)
{
    m_height = height;
    m_texture->setSize(m_width, m_height);
}

// Pixels are stored in the texture with (0, 0) being the lower left corner.
// This means that e.g. pixel (n, 3) is stored in row #3.
void GlyphMap::addGlyph(U32 codepoint, U32 height,
                        U32 glyphIdx, FT_GlyphSlot gslot)
{
    FT_Bitmap bitmap = gslot->bitmap;
    U32 w = bitmap.width;
    U32 h = bitmap.rows;
    S32 pitch = bitmap.pitch;
    if (w+m_xpos >= m_width)
    {
        m_xpos = 0;
        m_ypos += m_lineheight;
        m_lineheight = 0;
    }
    while (h+m_ypos >= m_height)
    {
        resizeTexture(m_height << 1);
    }
    U8* texData = static_cast<U8*>(m_texture->data());
    U8* bmData = static_cast<U8*>(bitmap.buffer);

    if (pitch < 0)
    {
        for (U32 rows = 0; rows < h; ++rows)
        {
            for (U32 x = 0; x < w; ++x)
            {
                texData[(rows+m_ypos)*m_width+(m_xpos+x)] =
                    bmData[rows*pitch+x];
            }
        }
    }
    else
    {
        for (U32 rows = 0; rows < h; ++rows)
        {
            for (U32 x = 0; x < w; ++x)
            {
                texData[(rows+m_ypos)*m_width+(m_xpos+x)] =
                    bmData[(h-rows-1)*pitch+x];
            }
        }
    }

    GlyphRectangle glyphRect;
    glyphRect.x = static_cast<U16>(m_xpos);
    glyphRect.y = static_cast<U16>(m_ypos);
    glyphRect.width  = static_cast<U16>(w);
    glyphRect.height = static_cast<U16>(h);
    Glyph g;
    g.texCoords = glyphRect;

    g.glyphIndex = glyphIdx;
    // FT stores advance offsets in 1/64ths of pixels.
    g.xAdvance = static_cast<U16>(gslot->advance.x >> 6);
    g.yAdvance = static_cast<U16>(gslot->advance.y >> 6);

    // Find bottom-left position from baseline position
    g.xOffset = static_cast<S16>(gslot->bitmap_left); // Left
    g.yOffset = static_cast<S16>(gslot->bitmap_top-h); // Bottom: top - height
    m_positions[{codepoint, height}] = g;

    m_xpos += w;
    m_lineheight = std::max(m_lineheight, h);
}

FT_GlyphSlot GlyphMap::renderGlyph(FT_Face& face, U32 glyphIdx)
{
    checkFTError(FT_Load_Glyph(face, glyphIdx, FT_LOAD_DEFAULT));
    checkFTError(FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL));
    return face->glyph;
}

} // namespace freetype

} // namespace graphics

} // namespace kraken

