#include "kraken/graphics/import.hpp"

#include "kraken/graphics/mesh.hpp"
#include "kraken/io/read/ply.hpp"
#include "kraken/io/read/stl.hpp"

namespace kraken::graphics
{

[[nodiscard]] std::optional<graphics::VertexDataType> toVDataType(io::PrimitiveDataType tp)
{
    switch (tp)
    {
    case io::PrimitiveDataType::Byte: return graphics::VertexDataType::Byte;
    case io::PrimitiveDataType::UByte: return graphics::VertexDataType::UByte;
    case io::PrimitiveDataType::Short: return graphics::VertexDataType::Short;
    case io::PrimitiveDataType::UShort: return graphics::VertexDataType::UShort;
    case io::PrimitiveDataType::Int: return graphics::VertexDataType::Int;
    case io::PrimitiveDataType::UInt: return graphics::VertexDataType::UInt;
    case io::PrimitiveDataType::Float: return graphics::VertexDataType::Float;
    case io::PrimitiveDataType::Double: default:
        return std::nullopt;
    }
}

std::optional<graphics::VertexType>
computeVertexType(const io::VertexLayout& layout, io::ExtendedAttributeNames* attrNames)
{
    graphics::VertexType vertexType;
    for (const auto& attribute : layout.attributes)
    {
        std::string name;
        auto currVDataType = toVDataType(attribute.dataType);
        if (!currVDataType) return std::nullopt;
        if (const auto* canName = io::getCanonicalName(attribute.usage))
        {
            name = canName;
        }
        else if (attrNames)
        {
            auto optName = attrNames->extendedName(attribute.usage);
            if (optName) name = *optName;
        }
        else if (attribute.usage ==  io::VertexAttributeUsageType::Invalid)
        {
            name = "padding";
        }

        if (name.empty()) name = "unknown";

        vertexType.emplace_back(std::move(name),
                                *currVDataType,
                                attribute.componentCount);
    }

    return vertexType;
}

bool importPLY(std::istream& input, Mesh* meshOut)
{
    namespace ply = io::formats::ply;
    io::ExtendedAttributeNames extNames;
    ExternalMemoryAllocation vertexOutput([meshOut](std::size_t size, std::size_t)
    {
        meshOut->resizeVertexBuffer(size);
        return meshOut->vertices();
    });
    ExternalMemoryAllocation triangleOutput([meshOut](std::size_t size, std::size_t)
    {
        meshOut->resizeIndexBuffer(size);
        return meshOut->indices();
    });
    io::VertexLayout vertexLayout;
    std::size_t vertexCount;
    ply::TriangleInfo triInfo;
    ply::ElementParserFunctions parsers;
    parsers[ply::getVertexElementName()] = ply::createDefaultVertexParser
    (
        vertexOutput, vertexLayout, vertexCount, &extNames
    );
    parsers[ply::getFaceElementName()] = ply::createTriangulatingFaceParser
    (
        triangleOutput, triInfo
    );

    io::DataSource source(input);
    if (!ply::read(source, parsers)) return false;
    auto vType = computeVertexType(vertexLayout, &extNames);
    if (!vType) return false; // If no vertex type can be generated, something
                              // is wrong.

    meshOut->setIndexByteWidth(triInfo.indexByteWidth);
    meshOut->resizeIndexBuffer(triInfo.indexByteWidth * triInfo.triangleCount * 3);
    meshOut->setPrimitiveType(PrimitiveType::Triangles);
    meshOut->setVertexType(vType.value());
    meshOut->update();
    return true;
}

bool importSTL(std::istream& input, Mesh* meshOut)
{
    namespace stl = io::formats::stl;

    ExternalMemoryAllocation vertexOutput([meshOut](std::size_t size, std::size_t)
                                          {
                                              meshOut->resizeVertexBuffer(size);
                                              return meshOut->vertices();
                                          });
    io::VertexLayout vertexLayout;
    std::size_t vertexCount;

    io::DataSource source(input);
    if (!stl::read(source,
                   vertexOutput,
                   vertexCount,
                   vertexLayout)) return false;
    auto vType = computeVertexType(vertexLayout, nullptr);
    if (!vType) return false; // If no vertex type can be generated, something
                              // is wrong.

    // Trim any extra space.
    meshOut->resizeVertexBuffer(vertexSize(*vType) * vertexCount);
    meshOut->setIndexByteWidth(0);
    meshOut->resizeIndexBuffer(0);
    meshOut->setPrimitiveType(PrimitiveType::Triangles);
    meshOut->setVertexType(vType.value());
    meshOut->update();
    return true;
}

} // namespace kraken::graphics
