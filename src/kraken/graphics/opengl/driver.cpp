#include <kraken/graphics/opengl/driver.hpp>

#include <kraken/graphics/opengl/opengl.hpp>

#include <kraken/graphics/font.hpp>
#include <kraken/graphics/opengl/framebuffer.hpp>
#include <kraken/graphics/opengl/mesh.hpp>
#include <kraken/graphics/opengl/shader.hpp>
#include <kraken/graphics/opengl/texture.hpp>
#include <kraken/graphics/opengl/texturesampler.hpp>
#include <kraken/graphics/opengl/vertex.hpp>
#include <kraken/graphics/typesetting.hpp>
#include <kraken/utility/string.hpp>

#include <stdexcept>

#include <GLFW/glfw3.h>

namespace kraken
{

namespace graphics
{

namespace opengl
{

Driver::Driver(void* windowHandle)
    : m_windowHandle(windowHandle),
      m_currShader{nullptr},
      m_currReadFramebuffer{nullptr},
      m_currWriteFramebuffer{nullptr},
      m_rendermode{RenderMode::Solid},
      m_windingorder{WindingOrder::CCW},
      m_cullmode{CullMode::Back}
{
    ogl_LoadFunctions();
    ensureContextCurrent();
    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);
    glGenBuffers(2, m_buffers);

    clearAllTextureUnits();

    m_width = 0;
    m_height = 0;

    glEnable(GL_CULL_FACE);

    setCullMode(CullMode::Back);
    setWindingOrder(WindingOrder::CCW);

    disable(Capability::AlphaTest);
    disable(Capability::DepthTest);

    // TODO: Add these as options.
    glDepthMask(GL_TRUE);
    glDepthFunc(GL_LEQUAL);
    glDepthRange(0.f, 1.f);
    glPointSize(2.0f);
    glLineWidth(1.0f);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    checkGLError();

    m_textShader = std::unique_ptr<Shader>(new Shader(*this));;
    m_textShader->addShader(R"***(#version 330 core

layout (location = 0) in ivec2 position;
// Contains texPos[x0, y0]; [width, height];
// x0 is stored in the two MSBs and y0 in the two LSBs.
layout (location = 1) in uvec2 texData;

uniform vec2 screenSize;
uniform vec2 glyphMapSize;
uniform vec2 offset;

out VertexData
{
    vec4 screenRect;
    vec4 texRect;
} vOut;

void main()
{
    vec2 blPos = vec2(float(position.x), float(position.y)) + offset;
    vec2 texPos = vec2(float(texData.x>>16), float(texData.x&0xffffu));
    vec2 dim = vec2(float(texData.y>>16), float(texData.y&0xffffu));
    vOut.screenRect.xy = (2.f * blPos - screenSize) / screenSize;
    vOut.screenRect.zw = (2.f * (blPos + dim) - screenSize) / screenSize;
    vOut.texRect.xy = texPos / glyphMapSize;
    vOut.texRect.zw = (texPos + dim) / glyphMapSize;
})***", Shader::Vertex);

    m_textShader->addShader(R"***(#version 330 core

layout(points) in;
layout(triangle_strip, max_vertices=4) out;

in VertexData
{
    vec4 screenRect; // x0, y0, x1, y1
    vec4 texRect; // x0, y0, x1, y1
} vertex[];

/*
x0,y1  x1,y1
 +-------+
 |      /|
 |    /  |
 |  /    |
 |/      |
 +-------+
x0,y0  x1,y0
*/

out FragmentData
{
    smooth vec2 tcoord;
} fragData;

void main()
{
    fragData.tcoord = vertex[0].texRect.zy;
    gl_Position = vec4(vertex[0].screenRect.zy, 0.f, 1.f);
    EmitVertex();
    fragData.tcoord = vertex[0].texRect.zw;
    gl_Position = vec4(vertex[0].screenRect.zw, 0.f, 1.f);
    EmitVertex();
    fragData.tcoord = vertex[0].texRect.xy;
    gl_Position = vec4(vertex[0].screenRect.xy, 0.f, 1.f);
    EmitVertex();
    fragData.tcoord = vertex[0].texRect.xw;
    gl_Position = vec4(vertex[0].screenRect.xw, 0.f, 1.f);
    EmitVertex();
})***", Shader::Geometry);

    m_textShader->addShader(R"***(#version 330 core

out vec4 colour;

uniform sampler2D textTexture;
uniform vec3 texCol;

in FragmentData
{
    smooth vec2 tcoord;
};

void main()
{
    colour = vec4(texCol, texture(textTexture, tcoord).x);
})***", Shader::Fragment);
    if (!m_textShader->compile())
    {
        throw std::logic_error("Text shader compilation failed.");
    }
}

Driver::~Driver()
{
    glDeleteBuffers(2, m_buffers);
    glDeleteVertexArrays(1, &m_vao);
}

bool Driver::beginDraw(bool clearCBuffer,
                       bool clearZBuffer,
                       image::Colour background,
                       image::ColourType colourType)
{
    ensureContextCurrent();
    auto bkgd = image::unpack(background, colourType);
    glViewport(0, 0, width(), height());
    U32 clear = 0;
    if (clearCBuffer)
    {
        clear |= GL_COLOR_BUFFER_BIT;
        glClearColor(bkgd.r, bkgd.g, bkgd.b, bkgd.a);
    }
    if (clearZBuffer)
    {
        clear |= GL_DEPTH_BUFFER_BIT;
        glClearDepth(1.f);
    }
    glClear(clear);
    checkGLError();
    return true;
}

bool Driver::endDraw()
{
    if (isRenderingText()) endTextRendering();
    if (m_currWriteFramebuffer)
    {
        useFramebuffer(nullptr, false, true);
    }
    glfwSwapBuffers(static_cast<GLFWwindow*>(m_windowHandle));
    checkGLError();

    return true;
}

void Driver::onFramebufferResize(U32 newX, U32 newY)
{
    ensureContextCurrent();
    m_width = newX;
    m_height = newY;
    glViewport(0, 0, newX, newY);
}

// We cannot use make_unique due to the constructors being private (but with
// Driver a friend of all classes, we can construct it here).
std::unique_ptr<graphics::Framebuffer> Driver::createFramebuffer(math::uvec2 dim)
{
    ensureContextCurrent();
    return std::unique_ptr<graphics::Framebuffer>(new Framebuffer(this, dim));
}

std::unique_ptr<graphics::Mesh> Driver::createMesh()
{
    ensureContextCurrent();
    return std::unique_ptr<graphics::Mesh>(new Mesh());
}

std::unique_ptr<graphics::Shader> Driver::createShader()
{
    ensureContextCurrent();
    return std::unique_ptr<graphics::Shader>(new Shader(*this));
}

std::unique_ptr<graphics::Texture1D> Driver::create1DTexture()
{
    ensureContextCurrent();
    return std::unique_ptr<graphics::Texture1D>(new Texture1D());
}

std::unique_ptr<graphics::Texture2D> Driver::create2DTexture()
{
    ensureContextCurrent();
    return std::unique_ptr<graphics::Texture2D>(new Texture2D());
}

std::unique_ptr<graphics::Texture3D> Driver::create3DTexture()
{
    ensureContextCurrent();
    return std::unique_ptr<graphics::Texture3D>(new Texture3D());
}

std::unique_ptr<graphics::TextureSampler> Driver::createTextureSampler()
{
    ensureContextCurrent();
    return std::unique_ptr<graphics::TextureSampler>(new TextureSampler());
}

void Driver::enable(Capability opt)
{
    m_capability[opt] = true;
    GLenum cap = GL_NONE;
    switch (opt)
    {
    case Capability::DepthTest: cap = GL_DEPTH_TEST; break;
    case Capability::AlphaTest: cap = GL_BLEND;      break;
    default:
        throw std::domain_error("Invalid option.");
    }
    glEnable(cap);
    checkGLError();
}

void Driver::disable(Capability opt)
{
    m_capability[opt] = false;
    GLenum cap = GL_NONE;
    switch (opt)
    {
    case Capability::DepthTest: cap = GL_DEPTH_TEST; break;
    case Capability::AlphaTest: cap = GL_BLEND;      break;
    default:
        throw std::domain_error("Invalid option.");
    }
    glDisable(cap);
    checkGLError();
}

bool Driver::isEnabled(Capability opt) const
{
    return m_capability.at(opt);
}

void Driver::setWindingOrder(WindingOrder order)
{
    switch (order)
    {
    case WindingOrder::CW:
        glFrontFace(GL_CW);
        break;

    case WindingOrder::CCW:
        glFrontFace(GL_CCW);
        break;

    default:
        throw std::domain_error("Invalid winding order.");
    }
    checkGLError();
    m_windingorder = order;
}

void Driver::setRenderMode(RenderMode rm)
{
    switch (rm)
    {
    case RenderMode::Solid:
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        break;

    case RenderMode::Wireframe:
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        break;

    case RenderMode::Points:
        glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
        break;

    default:
        throw std::domain_error("Invalid render mode.");
    }
    checkGLError();
    m_rendermode = rm;
}

void Driver::setCullMode(CullMode cm)
{
    if (cm == m_cullmode) return;

    GLenum newCullMode;
    switch (cm)
    {
    case CullMode::Front:        newCullMode = GL_FRONT;          break;
    case CullMode::Back:         newCullMode = GL_BACK;           break;
    case CullMode::FrontAndBack: newCullMode = GL_FRONT_AND_BACK; break;
    case CullMode::None:         break;
    default:                     throw std::domain_error("Invalid cull mode.");
    }

    bool currentlyCulling = m_cullmode != CullMode::None;
    bool shouldCull = cm != CullMode::None;

    if (shouldCull)
    {
        glCullFace(newCullMode);
    }

    if (shouldCull && !currentlyCulling)
    {
        glEnable(GL_CULL_FACE);
    }
    if (!shouldCull && currentlyCulling)
    {
        glDisable(GL_CULL_FACE);
    }

    checkGLError();
    m_cullmode = cm;
}

void Driver::ensureContextCurrent()
{
    auto* wnd = static_cast<GLFWwindow*>(m_windowHandle);
    if (glfwGetCurrentContext() != wnd)
    {
        glfwMakeContextCurrent(wnd);
    }
}

void Driver::setupTextRendering()
{
    auto* shader = m_currShader;
    use(m_textShader.get());
    auto currCullMode = cullMode();
    auto currRendermode = renderMode();
    bool depthTesting = isEnabled(Capability::DepthTest);
    bool alphaTesting = isEnabled(Capability::AlphaTest);
    m_fontRestore = [this, shader,
                     currCullMode, currRendermode, depthTesting,alphaTesting]()
        {
            this->setCullMode(currCullMode);
            this->setRenderMode(currRendermode);
            if (depthTesting) this->enable(Capability::DepthTest);
            else this->disable(Capability::DepthTest);
            if (alphaTesting) this->enable(Capability::AlphaTest);
            else this->disable(Capability::AlphaTest);
            this->use(shader);
        };
    setCullMode(CullMode::Back);
    setRenderMode(RenderMode::Solid);
    disable(Capability::DepthTest);
    enable(Capability::AlphaTest);

    m_textShader->setUniform(m_textShader->uniformID("screenSize"),
                             math::vec2{(F32)width(), (F32)height()});
}

void Driver::drawText(const TextGeometry& geom, math::ivec2 offset)
{
    m_textShader->setUniform(m_textShader->uniformID("offset"),
                             math::vec2{(F32)offset.x, (F32)height()-offset.y});
    for (const auto& partialGeometry : geom.geometry)
    {
        partialGeometry.font->setupRendering(m_textShader.get());
        partialGeometry.mesh->draw(this, partialGeometry.count);
    }
}

void Driver::onEndTextRendering()
{
    m_fontRestore();
}

void Driver::drawVertices(const void* vertices,
                          VertexType vertexType,
                          U64 vertexCount,
                          const void* indices,
                          U64 indexByteWidth,
                          U64 indexCount,
                          PrimitiveType pType)
{
    if (!indices)
    {
        drawVertices(vertices, vertexType, vertexCount, pType);
        return;
    }
    GLenum iType;
    switch (indexByteWidth)
    {
    case 1:  iType = GL_UNSIGNED_BYTE;  break;
    case 2: iType = GL_UNSIGNED_SHORT; break;
    case 4: iType = GL_UNSIGNED_INT;   break;
    default:
        throw std::runtime_error("Index width must be 1, 2 or 4 bytes.");
    }

    glBindBuffer(GL_ARRAY_BUFFER, m_buffers[0]);
    glBufferData(GL_ARRAY_BUFFER,
                 vertexSize(vertexType) * vertexCount,
                 vertices,
                 GL_STREAM_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buffers[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 static_cast<GLsizei>(indexByteWidth * indexCount),
                 indices,
                 GL_STREAM_DRAW);
    setupVertexRendering(vertexType, this);

    glDrawElements(primitiveTypeToGL(pType),
                   static_cast<GLsizei>(indexCount),
                   iType,
                   0);

    clearVertexRendering(vertexType, this);
}

void Driver::drawVertices(const void* vertices,
                          VertexType vertexType,
                          U64 vertexCount,
                          PrimitiveType pType)
{
    glBindBuffer(GL_ARRAY_BUFFER, m_buffers[0]);
    glBufferData(GL_ARRAY_BUFFER,
                 vertexSize(vertexType) * vertexCount,
                 vertices,
                 GL_STREAM_DRAW);

    setupVertexRendering(vertexType, this);

    glDrawArrays(primitiveTypeToGL(pType),
                 0,
                 static_cast<GLsizei>(vertexCount));

    clearVertexRendering(vertexType, this);
}

bool Driver::useFramebuffer(graphics::Framebuffer* buffer,
                            bool useAsInput, bool useAsOutput)
{
    auto* glbuf = downcast<opengl::Framebuffer>(buffer);
    bool changeInput = (glbuf != m_currReadFramebuffer) && useAsInput;
    bool changeOutput = (glbuf != m_currWriteFramebuffer) && useAsOutput;
    if (!changeInput && !changeOutput) return true;

    if (m_currReadFramebuffer == m_currWriteFramebuffer
        && m_currReadFramebuffer)
    {
        m_currReadFramebuffer->endRendering();
    }
    else
    {
        if (changeInput && m_currReadFramebuffer)
        {
            m_currReadFramebuffer->endRendering();
        }
        if (changeOutput && m_currWriteFramebuffer)
        {
            m_currWriteFramebuffer->endRendering();
        }
    }
    if (glbuf)
    {
        glbuf->setupRendering(0xffffffffu, useAsInput, useAsOutput);
    }
    else
    {
        if (changeOutput) glViewport(0, 0, width(), height());
    }
    if (changeInput) m_currReadFramebuffer = glbuf;
    if (changeOutput) m_currWriteFramebuffer = glbuf;
    return true;
}

const graphics::Framebuffer* Driver::currentReadFramebuffer() const
{
    return m_currReadFramebuffer;
}

graphics::Framebuffer* Driver::currentReadFramebuffer()
{
    return m_currReadFramebuffer;
}

const graphics::Framebuffer* Driver::currentWriteFramebuffer() const
{
    return m_currWriteFramebuffer;
}

graphics::Framebuffer* Driver::currentWriteFramebuffer()
{
    return m_currWriteFramebuffer;
}

GLint Driver::getTextureUnit(GLuint texture)
{
    for (GLint unit = 1; unit <= static_cast<GLint>(m_texbinds.size()); ++unit)
    {
        if (m_texbinds[unit-1].texture == texture) return unit;
    }
    return 0;
}

void Driver::takeOwnership(GLuint shader, GLuint shaderLoc, GLuint textureUnit)
{
    m_texbinds[textureUnit-1].shader = shader;
    m_texbinds[textureUnit-1].loc = shaderLoc;
}

GLint Driver::getTextureUnit(GLuint shader, GLuint shaderLoc, GLuint texture)
{
    GLint freeUnit = 0;
    for (GLint unit = 1; unit <= static_cast<GLint>(m_texbinds.size()); ++unit)
    {
        if (m_texbinds[unit-1].shader == 0)
        {
            freeUnit = unit;
        }
        if (m_texbinds[unit-1].texture == texture)
        {
            takeOwnership(shader, shaderLoc, unit);
            return unit;
        }
    }
    if (!freeUnit) for (GLint unit = 1; unit <= static_cast<GLint>(m_texbinds.size()); ++unit)
    {
        if (m_texbinds[unit-1].shader != shader)
        {
            freeUnit = unit;
            break;
        }
    }
    if (!freeUnit)
    {
        throw std::runtime_error("No free texture unit!");
    }
    takeOwnership(shader, shaderLoc, freeUnit);
    m_texbinds[freeUnit-1].texture = texture;
    return freeUnit;
}

void Driver::clearSingleTextureUnit(GLuint texture)
{
    auto unit = getTextureUnit(texture);
    if (unit) m_texbinds[unit-1] = {0, 0, 0};
}

void Driver::clearAllTextureUnits(GLuint shader)
{
    for (GLint unit = 1; unit <= static_cast<GLint>(m_texbinds.size()); ++unit)
    {
        if (m_texbinds[unit-1].shader == shader)
        {
            m_texbinds[unit-1] = {0, 0, 0};
        }
    }
}

void Driver::clearAllTextureUnits()
{
    for (GLint unit = 1; unit <= static_cast<GLint>(m_texbinds.size()); ++unit)
    {
        m_texbinds[unit-1] = {0, 0, 0};
    }
}

} // namespace opengl

} // namespace graphics

} // namespace kraken
