#include <kraken/graphics/opengl/framebuffer.hpp>
#include <kraken/graphics/opengl/driver.hpp>
#include <kraken/utility/memory.hpp>
#include <stdexcept>

namespace kraken
{

namespace graphics
{

namespace opengl
{

Framebuffer::Framebuffer(Driver* driver, math::uvec2 dims)
    : m_dims{0, 0}, m_driver{driver}, m_attachMask{0}, m_target{GL_FRAMEBUFFER}
{
    glGenFramebuffers(1, &m_fbo);
    GLint maxColbufs = 0;
    GLint maxDrawbuffers = 8;
    glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &maxColbufs);
    glGetIntegerv(GL_MAX_DRAW_BUFFERS, &maxDrawbuffers);
    checkGLError();
    if (maxColbufs < 8)
    {
        throw std::logic_error("OpenGL guarantees >= 8 colour buffers.");
    }
    if (maxDrawbuffers < 8)
    {
        throw std::logic_error("OpenGL guarantees >= 8 buffer attachments.");
    }
    m_maxColbufs = maxColbufs;
    m_maxDraw = maxDrawbuffers;
    if (m_maxDraw > 32-3) m_maxDraw = 32-3;
    m_attachments.resize(m_maxColbufs + 3, {0, 0, 0, 0});
    m_fbTextures.resize(m_attachments.size());
    resize(dims.x, dims.y);
}

Framebuffer::~Framebuffer()
{
    for (auto& attachment : m_attachments)
    {
        if (attachment.id)
        {
            glDeleteTextures(1, &attachment.id);
        }
    }
    glDeleteFramebuffers(1, &m_fbo);
}

void Framebuffer::resize(U32 newWidth, U32 newHeight)
{
    if (width() == newWidth && height() == newHeight) return;
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    glViewport(0, 0, newWidth, newHeight);
    m_dims = {newWidth, newHeight};
    for (U32 i = 0; i < m_attachments.size(); ++i)
    {
        if (!(m_attachMask & ((U32)1 << i))) continue;
        glBindTexture(GL_TEXTURE_2D, m_attachments[i].id);
        updateTexture2D(m_attachments[i]);
    }
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    checkGLError();
}

bool Framebuffer::createAttachment(U32 location, FramebufferFormat fmt)
{
    if (location >= m_attachments.size()) return false;
    bool isColourFormat = true;

    AttachedTexture at{0, 0, 0, 0};
    switch (fmt)
    {
    case FramebufferFormat::ColourRGBA8:
        at.internalFormat = GL_RGBA8;
        at.format = GL_RGBA;
        at.type = GL_UNSIGNED_BYTE;
        break;
    case FramebufferFormat::Depth24Stencil8:
        isColourFormat = false;
        location = m_maxColbufs;
        at.internalFormat = GL_DEPTH24_STENCIL8;
        at.format = GL_DEPTH_STENCIL;
        at.type = GL_UNSIGNED_INT_24_8;
        break;
    }
    if (isColourFormat && location >= m_maxColbufs)
    {
        return false;
    }
    U32 locmask = U32(1) << location;
    if (m_attachMask & locmask)
    {
        removeAttachment(location);
        at.id = m_attachments[location].id;
    }
    m_attachments[location] = at;
    if (!m_attachments[location].id)
    {
        glGenTextures(1, &m_attachments[location].id);
    }
    glBindTexture(GL_TEXTURE_2D, m_attachments[location].id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    updateTexture2D(at);
    m_attachMask |= locmask;

    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, locationToAttachmentTarget(location),
                           GL_TEXTURE_2D, m_attachments[location].id, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    checkGLError();
    return true;
}

const graphics::FramebufferTexture* Framebuffer::attachmentTexture(U32 location) const
{
    if (!isAttached(location)) return nullptr;
    m_fbTextures[location].texture = m_attachments[location].id;
    return &m_fbTextures[location];
}

void Framebuffer::removeAttachment(U32 location)
{
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    glFramebufferTexture(GL_FRAMEBUFFER, locationToAttachmentTarget(location),
                         0, 0);
    U32 locmask = U32(1) << location;
    m_attachMask &= ~locmask;
    checkGLError();
}

void Framebuffer::clearAttachment(U32 id, const void* values, S32 stencil,
                                  AttachmentClearType type)
{
    GLenum buf = GL_COLOR;
    if (id >= m_maxColbufs)
    {
        if (id == depthAttachmentID()) buf = GL_DEPTH;
        else if (id == stencilAttachmentID()) buf = GL_STENCIL;
        else if (id == depthStencilAttachmentID()) buf = GL_DEPTH_STENCIL;
        else
        {
            throw std::runtime_error("Cannot clear non-existing buffer.");
        }
    }
    if (buf == GL_DEPTH && type != AttachmentClearType::Float)
    {
        throw std::runtime_error("Must clear depth buffer with float.");
    }
    if (buf == GL_STENCIL && type != AttachmentClearType::Int)
    {
        throw std::runtime_error("Must clear stencil buffer with int.");
    }
    if (buf == GL_DEPTH_STENCIL && type != AttachmentClearType::DepthStencil)
    {
        throw std::runtime_error("Must clear DS-buffer with DS.");
    }
    if (buf != GL_DEPTH_STENCIL)
    {
        GLint drawBuf = id;
        if (buf != GL_COLOR) drawBuf = 0;
        if (type == AttachmentClearType::Unsigned)
        {
            glClearBufferuiv(buf, drawBuf, static_cast<const GLuint*>(values));
        }
        else if (type == AttachmentClearType::Int)
        {
            glClearBufferiv(buf, drawBuf, static_cast<const GLint*>(values));
        }
        else if (type == AttachmentClearType::Float)
        {
            glClearBufferfv(buf, drawBuf, static_cast<const GLfloat*>(values));
        }
        else
        {
            throw std::runtime_error("Cannot clear non DS-buffer with DS.");
        }
    }
    else
    {
        glClearBufferfi(GL_DEPTH_STENCIL, 0,
                        *static_cast<const GLfloat*>(values), stencil);
    }
}

void Framebuffer::setupRendering(U32 attachMask, bool asInput, bool asOutput)
{
    attachMask &= m_attachMask;
    m_target = GL_FRAMEBUFFER;
    if (!asInput) m_target = GL_DRAW_FRAMEBUFFER;
    if (!asOutput) m_target = GL_READ_FRAMEBUFFER;
    glBindFramebuffer(m_target, m_fbo);
    auto status = glCheckFramebufferStatus(m_target);
    if (status != GL_FRAMEBUFFER_COMPLETE)
    {
        switch (status)
        {
        case GL_FRAMEBUFFER_UNDEFINED:
            throw std::logic_error("GL_FRAMEBUFFER_UNDEFINED received.");
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
            throw std::logic_error("GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT received.");
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
            throw std::runtime_error("No attachments to framebuffer.");
        case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
            throw std::runtime_error("No colour attachment for read buffer.");
        case GL_FRAMEBUFFER_UNSUPPORTED:
            throw std::domain_error("Unsupported framebuffer formats.");
        case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
            throw std::runtime_error("Multisampled framebuffer inconsistency.");
        case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
            throw std::runtime_error("Framebuffer layering inconsistency.");
        default:
            throw std::logic_error("And now for something completely different:"
                                   " An unknown framebuffer error returned by"
                                   " OpenGL.");
        }
    }
    std::vector<GLenum> drawInto;
    for (U32 i = 0; i < maxColourAttachments(); ++i, attachMask>>=1)
    {
        if (attachMask&1) drawInto.push_back(GL_COLOR_ATTACHMENT0+i);
    }
    glDrawBuffers(drawInto.size(), drawInto.data());
    checkGLError();
    glViewport(0, 0, width(), height());
}

void Framebuffer::endRendering()
{
    glBindFramebuffer(m_target, 0);
}


void Framebuffer::updateTexture2D(const AttachedTexture& params)
{
    glTexImage2D(GL_TEXTURE_2D, 0, params.internalFormat, width(), height(), 0,
                 params.format, params.type, NULL);
}

GLenum Framebuffer::locationToAttachmentTarget(U32 location) const
{
    if (location < m_maxColbufs)
    {
        return GL_COLOR_ATTACHMENT0 + location;
    }
    else if (location == depthStencilAttachmentID())
    {
        return GL_DEPTH_STENCIL_ATTACHMENT;
    }
    else if (location == depthAttachmentID())
    {
        return GL_DEPTH_ATTACHMENT;
    }
    else if (location == stencilAttachmentID())
    {
        return GL_STENCIL_ATTACHMENT;
    }
    else
    {
        throw std::logic_error("Bad location... Dispatching drones.");
    }
}
} // namespace opengl

} // namespace graphics

} // namespace kraken
