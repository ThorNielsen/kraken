#include <kraken/graphics/opengl/mesh.hpp>
#include <kraken/graphics/opengl/vertex.hpp>

#include <cstring>
#include <stdexcept>

namespace kraken
{

namespace graphics
{

namespace opengl
{

Mesh::Mesh()
    : m_vType(), m_buf{}, m_vData{nullptr}, m_iData{nullptr}, m_vAlloc{0},
      m_iAlloc{0}, m_vElemSize{0}, m_iElemSize{1}, m_vCount{0}, m_iCount{0},
      m_pType{}, initialised{true}
{
    glGenBuffers(2, m_buf);
}

Mesh::~Mesh()
{
    if (m_vData) deleteUntyped(m_vData);
    if (m_iData) deleteUntyped(m_iData);
    if (initialised)
    {
        glDeleteBuffers(2, m_buf);
    }
}

void Mesh::resizeBuffer(U64 newSz, U8*& data, U64& allocCnt,
                        U64& elemCount, U64 elemSize)
{
    if (newSz == allocCnt) return;

    U8* newBuf = nullptr;
    if (newSz) newBuf = static_cast<U8*>(allocateUntyped(newSz));
    if (data && newSz)
    {
        std::memcpy(newBuf, data, std::min(allocCnt, newSz));
        deleteUntyped(data);
    }
    data = newBuf;
    allocCnt = newSz;
    if (elemSize) elemCount = newSz / elemSize;
    else elemCount = 0;
}

void Mesh::construct(const void* vertices_,
                     U64 vertexCount_,
                     const VertexType& vertexType_,
                     PrimitiveType primitiveType_,
                     const void* indices_,
                     U64 indexByteWidth_,
                     U64 indexCount_)
{
    clear();
    setVertexType(vertexType_);
    setPrimitiveType(primitiveType_);
    append(vertices_,
           vertexCount_,
           indices_,
           indexByteWidth_,
           indexCount_,
           false);
    update();
}

void Mesh::append(const void* vertices_,
                  U64 vertexCount_,
                  const void* indices_,
                  U64 indexByteWidth_,
                  U64 indexCount_,
                  bool indexIntoPrevious)
{
    if (!vertexCount_ && !indexCount_) return;
    U64 cVertices = vertexCount();
    resizeVertexBuffer((cVertices+vertexCount_) * m_vElemSize);
    U8* insertAt = m_vData + cVertices*m_vElemSize;
    std::memcpy(insertAt, vertices_, vertexCount_ * m_vElemSize);

    if (indices_ && indexByteWidth_ && indexCount_)
    {
        U64 cIndices = indexCount();
        if (!usingIndices() && cVertices)
        {
            constructIndices();
        }
        if (vertexCount() > maxIndex())
        {
            widenIndicesToFit(vertexCount());
        }

        resizeIndexBuffer((cIndices+indexCount_) * m_iElemSize);
        insertAt = m_iData + cIndices;

        if (m_iElemSize == indexByteWidth_ && indexIntoPrevious)
        {
            std::memcpy(insertAt, indices_, indexCount_ * m_iElemSize);
        }
        else
        {
            U64 add = 0;
            if (!indexIntoPrevious) add = cIndices;
            for (U64 i = 0; i < indexCount_; ++i)
            {
                appendIndex(indices_, indexByteWidth_, insertAt, i, add);
            }
        }
    }
}

void Mesh::appendIndex(const void* src, U64 srcWidth,
                       void* dest, U64 i, U64 add)
{
    U64 appended = add;
    switch (srcWidth)
    {
    case 1: appended += reinterpret_cast<const U8*>(src)[i]; break;
    case 2: appended += reinterpret_cast<const U16*>(src)[i]; break;
    case 4: appended += reinterpret_cast<const U32*>(src)[i]; break;
    default:
        ; // Only the above options are possible, but compiler wants default.
    }
    switch (m_iElemSize)
    {
    case 1: reinterpret_cast<U8*>(dest)[i] = static_cast<U8>(appended); break;
    case 2: reinterpret_cast<U16*>(dest)[i] = static_cast<U16>(appended); break;
    case 4: reinterpret_cast<U32*>(dest)[i] = static_cast<U32>(appended); break;
    default:
        ; // Only the above options are possible, but compiler wants default.
    }
}

void Mesh::constructIndices()
{
    m_iCount = m_vCount;
    if (m_vCount <= 0xff)
    {
        m_iElemSize = 1;
        resizeIndexBuffer(m_iElemSize * m_iCount);
        U8* iBuf = static_cast<U8*>(m_iData);
        for (size_t i = 0; i < m_vCount; ++i)
        {
            iBuf[i] = static_cast<U8>(i);
        }
    }
    else if (m_vCount <= 0xffff)
    {
        m_iElemSize = 2;
        resizeIndexBuffer(m_iElemSize * m_iCount);
        U16* iBuf = static_cast<U16*>(static_cast<void*>(m_iData));
        for (size_t i = 0; i < m_vCount; ++i)
        {
            iBuf[i] = static_cast<U16>(i);
        }
    }
    else if (m_vCount <= 0xffffffff)
    {
        m_iElemSize = 4;
        resizeIndexBuffer(m_iElemSize * m_iCount);
        U32* iBuf = static_cast<U32*>(static_cast<void*>(m_iData));
        for (size_t i = 0; i < m_vCount; ++i)
        {
            iBuf[i] = static_cast<U32>(i);
        }
    }
    else throw std::logic_error("Too many vertices.");
}

void Mesh::setVertexType(const VertexType& vType)
{
    m_vType = vType;
    m_vElemSize = vertexSize(vType);
    if (!m_vElemSize)
    {
        throw std::logic_error("Invalid vertex type.");
    }
    m_vCount = m_vAlloc / m_vElemSize;
}

void Mesh::setIndexByteWidth(U64 bytes)
{
    m_iElemSize = bytes;
    m_iCount = bytes ? m_iAlloc / m_iElemSize : 0;
}

void Mesh::clear()
{
    bool updateBuffers = m_vElemSize > 0;
    setIndexByteWidth(1);
    resizeVertexBuffer(0);
    resizeIndexBuffer(0);
    if (updateBuffers) update(); // Clears buffers
}

void Mesh::update()
{
    glBindBuffer(GL_ARRAY_BUFFER, m_buf[0]);
    glBufferData(GL_ARRAY_BUFFER, m_vElemSize * m_vCount,
                 m_vData, GL_STATIC_DRAW);

    if (m_iCount)
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buf[1]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_iElemSize * m_iCount,
                     m_iData, GL_STATIC_DRAW);
    }
}

void Mesh::copyByteShort(U16* newBuf)
{
    U8* cBuf = static_cast<U8*>(m_iData);
    for (U64 i = 0; i < m_iCount; ++i)
    {
        newBuf[i] = cBuf[i];
    }
}

void Mesh::copyByteInt(U32* newBuf)
{
    U8* cBuf = static_cast<U8*>(m_iData);
    for (U64 i = 0; i < m_iCount; ++i)
    {
        newBuf[i] = cBuf[i];
    }
}

void Mesh::copyShortInt(U32* newBuf)
{
    U16* cBuf = static_cast<U16*>(static_cast<void*>(m_iData));
    for (U64 i = 0; i < m_iCount; ++i)
    {
        newBuf[i] = cBuf[i];
    }
}

void Mesh::widenIndices(U64 newByteWidth)
{
    if (!m_iData)
    {
        m_iElemSize = newByteWidth;
        return;
    }

    if (newByteWidth <= m_iElemSize) return;
    void* newBuf = allocateUntyped(newByteWidth * m_iCount);
    if (m_iElemSize == 1 && newByteWidth == 2)
    {
        copyByteShort(static_cast<U16*>(newBuf));
    }
    else if (m_iElemSize == 1 && newByteWidth == 4)
    {
        copyByteInt(static_cast<U32*>(newBuf));
    }
    else if (m_iElemSize == 2 && newByteWidth == 4)
    {
        copyShortInt(static_cast<U32*>(newBuf));
    }
    else
    {
        throw std::logic_error("Index width combination unsupported.");
    }

    m_iElemSize = newByteWidth;
    deleteUntyped(m_iData);
    m_iData = static_cast<U8*>(newBuf);
}

void Mesh::widenIndicesToFit(U64 index)
{
    if (index <= 0xff) widenIndices(1);
    else if (index <= 0xffff) widenIndices(2);
    else if (index <= 0xffffffff) widenIndices(4);
    else throw std::logic_error("Too large index.");
}

U64 Mesh::maxIndex() const
{
    if (m_iElemSize == 1) return 0xff;
    if (m_iElemSize == 2) return 0xffff;
    if (m_iElemSize == 4) return 0xffffffff;
    throw std::logic_error("Bad index width.");
}

void Mesh::draw(VideoDriver* drv, U32 count)
{
    if (!indexCount()) drawSimple(downcast<Driver>(drv), count);
    else drawInstanced(downcast<Driver>(drv), count);
    checkGLError();
}

void Mesh::drawSimple(Driver* drv, U32 count)
{
    if (!count || count > vertexCount()) count = vertexCount();
    glBindBuffer(GL_ARRAY_BUFFER, m_buf[0]);
    setupVertexRendering(m_vType, drv);
    glDrawArrays(primitiveTypeToGL(m_pType),
                 0,
                 static_cast<GLsizei>(count));
    clearVertexRendering(m_vType, drv);
}

void Mesh::drawInstanced(Driver* drv, U32 count)
{
    if (!count || count > indexCount()) count = indexCount();
    glBindBuffer(GL_ARRAY_BUFFER, m_buf[0]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buf[1]);
    setupVertexRendering(m_vType, drv);
    GLenum iType;
    switch (m_iElemSize)
    {
    case 1: iType = GL_UNSIGNED_BYTE;  break;
    case 2: iType = GL_UNSIGNED_SHORT; break;
    case 4: iType = GL_UNSIGNED_INT;   break;
    default:
        throw std::logic_error("Bad index width.");
    }
    glDrawElements(primitiveTypeToGL(m_pType),
                   static_cast<GLsizei>(count),
                   iType,
                   0);

    clearVertexRendering(m_vType, drv);
}

} // namespace opengl

} // namespace graphics

} // namespace kraken
