#include <kraken/graphics/opengl/opengl.hpp>

#include <cstdlib>
#include <iostream>
#include <string>

namespace kraken
{

namespace graphics
{

namespace opengl
{

void printGLError(GLint err, std::string func, std::string file, int line)
{
    std::cerr << "GLError (" << func << " [" << file << ":" << line << "]): ";
    switch(err)
    {
    // Wrong input passed to function, either a user that has called this
    // function directly, a new OpenGL error code or memory corruption or
    // perhaps something else entirely.
    default:
        std::cerr << "Bad OpenGL error code (" << err << ")!\n";
        std::exit(11);
        break;
    // Programming error in error handling.
    case GL_NO_ERROR:
        std::cerr << "No error registered as error!\n";
        std::exit(12);
        break;
    // No attempt is made at handling out of memory issues - results of commands
    // are undefined (at least all commands causing an out-of-memory-error), so
    // OpenGL's state is undefined.
    case GL_OUT_OF_MEMORY:
        std::cerr << "OpenGL is out of memory!\n";
        std::exit(10);
        break;

    // "Normal" errors.
    case GL_INVALID_ENUM:
        std::cerr << "GL_INVALID_ENUM\n";
        break;
    case GL_INVALID_VALUE:
        std::cerr << "GL_INVALID_VALUE\n";
        break;
    case GL_INVALID_OPERATION:
        std::cerr << "GL_INVALID_OPERATION\n";
        break;
    case GL_INVALID_FRAMEBUFFER_OPERATION:
    std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION\n";
        break;
    }
}

} // namespace opengl

} // namespace graphics

} // namespace kraken
