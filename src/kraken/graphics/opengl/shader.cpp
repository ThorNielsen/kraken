#include <kraken/graphics/opengl/shader.hpp>

#include <kraken/graphics/opengl/driver.hpp>

#include <iostream>
#include <stdexcept>

namespace kraken
{

namespace graphics
{

namespace opengl
{

Shader::Shader(Driver& drv)
    : m_shaders{},
      m_unifinfo{},
      m_driver(drv),
      m_program{},
      m_linked{false}
{
    m_program = glCreateProgram();
    checkGLError();
}

Shader::~Shader()
{
    for (GLuint s : m_shaders)
    {
        glDeleteShader(s);
    }
    glDeleteProgram(m_program);
}

bool Shader::use()
{
    if (m_linked)
    {
        glUseProgram(m_program);
        checkGLError();
    }
    return m_linked;
}

bool Shader::compile()
{
    for (GLuint s : m_shaders)
    {
        glAttachShader(m_program, s);
    }
    glLinkProgram(m_program);

    for (GLuint s : m_shaders)
    {
        glDetachShader(m_program, s);
        glDeleteShader(s);
    }
    m_shaders.clear();

    GLint log_length;
    glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &log_length);
    if (log_length > 1)
    {
        std::string log;
        log.resize(log_length);
        glGetProgramInfoLog(m_program, log_length, nullptr, &log[0]);
        std::cerr << "Shader program compilation error:\n" << log << "\n";
        m_linked = false;
        checkGLError();
        return false;
    }
    else
    {
        m_linked = true;
        readAttributes();
        readUniforms();
    }
    checkGLError();
    return true;
}


bool Shader::addShader(const std::string& code, Stage stage)
{
    GLuint oglType;
    switch (stage)
    {
    case Stage::Vertex: oglType = GL_VERTEX_SHADER; break;
    case Stage::Geometry: oglType = GL_GEOMETRY_SHADER; break;
    case Stage::Fragment: oglType = GL_FRAGMENT_SHADER; break;
    default:
        throw std::logic_error("Invalid shader stage.");
    }
    GLuint shader = glCreateShader(oglType);
    const char* cstr = code.c_str();
    glShaderSource(shader, 1, &cstr, nullptr);
    glCompileShader(shader);

    GLint logLength;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 1)
    {
        std::string log;
        log.resize(logLength);
        glGetShaderInfoLog(shader, logLength, nullptr, &log[0]);
        std::cerr << "Shader compilation error:\n" << log << "\n";
        std::cerr << "Shader code: " << code << "\n";

        glDeleteShader(shader);
        return false;
    }
    m_shaders.push_back(shader);
    return true;
}

void Shader::readAttributes()
{
    GLint count;
    glGetProgramiv(m_program, GL_ACTIVE_ATTRIBUTES, &count);

    GLint bufLen;
    glGetProgramiv(m_program, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &bufLen);

    // Get around certain driver bugs where GL_ACTIVE_ATTRIBUTE_MAX_LENGTH
    // returns a bad length.
    bufLen = std::max(64, bufLen);

    std::vector<GLchar> nameBuf(bufLen);

    for (GLint i = 0; i < count; ++i)
    {
        GLsizei attrNameLength;
        GLint attrCount;
        GLenum attrType;
        glGetActiveAttrib(m_program,
                          static_cast<GLuint>(i),
                          bufLen,
                          &attrNameLength,
                          &attrCount,
                          &attrType,
                          nameBuf.data());

        std::string name;
        name.assign(nameBuf.data(), attrNameLength);
        AttributeDataType dType;
        switch (attrType)
        {
        case GL_FLOAT:              dType = AttributeDataType::Float; break;
        case GL_FLOAT_VEC2:         dType = AttributeDataType::Vector2; break;
        case GL_FLOAT_VEC3:         dType = AttributeDataType::Vector3; break;
        case GL_FLOAT_VEC4:         dType = AttributeDataType::Vector4; break;
        case GL_FLOAT_MAT2:         dType = AttributeDataType::Matrix2; break;
        case GL_FLOAT_MAT3:         dType = AttributeDataType::Matrix3; break;
        case GL_FLOAT_MAT4:         dType = AttributeDataType::Matrix4; break;
        case GL_INT_VEC2:           dType = AttributeDataType::IVector2; break;
        case GL_INT_VEC3:           dType = AttributeDataType::IVector3; break;
        case GL_INT_VEC4:           dType = AttributeDataType::IVector4; break;
        case GL_UNSIGNED_INT_VEC2:  dType = AttributeDataType::UVector2; break;
        case GL_UNSIGNED_INT_VEC3:  dType = AttributeDataType::UVector3; break;
        case GL_UNSIGNED_INT_VEC4:  dType = AttributeDataType::UVector4; break;
        default:
            throw std::runtime_error("Unknown attribute data type.");
        }
        GLint attrLoc = glGetAttribLocation(m_program, name.c_str());
        m_attrinfo.push_back({name, dType,
                             static_cast<unsigned>(attrCount), attrLoc});
    }
}

void Shader::readUniforms()
{
    GLint count;
    glGetProgramiv(m_program, GL_ACTIVE_UNIFORMS, &count);

    GLint bufLen;
    glGetProgramiv(m_program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &bufLen);

    // Get around certain driver bugs where GL_ACTIVE_UNIFORM_MAX_LENGTH
    // returns a bad length.
    bufLen = std::max(64, bufLen);

    std::vector<GLchar> nameBuf(bufLen);

    for (GLint i = 0; i < count; ++i)
    {
        GLsizei unifNameLength;
        GLint unifCount;
        GLenum unifType;
        glGetActiveUniform(m_program,
                           static_cast<GLuint>(i),
                           bufLen,
                           &unifNameLength,
                           &unifCount,
                           &unifType,
                           nameBuf.data());

        std::string name;
        name.assign(nameBuf.data(), unifNameLength);
        UniformDataType dType;
        switch (unifType)
        {
        case GL_FLOAT:              dType = UniformDataType::Float; break;
        case GL_FLOAT_VEC2:         dType = UniformDataType::Vector2; break;
        case GL_FLOAT_VEC3:         dType = UniformDataType::Vector3; break;
        case GL_FLOAT_VEC4:         dType = UniformDataType::Vector4; break;
        case GL_INT:                dType = UniformDataType::Integer; break;
        case GL_INT_VEC2:           dType = UniformDataType::IVector2; break;
        case GL_INT_VEC3:           dType = UniformDataType::IVector3; break;
        case GL_INT_VEC4:           dType = UniformDataType::IVector4; break;
        case GL_UNSIGNED_INT:       dType = UniformDataType::Unsigned; break;
        case GL_UNSIGNED_INT_VEC2:  dType = UniformDataType::UVector2; break;
        case GL_UNSIGNED_INT_VEC3:  dType = UniformDataType::UVector3; break;
        case GL_UNSIGNED_INT_VEC4:  dType = UniformDataType::UVector4; break;
        case GL_FLOAT_MAT2:         dType = UniformDataType::Matrix2; break;
        case GL_FLOAT_MAT3:         dType = UniformDataType::Matrix3; break;
        case GL_FLOAT_MAT4:         dType = UniformDataType::Matrix4; break;
        case GL_SAMPLER_1D:         dType = UniformDataType::Sampler1D; break;
        case GL_SAMPLER_2D:         dType = UniformDataType::Sampler2D; break;
        case GL_SAMPLER_3D:         dType = UniformDataType::Sampler3D; break;
        case GL_INT_SAMPLER_1D:     dType = UniformDataType::ISampler1D; break;
        case GL_INT_SAMPLER_2D:     dType = UniformDataType::ISampler2D; break;
        case GL_INT_SAMPLER_3D:     dType = UniformDataType::ISampler3D; break;
        case GL_UNSIGNED_INT_SAMPLER_1D:
            dType = UniformDataType::USampler1D;
            break;
        case GL_UNSIGNED_INT_SAMPLER_2D:
            dType = UniformDataType::USampler2D;
            break;
        case GL_UNSIGNED_INT_SAMPLER_3D:
            dType = UniformDataType::USampler3D;
            break;
        default:
            throw std::logic_error("Unknown uniform data type.");
        }
        GLint unifLoc = glGetUniformLocation(m_program, name.c_str());
        m_unifinfo.push_back({name, dType,
                              static_cast<unsigned>(unifCount), unifLoc});
    }
}

void Shader::setUniform(UniformID id, math::mat4 m)
{
    if (!isValidUniformID(id)) return;
    glUniformMatrix4fv(toLocation(id), 1, GL_TRUE, m.data());
    checkGLError();
}

void Shader::setUniform(UniformID id, math::mat3 m)
{
    if (!isValidUniformID(id)) return;
    glUniformMatrix3fv(toLocation(id), 1, GL_TRUE, m.data());
    checkGLError();
}

void Shader::setUniform(UniformID id, math::mat2 m)
{
    if (!isValidUniformID(id)) return;
    glUniformMatrix2fv(toLocation(id), 1, GL_TRUE, m.data());
    checkGLError();
}

void Shader::setUniform(UniformID id, math::vec4 v)
{
    if (!isValidUniformID(id)) return;
    glUniform4fv(toLocation(id), 1, &v[0]);
    checkGLError();
}

void Shader::setUniform(UniformID id, math::vec3 v)
{
    if (!isValidUniformID(id)) return;
    glUniform3fv(toLocation(id), 1, &v[0]);
    checkGLError();
}

void Shader::setUniform(UniformID id, math::vec2 v)
{
    if (!isValidUniformID(id)) return;
    glUniform2fv(toLocation(id), 1, &v[0]);
    checkGLError();
}

void Shader::setUniform(UniformID id, float f)
{
    if (!isValidUniformID(id)) return;
    glUniform1f(toLocation(id), f);
    checkGLError();
}


void Shader::setUniform(UniformID id, math::ivec4 v)
{
    if (!isValidUniformID(id)) return;
    glUniform4iv(toLocation(id), 1, &v[0]);
    checkGLError();
}

void Shader::setUniform(UniformID id, math::ivec3 v)
{
    if (!isValidUniformID(id)) return;
    glUniform3iv(toLocation(id), 1, &v[0]);
    checkGLError();
}

void Shader::setUniform(UniformID id, math::ivec2 v)
{
    if (!isValidUniformID(id)) return;
    glUniform2iv(toLocation(id), 1, &v[0]);
    checkGLError();
}

void Shader::setUniform(UniformID id, S32 s)
{
    if (!isValidUniformID(id)) return;
    glUniform1i(toLocation(id), s);
    checkGLError();
}


void Shader::setUniform(UniformID id, math::uvec4 v)
{
    if (!isValidUniformID(id)) return;
    glUniform4uiv(toLocation(id), 1, &v[0]);
    checkGLError();
}

void Shader::setUniform(UniformID id, math::uvec3 v)
{
    if (!isValidUniformID(id)) return;
    glUniform3uiv(toLocation(id), 1, &v[0]);
    checkGLError();
}

void Shader::setUniform(UniformID id, math::uvec2 v)
{
    if (!isValidUniformID(id)) return;
    glUniform2uiv(toLocation(id), 1, &v[0]);
    checkGLError();
}

void Shader::setUniform(UniformID id, U32 u)
{
    if (!isValidUniformID(id)) return;
    glUniform1ui(toLocation(id), u);
    checkGLError();
}


void Shader::useTexture(TextureID tid,
                        const graphics::Texture1D* txt,
                        const graphics::TextureSampler* sampler)
{
    auto texture = static_cast<const opengl::Texture1D*>(txt);
    GLuint oglTextureID = texture->getGLTexture().texture();
    auto texsampler =
        static_cast<const opengl::TextureSampler*>(sampler);
    useTexture(tid, oglTextureID, texsampler->m_sampler, GL_TEXTURE_1D);
}

void Shader::useTexture(TextureID tid,
                        const graphics::Texture2D* txt,
                        const graphics::TextureSampler* sampler)
{
    auto texture = static_cast<const opengl::Texture2D*>(txt);
    GLuint oglTextureID = texture->getGLTexture().texture();
    auto texsampler =
        static_cast<const opengl::TextureSampler*>(sampler);
    useTexture(tid, oglTextureID, texsampler->m_sampler, GL_TEXTURE_2D);
}

void Shader::useTexture(TextureID tid,
                        const graphics::Texture3D* txt,
                        const graphics::TextureSampler* sampler)
{
    auto texture = static_cast<const opengl::Texture3D*>(txt);
    GLuint oglTextureID = texture->getGLTexture().texture();
    auto texsampler =
        static_cast<const opengl::TextureSampler*>(sampler);
    useTexture(tid, oglTextureID, texsampler->m_sampler, GL_TEXTURE_3D);
}

void Shader::useTexture(TextureID tid,
                        const graphics::FramebufferTexture* txt)
{
    auto texture = static_cast<const opengl::FramebufferTexture*>(txt);
    useTexture(tid, texture->texture, 0, GL_TEXTURE_2D);
}

void Shader::useTexture(TextureID tid, GLuint oglTextureID,
                        GLuint sampler, GLenum textureType)
{
    // Find a texture image unit.
    GLint texUnit = m_driver.getTextureUnit(m_program, tid, oglTextureID);

    // Associate sampler uniform with texture image unit.
    setUniform(tid, texUnit);

    // Tell OpenGL what texture we wish to place at the texture image unit.
    glActiveTexture(GL_TEXTURE0 + texUnit);
    glBindTexture(textureType, oglTextureID);

    // Associate sampler with texture.
    glBindSampler(texUnit, sampler);
}

} // namespace opengl

} // namespace graphics

} // namespace kraken
