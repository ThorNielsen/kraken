#include <kraken/graphics/opengl/texture.hpp>

#include <stdexcept>

namespace kraken
{

namespace graphics
{

namespace opengl
{

GLenum getStorageFormat(graphics::TexelStorageFormat itt)
{
    switch(itt)
    {
    default: throw std::domain_error("Invalid texel storage format.");
    case TexelStorageFormat::R8: return GL_R8;
    case TexelStorageFormat::RG8: return GL_RG8;
    case TexelStorageFormat::RGB8: return GL_RGB8;
    case TexelStorageFormat::RGBA8: return GL_RGBA8;
    case TexelStorageFormat::SRGB8: return GL_SRGB8;
    case TexelStorageFormat::SRGBA8: return GL_SRGB8_ALPHA8;
    case TexelStorageFormat::R32I: return GL_R32I;
    case TexelStorageFormat::RG32I: return GL_RG32I;
    case TexelStorageFormat::RGB32I: return GL_RGB32I;
    case TexelStorageFormat::RGBA32I: return GL_RGBA32I;
    case TexelStorageFormat::R32U: return GL_R32UI;
    case TexelStorageFormat::RG32U: return GL_RG32UI;
    case TexelStorageFormat::RGB32U: return GL_RGB32UI;
    case TexelStorageFormat::RGBA32U: return GL_RGBA32UI;
    case TexelStorageFormat::R32F: return GL_R32F;
    case TexelStorageFormat::RG32F: return GL_RG32F;
    case TexelStorageFormat::RGB32F: return GL_RGB32F;
    case TexelStorageFormat::RGBA32F: return GL_RGBA32F;
    case TexelStorageFormat::CompressedRed: return GL_COMPRESSED_RED;
    case TexelStorageFormat::CompressedRG: return GL_COMPRESSED_RG;
    case TexelStorageFormat::CompressedRGB: return GL_COMPRESSED_RGB;
    case TexelStorageFormat::CompressedRGBA: return GL_COMPRESSED_RGBA;
    case TexelStorageFormat::CompressedSRGB: return GL_COMPRESSED_SRGB;
    case TexelStorageFormat::CompressedSRGBA: return GL_COMPRESSED_SRGB_ALPHA;
    case TexelStorageFormat::Depth24Stencil8: return GL_DEPTH24_STENCIL8;
    }
}

GLenum getChannelFormat(graphics::ChannelType ct)
{
    switch(ct)
    {
    default: throw std::domain_error("Invalid channel type.");
    case ChannelType::R: return GL_RED;
    case ChannelType::RG: return GL_RG;
    case ChannelType::RGB: return GL_RGB;
    case ChannelType::BGR: return GL_BGR;
    case ChannelType::RGBA: return GL_RGBA;
    case ChannelType::BGRA: return GL_BGRA;
    case ChannelType::DepthStencil: return GL_DEPTH_STENCIL;
    }
}

GLenum getTexelFormat(graphics::TexelType tt)
{
    switch(tt)
    {
    default: throw std::domain_error("Invalid texel type.");
    case TexelType::UByte: return GL_UNSIGNED_BYTE;
    case TexelType::Byte: return GL_BYTE;
    case TexelType::UShort: return GL_UNSIGNED_SHORT;
    case TexelType::Short: return GL_SHORT;
    case TexelType::UInt: return GL_UNSIGNED_INT;
    case TexelType::Int: return GL_INT;
    case TexelType::Float: return GL_FLOAT;
    case TexelType::UInt24UInt8: return GL_UNSIGNED_INT_24_8;
    }
}

Texture::Texture()
{
    glGenTextures(1, &m_texture);
    if (!m_texture)
    {
        throw std::logic_error("Assuming texture is nonzero. after alloc.");
    }
}

Texture::~Texture()
{
    if (m_texture) glDeleteTextures(1, &m_texture);
}

Texture& Texture::operator=(Texture&& other)
{
    if (m_texture) glDeleteTextures(1, &m_texture);
    m_texture = other.m_texture;
    other.m_texture = 0;
    return *this;
}

Texture1D::Texture1D() :
    m_texture{},
    m_width{0}
    {}

Texture1D::~Texture1D()
    {}

void Texture1D::update()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_1D, m_texture.texture());
    GLenum oglStorageFormat = getStorageFormat(texelStorageFormat());
    GLenum oglChannelFormat = getChannelFormat(channelType());
    GLenum oglTexelFormat = getTexelFormat(texelType());
    U32 mipmapwidth = m_width;
    for (U32 mipmap = 0; mipmap < mipmapCount(); ++mipmap)
    {
        if (isDirty(mipmap))
        {
            internalUpdateMipmap(mipmap, mipmapwidth, oglStorageFormat,
                                 oglChannelFormat, oglTexelFormat);
            markUpdated(mipmap);
        }
        mipmapwidth >>= 1;
        mipmapwidth = std::max(mipmapwidth, 1u);
    }
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAX_LEVEL, mipmapCount()-1);
    checkGLError();
}

void Texture1D::updateMipmap(U32 mipmap)
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_1D, m_texture.texture());
    internalUpdateMipmap(mipmap,
                         std::max(m_width >> mipmap, 1u),
                         getStorageFormat(texelStorageFormat()),
                         getChannelFormat(channelType()),
                         getTexelFormat(texelType()));
    checkGLError();
}

void Texture1D::internalUpdateMipmap(U32 mipmap,
                                     U32 mipmapwidth,
                                     GLenum oglStorageFormat,
                                     GLenum oglChannelFormat,
                                     GLenum oglTexelFormat)
{
    glTexImage1D(GL_TEXTURE_1D, mipmap, oglStorageFormat, mipmapwidth,
                 0, oglChannelFormat, oglTexelFormat, data(mipmap));
    checkGLError();
}

void Texture1D::setSize(U32 width_)
{
    m_width = width_;
    size_t mipmapWidth = width_;
    size_t bytesPerTexel = texelSize();
    U32 level ;
    for (level = 0;
         level < mipmapCount() && mipmapWidth;
         ++level, mipmapWidth >>= 1)
    {
        mipmapData(level).resize(mipmapWidth * bytesPerTexel);
    }
    if (level < mipmapCount()) removeMipmaps(level);
}

void Texture1D::onAllocateMipmaps(U32 oldLevel, U32 newMaxLevel)
{
    for (U32 level = oldLevel+1; level < newMaxLevel; ++level)
    {
        mipmapData(level).resize((m_width>>level) * texelSize());
    }
}

void Texture1D::onClear()
{
    m_texture = opengl::Texture();
    m_width = 0;
}


Texture2D::Texture2D() :
    m_texture{},
    m_width{0},
    m_height{0}
    {}

Texture2D::~Texture2D()
    {}

void Texture2D::update()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_texture.texture());
    GLenum oglStorageFormat = getStorageFormat(texelStorageFormat());
    GLenum oglChannelFormat = getChannelFormat(channelType());
    GLenum oglTexelFormat = getTexelFormat(texelType());
    U32 mipmapwidth = m_width;
    U32 mipmapheight = m_height;
    for (U32 mipmap = 0; mipmap < mipmapCount(); ++mipmap)
    {
        if (isDirty(mipmap))
        {
            internalUpdateMipmap(mipmap, mipmapwidth, mipmapheight,
                                 oglStorageFormat, oglChannelFormat,
                                 oglTexelFormat);
            markUpdated(mipmap);
        }
        mipmapwidth = std::max(mipmapwidth >> 1, 1u);
        mipmapheight = std::max(mipmapheight >> 1, 1u);
    }
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, mipmapCount()-1);
    checkGLError();
}

void Texture2D::updateMipmap(U32 mipmap)
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_texture.texture());
    internalUpdateMipmap(mipmap,
                         std::max(m_width >> mipmap, 1u),
                         std::max(m_height >> mipmap, 1u),
                         getStorageFormat(texelStorageFormat()),
                         getChannelFormat(channelType()),
                         getTexelFormat(texelType()));
    glBindTexture(GL_TEXTURE_2D, 0);
    checkGLError();
}

void Texture2D::internalUpdateMipmap(U32 mipmap,
                                     U32 mipmapwidth,
                                     U32 mipmapheight,
                                     GLenum oglStorageFormat,
                                     GLenum oglChannelFormat,
                                     GLenum oglTexelFormat)
{
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexImage2D(GL_TEXTURE_2D, mipmap, oglStorageFormat, mipmapwidth,
                 mipmapheight, 0, oglChannelFormat, oglTexelFormat,
                 data(mipmap));
    checkGLError();
}

void Texture2D::setSize(U32 width_, U32 height_)
{
    m_width = width_;
    m_height = height_;
    size_t mipmapWidth = width_;
    size_t mipmapHeight = height_;
    size_t bytesPerTexel = texelSize();
    U32 level;
    for (level = 0;
         level < mipmapCount() && mipmapWidth && mipmapHeight;
         ++level, mipmapWidth >>= 1, mipmapHeight >>= 1)
    {
        mipmapData(level).resize(mipmapWidth * mipmapHeight * bytesPerTexel);
    }
    if (level < mipmapCount()) removeMipmaps(level);
}

void Texture2D::onAllocateMipmaps(U32 oldLevel, U32 newMaxLevel)
{
    for (U32 level = oldLevel+1; level < newMaxLevel; ++level)
    {
        mipmapData(level).resize((m_width>>level)*(m_height>>level)
                                 * texelSize());
    }
}

void Texture2D::onClear()
{
    m_texture = opengl::Texture();
    m_width = 0;
    m_height = 0;
}


Texture3D::Texture3D() :
    m_texture{},
    m_width{0},
    m_height{0},
    m_depth{0}
    {}

Texture3D::~Texture3D()
    {}

void Texture3D::update()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_3D, m_texture.texture());
    GLenum oglStorageFormat = getStorageFormat(texelStorageFormat());
    GLenum oglChannelFormat = getChannelFormat(channelType());
    GLenum oglTexelFormat = getTexelFormat(texelType());
    U32 mipmapwidth = m_width;
    U32 mipmapheight = m_height;
    U32 mipmapdepth = m_depth;
    for (U32 mipmap = 0; mipmap < mipmapCount(); ++mipmap)
    {
        if (isDirty(mipmap))
        {
            internalUpdateMipmap(mipmap, mipmapwidth, mipmapheight, mipmapdepth,
                                 oglStorageFormat, oglChannelFormat,
                                 oglTexelFormat);
            markUpdated(mipmap);
        }
        mipmapwidth = std::max(mipmapwidth >> 1, 1u);
        mipmapheight = std::max(mipmapheight >> 1, 1u);
        mipmapdepth = std::max(mipmapdepth >> 1, 1u);
    }
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAX_LEVEL, mipmapCount()-1);
    checkGLError();
}

void Texture3D::updateMipmap(U32 mipmap)
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_3D, m_texture.texture());
    internalUpdateMipmap(mipmap,
                         std::max(m_width >> mipmap, 1u),
                         std::max(m_height >> mipmap, 1u),
                         std::max(m_depth >> mipmap, 1u),
                         getStorageFormat(texelStorageFormat()),
                         getChannelFormat(channelType()),
                         getTexelFormat(texelType()));
    checkGLError();
}

void Texture3D::internalUpdateMipmap(U32 mipmap,
                                     U32 mipmapwidth,
                                     U32 mipmapheight,
                                     U32 mipmapdepth,
                                     GLenum oglStorageFormat,
                                     GLenum oglChannelFormat,
                                     GLenum oglTexelFormat)
{
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexImage3D(GL_TEXTURE_3D, mipmap, oglStorageFormat, mipmapwidth,
                 mipmapheight, mipmapdepth, 0, oglChannelFormat, oglTexelFormat,
                 data(mipmap));
    checkGLError();
}

void Texture3D::setSize(U32 width_, U32 height_, U32 depth_)
{
    m_width = width_;
    m_height = height_;
    m_depth = depth_;
    size_t mipmapWidth = width_;
    size_t mipmapHeight = height_;
    size_t mipmapDepth = depth_;
    size_t bytesPerTexel = texelSize();
    U32 level;
    for (level = 0;
         level < mipmapCount() && mipmapWidth && mipmapHeight && mipmapDepth;
         ++level, mipmapWidth >>= 1, mipmapHeight >>= 1, mipmapDepth >>= 1)
    {
        mipmapData(level).resize(mipmapWidth * mipmapHeight * mipmapDepth
                                 * bytesPerTexel);
    }
    if (level < mipmapCount()) removeMipmaps(level);
}

void Texture3D::onAllocateMipmaps(U32 oldLevel, U32 newMaxLevel)
{
    for (U32 level = oldLevel+1; level < newMaxLevel; ++level)
    {
        mipmapData(level).resize((m_width>>level)*(m_height>>level)
                                 *(m_depth>>level)* texelSize());
    }
}

void Texture3D::onClear()
{
    m_texture = opengl::Texture();
    m_width = 0;
    m_height = 0;
    m_depth = 0;
}

} // namespace opengl

} // namespace graphics

} // namespace kraken

