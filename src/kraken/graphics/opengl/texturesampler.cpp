#include <kraken/graphics/opengl/texturesampler.hpp>

#include <stdexcept>

namespace kraken
{

namespace graphics
{

namespace opengl
{

TextureSampler::TextureSampler()
{
    glGenSamplers(1, &m_sampler);
}

TextureSampler::~TextureSampler()
{
    glDeleteSamplers(1, &m_sampler);
}

void TextureSampler::setMinFilter(InterpolationFilter minf)
{
    GLenum minfilter;
    switch (minf)
    {
    case InterpolationFilter::Nearest:
        minfilter = GL_NEAREST;
        break;
    case InterpolationFilter::Linear:
        minfilter = GL_LINEAR;
        break;
    case InterpolationFilter::NearestTexelNearestMipmap:
        minfilter = GL_NEAREST_MIPMAP_NEAREST;
        break;
    case InterpolationFilter::NearestTexelLinearMipmap:
        minfilter = GL_NEAREST_MIPMAP_LINEAR;
        break;
    case InterpolationFilter::LinearTexelNearestMipmap:
        minfilter = GL_LINEAR_MIPMAP_NEAREST;
        break;
    case InterpolationFilter::LinearTexelLinearMipmap:
        minfilter = GL_LINEAR_MIPMAP_LINEAR;
        break;
    default:
        throw std::domain_error("Bad minification filter.");
    }

    m_minfilter = minf;
    glSamplerParameteri(m_sampler, GL_TEXTURE_MIN_FILTER, minfilter);
    checkGLError();
}

void TextureSampler::setMagFilter(InterpolationFilter magf)
{
    GLenum magfilter;
    switch (magf)
    {
    case InterpolationFilter::NearestTexelLinearMipmap: // Fallthrough
    case InterpolationFilter::NearestTexelNearestMipmap: // Fallthrough
    case InterpolationFilter::Nearest:
        magfilter = GL_NEAREST;
        break;
    case InterpolationFilter::LinearTexelLinearMipmap: // Fallthrough
    case InterpolationFilter::LinearTexelNearestMipmap: // Fallthrough
    case InterpolationFilter::Linear:
        magfilter = GL_LINEAR;
        break;
    default:
        throw std::domain_error("Bad magnification filter.");
    }

    m_magfilter = magf;
    glSamplerParameteri(m_sampler, GL_TEXTURE_MAG_FILTER, magfilter);
    checkGLError();
}

void TextureSampler::setMinMipmap(U32 minlevel)
{
    m_minmm = minlevel;
    // Mipmap level is floating-point in OpenGL.
    GLfloat mlvl = static_cast<GLfloat>(minlevel);
    glSamplerParameterf(m_sampler, GL_TEXTURE_MIN_LOD, mlvl);
    checkGLError();
}

void TextureSampler::setMaxMipmap(U32 maxlevel)
{
    m_maxmm = maxlevel;
    // Mipmap level is floating-point in OpenGL.
    GLfloat mlvl = static_cast<GLfloat>(maxlevel);
    glSamplerParameterf(m_sampler, GL_TEXTURE_MAX_LOD, mlvl);
    checkGLError();
}

inline GLenum wrapModeToGLenum(WrapMode wm)
{
    switch (wm)
    {
    case WrapMode::Repeat: return GL_REPEAT;
    case WrapMode::MirrorRepeat: return GL_MIRRORED_REPEAT;
    case WrapMode::EdgeClamp: return GL_CLAMP_TO_EDGE;
    default:
        throw std::domain_error("Bad wrap mode.");
    }
}

void TextureSampler::setSWrapMode(WrapMode wm)
{
    m_swrap = wm;
    glSamplerParameteri(m_sampler, GL_TEXTURE_WRAP_S, wrapModeToGLenum(wm));
}

void TextureSampler::setTWrapMode(WrapMode wm)
{
    m_twrap = wm;
    glSamplerParameteri(m_sampler, GL_TEXTURE_WRAP_T, wrapModeToGLenum(wm));
}

void TextureSampler::setPWrapMode(WrapMode wm)
{
    m_pwrap = wm;
    glSamplerParameteri(m_sampler, GL_TEXTURE_WRAP_R, wrapModeToGLenum(wm));
}

} // namespace opengl

} // namespace graphics

} // namespace kraken

