#include <kraken/graphics/opengl/vertex.hpp>
#include <kraken/graphics/opengl/driver.hpp>
#include <kraken/graphics/opengl/shader.hpp>

#include <stdexcept>

namespace kraken
{

namespace graphics
{

namespace opengl
{

GLenum vertexDataTypeToGL(VertexDataType d)
{
    switch (d & VertexDataType::TypeMask)
    {
    case VertexDataType::Byte:   return GL_BYTE;
    case VertexDataType::UByte:  return GL_UNSIGNED_BYTE;
    case VertexDataType::Short:  return GL_SHORT;
    case VertexDataType::UShort: return GL_UNSIGNED_SHORT;
    case VertexDataType::Int:    return GL_INT;
    case VertexDataType::UInt:   return GL_UNSIGNED_INT;
    case VertexDataType::Float:  return GL_FLOAT;

    // These are not valid but in order to let the compiler check for missed
    // cases in switch-statements, they have been added.
    case VertexDataType::TypeMask: case VertexDataType::Unnormalised:
    case VertexDataType::Integer: case VertexDataType::Padding:
    default:
        throw std::runtime_error("Invalid VertexDataType!");
    }
}

GLenum primitiveTypeToGL(PrimitiveType pType)
{
    switch (pType)
    {
    case PrimitiveType::Triangles:     return GL_TRIANGLES;
    case PrimitiveType::TriangleFan:   return GL_TRIANGLE_FAN;
    case PrimitiveType::TriangleStrip: return GL_TRIANGLE_STRIP;
    case PrimitiveType::Lines:         return GL_LINES;
    case PrimitiveType::LineStrip:     return GL_LINE_STRIP;
    case PrimitiveType::Points:        return GL_POINTS;
    default:
        throw std::runtime_error("Primitive type invalid.");
    }
}

void setupVertexRendering(const VertexType& vType, Driver* drv)
{
    auto* shader = drv->currentShader();
    GLsizei stride = static_cast<GLsizei>(vertexSize(vType));
    size_t offset = 0;
    for (GLuint i = 0; i < vType.size(); ++i)
    {
        if (vType[i].type != VertexDataType::Padding)
        {
            auto attrId = shader->attributeID(vType[i].name);

            // The attribute may have been optimised out and / or not exist.
            // However, we still need to update our offset or we will pass the
            // wrong data.
            if (attrId < 0) goto updateOffset;

            auto loc = shader->attribute(attrId)._index;

            glEnableVertexAttribArray(loc);
            bool normaliseData = !!(vType[i].type & VertexDataType::Unnormalised);
            if (vType[i].type == VertexDataType::Float) normaliseData = false;
            bool passInteger = !(vType[i].type & VertexDataType::Integer);
            if (passInteger)
            {
                glVertexAttribIPointer(loc,
                                       static_cast<GLint>(vType[i].count),
                                       vertexDataTypeToGL(vType[i].type),
                                       stride,
                                       reinterpret_cast<const GLvoid*>(offset));
                checkGLError();
            }
            else
            {
                glVertexAttribPointer(loc,
                                      static_cast<GLint>(vType[i].count),
                                      vertexDataTypeToGL(vType[i].type),
                                      normaliseData,
                                      stride,
                                      reinterpret_cast<const GLvoid*>(offset));
                checkGLError();
            }
        }

    updateOffset:
        offset += vType[i].count * dataTypeSize(vType[i].type);
    }
}

void clearVertexRendering(const VertexType& vType, Driver* drv)
{
    auto* shader = drv->currentShader();
    for (GLuint i = 0; i < vType.size(); ++i)
    {
        if (vType[i].type != VertexDataType::Padding)
        {
            auto attrId = shader->attributeID(vType[i].name);

            // The attribute may have been optimised out and / or not exist.
            if (attrId < 0) continue;

            auto loc = shader->attribute(attrId)._index;

            glDisableVertexAttribArray(loc);
        }
    }
}

} // namespace opengl

} // namespace graphics

} // namespace kraken
