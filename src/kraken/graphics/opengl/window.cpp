#include <kraken/graphics/opengl/window.hpp>
#include <kraken/graphics/opengl/driver.hpp>
#include <kraken/utility/memory.hpp>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <stdexcept>

namespace kraken
{

namespace graphics
{

namespace opengl
{

void GLSurfaceCreationInfo::beforeWindowOpen()
{
    glfwWindowHint(GLFW_RED_BITS, m_cs.redBits);
    glfwWindowHint(GLFW_GREEN_BITS, m_cs.greenBits);
    glfwWindowHint(GLFW_BLUE_BITS, m_cs.blueBits);
    glfwWindowHint(GLFW_DEPTH_BITS, m_cs.depthBits);
    glfwWindowHint(GLFW_STENCIL_BITS, m_cs.stencilBits);
    glfwWindowHint(GLFW_SAMPLES, m_cs.samples);
    glfwWindowHint(GLFW_STEREO, m_cs.stereo);
    glfwWindowHint(GLFW_DOUBLEBUFFER, m_cs.doubleBuffer);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, m_cs.majorVersion);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, m_cs.minorVersion);

    // Todo: support choosing between OpenGL and OpenGL ES.
    if (m_cs.majorVersion >= 3)
    {
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, true);
        if (m_cs.minorVersion >= 2 || m_cs.majorVersion > 3)
        {
            if (m_cs.coreProfile)
            {
                glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
            }
            else
            {
                glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
            }
        }
    }
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, m_cs.debugContext);
}

GLSurfaceCreationInfo::~GLSurfaceCreationInfo()
{
    if (m_driver)
    {
        // This should be impossible, but if for some reason the destructor is
        // (erroneously) called before beforeWindowClose(), we shouldn't leak
        // memory -- especially not a heavyweight object like opengl::Driver.
        delete static_cast<opengl::Driver*>(m_driver);
    }
}

void* GLSurfaceCreationInfo::afterWindowOpen(void* handle)
{
    return m_driver = new opengl::Driver(handle);
}

void GLSurfaceCreationInfo::beforeWindowClose(void*)
{
    if (!m_driver)
    {
        throw std::logic_error("beforeWindowClose called without opening window.");
    }
    delete static_cast<opengl::Driver*>(m_driver);
    m_driver = nullptr;
}

void GLSurfaceCreationInfo::onFramebufferResize(U32 newX, U32 newY)
{
    static_cast<opengl::Driver*>(m_driver)->onFramebufferResize(newX, newY);
}

std::unique_ptr<window::SurfaceCreationInfo> surfaceCreator(ContextSettings cs)
{
    return make_unique<GLSurfaceCreationInfo>(cs);
}

} // namespace opengl

} // namespace graphics

} // namespace kraken
