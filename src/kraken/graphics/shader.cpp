#include <ios>
#include <istream>
#include <kraken/graphics/shader.hpp>
#include <string>

namespace kraken
{

namespace graphics
{

bool Shader::addShader(std::istream& input, Stage stage)
{
    std::string code;
    auto offset = input.tellg();
    input.seekg(0, std::ios::end);
    code.resize(input.tellg()-offset);
    input.seekg(0, std::ios::beg);
    input.read(&code[0], code.size());
    return addShader(code, stage);
}

} // namespace graphics

} // namespace kraken
