#include <kraken/graphics/texture.hpp>
#include <cstring>

namespace kraken
{

namespace graphics
{

bool Texture2D::createFromImage(const image::Image& img)
{
    auto channels = img.colourType().channels;
    auto depth = img.colourType().bitDepth;
    ChannelType ct;
    switch (channels)
    {
    default:
    case 1: ct = ChannelType::Red; break;
    case 2: ct = ChannelType::RG; break;
    case 3: ct = ChannelType::RGB; break;
    case 4: ct = ChannelType::RGBA; break;
    }
    TexelType tt;
    if (depth == 8) tt = TexelType::UByte;
    else if (depth == 16) tt = TexelType::UShort;
    else return false;
    TexelStorageFormat tsf;
    if (tt == TexelType::UByte)
    {
        if (ct == ChannelType::Red) tsf = TexelStorageFormat::R8;
        else if (ct == ChannelType::RG) tsf = TexelStorageFormat::RG8;
        else if (ct == ChannelType::RGB) tsf = TexelStorageFormat::RGB8;
        else if (ct == ChannelType::RGBA) tsf = TexelStorageFormat::RGBA;
        else return false;
    }
    else if (tt == TexelType::UShort)
    {
        if (ct == ChannelType::Red) tsf = TexelStorageFormat::R32U;
        else if (ct == ChannelType::RG) tsf = TexelStorageFormat::RG32U;
        else if (ct == ChannelType::RGB) tsf = TexelStorageFormat::RGB32U;
        else if (ct == ChannelType::RGBA) tsf = TexelStorageFormat::RGBA32U;
        else return false;
    }

    clear();
    setFormat(ct, tt, tsf);
    setSize(img.width(), img.height());

    std::memcpy(data(), img.data(),
                img.width() * img.height() * ((channels * depth)>>3));
    update();
    return true;
}

} // namespace graphics

} // namespace kraken
