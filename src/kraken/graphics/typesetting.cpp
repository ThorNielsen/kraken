#include <kraken/graphics/typesetting.hpp>

#include <kraken/graphics/font.hpp>
#include <kraken/graphics/freetype/freetype.hpp>
#include <kraken/graphics/videodriver.hpp>

namespace kraken
{

namespace graphics
{

/// \brief Moves a contiguous sequence of positions by a given offset.
///
/// \param tm TextMetrics object containing the positions.
/// \param offset Offset to add to each position.
/// \param begin First element to move.
/// \param end One-past-the-last element to move.
void applyOffset(TextMetrics& tm, math::ivec2 offset, size_t begin, size_t end)
{
    for (auto i = begin; i < end; ++i)
    {
        tm.moveRelative(i, offset);
    }
}

/// \brief Determines whether the current line is breakable or if the following
///        glyphs should simply be placed on the next line.
/// \param metrics TextMetrics object containing... Correct, metrics.
/// \param glyph The current glyph.
/// \param prevSpace Position of previous space ON THIS LINE. npos if none.
/// \param xPos Current x-position.
/// \param glyphWidth Self-explanatory.
/// \param maxWidth Also self-explanatory.
/// \return See description.
inline bool isBreakable(const TextMetrics& metrics,
                        size_t prevSpace,
                        int xPos,
                        int glyphWidth,
                        int maxWidth)
{
    return (prevSpace != TextMetrics::npos)
        && (prevSpace+1 < metrics.size())
        && ((xPos + glyphWidth - metrics[prevSpace+1].x) < maxWidth);
}

void alignLine(TextMetrics& metrics,
               const std::vector<size_t>& spaces,
               TextAlignment align,
               size_t begin,
               size_t end,
               int width,
               int maxWidth)
{
    switch (align)
    {
    case TextAlignment::Left:
        return;
    case TextAlignment::Center:
    {
        math::ivec2 offset = {(maxWidth - width) / 2, 0};
        applyOffset(metrics, offset, begin, end);
        return;
    }
    case TextAlignment::Right:
    {
        math::ivec2 offset = {(maxWidth - width), 0};
        applyOffset(metrics, offset, begin, end);
        return;
    }
    case TextAlignment::Justified:
    {
        size_t maxSpace = spaces.size();
        while (maxSpace && spaces[maxSpace-1] == end)
        {
            --maxSpace;
        }
        if (!maxSpace) return;
        int dist = maxWidth - width;
        size_t spaceWidth = dist / maxSpace;
        size_t extraSpaces = dist % maxSpace;
        size_t accSpaces = 0;
        for (size_t i = 0; i < maxSpace; ++i)
        {
            accSpaces += spaceWidth + (i < extraSpaces);
            applyOffset(metrics,
                        math::ivec2{static_cast<int>(accSpaces), 0},
                        spaces[i],
                        i+1 < maxSpace ?
                            spaces[i+1] : end);
        }
        return;
    }
    default:
        throw std::logic_error("Unknown alignment.");
    }
}


// TODO: Add boundary detection support (allowing for better justification for
// text with non-space boundaries).
TextMetrics typeset(const String& text,
                    TextAlignment align,
                    std::function<Font*(size_t)> fontfunc,
                    std::function<U32(size_t)> height,
                    U32 maxWidth)
{
    if (!maxWidth) maxWidth = 1073741823;
    Font* cfont = fontfunc(0);
    size_t fontBegin = 0;

    TextMetrics metrics;
    math::ivec2 pos = {0, 0};
    U32 prev = 0;

    // Line-specific variables
    size_t lineBegin = 0;

    // Measurements
    U32 width = 0;
    S32 minY = std::numeric_limits<S32>::max();
    S32 maxY = std::numeric_limits<S32>::min();

    std::vector<size_t> spaces;

    U32 prevHeight = 0;
    U32 wordHeight = height(0);

    size_t currGlyph = 0;
    size_t i = 0;
    for (auto it = text.begin(); it != text.end(); ++i, ++it)
    {
        U32 charHeight = height(i);
        wordHeight = std::max(wordHeight, charHeight);
        size_t elem = metrics.size();

        Font* nfont = fontfunc(i);
        if (nfont != cfont)
        {
            metrics.setFont(cfont, fontBegin, currGlyph);
            fontBegin = currGlyph;
            cfont = nfont;
            prev = 0;
        }

        if (*it == '\n')
        {
            U32 currHeight = std::max(prevHeight, wordHeight);
            prevHeight = 0;
            wordHeight = 0;
            pos.x = 0;
            pos.y -= currHeight + currHeight / 4;
            prev = 0;

            U32 lineWidth = 0;
            if (elem)
            {
                lineWidth = metrics[elem-1].x + metrics.metrics(elem-1).width;
            }
            // If there's a forced line break in justified alignment, that line
            // shouldn't be justified.
            if (align == TextAlignment::Justified)
            {
                alignLine(metrics, spaces, TextAlignment::Left, lineBegin,
                          elem, lineWidth, maxWidth);
            }
            else
            {
                alignLine(metrics, spaces, align, lineBegin,
                          elem, lineWidth, maxWidth);
            }
            lineBegin = elem;
            spaces.clear();
            continue; // Newlines needn't be processed any further.
        }

        Glyph glyph = cfont->getGlyph(*it, charHeight);

        if (cfont->isKerningEnabled() && prev && glyph.glyphIndex)
        {
            FT_Vector kern;
            checkFTError(FT_Get_Kerning(cfont->getInternalFace()->face,
                                        prev,
                                        glyph.glyphIndex,
                                        FT_KERNING_DEFAULT,
                                        &kern));
            pos.x += static_cast<int>(kern.x >> 6);
        }

        if (pos.x + glyph.xAdvance >= static_cast<S32>(maxWidth))
        {
            U32 newLineHeight = 0;
            U32 advanceHeight = 0;
            // If the line contains multiple words (spaces.size() > 0) and we
            // aren't processing a space (spaces.back() < elem). Also check that
            // breaking at the last space doesn't cause the word being moved
            // down to overflow the next line.
            if (spaces.size() && spaces.back() < elem
                && (pos.x + glyph.xAdvance - metrics[spaces.back()].x)
                    < static_cast<S32>(maxWidth))
            {
                // We break at a space, so the new line will be as high as the
                // current word (currently) while we should advance vertically
                // by the height of the line before this word.
                newLineHeight = wordHeight;
                advanceHeight = prevHeight + prevHeight / 4;

                math::ivec2 wbegin = metrics[spaces.back()];
                pos.x = pos.x - wbegin.x;
                applyOffset(metrics,
                            math::ivec2{-wbegin.x, -static_cast<S32>(advanceHeight)},
                            spaces.back(),
                            elem);

                U32 lineWidth = 0;
                if (spaces.back() > 0)
                {
                    lineWidth = metrics[spaces.back()-1].x
                                + metrics.metrics(spaces.back()-1).width;
                }
                else
                {
                    throw std::runtime_error("First glyph is wider than line.");
                }
                alignLine(metrics, spaces, align, lineBegin,
                          spaces.back(), lineWidth, maxWidth);
                lineBegin = spaces.back();
            }
            // Otherwise, break at current position.
            else
            {
                if (!metrics.size())
                {
                    throw std::runtime_error("Glyph wider than line.");
                }
                U32 lineWidth = metrics[elem-1].x
                                + metrics.metrics(elem-1).width;
                alignLine(metrics, spaces, align, lineBegin,
                          elem, lineWidth, maxWidth);
                lineBegin = elem;
                pos.x = 0;

                advanceHeight = std::max(wordHeight, prevHeight);
                advanceHeight += advanceHeight / 4;
                newLineHeight = charHeight;
            }
            pos.y -= advanceHeight;
            prevHeight = 0;
            wordHeight = newLineHeight;
            prev = 0;
            spaces.clear();
        }

        if (glyph.texCoords.height > 0 && glyph.texCoords.width > 0)
        {
            wordHeight = std::max(wordHeight, charHeight);
            math::ivec2 glyphPos = pos + math::ivec2{glyph.xOffset, glyph.yOffset};
            metrics.append(*it,
                           glyphPos,
                           static_cast<U16>(charHeight),
                           glyph.texCoords.width,
                           glyph.texCoords.height);
            ++currGlyph;

            width = std::max(width, (U32)(glyphPos.x + glyph.texCoords.width));
            minY = std::min(minY, glyphPos.y);
            maxY = std::max(maxY, glyphPos.y + glyph.texCoords.height);
        }
        else
        {
            spaces.push_back(elem);
            prevHeight = std::max(prevHeight, wordHeight);
            wordHeight = 0;
        }

        pos += math::ivec2{glyph.xAdvance, glyph.yAdvance};

        if (pos.x >= static_cast<S32>(maxWidth))
        {
            throw std::runtime_error("Couldn't fit text in "
                                     + std::to_string(maxWidth)
                                     + " pixels (used "
                                     + std::to_string(pos.x+1)
                                     + ").");
        }

        prev = glyph.glyphIndex;
    }
    if (lineBegin != metrics.size())
    {
        U32 lineWidth = 0;
        auto last = metrics.size();
        if (last)
        {
            lineWidth = metrics[last-1].x + metrics.metrics(last-1).width;
        }
        // We should not justify the last line.
        if (align == TextAlignment::Justified)
        {
            alignLine(metrics, spaces, TextAlignment::Left, lineBegin,
                      last, lineWidth, maxWidth);
        }
        else
        {
            alignLine(metrics, spaces, align, lineBegin,
                      last, lineWidth, maxWidth);
        }
    }
    metrics.setFont(cfont, fontBegin, currGlyph);
    metrics.updateWidth(width+1);
    metrics.updateHeight(maxY-minY+1);
    math::ivec2 offset{-9, 17};
    S32 wDiff = static_cast<S32>(metrics.width()) - static_cast<S32>(maxWidth);
    switch (align)
    {
    case TextAlignment::Left:
    case TextAlignment::Justified:
        offset = {0, 0-maxY};
        break;
    case TextAlignment::Center:
        offset = {wDiff / 2, 0-maxY};
        break;
    case TextAlignment::Right:
        offset = {wDiff, 0-maxY};
        break;
    }
    metrics.setOffset(offset);
    return metrics;
}

math::ivec2 getOffset(Alignment align, math::ivec2 dim)
{
    switch (align)
    {
    case Alignment::TopLeft:
        return {0, 0};

    case Alignment::TopCenter:
        return {0-dim.x / 2, 0};

    case Alignment::TopRight:
        return {0-dim.x, 0};

    case Alignment::CenterLeft:
        return {0, dim.y / 2};

    case Alignment::Center:
        return {0-dim.x / 2, dim.y / 2};

    case Alignment::CenterRight:
        return {0-dim.x, dim.y / 2};

    case Alignment::BottomLeft:
        return {0, dim.y};

    case Alignment::BottomCenter:
        return {0-dim.x / 2, dim.y};

    case Alignment::BottomRight:
        return {0-dim.x, dim.y};

    default:
        throw std::domain_error("Unknown alignment.");
    }
}

void TextGeometry::append(const TextMetrics& metrics, math::ivec2 position,
                          Alignment anchor)
{
    position.y = -position.y;
    math::ivec2 totalSize{(S32)metrics.width(), (S32)metrics.height()};
    math::ivec2 offset = getOffset(anchor, totalSize) + position;
    m_topLeft.x = std::min(offset.x, m_topLeft.x);
    m_topLeft.y = std::min(offset.y, m_topLeft.y);
    m_bottomRight.x = std::min(offset.x+totalSize.x, m_bottomRight.x);
    m_bottomRight.y = std::min(offset.y+totalSize.y, m_bottomRight.y);
    for (auto font = metrics.fontBegin(); font != metrics.fontEnd(); ++font)
    {
        const Font* currFont = font->first;
        size_t idx = geometry.size();
        for (size_t i = 0; i < geometry.size(); ++i)
        {
            if (geometry[i].font == currFont) break;
        }
        if (idx == geometry.size())
        {
            geometry.push_back({m_driver->createMesh(), currFont, 0});
            geometry.back().mesh->setPrimitiveType(PrimitiveType::Points);
            VertexType vt;
            vt.push_back({"position", VertexDataType::Int | VertexDataType::Integer, 2});
            vt.push_back({"texData", VertexDataType::UInt | VertexDataType::Integer, 2});
            geometry.back().mesh->setVertexType(vt);
        }
        auto& mesh = geometry[idx].mesh;
        GlyphData* glyphOut = static_cast<GlyphData*>(mesh->vertices());
        auto& glyphPos = geometry[idx].count;

        for (auto range : font->second)
        {
            // Resize mesh vertex buffer if necessary
            size_t rangeCount = range.end - range.begin;
            size_t expectedSize = (rangeCount + glyphPos) * sizeof(GlyphData);
            size_t actualSize = mesh->vertexBufferSize();
            if (expectedSize > actualSize)
            {
                size_t resizeTo = ((5 * actualSize) >> 2) + 8 * sizeof(GlyphData);
                resizeTo -= resizeTo % sizeof(GlyphData);
                if (resizeTo < expectedSize) resizeTo = expectedSize;
                mesh->resizeVertexBuffer(resizeTo);
                glyphOut = static_cast<GlyphData*>(mesh->vertices());
            }

            // Fill mesh vertex buffer with data
            for (size_t i = range.begin; i < range.end; ++i)
            {
                const auto& glyphMetrics = metrics.metrics(i);
                const auto& rect = currFont->getGlyph(glyphMetrics.codepoint,
                                                      glyphMetrics.fontHeight);
                glyphOut[glyphPos].pos = metrics[i] + metrics.offset() + offset;
                const auto& tc = rect.texCoords;
                glyphOut[glyphPos].texData =
                    math::uvec2{static_cast<U32>((tc.x << 16) | tc.y),
                                static_cast<U32>((tc.width << 16) | tc.height)};
                ++glyphPos;
            }
        }
        mesh->update();
    }
}

} // namespace graphics

} // namespace kraken
