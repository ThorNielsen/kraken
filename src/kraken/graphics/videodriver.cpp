#include <kraken/graphics/videodriver.hpp>
#include <kraken/graphics/font.hpp>

namespace kraken
{

namespace graphics
{

VideoDriver::FontWrapper::~FontWrapper()
{
    if (font) delete font;
}

Font* VideoDriver::loadFont(const std::string& path)
{
    m_fonts.push_back({});
    m_fonts.back().font = new Font(this);
    Font* font = m_fonts.back().font;
    if (!font->load(path))
    {
        font = nullptr;
        m_fonts.pop_back();
    }
    return font;
}

Font* VideoDriver::loadFont(const U8* data, U64 length)
{
    m_fonts.push_back({});
    m_fonts.back().font = new Font(this);
    Font* font = m_fonts.back().font;
    if (!font->load(data, length))
    {
        font = nullptr;
        m_fonts.pop_back();
    }
    return font;
}

void VideoDriver::destroyFont(Font* font)
{
    for (size_t i = 0; i < m_fonts.size(); ++i)
    {
        if (m_fonts[i].font == font)
        {
            std::swap(m_fonts[i], m_fonts.back());
            m_fonts.pop_back();
            return;
        }
    }
    throw std::runtime_error("Font does not exists.");
}

void VideoDriver::draw(const TextMetrics& metrics, math::ivec2 offset,
                       Alignment align)
{
    TextGeometry geom(this);
    geom.append(metrics, {0, 0}, align);
    draw(geom, offset);
}

}

}
