#include "kraken/types.hpp"
#include <cstddef>
#include <kraken/image/colour.hpp>

#include <ostream>
#include <type_traits>

namespace kraken::image
{

std::ostream& operator<<(std::ostream& ost, const ColourType& ct)
{
    const char* toDigit = "0123456789";
    char buf[7];
    size_t pos = 0;
    if (ct.channels >= 10) buf[pos++] = toDigit[(ct.channels/10)%10];
    buf[pos++] = toDigit[ct.channels%10];
    buf[pos++] = 'c';
    if (ct.bitDepth >= 10) buf[pos++] = toDigit[(ct.bitDepth/10)%10];
    buf[pos++] = toDigit[ct.bitDepth%10];
    buf[pos++] = 'b';
    buf[pos++] = 0;
    return ost << buf;
}

Colour convert(Colour c, ColourType from, ColourType to) noexcept
{
    static_assert(std::is_same<Colour, U64>::value,
                  "Colour is assumed to be packed in U64.");
    U64 readMask = (1 << from.bitDepth) - 1;

    // Pad c to have 4 channels, and set alpha to 1 if that is not in the
    // original colour. This is written in a branchless version:
    // from.channels >> 2  is 1 exactly if the number of channels is 4 (of
    // course assuming # of channels is 1, 2, 3 or 4), subtracting that from 1
    // gives 1 exactly if # of channels is less than 4.
    c = (c << ((4 - from.channels) * from.bitDepth)) |
        ((1-(from.channels >> 2)) * readMask);
    U16 a = c & readMask;
    c >>= from.bitDepth;
    U16 b = c & readMask;
    c >>= from.bitDepth;
    U16 g = c & readMask;
    c >>= from.bitDepth;
    U16 r = c;// & readMask is unnecessary
    if (from.bitDepth >= to.bitDepth)
    {
        auto shift = from.bitDepth - to.bitDepth;
        a >>= shift;
        b >>= shift;
        g >>= shift;
        r >>= shift;
    }
    else
    {
        U16 mul = (U32(1 << to.bitDepth)-1) / readMask;
        a *= mul;
        b *= mul;
        g *= mul;
        r *= mul;
    }
    U64 n = ((((U64(r << to.bitDepth) | g)
                      << to.bitDepth) | b)
                      << to.bitDepth) | a;
    // Remove superfluous channels.
    return n >> ((4 - to.channels) * to.bitDepth);
}

} // namespace kraken::image
