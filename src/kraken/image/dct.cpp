#include "kraken/types.hpp"
#include <kraken/image/dct.hpp>

#include <kraken/math/comparison.hpp>
#include <kraken/math/constants.hpp>

#include <array>
#include <cmath>
#include <cstring>

namespace kraken::image
{

void jpeg8x8IDCTNaive(const F32* inputBuf, U8* outputBuf)
{
    F32 invSqrt2 = 1.f / std::sqrt(2.f);
    auto pi16 = math::Constant<F32>::pi / 16.f;
    for (size_t y = 0; y < 8; ++y)
    {
        for (size_t x = 0; x < 8; ++x)
        {
            F32 accum = 0.f;
            for (size_t v = 0; v < 8; ++v)
            {
                for (size_t u = 0; u < 8; ++u)
                {
                    F32 cu = u ? 1.f : invSqrt2;
                    F32 cv = v ? 1.f : invSqrt2;
                    auto sample = inputBuf[8*v+u];
                    auto cosXUTerm = std::cos((2*x+1)*u*pi16);
                    auto cosYVTerm = std::cos((2*y+1)*v*pi16);
                    accum += cu * cv * sample * cosXUTerm * cosYVTerm;
                }
            }
            outputBuf[8*x+y] = math::clamp(accum*.25f+128.5f, 0.f, 255.f);
        }
    }
}

void jpeg8x8IDCTFactoredNaive(const F32* inputBuf, U8* outputBuf)
{
    static thread_local bool initialised = false;
    static thread_local std::array<float, 64> precomp;

    if (!initialised)
    {
        double invSqrt2 = 1./std::sqrt(2.);
        double piOver16 = math::Constant<double>::pi * 0.0625;
        for (size_t x = 0; x < 8; ++x)
        {
            for (size_t u = 0; u < 8; ++u)
            {
                precomp[(x << 3) | u] = std::cos((2*x+1)*u*piOver16)
                                                 * (u ? 1. : invSqrt2);
            }
        }
        initialised = true;
    }

    std::array<float, 64> dotprods;
    for (size_t x = 0; x < 8; ++x)
    {
        auto xBase = x << 3;
        for (size_t v = 0; v < 8; ++v)
        {
            auto vBase = v << 3;
            auto& acc = dotprods[xBase | v];
            acc = 0.f;
            for (size_t u = 0; u < 8; ++u)
            {
                acc += inputBuf[vBase | u] * precomp[xBase | u];
            }
        }
    }

    for (size_t x = 0; x < 8; ++x)
    {
        auto xBase = x << 3;
        for (size_t y = 0; y < 8; ++y)
        {
            auto yBase = (y << 3);

            float value = 0.f;
            for (size_t v = 0; v < 8; ++v)
            {
                value += dotprods[xBase | v] * precomp[yBase | v];
            }

            *outputBuf++ = math::clamp(value*.25f+128.5f, 0.f, 255.f);
        }
    }
}

void jpeg8IDCTInplaceNaive(F32* buffer, size_t stride)
{
    F32 output[8];
    double invSqrt2 = 1./std::sqrt(2.);
    double piOver16 = math::Constant<double>::pi * 0.0625;
    for (size_t x = 0; x < 8; ++x)
    {
        output[x] = 0;
        for (size_t u = 0; u < 8; ++u)
        {
            output[x] += (u ? 1. : invSqrt2)
                      * buffer[u*stride]
                      * std::cos((2*x+1)*u*piOver16);
        }
    }
    for (size_t x = 0; x < 8; ++x)
    {
        buffer[x*stride] = output[x];
    }
}

void jpeg8x8IDCTBy1DFactorisationNaive(const F32* inputBuf, U8* outputBuf)
{
    F32 buffer[64];
    std::memcpy(buffer, inputBuf, 64*4);
    for (size_t row = 0; row < 8; ++row)
    {
        jpeg8IDCTInplaceNaive(buffer+8*row, 1);
    }
    for (size_t col = 0; col < 8; ++col)
    {
        jpeg8IDCTInplaceNaive(buffer+col, 8);
    }

    for (size_t k = 0; k < 64; ++k)
    {
        // Swap the last two sets of three bits of k so as to swap columns/rows.
        size_t ok = ((k&7) << 3) | (k>>3);
        outputBuf[ok] = math::clamp(buffer[k]*.25f+128.5f, 0.f, 255.f);
    }
}

// Uses a slight variant of an algorithm described in:
// C. Loeffler, A. Ligtenberg and G. S. Moschytz,
// "Practical fast 1-D DCT algorithms with 11 multiplications,"
// International Conference on Acoustics, Speech, and Signal Processing,
// Glasgow, UK, 1989, pp. 988-991 vol.2, doi: 10.1109/ICASSP.1989.266596.
template <size_t Stride>
void jpeg8IDCTInplaceFast(F32* buffer)
{
    // In the below, XtermY means
    // trig(X)(Y*pi/16), where trig(c) = cos, trig(s) = sin.
    // Furthermore, cZsY is simply ctermY [+-] stermY, where the operation is
    // plus if Z=p, and minus if Z=m.
    // So, e.g. cps3 means cterm3 + sterm3,
    // and e.g. sterm3 = sin(3*pi/16).

    constexpr F32 invSqrt2 =  0.7071067811865475f;
    constexpr F32 sterm6   =  0.9238795325112867f;
    constexpr F32 cms6     = -0.5411961001461969f;
    constexpr F32 cps6     =  1.3065629648763766f;
    constexpr F32 sterm3   =  0.5555702330196022f;
    constexpr F32 cms3     =  0.27589937928294306f;
    constexpr F32 cps3     =  1.3870398453221475f;
    constexpr F32 sterm1   =  0.19509032201612825f;
    constexpr F32 cms1     =  0.7856949583871022f;
    constexpr F32 cps1     =  1.1758756024193586f;

    F32 tmp[8];
    // Stage 4; write to tmp.
    tmp[6] = buffer[Stride*5];
    tmp[7] = (buffer[Stride*1] + buffer[Stride*7]) * invSqrt2;
    tmp[4] = (buffer[Stride*1] - buffer[Stride*7]) * invSqrt2;

    tmp[0] = buffer[Stride*0] * invSqrt2;
    tmp[1] = buffer[Stride*4] * invSqrt2;

    // Stage 3; write to buffer.
    buffer[Stride*0] = tmp[0] + tmp[1];
    buffer[Stride*1] = tmp[0] - tmp[1];

    buffer[Stride*7] = tmp[7] + buffer[Stride*3];
    buffer[Stride*5] = tmp[7] - buffer[Stride*3];

    F32 mid = sterm6 * (buffer[Stride*2]-buffer[Stride*6]);
    buffer[Stride*2] = cms6 * buffer[Stride*2] + mid;
    buffer[Stride*3] = cps6 * buffer[Stride*6] + mid;

    buffer[Stride*4] = tmp[4] + tmp[6];
    buffer[Stride*6] = tmp[4] - tmp[6];

    // Stage 2; write to tmp.
    tmp[0] = buffer[Stride*0] + buffer[Stride*3];
    tmp[3] = buffer[Stride*0] - buffer[Stride*3];
    tmp[1] = buffer[Stride*1] + buffer[Stride*2];
    tmp[2] = buffer[Stride*1] - buffer[Stride*2];

    mid = sterm3 * (buffer[Stride*4]-buffer[Stride*7]);
    tmp[4] = cms3 * buffer[Stride*4] + mid;
    tmp[7] = cps3 * buffer[Stride*7] + mid;

    mid = sterm1 * (buffer[Stride*5]-buffer[Stride*6]);
    tmp[5] = cms1 * buffer[Stride*5] + mid;
    tmp[6] = cps1 * buffer[Stride*6] + mid;

    // Stage 1; write final result to buffer.
    buffer[Stride*0] = tmp[0] + tmp[7];
    buffer[Stride*7] = tmp[0] - tmp[7];
    buffer[Stride*1] = tmp[1] + tmp[6];
    buffer[Stride*6] = tmp[1] - tmp[6];
    buffer[Stride*2] = tmp[2] + tmp[5];
    buffer[Stride*5] = tmp[2] - tmp[5];
    buffer[Stride*3] = tmp[3] + tmp[4];
    buffer[Stride*4] = tmp[3] - tmp[4];
}

void jpeg8x8IDCTBy1DFactorisationFast(const F32* inputBuf, U8* outputBuf)
{
    F32 buffer[64];
    std::memcpy(buffer, inputBuf, 64*4);
    for (size_t row = 0; row < 8; ++row)
    {
        jpeg8IDCTInplaceFast<1>(buffer+8*row);
    }
    for (size_t col = 0; col < 8; ++col)
    {
        jpeg8IDCTInplaceFast<8>(buffer+col);
    }

    for (size_t k = 0; k < 64; ++k)
    {
        // Swap the last two sets of three bits of k so as to swap columns/rows.
        size_t ok = ((k&7) << 3) | (k>>3);
        outputBuf[ok] = math::clamp(buffer[k]*.25f+128.5f, 0.f, 255.f);
    }
}

} // namespace kraken::image
