#include <kraken/image/image.hpp>
#include <kraken/utility/memory.hpp>

#include <cctype>
#include <iomanip>
#include <stdexcept>

namespace kraken
{

namespace image
{

void writeIntLE(U64 val, U8 bytes, U8* writeTo)
{
    while (bytes)
    {
        --bytes;
        *writeTo++ = val & 0xff;
        val >>= 8;
    }
}

bool Image::writeBMP(std::ostream& output) const
{
    auto ctype = usesPalette() ? paletteColourType() : colourType();
    if (usesPalette() && ctype != RGB8)
    {
        return false; // Only RGB8 is supported when paletted data is used.
    }
    else
    {
        if (ctype != R1 && ctype != R4 && ctype != R8 && ctype != RGB8)
        {
            return false;
        }
    }


    U32 paletteEntries = 0;
    if (usesPalette())
    {
        paletteEntries = paletteCount();
    }
    else if (ctype.channels == 1)
    {
        paletteEntries = 1 << ctype.bitDepth;
    }

    std::unique_ptr<U8[]> paletteData;
    if (paletteEntries)
    {
        paletteData = make_unique<U8[]>(paletteEntries*4);
        fillBMPPalette(paletteData.get(), paletteEntries);
    }

    U32 rowWidth = rowBytes();
    U32 padBytes = 0;
    if (rowWidth % 4 != 0)
    {
        padBytes = 4 - rowWidth % 4;
    }
    rowWidth += padBytes;
    U32 offset = 14+40+paletteEntries*4;
    U64 size = (U64)rowWidth * (U64)height() + offset;

    if (size != (size & 0xffffffff)) return false; // Image too large.

    U8 header[40];
    header[0] = 'B';
    header[1] = 'M';
    writeIntLE(size, 4, header+2);
    writeIntLE(0, 4, header+6);
    writeIntLE(offset, 4, header+10);
    output.write(reinterpret_cast<const char*>(header), 14);

    if (m_bpp != m_ctype.bitDepth * m_ctype.channels)
    {
        throw std::logic_error("Bad invariant!");
    }
    // Note: The header array is reused for the image header (as opposed to the
    // file header).
    writeIntLE(40, 4, header); // Header size
    writeIntLE(width(), 4, header+4);
    writeIntLE(height(), 4, header+8);
    writeIntLE(1, 2, header+12); // Required to be 1
    writeIntLE(m_bpp, 2, header+14); // Bits per pixel
    writeIntLE(0, 4, header+16); // Compression (no).
    writeIntLE(size-offset, 4, header+20);
    writeIntLE(0, 4, header+24); // Pixels / meter (x)
    writeIntLE(0, 4, header+28); // Pixels / meter (y)
    writeIntLE(paletteEntries, 4, header+32); // Used palette entries
    writeIntLE(0, 4, header+36); // Important palette entries (zero = all)

    output.write(reinterpret_cast<const char*>(header), 40);
    if (paletteEntries)
    {
        output.write(reinterpret_cast<const char*>(paletteData.get()),
                     paletteEntries*4);
    }
    return writeBMPColourdata(output, padBytes, rowWidth);
}

void Image::fillBMPPalette(U8* begin, U32 entries) const
{
    if (usesPalette())
    {
        if (entries != paletteCount())
        {
            throw std::logic_error("Bad palette entry size.");
        }
        for (size_t i = 0; i < entries; ++i)
        {
            U32 col = getPaletteElement(i);
            *begin++ = col & 0xff; col >>= 8;
            *begin++ = col & 0xff; col >>= 8;
            *begin++ = col & 0xff;
            *begin++ = 0;
        }
    }
    else
    {
        for (size_t i = 0; i < entries; ++i)
        {
            // Single-channel images are monochromatic.
            U8 col = image::convert(i, m_ctype, R8)&0xff;
            *begin++ = col;
            *begin++ = col;
            *begin++ = col;
            *begin++ = 0;
        }
    }
}

bool Image::writeBMPColourdata(std::ostream& output, U32 padBytes,
                               U32 rowWidth) const
{
    U64 bufSize = 32*1024;
    bufSize += rowWidth - bufSize % rowWidth;
    auto rowsPerBuf = bufSize / rowWidth;
    if (rowsPerBuf * rowWidth != bufSize)
    {
        throw std::logic_error("Bad math.");
    }
    if (bufSize > rowWidth * height())
    {
        bufSize = rowWidth * height();
    }

    U64 written = 0;
    std::vector<U8> lineBuffer(bufSize, 0);
    U8* writeAt = lineBuffer.data();
    const U8* readAt = m_data.data() + m_data.size();
    for (U32 y = 0; y < height(); ++y)
    {
        readAt -= rowBytes();
        if (m_ctype == RGB8)
        {
            for (U32 x = 0; x < width(); ++x)
            {
                U8 red = *readAt++;
                U8 green =*readAt++;
                U8 blue = *readAt++;
                *writeAt++ = blue;
                *writeAt++ = green;
                *writeAt++ = red;
            }
        }
        else // Copy rows without any swapping.
        {
            for (U32 x = 0; x < rowBytes(); ++x)
            {
                *writeAt++ = *readAt++;
            }
        }

        writeAt += padBytes;
        readAt -= rowBytes();
        if ((y+1) % rowsPerBuf == 0)
        {
            output.write(reinterpret_cast<const char*>(lineBuffer.data()),
                         lineBuffer.size());
            written += lineBuffer.size();
            writeAt = lineBuffer.data();
        }
    }
    auto extraWritten = (U64)(writeAt - lineBuffer.data());
    output.write(reinterpret_cast<const char*>(lineBuffer.data()),
                 extraWritten);
    written += extraWritten;
    if (output.fail())
    {
        throw std::runtime_error("Writing failed.");
    }
    if (written != height() * rowWidth)
    {
        throw std::logic_error("Not enough data written.");
    }
    return true;
}

bool Image::writePNM(std::ostream& output) const
{
    // Palette output not yet supported.
    if (usesPalette()) return false;

    // Even numbers of channels are unsupported.
    if (m_ctype.channels % 2 == 0) return false;

    // Only grayscale images with less than 8 bits per channel are supported.
    if (m_ctype.channels > 1 && m_ctype.bitDepth < 8) return false;

    char magic = '6';
    if (m_ctype.channels == 1)
    {
        magic = m_ctype.bitDepth == 1 ? '4' : '5';
    }

    output << "P" << magic << "\n";
    output << std::dec << width() << " " << height();
    if (magic != '4')
    {
        output << " " << ((1 << m_ctype.bitDepth) - 1);
    }
    output << "\n";
    if (m_ctype.bitDepth == 8 || m_ctype.bitDepth == 16)
    {
        // No concern for 16-bit colours; everything is stored MSB first.
        output.write(reinterpret_cast<const char*>(m_data.data()),
                     m_data.size());
    }
    else if (m_ctype.bitDepth == 1)
    {
        // Apparently PBM has the convention 0 == white and 1 == black.
        // As this is not how we store the pixels, we have to invert every
        // single byte before writing them.
        std::vector<U8> data(m_data.size());
        for (size_t i = 0; i < m_data.size(); ++i)
        {
            data[i] = ~m_data[i];
        }
        output.write(reinterpret_cast<const char*>(data.data()), data.size());
    }
    else
    {
        std::vector<U8> dataOut(width() * height() * m_ctype.channels);
        for (unsigned y = 0; y < height(); ++y)
        {
            for (unsigned x = 0; x < width(); ++x)
            {
                auto col = getPixel(x, y);
                for (unsigned i = 1; i <= m_ctype.channels; ++i)
                {
                    dataOut[(y*width()+x)*m_ctype.channels
                            +(m_ctype.channels-i)] = col & 0xff;
                    col >>= 8;
                }
            }
        }
        output.write(reinterpret_cast<const char*>(dataOut.data()),
                     dataOut.size());
    }
    return output.good();
}

} // namespace image

} // namespace kraken
