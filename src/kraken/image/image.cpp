#include <kraken/image/image.hpp>
#include <kraken/math/integer.hpp>

#include <stdexcept>

namespace kraken
{

namespace image
{

Image::Image(const Image& img)
{
    *this = img;
}

Image::Image(Image&& img)
{
    *this = std::move(img);
}

Image& Image::operator=(const Image& img)
{
    m_metadata = img.m_metadata;
    m_palette = img.m_palette;
    m_data = img.m_data;
    copyTriviallyDestructibleVariables(img);
    return *this;
}

Image& Image::operator=(Image&& img)
{
    m_metadata = std::move(img.m_metadata);
    m_palette = std::move(img.m_palette);
    m_data = std::move(img.m_data);
    copyTriviallyDestructibleVariables(img);
    return *this;
}

void Image::copyTriviallyDestructibleVariables(const Image& img)
{
    m_bpp = img.m_bpp;
    m_mask = img.m_mask;
    m_width = img.m_width;
    m_height = img.m_height;
    m_gamma = img.m_gamma;
    m_ctype = img.m_ctype;
    m_palCtype = img.m_palCtype;
    m_colSpace = img.m_colSpace;
}

void Image::fill(const std::function<Colour(U32, U32)>& fillfunc)
{
    for (U32 y = 0; y < height(); ++y)
    {
        for (U32 x = 0; x < width(); ++x)
        {
            setPixel(x, y, fillfunc(x, y) & m_mask);
        }
    }
}

void Image::fill(const std::function<math::ivec4(U32, U32)>& fillfunc)
{
    for (U32 y = 0; y < height(); ++y)
    {
        for (U32 x = 0; x < width(); ++x)
        {
            setPixel(x, y, pack(fillfunc(x, y), colourType()) & m_mask);
        }
    }
}

void Image::fill(const std::function<math::uvec4(U32, U32)>& fillfunc)
{
    for (U32 y = 0; y < height(); ++y)
    {
        for (U32 x = 0; x < width(); ++x)
        {
            setPixel(x, y, pack(fillfunc(x, y), colourType()) & m_mask);
        }
    }
}

void Image::fill(const std::function<math::vec4(U32, U32)>& fillfunc)
{
    for (U32 y = 0; y < height(); ++y)
    {
        for (U32 x = 0; x < width(); ++x)
        {
            setPixel(x, y, pack(fillfunc(x, y), colourType()) & m_mask);
        }
    }
}

void Image::resize(U32 newWidth, U32 newHeight, ColourType newCType)
{
    if (!isColourTypeSupported(newCType))
    {
        throw std::runtime_error("Unsupported colour type.");
    }
    m_width = newWidth;
    m_height = newHeight;
    m_ctype = newCType;
    m_palCtype = {0, 0};
    m_data.clear();
    m_data.resize(rowBytes() * height(), 0);
    m_bpp = m_ctype.bitDepth * m_ctype.channels;
    m_mask = m_bpp >= 64 ? 0xffffffffffffffff : (U64(1) << U64(m_bpp)) - 1;
    m_gamma = 1.f;
}

Image Image::crop(U32 newWidth, U32 newHeight, U32 xOffset, U32 yOffset,
                  Colour replacementCol)
{
    Image img(newWidth, newHeight, colourType());
    U32 xMax = xOffset + width();
    U32 yMax = yOffset + height();
    U32 xMin = xOffset;
    U32 yMin = yOffset;
    if (xMax > newWidth) xMax = newWidth;
    if (yMax > newHeight) yMax = newHeight;

    img.fill([this,xMin,xMax,yMin,yMax,replacementCol](U32 x, U32 y)
    {
        if (xMin <= x && x < xMax && yMin <= y && y < yMax) return getPixel(x, y);
        return replacementCol;
    });
    return img;
}

bool Image::isColourTypeSupported(ColourType ctype) const noexcept
{
    auto logDepth = ilog2(ctype.bitDepth);
    // The bit depth must be a power of two.
    if (ctype.bitDepth != (1 << logDepth)) return false;
    // Also, the total number of bits per pixel must either be a multiple of 8
    // or evenly divide 8 (such that either pixels are entirely contained in one
    // byte or the bytes they occupy they occupy fully).
    if (ctype.channels == 3)
    {
        return ctype.bitDepth == 8 || ctype.bitDepth == 16;
    }
    if (ctype.channels == 1 || ctype.channels == 2 || ctype.channels == 4)
    {
        return ctype.bitDepth <= 16;
    }
    return false;
}

bool Image::read(std::istream& input)
{
    clear();
    U8 header[2] = { 0, 0 };
    input.read(reinterpret_cast<char*>(header), 2);
    if (header[0] == 'P' && '1' <= header[1] && header[1] <= '6')
    {
        return readPNM(input, header[1]-'0');
    }
    if (header[0] == 'B' && header[1] == 'M')
    {
        return readBMP(input);
    }
    if (header[0] == 0x89 && header[1] == 'P')
    {
        input.read(reinterpret_cast<char*>(header), 2);
        if (header[0] != 'N' || header[1] != 'G') return false;
        input.read(reinterpret_cast<char*>(header), 2);
        if (header[0] != 0x0d || header[1] != 0x0a) return false;
        input.read(reinterpret_cast<char*>(header), 2);
        if (header[0] != 0x1a || header[1] != 0x0a) return false;
        return readPNG(input);
    }
    if (header[0] == 0xff && header[1] == 0xd8)
    {
        return readJPEG(input);
    }
    return false;
}

bool Image::write(std::ostream& output, std::string fmt) const
{
    if (!m_width || !m_height)
    {
        throw std::runtime_error("Cannot write image with zero dimensions.");
    }
    fmt = lowercase(fmt);
    if (fmt == "png") return writePNG(output);
    if (fmt == "pnm" || fmt == "pgm" || fmt == "pbm") return writePNM(output);
    if (fmt == "bmp") return writeBMP(output);
    return false;
}

void Image::setMetadata(String key, String value)
{
    m_metadata[key] = value;
}

bool Image::hasMetadata(String key) const noexcept
{
    return m_metadata.find(key) != m_metadata.end();
}

const String& Image::getMetadata(String key) const
{
    return m_metadata.at(key);
}

void Image::removeMetadata()
{
    m_metadata.clear();
}

bool Image::usesPalette() const noexcept
{
    return !m_palette.empty();
}

void Image::removePalette() noexcept
{
    m_palette.clear();
}

size_t Image::paletteCount() const noexcept
{
    return m_palette.size();
}

void Image::createPalette(size_t elemCount)
{
    ColourType newCtype{1, 1};
    if (elemCount > (1 << 1)) newCtype.bitDepth = 2;
    if (elemCount > (1 << 2)) newCtype.bitDepth = 4;
    if (elemCount > (1 << 4)) newCtype.bitDepth = 8;
    if (elemCount > (1 << 8)) newCtype.bitDepth = 16;
    if (elemCount > (1 << 16))
    {
        throw std::runtime_error("Too many palette entries (>2**16).");
    }

    resize(width(), height(), newCtype);
    m_palette.resize(elemCount, 0);
    setPaletteColourType(RGB8);
}

void Image::setPaletteElement(size_t idx, Colour col)
{
    m_palette[idx] = col;
}

Colour Image::getPaletteElement(size_t idx) const
{
    return m_palette[idx];
}

ColourType Image::paletteColourType() const noexcept
{
    return m_palCtype;
}

void Image::setPaletteColourType(ColourType cType) noexcept
{
    m_palCtype = cType;
}

void Image::convertPaletteColourType(ColourType cType) noexcept
{
    for (auto& col : m_palette)
    {
        col = image::convert(col, m_palCtype, cType);
    }
    setPaletteColourType(cType);
}

void Image::applyPalette()
{
    if (!usesPalette()) return;
    Image img(width(), height(), paletteColourType());
    for (U32 y = 0; y < height(); ++y)
    {
        for (U32 x = 0; x < width(); ++x)
        {
            img.setPixel(x, y, getPaletteElement(getPixel(x, y)));
        }
    }
    std::swap(img.m_data, m_data);
    m_ctype = paletteColourType();
    removePalette();
}

Image Image::convert(ColourType newCType) const
{
    Image img(m_width, m_height, newCType);
    if (usesPalette())
    {
        for (size_t y = 0; y < m_height; ++y)
        {
            for (size_t x = 0; x < m_width; ++x)
            {
                img.setPixel(x, y,
                             image::convert(getPaletteElement(getPixel(x, y)),
                                            colourType(),
                                            newCType));
            }
        }
    }
    for (size_t y = 0; y < m_height; ++y)
    {
        for (size_t x = 0; x < m_width; ++x)
        {
            img.setPixel(x, y, image::convert(getPixel(x, y),
                                              colourType(),
                                              newCType));
        }
    }
    return img;
}

namespace
{

template <ColourSpace from, ColourSpace to>
void convertEntireImage(U8* data, U32 width, U32 height, U64 rowSize)
{
    auto padding = rowSize - width * 3;
    for (size_t y = 0; y < height; ++y)
    {
        for (size_t x = 0; x < width; ++x)
        {
            convert<from, to>(data[0], data[1], data[2],
                              data[0], data[1], data[2]);
            data += 3;
        }
        data += padding;
    }
}

} // end anonymous namespace

bool Image::transform(ColourSpace dest, std::optional<F32> newGamma) noexcept
{
    if (colourSpace() == dest)
        ;
    else if (colourType() == RGB8
             && colourSpace() == ColourSpace::YCbCrJPEG
             && dest == ColourSpace::sRGB)
    {
        convertEntireImage<ColourSpace::YCbCrJPEG, ColourSpace::sRGB>
            (data(), width(), height(), rowBytes());
    }
    else return false;

    m_gamma = newGamma.value_or(m_gamma);
    return true;
}

} // namespace image

} // namespace kraken
