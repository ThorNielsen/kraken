#include <kraken/image/image.hpp>
#include <kraken/utility/memory.hpp>

#include <stdexcept>

namespace kraken
{

namespace image
{

namespace
{

bool readBytes(std::istream& from, U8* to, size_t bytes)
{
    from.read(reinterpret_cast<char*>(to), bytes);
    if ((size_t)from.gcount() != bytes) return false;
    return true;
}

U64 readIntLE(U8* readFrom, U8 bytes)
{
    U64 val = 0;
    U64 shift = 0;
    while (bytes)
    {
        --bytes;
        val |= U64(*readFrom++) << shift;
        shift += 8;
    }
    return val;
}

bool skipWhitespace(std::istream& input)
{
    bool skipped = false;
    while (input.good() && std::iswspace(input.peek()))
    {
        input.get();
        skipped = true;
    }
    return skipped;
}

void skipRestOfLine(std::istream& input)
{
    while (input.good() && input.peek() != '\n' && input.peek() != '\r')
    {
        input.get();
    }
    input.get();
}

bool skipToNextNumber(std::istream& input)
{
    while (input.good() && (input.peek() < '0' || input.peek() > '9'))
    {
        bool skipped = false;
        if (input.peek() == '#')
        {
            skipRestOfLine(input);
            skipped = true;
        }
        if (skipWhitespace(input))
        {
            skipped = true;
        }
        if (!skipped) return false;
    }
    return true;
}

U32 readInteger(std::istream& input)
{
    U32 i = 0;
    while (input.good())
    {
        auto rd = input.peek();
        if (rd < '0' || rd > '9') break;
        input.get();
        i = 10*i + (rd-'0');
    }
    return i;
}

} // end anonymous namespace

bool Image::readBMP(std::istream& input)
{
    // 'BM' has been read.
    U8 header[40];
    if (!readBytes(input, header+2, 12)) return false;

    // We don't need to know the file size.
    //U32 fileSize = readIntLE(header+2, 4);

    // 2 shorts required to be zero.
    if (readIntLE(header+6, 4) != 0) return false;
    U32 dataOffset = readIntLE(header+10, 4);

    dataOffset -= 14; // Header bytes.

    // Read the first 40 bytes of the header, which is what we need.
    if (!readBytes(input, header, 40)) return false;
    U32 headerSize = readIntLE(header, 4);

    if (headerSize != 40)
    {
        if (headerSize == 108)
        {
            // We don't use any other than the first part of the 108-byte header
            // so we can safely ignore it. (It is not like this is some kind of
            // library processing images, right?)
            input.ignore(108-40);
        }
        else if (headerSize > 40)
        {
            // Hope for the best (it is likely some extended header).
            input.ignore(headerSize-40);
        }
        else
        {
            return false;
        }
    }

    dataOffset -= headerSize;

    U32 imgWidth = readIntLE(header+4, 4);
    U32 imgHeight = readIntLE(header+8, 4);
    if (readIntLE(header+12, 2) != 1) return false;
    U32 bpp = readIntLE(header+14, 2);
    U32 compressionType = readIntLE(header+16, 4);
    //U32 imgSize = readIntLE(header+20, 4);
    //U32 xppm = readIntLE(header+24, 4);
    //U32 yppm = readIntLE(header+28, 4);
    U32 usedColours = readIntLE(header+32, 4);
    //U32 significantColours = readIntLE(header+36, 4);

    // ^ Some of the above values are not used for importing. E.g. # of
    // significant colours is not very relevant, there is currently no way to
    // store # of pixels per meter, and imgSize is not needed for anything (it
    // is computable from the other parameters).

    U64 rowWidth = (imgWidth * U64(bpp) + 7) >> 3;
    while (rowWidth % 4)
        ++rowWidth;

    ColourType newCT;

    // Hack: If there are more than 8 bits per pixel, we assume that there are
    // bpp/8 channels (i.e. there are n channels each consisting of 8 bits).
    newCT.channels = (bpp+7) >> 3;
    newCT.bitDepth = bpp / newCT.channels;

    if (compressionType == 3)
    {
        // Todo: Use the masks to correctly interpret the data (and read them
        // from the header).
    }
    else if (compressionType != 0) return false; // Unimplemented.


    resize(imgWidth, imgHeight, newCT);

    bool paletteUsed = bpp <= 8;
    if (paletteUsed && !usedColours) usedColours = 256;
    if (paletteUsed)
    {
        createPalette(usedColours);
        setPaletteColourType(RGB8);
        auto palData = make_unique<U8[]>(usedColours*4);
        if (!readBytes(input, palData.get(), usedColours*4)) return false;
        dataOffset -= usedColours * 4;
        for (U32 i = 0; i < usedColours; ++i)
        {
            U8 b = palData[4*i  ];
            U8 g = palData[4*i+1];
            U8 r = palData[4*i+2];
            setPaletteElement(i, pack(math::uvec4{r,g,b,0}, RGB8));
        }
    }
    input.ignore(dataOffset);

    U64 imgDataSize = rowWidth * imgHeight;

    U64 bufSize = 32*1024;
    bufSize += rowWidth - bufSize % rowWidth;
    if (bufSize > imgDataSize) bufSize = imgDataSize;

    auto rowsPerBuf = bufSize / rowWidth;
    U64 readImageData = 0;
    auto buffer = make_unique<U8[]>(bufSize);
    U32 lastRow = 0;
    while (readImageData < imgDataSize)
    {
        U64 toRead = bufSize;
        if (toRead > imgDataSize - readImageData)
        {
            toRead = imgDataSize - readImageData;
        }
        if (!readBytes(input, buffer.get(), toRead)) return false;
        readImageData += toRead;

        // Ensure that we don't try to read non-existent rows.
        if (imgHeight - lastRow < rowsPerBuf)
        {
            rowsPerBuf = imgHeight - lastRow;
        }
        for (U32 rowCnt = 0; rowCnt < rowsPerBuf; ++rowCnt)
        {
            U32 y = lastRow++;
            const U8* readFrom = buffer.get() + rowCnt * rowWidth;
            if (bpp == 1 || bpp == 4 || bpp == 8)
            {
                U64 offset = (height() - 1 - y) * rowBytes();
                U8* writeAt = m_data.data() + offset;
                U32 bytesToRead = (imgWidth >> 3) + (imgWidth % 8 != 0);
                if (bpp == 4)
                {
                    bytesToRead = (imgWidth >> 1) + (imgWidth % 2 != 0);
                }
                if (bpp == 8)
                {
                    bytesToRead = imgWidth;
                }
                for (U32 x = 0; x < bytesToRead; ++x)
                {
                    *writeAt++ = *readFrom++;
                }
            }
            else
            {
                for (U32 x = 0; x < imgWidth; ++x)
                {
                    U8 b = *readFrom++;
                    U8 g = *readFrom++;
                    U8 r = *readFrom++;
                    U8 a = 0;
                    if (bpp == 32) a = *readFrom++;
                    setPixel(x, height()-1-y,
                             pack(math::uvec4{r,g,b,a}, colourType()));
                }
            }
        }
    }

    return true;
}

bool Image::readPNM(std::istream& input, U8 variant)
{
    if (!skipToNextNumber(input)) return false;
    auto newWidth = readInteger(input);
    if (!skipToNextNumber(input)) return false;
    auto newHeight = readInteger(input);
    U32 maxColVal = 1;
    if (variant != 1 && variant != 4)
    {
        if (!skipToNextNumber(input)) return false;
        maxColVal = readInteger(input);
    }
    while (input.good() && input.peek() == '#')
    {
        skipRestOfLine(input);
    }
    if (!input.good() || (input.peek() != '\n' && input.peek() != '\r'))
    {
        return false; // Invalid file.
    }
    input.get(); // Skip final newline.

    ColourType newCtype;
    newCtype.bitDepth = 1;
    if (maxColVal > 1) newCtype.bitDepth = 2;
    if (maxColVal > 3) newCtype.bitDepth = 4;
    if (maxColVal > 15) newCtype.bitDepth = 8;
    if (maxColVal > 255) newCtype.bitDepth = 16;

    bool rawReadPossible = false;
    if (newCtype.bitDepth >= 8 || newCtype.bitDepth == 1)
    {
        rawReadPossible = true;
    }

    newCtype.channels = 1;
    if (variant == 3 || variant == 6)
    {
        newCtype.channels = 3;
        if (newCtype.bitDepth < 8)
        {
            newCtype.bitDepth = 8;
            rawReadPossible = false;
        }
    }

    resize(newWidth, newHeight, newCtype);

    if (variant == 1 || variant == 2 || variant == 3)
    {
        if (!readPlainPNM(input, variant)) return false;
    }
    else
    {
        if (!readBinaryPNM(input, variant, rawReadPossible)) return false;
    }
    if (U64(1 << colourType().bitDepth)-1 != maxColVal)
    {
        // We have to rescale the colour values because there is a weird number
        // of colours (also known as one not of the form 2^(2^n)).
        U32 newMax = (1 << colourType().bitDepth)-1;
        U32 oldMax = maxColVal;
        // Ensure correct rounding.
        U32 half = oldMax >> 1;
        if (colourType().channels == 1)
        {
            for (U32 y = 0; y < height(); ++y)
            {
                for (U32 x = 0; x < width(); ++x)
                {
                    setPixel(x, y, (getPixel(x, y) * newMax + half) / oldMax);
                }
            }
        }
        else
        {
            for (U32 y = 0; y < height(); ++y)
            {
                for (U32 x = 0; x < width(); ++x)
                {
                    auto col = iunpack(getPixel(x, y), colourType());
                    col.r = (col.r * newMax + half) / oldMax;
                    col.g = (col.g * newMax + half) / oldMax;
                    col.b = (col.b * newMax + half) / oldMax;
                    setPixel(x, y, pack(col, colourType()));
                }
            }
        }
    }
    return true;
}

bool Image::readPlainPNM(std::istream& input, U8 variant)
{
    if (variant == 1)
    {
        for (U32 y = 0; y < height(); ++y)
        {
            for (U32 x = 0; x < width(); ++x)
            {
                skipWhitespace(input);
                setPixel(x, y, input.get() == '0');
            }
        }
    }
    else
    {
        for (U32 y = 0; y < height(); ++y)
        {
            for (U32 x = 0; x < width(); ++x)
            {
                skipWhitespace(input);
                Colour col = readInteger(input);
                if (colourType().channels == 3)
                {
                    col <<= colourType().bitDepth;
                    skipWhitespace(input);
                    col |= readInteger(input);
                    col <<= colourType().bitDepth;
                    skipWhitespace(input);
                    col |= readInteger(input);
                }
                setPixel(x, y, col);
            }
        }
    }
    return input.good();
}

bool Image::readBinaryPNM(std::istream& input, U8 variant, bool rawReadPossible)
{
    U64 bytesToRead = rowBytes() * height();

    if (rawReadPossible)
    {
        input.read(reinterpret_cast<char*>(m_data.data()), bytesToRead);
        if ((U64)input.gcount() != bytesToRead) return false; // Too little data.
        if (variant == 4)
        {
            // Fix 0 being white and 1 being black.
            for (U8& byte : m_data)
            {
                byte = ~byte;
            }
        }
    }
    else
    {
        // If the bit depth is < 8 then all samples fit in a single byte. Note
        // that 3-channel images then have 3 bytes per pixel.
        bytesToRead = colourType().channels * width() * height();
        auto buf = make_unique<U8[]>(bytesToRead);
        input.read(reinterpret_cast<char*>(buf.get()), bytesToRead);
        if ((U64)input.gcount() != bytesToRead) return false;
        U32 readRowWidth = colourType().channels * width();
        if (variant == 5)
        {
            for (U32 y = 0; y < height(); ++y)
            {
                for (U32 x = 0; x < width(); ++x)
                {
                    setPixel(x, y, buf[y*readRowWidth+x]);
                }
            }
        }
        if (variant == 6)
        {
            for (U32 y = 0; y < height(); ++y)
            {
                for (U32 x = 0; x < width(); ++x)
                {
                    Colour c = buf[readRowWidth*y+3*x];
                    c <<= colourType().bitDepth;
                    c |= buf[readRowWidth*y+3*x+1];
                    c <<= colourType().bitDepth;
                    c |= buf[readRowWidth*y+3*x+2];
                    setPixel(x, y, c);
                }
            }
        }
    }
    return true;
}

} // namespace image

} // namespace kraken
