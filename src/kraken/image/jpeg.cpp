#include <kraken/image/image.hpp>

#include <kraken/image/colourspace.hpp>
#include <kraken/image/dct.hpp>
#include <kraken/image/sampling.hpp>
#include <kraken/types.hpp>
#include <kraken/utility/hufftree.hpp>
#include <kraken/utility/memory.hpp>

#include <array>
#include <optional>
#include <vector>
#include <unordered_map>

namespace kraken::image
{

// At some point this should be moved to a (mostly) public header.
struct JPEGDecodeOptions
{
    bool extractThumbnail = false;
};

namespace
{

enum class JFIFUnits : U8
{
    NoUnits = 0,
    DotsPerInch = 1,
    DotsPerCentimeter = 2,
};

struct JFIFInfo
{
    U8 majorVersion;
    U8 minorVersion;
    U8 componentCount;
    JFIFUnits units;
    ColourSpace space;
    math::Vector<2, U16> density;
    std::optional<Image> thumbnail;
};

struct ComponentInfo
{
    U8 componentID;
    U8 sampleFactorX;
    U8 sampleFactorY;
    U8 quantisationTable;
};

struct FrameInfo
{
    std::vector<ComponentInfo> components;
    size_t imageWidth; // In pixels
    size_t imageHeight; // In pixels
};

struct ScanComponent
{
    S16 predictor;
    U8 componentSelector;
    U8 dcTable;
    U8 acTable;

    size_t stretchFactorX;
    size_t stretchFactorY;
};

struct ScanInfo
{
    std::array<ScanComponent, 4> components;
    size_t componentCount;
    size_t firstDCT;
    size_t lastDCT;
    size_t approxBitHigh;
    size_t approxBitLow;

    // Note: Scans must code enough MCUs to fill out an entire lineMCUs must be coded such that they fill out lines completely; so
    // while MCU sizes are not necessarily stable over the entire frame, they
    // are over a scan, which as mentioned does fill lines correctly.
    size_t mcuWidth;
    size_t mcuHeight;

    // Mutates during MCU decoding; expreses positions in *pixels*.
    size_t nextMCUXStart;
    size_t nextMCUYStart;
};

// Note: For internal use only; and *instances* are not thread-safe.
class JPEGDecoder
{
public:
    JPEGDecoder()
    {
        clear();
    }

    void clear();

    // Right now we require the entire image in memory -- of course that is not
    // the final intended version.
    Image read(const U8* begin, const U8* end, JPEGDecodeOptions options = {});

private:
    bool parseSOFHeader(const U8*& headerBegin, const U8* dataEnd);

    bool parseHeaderSize(const U8*& headerBegin,
                         const U8* dataEnd,
                         size_t& sizeOut);

    bool gobbleSegment(const U8*& headerBegin, const U8* dataEnd);

    bool parseSOSHeader(const U8*& headerBegin, const U8* dataEnd);
    bool parseDQTHeader(const U8*& headerBegin, const U8* dataEnd);
    bool parseDHTHeader(const U8*& headerBegin, const U8* dataEnd);
    bool parseAPP0Header(const U8*& headerBegin, const U8* dataEnd);

    bool parseJFIFSegment(const U8*& begin, size_t segmentBytesLeft);

    bool decodeSingleBlock(ScanComponent& currComponent,
                           const U8* begin,
                           const U8* dataEnd,
                           U64& bitpos,
                           U8* decodeDst);
    void decodeMCU(const U8* begin, const U8* dataEnd, U64& bitpos);
    const U8* parseScan(const U8* scanBegin, const U8* dataEnd);
    const U8* parseSegment(U8 marker, const U8* segmentBegin, const U8* imageEnd);

    bool isAtEndOfImage() const noexcept { return m_eoi; }

    void handleRestartInterval(U8 valueMod8);

    // Returns nullptr if component doesn't exist.
    const ComponentInfo* getComponentInfo(U8 componentID) const noexcept;

    // Gets the index of a component into the frame. Be warned, though, as this
    // throws exceptions on invalid components.
    size_t componentPosition(U8 componentID) const;

    bool shouldDecodeThumbnail() const noexcept
    {
        return m_decodeOpts.extractThumbnail;
    }

    std::array<kraken::priv::HufftreeDecoder, 4> m_acTrees;
    std::array<kraken::priv::HufftreeDecoder, 4> m_dcTrees;
    std::array<std::optional<std::array<U16, 64>>, 4> m_quantisationTables;

    // Large enough to hold a single decoded block.
    std::array<U8, 64> m_decodeBuffer;
    std::array<float, 64> m_dequantBuf;

    FrameInfo m_frameInfo;
    ScanInfo m_currScan;
    Image m_currImage;

    JPEGDecodeOptions m_decodeOpts;

    // Holds number of MCU read so far.
    size_t m_mcuReadCount;

    // Holds number of lines read until the current scan.
    size_t m_currScanLineOffset;

    std::optional<JFIFInfo> m_jfifInfo;

    bool m_eoi;
};

void JPEGDecoder::clear()
{
    for (size_t i = 0; i < 4; ++i)
    {
        m_acTrees[i].clear();
        m_dcTrees[i].clear();
        m_quantisationTables[i] = std::nullopt;
    }
    m_frameInfo.components.clear();
    m_currScan.componentCount = 0;
    m_currImage.clear();
    m_mcuReadCount = 0;
    m_currScanLineOffset = 0;
    m_jfifInfo = std::nullopt;
    m_eoi = false;
}

const ComponentInfo* JPEGDecoder::getComponentInfo(U8 componentID) const noexcept
{
    for (const auto& elem : m_frameInfo.components)
    {
        if (componentID == elem.componentID) return &elem;
    }
    return nullptr;
}

size_t JPEGDecoder::componentPosition(U8 componentID) const
{
    for (size_t i = 0; i < m_frameInfo.components.size(); ++i)
    {
        if (componentID == m_frameInfo.components[i].componentID) return i;
    }
    throw std::runtime_error("Invalid component id.");
}

void JPEGDecoder::handleRestartInterval(U8 valueMod8)
{
    (void)valueMod8;
    throw std::logic_error("Not yet implemented.");
}

// Skips over all 0xff bytes and guarantees that the returned pointer is either
// 'end' or points to the second byte of a JPEG marker.
const U8* nextJPEGMarkerByte(const U8* currPos, const U8* end) noexcept
{
    bool wasLastBegin = false;
    while (currPos != end)
    {
        if (*currPos == 0xff) wasLastBegin = true;
        else if (wasLastBegin && *currPos) return currPos;
        else wasLastBegin = false;
        ++currPos;
    }
    return end;
}

bool JPEGDecoder::parseSOFHeader(const U8*& headerBegin, const U8* dataEnd)
{
    size_t numBytes;
    if (!parseHeaderSize(headerBegin, dataEnd, numBytes)) return false;

    size_t precision = readBigEndian<U8>(headerBegin);
    size_t lineCount = readBigEndian<U16>(headerBegin);
    size_t pixelsPerLine = readBigEndian<U16>(headerBegin);
    size_t numImageComponents = readBigEndian<U8>(headerBegin);

    if (numImageComponents > 4)
    {
        throw std::logic_error("No support for more than 4 image components.");
    }

    m_frameInfo.imageWidth = pixelsPerLine;
    m_frameInfo.imageHeight = lineCount;

    constexpr size_t fixedHeaderSize = 1+2+2+1;

    if (precision < 2 || precision > 16) return false;
    if (numImageComponents * 3 + fixedHeaderSize != numBytes) return false;

    auto& components = m_frameInfo.components;
    if (!components.empty()) components.clear();

    for (size_t i = 0; i < numImageComponents; ++i)
    {
        components.push_back({});
        auto& compInfo = components.back();
        compInfo.componentID = readBigEndian<U8>(headerBegin);
        size_t packedSampleFactors = readBigEndian<U8>(headerBegin);
        compInfo.sampleFactorX = packedSampleFactors >> 4;
        compInfo.sampleFactorY = packedSampleFactors & 0xf;
        compInfo.quantisationTable = readBigEndian<U8>(headerBegin);
    }

    // 8 bits per pixel is the assumption here.
    m_currImage.resize(pixelsPerLine, lineCount,
                       ColourType{(U8)numImageComponents, 8});

    // Practically all JPEG files has YCbCr colour space;
    m_currImage.setColourSpace(ColourSpace::YCbCrJPEG);

    return true;
}

bool JPEGDecoder::parseDQTHeader(const U8*& headerBegin, const U8* dataEnd)
{
    size_t bytesLeft;
    if (!parseHeaderSize(headerBegin, dataEnd, bytesLeft)) return false;

    if (!bytesLeft) return false;

    while (bytesLeft)
    {
        size_t precision = *headerBegin >> 4;
        size_t destination = *headerBegin & 0xf;
        if (precision > 1 || destination > 3) return false;
        ++headerBegin;
        --bytesLeft;
        size_t tableSize = 64 * (precision + 1);
        if (bytesLeft < tableSize) return false;
        bytesLeft -= tableSize;

        auto& table = (m_quantisationTables[destination]
                       = std::make_optional<std::array<U16, 64>>()).value();

        if (!precision) for (size_t i = 0; i < 64; ++i)
        {
            table[i] = *headerBegin++;
        }
        else for (size_t i = 0; i < 64; ++i)
        {
            table[i] = readBigEndian<U16>(headerBegin);
        }
    }

    headerBegin += bytesLeft;

    return true;
}

bool JPEGDecoder::parseDHTHeader(const U8*& headerBegin, const U8* dataEnd)
{
    size_t bytesLeft;
    if (!parseHeaderSize(headerBegin, dataEnd, bytesLeft)) return false;

    if (!bytesLeft) return false;

    while (bytesLeft)
    {
        size_t tableClass = *headerBegin >> 4;
        size_t destination = *headerBegin & 0xf;
        ++headerBegin;
        --bytesLeft;

        std::vector<U8> elemCodeLengths;
        std::vector<U16> elemValues;

        if (bytesLeft < 16) return false;
        std::array<U8, 16> codeLengths;
        bytesLeft -= 16;
        size_t totalLengths = 0;
        for (size_t i = 0; i < 16; ++i)
        {
            totalLengths += (codeLengths[i] = *headerBegin++);
        }
        if (bytesLeft < totalLengths) return false;
        std::vector<std::vector<U8>> codeValues(16);
        for (size_t i = 0; i < 16; ++i)
        {
            for (size_t j = 0; j < codeLengths[i]; ++j)
            {
                codeValues[i].push_back(*headerBegin++);
                elemCodeLengths.push_back(i+1);
                elemValues.push_back(codeValues[i].back());
            }
        }
        bytesLeft -= totalLengths;

        auto& treedest = tableClass ? m_acTrees : m_dcTrees;

        treedest[destination].create(elemCodeLengths.data(),
                                     elemCodeLengths.size(),
                                     elemValues.data(),
                                     false);
    }
    headerBegin += bytesLeft;
    return true;
}

bool JPEGDecoder::parseAPP0Header(const U8*& headerBegin, const U8* dataEnd)
{
    size_t bytesLeft;
    if (!parseHeaderSize(headerBegin, dataEnd, bytesLeft)) return false;

    if (bytesLeft >= 5
        && !std::memcmp(headerBegin, "JFIF\0", 5))
    {
        return parseJFIFSegment(headerBegin += 5, bytesLeft-5);
    }
    else if (bytesLeft >= 5
             && !std::memcmp(headerBegin, "JFXX\0", 5))
    {
        return false; // Not supported yet.
    }

    return false; // E.g. JFXX not supported yet.
}

bool JPEGDecoder::parseJFIFSegment(const U8*& begin, size_t bytesLeft)
{
    constexpr static size_t jfifHeaderSize = 2+1+2+2+1+1;
    if (bytesLeft < jfifHeaderSize) return false;
    bytesLeft -= jfifHeaderSize;

    JFIFInfo jfif;
    jfif.majorVersion = *begin++;

    if (jfif.majorVersion != 1) return false; // This implements spec 1.02.

    jfif.minorVersion = *begin++;
    U8 unitStor = *begin++;
    jfif.units = unitStor < 3
               ? static_cast<JFIFUnits>(unitStor)
               : JFIFUnits::NoUnits; // As fallback.
    jfif.density.x = readBigEndian<U16>(begin);
    jfif.density.y = readBigEndian<U16>(begin);
    size_t thumbnailX = *begin++;
    size_t thumbnailY = *begin++;

    // Thumbnails are RGB values, so that's where 3 comes from.
    size_t thumbSize = thumbnailX * thumbnailY * 3;

    if (shouldDecodeThumbnail() && thumbSize)
    {
        // We only check bounds if we are decoding thumbnails, so if something
        // generates invalid thumbnails but otherwise valid files, they will
        // still be decodable.
        if (bytesLeft < thumbSize) return false;

        jfif.thumbnail = std::make_optional<Image>(thumbnailX,
                                                   thumbnailY,
                                                   RGB8);

        std::memcpy(jfif.thumbnail->data(), begin, thumbSize);
        begin += thumbSize;
        bytesLeft -= thumbSize;
    }

    if (bytesLeft)
    {
        throw std::runtime_error("Extra data left at the end of JFIF segment.");
    }
    return true;
}

// WARNING: Only advances bitpos if the required number of bits is successfully
// read.
std::optional<U16> readNextBits(U32 numBits, // Must be <= 16
                                const U8* dataBegin,
                                const U8* dataEnd,
                                size_t& bitpos)
{
    U32 result{0};
    U32 bitOffset = bitpos & 7;
    U32 numBitsToRead = numBits + bitOffset; // We'll discard the extra ones
                                             // because bitOffset is the number
                                             // of bits we've already read.
    dataBegin += bitpos >> 3;

    U32 readBits = 0;

    while (dataBegin != dataEnd && readBits < numBitsToRead) // "while (data left)"
    {
        result = (result << 8) | *dataBegin++;
        readBits += 8;
    }
    if (readBits < numBitsToRead) return std::nullopt;
    bitpos += numBits;
    result = (result >> (readBits - numBitsToRead)) & ((U32(1)<<numBits)-1);
    return result;
}

std::optional<U16> decode(const priv::HufftreeDecoder& decoder,
                          const U8* dataBegin,
                          const U8* dataEnd,
                          size_t& bitpos)
{
    U32 result{0};
    U32 bitOffset = bitpos & 7;
    dataBegin += bitpos >> 3;

    U8 byteMask = (U16(1) << U16(8 - bitOffset))-1;

    U32 readBits = 0;

    while (dataBegin != dataEnd && readBits < 16) // "while (data left)"
    {
        result = (result << 8) | (*dataBegin++ & byteMask);
        readBits += 8-bitOffset;
        bitOffset = 0;

        U16 parseMe = readBits <= 16 ? result << (16-readBits) : result >> (readBits-16);

        auto decoded = decoder.decodeSymbol(parseMe);
        if (decoded.length && decoded.length <= readBits)
        {
            bitpos += decoded.length;
            return decoded.decoded;
        }
        byteMask = 0xff;
    }

    return std::nullopt; // Decoding failed!
}

std::optional<S16>
decodeCoefficient(S16 numBits, // MUST be <= 0xf
                  const U8* dataBegin, const U8* dataEnd, U64& bitpos)
{
    auto nextBits = readNextBits(numBits, dataBegin, dataEnd, bitpos);
    if (!nextBits.has_value()) return std::nullopt;
    auto value = (S16)nextBits.value();
    auto magnitude = S16(1) << S16(numBits-1);
    return value >= magnitude
         ? value
         : value - (magnitude<<(S16)1) + 1;
}

// Returns whether decoding was successful or not. If not, that is not because
// of an error in the data (which simply results in a thrown exception), but
// because this is at the end of data.
// Note that decodeDst must have AT LEAST 64 bytes space (and this is assuming
// 8-bit data).
bool JPEGDecoder::decodeSingleBlock(ScanComponent& currComponent,
                                    const U8* begin,
                                    const U8* dataEnd,
                                    U64& bitpos,
                                    U8* decodeDst)
{
    U64 bitsLeft = static_cast<U64>(dataEnd - begin)<<U64(3);
    bitsLeft -= bitpos;
    if (bitsLeft <= 7)
    {
        if (!bitsLeft) return false;
        // If there's only 7 or fewer bits left, we might be at the end of a
        // block. Thus, we have to check if the last bits are only ones, since
        // that is exactly what the end of a scan is stuffed with; in which case
        // we won't even attempt any parsing as that cannot work.
        auto currByte = *(begin + (bitpos >> 3));
        U8 mask = U8(1 << bitsLeft) - 1;
        if ((currByte & mask) == mask)
        {
            bitpos += bitsLeft; // Om nom nom
            return false;
        }
    }
    const auto& dcDecoder = m_dcTrees[currComponent.dcTable];
    const auto& acDecoder = m_acTrees[currComponent.acTable];

    if (dcDecoder.empty())
    {
        throw std::runtime_error("Referred DC table has not been defined.");
    }
    if (acDecoder.empty())
    {
        throw std::runtime_error("Referred AC table has not been defined.");
    }

    auto dcBits = decode(dcDecoder, begin, dataEnd, bitpos).value();
    if (dcBits > 15)
    {
        // Note: Baseline/8 bits defines up to 11 bits, extended/12 bits defines
        // up to 15.
        throw std::runtime_error("Bad number of dc bits.");
    }
    S16 diff = dcBits
             ? decodeCoefficient(dcBits, begin, dataEnd, bitpos).value()
             : 0;

    std::array<S16, 64> coefficients;
    coefficients[0] = diff + currComponent.predictor;
    currComponent.predictor = coefficients[0];

    size_t i;
    for (i = 1; i < 64;)
    {
        auto codebyte = decode(acDecoder, begin, dataEnd, bitpos).value();
        if (codebyte > 0xff)
        {
            throw std::runtime_error("Unexpectedly large encoded zigzag coeff.");
        }
        if (!codebyte) break; // End of block.
        if (size_t(codebyte >> U16(4)) >= 64 - i)
        {
            throw std::logic_error("Coefficients out of bounds.");
        }
        for (size_t k = codebyte >> 4; k; --k)
        {
            coefficients[i++] = 0;
        }
        auto partial = codebyte & 0xf;
        if (!partial)
        {
            coefficients[i++] = 0;
        }
        else
        {
            S16 value = decodeCoefficient(partial, begin, dataEnd, bitpos).value();
            coefficients[i++] = value;
        }
    }
    while (i < 64) coefficients[i++] = 0;

    constexpr static U8 coeffOrdering[64] =
    {
         0,  8,  1,  2,  9, 16, 24, 17,
        10,  3,  4, 11, 18, 25, 32, 40,
        33, 26, 19, 12,  5,  6, 13, 20,
        27, 34, 41, 48, 56, 49, 42, 35,
        28, 21, 14,  7, 15, 22, 29, 36,
        43, 50, 57, 58, 51, 44, 37, 30,
        23, 31, 38, 45, 52, 59, 60, 53,
        46, 39, 47, 54, 61, 62, 55, 63,
    };

    auto compInfo = getComponentInfo(currComponent.componentSelector);
    if (!compInfo)
    {
        throw std::logic_error("Unknown component referred in scan.");
    }

    const auto& table = m_quantisationTables.at(compInfo->quantisationTable);
    if (!table.has_value())
    {
        throw std::runtime_error("Tried to decode using undefined "
                                 "quantisation table.");
    }

    const auto* quanttable = m_quantisationTables.at(compInfo->quantisationTable)->data();

    for (i = 0; i < 64; ++i)
    {
        size_t dst = coeffOrdering[i];
        size_t src = i;
        m_dequantBuf[dst] = (float)quanttable[src] * (float)coefficients[src];
    }

    jpeg8x8IDCTBy1DFactorisationFast(m_dequantBuf.data(), decodeDst);
    decodeDst += 64;
    return true;
}

// Note: Doesn't handle spontaneous markers... But they are fortuitously handled
// by passing unstuffed data into this, so this can just read as long as there's
// any data left.
void JPEGDecoder::decodeMCU(const U8* begin, const U8* dataEnd, U64& bitpos)
{
    // Assuming 8 bits / channel.
    auto pixelStride = m_frameInfo.components.size();
    auto rowStride = m_currImage.rowBytes();

    for (size_t compIndex = 0;
         compIndex < m_currScan.componentCount;
         ++compIndex)
    {
        auto& currComp = m_currScan.components[compIndex];
        const auto* compSel = getComponentInfo(currComp.componentSelector);
        if (!compSel)
        {
            // Should have been pre-checked.
            throw std::logic_error("Decoding invalid component in MCU.");
        }
        size_t xStretch = currComp.stretchFactorX;
        size_t yStretch = currComp.stretchFactorY;
        for (size_t yBlock = 0; yBlock < compSel->sampleFactorY; ++yBlock)
        {
            bool ignoreY = false;
            size_t yPixelBegin = m_currScan.nextMCUYStart
                               + 8*yBlock*yStretch;
            if (yPixelBegin >= m_currImage.height())
            {
                ignoreY = true; // Some blocks can go out of range, e.g. if we
                                // have a subsampled 8x8-image.
            }

            for (size_t xBlock = 0; xBlock < compSel->sampleFactorX; ++xBlock)
            {
                size_t xPixelBegin = m_currScan.nextMCUXStart
                                   + 8*xBlock*xStretch;
                bool ignoreX = false;
                if (xPixelBegin >= m_currImage.width())
                {
                    ignoreX = true;
                }
                if (!decodeSingleBlock(currComp,
                                       begin,
                                       dataEnd,
                                       bitpos,
                                       m_decodeBuffer.data()))
                {
                    if (!compIndex && !xBlock && !yBlock)
                    {
                        return; // No MCU to decode as we are at the end.
                    }
                    throw std::logic_error("Incomplete MCU (too few components"
                                           " to decode).");
                }
                if (ignoreX || ignoreY) continue;

                auto xPixelsLeft = std::min<size_t>(8, (m_currImage.width() - xPixelBegin + (xStretch - 1))/xStretch);
                auto yPixelsLeft = std::min<size_t>(8, (m_currImage.height() - yPixelBegin + (yStretch - 1))/yStretch);

                if (!xPixelsLeft || !yPixelsLeft)
                {
                    throw std::logic_error("Earlier bounds check did not catch "
                                           "\"no pixels left\"-situation.");
                }

                size_t xSkip = 8-xPixelsLeft;

                auto* dstBegin = m_currImage.data()
                               + yPixelBegin * rowStride
                               + xPixelBegin * pixelStride
                               + componentPosition(currComp.componentSelector);

                const auto* src = m_decodeBuffer.data();

                for (size_t y = 0; y < yPixelsLeft; ++y)
                {
                    auto offset = yStretch * y * rowStride;
                    for (size_t x = 0; x < xPixelsLeft; ++x)
                    {
                        dstBegin[offset] = *src++;
                        offset += xStretch*pixelStride;
                    }
                    src += xSkip;
                }
            }
        }
    }

    m_currScan.nextMCUXStart += m_currScan.mcuWidth;
    if (m_currScan.nextMCUXStart >= m_currImage.width())
    {
        m_currScan.nextMCUXStart = 0;
        m_currScan.nextMCUYStart += m_currScan.mcuHeight;
    }
    ++m_mcuReadCount;
}

const U8* JPEGDecoder::parseScan(const U8* scanBegin, const U8* dataEnd)
{
    std::vector<U8> bytebuffer;
    for (; scanBegin != dataEnd; ++scanBegin)
    {
        if (*scanBegin != 0xff) bytebuffer.push_back(*scanBegin);
        else
        {
            ++scanBegin;
            if (scanBegin != dataEnd)
            {
                if (*scanBegin)
                {
                    --scanBegin;
                    break;
                }
                else bytebuffer.push_back(0xff);
            }
            else break;
        }
    }

    U64 bitpos = 0;
    U64 bitlen = bytebuffer.size() << U64(3);

    const U8* bufBegin = bytebuffer.data();
    auto bufEnd = bufBegin + bytebuffer.size();

    while (bitpos < bitlen)
    {
        decodeMCU(bufBegin, bufEnd, bitpos);
    }
    for (size_t compIndex = 0;
         compIndex < m_currScan.componentCount;
         ++compIndex)
    {
        auto& currComp = m_currScan.components[compIndex];
        const auto* compSel = getComponentInfo(currComp.componentSelector);
        if (!compSel)
        {
            // Should have been pre-checked.
            throw std::logic_error("Invalid scan component detected.");
        }
        if (currComp.stretchFactorX != 1 || currComp.stretchFactorY != 1)
        {
            upsampleComponentInplace(m_currImage,
                                     UpsamplingMethod::Linear,
                                     compIndex,
                                     currComp.stretchFactorX,
                                     currComp.stretchFactorY);
        }
    }

    return scanBegin;
}


// This parses the size of a header when headerBegin is at the beginning of a
// header size field; it increments headerBegin by the size of that field and
// returns the actual size in sizeOut. It also performs bounds checking using
// dataEnd and thus (contingent upon the input dataEnd being correct), after
// this function it is safe to read sizeOut more bytes starting from headerBegin
// as its value is after this function.
// If something is wrong (too little remaining data, etc.), then this instead
// returns false.
bool JPEGDecoder::parseHeaderSize(const U8*& headerBegin,
                                  const U8* dataEnd,
                                  size_t& sizeOut)
{
    sizeOut = 0;
    if (headerBegin + 2 >= dataEnd) return false;
    sizeOut = readBigEndian<U16>(headerBegin) - 2;
    auto bytesLeft = static_cast<size_t>(dataEnd - headerBegin);
    if (sizeOut > bytesLeft)
    {
        sizeOut = bytesLeft;
        return false;
    }
    return true;
}

bool JPEGDecoder::gobbleSegment(const U8*& headerBegin, const U8* dataEnd)
{
    size_t bytesLeft;
    if (!parseHeaderSize(headerBegin, dataEnd, bytesLeft)) return false;
    headerBegin += bytesLeft;
    return true;
}

bool JPEGDecoder::parseSOSHeader(const U8*& headerBegin, const U8* dataEnd)
{
    size_t bytesLeft;
    if (!parseHeaderSize(headerBegin, dataEnd, bytesLeft)) return false;

    if (!bytesLeft) return false;
    size_t numComponents = readBigEndian<U8>(headerBegin);
    if (numComponents < 1 || numComponents > 4) return false;
    --bytesLeft;

    if (bytesLeft < numComponents * 2) return false;
    bytesLeft -= numComponents * 2;

    m_currScan.componentCount = numComponents;

    size_t maxSampleFacX = 1;
    size_t maxSampleFacY = 1;


    for (size_t i = 0; i < numComponents; ++i)
    {
        auto& scanComp = m_currScan.components[i];
        scanComp.componentSelector = *headerBegin++;
        scanComp.dcTable = *headerBegin >> 4;
        scanComp.acTable = *headerBegin & 0xf;
        scanComp.predictor = 0;
        ++headerBegin;

        auto currComp = getComponentInfo(scanComp.componentSelector);
        if (!currComp)
        {
            throw std::runtime_error("Scan refers to undefined component.");
        }
        maxSampleFacX = std::max<size_t>(maxSampleFacX,
                                         currComp->sampleFactorX);
        maxSampleFacY = std::max<size_t>(maxSampleFacY,
                                         currComp->sampleFactorY);
    }

    // We must divide the loop into two as otherwise we do not know how large an
    // MCU is, and thus cannot know how many times components occur.

    for (size_t i = 0; i < numComponents; ++i)
    {
        auto& scanComp = m_currScan.components[i];
        auto currComp = getComponentInfo(scanComp.componentSelector);
        if (!currComp)
        {
            throw std::runtime_error("Scan refers to undefined component.");
        }
        size_t xFactor = currComp->sampleFactorX;
        size_t yFactor = currComp->sampleFactorY;
        if (maxSampleFacX % xFactor || maxSampleFacY % yFactor)
        {
            throw std::runtime_error("Got a sample factor which the max factor "
                                     "wasn't evenly divisible by.");
        }

        scanComp.stretchFactorX = maxSampleFacX / xFactor;
        scanComp.stretchFactorY = maxSampleFacY / yFactor;
    }

    m_currScan.mcuWidth = 8 * maxSampleFacX;
    m_currScan.mcuHeight = 8 * maxSampleFacY;
    m_currScan.nextMCUXStart = 0;
    m_currScan.nextMCUYStart = m_currScanLineOffset;

    if (bytesLeft != 3) return false;
    bytesLeft -= 2; // We only increment the next two elements.
    m_currScan.firstDCT = *headerBegin++;
    m_currScan.lastDCT = *headerBegin++;
    m_currScan.approxBitHigh = *headerBegin >> 4;
    m_currScan.approxBitLow = *headerBegin & 0xf;

    headerBegin += bytesLeft;
    return true;
}

// Returns a pointer to the start of the next segment; i.e. to the 0xff byte; or
// if at the end, a pointer to the end. Thus, the returned pointer cannot
// necessarily be dereferenced safely.
const U8* JPEGDecoder::parseSegment(U8 marker, const U8* segmentBegin, const U8* imageEnd)
{
    switch (marker)
    {
    // TEM / Temporary marker for arithmetic coding.
    case 0x01:
        throw std::runtime_error("Leftover temporary arithmetic coding marker "
                                 "detected.");

    // SOF / Start of frame (Note: Holes in byte values are on purpose)
    case 0xc0: // Baseline DCT (Huffman)
        if (!parseSOFHeader(segmentBegin, imageEnd))
        {
            throw std::runtime_error("Failed to parse start of frame.");
        }
        break;
    case 0xc1: // Extended sequential DCT (Huffman)
    case 0xc2: // Progressive DCT (Huffman)
    case 0xc3: // Lossless (sequential, Huffman)
    case 0xc5: // Differential sequential DCT (Huffman)
    case 0xc6: // Differential progressive DCT (Huffman)
    case 0xc7: // Differential lossless (sequential)
    case 0xc9: // Extended sequential DCT (arithmetic coding)
    case 0xca: // Progressive DCT (arithmetic coding)
    case 0xcb: // Lossless (sequential, arithmetic coding)
    case 0xcd: // Differential sequential DCT (arithmetic coding)
    case 0xce: // Differential progressive DCT (arithmetic coding)
    case 0xcf: // Differential lossless (sequential, arithmetic coding)
        throw std::logic_error(std::string("Unuspported frame type (marker "
                                           "0xc")
                               + "0123456789abcdef"[marker&0xf]
                               + std::string(")."));

    // DHT / Define Huffman tables
    case 0xc4:
        if (!parseDHTHeader(segmentBegin, imageEnd))
        {
            throw std::runtime_error("Failed to parse Huffman tables.");
        }
        break;

    // DAC / Define arithmetic coding conditions.
    case 0xcc:
        throw std::logic_error("Arithmetic coding unsupported; cannot define "
                               "coding conditions.");

    // Restart markers
    case 0xd0: case 0xd1: case 0xd2: case 0xd3: case 0xd4: case 0xd5:
    case 0xd6: case 0xd7:
        handleRestartInterval(marker & 7);
        break;

    // SOI / Start of image
    case 0xd8:
        return segmentBegin; // No extra data; next marker right after.

    // EOI / End of image
    case 0xd9:
        m_eoi = true;
        return segmentBegin;

    // SOS / Start of scan
    case 0xda:
        if (!parseSOSHeader(segmentBegin, imageEnd))
        {
            throw std::runtime_error("Failed to parse SOS header.");
        }
        segmentBegin = parseScan(segmentBegin, imageEnd);
        break;

    // DQT / Define quantisation tables
    case 0xdb:
        if (!parseDQTHeader(segmentBegin, imageEnd))
        {
            throw std::runtime_error("Failed to parse quantisation tables.");
        }
        break;

    // DNL / Define number of lines:
    case 0xdc:
        throw std::logic_error("Defining number of lines not implemented.");

    // DRI / Define restart interval
    case 0xdd:
        throw std::logic_error("Defining restarts intervals unsupported.");

    // DHP / Define hierarchical progression
    case 0xde:
        throw std::logic_error("Hierarchical pictures not supported.");

    // EXP / Expand reference components
    case 0xdf:
        throw std::logic_error("Expansion of reference components unsupported.");

    // COM / Comment
    case 0xfe:
        // Just ignore it.
        break;

    default:
        // Application segment
        if ((marker & 0xe0) == 0xe0)
        {
            if (marker == 0xe0) // APP0
            {
                if (!parseAPP0Header(segmentBegin, imageEnd))
                {
                    throw std::logic_error("Failed to parse APP0 header.");
                }
            }
            else if (!gobbleSegment(segmentBegin, imageEnd))
            {
                throw std::runtime_error("Failed to skip segment (file too "
                                         "short?).");
            }
            break;
        }
        else if (0xf0 <= marker && marker <= 0xfd)
        {
            break; // Ignore JPEG extensions.
        }
        else
        {
            const char* magicToHex = "0123456789abcdef";
            std::string msg = "Unknown marker 0x";
            msg += magicToHex[marker >> 4];
            msg += magicToHex[marker & 0xf];
            msg += ".";
            throw std::runtime_error(msg);
        }
    }

    auto next = nextJPEGMarkerByte(segmentBegin, imageEnd);
    return next == imageEnd ? next : next-1;
}

Image JPEGDecoder::read(const U8* begin,
                        const U8* end,
                        JPEGDecodeOptions opts)
{
    m_decodeOpts = opts;
    while (!isAtEndOfImage())
    {
        auto next = nextJPEGMarkerByte(begin, end);
        if (next == end)
        {
            throw std::runtime_error("Got to end of JPEG without finding EOI.");
        }
        begin = parseSegment(*next, next+1, end);
    }

    return std::move(m_currImage);
}

} // end anonymous namespace

bool Image::readJPEG(std::istream& input)
{
    JPEGDecoder decoder;
    auto beginPos = input.tellg();
    input.seekg(0, std::ios::end);
    auto endPos = input.tellg();
    input.seekg(beginPos, std::ios::beg);
    if (endPos < beginPos) return false;
    if (beginPos == endPos) return false;
    auto byteSize = static_cast<size_t>(endPos-beginPos);
    std::unique_ptr<U8[]> data(new U8[byteSize]);
    input.read(reinterpret_cast<char*>(data.get()), byteSize);
    *this = decoder.read(data.get(), data.get() + byteSize);
    return true;
}

} // namespace kraken::image
