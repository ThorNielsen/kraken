#include <kraken/image/image.hpp>
#include <kraken/utility/checksum.hpp>
#include <kraken/utility/zlib.hpp>
#include <kraken/utility/memory.hpp>

#include <stdexcept>

#include <iostream>

namespace kraken
{

namespace image
{

namespace
{

bool readBytes(std::istream& from, U8* to, size_t bytes)
{
    from.read(reinterpret_cast<char*>(to), bytes);
    if ((size_t)from.gcount() != bytes) return false;
    return true;
}

bool writeBytes(const U8* from, size_t bytes, std::ostream& to)
{
    to.write(reinterpret_cast<const char*>(from), bytes);
    if (!to.good()) return false;
    return true;
}

U64 readIntBE(const U8* readFrom, U8 bytes)
{
    U64 val = 0;
    U64 shift = (bytes-1)<<3;
    while (bytes)
    {
        --bytes;
        val |= U64(*readFrom++) << shift;
        shift -= 8;
    }
    return val;
}

void writeIntBE(U64 val, U8 bytes, U8* writeTo)
{
    U8* writeAt = writeTo + bytes;
    while (writeAt != writeTo)
    {
        *--writeAt = val&0xff;
        val >>= 8;
    }
}

U32 assembleU32BE(U8 bytes[4])
{
    U32 res = bytes[0]; res <<= 8;
    res |= bytes[1]; res <<= 8;
    res |= bytes[2]; res <<= 8;
    res |= bytes[3];
    return res;
}

std::string toString(U8 bytes[4])
{
    return std::string(reinterpret_cast<const char*>(bytes), 4);
}

struct PNGChunk
{
    std::vector<U8> data;
    U32 checksum;
    U8 name[4];
};

U8 paeth(S16 a, S16 b, S16 c)
{
    S16 p = a+b-c;
    auto pa = std::abs(p-a);
    auto pb = std::abs(p-b);
    auto pc = std::abs(p-c);
    if (pa <= pb && pa <= pc) return (U8)a;
    if (pb <= pc) return (U8)b;
    return (U8)c;
}

bool readPNGChunk(std::istream& input, PNGChunk& out,
                  std::vector<U8> initial = {})
{
    U8 buf[4];
    if (!readBytes(input, buf, 4)) return false;
    U32 offset = initial.size();
    U32 chunkSize = assembleU32BE(buf);
    out.data = initial;
    out.data.resize(offset+chunkSize);
    if (!readBytes(input, out.name, 4)) return false;
    CRC32sum crcCalculator;
    crcCalculator.append(out.name, 4);
    if (!readBytes(input, out.data.data()+offset, chunkSize))
    {
        return false;
    }
    crcCalculator.append(out.data.data()+offset, chunkSize);
    if (!readBytes(input, buf, 4)) return false;
    out.checksum = assembleU32BE(buf);
    return out.checksum == crcCalculator.sum();
}

} // end anonymous namespace


bool Image::readPNG(std::istream& input)
{
    PNGChunk header;
    if (!readPNGChunk(input, header)) return false;
    if (toString(header.name) != "IHDR") return false;
    const U8* headerData = header.data.data();

    U32 newWidth = readIntBE(headerData, 4);
    U32 newHeight = readIntBE(headerData+4, 4);
    if (!newWidth || !newHeight) return false;
    U8 bitDepth = headerData[8];
    U8 pngColType = headerData[9];
    U8 compressionMethod = headerData[10];
    U8 filterMethod = headerData[11];
    U8 interlaceMethod = headerData[12];

    bool interlaced = interlaceMethod == 1;

    if (compressionMethod != 0) return false; // No other methods defined.
    if (filterMethod != 0) return false; //      ^

    ColourType newCT;
    newCT.bitDepth = bitDepth;
    switch (pngColType)
    {
    case 0: newCT.channels = 1; break;
    case 2: newCT.channels = 3; break;
    case 3: newCT.channels = 1; break;
    case 4: newCT.channels = 2; break;
    case 6: newCT.channels = 4; break;
    default:
        return false; // Invalid colour type.
    }

    if (!isColourTypeSupported(newCT)) return false;
    resize(newWidth, newHeight, newCT);

    U32 chWidth = width() - 1;
    U32 passWidths[7] =
    {
        (chWidth>>3)+1, (chWidth+4)>>3, (chWidth>>2)+1, (chWidth+2)>>2,
        (chWidth>>1)+1, width()>>1, width()
    };
    U32 chHeight = height() - 1;
    U32 passHeights[7] =
    {
        (chHeight>>3)+1, (chHeight>>3)+1, (chHeight+4)>>3, (chHeight>>2)+1,
        (chHeight+2)>>2, (chHeight>>1)+1, height()>>1
    };

    for (size_t i = 1; i < 7; ++i)
    {
        passHeights[i] *= passWidths[i] > 0;
        passHeights[i] += passHeights[i-1];
    }

    if (interlaced)
    {
        U64 pixelBits = newCT.bitDepth * newCT.channels;
        for (size_t i = 0; i < 7; ++i)
        {
            passWidths[i] = (pixelBits * passWidths[i] + 7) >> 3;
        }
    }
    else
    {
        passWidths[0] = rowBytes();
        passHeights[0] = height();
    }

    PNGChunk chunk;
    U32 pass = 0;
    std::vector<U8> prevRow(passWidths[pass]+1, 0);
    std::vector<U8> currRow(passWidths[pass]+1);
    U32 row = 0;
    Inflate inflator;
    inflator.setOutput(currRow.data(), currRow.size());
    bool imageDataRead = false;
    bool needsPalette = pngColType == 3;
    bool paletteRead = false;

    U8 chunkBitMask = 1 << 5;

    std::vector<U8> extraBytes;

    for (;;)
    {
        if (!readPNGChunk(input, chunk, extraBytes)) return false;
        extraBytes = {};
        auto name = toString(chunk.name);
        if (name == "IEND")
        {
            return imageDataRead && (!needsPalette || paletteRead);
        }
        else if (name == "IDAT")
        {
            inflator.setInput(chunk.data.data(), chunk.data.size());
            for (;;)
            {
                inflator.inflate();
                if (inflator.status() == Inflate::BadChecksum) return false;
                if (inflator.status() == Inflate::BadInput) return false;
                if (inflator.status() == Inflate::RequiresDictionary)
                {
                    return false; // This is not allowed by the PNG standard.
                }
                if (inflator.status() == Inflate::RequiresOutput)
                {
                    U32 subrow = row;
                    if (pass) subrow -= passHeights[pass-1];
                    if (!reconstructPNGRow(currRow, prevRow, row,
                                           pass, subrow, interlaced))
                    {
                        return false;
                    }
                    ++row;
                    std::swap(prevRow, currRow);
                    while (interlaced && pass < 6 && row == passHeights[pass])
                    {
                        ++pass;
                        prevRow.clear();
                        prevRow.resize(passWidths[pass]+1, 0);
                        currRow.resize(passWidths[pass]+1);
                    }
                    inflator.setOutput(currRow.data(), currRow.size());
                }
                if (inflator.status() == Inflate::RequiresInput)
                {
                    const U8* at = inflator.inputAt();
                    while (at != inflator.inputEnd())
                    {
                        extraBytes.push_back(*at++);
                    }
                    break;
                }
                if (inflator.status() == Inflate::Finished)
                {
                    if (inflator.outputAt() == inflator.outputEnd())
                    {
                        U32 subrow = row;
                        if (pass) subrow -= passHeights[pass-1];
                        if (!reconstructPNGRow(currRow, prevRow, row,
                                               pass, subrow, interlaced))
                        {
                            return false;
                        }
                    }
                    // All scanlines has been read, so we need only IEND now.
                    imageDataRead = true;
                    break;
                }
            }
        }
        else if (name == "PLTE")
        {
            if (!needsPalette) continue;
            if (chunk.data.size() % 3 != 0) return false;
            paletteRead = true;
            createPalette(chunk.data.size() / 3);
            setPaletteColourType(RGB8);
            for (size_t i = 0; i < paletteCount(); ++i)
            {
                Colour c = 0;
                c |= chunk.data[3*i  ];
                c <<= 8;
                c |= chunk.data[3*i+1];
                c <<= 8;
                c |= chunk.data[3*i+2];
                setPaletteElement(i, c);
            }
        }
        else
        {
            if ((name[0] & chunkBitMask) == 1)
            {
                // Unknown critical chunk.
                return false;
            }
            if ((name[2] & chunkBitMask) == 1)
            {
                // Disallowed setting of the reserved bit.
                return false;
            }
        }
    }
}

bool Image::reconstructPNGRow(std::vector<U8>& currRow,
                              const std::vector<U8>& prevRow,
                              U32 row,
                              U32 interlacePass,
                              U32 subrow,
                              bool interlaced)
{
    //if (interlaced) return false; // Unimplemented.

    if (!currRow.size()) return false;
    if (prevRow.size() != currRow.size()) return false;
    U8 filter = currRow[0];
    U8 prevs[8] = {0,0,0,0, 0,0,0,0};
    U8 abovePrevs[8] = {0,0,0,0, 0,0,0,0};
    U8 prevIdx = 0;
    U8 stride = (colourType().bitDepth * colourType().channels + 7) >> 3;
    U8 invStride = 8-stride;
    switch (filter)
    {
    case 0:
        break;
    case 1:
        for (size_t i = 1; i < currRow.size(); ++i)
        {
            prevs[prevIdx] = (currRow[i] += prevs[(prevIdx+invStride)&7]);
            prevIdx = (prevIdx + 1) & 7;
        }
        break;
    case 2:
        for (size_t i = 1; i < currRow.size(); ++i)
        {
            currRow[i] += prevRow[i];
        }
        break;
    case 3:
        for (size_t i = 1; i < currRow.size(); ++i)
        {
            U8 idx = (prevIdx+invStride)&7;
            currRow[i] += U8(((U16)(prevs[idx]) + (U16)(prevRow[i]))>>1);
            prevs[prevIdx] = currRow[i];
            prevIdx = (prevIdx + 1) & 7;
        }
        break;
    case 4:
        for (size_t i = 1; i < currRow.size(); ++i)
        {
            U8 idx = (prevIdx+invStride)&7;
            U8 prev = prevs[idx];
            U8 abovePrev = abovePrevs[idx];
            prev = (currRow[i] += paeth(prev, prevRow[i], abovePrev));
            prevs[prevIdx] = prev;
            abovePrevs[prevIdx] = prevRow[i];
            prevIdx = (prevIdx + 1) & 7;
        }
        break;
    default:
        return false;
    }

    if (interlaced)
    {
        U32 hstride[7] = {8,8,4,4,2,2,1};
        U32 hoffset[7] = {0,4,0,2,0,1,0};
        U32 vstride[7] = {8,8,8,4,4,2,2};
        U32 voffset[7] = {0,0,4,0,2,0,1};
        U32 writeX = hoffset[interlacePass];
        U32 writeY = voffset[interlacePass]+subrow*vstride[interlacePass];
        U32 chWidth = width() - 1;
        U32 passWidths[7] =
        {
            (chWidth>>3)+1, (chWidth+4)>>3, (chWidth>>2)+1, (chWidth+2)>>2,
            (chWidth>>1)+1, width()>>1, width()
        };
        const U8* readAt = currRow.data()+1;
        if (m_bpp >= 8)
        {
            auto bytes = m_bpp >> 3;
            for (U32 x = 0; x < passWidths[interlacePass]; ++x)
            {
                U64 val = 0;
                for (size_t i = 0; i < bytes; ++i)
                {
                    val <<= 8;
                    val |= *readAt++;
                }
                setPixel(writeX, writeY, val);
                writeX += hstride[interlacePass];
            }
        }
        else
        {
            U8 mask = (1 << m_bpp) - 1;
            for (U32 x = 0; x < passWidths[interlacePass]; ++x)
            {
                U64 bitColOffset = m_bpp * x;
                U8 val = readAt[bitColOffset >> 3];
                setPixel(writeX, writeY,
                         (val >> (8-m_bpp-(bitColOffset&7)))&mask);
                writeX += hstride[interlacePass];
            }
        }
    }
    else
    {
        size_t offset = row*rowBytes();
        for (size_t i = 1; i < currRow.size(); ++i)
        {
            m_data[offset+i-1] = currRow[i];
        }
    }

    return true;
}

bool Image::writePNG(std::ostream& output) const
{
    U8 pngColType = 0;
    switch (colourType().channels)
    {
    case 1:
        pngColType = 0;
        if (colourType().bitDepth != 1
            && colourType().bitDepth != 2
            && colourType().bitDepth != 4
            && colourType().bitDepth != 8
            && colourType().bitDepth != 16)
        {
            return false;
        }
        break;
    case 2:
        pngColType = 4;
        if (colourType().bitDepth != 8 && colourType().bitDepth != 16)
        {
            return false;
        }
        break;
    case 3:
        pngColType = 2;
        if (colourType().bitDepth != 8 && colourType().bitDepth != 16)
        {
            return false;
        }
        break;
    case 4:
        pngColType = 6;
        if (colourType().bitDepth != 8 && colourType().bitDepth != 16)
        {
            return false;
        }
        break;
    default:
        throw std::logic_error("Bad number of channels.");
    }
    U8 fileheader[] = { 0x89, 'P', 'N', 'G', 0x0d, 0x0a, 0x1a, 0x0a };
    if (!writeBytes(fileheader, 8, output)) return false;
    U8 ihdr[13];
    writeIntBE(width(), 4, ihdr);
    writeIntBE(height(), 4, ihdr+4);
    writeIntBE(colourType().bitDepth, 1, ihdr+8);
    if (usesPalette())
    {
        pngColType = 3;
        if (colourType().bitDepth != 1
            && colourType().bitDepth != 2
            && colourType().bitDepth != 4
            && colourType().bitDepth != 8)
        {
            return false;
        }
        // Test for unsupported configurations.
        if (paletteColourType() != RGB8) return false;
        if (colourType().bitDepth > 8) return false;
    }
    writeIntBE(pngColType, 1, ihdr+9);
    writeIntBE(0, 2, ihdr+10); // Compression + filter method.
    writeIntBE(0, 1, ihdr+12); // Interlace method is straight laced.
    if (!writePNGChunk(output, "IHDR", ihdr, ihdr+13)) return false;

    if (usesPalette())
    {
        // Yes, this is tested above. But if the code for some reason change and
        // some fool (probably myself) don't look down here, then it is good to
        // check the assumption once more.
        if (paletteColourType() != RGB8) return false;
        std::vector<U8> palette(m_palette.size()*3, 0);
        for (U32 i = 0; i < m_palette.size(); ++i)
        {
            auto col = iunpack(m_palette[i], RGB8);
            palette[3*i] = col.r;
            palette[3*i+1] = col.g;
            palette[3*i+2] = col.b;
        }
        if (!writePNGChunk(output, "PLTE", palette.data(),
                           palette.data() + palette.size()))
        {
            return false;
        }
    }

    Deflate deflate;

    // The in-memory pixel format corresponds exactly to the PNG format for all
    // compatible pixel formats.
    U64 scanlinesize = rowBytes() + 1;

    // If the image isn't too large then we just fit the scan line buffer in
    // memory.
    U64 slbufsize = scanlinesize*height();
    if (slbufsize > 1024*1024) slbufsize = 1024*1024;
    slbufsize -= slbufsize % scanlinesize;

    // We want to have at least two scanlines in memory such that the preceding
    // line is always accessible (for filtering purposes).
    if (slbufsize < 2*scanlinesize) slbufsize = 2*scanlinesize;

    // Note: It is important that the buffer (or at least the final line) is
    // zero-initialised, since the "virtual" scanline above the first real scan-
    // line is defined as all zeroes.
    std::vector<U8> scanlinebuf(slbufsize, 0);

    U8* currLine = scanlinebuf.data();
    const U8* prevLine = currLine + slbufsize - scanlinesize;

    U32 currRow = 0;

    std::vector<U8> outputbuf(slbufsize);
    deflate.setOutput(outputbuf.data(), outputbuf.size());

    U64 bufRows = slbufsize / scanlinesize;

    const U8* slbufbegin = scanlinebuf.data();
    const U8* slbufend = slbufbegin + scanlinebuf.size();
    const U8* slbuffinalend = slbufbegin + (height() % bufRows) * scanlinesize;

    while (currRow < height())
    {
        writePNGScanline(currLine, prevLine, currRow);
        prevLine = currLine;
        currLine += scanlinesize;
        if (currLine == scanlinebuf.data() + slbufsize)
        {
            currLine = scanlinebuf.data();
            deflate.setInput(slbufbegin, slbufend);
            while (!deflate.deflate(currRow == height()))
            {
                if (deflate.status() & Deflate::RequiresOutput)
                {
                    if (!writePNGChunk(output, "IDAT", outputbuf.data(),
                                       deflate.outputAt()))
                    {
                        return false;
                    }
                    deflate.setOutput(outputbuf.data(), outputbuf.size());
                }
                if (deflate.status() & Deflate::RequiresInput)
                {
                    break; // All input consumed.
                }
            }
            if (deflate.inputAt() != deflate.inputEnd())
            {
                throw std::logic_error("Deflate did not consume all the "
                                       "required input.");
            }
        }
        ++currRow;
    }
    if (slbuffinalend != slbufbegin)
    {
        deflate.setInput(slbufbegin, slbuffinalend);
    }

    while (deflate.status() != Deflate::Finished)
    {
        deflate.deflate(true);
        if (deflate.status() & Deflate::RequiresInput)
        {
            throw std::logic_error("Deflate has been told it has all input, "
                                   "yet it still wants more.");
        }
        if (deflate.status() & Deflate::RequiresOutput)
        {
            if (!writePNGChunk(output, "IDAT", outputbuf.data(),
                               deflate.outputAt()))
            {
                return false;
            }
            deflate.setOutput(outputbuf.data(), outputbuf.size());
        }
    }
    if (deflate.outputAt() != outputbuf.data())
    {
        if (!writePNGChunk(output, "IDAT", outputbuf.data(),
                           deflate.outputAt()))
        {
            return false;
        }
    }
    writePNGChunk(output, "IEND", nullptr, nullptr);
    return true;
}

void Image::writePNGScanline(U8* currLine, const U8* prevLine,
                             U32 row, U8 filterMethod) const
{
    if (filterMethod > 4)
    {
        U64 bestScore = 0;
        U64 bestMethod = 0;
        for (U8 method = 0; method < 5; ++method)
        {
            writePNGScanline(currLine, prevLine, row, method);
            U64 score = 0;
            for (U32 i = 0; i < rowBytes(); ++i)
            {
                score += 255 - currLine[1+i];
            }
            if (score > bestScore)
            {
                bestScore = score;
                bestMethod = method;
            }
        }
        // If the best method was the last one (Paeth), then that is already
        // stored in the buffer pointed to by currLine, and we can therefore
        // safely return that.
        if (bestMethod == 4) return;
        // Otherwise, we need to recompute it. This is a minor trade-off to
        // avoid storing another (the current best) filtered scanline in memory
        // somewhere -- even if we did that, we would in some cases have to
        // copy from the auxiliary scanline to this one if the auxiliary one
        // contained the best filtered scanline. As the filters are all quite
        // simple, it is likely that this recomputation does not have any effect
        // except lower memory usage slightly.
        filterMethod = bestMethod;
    }

    const U8* orig = m_data.data() + rowBytes() * row;
    const U8* origA = orig;
    // If row > 0 then we have a row above, while if row = 0 then that is
    // assumed to be zero, but we know that prevLine contains that many zeroes.
    const U8* origB = row ? orig - rowBytes() : prevLine;
    const U8* origC = origB;

    const U8* readTo = orig+rowBytes();

    U8* writeAt = currLine;
    *writeAt++ = filterMethod;

    U8 stride = pixelBytes();
    // We write the first 'stride' bytes manually so that the code below does
    // not constantly have to check if it is at the beginning of a line and
    // therefore cannot refer to origA or origC.
    if (filterMethod == 1 || filterMethod == 3 || filterMethod == 4)
    {
        for (size_t i = 0; i < stride; ++i)
        {
            if (filterMethod == 1)
            {
                *writeAt++ = *orig++;
            }
            if (filterMethod == 3)
            {
                *writeAt++ = *orig++ - (*origB++>>1);
            }
            if (filterMethod == 4)
            {
                *writeAt++ = *orig++ - paeth(0, *origB++, 0);
            }
        }
    }

    if (filterMethod == 0)
    {
        while (orig != readTo)
        {
            *writeAt++ = *orig++;
        }
    }
    if (filterMethod == 1)
    {
        while (orig != readTo)
        {
            *writeAt++ = *orig++ - *origA++;
        }
    }
    if (filterMethod == 2)
    {
        while (orig != readTo)
        {
            *writeAt++ = *orig++ - *origB++;
        }
    }
    if (filterMethod == 3)
    {
        while (orig != readTo)
        {
            *writeAt++ = *orig++ -
                         U8((U16(*origA++) + U16(*origB++))>>1);
        }
    }
    if (filterMethod == 4)
    {
        while (orig != readTo)
        {
            *writeAt++ = *orig++ - paeth(*origA++, *origB++, *origC++);
        }
    }
}

bool Image::writePNGChunk(std::ostream& output, const char name[4],
                          const U8* begin, const U8* end) const
{
    CRC32sum checksum;
    auto dataLen = static_cast<U32>(end-begin);
    U8 buf[8];
    writeIntBE(dataLen, 4, buf);
    for (size_t i = 0; i < 4; ++i)
    {
        buf[4+i] = name[i];
    }
    if (!writeBytes(buf, 8, output)) return false;
    if (!writeBytes(begin, dataLen, output)) return false;
    checksum.append(buf+4, 4);
    checksum.append(begin, end);
    writeIntBE(checksum, 4, buf);
    if (!writeBytes(buf, 4, output)) return false;
    return true;
}

} // namespace image

} // namespace kraken
