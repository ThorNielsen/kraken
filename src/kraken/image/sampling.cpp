#include <kraken/image/sampling.hpp>

namespace kraken::image
{

namespace
{

template <typename ComponentType>
void upsampleInplaceNearest(Image& img,
                            size_t component,
                            size_t stretchX,
                            size_t stretchY)
{
    size_t xSamples = img.width() / stretchX;
    size_t ySamples = img.height() / stretchY;

    // We divide by the remainder of the stretching factor so as to be able to
    // take a fast path for the vast majority of samples, and special case the
    // rest.
    auto extraXSamples = img.width() % stretchX;
    auto extraYSamples = img.height() % stretchY;

    if ((!xSamples && !extraXSamples)
        || (!ySamples && !extraYSamples))
    {
        return;
    }

    auto* const base = reinterpret_cast<ComponentType*>(img.data()+component);
    auto* writeDst = base;

    size_t channelCount = img.colourType().channels;
    // Note: The stride is expressed in sizeof(ComponentType) units!
    // So it is correct not to multiply by sizeof(ComponentType).
    size_t rowStride = channelCount * img.width();

    // This is a slightly complex loop which is split into two, but the idea is
    // to avoid having to do extra bounds checking within the inner loop. This
    // is necessary with a naïve loop if the image dimensions are not multiples
    // of their respective stretch factors -- to ameliorate this, we can either
    // check if we are at the last sample section in every iteration of the loop
    // OR we can just duplicate the very short inner body.
    for (size_t y = 0; y <= ySamples; ++y)
    {
        auto yRepeat = y == ySamples ? extraYSamples : stretchY;
        for (size_t yRep = 0; yRep < yRepeat; ++yRep)
        {
            // Note: We set the source *here*, as each repeat should not change
            // the line we read from.
            const auto* src = base+y*rowStride*stretchY;

            for (size_t x = 0; x < xSamples; ++x)
            {
                for (size_t xRep = 0; xRep < stretchX; ++xRep)
                {
                    *writeDst = *src;
                    writeDst += channelCount;
                }
                src += channelCount * stretchX;
            }
            for (size_t xr = 0; xr < extraXSamples; ++xr)
            {
                *writeDst = *src;
                writeDst += channelCount;
            }
        }
    }
}

template <typename ComponentType>
void upsampleInplaceLinear(Image& img,
                           size_t component,
                           size_t stretchX,
                           size_t stretchY)
{
    size_t xSamples = (img.width() + stretchX - 1) / stretchX;
    size_t ySamples = (img.height() + stretchY - 1) / stretchY;

    auto extraYSamples = img.height() % stretchY;

    if (!xSamples || !ySamples)
    {
        return;
    }

    auto xHalf = stretchX >> 1;

    auto* const base = reinterpret_cast<ComponentType*>(img.data()+component);
    auto* writeDst = base;

    size_t channelCount = img.colourType().channels;
    size_t rowStride = channelCount * img.width();
    size_t xScaleOffset = 2 - (stretchX&1);
    size_t yScaleOffset = 2 - (stretchY&1);

    auto xsoHalf = xScaleOffset >> 1;
    auto ysoHalf = yScaleOffset >> 1;

    size_t fullXSamples = xSamples;
    if (fullXSamples * stretchX + xHalf >= img.width())
    {
        --fullXSamples;
    }
    size_t extraXSamples = img.width() - fullXSamples * stretchX - xHalf;
    size_t lastY = extraYSamples ? extraYSamples : stretchY;

    size_t currYInner = stretchY - stretchY / 2;

    for (size_t y = 0; y < ySamples; ++y)
    {
        // Note: We set the source *here*, as each repeat should not change
        // the line we read from.
        auto yRepeat = y+1 == ySamples ? lastY : stretchY;
        for (size_t yRep = 0; yRep < yRepeat; ++yRep)
        {
            size_t yCoord = y*stretchY + yRep;
            auto isFirstStretch = yCoord <= stretchY / 2;
            // Note that we consider the image as starting from the top-left,
            // and proceeding downwards and rightwards.
            auto srcYUpper = isFirstStretch
                           ? 0
                           : ((yCoord - stretchY / 2) / stretchY)*stretchY;
            auto srcYLower = srcYUpper + stretchY < img.height()
                           ? srcYUpper + stretchY
                           : srcYUpper;

            const auto* upperSrc = base+srcYUpper*rowStride;
            const auto* lowerSrc = base+srcYLower*rowStride;

            auto upperSubFrom = yScaleOffset * stretchY - ysoHalf;

            size_t upperWeight = yScaleOffset * (stretchY - yRep) - ysoHalf;
            size_t lowerWeight = yScaleOffset * yRep + ysoHalf;

            if (isFirstStretch)
            {
                upperWeight = yScaleOffset * stretchY;
                lowerWeight = 0;
            }
            else
            {
                upperWeight = upperSubFrom - yScaleOffset * currYInner;
                lowerWeight = yScaleOffset * currYInner + ysoHalf;
            }
            currYInner = (currYInner+1) % stretchY;

            size_t ulDenom = upperWeight + lowerWeight;

            auto leftSubFrom = xScaleOffset * stretchX - xsoHalf;
            size_t lrDenom = xScaleOffset * stretchX;
            lrDenom *= ulDenom;

            // [UL][LR] = [upper/lower][left/right]; the larger size is to avoid
            // overflows.
            U64 ulSample = *upperSrc;
            U64 urSample = ulSample;
            U64 llSample = *lowerSrc;
            U64 lrSample = llSample;

            for (size_t x = 0; x < xHalf; ++x)
            {
                auto value = ulSample * upperWeight
                           + llSample * lowerWeight;
                *writeDst = value / ulDenom;
                writeDst += channelCount;
            }
            for (size_t x = 0; x < fullXSamples; ++x)
            {
                ulSample = urSample;
                llSample = lrSample;
                urSample = *(upperSrc += channelCount * stretchX);
                lrSample = *(lowerSrc += channelCount * stretchX);
                auto leftVal = ulSample * upperWeight
                             + llSample * lowerWeight;
                auto rightVal = urSample * upperWeight
                              + lrSample * lowerWeight;
                for (size_t xRep = 0; xRep < stretchX; ++xRep)
                {
                    size_t leftWeight = leftSubFrom - xScaleOffset * xRep;
                    size_t rightWeight = xScaleOffset * xRep + xsoHalf;
                    auto value = leftWeight * leftVal + rightWeight * rightVal;
                    *writeDst = value / lrDenom;
                    writeDst += channelCount;
                }
            }
            ulSample = urSample;
            llSample = lrSample;
            for (size_t x = 0; x < extraXSamples; ++x)
            {
                auto value = ulSample * upperWeight
                           + llSample * lowerWeight;
                *writeDst = value / ulDenom;
                writeDst += channelCount;
            }
        }
    }
}

template <typename ComponentType>
void upsampleInplace(Image& img,
                     UpsamplingMethod method,
                     size_t component,
                     size_t stretchX,
                     size_t stretchY)
{
    if (method == UpsamplingMethod::NearestNeighbour)
    {
        upsampleInplaceNearest<ComponentType>(img,
                                              component,
                                              stretchX,
                                              stretchY);
    }
    else if (method == UpsamplingMethod::Linear)
    {
        upsampleInplaceLinear<ComponentType>(img,
                                             component,
                                             stretchX,
                                             stretchY);
    }
    else
    {
        throw std::logic_error("Upsampling method not implemented.");
    }
}

void upsampleInplace(Image& img,
                     UpsamplingMethod method,
                     size_t component,
                     size_t stretchX,
                     size_t stretchY)
{
    if (img.colourType().bitDepth & 7)
    {
        throw std::domain_error("Bad bit depth not divisible by 8");
    }
    size_t byteSize = img.colourType().bitDepth >> 3;

    if (byteSize == 1)
    {
        return upsampleInplace<U8>(img, method, component, stretchX, stretchY);
    }
    else if (byteSize == 2)
    {
        return upsampleInplace<U16>(img, method, component, stretchX, stretchY);
    }
    else
    {
        throw std::domain_error("Bad image bit depth.");
    }
}

} // end anonymous namespace

void upsampleComponentInplace(Image& img,
                              UpsamplingMethod method,
                              size_t component,
                              size_t stretchFactorX,
                              size_t stretchFactorY)
{
    auto ct = img.colourType();
    if (component >= ct.channels)
    {
        throw std::domain_error("Invalid component in upsampling.");
    }
    if (ct.bitDepth & 7)
    {
        auto toConvertTo = ct;
        toConvertTo.bitDepth += 8-(ct.bitDepth);
        auto newImg = img.convert(toConvertTo);
        upsampleComponentInplace(newImg,
                                 method,
                                 component,
                                 stretchFactorX,
                                 stretchFactorY);

        img.fill([&newImg](U32 x, U32 y)
        {
            return uunpack(newImg.getPixel(x, y), newImg.colourType());
        });

        return;
    }
    else
    {
        upsampleInplace(img, method, component, stretchFactorX, stretchFactorY);
    }
}

} // namespace kraken::image
