#include "kraken/io/data/mesh.hpp"

#include <kraken/utility/range.hpp>

namespace kraken::io
{

// WARNING: Returns nullptr if there is no such name.
[[nodiscard]] const char* getCanonicalName(VertexAttributeUsageType usageType) noexcept
{
    switch (usageType)
    {
    case VertexAttributeUsageType::Position: return "position";
    case VertexAttributeUsageType::Normal: return "normal";
    case VertexAttributeUsageType::Tangent: return "tangent";
    case VertexAttributeUsageType::Texcoord: return "texcoord";
    case VertexAttributeUsageType::Colour: return "colour";
    case VertexAttributeUsageType::Joint: return "joint";
    case VertexAttributeUsageType::Weight: return "weight";

    case VertexAttributeUsageType::KnownTypeCount:
    case VertexAttributeUsageType::ExtendedBit:
    case VertexAttributeUsageType::Unknown:
    case VertexAttributeUsageType::Invalid:
    default:
        return nullptr;
    }
}

VertexAttributeUsageType ExtendedAttributeNames::makeExtendedUsage(std::string_view name)
{
    auto it = m_indexToName.find(name);

    if (it == m_indexToName.end())
    {
        it = m_indexToName.emplace(name, m_names.size()).first;
        m_names.emplace_back(name);
    }

    return makeExtendedAttributeType(it->second);
}

std::optional<std::string_view>
ExtendedAttributeNames::ExtendedAttributeNames::extendedName(VertexAttributeUsageType attrType) const noexcept
{
    auto ei = getExtendedIndex(attrType);
    if (ei >= m_names.size()) return std::nullopt;
    return m_names[ei];
}

} // namespace kraken::io
