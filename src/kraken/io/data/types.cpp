#include "kraken/io/data/types.hpp"

namespace kraken::io
{

[[nodiscard]] std::size_t byteSize(PrimitiveDataType pt) noexcept
{
    switch (pt)
    {
    case PrimitiveDataType::Byte:
    case PrimitiveDataType::UByte:
        return 1;
    case PrimitiveDataType::Short:
    case PrimitiveDataType::UShort:
        return 2;
    case PrimitiveDataType::Int:
    case PrimitiveDataType::UInt:
        return 4;
    case PrimitiveDataType::Long:
    case PrimitiveDataType::ULong:
        return 8;
    case PrimitiveDataType::Float:
        return 4;
    case PrimitiveDataType::Double:
        return 8;

    default:
        return 0;
    }
}

[[nodiscard]] std::size_t requiredAlignment(PrimitiveDataType pt) noexcept
{
    switch (pt)
    {
    case PrimitiveDataType::Byte:   return alignof(S8);
    case PrimitiveDataType::UByte:  return alignof(U8);
    case PrimitiveDataType::Short:  return alignof(S16);
    case PrimitiveDataType::UShort: return alignof(U16);
    case PrimitiveDataType::Int:    return alignof(S32);
    case PrimitiveDataType::UInt:   return alignof(U32);
    case PrimitiveDataType::Long:   return alignof(S64);
    case PrimitiveDataType::ULong:  return alignof(U64);
    case PrimitiveDataType::Float:  return alignof(F32);
    case PrimitiveDataType::Double: return alignof(F64);

    default:
        return 64; // If it is another type, we can technically be wrong. But on
                   // all reasonable architectures (including GPUs), primitive
                   // types are usually at most 64 bytes large (think mat4), and
                   // higher alignment requirements are probably not going to be
                   // relevant in the immediate future.
    }
}

[[nodiscard]] bool isInteger(PrimitiveDataType pt) noexcept
{
    return static_cast<U32>(pt) < static_cast<U32>(PrimitiveDataType::Float);
}

[[nodiscard]] bool isFloatingPoint(PrimitiveDataType pt) noexcept
{
    return !isInteger(pt);
}

} // namespace kraken::io
