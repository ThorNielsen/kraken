#include "kraken/io/datasource.hpp"

#include <istream>

namespace kraken::io
{

DataSource::DataSource(const void* begin, size_t byteCount)
{
    init(begin, byteCount);
}

DataSource::DataSource(std::istream& input)
{
    init(input);
}

void DataSource::init(const void* begin, size_t byteCount)
{
    clear();

    m_data = std::make_unique<std::byte[]>(byteCount);
    std::memcpy(m_data.get(), begin, byteCount);
    m_maxSize = byteCount;
}

void DataSource::init(std::istream& input)
{
    clear();

    input.seekg(0, std::ios::end);
    auto endPos = input.tellg();
    input.seekg(0, std::ios::beg);
    auto beginPos = input.tellg();
    auto expectedSize = endPos - beginPos;

    m_data = std::make_unique<std::byte[]>(expectedSize);
    input.read(reinterpret_cast<char*>(m_data.get()), expectedSize);

    m_maxSize = static_cast<size_t>(input.gcount());
}

void DataSource::clear()
{
    m_data = nullptr;
    m_position = 0;
    m_maxSize = 0;
}

} // namespace kraken::io
