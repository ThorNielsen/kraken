#include "kraken/io/formats/ply.hpp"

namespace kraken::io::formats::ply
{

ElementWithCount* Header::getElementByName(std::string_view name)
{
    return const_cast<ElementWithCount*>(const_cast<const Header*>(this)->getElementByName(name));
}

const ElementWithCount* Header::getElementByName(std::string_view name) const
{
    for (const auto& elem : elements)
    {
        if (elem.element.name == name) return &elem;
    }
    return nullptr;
}

const char* getVertexElementName() noexcept
{
    return "vertex";
}

const char* getFaceElementName() noexcept
{
    return "face";
}

} // namespace kraken::io::formats::ply
