#include "kraken/io/meshoutput.hpp"

#include <stdexcept>

namespace kraken::io
{

#if 0

void MeshOutputWriter::setVertexLayout(const std::map<VertexComponent, DataRange>& spec)
{
    m_vertexFormatSpec = spec;
    m_vertexSize = 0;
    for (const auto& [comp, range] : spec)
    {
        m_vertexSize = std::max(*m_vertexSize, range.rangeEnd());
    }
    setVertexLayoutImpl(spec);
}

void MeshOutputWriter::setVertexLayoutImpl(const std::map<VertexComponent, DataRange>&)
{
    // By default there's nothing to do.
}

void MeshOutputWriter::reserveVertices(std::size_t, bool)
{
    // Nothing to do by default; we don't require an implementation.
}

namespace
{

graphics::VertexDataType getGraphicsDataType(PrimitiveDataType pdt)
{
    using From = PrimitiveDataType;
    using To = graphics::VertexDataType;
    switch (pdt)
    {
    case From::Byte: return To::Byte;
    case From::UByte: return To::UByte;
    case From::Short: return To::Short;
    case From::UShort: return To::UShort;
    case From::Int: return To::Int;
    case From::UInt: return To::UInt;
    case From::Float: return To::Float;
    case From::Double:
        throw std::logic_error("Doubles not supported as graphics type");
    case From::Long: case From::ULong: default:
        throw std::logic_error("Unuspported type.");
    }
}

} // end anonymous namespace

SimpleMeshOutputWriter::SimpleMeshOutputWriter
(
    ExternalMemoryAllocation::MemoryAllocationFunction vertexAlloc,
    ExternalMemoryAllocation::MemoryAllocationFunction indexAlloc
)
    : m_vertexAlloc{std::move(vertexAlloc)}
    , m_indexAlloc{std::move(indexAlloc)}
{}

graphics::VertexType SimpleMeshOutputWriter::vertexType() const
{
    std::map<DataRange, VertexComponent> orderedComponents;
    for (const auto& [component, range] : getFormatSpec())
    {
        orderedComponents.emplace(range, component);
    }

    graphics::VertexType vtypeOut;
    std::size_t lastOffset = 0;
    for (const auto& [range, component] : orderedComponents)
    {
        if (range.rangeBegin() != lastOffset)
        {
            vtypeOut.emplace_back("padding",
                                  graphics::VertexDataType::Padding,
                                  lastOffset - range.rangeBegin());
        }
        lastOffset = range.rangeEnd();
        auto gdt = getGraphicsDataType(component.dataType);
        if (!isKnownType(component.usageType) && isInteger(component.dataType))
        {
            // If we do not know the type and it is not a float, we should not
            // normalise it.
            gdt |= graphics::VertexDataType::Integer;
        }
        vtypeOut.emplace_back(component.name,
                              gdt,
                              range.length);
    }

    return vtypeOut;
}

std::optional<U32> SimpleMeshOutputWriter::indexByteWidth() const
{
    if (!m_indexByteWidth.has_value())
    {
        throw std::logic_error("No index byte width defined!");
    }
    if (!*m_indexByteWidth) return std::nullopt;
    return m_indexByteWidth;
}

MeshOutputWriterStatus
SimpleMeshOutputWriter::writeVertices(const void* source,
                                      std::size_t count,
                                      std::size_t srcStride)
{
    // As a rule of thumb, an alignment of 16 is usually enough for all usages.
    // Maybe it should be relaxed and/or computed from the vertex type, but that
    // can be done at another point...
    constexpr std::size_t requiredAlignment = 16;
    auto currVertexSize = vertexSize();
    if (!currVertexSize.has_value())
    {
        return MeshOutputWriterStatus::NoVertexSizeDefined;
    }

    const auto dstStride = currVertexSize.value();
    if (srcStride < dstStride)
    {
        return MeshOutputWriterStatus::TooSmallVertexStride;
    }

    const auto requiredSpace = currVertexSize.value() * count;
    const bool canMemcpy = srcStride == dstStride;

    if (canMemcpy)
    {
        m_vertexAlloc.memcpy_back(source, requiredSpace, requiredAlignment);
    }
    else for (std::size_t k = 0; k < count; ++k)
    {
        m_vertexAlloc.memcpy_back(source, dstStride, requiredAlignment);
        source = pointeradd(source, srcStride);
    }
    return MeshOutputWriterStatus::Success;
}

template <typename Element>
[[nodiscard]] Element readBinary(const void*& source) noexcept
{
    auto ptr = static_cast<const Element*>(source);
    source = pointeradd(source, sizeof(Element));
    return *ptr;
}

template <typename Element>
[[nodiscard]] std::optional<Element> readBinary(const void*& source, const void* end) noexcept
{
    if (byteDistance(source, end) < sizeof(Element)) return std::nullopt;
    return read<Element>(source);
}

template <typename Element>
[[nodiscard]] bool readBinary(Element& dst, const void*& source, const void* end) noexcept
{
    if (byteDistance(source, end) < sizeof(Element)) return false;
    dst = read<Element>(source);
    return true;
}

template <typename IndexType>
[[nodiscard]]
MeshOutputWriterStatus triangulateByFan(const IndexType* sourceIndices,
                                        std::size_t indexCount,
                                        ExternalMemoryAllocation& indexWriter)
{
    // If there are no triangles, we simply just skip it.
    if (indexCount < 3) return MeshOutputWriterStatus::Success;
    auto bytesToWrite = (indexCount - 2)*3;
    if (!indexWriter.reserve(bytesToWrite, alignof(IndexType)))
    {
        return MeshOutputWriterStatus::MemoryAllocationFailed;
    }
    auto dst = static_cast<IndexType*>(indexWriter.currDataPos());
    auto orig = sourceIndices[0];
    auto prev = sourceIndices[1];
    for (std::size_t k = 2; k < indexCount; ++k)
    {
        *dst++ = orig;
        *dst++ = prev;
        *dst++ = (prev = sourceIndices[k]);
    }
    indexWriter.addUsed(bytesToWrite);
    return MeshOutputWriterStatus::Success;
}

MeshOutputWriterStatus
SimpleMeshOutputWriter::writePrimitives(const void* sourceBegin,
                                        const void* sourceEnd,
                                        MeshPrimitiveType primitiveType,
                                        PrimitiveDataType indexType,
                                        std::optional<PrimitiveDataType> listIndexType)
{
    if (!vertexSize().has_value())
    {
        return MeshOutputWriterStatus::NoVertexSizeDefined;
    }


    const auto requiredAlignment = byteSize(indexType);
    const auto indexWidth = byteSize(indexType);

    if (indexWidth != 1 && indexWidth != 2 && indexWidth != 4)
    {
        return MeshOutputWriterStatus::InvalidListIndexType;
    }

    if (!listIndexType.has_value())
    {
        return m_indexAlloc.memcpy_back(sourceBegin,
                                        byteDistance(sourceBegin, sourceEnd),
                                        requiredAlignment)
            ? MeshOutputWriterStatus::Success
            : MeshOutputWriterStatus::MemoryAllocationFailed
            ;
    }

    auto listIndexTypeBytes = byteSize(*listIndexType);

    if (listIndexTypeBytes != 1
        && listIndexTypeBytes != 2
        && listIndexTypeBytes != 4)
    {
        return MeshOutputWriterStatus::InvalidListIndexType;
    }
    switch (primitiveType)
    {
    case MeshPrimitiveType::Points:
    case MeshPrimitiveType::LineSegments:
    case MeshPrimitiveType::Triangles:
    default:
        return MeshOutputWriterStatus::BadPrimitiveType;
    case MeshPrimitiveType::LineStrips:
    case MeshPrimitiveType::TriangleStrips:
    case MeshPrimitiveType::TriangleFans:
        throw std::logic_error("Unimplemented primitive type (restarts).");
    case MeshPrimitiveType::Polygons:
        break; // All good.
    }

    if (primitiveType != MeshPrimitiveType::Polygons)
    {
        throw std::logic_error("Unimplemented primitive type.");
    }

    while (byteDistance(sourceBegin, sourceEnd) >= listIndexTypeBytes)
    {
        std::size_t indexCount = 0;
        switch (listIndexTypeBytes)
        {
        default: // Checked before, so we just fall through.
        case 1: indexCount = readBinary<U8>(sourceBegin); break;
        case 2: indexCount = readBinary<U16>(sourceBegin); break;
        case 4: indexCount = readBinary<U32>(sourceBegin); break;
        }

        const auto inputByteCount = indexCount*indexWidth;
        if (byteDistance(sourceBegin, sourceEnd) < inputByteCount)
        {
            return MeshOutputWriterStatus::TooLittleInputData;
        }
        auto triResult = MeshOutputWriterStatus::UnknownError;
        switch (indexWidth)
        {
        case 1:
            triResult = triangulateByFan(static_cast<const U8*>(sourceBegin),
                                         indexCount,
                                         m_indexAlloc);
            break;
        case 2:
            triResult = triangulateByFan(static_cast<const U16*>(sourceBegin),
                                         indexCount,
                                         m_indexAlloc);
            break;
        case 4:
            triResult = triangulateByFan(static_cast<const U32*>(sourceBegin),
                                         indexCount,
                                         m_indexAlloc);
            break;
        default:
            ;
        }
        if (triResult != MeshOutputWriterStatus::Success) return triResult;
        sourceBegin = pointeradd(sourceBegin, inputByteCount);
    }

    return MeshOutputWriterStatus::Success;
}

#endif

} // namespace kraken::io
