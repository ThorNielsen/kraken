#include "kraken/io/read/ply.hpp"

#include "kraken/io/read/ply/attributeidentification.hpp"
#include "kraken/io/read/ply/parsing.hpp"
#include "kraken/io/utility/tokeniser.hpp"
#include "kraken/utility/range.hpp"

namespace kraken::io::formats::ply
{

std::optional<Header>
readHeader(DataSource& source)
{
    std::byte magic[4];
    if (source.memcpy(magic, 4) != 4) return std::nullopt;
    if (   std::to_integer<char>(magic[0]) != 'p'
        || std::to_integer<char>(magic[1]) != 'l'
        || std::to_integer<char>(magic[2]) != 'y')
    {
        return std::nullopt;
    }
    Header header
    {
        .format{StoredFormat::Ascii},
        .elements{},
        .comments{},
        .lineEnd = LineEnd::cr()
    };
    header.lineEnd.lineEnd = magic[3];
    if (header.lineEnd.lineEnd != std::byte{0x0d}
        && header.lineEnd.lineEnd != std::byte{0x0a})
    {
        return std::nullopt;
    }
    else
    {
        std::size_t readLength = 4;
        auto otherLineEnd = std::byte{0x0a^0x0d}^header.lineEnd.lineEnd;
        source.readString([otherLineEnd](std::byte b) noexcept
                          -> bool { return b == otherLineEnd; },
                          magic,
                          &readLength);
        if (readLength != 4)
        {
            header.lineEnd.secondLineEndChar = otherLineEnd;
        }
    }

    std::string reader;

    while (source.bytesLeft().value() && reader != "end_header")
    {
        if (!nextToken(source, reader)) return std::nullopt;
        if (reader == "format")
        {
            if (!nextToken(source, reader)) return std::nullopt;
            if (reader == "ascii")
            {
                header.format = StoredFormat::Ascii;
            }
            else if (reader == "binary_little_endian")
            {
                header.format = StoredFormat::BinaryLittleEndian;
            }
            else if (reader == "binary_big_endian")
            {
                header.format = StoredFormat::BinaryBigEndian;
            }
            else
            {
                return std::nullopt; // Unknown format.
            }
        }
        else if (reader == "comment")
        {
            advanceToNextToken(source);
            header.comments.emplace_back();
            header.comments.back() =
            {
                .text = getline(source, header.lineEnd),
                .beforeElementIndex = header.elements.size(),
                .beforePropertyIndex = header.elements.empty()
                                     ? 0
                                     : header.elements.back().element.properties.size(),
            };
            continue;
        }
        else if (reader == "element")
        {
            header.elements.emplace_back();
            header.elements.back().element.name = nextToken(source);
            header.elements.back().count = parseNext<std::size_t>(source);
        }
        else if (reader == "property")
        {
            if (header.elements.empty()) return std::nullopt;
            Property prop;
            auto type = nextToken(source);
            if (type == "list")
            {
                auto listType = nextToken(source);
                auto parsedListType = read::ply::toPrimitiveDataType(listType.c_str());
                if (!parsedListType) return std::nullopt;
                if (!isInteger(*parsedListType)) return std::nullopt;
                prop.listIndexType = parsedListType;
                type = nextToken(source);
            }
            if (auto parsedType = read::ply::toPrimitiveDataType(type.c_str()))
            {
                prop.type = parsedType.value();
            }
            else return std::nullopt;
            prop.name = nextToken(source);
            header.elements.back().element.properties.emplace_back(std::move(prop));
        }
        else if (reader == "end_header")
            ;
        else
        {
            // Unknown word in header; this may be controversial, but we cannot
            // in good conscience continue parsing.
            return std::nullopt;
        }
        read::ply::skipToLineEnd(source, header);
    }
    return header;
}

bool readBody(DataSource& source,
              const Header& header,
              const ElementParserFunctions& parsers)
{
    std::optional<const ElementParser*> handleUnknownElement;
    for (std::size_t elemIndex : range(header.elements.size()))
    {
        const auto& currName = header.elements[elemIndex].element.name;
        auto parseIter = parsers.find(currName);
        if (parseIter != parsers.end())
        {
            if (!parseIter->second(source, header, elemIndex))
            {
                return false;
            }
        }
        else
        {
            if (!handleUnknownElement.has_value())
            {
                auto hueIt = parsers.find("");
                if (hueIt != parsers.end())
                {
                    handleUnknownElement = &hueIt->second;
                }
                else handleUnknownElement = nullptr;
            }
            if (*handleUnknownElement)
            {
                if (!(**handleUnknownElement)(source, header, elemIndex))
                {
                    return false;
                }
            }
            else
            {
                if (!read::ply::skipElement(source, header, elemIndex))
                {
                    return false;
                }
            }
        }
    }

    return true;
}

bool read(DataSource& source, const ElementParserFunctions& parsers)
{
    auto header = readHeader(source);
    if (!header) return false;
    return readBody(source, *header, parsers);
}

ElementParser createDefaultVertexParser(ExternalMemoryAllocation& dataOut,
                                        VertexLayout& layoutOut,
                                        std::size_t& vertexCountOut,
                                        ExtendedAttributeNames* attrNames)
{
    return [&dataOut, &layoutOut, &vertexCountOut, attrNames]
    (DataSource& source, const Header& header, std::size_t elemIndex)
    {
        auto attributes = read::ply::identifyAttributes(header.elements[elemIndex].element, attrNames);
        if (!attributes) return false;
        auto compMappings = computeComponentMapping(*attributes,
                                                    attributes->attributes.data(),
                                                    attributes->attributes.size());
        layoutOut.attributes = attributes->attributes;

        auto result = read::ply::readVertices(source,
                                              header.format,
                                              dataOut,
                                              header.elements[elemIndex].count,
                                              compMappings.data(),
                                              compMappings.size());

        if (result)
        {
            vertexCountOut = header.elements[elemIndex].count;
        }

        return result;
    };
}

ElementParser createRelocatingVertexParser(ExternalMemoryAllocation& dataOut,
                                           const VertexLayout& desiredLayout,
                                           VertexLayout& resultinglayoutOut,
                                           std::size_t& vertexCountOut,
                                           ExtendedAttributeNames* attrNames)
{
    return [&dataOut, &desiredLayout, &resultinglayoutOut, &vertexCountOut, attrNames]
        (DataSource& source, const Header& header, std::size_t elemIndex)
    {
        auto attributes = read::ply::identifyAttributes(header.elements[elemIndex].element, attrNames);
        if (!attributes) return false;
        auto compMappings = computeComponentMapping(*attributes,
                                                    desiredLayout.attributes.data(),
                                                    desiredLayout.attributes.size());
        resultinglayoutOut.attributes = attributes->attributes;

        auto result = read::ply::readVertices(source,
                                              header.format,
                                              dataOut,
                                              header.elements[elemIndex].count,
                                              compMappings.data(),
                                              compMappings.size());

        if (result)
        {
            vertexCountOut = header.elements[elemIndex].count;
        }

        return result;
    };
}

ElementParser createTriangulatingFaceParser(ExternalMemoryAllocation& dataOut,
                                            TriangleInfo& triangleInfoOut)
{
    return [&dataOut, &triangleInfoOut]
    (DataSource& source, const Header& header, std::size_t elemIndex)
    {
        read::ply::FaceInfo info;
        info.desiredIndexWidths = {2, 4, 0};
        for (const auto& [element, count] : header.elements)
        {
            if (element.name == "vertex")
            {
                info.vertexCount = count;
                break;
            }
        }
        auto result = read::ply::readFacesTriangulated(source,
                                                       header.elements[elemIndex].element,
                                                       header.elements[elemIndex].count,
                                                       header.format,
                                                       dataOut,
                                                       info);

        if (result)
        {
            triangleInfoOut.indexByteWidth = info.chosenIndexWidth;
            triangleInfoOut.triangleCount = dataOut.size() / (info.chosenIndexWidth * 3);
        }

        return result;
    };
}

} // namespace kraken::io::formats::ply
