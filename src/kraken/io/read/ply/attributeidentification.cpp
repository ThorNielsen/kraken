#include "kraken/io/read/ply/attributeidentification.hpp"

#include "kraken/io/formats/ply.hpp"
#include "kraken/utility/range.hpp"

#include <charconv>
#include <cstring>

namespace kraken::io::read::ply
{

struct VertexAttributeComponent
{
    std::string name;
    VertexAttributeUsageType usage;
    std::optional<size_t> component;

    void canonicaliseName();
};

void VertexAttributeComponent::canonicaliseName()
{
    if (auto canonicalName = getCanonicalName(usage))
    {
        name = canonicalName;
    }
}

[[nodiscard]] std::optional<PrimitiveDataType>
toPrimitiveDataType(const char* s)
{
    if      (!strcmp(s, "char"))    return PrimitiveDataType::Byte;
    else if (!strcmp(s, "uchar"))   return PrimitiveDataType::UByte;
    else if (!strcmp(s, "short"))   return PrimitiveDataType::Short;
    else if (!strcmp(s, "ushort"))  return PrimitiveDataType::UShort;
    else if (!strcmp(s, "int"))     return PrimitiveDataType::Int;
    else if (!strcmp(s, "uint"))    return PrimitiveDataType::UInt;
    else if (!strcmp(s, "float"))   return PrimitiveDataType::Float;
    else if (!strcmp(s, "double"))  return PrimitiveDataType::Double;

    // The following malformed data type specifications has been found in
    // random files: uint8, int32 and float32.
    else if (!strcmp(s, "int8"))    return PrimitiveDataType::Byte;
    else if (!strcmp(s, "int16"))   return PrimitiveDataType::Short;
    else if (!strcmp(s, "int32"))   return PrimitiveDataType::Int;
    else if (!strcmp(s, "uint8"))   return PrimitiveDataType::UByte;
    else if (!strcmp(s, "uint16"))  return PrimitiveDataType::UShort;
    else if (!strcmp(s, "uint32"))  return PrimitiveDataType::UInt;
    else if (!strcmp(s, "float32")) return PrimitiveDataType::Float;
    else if (!strcmp(s, "float64")) return PrimitiveDataType::Double;
    else return std::nullopt;
}

std::optional<std::size_t> suffixCharToComponentIndex(char c)
{
    switch (c)
    {
    case 'x': case 's': case 'r': case '0': return 0;
    case 'y': case 't': case 'g': case '1': return 1;
    case 'z': case 'p': case 'b': case '2': return 2;
    case 'w': case 'q': case 'a': case '3': return 3;
    case '4': return 4;
    case '5': return 5;
    case '6': return 6;
    case '7': return 7;
    case '8': return 8;
    case '9': return 9;
    default:
        return std::nullopt;
    }
}

// Note: MUST be trimmed; in particular *(end-1) must be part of the name.
// Furthermore the returned name has been stripped of the suffix, if any.
std::optional<VertexAttributeComponent>
detectVertexUsageByPLYName(const char* begin, const char* end)
{
    if (begin == end) return std::nullopt;

    VertexAttributeComponent component
    {
        .name{begin, end},
        .usage{VertexAttribute::Unknown},
        .component{suffixCharToComponentIndex(end[-1])},
    };

    if (component.component.has_value())
    {
        component.name.pop_back(); // Remove suffix.
    }

    auto origNameLength = static_cast<std::size_t>(end - begin);

    if (origNameLength == 1)
    {
        component.component = suffixCharToComponentIndex(*begin);
        switch (*begin)
        {
        case 'x': case 'y': case 'z': case 'w':
            component.usage = VertexAttribute::Position;
            component.canonicaliseName();
            return component;
        case 's': case 't': case 'p': case 'q':
            component.usage = VertexAttribute::Texcoord;
            component.canonicaliseName();
            return component;
        case 'r': case 'g': case 'b': case 'a':
            component.usage = VertexAttribute::Colour;
            component.canonicaliseName();
            return component;
        default:
            ;
        }
    }

    // The only general two-letter description is for normals.
    if (origNameLength == 2
        && *begin == 'n'
        && (begin[1] == 'x' || begin[1] == 'y' || begin[1] == 'z' || begin[1] == 'w'))
    {
        component.usage = VertexAttribute::Normal;
        component.canonicaliseName();
        return component;
    }

    {
        std::optional<size_t> colourComponent;
        if      (!strncmp("red",   begin, origNameLength)) colourComponent = 0;
        else if (!strncmp("green", begin, origNameLength)) colourComponent = 1;
        else if (!strncmp("blue",  begin, origNameLength)) colourComponent = 2;
        else if (!strncmp("alpha", begin, origNameLength)) colourComponent = 3;

        if (colourComponent.has_value())
        {
            component.component = *colourComponent;
            component.usage = VertexAttribute::Colour;
            component.canonicaliseName();
            return component;
        }
    }

    if (!component.component.has_value())
    {
        const char* suffixStart = end;
        while (--suffixStart != begin)
        {
            if (*suffixStart == '_' || *suffixStart == '-')
            {
                // In this case we have scanned too far back.
                ++suffixStart;
                break;
            }
        }

        size_t value;
        // We will also try to parse it as an integer.
        auto result = std::from_chars(suffixStart, end, value, 10);
        if (result.ec == std::errc{})
        {
            component.component = value;
            component.name.resize(static_cast<std::size_t>(suffixStart - begin));
        }
    }

    return component;
}

class VertexAttributeIdentificationHelper
{
public:
    void insertNext(VertexAttributeComponent&& component,
                    const formats::ply::Property& property,
                    VertexAttributeIdentification& into,
                    ExtendedAttributeNames* extNames)
    {
        DataRange nextRange{0, byteSize(property.type)};
        if (!into.locations.empty())
        {
            nextRange.offset = into.locations.back().byteRange.rangeEnd();
        }

        into.locations.emplace_back
        (
            nextRange,
            findAttributeIndex(std::move(component), property, into, extNames),
            component.component.value_or(0)
        );
    }

    // Sets component counts and normalises
    void finalise(VertexAttributeIdentification& into)
    {
        ensureAttributeMaskSync(into);
        for (auto& mask : usedComponentMasks)
        {
            auto leastComponent = std::countr_zero(mask);
            mask >>= leastComponent;
            // Technically we can work around this by just filling the component
            // in question with garbage (or zero-init if we are nice).
            if (std::popcount(mask) != std::countr_one(mask))
            {
                throw std::runtime_error("Missing component in attribute.");
            }
            mask = leastComponent; // This way we can subtract from the
                                   // components in question without having to
                                   // allocate new memory.
        }
        for (auto& component : into.locations)
        {
            auto& attribute = into.attributes.at(component.attributeIndex);
            auto offset = usedComponentMasks.at(component.attributeIndex);
            component.component -= offset;
            attribute.componentCount = std::max<U8>(attribute.componentCount,
                                                    component.component+1);
        }
    }

private:
    std::vector<U64> usedComponentMasks;

    void ensureAttributeMaskSync(VertexAttributeIdentification& into)
    {
        if (usedComponentMasks.size() != into.attributes.size())
        {
            throw std::logic_error("Desync in attribute identification helper "
                                   "and attribute identification data structure"
                                   ".");
        }
    }

    std::size_t findAttributeIndex(VertexAttributeComponent&& component,
                                   const formats::ply::Property& property,
                                   VertexAttributeIdentification& into,
                                   ExtendedAttributeNames* extNames)
    {
        if (component.usage == VertexAttributeUsageType::Unknown
            && extNames)
        {
            component.usage = extNames->makeExtendedUsage(component.name);
        }
        auto currCompMask = U64(1) << U64(component.component.value_or(0));

        ensureAttributeMaskSync(into);

        U8 nextSetIndex = 0;
        // Yes, yields O(n²) complexity (as we do this for every component); but
        // if that ever matters we have other issues to deal with, as the number
        // of components *really* shouldn't be that large.
        for (auto i : range(into.attributes.size()))
        {
            if (into.attributes[i].usage == component.usage)
            {
                ++nextSetIndex;
                if (!(usedComponentMasks[i] & currCompMask))
                {
                    usedComponentMasks[i] |= currCompMask;
                    return i;
                }
            }
        }

        into.attributes.emplace_back(property.type,
                                     component.usage,
                                     0,
                                     nextSetIndex);
        usedComponentMasks.emplace_back(currCompMask);
        return into.attributes.size()-1;
    }
};

std::optional<VertexAttributeIdentification>
identifyAttributes(const formats::ply::Element& element,
                   ExtendedAttributeNames* extAttrNames)
{
    if (element.containsList()) return std::nullopt;

    VertexAttributeIdentification attributes;
    VertexAttributeIdentificationHelper helper;

    for (const auto& property : element.properties)
    {
        auto nameBegin = property.name.c_str();
        auto nameEnd = nameBegin + property.name.size();
        auto component = detectVertexUsageByPLYName(nameBegin, nameEnd).value();
        helper.insertNext(std::move(component),
                          property,
                          attributes,
                          extAttrNames);
    }

    helper.finalise(attributes);

    return attributes;
}

} // namespace kraken::io::read::ply
