#include "kraken/io/read/ply/parsing.hpp"

#include "kraken/utility/range.hpp"

namespace kraken::io::read::ply
{

void skipElementWithList(DataSource& source,
                         const formats::ply::Header& header,
                         std::size_t elementIndex)
{
    const auto& element = header.elements.at(elementIndex).element;
    auto multiplicity = header.elements[elementIndex].count;
    if (header.format == formats::ply::StoredFormat::Ascii)
    {
        while (multiplicity --> 0)
        {
            for (const auto& prop : element.properties)
            {
                skipPlainProperty(source, prop);
            }
        }
    }
    else if (header.format == formats::ply::StoredFormat::BinaryBigEndian
             || header.format == formats::ply::StoredFormat::BinaryLittleEndian)
    {
        const auto endian = header.format == formats::ply::StoredFormat::BinaryLittleEndian
                          ? std::endian::little
                          : std::endian::big
                          ;
        std::vector<std::size_t> byteSizes(element.properties.size(), 0);
        std::vector<std::size_t> indexSizes(element.properties.size(), 0);
        for (auto i : range(element.properties.size()))
        {
            byteSizes[i] = byteSize(element.properties[i].type);
            if (auto listIndexType = element.properties[i].listIndexType)
            {
                indexSizes[i] = byteSize(*listIndexType);
            }
        }
        while (multiplicity --> 0)
        {
            for (auto i : range(byteSizes.size()))
            {
                auto bytesToDiscard = byteSizes[i];
                switch (indexSizes[i])
                {
                case 1:
                    bytesToDiscard *= std::size_t(readBinary<U8>(source, endian));
                    break;
                case 2:
                    bytesToDiscard *= std::size_t(readBinary<U16>(source, endian));
                    break;
                case 4:
                    bytesToDiscard *= std::size_t(readBinary<U32>(source, endian));
                    break;
                default:
                    ;
                }
                source.skip(bytesToDiscard);
            }
        }
    }
    else throw std::logic_error("Invalid ply::StoredFormat.");
}

bool skipElement(DataSource& source,
                 const formats::ply::Header& header,
                 std::size_t elementIndex)
{
    const auto& [element, multiplicity] = header.elements.at(elementIndex);
    if (element.containsList())
    {
        skipElementWithList(source, header, elementIndex);
    }
    else if (header.format == formats::ply::StoredFormat::Ascii)
    {
        // If we do not have a list, each element contributes exactly one token.
        auto toSkip = element.properties.size() * multiplicity;
        for ([[maybe_unused]] auto index : range(toSkip))
        {
            skipNextToken(source);
        }
    }
    else if (header.format == formats::ply::StoredFormat::BinaryBigEndian
             || header.format == formats::ply::StoredFormat::BinaryLittleEndian)
    {
        std::size_t elemByteSize = 0;
        for (const auto& prop : element.properties)
        {
            elemByteSize += byteSize(prop.type);
        }
        source.skip(elemByteSize * multiplicity);
    }
    else throw std::logic_error("Invalid ply::StoredFormat.");
    return true;
}


// Assumes the output size has been reserved in advance, and will not check
// if that is the case.
template <bool isNativeEndian>
bool readBinaryVertices(DataSource& source,
                        std::size_t vertexCount,
                        const VertexAttributeComponentMapping* pMappings,
                        std::size_t mappingCount,
                        ExternalMemoryAllocation& destination)
{
    std::vector<std::size_t> inputSizes(mappingCount);
    for (auto i : range(mappingCount))
    {
        inputSizes[i] = byteSize(i[pMappings].dataType);
    }
    // We can read at most 8 bytes at once
    U8 tmpbuf[8];
    for ([[maybe_unused]] auto vert : range(vertexCount))
    {
        for (const auto comp : range(mappingCount))
        {
            if (source.memcpy(tmpbuf, inputSizes[comp]) != inputSizes[comp])
            {
                return false;
            }

            if constexpr (!isNativeEndian)
            {
                std::reverse(tmpbuf, tmpbuf + inputSizes[comp]);
            }
            if (pMappings[comp].shouldOutput)
            {
                destination.memcpy(tmpbuf, inputSizes[comp]);
            }
        }
    }

    return true;
}

bool readPlainVertices(DataSource& source,
                       std::size_t vertexCount,
                       const VertexAttributeComponentMapping* pMappings,
                       std::size_t mappingCount,
                       ExternalMemoryAllocation& destination)
{
    std::vector<std::size_t> outputSizes(mappingCount);
    for (auto i : range(mappingCount))
    {
        outputSizes[i] = i[pMappings].shouldOutput
                           ? byteSize(i[pMappings].dataType)
                           : 0
            ;
    }

    U8 tmpbuf[8];
    for ([[maybe_unused]] auto vert : range(vertexCount))
    {
        for (auto mapping : range(mappingCount))
        {
            advanceToNextToken(source);
            if (!parseAscii(source, tmpbuf, pMappings[mapping].dataType))
            {
                return false;
            }
            destination.memcpy(tmpbuf, outputSizes[mapping]);
        }
    }

    return true;
}

bool readVertices(DataSource& source,
                  formats::ply::StoredFormat format,
                  ExternalMemoryAllocation& destination,
                  std::size_t vertexCount,
                  const VertexAttributeComponentMapping* pMappings,
                  std::size_t mappingCount,
                  std::size_t* vertexSize)
{
    std::size_t vertexStride = vertexSize ? *vertexSize : 0;
    bool canMemcpy = isFormatNativeEndian(format);
    for (auto i : range(mappingCount))
    {
        const auto& mapping = pMappings[i];
        if (mapping.shouldOutput)
        {
            vertexStride = std::max(vertexStride,
                                    mapping.outputOffset
                                        + byteSize(mapping.dataType));
        }
        else canMemcpy = false;
    }

    if (vertexSize) *vertexSize = vertexStride;

    const auto outputByteSize = vertexStride * vertexCount;

    if (!destination.reserveExact(destination.size() + outputByteSize, 8)) // 8 bytes of alignment is the max possible (F64)
    {
        return false;
    }

    if (canMemcpy)
    {
        auto copied = source.memcpy(destination.currDataPos(), outputByteSize);
        return copied == outputByteSize;
    }

    if (format == formats::ply::StoredFormat::Ascii)
    {
        return readPlainVertices(source, vertexCount, pMappings, mappingCount, destination);
    }
    bool isNativeLE = std::endian::native == std::endian::little;
    bool isStoredLE = format == formats::ply::StoredFormat::BinaryLittleEndian;

    if (isNativeLE == isStoredLE)
    {
        return readBinaryVertices<true>(source, vertexCount, pMappings, mappingCount, destination);
    }
    else
    {
        return readBinaryVertices<false>(source, vertexCount, pMappings, mappingCount, destination);
    }
}

bool readFacesTriangulated(DataSource& source,
                           const formats::ply::Element& element,
                           std::size_t faceCount,
                           formats::ply::StoredFormat format,
                           ExternalMemoryAllocation& destination,
                           FaceInfo& auxInfo)
{
    std::size_t vertexIndexLocation = element.properties.size();
    for (auto propIndex : range(element.properties.size()))
    {
        if ((element.properties[propIndex].name == "vertex_indices"
             || element.properties[propIndex].name == "vertex_index")
            && element.properties[propIndex].isList())
        {
            vertexIndexLocation = propIndex;
            break;
        }
    }
    if (element.properties.size() == 1
        && element.properties[0].isList())
    {
        vertexIndexLocation = 0; // Probably just weirdly named.
    }
    // No indices found.
    if (vertexIndexLocation >= element.properties.size()) return false;

    const auto& indexProp = element.properties[vertexIndexLocation];

    // We cannot memcpy in any circumstance due to the leading list element.
    auto endian = getEndian(format);

    if (auxInfo.vertexCount.has_value())
    {
        // We specifically do not allow the max value as an index, since that
        // may be interpreted as a primitive restart index in some contexts.
        if (auxInfo.vertexCount <= 0xfe) auxInfo.chosenIndexWidth = 1;
        else if (auxInfo.vertexCount <= 0xfffe) auxInfo.chosenIndexWidth = 2;
        else auxInfo.chosenIndexWidth = 4;
        for (auto desiredWidth : auxInfo.desiredIndexWidths)
        {
            if (desiredWidth && auxInfo.chosenIndexWidth <= desiredWidth)
            {
                auxInfo.chosenIndexWidth = desiredWidth;
                break;
            }
        }
    }
    else auxInfo.chosenIndexWidth = 4; // Max width is the only safe.

    const auto listByteCount = byteSize(*indexProp.listIndexType)
                             * std::size_t(endian.has_value());

    const auto indexByteCount = byteSize(indexProp.type)
                              * std::size_t(endian.has_value());

    destination.reserve(faceCount * 3 * auxInfo.chosenIndexWidth, 4);

    for ([[maybe_unused]] auto faceIndex : range(faceCount))
    {
        for (auto propIndex : range(element.properties.size()))
        {
            const auto& property = element.properties[propIndex];
            if (propIndex != vertexIndexLocation)
            {
                if (endian) read::ply::skipBinaryProperty(source, property, *endian);
                else read::ply::skipPlainProperty(source, property);
                continue;
            }

            // Index count is assumed to be an unsigned integer since non-
            // negative counts are meaningless.
            U32 indexCount = 0;
            if (!read::ply::readInteger(indexCount, source, listByteCount, endian))
            {
                return false;
            }

            // We cannot have a non-degenerate face if there are fewer than 3;
            // indicating something wrote an invalid model.
            if (indexCount < 3)
            {
                return false;
            }

            U32 first, prev;
            if (!read::ply::readInteger(first, source, indexByteCount, endian))
            {
                return false;
            }
            if (!read::ply::readInteger(prev, source, indexByteCount, endian))
            {
                return false;
            }
            for ([[maybe_unused]] auto iter : range<U32>(2, indexCount))
            {
                U32 curr;
                if (!read::ply::readInteger(curr, source, indexByteCount, endian))
                {
                    return false;
                }
                bool result = false;
                switch (auxInfo.chosenIndexWidth)
                {
                case 1:
                    result = writeIndices<U8>(destination, first, prev, curr);
                    break;
                case 2:
                    result = writeIndices<U16>(destination, first, prev, curr);
                    break;
                case 4:
                    result = writeIndices<U32>(destination, first, prev, curr);
                    break;
                default:
                    ;
                }

                if (!result)
                {
                    return result;
                }
                prev = curr;
            }
        }
    }

    return true;
}

} // namespace kraken::io::read::ply
