#include "kraken/io/read/stl.hpp"

#include "kraken/io/utility/attributeidentification.hpp"
#include "kraken/io/utility/tokeniser.hpp"

namespace kraken::io::formats::stl
{

namespace
{

VertexAttributeIdentification getPlainIdentification()
{
    return
    {
        .attributes
        {
            {
                .dataType = PrimitiveDataType::Float,
                .usage = VertexAttributeUsageType::Position,
                .componentCount = 3,
                .setIndex = 0,
            },
            {
                .dataType = PrimitiveDataType::Float,
                .usage = VertexAttributeUsageType::Normal,
                .componentCount = 3,
                .setIndex = 0,
            },
        },
        .locations
        {
            // First we have normals:
            {{0, 4}, 1, 0}, {{4, 8}, 1, 1}, {{8, 12}, 1, 2},

            // Then positions. There are technically three of these.
            {{12, 16}, 0, 0}, {{16, 20}, 0, 1}, {{20, 24}, 0, 2},
        },
    };
}

VertexAttributeIdentification getBinaryIdentification()
{
    return
    {
        .attributes
        {
            {
                .dataType = PrimitiveDataType::Float,
                .usage = VertexAttributeUsageType::Position,
                .componentCount = 3,
                .setIndex = 0,
            },
            {
                .dataType = PrimitiveDataType::Float,
                .usage = VertexAttributeUsageType::Normal,
                .componentCount = 3,
                .setIndex = 0,
            },
            {
                .dataType = PrimitiveDataType::UShort,
                .usage = VertexAttributeUsageType::Unknown,
                .componentCount = 1,
                .setIndex = 0,
            },
        },
        .locations
        {
            // Normals
            {{0, 4}, 1, 0}, {{4, 8}, 1, 1}, {{8, 12}, 1, 2},

            // Positions
            {{12, 16}, 0, 0}, {{16, 20}, 0, 1}, {{20, 24}, 0, 2},

            // 'Weird' attributes
            {{24, 26}, 2, 0},
        },
    };
}

} // end anonymous namespace

bool readPlain(DataSource& source,
               ExternalMemoryAllocation& verticesOut,
               std::size_t& vertexCountOut,
               VertexLayout& layoutOut,
               const VertexLayout* desiredLayout)
{
    vertexCountOut = 0;
    auto identification = getPlainIdentification();

    if (identification.locations.size() != 3+3)
    {
        throw std::logic_error("Got bad identification.");
    }

    if (desiredLayout) layoutOut = *desiredLayout;
    else layoutOut.attributes = identification.attributes;

    auto mappings = computeComponentMapping(identification,
                                            layoutOut.attributes.data(),
                                            layoutOut.attributes.size());
    if (mappings.size() != 3+3)
    {
        throw std::logic_error("Got bad mapping.");
    }

    std::string reader;
    if (!nextToken(source, reader)) return false;
    if (reader != "solid") return false;
    source.skip([](std::byte b) noexcept
                -> bool { return b != std::byte{0x0a} && b != std::byte{0x0d}; });
    if (source.atEnd()) return false;

    LineEnd currLineEnd;
    if (!source.readSingleByte(currLineEnd.lineEnd)) return false;
    if (source.readSingleByte(currLineEnd.secondLineEndChar))
    {
        if (currLineEnd.secondLineEndChar != std::byte{0x0a}
            && currLineEnd.secondLineEndChar != std::byte{0x0d})
        {
            currLineEnd.secondLineEndChar = std::byte{0x0};
            if (!source.putback(1)) return false;
        }
    }

    // We are now at the beginning of the next line.
    std::vector<std::byte> vertexStorage(layoutOut.vertexStride(), std::byte{0});

    F32 tmpFloat;

    while (source.bytesLeft().value())
    {
        if (!nextToken(source, reader)) return false;

        if (reader == "facet")
        {
            if (!nextToken(source, reader)) return false;
            if (reader != "normal") return false;
            for (auto i : range(3))
            {
                advanceToNextToken(source);
                if (!source.readAscii(tmpFloat)) return false;
                if (mappings[i].shouldOutput)
                {
                    std::memcpy(vertexStorage.data()+mappings[i].outputOffset,
                                &tmpFloat,
                                sizeof(tmpFloat));
                }
            }
        }
        else if (reader == "outer")
        {
            if (!nextToken(source, reader)
                || reader != "loop") return false;
            else continue;
        }
        else if (reader == "vertex")
        {
            for (auto i : range(3))
            {
                advanceToNextToken(source);
                if (!source.readAscii(tmpFloat)) return false;
                if (mappings[i].shouldOutput)
                {
                    std::memcpy(vertexStorage.data()+mappings[i+3].outputOffset,
                                &tmpFloat,
                                sizeof(tmpFloat));
                }
            }
            if (!verticesOut.memcpy_back(vertexStorage.data(),
                                         vertexStorage.size(),
                                         alignof(F32)))
            {
                return false;
            }
            ++vertexCountOut;
        }
        else if (reader == "endloop" || reader == "endfacet") continue;
        else if (reader == "endsolid")
        {
            skipRestOfLine(source, currLineEnd);
            return vertexCountOut % 3 == 0;
        }
    }

    return false;
}

bool readBinary(DataSource& source,
                ExternalMemoryAllocation& verticesOut,
                std::size_t& vertexCountOut,
                VertexLayout& layoutOut,
                const VertexLayout* desiredLayout)
{
    vertexCountOut = 0;
    auto identification = getBinaryIdentification();

    if (identification.locations.size() != 3+3+1)
    {
        throw std::logic_error("Got bad identification.");
    }

    if (desiredLayout) layoutOut = *desiredLayout;
    else layoutOut.attributes = identification.attributes;

    auto mappings = computeComponentMapping(identification,
                                            layoutOut.attributes.data(),
                                            layoutOut.attributes.size());
    if (mappings.size() != 3+3+1)
    {
        throw std::logic_error("Got bad mapping.");
    }

    source.skip(80);

    U32 faceCount = 0;
    if (!source.readBinary(faceCount, std::endian::little)) return false;

    vertexCountOut = 3*faceCount;

    auto vertexSize = layoutOut.vertexStride();
    if (!verticesOut.reserveExact(vertexSize * vertexCountOut, alignof(float)))
    {
        return false;
    }

    std::vector<std::byte> vertexStorage(vertexSize, std::byte{0});

    F32 positionData[9];

    for ([[maybe_unused]] auto faceIndex : range(faceCount))
    {
        for (auto n : range(3))
        {
            if (mappings[n].shouldOutput)
            {
                if (!source.readBinary(vertexStorage.data() + mappings[n].outputOffset,
                                       4,
                                       std::endian::little))
                {
                    return false;
                }
            }
            else source.skip(4);
        }
        if constexpr (std::endian::native == std::endian::little)
        {
            constexpr auto bytesToCopy = 9 * sizeof(F32);
            if (source.memcpy(positionData, bytesToCopy) != bytesToCopy)
            {
                return false;
            }
        }
        else
        {
            for (auto v : range(9))
            {
                if (!source.readBinary(positionData[v], std::endian::little))
                {
                    return false;
                }
            }
        }

        if (mappings[6].shouldOutput)
        {
            if (!source.readBinary(vertexStorage.data() + mappings[6].outputOffset,
                                   2,
                                   std::endian::little))
            {
                return false;
            }
        }
        else source.skip(2);

        std::size_t posIndex{0};
        for ([[maybe_unused]] auto vert : range(3))
        {
            for (auto coord : range(3))
            {
                if (mappings[3+coord].shouldOutput)
                {
                    std::memcpy(vertexStorage.data() + mappings[3+coord].outputOffset,
                                positionData + posIndex++,
                                sizeof(F32));
                }
            }

            if (!verticesOut.memcpy(vertexStorage.data(), vertexStorage.size()))
            {
                return false;
            }
        }
    }

    return true;
}


bool read(DataSource& source,
          ExternalMemoryAllocation& verticesOut,
          std::size_t& vertexCountOut,
          VertexLayout& layoutOut,
          const VertexLayout* desiredLayout)
{
    std::byte buf[5];
    if (!source.memcpy(buf, 5)) return false;
    if (!source.putback(5)) return false;

    bool isAscii = !std::strncmp(reinterpret_cast<const char*>(buf), "solid", 5);

    if (isAscii)
    {
        return readPlain(source,
                         verticesOut,
                         vertexCountOut,
                         layoutOut,
                         desiredLayout);
    }
    else
    {
        return readBinary(source,
                          verticesOut,
                          vertexCountOut,
                          layoutOut,
                          desiredLayout);
    }

    return false;
}

} // namespace kraken::io::formats::stl
