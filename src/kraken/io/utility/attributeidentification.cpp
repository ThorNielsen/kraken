#include "kraken/io/utility/attributeidentification.hpp"

#include "kraken/utility/range.hpp"

#include <stdexcept>

namespace kraken::io
{

[[nodiscard]]
std::vector<VertexAttributeComponentMapping>
computeComponentMapping(const VertexAttributeIdentification& identification,
                        const VertexAttribute* pAttributes,
                        std::size_t attributeCount)
{
    struct CompareVertexAttributeDisregardComponentCount
    {
        [[nodiscard]] bool operator()(const VertexAttribute& a,
                                      const VertexAttribute& b) const noexcept
        {
            return std::tie(a.usage, a.setIndex, a.dataType)
                 < std::tie(b.usage, b.setIndex, b.dataType);
        }
    };

    // This way we can for a given element access the next index to find its
    // upper bound, thus easily be able to truncate attributes.
    std::vector<std::size_t> offsets(attributeCount+1, 0);
    std::map<VertexAttribute,
             std::size_t,
             CompareVertexAttributeDisregardComponentCount> attributeToNewIndex;

    std::size_t currOffset = 0;
    for (auto i : range(attributeCount))
    {
        attributeToNewIndex.emplace(pAttributes[i], i);
        offsets[i] = currOffset;
        currOffset += byteSize(pAttributes[i].dataType) * pAttributes[i].componentCount;
    }

    offsets.back() = currOffset;

    const auto& locations = identification.locations;

    // This check is merely a sanity one and can be omitted if one is sure it is
    // not violated.
    for (auto i : range<std::size_t>(1, locations.size()))
    {
        if (locations[i-1].byteRange.rangeBegin()
            > locations[i].byteRange.rangeBegin())
        {
            throw std::logic_error("Input identification / locations MUST be "
                                   "sorted.");
        }
    }

    std::vector<VertexAttributeComponentMapping> mapping(locations.size());
    for (auto i : range(locations.size()))
    {
        const auto& loc = locations[i];
        const auto& srcAttr = identification.attributes.at(loc.attributeIndex);
        mapping[i] =
        {
            .dataType = srcAttr.dataType,
            .outputOffset = 0,
            .shouldOutput = false,
        };
        auto it = attributeToNewIndex.find(srcAttr);
        if (it == attributeToNewIndex.end()) continue;

        // Offset into the same attribute.
        auto compSize = byteSize(srcAttr.dataType);
        auto attrOffset = compSize * loc.component;

        auto attrStart = offsets[it->second];
        auto attrEnd = offsets[it->second+1];
        if (attrStart + attrOffset + compSize > attrEnd) continue;
        mapping[i].outputOffset = attrStart + attrOffset;
        mapping[i].shouldOutput = true;
    }

    return mapping;
}

} // namespace kraken::io
