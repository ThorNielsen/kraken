set(INCDIR ${PROJECT_SOURCE_DIR}/include/kraken/utility)
set(SRCDIR ${PROJECT_SOURCE_DIR}/src/kraken/utility)

set(PUBHEADERS
    ${INCDIR}/base64.hpp
    ${INCDIR}/checksum.hpp
    ${INCDIR}/containers.hpp
    ${INCDIR}/filesystem.hpp
    ${INCDIR}/hufftree.hpp
    ${INCDIR}/json.hpp
    ${INCDIR}/memory.hpp
    ${INCDIR}/range.hpp
    ${INCDIR}/string.hpp
    ${INCDIR}/template_support.hpp
    ${INCDIR}/timer.hpp
    ${INCDIR}/utf.hpp
    ${INCDIR}/zlib.hpp
)

set(SOURCES
    ${SRCDIR}/checksum.cpp
    ${SRCDIR}/deflate.cpp
    ${SRCDIR}/filesystem.cpp
    ${SRCDIR}/hufftree.cpp
    ${SRCDIR}/json.cpp
    ${SRCDIR}/memory.cpp
    ${SRCDIR}/string.cpp
    ${SRCDIR}/utf.cpp
    ${SRCDIR}/zlib.cpp
)

add_library(kraken-utility SHARED ${SOURCES})
target_sources(kraken-utility PUBLIC FILE_SET HEADERS BASE_DIRS ${PROJECT_SOURCE_DIR}/include FILES ${PUBHEADERS})
set_target_properties(kraken-utility PROPERTIES LINKER_LANGUAGE CXX)
if (KRAKEN_ENABLE_UNICODE)
    target_link_libraries(kraken-utility boost_locale)
endif()
install(TARGETS kraken-utility
        LIBRARY DESTINATION ${CMAKE_INSTALL_PREFIX}/lib
        FILE_SET HEADERS)
