#include "kraken/utility/memory.hpp"
#include <algorithm>
#include <cmath>
#include <cstddef>
#include <limits>
#include <queue>
#include <stack>
#include <stdexcept>
#include <utility>
#include <vector>

#include <kraken/types.hpp>
#include <kraken/math/integer.hpp>
#include <kraken/utility/checksum.hpp>
#include <kraken/utility/containers.hpp>
#include <kraken/utility/zlib.hpp>

namespace kraken
{

namespace priv
{

struct Huffcode
{
    U16 code;
    U8 bitlen;
};

// Stores nodes tree in the following way:
// If left == right then it is a leaf node and that is the symbol which is
// stored. Otherwise, left is the index of the left node in the array and
// similarly for right.
struct HufftreeNode
{
    U64 count;
    U16 left;
    U16 right;
};

std::vector<U8> convertToLengths(const std::vector<HufftreeNode>& nodes,
                                 U16 root,
                                 U16 numCodes)
{
    struct NodeInfo
    {
        U32 index;
        U32 codelen;
    };
    std::stack<NodeInfo> niStack;
    niStack.push({(U32)root, 0});
    std::vector<U8> huffcodes(numCodes, 0);
    // If (and only if) there is only a single code, the tree is not complete
    // and in particular, there are no internal nodes.
    if (numCodes == 1)
    {
        // We therefore know that we can just set the length to be 1.
        huffcodes[root] = 1;
        return huffcodes;
    }
    while (!niStack.empty())
    {
        auto cinfo = niStack.top(); niStack.pop();
        const auto& cnode = nodes[cinfo.index];
        if (cnode.left == cnode.right) // Leaf node
        {
            huffcodes.at(cnode.left) = static_cast<U8>(cinfo.codelen);
        }
        else // Internal node
        {
            niStack.push({cnode.left, cinfo.codelen+1});
            niStack.push({cnode.right, cinfo.codelen+1});
        }
    }
    return huffcodes;
}

std::vector<U16> findViolatingParents(const std::vector<HufftreeNode>& nodes,
                                      size_t root, U8 maxlen)
{
    std::vector<U16> ids;
    std::queue<std::pair<U16, U8>> depths;
    depths.push({root, 1});
    while (!depths.empty())
    {
        if (depths.size() > (U64(1)<<maxlen) || ids.size() > nodes.size())
        {
            throw std::logic_error("Way too many violators.");
        }
        auto curr = depths.front();
        depths.pop();
        auto& node = nodes[curr.first];

        // Test for leaf node.
        if (node.left == node.right) continue;

        if (curr.second >= maxlen) ids.push_back(curr.first);
        depths.push({node.left, curr.second + 1});
        depths.push({node.right, curr.second + 1});
    }
    return ids;
}

void computeDepths(const std::vector<HufftreeNode>& nodes, size_t currNode,
                   std::vector<U8>& depths, U8 currDepth = 1)
{
    auto curr = nodes[currNode];
    depths[currNode] = currDepth;
    if (curr.left != curr.right)
    {
        computeDepths(nodes, curr.left, depths, currDepth+1);
        computeDepths(nodes, curr.right, depths, currDepth+1);
    }
}

std::vector<U8> computeDepths(const std::vector<HufftreeNode>& nodes,
                              size_t root)
{
    std::vector<U8> depths(nodes.size());
    computeDepths(nodes, root, depths);
    return depths;
}

void fixupSubtree(U16 nodeID, std::vector<HufftreeNode>& nodes,
                  std::vector<U8>& depths, U8 maxDepth)
{
    if (depths[nodeID]+1 < maxDepth) return;
    U16 bestID = 0xffff;
    U8 bestDepth = 0;
    for (U16 i = 0; i < nodes.size(); ++i)
    {
        // A given node is only better than any previous one if it is a leaf
        // node (always required) and that its depth is higher than the best
        // found so far but still less than the maximal depth (this way the tree
        // will be close to the optimal one assuming that the maximal depth
        // isn't ridiculously low).
        if (nodes[i].left == nodes[i].right &&
            depths[i]+1 < maxDepth && depths[i] > bestDepth)
        {
            bestID = i;
            bestDepth = depths[i];
        }
    }
    if (!bestDepth)
    {
        throw std::logic_error("Could not find anywhere to put subtree.");
    }
    U16 lchild = nodes[nodeID].left;
    std::swap(nodes[nodeID], nodes[bestID]);
    std::swap(nodes[nodeID], nodes[lchild]);
    computeDepths(nodes, nodeID, depths, depths[nodeID]);
    computeDepths(nodes, bestID, depths, depths[bestID]);
}

// Note: If truncation is impossible (too many codes for a given bit length),
// then nothing is done to the tree.
std::vector<U8> truncateHufftree(std::vector<HufftreeNode>& nodes,
                                 size_t root, size_t numCodes, U8 maxlen)
{
    if (!maxlen)
    {
        if (maxlen && (U64(1) << maxlen) < numCodes)
        {
            throw std::runtime_error("Truncation impossible (too few codes).");
        }
        return convertToLengths(nodes, root, numCodes);
    }
    U8 maxDepth = maxlen;

    auto problems = findViolatingParents(nodes, root, maxDepth);
    //std::reverse(problems.begin(), problems.end());
    auto depths = computeDepths(nodes, root);
    while (!problems.empty())
    {
        auto node = problems.back();
        problems.pop_back();
        if (depths[node]+1 >= maxDepth && nodes[node].left != nodes[node].right)
        {
            fixupSubtree(node, nodes, depths, maxDepth);
        }
    }

    return convertToLengths(nodes, root, numCodes);
}

std::vector<U8> generateHufftree(std::vector<U64> counts, U16 maxcode, U8 truncate = 0)
{
    PriorityQueue<std::pair<U64, U16>> q;
    if (counts.size() >= 0xffff)
    {
        throw std::runtime_error("Too many symbols.");
    }
    std::vector<HufftreeNode> nodes;
    std::vector<U16> mappings;
    auto treesize = counts.size() > maxcode ? maxcode + 1 : counts.size();
    for (U16 i = 0; i < treesize; ++i)
    {
        if (counts[i])
        {
            U16 idx = (U16)mappings.size();
            q.push({counts[i], idx});
            nodes.push_back({counts[i], idx, idx});
            mappings.push_back(i);
        }
    }
    if (mappings.empty())
    {
        return std::vector<U8>(treesize, 0);
    }
    size_t root = 0;
    while (!q.empty())
    {
        auto a = q.top(); q.pop();
        if (q.empty())
        {
            // There is only a single undecided node and that must necessarily
            // be the top one.
            root = a.second;
            break;
        }
        auto b = q.top(); q.pop();
        nodes.push_back({a.first + b.first, a.second, b.second});
        q.push({nodes.back().count, nodes.size() - 1});
    }
    auto newTree = truncateHufftree(nodes, root, mappings.size(), truncate);
    std::vector<U8> expandedTree(treesize, 0);
    for (size_t i = 0; i < mappings.size(); ++i)
    {
        expandedTree[mappings[i]] = newTree[i];
    }
    return expandedTree;
}

template <typename Float>
Float distance(const std::vector<U64>& firstCounts,
               const std::vector<U64>& secondCounts,
               U64 firstTotal, U64 secondTotal)
{
    size_t upto = std::min(firstCounts.size(), secondCounts.size());
    Float dist(0);
    Float mul1 = Float(1)/Float(firstTotal);
    Float mul2 = Float(1)/Float(secondTotal);
    for (size_t i = 0; i < upto; ++i)
    {
        Float n1 = Float(firstCounts[i])*mul1;
        Float n2 = Float(secondCounts[i])*mul2;
        if (n1 <= Float(1e-6f)) n1 = Float(1);
        if (n2 <= Float(1e-6f)) n2 = Float(1);
        dist += std::abs(n1*std::log2(n1)-n2*std::log2(n2));
    }
    return dist;
}

template <typename Float>
Float attenuatedDistance(const std::vector<U64>& firstCounts,
                         const std::vector<U64>& secondCounts,
                         U64 firstTotal, U64 secondTotal)
{
    Float x = Float(firstTotal) / Float(firstTotal + secondTotal);
    auto d = distance<Float>(firstCounts, secondCounts,
                              firstTotal, secondTotal);
    return 5*x*(Float(1)-x)*d/*std::sin(x*Float(3.14159265))*/;
}

template <typename Float>
Float entropy(const std::vector<U64>& firstCounts,
              const std::vector<U64>& secondCounts,
              U64 firstTotal, U64 secondTotal)
{
    size_t upto = std::min(firstCounts.size(), secondCounts.size());
    Float dist(0);
    Float mul1 = Float(1)/Float(firstTotal);
    Float mul2 = Float(1)/Float(secondTotal);
    for (size_t i = 0; i < upto; ++i)
    {
        Float n1 = Float(firstCounts[i])*mul1;
        Float n2 = Float(secondCounts[i])*mul2;
        dist += std::abs((n1+n2)*std::log2(n1+n2));
    }
    return dist;
}

template <typename It>
std::vector<U64> freqs(It begin, It end, U16 maxValue)
{
    std::vector<U64> freqs(maxValue+1, 0);
    while (begin != end)
    {
        auto curr = static_cast<U16>(*begin++);
        if (curr <= maxValue) ++freqs[curr];
    }
    return freqs;
}

// Moves 'at' 'length' steps if possible, otherwise to 'end' while updating
// frequencies: 'beforeFreqs' are assumed to hold all frequencies which we move
// to, while 'afterFreqs' contains the frequencies we have moved away from.
template <typename It>
U64 moveItFreqs(It& at, It end, U64 length,
                std::vector<U64>& beforeFreqs, std::vector<U64>& afterFreqs)
{
    if (at == end) return 0;
    for (U64 i = 0; i < length; ++i)
    {
        auto curr = *at++;
        if (curr < beforeFreqs.size())
        {
            --afterFreqs[curr];
            ++beforeFreqs[curr];
        }
        if (at == end) return i;
    }
    return length;
}

// Finds the best splitting point between 'begin' and 'end', if such one exists.
// If the distance from a point to one of the endpoints is less than minDist, it
// is not valid. The distance between the frequencies for a given point must be
// greater than (or equal to) 'threshold' if it is to be considered valid.
// If no valid splitting point is found, 'end' is returned.
// Note that in all cases it is assumed that 'beforeFreqs' initially contains
// the frequencies of all elements from 'begin' to 'end', and after this has
// executed, 'beforeFreqs' will contain the frequencies in the interval
// [begin, rtn), while 'afterFreqs' will do that for the interval (rtn, end),
// where 'rtn' is the returned iterator, i.e. 'rtn' is removed from both of the
// frequencies. If 'end' is returned, the only guarantee is that
// beforeFreqs[i]+moreFreqs[i] is equal to the original value of beforeFreqs[i]
// for all i.
template <typename It>
It findBestSplit(It begin, It end, std::vector<U64>& beforeFreqs,
                 std::vector<U64>& afterFreqs, U64 beginDist, U64 endDist,
                 U64 sampleDist, float threshold)
{
    //testInvariant(beforeFreqs, afterFreqs, begin, end);
    It at = begin;
    float minVal = std::numeric_limits<float>::max();
    float maxVal = -minVal;
    It bestDist = end;
    if (begin == end) return end;
    if (moveItFreqs(at, end, beginDist, beforeFreqs, afterFreqs) < beginDist)
    {
        return end;
    }
    while (at != end)
    {
        //testInvariant(beforeFreqs, afterFreqs, begin, end);
        auto dist = attenuatedDistance<float>(beforeFreqs, afterFreqs,
                                              std::distance(begin, at),
                                              std::distance(at, end));
        minVal = std::min(minVal, dist);
        if (maxVal < dist && std::distance(begin, end) >= (S64)endDist)
        {
            //std::cerr << "FOUND SOMETHING!!!\n";
            //std::cerr << "  " << (void*)bestDist << ":" << (void*)end << "\n";
            //std::cerr << "  Best went from " << maxVal << " to " << dist << "\n";
            maxVal = dist;
            bestDist = at;
            //testInvariant(beforeFreqs, afterFreqs, begin, end);
        }
        moveItFreqs(at, end, sampleDist, beforeFreqs, afterFreqs);
    }

    // We might not have found a splitting position at all.
    if (bestDist == end) return end;
    // Or it might not be sufficiently good.
    if (maxVal - minVal < threshold) return end;
    ///std::cerr << maxVal - minVal << "\n";
    // beforeFreqs contains all frequencies. We need to move those AFTER 'at'
    // to afterFreqs and remove *at from beforeFreqs entirely.
    It move = bestDist;
    while (++move != end)
    {
        if (*move < afterFreqs.size())
        {
            ++afterFreqs[*move];
            --beforeFreqs[*move];
        }
    }
    if (*bestDist < beforeFreqs.size()) --beforeFreqs[*bestDist];
    return bestDist;
}

template <typename It, typename OutIt>
void findSplitPoints(It begin, It end, std::vector<U64>& freqs,
                     U64 beginDist, U64 endDist, U64 maxDepth, U64 sampleDist,
                     float threshold, OutIt outputAt)
{
    auto beDist = 0;
    if (!maxDepth || begin == end
        || (beDist = std::distance(begin, end)) < (S64)(beginDist+endDist))
    {
        return;
    }
    std::vector<U64> moreFreqs(freqs.size(), 0);

    It mid = findBestSplit(begin, end, moreFreqs, freqs, beginDist, endDist,
                           sampleDist, threshold);
    if (mid != end)
    {
        findSplitPoints(begin, mid, moreFreqs, beginDist, endDist, maxDepth-1,
                        sampleDist, threshold, outputAt);
        *outputAt++ = mid;
        findSplitPoints(mid+1, end, freqs, beginDist, endDist, maxDepth-1,
                        sampleDist, threshold, outputAt);
        if (*mid < freqs.size()) ++freqs[*mid];
    }

    // Restore original frequencies.
    for (size_t i = 0; i < freqs.size(); ++i)
    {
        freqs[i] += moreFreqs[i];
    }
}

template <typename It, typename OutIt>
void findSplitPoints(It begin, It end, std::vector<U64>& freqs,
                     U64 minDist, U64 maxDepth, U64 sampleDist,
                     float threshold, OutIt outputAt)
{
    findSplitPoints(begin, end, freqs, minDist, minDist, maxDepth, sampleDist,
                    threshold, outputAt);
}

struct Huffcoding
{
    std::vector<U8> litlentree;
    std::vector<U8> disttree;
    std::vector<U16> elems;
};

constexpr static U16 distcodeoffset = 286;

template <typename It>
Huffcoding huffcodeChunk(std::vector<U16>&& chunk)
{
    auto chunkfreqs = freqs(chunk.begin(), chunk.end(), 315);
    Huffcoding coding;
    chunkfreqs[256] = 1;
    coding.litlentree = generateHufftree(chunkfreqs, 285, 15);
    for (auto l : coding.litlentree)
    {
        if (l > 15) throw std::logic_error("Root cause!");
    }
    for (U16 i = 0; i <= 29; ++i)
    {
        chunkfreqs[i] = chunkfreqs[i+distcodeoffset];
    }
    coding.disttree = generateHufftree(chunkfreqs, 29, 15);
    coding.elems = std::move(chunk);
    coding.elems.push_back(256);
    return coding;
}

template <typename It>
std::vector<Huffcoding> huffcodeChunk(It begin, It end, U16 alphabetMax,
                                      U64 minSplitDist, U64 sampleDist,
                                      float threshold = 2.5f, U64 level = 5)
{
    std::vector<It> splits;
    auto allfreqs = freqs(begin, end, alphabetMax);
    findSplitPoints(begin, end, allfreqs, minSplitDist, level,
                    sampleDist, threshold, std::back_inserter(splits));
    It prevSplit = begin;
    It split = begin;
    size_t nextSplit = 0;
    std::vector<Huffcoding> segments;
    do
    {
        prevSplit = split;
        if (nextSplit < splits.size())
        {
            split = splits[nextSplit++];
            // Prevent split from being in the middle of a (length,dist)-code.
            while (split != end && *split > 285)
            {
                ++split;
            }
        }
        else split = end;

        // Note: Frequencies recomputed. Possibly move Huffman table generation
        // to split point generation since the frequencies are found up there.
        segments.push_back({});
        auto chunkfreqs = freqs(prevSplit, split, 315);
        chunkfreqs[256] = 1;
        segments.back().litlentree = generateHufftree(chunkfreqs, 285, 15);
        for (auto l : segments.back().litlentree)
        {
            if (l > 15) throw std::logic_error("Root cause!");
        }
        for (U16 i = 0; i <= 29; ++i)
        {
            chunkfreqs[i] = chunkfreqs[i+distcodeoffset];
        }
        segments.back().disttree = generateHufftree(chunkfreqs, 29, 15);
        while (begin != split)
        {
            segments.back().elems.push_back(*begin++);
        }
        segments.back().elems.push_back(256);

    } while (split != end);
    return segments;
}

// Stores any 15-bit number indexed by a 24-bit number. Note that only a single
// element can be indexed by a single key, and that there is a hard limit of
// 2^15 = 32768 possible keys that may be stored.
// All operations (except for construction) runs in O(1) expected time.
class ThreeByteMap
{
public:
    // For this to be an actual hash table and for the expected runtime to hold,
    // this *must* be initialised with two random 24-bit numbers.
    ThreeByteMap(U32 randA = 0xafde25, U32 randB = 0xa0beef);

    // Obviously this inserts 'value' so that it can be accessed by 'key'.
    // However, note that if 'key' already exists, the value stored under that
    // key will simply be overwritten with the given one.
    void insert(U32 key, U16 value);

    // Finds a value for a given key. If no value is found, 0xffff is returned.
    [[nodiscard]] U16 find(U32 key) const;
    U16 operator[](U32 key) const { return find(key); }

    void remove(U32 key);

    U32 size()
    {
        return m_size;
    }


private:
    struct ExtraNode
    {
        U32 key; // Is equal to 0xffffffff if empty.
        U16 value; // Is equal to 0xffff if empty.
        U16 next; // Is equal to the index of the next free element if empty,
                  // but to next element of the chain if non-empty.
                  // A value of 0xffff denotes that the chain ends if non-empty,
                  // but if empty it means that this is the final free element.
    };
    static_assert(sizeof(ExtraNode) == 8, "ExtraList has padding.");
    static_assert(sizeof(ExtraNode[2]) == 16, "ExtraList not tightly packed.");
    // Hashes integers from [0, 2^24) uniformly (i.e. with equal probability)
    // into the integers in [0, 2^15).
    [[nodiscard]] U16 hash(U32 value) const
    {
        return (((U64)m_a*(U64)value+(U64)m_b) & 0xffffff) >> 9;
    }
    [[nodiscard]] bool storesElement(U16 bucket) const
    {
        return m_buckets[bucket];
    }
    [[nodiscard]] U32 getKey(U16 bucket) const
    {
        return m_buckets[bucket] >> 24;
    }
    [[nodiscard]] U16 getValue(U16 bucket) const
    {
        return m_buckets[bucket] & 0xffff;
    }
    [[nodiscard]] bool isChain(U16 bucket) const
    {
        return m_buckets[bucket] & 0x8000;
    }
    [[nodiscard]] U16 chainStart(U16 bucket) const
    {
        return m_buckets[bucket] >> 16;
    }
    void setChainStart(U16 bucket, U16 begin)
    {
        m_buckets[bucket] = 0xffff | (begin << 16);
    }

    void insertInChain(U32 key, U16 value);
    void removeFromChain(U16 idx)
    {
        m_extras[idx] = {0xffffffff, 0xffff, m_nextfree};
        m_nextfree = idx;
    }

    U16 newChain()
    {
        U16 pos = m_nextfree;
        if (pos > 0x7fff) throw std::logic_error("Too many elements in table.");
        m_nextfree = m_extras[m_nextfree].next;
        return pos;
    }

    // Elements packed in buckets as (where bit #0 is least-significant):
    // Bits 0-15: Value
    // Bit 16: Is the current bucket occupied?
    // Bits 24-47: Key
    // If the stored value has bit #15 set (i.e. is not a 15-bit value), then
    // it signifies that multiple elements have hashed to this particular value
    // and are therefore stored in the extra array. Note that in this case, no
    // key is stored in the upper bits, instead in bits 16-30 the index into the
    // extra array is stored.
    std::vector<U64> m_buckets;
    std::vector<ExtraNode> m_extras;
    U32 m_size;
    U32 m_a;
    U32 m_b;
    U16 m_nextfree;
};

ThreeByteMap::ThreeByteMap(U32 randA, U32 randB)
{
    m_buckets.resize(32768);
    m_extras.resize(32768);
    for (U16 i = 0; i < 32768; ++i)
    {
        m_extras[i] = {0xffffffff, 0xffff, (U16)(i+1)};
    }
    m_extras.back().next = 0xffff;
    // 'a' must be a random 24-bit odd number, so we ensure this here, as other-
    // wise this would not be a universal hash function.
    m_a = (randA|1) & 0xffffff;
    m_b = randB & 0xffffff;
    m_nextfree = 0;
    m_size = 0;
}

void ThreeByteMap::insert(U32 key, U16 value)
{
    if (find(key) == 0xffff) ++m_size;
    auto idx = hash(key);
    if (!isChain(idx))
    {
        if (!storesElement(idx) || getKey(idx) == key)
        {
            m_buckets[idx] = ((U64)key << 24) | (U64)value | ((U64)1 << 16);
        }
        else
        {
            // We first have to move the old element out of the main array and
            // into the extra array.
            auto insertAt = newChain();
            m_extras[insertAt].key = getKey(idx);
            m_extras[insertAt].value = getValue(idx);
            m_extras[insertAt].next = 0xffff;
            setChainStart(idx, insertAt);

            // Then we can insert our new element.
            insertInChain(key, value);
        }
    }
    else
    {
        auto at = chainStart(idx);
        while (at != 0xffff)
        {
            if (m_extras[at].key == key)
            {
                m_extras[at].value = value;
                return;
            }
            at = m_extras[at].next;
        }
        insertInChain(key, value);
    }
}

void ThreeByteMap::insertInChain(U32 key, U16 value)
{
    auto idx = hash(key);
    auto next = chainStart(idx);
    auto insertAt = newChain();
    m_extras[insertAt].key = key;
    m_extras[insertAt].value = value;
    m_extras[insertAt].next = next;
    setChainStart(idx, insertAt);
}

U16 ThreeByteMap::find(U32 key) const
{
    auto idx = hash(key);
    if (!storesElement(idx)) return 0xffff;
    if (!isChain(idx) && key == getKey(idx)) return getValue(idx);
    if (isChain(idx))
    {
        auto at = chainStart(idx);
        while (at != 0xffff)
        {
            if (m_extras[at].key == key) return m_extras[at].value;
            at = m_extras[at].next;
        }
    }
    return 0xffff;
}

void ThreeByteMap::remove(U32 key)
{
    --m_size;
    auto idx = hash(key);
    if (!storesElement(idx)) return;
    if (!isChain(idx) && key == getKey(idx)) m_buckets[idx] = 0;
    else if (isChain(idx))
    {
        auto prev = 0xffff;
        auto at = chainStart(idx);
        while (at != 0xffff)
        {
            auto next = m_extras[at].next;
            if (m_extras[at].key == key)
            {
                if (prev == 0xffff)
                {
                    // In this case, we try to delete the initial chain element.
                    if (next == 0xffff) m_buckets[idx] = 0;
                    else setChainStart(idx, next);
                }
                else
                {
                    // We are somewhere not in the beginning of the chain.
                    // Simply remove all references to the current element.
                    m_extras[prev].next = next;
                }
                removeFromChain(at);
                return;
            }
            prev = at;
            at = m_extras[at].next;
        }
    }
}

// This takes any stream of bytes and turns it into a stream of symbols for use
// with DeflateImpl compression. Note that a value x in the stream signifies the
// following:
// 0 <= x <= 255 means it is a literal.
// 257 <= x <= 285 means it is a length code, and the following one to three
//     bytes are special. Their values have the following meaning:
//     * If there are extra length bits, they are stored in the next byte.
//     * Then, a distance code is stored.
//     * If the distance code has extra bits, they follow the distance code.
//     The way to interpret this is that we have the following byte stream:
//     ...[length code](length bits)[distance code](distance bits)...
//     Here, square brackets means the byte is always present, while ordinary
//     parentheses signify that it might or might not be present, depending upon
//     the values of the preceding bytes.
//     It is *VERY* important to note that the extra bits are NOT stored
//     directly, but 1024 is added to the length bits and 2048 to the distance
//     bits. Furthermore, 286 is added to the distance code. This gives the
//     following properties which might be useful for debugging and/or
//     synchronising a stream:
// 286 <= x <= 315 means that it is 286 + a distance code.
// 1024 <= x <= 1282 means it is 1024 + the extra bits for a length code.
// 2048 <= x <= 34816 means it is 2048 + the extra bits for a distance code.
// No other values will be produced on purpose, and as such any other value than
// those mentioned above should be treated as an error, likely due to a bug in
// the code or corruption of the stream.

template <typename OutIt>
class Compressor
{
public:
    Compressor(OutIt output, U8* initialWindowBegin, U8* initialWindowEnd);
    Compressor(OutIt output)
        : Compressor(output, nullptr, nullptr) {}

    void clear(OutIt output, U8* initialWindowBegin, U8* initialWindowEnd);
    void clear(OutIt output)
    {
        clear(output, nullptr, nullptr);
    }

    void append(U8 elem, bool final = false);
    void flush();

    void setOutput(OutIt outAt) { m_output = outAt; }
    [[nodiscard]] OutIt output() const { return m_output; }

private:
    struct MatchInfo
    {
        U16 offset;
        U16 length;
        U16 backdist; // Distance from offset.
        // I.e. if we have ABCDEFABCDEFABCDEFABCDEF
        // where              ^     ^       ^
        //                match  input    input+offset
        // then backdist =    |-------------|
    };
    struct LengthSymbol
    {
        U16 symbol;
        U16 extraBits;
        U16 extraData;
    };
    struct BackdistSymbol
    {
        U16 symbol;
        U16 extraBits;
        U16 extraData;
    };
    // So i.e. MatchInfo{10, 5, 13} means that from the current position in the
    // input stream, if we go 10 bytes forward, then the next 5 bytes from there
    // are equal to the five bytes we get if we instead go 3 bytes back from the
    // current position in the input stream.

    // This contains two streams which has copies of the input data.
    // Assume the input data is some stream:
    // ======================================================================
    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                    ^^^^^^^^^^^^^^^^^^
    //   Processed data which is now   ^^^^^^^^^^^^^^^^^^^^  Data which has
    //       stored in window.         Data which is curr-    not yet been
    //                                 ently stored in the       read.
    //                                    input buffer.

    // Note that to minimise space usage, all buffers are circular.

    // The input buffer contains up to 511 elements.
    // Note that m_lastProcessed contains the position of the first element,
    // while m_inputAt is one element past the end of the input buffer.
    // If these are equal, no elements are stored in that buffer.

    // This returns the offset'th element of the input buffer (i.e. unprocessed
    // input data).
    [[nodiscard]] U8 peekInput(U16 offset) const
    {
        return m_inputbuf[(m_lastProcessed + offset)&0x1ff];
    }
    // This returns how many unprocessed input elements there are in the first
    // buffer.
    [[nodiscard]] U16 inputCount() const
    {
        return ((m_inputAt + 512) - m_lastProcessed) & 0x1ff;
    }
    // This consumes 'count' bytes from the input buffer, and then moves the
    // window 'count' bytes forward.
    void advanceProcessed(U16 count = 1);

    // This adds a sinle byte to the end of the input buffer (i.e. increases
    // inputCount() by 1).
    void appendInput(U8 byte)
    {
        m_inputbuf[m_inputAt++] = byte;
        m_inputAt &= 0x1ff;
    }

    // The window contains the last 32768 elements processed (so there is no
    // overlap between the window and the input buffer).
    // m_windowAt refers to the oldest element to the window / one past the
    // newest element.
    // An important thing to note is that *if* m_wrapped is true, then all of
    // the window's elements are ones that have been seen before, but if
    // m_wrapped is false, then only elements 0...m_windowAt-1 have been seen,
    // while the other ones are uninitialised.

    // This returns the element 'backdist' places back, i.e. if backdist = 1,
    // this réturns the latest seen element, backdist = 10 gives the 10th latest
    // element, while backdist=32768 gives the oldest element in the window
    // (assuming m_wrapped=true). Note that 1 <= backdist <= 32768 if m_wrapped
    // is true, otherwise it is required that 1 <= backdist <= m_windowAt.
    [[nodiscard]] U8 peekWindow(U16 backdist) const
    {
        return m_window[((m_windowAt + 32768) - backdist)&0x7fff];
    }

    // This takes a backdist and extracts three bytes from the window. Note that
    // it takes the distance to the newest of these three bytes, i.e. if one
    // wanted to extract the latest 3 bytes, one would have to call extract3B(1)
    // and not with 3.
    [[nodiscard]] U32 extract3B(U16 backdist) const
    {
        return (((U32)peekWindow(backdist+2) << 16)
               |((U32)peekWindow(backdist+1) << 8)
               | (U32)peekWindow(backdist));
    }

    // This computes the backwards distance from the current window position and
    // back to 'pos'.
    [[nodiscard]] U16 computeBackdist(U16 pos) const
    {
        return ((m_windowAt + 32768) - pos) & 0x7fff;
    }

    void appendOutput(U16 symbol)
    {
        /*
        static U16 s0 = 0;
        static U16 s1 = 0;
        static U16 s2 = 0;

        if (265 <= s0 && s0 < 285
            && 286+4 <= s2 && symbol < 2048)
        {
            throw std::logic_error("Bad [len,len-extra,dist,dist-extra].");
        }
        if (symbol >= 2048 && !(286+4 <= s2 && s2 <= 286+29))
        {
            throw std::logic_error("Preceding code is NOT a distance code!");
        }
        if (265 <= s1 && s1 < 285 && !(286 <= symbol && symbol <= 286+29))
        {
            throw std::logic_error("Bad again!");
        }*/
        if ((315 < symbol && symbol < 1024)
            || (1282 < symbol && symbol < 2048)
            || (34816 < symbol) || symbol == 256)
        {
            throw std::logic_error("Compressor produced invalid symbol.");
        }
        *m_output++ = symbol;

        /*
        s0 = s1;
        s1 = s2;
        s2 = symbol;*/
    }

    S32 approxValue(MatchInfo info);

    // This moves the window one byte forward by placing 'byte' as the next
    // element.
    void advanceWindow(U8 byte);

    // If startInWindow: backdist refers to the number of elements to move back
    // in window to start checking match from.
    // If !startInWindow: backdist refers to the same, but in input buffer
    // instead.
    MatchInfo matchlen(U16 inputOffset, U16 backdist, bool startInWindow);
    MatchInfo findMatch(U16 inputOffset);
    void appendMatch(MatchInfo info);

    LengthSymbol getLengthSymbol(U16 length)
    {
        U16 code = 257;
        U16 len = length - 3;
        U16 base = 0;
        U16 extraBits = 0;
        if (len < 8)
        {
            base = length;
            code += len;
        }
        else if (len == 255)
        {
            base = length;
            code = 285;
        }
        else
        {
            auto il = ilog2(len);
            auto diff = (len - (1<<il)) >> (il-2);
            extraBits = il-2;
            base = 3+(1<<il)+(diff<<extraBits);
            code = 265+4*(il-3)+diff;
        }
        return {code, extraBits, (U16)(length - base)};
    }
    BackdistSymbol getBackdistSymbol(U16 backdist)
    {
        /*if (backdist == 177)
        {
            std::cout << "BOOP\n";
        }*/
        U16 code = 0;
        U16 base = 0;
        U16 extraBits = 0;
        if (backdist <= 4)
        {
            code = backdist - 1;
            base = backdist;
        }
        else
        {
            auto d = backdist - 1;
            auto il = ilog2(d);
            auto delta = d >= (1 << (il-1)) + (1 << il);
            code = 4+(il-2)*2 + delta;
            base = (1 << il) + delta*(1 << (il-1)) + 1;
            extraBits = il-1;
            /*
            auto rest = d - (1 << (il-1));
            extraBits = il - 1;
            code = 4+(1<<(il-2))+(rest>>il);
            base = 1+(1<<il)+((rest>>il)<<extraBits);*/
        }

        if (code > 200)
        {
            throw std::logic_error("Bad backdist generated.");
        }

        return {code, extraBits, (U16)(backdist - base)};
    }

    MatchInfo m_cachedMatch;
    ThreeByteMap m_lastseen;
    std::vector<U8> m_inputbuf;
    std::vector<U16> m_window;
    std::vector<U16> m_prevlist;
    OutIt m_output;
    size_t m_minViableLength;
    size_t m_lastProcessed;
    size_t m_inputAt;
    size_t m_windowAt; // Points to the position where we have to insert the
                       // next element.

    bool m_deleteOld;
    bool m_wrapped;
};

template <typename OutIt>
void Compressor<OutIt>::advanceProcessed(U16 count)
{
    for (size_t i = 0; i < count; ++i)
    {
        advanceWindow(peekInput(i));
    }
    m_lastProcessed += count;
    m_lastProcessed &= 0x1ff;
}

template <typename OutIt>
void Compressor<OutIt>::advanceWindow(U8 byte)
{
    if (m_wrapped && !m_windowAt) m_deleteOld = true;
    if (m_windowAt == 0x7fff) m_wrapped = true;
    if (m_deleteOld)
    {
        U32 key = extract3B(32766);
        auto seen = m_lastseen[key];
        if (seen == 0xffff)
        {
            throw std::logic_error("3-byte sequence not in hash table.");
        }
        if (seen == m_windowAt) m_lastseen.remove(key);
    }
    m_window[m_windowAt++] = byte;
    m_windowAt &= 0x7fff;
    if (m_wrapped || m_windowAt >= 3)
    {
        U32 key = extract3B(1);
        U16 beginIdx = (m_windowAt + 32768 - 3) & 0x7fff;
        auto lastIdx = m_lastseen[key];
        m_lastseen.insert(key, beginIdx);
        if (lastIdx != 0xffff) m_prevlist[beginIdx] = lastIdx;
    }
}

template <typename OutIt>
void Compressor<OutIt>::clear(OutIt output, U8* begin, U8* end)
{
    m_output = output;
    m_inputbuf.clear();
    m_window.clear();
    m_prevlist.clear();
    m_inputbuf.resize(512, 0);
    m_window.resize(32768, 0);
    m_prevlist.resize(32768, 0);
    m_lastProcessed = 0;
    m_inputAt = 0;
    m_windowAt = 0;
    if (begin < end)
    {
        if (end - begin > 32768) begin = end - 32768;
        while (begin != end)
        {
            m_window[m_windowAt++] = *begin++;
        }
    }
    m_minViableLength = 258;
    m_deleteOld = false;
    m_wrapped = false;
    m_cachedMatch = {0, 0, 0};
}

template <typename OutIt>
Compressor<OutIt>::Compressor(OutIt output, U8* begin, U8* end)
    : m_output{output}
{
    clear(output, begin, end);
}

template <typename OutIt>
typename Compressor<OutIt>::MatchInfo
Compressor<OutIt>::matchlen(U16 inputOffset, U16 backdist, bool startInWindow)
{
    MatchInfo info = {0, 0, 0};
    if (!m_wrapped && backdist > m_windowAt) return info;
    if (startInWindow) info.backdist = inputOffset + backdist;
    else info.backdist = backdist;
    if (info.backdist > 32768) return {0, 0, 0};
    info.offset = inputOffset;
    U16 lookAt = inputOffset - backdist;
    if (startInWindow)
    {
        while (backdist && inputOffset < inputCount()
               && peekWindow(backdist) == peekInput(inputOffset))
        {
            ++info.length;
            ++inputOffset;
            --backdist;
        }
        if (backdist && inputOffset < inputCount())
        {
            // The match ended somewhere.
            return info;
        }
        // Fallthrough to case where it is in the input buffer; so far there's a
        // match where we have recorded the length -- now we just need to see
        // how far that match goes.
        //backdist = inputOffset;
        lookAt = 0;
    }
    while (inputOffset < inputCount()
           && peekInput(lookAt) == peekInput(inputOffset))
    {
        ++info.length;
        ++inputOffset;
        ++lookAt;
    }
    return info;
}

template <typename OutIt>
typename Compressor<OutIt>::MatchInfo
Compressor<OutIt>::findMatch(U16 inputOffset/*, U16 maxTries*/)
{
    MatchInfo info = {0, 0, 0};
    if (inputCount() < 3 + inputOffset) return info;
    U32 initial = peekInput(inputOffset);
    initial = (initial << 8) | peekInput(inputOffset+1);
    initial = (initial << 8) | peekInput(inputOffset+2);
    U16 lastAt = m_lastseen[initial];
    // Try scanning from [inputPos-2, inputPos+inputOffset) to check if there
    // are any repetitions here. If so, add these to potential list of repeats.
    auto match = matchlen(inputOffset, 2, true);
    ///if (match.length >= info.length) info = match;
    if (approxValue(match) >= approxValue(info)) info = match;
    match = matchlen(inputOffset, 1, true);
    ///if (match.length >= info.length) info = match;
    if (approxValue(match) >= approxValue(info)) info = match;

    for (U16 i = 0; i < inputOffset; ++i)
    {
        match = matchlen(inputOffset, inputOffset-i, false);
        ///if (match.length >= info.length) info = match;
        if (approxValue(match) >= approxValue(info)) info = match;
    }
    // Now we have tested all of the matches not tracked by the window. Now we
    // check the window (and it is extremely quick to check if there is a match,
    // as we can look it up).
    if (lastAt != 0xffff)
    {
        auto backdist = computeBackdist(lastAt);
        if (!backdist) backdist = 32768;
        if (backdist < 3)
        {
            throw std::logic_error("Back distance cannot be 1 or 2.");
        }
        while (initial == extract3B(backdist-2))
        {
            match = matchlen(inputOffset, backdist, true);
            if (approxValue(match) >= approxValue(info)) info = match;
            auto newBackdist = computeBackdist(lastAt = m_lastseen[lastAt]);
            if (!newBackdist) newBackdist = 32768;
            if (newBackdist <= backdist || newBackdist + inputOffset >= 32768)
            {
                break;
            }
            backdist = newBackdist;
            if (backdist < 3)
            {
                throw std::logic_error("Back distance cannot be 1 or 2.");
            }
        }
    }
    if (info.backdist == 0 && info.length)
    {
        throw std::logic_error("Inconsistent state!");
    }

    if (approxValue(info) <= 0)
    {
        info.length = 0;
        info.backdist = 0;
    }
    return info;
}

template <typename OutIt>
void Compressor<OutIt>::appendMatch(MatchInfo info)
{
    for (U16 i = 0; i < info.offset; ++i)
    {
        appendOutput(peekInput(i));
    }
    advanceProcessed(info.offset);
    if (info.length >= 3)
    {
        auto len = getLengthSymbol(info.length);
        auto backdist = getBackdistSymbol(info.backdist);

        appendOutput(len.symbol);
        if (len.extraBits) appendOutput(len.extraData + 1024);

        appendOutput(backdist.symbol + 286);
        if (backdist.symbol < 0)
        {
            throw std::runtime_error("Badness 10000!");
        }
        if (backdist.extraBits) appendOutput(backdist.extraData + 2048);

        advanceProcessed(info.length);
    }
}

template <typename OutIt>
S32 Compressor<OutIt>::approxValue(MatchInfo info)
{
    if (!info.length) return 0;
    S32 lenbits = (S32)getLengthSymbol(info.length).extraBits;
    S32 distbits = (S32)getBackdistSymbol(info.backdist).extraBits;
    return (S32)info.length*8 - (lenbits + distbits) - 9;
}

template <typename OutIt>
void Compressor<OutIt>::append(U8 elem, bool final)
{
    appendInput(elem);
    if (m_cachedMatch.length)
    {
        if (!m_cachedMatch.offset)
        {
            throw std::logic_error("Cached match should NOT have zero offset.");
            m_cachedMatch = {0, 0, 0}; // Simply drop it then.
        }
        --m_cachedMatch.offset; // Since we have appended one input element.
    }
    while (inputCount() >= m_minViableLength || (final && inputCount()))
    {
        auto match = m_cachedMatch.length ? m_cachedMatch : findMatch(0);
        m_cachedMatch = {0, 0, 0};
        if (match.length >= 3)
        {
            MatchInfo matchN{0, 0, 0};
            if (match.length >= 4)
            {
                matchN = findMatch(match.length - 1);
                if (matchN.length == 3)
                {
                    --match.length;
                }
                if (match.length == 3)
                {
                    appendMatch(match);

                    // Make matchN valid after having appended another match.
                    matchN.offset -= match.length + match.offset;
                    m_cachedMatch = matchN;
                    continue;
                }
                // In this case, matchN will just start one byte further.
                if (matchN.length != 3) matchN = {0, 0, 0};
            }
            auto match1 = findMatch(1);
            auto val0 = approxValue(match);
            auto val1 = approxValue(match1);
            // Determine whether to start in the next byte or in the current.
            if (val1-8 >= val0)
            {
                // Either we can squeeze match1 in before matchN or we have to
                // preserve the original match (assuming matchN exists).
                if (matchN.length && matchN.offset == 3)
                {
                    // Decide now whether to choose val0 and matchN or just
                    // match1.
                    auto valN = approxValue(matchN);
                    if (val1-8 >= val0 + valN)
                    {
                        appendMatch(match1);
                        continue;
                    }
                    appendMatch(match);

                    // Make matchN valid after.
                    matchN.offset -= match.length + match.offset;
                    m_cachedMatch = matchN;
                    continue;
                }
                appendMatch(match1);
            }
            else
            {
                appendMatch(match);
            }
        }
        else
        {
            appendOutput(peekInput(0));
            advanceProcessed(1);
        }
    }
    if (!m_cachedMatch.offset) m_cachedMatch = {0, 0, 0};
    if (m_cachedMatch.length && final)
    {
        appendMatch(m_cachedMatch);
    }
}

template <typename OutIt>
void Compressor<OutIt>::flush()
{
    if (!inputCount()) return;
    auto last = peekInput(inputCount()-1);
    m_inputAt = (m_inputAt + 511) & 0x1ff;
    append(last, true);
}

U16 reverseBinary(U16 val, U8 bits)
{
    U16 nval = 0;
    while (bits)
    {
        nval <<= 1;
        nval |= val & 1;
        val >>= 1;
        --bits;
    }
    return nval;
}

Huffcode reverseCode(Huffcode code)
{
    Huffcode nc;
    nc.bitlen = code.bitlen;
    nc.code = reverseBinary(code.code, code.bitlen);
    return nc;
}

std::vector<Huffcode> toHuffcodes(const std::vector<U8>& bitlens)
{
    std::vector<U8> bitcnt(16, 0);
    U8 maxbitlen = 0;
    for (auto& len : bitlens)
    {
        ++bitcnt.at(len);
        maxbitlen = std::max(maxbitlen, len);
    }
    bitcnt[0] = 0;

    U16 startcode = 0;
    std::vector<U16> nextcode(maxbitlen+1);
    for (U8 bits = 1; bits <= maxbitlen; ++bits)
    {
        startcode = static_cast<U16>((startcode+bitcnt[bits-1]) << 1);
        nextcode[bits] = startcode;
    }
    std::vector<Huffcode> codes(bitlens.size(), {0, 0});
    for (U32 i = 0; i < bitlens.size(); ++i)
    {
        codes[i].code = nextcode[bitlens[i]]++;
        codes[i].bitlen = bitlens[i];
        codes[i] = reverseCode(codes[i]);
    }
    return codes;
}


template <typename OutIt>
class Encoder
{
public:
    Encoder(OutIt output);
    void encode(const Huffcoding& coding, U64& correspondingBytes,
                const U8* origDataBegin, const U8* origDataEnd,
                U32 adler32, bool isFinal);

    void clear(OutIt output);

private:
    enum class State
    {
        StreamStart, BlockBegin, InCompressedBlock, InUncompressedBlock
    };

    void outputBitsLSB(U16 value, U8 bits);
    void synchroniseToByteBoundary();
    //void outputBitsMSB(U16 value, U8 bits);
    void outputHuffcode(Huffcode code)
    {
        if (!code.bitlen)
        {
            throw std::logic_error("Cannot output empty code.");
        }
        outputBitsLSB(code.code, code.bitlen);
    }
    // Todo: Support dicitionaries, compression level setting etc.
    void outputStreamHeader();
    void handleBlockBegin(const std::vector<U8>& litlens,
                          const std::vector<U8>& dists, bool isFinal);
    void encodeBlock(const Huffcoding& coding, U64& corrBytes);
    void outputTreeEncoderTreeSymbol(U8& sym, U16& count, std::vector<U8>& out);

    [[nodiscard]] bool isCompressible(const Huffcoding& coding) const;
    [[nodiscard]] U64 countBytes(const Huffcoding& coding) const;
    void outputUncompressed(const Huffcoding& coding, const U8* uncompBegin,
                            const U8* uncompEnd, U64& corrBytes,
                            bool final);
    void outputUncompressed(const U8* data, U16 count, bool final);

    std::vector<Huffcode> m_litlenhuff;
    std::vector<Huffcode> m_disthuff;
    OutIt m_output;
    State m_state;
    U8 m_currbyte;
    U8 m_cbat;
};

template <typename OutIt>
void Encoder<OutIt>::clear(OutIt output)
{
    m_litlenhuff.clear();
    m_disthuff.clear();
    m_output = output;
    m_state = State::StreamStart;
    m_currbyte = 0;
    m_cbat = 0;
}

template <typename OutIt>
void Encoder<OutIt>::outputBitsLSB(U16 value, U8 bits)
{
    if (!bits || value >= (1 << bits))
    {
        throw std::logic_error("Bad output value!");
    }
    U8 lowermask[9] = { 0, 1, 3, 7, 0xf, 0x1f, 0x3f, 0x7f, 0xff };
    U16 valueLeft = value;
    U8 bitsLeft = bits;
    while (bitsLeft)
    {
        U8 toRead = 8 - m_cbat;
        if (toRead > bitsLeft) toRead = bitsLeft;
        U8 toApp = valueLeft & lowermask[toRead];
        m_currbyte |= toApp << m_cbat;
        valueLeft >>= toRead;
        bitsLeft -= toRead;
        m_cbat += toRead;
        if (m_cbat == 8)
        {
            *m_output++ = m_currbyte;
            m_currbyte = 0;
            m_cbat = 0;
        }
    }
}

template <typename OutIt>
void Encoder<OutIt>::synchroniseToByteBoundary()
{
    if (!m_cbat) return;
    *m_output++ = m_currbyte;
    m_cbat = 0;
    m_currbyte = 0;
}

template <typename OutIt>
Encoder<OutIt>::Encoder(OutIt output)
    : m_output{output}
{
    m_state = State::StreamStart;
    m_currbyte = 0;
    m_cbat = 0;
}

template <typename OutIt>
bool Encoder<OutIt>::isCompressible(const Huffcoding& coding) const
{
    U16 lowCodes = 0;
    U16 midCodes = 0;
    U16 highCodes = 0;
    U64 repetitions = 0;
    for (U16 i = 0; i <= 256; ++i)
    {
        U8 clen = coding.litlentree[i];
        if (clen <= 6) return true;
        else if (clen == 7) ++lowCodes;
        else if (clen == 8) ++midCodes;
        else if (clen == 9) ++highCodes;
        //else return true;
    }
    if (lowCodes >= highCodes + 50 || highCodes >= lowCodes + 50)
    {
        return true;
    }
    U64 totalSym = 0;
    // The below is just an estimate of the number of repeated bytes.
    for (U16 code : coding.elems)
    {
        if (286 <= code && code <= 314) ++repetitions;
        if (code == 315) repetitions += 258;
        if (1024 <= code && code <= 1282) repetitions += code - 1024;
        ++totalSym;
    }
    if ((totalSym <= 500 && repetitions >= 50)) return true;
    if (25 * repetitions > totalSym) return true;
    return false;
}

template <typename OutIt>
void Encoder<OutIt>::outputUncompressed(const U8* data, U16 count, bool final)
{
    outputBitsLSB(final, 1);
    outputBitsLSB(0, 2);
    synchroniseToByteBoundary();
    U16 nlen = ~count;
    *m_output++ = count&0xff;
    *m_output++ = count>>8;
    *m_output++ = nlen&0xff;
    *m_output++ = nlen>>8;

    while (count)
    {
        *m_output++ = *data++;
        --count;
    }
}

template <typename OutIt>
U64 Encoder<OutIt>::countBytes(const Huffcoding& coding) const
{
    constexpr U16 lenBase[29] =
    {
        3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31,
        35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258
    };
    constexpr U8 lenExtraBits[29] =
    {
        0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3,
        4, 4, 4, 4, 5, 5, 5, 5, 0
    };

    U64 count = 0;

    for (size_t i = 0; i < coding.elems.size(); ++i)
    {
        auto elem = coding.elems[i];
        if (elem < 256)
        {
            ++count;
        }
        else if (elem == 256)
        {
            if (i+1 != coding.elems.size())
            {
                throw std::logic_error("Bug: Non-terminal 256 appeared.");
            }
            break;
        }
        else if (elem <= 285)
        {
            U16 lencode = elem - 257;
            if (i+1 == coding.elems.size() || lencode > 29)
            {
                throw std::logic_error("Bad length code.");
            }
            U16 len = lenBase[lencode];
            if (lenExtraBits[lencode])
            {
                len += coding.elems[i+1] - 1024;
                ++i;
            }
            count += len;
        }
    }
    return count;
}

template <typename OutIt>
void Encoder<OutIt>::outputUncompressed(const Huffcoding& coding,
                                        const U8* begin,
                                        const U8* end,
                                        U64& corrBytes, bool final)
{

    U64 byteCount = countBytes(coding);
    corrBytes += byteCount;
    if (end < begin || end - begin < (S64)byteCount)
    {
        throw std::logic_error("Too little input data given.");
    }
    while (byteCount)
    {
        U16 toAppend = byteCount > 0xffff ? 0xffff : (U16)byteCount;
        byteCount -= toAppend;
        outputUncompressed(begin, toAppend, final && !byteCount);
        begin += toAppend;
    }
}

template <typename OutIt>
void Encoder<OutIt>::encode(const Huffcoding& coding, U64& correspondingBytes,
                            const U8* origDataBegin, const U8* origDataEnd,
                            U32 adler32, bool isFinal)
{
    correspondingBytes = 0;
    if (m_state == State::StreamStart)
    {
        outputStreamHeader();
        m_state = State::BlockBegin;
    }
    if (m_state == State::BlockBegin)
    {
        bool compress = isCompressible(coding);
        if (compress)
        {
            handleBlockBegin(coding.litlentree, coding.disttree, isFinal);
            m_state = State::InCompressedBlock;
        }
        else
        {
            m_state = State::InUncompressedBlock;
        }
    }
    if (m_state == State::InCompressedBlock)
    {
        encodeBlock(coding, correspondingBytes);
    }
    else
    {
        outputUncompressed(coding, origDataBegin, origDataEnd,
                           correspondingBytes, isFinal);
    }
    m_state = isFinal ? State::StreamStart : State::BlockBegin;
    if (isFinal)
    {
        synchroniseToByteBoundary();
        outputBitsLSB((adler32 >> 24) & 0xff, 8);
        outputBitsLSB((adler32 >> 16) & 0xff, 8);
        outputBitsLSB((adler32 >> 8) & 0xff, 8);
        outputBitsLSB(adler32 & 0xff, 8);
    }
}

template <typename OutIt>
void Encoder<OutIt>::outputStreamHeader()
{
    U16 cm = 8;
    U16 cinfo = 7;
    U16 fcheck = 0;
    U16 fdict = 0;
    U16 flevel = 3;
    for (U16 i = 0; i < 32; ++i)
    {
        U16 cmf = cm | (cinfo << 4);
        U16 flg = i | (fdict << 5) | (flevel << 6);
        if ((cmf*256+flg) % 31 == 0)
        {
            fcheck = i;
            break;
        }
    }
    outputBitsLSB(cm, 4);
    outputBitsLSB(cinfo, 4);
    outputBitsLSB(fcheck, 5);
    outputBitsLSB(fdict, 1);
    outputBitsLSB(flevel, 2);
}

template <typename OutIt>
void Encoder<OutIt>::outputTreeEncoderTreeSymbol(U8& sym, U16& count,
                                                 std::vector<U8>& out)
{
    if (sym)
    {
        out.push_back(sym);
        --count;
    }
    while (count >= 3)
    {
        if (sym)
        {
            out.push_back(16);
            U8 repeatLen = (U8)std::min<U16>(6, count);
            count -= repeatLen;
            out.push_back(32+repeatLen-3);
        }
        else
        {
            // Output zeroes.
            U8 repeatLen = (U8)std::min<U16>(138, count);
            count -= repeatLen;
            out.push_back((repeatLen >= 11) ? 18 : 17);
            out.push_back(32+repeatLen - ((repeatLen >= 11) ? 11 : 3));
        }
    }
    while (count)
    {
        if (sym > 15) throw std::logic_error("Symbol too large.");
        out.push_back(sym);
        --count;
    }
}

template <typename OutIt>
void Encoder<OutIt>::handleBlockBegin(const std::vector<U8>& litlens,
                                      const std::vector<U8>& dists,
                                      bool isFinal)
{
    outputBitsLSB(isFinal, 1);
    outputBitsLSB(2, 2);
    U16 hlit = 257;
    for (U16 i = 257; i < litlens.size(); ++i)
    {
        if (litlens[i]) hlit = i+1;
    }
    hlit -= 257;
    U16 hdist = 1;
    for (U16 i = 1; i < dists.size(); ++i)
    {
        if (dists[i]) hdist = i+1;
    }
    hdist -= 1;
    std::vector<U8> symOut;

    U8 lastSym = 0;
    U16 lastSymCount = 0;
    for (U16 i = 0; i < hlit + 257; ++i)
    {
        if (lastSymCount && lastSym != litlens[i])
        {
            outputTreeEncoderTreeSymbol(lastSym, lastSymCount, symOut);
        }
        ++lastSymCount;
        lastSym = litlens[i];
    }
    for (U16 i = 0; i < hdist + 1; ++i)
    {
        if (lastSymCount && lastSym != dists[i])
        {
            outputTreeEncoderTreeSymbol(lastSym, lastSymCount, symOut);
        }
        ++lastSymCount;
        lastSym = dists[i];
    }
    outputTreeEncoderTreeSymbol(lastSym, lastSymCount, symOut);

    auto clfreqs = freqs(symOut.begin(), symOut.end(), 19);
    auto clht = toHuffcodes(generateHufftree(clfreqs, 19, 7));
    U8 perm[19] = {16,17,18,0,8,7,9,6,10,5,11,4,12,3,13,2,14,1,15};
    U8 hclen = 4;
    for (U8 i = 4; i < 19; ++i)
    {
        if (clht[perm[i]].bitlen) hclen = i+1;
    }
    hclen -= 4;
    outputBitsLSB(hlit, 5);
    outputBitsLSB(hdist, 5);
    outputBitsLSB(hclen, 4);
    for (U8 i = 0; i < hclen + 4; ++i)
    {
        outputBitsLSB(clht[perm[i]].bitlen, 3);
    }
    for (U16 i = 0; i < symOut.size(); ++i)
    {
        outputHuffcode(clht.at(symOut[i]));
        if (symOut[i] >= 16)
        {
            if (symOut[i] == 16) outputBitsLSB(symOut[i+1]-32, 2);
            if (symOut[i] == 17) outputBitsLSB(symOut[i+1]-32, 3);
            if (symOut[i] == 18) outputBitsLSB(symOut[i+1]-32, 7);
            ++i;
        }
    }
}

template <typename OutIt>
void Encoder<OutIt>::encodeBlock(const Huffcoding& coding,
                                 U64& corrBytes)
{
    std::vector<Huffcode> litlencoder = toHuffcodes(coding.litlentree);
    std::vector<Huffcode> distcoder = toHuffcodes(coding.disttree);
    constexpr U16 lenBase[29] =
    {
        3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31,
        35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258
    };
    constexpr U8 lenExtraBits[29] =
    {
        0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3,
        4, 4, 4, 4, 5, 5, 5, 5, 0
    };
    constexpr U8 distExtraBits[30] =
    {
        0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6,
        7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13
    };
    if (litlencoder.size() <= 256)
    {
        throw std::logic_error("Terminal byte cannot be encoded.");
    }
    for (size_t i = 0; i < coding.elems.size(); ++i)
    {
        auto elem = coding.elems[i];
        if (elem >= litlencoder.size())
        {
            throw std::logic_error("Element in data stream but not in tree.");
        }
        outputHuffcode(litlencoder[elem]);
        if (elem < 256)
        {
            ++corrBytes;
        }
        if (elem == 256)
        {
            if (i+1 != coding.elems.size())
            {
                throw std::logic_error("Premature block end code.");
            }
        }
        if (elem > 256)
        {
            auto lencode = elem-257;
            if (lencode >= 29)
            {
                throw std::logic_error("Length code too large.");
            }
            auto lenbits = lenExtraBits[lencode];
            U16 len = 0;
            size_t offset = 1;

            if (lenbits)
            {
                len = coding.elems.at(i+offset) - 1024;
                outputBitsLSB(len, lenbits);
                ++offset;

                corrBytes += len;
            }

            corrBytes += lenBase[lencode];

            auto distcode = coding.elems.at(i+offset) - 286;
            if (distcode >= 30)
            {
                throw std::logic_error("Distance code too great.");
            }
            if (distcode < 0)
            {
                throw std::logic_error("Distance code illegally small.");
            }
            outputHuffcode(distcoder[distcode]);
            auto distbits = distExtraBits[distcode];
            U16 dist = 0;
            if (distbits)
            {
                ++offset;
                dist = coding.elems.at(i+offset) - 2048;
                outputBitsLSB(dist, distbits);
            }
            i += offset;
        }
    }
    if (!coding.elems.empty() && coding.elems.back() != 256)
    {
        throw std::logic_error("Unterminated block.");
    }
    auto expectedBytes = countBytes(coding);
    if (corrBytes != expectedBytes)
    {
        throw std::logic_error("I cannot count.");
    }
}

class Deflate
{
public:
    enum DeflateStatus : U32
    {
        Good = 0,
        Finished = 1,
        RequiresInput = 2,
        RequiresOutput = 4
    };
    Deflate();

    void setInput(const U8* begin, const U8* end);

    [[nodiscard]] const U8* inputAt() const { return m_inputAt; }
    [[nodiscard]] const U8* inputEnd() const { return m_inputEnd; }
    [[nodiscard]]       U8* outputAt() const { return m_outputAt; }
    [[nodiscard]] const U8* outputEnd() const { return m_outputEnd; }

    void setOutput(U8* begin, const U8* end);

    bool deflate(bool finalInput);

    [[nodiscard]] DeflateStatus status() const
    {
        return static_cast<DeflateStatus>(m_status);
    }

    void clear();

private:
    void tryOutputting(bool finalChunk, bool force = false);
    void outputOutbuf();
    void readAndCompress(size_t byteCount);
    void huffcodeBlock(std::vector<U16>::iterator split,
                       std::vector<U64>& freqs, const U8* rawDataBegin,
                       const U8* rawDataEnd, bool finalBlock);

    std::vector<U16> m_symbols;
    std::vector<U64> m_symfreqs;
    Huffcoding m_symout;
    std::vector<U8> m_inbuf;
    std::vector<U8> m_outbuf;
    Compressor<decltype(std::back_inserter(m_symbols))> m_compressor;
    Encoder<decltype(std::back_inserter(m_outbuf))> m_encoder;
    ADLER32sum m_checksum;
    const U8* m_inputAt = nullptr;
    const U8* m_inputEnd = nullptr;
    U8* m_outputAt = nullptr;
    const U8* m_outputEnd = nullptr;
    size_t m_outbufPos = 0;
    size_t m_symfreqsAt = 0;
    unsigned m_status = 0;
};

Deflate::Deflate()
    : m_compressor(std::back_inserter(m_symbols)),
      m_encoder(std::back_inserter(m_outbuf))
{
    clear();
}

void Deflate::clear()
{
    m_symbols.clear();
    m_symfreqs.clear();
    m_symout.disttree.clear();
    m_symout.elems.clear();
    m_symout.litlentree.clear();
    m_inbuf.clear();
    m_outbuf.clear();
    m_compressor.clear(std::back_inserter(m_symbols));
    m_encoder.clear(std::back_inserter(m_outbuf));
    m_checksum.clear();
    m_inputAt = nullptr;
    m_inputEnd = nullptr;
    m_outputAt = nullptr;
    m_outputEnd = nullptr;
    m_outbufPos = 0;
    m_symfreqsAt = 0;
    m_status = 0;
}

void Deflate::setInput(const U8* begin, const U8* end)
{
    m_inputAt = begin;
    m_inputEnd = end;
}

void Deflate::setOutput(U8* begin, const U8* end)
{
    m_outputAt = begin;
    m_outputEnd = end;
}

void Deflate::outputOutbuf()
{
    m_status &= ~(U32)DeflateStatus::RequiresOutput;
    while (m_outbufPos < m_outbuf.size() && outputAt() != outputEnd())
    {
        *m_outputAt++ = m_outbuf[m_outbufPos++];
        if (2*m_outbufPos >= m_outbuf.size() && m_outbuf.size() > 64)
        {
            m_outbuf.erase(m_outbuf.begin(), m_outbuf.begin()+m_outbufPos);
            m_outbufPos = 0;
        }
    }
    if (m_outbufPos < m_outbuf.size())
    {
        m_status |= DeflateStatus::RequiresOutput;
    }
}

void Deflate::readAndCompress(size_t byteCount)
{
    m_status &= ~(U32)DeflateStatus::RequiresInput;
    while (byteCount && inputAt() != inputEnd())
    {
        m_compressor.append(*inputAt());
        m_inbuf.push_back(*inputAt());
        m_checksum.append(m_inputAt, 1);
        ++m_inputAt;
        --byteCount;
    }
}

void Deflate::huffcodeBlock(std::vector<U16>::iterator split,
                            std::vector<U64>& freqs,
                            const U8* rawDataBegin, const U8* rawDataEnd,
                            bool finalBlock)
{
    if (!rawDataBegin) rawDataBegin = m_inbuf.data();
    if (!rawDataEnd) rawDataEnd = m_inbuf.data() + m_inbuf.size();
    Huffcoding coding;
    std::copy(m_symbols.begin(), split, std::back_inserter(coding.elems));
    m_symbols.erase(m_symbols.begin(), split);
    coding.elems.push_back(256);
    freqs[256] = 1;
    coding.litlentree = generateHufftree(freqs, 285, 15);
    for (auto l : coding.litlentree)
    {
        if (l > 15) throw std::logic_error("Root cause!");
    }
    for (U16 i = 0; i <= 29; ++i)
    {
        freqs[i] = freqs[i+distcodeoffset];
    }
    coding.disttree = generateHufftree(freqs, 29, 15);
    U64 encbytes = 0;
    m_encoder.encode(coding, encbytes, rawDataBegin, rawDataEnd,
                     m_checksum.sum(), finalBlock);
    m_inbuf.erase(m_inbuf.begin(), m_inbuf.begin() + encbytes);
}

bool Deflate::deflate(bool finalInput)
{
    outputOutbuf();
    if (m_outbuf.size() >= 64*1024) return false;

    m_compressor.setOutput(std::back_inserter(m_symbols));

    readAndCompress(16*1024);

    bool flush = m_inbuf.size() >= 128*1024;
    if (finalInput && inputAt() == inputEnd())
    {
        flush = true;
    }

    if (flush) m_compressor.flush();

    if (m_symfreqs.empty())
    {
        m_symfreqs.resize(316, 0);
    }
    while (m_symfreqsAt < m_symbols.size())
    {
        if (m_symbols[m_symfreqsAt] <= 315)
        {
            ++m_symfreqs[m_symbols[m_symfreqsAt]];
        }
        ++m_symfreqsAt;
    }

    size_t beginDist = m_inbuf.size() >= 32*1024 ? m_inbuf.size() - 16*1024 : 0;

    std::vector<decltype(m_symbols.begin())> splits;
    findSplitPoints(m_symbols.begin(), m_symbols.end(), m_symfreqs,
                    beginDist, 512, 3, 128, 1.f, std::back_inserter(splits));

    auto split = m_symbols.end();
    if (!splits.empty())
    {
        split = splits[0];
        while (split != m_symbols.end() && *split > 256)
        {
            ++split;
        }
    }


    if (split != m_symbols.end() || flush)
    {
        std::vector<U64> prefreqs(316, 0);
        for (auto it = m_symbols.begin(); it != split; ++it)
        {
            if (*it > 315) continue;
            ++prefreqs[*it];
            --m_symfreqs[*it];
        }
        const U8* blockdataBegin = m_inbuf.data();
        const U8* blockdataEnd = nullptr;
        bool finalBlock = finalInput
                          && inputAt() == inputEnd()
                          && split == m_symbols.end();
        huffcodeBlock(split, prefreqs, blockdataBegin, blockdataEnd,
                      finalBlock);
        outputOutbuf();
    }
    if (inputAt() == inputEnd() && !finalInput)
    {
        m_status |= DeflateStatus::RequiresInput;
    }
    if (m_status == DeflateStatus::Good && inputAt() == inputEnd()
        && finalInput && m_symbols.empty() && m_outbufPos == m_outbuf.size())
    {
        m_status = DeflateStatus::Finished;
    }
    return m_status == DeflateStatus::Finished;
}

} // namespace priv

Deflate::Deflate()
{
    m_deflate = make_unique<priv::Deflate>();
}

Deflate::~Deflate()
{
    ;
}

void Deflate::setInput(const U8* begin, const U8* end)
{
    m_deflate->setInput(begin, end);
}

const U8* Deflate::inputAt() const
{
    return m_deflate->inputAt();
}

const U8* Deflate::inputEnd() const
{
    return m_deflate->inputEnd();
}

U8* Deflate::outputAt() const
{
    return m_deflate->outputAt();
}

const U8* Deflate::outputEnd() const
{
    return m_deflate->outputEnd();
}

void Deflate::setOutput(U8* begin, const U8* end)
{
    m_deflate->setOutput(begin, end);
}

bool Deflate::deflate(bool finalInput)
{
    return m_deflate->deflate(finalInput);
}

Deflate::DeflateStatus Deflate::status() const
{
    return static_cast<DeflateStatus>(static_cast<U32>(m_deflate->status()));
}

void Deflate::clear()
{
    m_deflate->clear();
}

} // namespace kraken
