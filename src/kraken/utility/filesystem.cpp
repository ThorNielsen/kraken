#include "kraken/types.hpp"
#include <cstddef>
#include <filesystem>
#include <kraken/utility/filesystem.hpp>

#include <memory>
#include <mutex>
#include <random>
#include <stdexcept>
#include <string>
#include <system_error>
#include <vector>

namespace kraken
{

namespace
{

// Returns the filename only; not actually random and not cryptographically
// secure; the randomness is only to avoid clashing with potential file names of
// other programs.
std::string
createRandomNonExistingFilename(const std::filesystem::path& directory,
                                size_t numRandomChars = 8)
{
    const char* possibleChars = "abcdefghijklmnopqrstuvwxyz"
                                "0123456789";
    constexpr size_t numChars = 26+10;

    static std::mutex mutex;
    std::lock_guard guard(mutex);

    static std::minstd_rand engine(std::random_device{}());
    std::uniform_int_distribution<size_t> dist(0, numChars - 1);
    size_t guardIt = 0;
    std::string name;
    name.reserve(numRandomChars);
    while (++guardIt <= 1000)
    {
        for (size_t i = 0; i < numRandomChars; ++i)
        {
            name.push_back(possibleChars[dist(engine)]);
        }
        if (!std::filesystem::exists(directory / name))
        {
            return name;
        }
    }
    if (numRandomChars < 3)
    {
        throw std::runtime_error("Failed to generate random filename; try "
                                 "increasing the length of the name.");
    }
    throw std::runtime_error("Failed to generate random filename: Number of "
                             "iterations exhausted.");
}

} // end anonymous namespace

TemporaryFile::TemporaryFile(const std::filesystem::path& directory)
{
    if (directory.empty()) m_path = std::filesystem::temp_directory_path();
    else if (!std::filesystem::is_directory(directory))
    {
        throw std::runtime_error("The specified directory is not a directory.");
    }
    else
    {
        m_path = directory;
    }
    m_path = m_path / createRandomNonExistingFilename(m_path);
}

TemporaryFile::~TemporaryFile()
{
    // We do not check the error code for it cannot be handled here nor can it
    // be thrown.
    std::error_code errcode;
    if (std::filesystem::exists(m_path, errcode))
    {
        std::filesystem::remove(m_path, errcode);
    }
}

TemporaryFile& TemporaryFile::operator=(TemporaryFile&& other)
{
    m_path = other.release();
    return *this;
}

std::filesystem::path TemporaryFile::release() noexcept
{
    auto toReturn = m_path;
    m_path = "";
    return toReturn;
}

std::vector<U8> readEntireFile(const std::filesystem::path& path)
{
    std::vector<U8> storage;
    size_t numBytesRead;
    if (!readEntireFile(path, [&storage](size_t numBytes)
                        {
                            storage.resize(storage.size()+numBytes);
                            return storage.data();
                        },
                        &numBytesRead))
    {
        throw std::runtime_error("Failed to open or read \""
                                 + path.string()
                                 + "\".");
    }
    return storage;
}

std::unique_ptr<U8[]> readEntireFile(const std::filesystem::path& path,
                                     size_t& sizeOut)
{
    std::unique_ptr<U8[]> result;
    if (!readEntireFile(path, [&result](size_t numBytes) -> U8*
                        {
                            if (result) return nullptr;
                            result = std::unique_ptr<U8[]>(new U8[numBytes]);
                            return result.get();
                        },
                        &sizeOut))
    {
        throw std::runtime_error("Failed to open or read \""
                                 + path.string()
                                 + "\".");
    }
    return result;
}

} // namespace kraken
