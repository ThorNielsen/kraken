#include "kraken/types.hpp"
#include <algorithm>
#include <cstddef>
#include <kraken/utility/hufftree.hpp>

#include <array>
#include <stdexcept>
#include <utility>
#include <vector>

namespace kraken::priv
{

U8 reverseByte(U8 byte)
{
    U64 magic = 0xf7b3d591e6a2c480;
    U8 lo = magic >> (U64(byte & 0xf) << 2);
    U8 hi = magic >> (U64(byte >> 4) << 2);
    return (lo << U8(4)) | (hi & 0xf);
}

U16 reverse(U16 num, U8 bits)
{
    U16 rev = reverseByte(num >> 8) | (reverseByte(num&0xff)<<8);
    return rev >> (16-bits);
}

HufftreeDecoder::HufftreeDecoder()
{
    clear();
}

HufftreeDecoder& HufftreeDecoder::operator=(const HufftreeDecoder& other)
{
    m_cache = other.m_cache;
    m_extraCacheIndices = other.m_extraCacheIndices;
    m_extraCaches = other.m_extraCaches;
    m_cacheBitMask = other.m_cacheBitMask;
    m_cachebits = other.m_cachebits;
    return *this;
}

HufftreeDecoder& HufftreeDecoder::operator=(HufftreeDecoder&& other)
{
    m_cache = std::move(other.m_cache);
    m_extraCacheIndices = std::move(other.m_extraCacheIndices);
    m_extraCaches = std::move(other.m_extraCaches);
    m_cacheBitMask = other.m_cacheBitMask;
    m_cachebits = other.m_cachebits;
    return *this;
}

void HufftreeDecoder::create(const U8* pBitLengths,
                             size_t bitlenCount,
                             const U16* pValues,
                             bool startsFromLSB)
{
    m_startsFromLSB = startsFromLSB;
    std::array<U8, 17> bitcounts;
    bitcounts.fill(0);
    U8 maxbitlen = 0;
    for (size_t i = 0; i < bitlenCount; ++i)
    {
        auto len = pBitLengths[i];
        if (len > 16) continue;
        ++bitcounts[len];
        maxbitlen = std::max(maxbitlen, len);
    }
    bitcounts[0] = 0;

    U16 startcode = 0;
    std::array<U16, 17> nextcode;
    for (U8 bits = 1; bits <= maxbitlen; ++bits)
    {
        startcode = static_cast<U16>((startcode+bitcounts[bits-1]) << 1);
        nextcode[bits] = startcode;
    }

    U8 cachebits = maxbitlen;
    if (cachebits > 9)
    {
        for (size_t i = 9; i > 0; --i)
        {
            if (bitcounts[i])
            {
                cachebits = i;
                break;
            }
        }
    }

    setCacheBits(cachebits);

    for (size_t i = 0; i < bitlenCount; ++i)
    {
        if (pBitLengths[i])
        {
            auto value = pValues ? pValues[i] : i;
            auto code = nextcode[pBitLengths[i]];
            if (m_startsFromLSB) code = reverse(code, pBitLengths[i]);
            addSymbol(value, code, pBitLengths[i]);
            ++nextcode[pBitLengths[i]];
        }
    }
}

void HufftreeDecoder::create(const std::vector<U8>& bitlens)
{
    create(bitlens.data(), bitlens.size());
}

void HufftreeDecoder::clear()
{
    m_cache.clear();
    m_extraCacheIndices.clear();
    m_extraCaches.clear();
    m_cachebits = 0;
    m_cacheBitMask = 0;
}

void HufftreeDecoder::setCacheBits(U8 count)
{
    if (count < 1) count = 1;
    if (count > 9) count = 9;
    m_cachebits = count;
    m_cacheBitMask = (U16(1) << U16(count)) - 1;
    m_cache.resize(1 << m_cachebits, 0);
    auto oldECISize = m_extraCacheIndices.size();
    m_extraCacheIndices.resize(1 << m_cachebits, 0xffff);
    auto fixupCount = std::max(oldECISize, m_extraCacheIndices.size());
    std::fill(m_extraCacheIndices.begin(),
              m_extraCacheIndices.begin() + fixupCount,
              0xffff);
    m_extraCaches.clear();
}

[[nodiscard]] HufftreeDecoder::DecodeResult
HufftreeDecoder::decodeSymbol(U16 encoded) const noexcept
{
    if (m_cache.empty())
    {
        // If there are no codes; we cannot decode anything.
        return {0, 0};
    }
    if (m_startsFromLSB) return decodeSymbolLSB(encoded);
    else return decodeSymbolMSB(encoded);
}

[[nodiscard]] HufftreeDecoder::DecodeResult
HufftreeDecoder::decodeSymbolLSB(U16 encoded) const noexcept
{
    auto lowerPart = encoded & m_cacheBitMask;
    auto initial = m_cache[lowerPart];
    auto initCodeLength = initial >> codeBitCount;
    if (initCodeLength <= m_cachebits)
    {
        return {U16(initial & codeMask), U8(initial >> codeBitCount)};
    }
    auto upperIndex = m_extraCacheIndices[lowerPart];
    if (upperIndex >= m_extraCaches.size())
    {
        return {0xffff, 0}; // Severe decoding error.
    }
    initial = m_extraCaches[upperIndex][encoded >> m_cachebits];
    return {U16(initial & codeMask), U8(initial >> codeBitCount)};
}

[[nodiscard]] HufftreeDecoder::DecodeResult
HufftreeDecoder::decodeSymbolMSB(U16 encoded) const noexcept
{
    auto toLookup = encoded >> U16(16-m_cachebits);
    auto initial = m_cache[toLookup];
    auto initCodeLength = initial >> codeBitCount;
    if (initCodeLength <= m_cachebits)
    {
        return {U16(initial & codeMask), U8(initial >> codeBitCount)};
    }
    auto extBitMask = (U16(1) << U16(16-m_cachebits)) - 1;
    auto remainder = encoded & extBitMask;
    auto upperIndex = m_extraCacheIndices[toLookup];
    if (upperIndex >= m_extraCaches.size())
    {
        return {0xffff, 0}; // Severe decoding error.
    }
    initial = m_extraCaches[upperIndex][remainder];
    return {U16(initial & codeMask), U8(initial >> codeBitCount)};
}


void HufftreeDecoder::addSymbol(U16 value, U16 code, U8 codeLength)
{
    if (codeLength > 16)
    {
        throw std::runtime_error("Failed to construct Huffman tree "
                                 "(invalid bit length).");
    }
    // In the cache we store values as
    // bbbbbccc cccccccc,
    // where bbbbb is the length (in bits) of this code, and
    // ccc cccccccc is the actual code value.
    // We want our value to be stored as
    // eeeeeeee eeeeeeee eeebbbbc cccccccc
    // e = index for extra cache, b = bitlen, c = code
    // Any ordinary call will not have any length bits set, so here we can check
    // if we haven't processed the symbol yet (as we fill in all possible
    // suffixes of a given symbol such that the length becomes either # of
    // cachebits or 15).
    if (!(value >> codeBitCount)) value |= codeLength << codeBitCount;

    // The initial indexing value depends highly on whether we will start
    // decoding from LSB or MSB.
    U16 lowerValue;
    // To extract the cached code, we will make a number where the lowest bit of
    // the cached code is the lowest bit of what we store. So initially we just
    // extract the code, potentially with more high bits set.
    if (m_startsFromLSB)
    {
        // The value is already aligned.
        lowerValue = code;
    }
    else
    {
        // If we start from the MSB, the code lies in 'code' like this:
        // xxxx xxxx xccc cccc (in this example if codeLength=7)
        // Now, if the code length is equal to the cachebits, we are done, but
        // if not, we'll have to shift it either direction by the difference,
        // so in the example above if cachebits is 4, we'll shift by 3 to the
        // right, while if cachebits is 9, we'll shift by 2 to the left.
        if (codeLength <= m_cachebits)
        {
            lowerValue = code << U16(m_cachebits-codeLength);
        }
        else
        {
            lowerValue = code >> U16(codeLength - m_cachebits);
        }
    }
    // Now we can remove the high bits.
    lowerValue &= m_cacheBitMask;

    // We can only insert into the cache when we have filled out the code enough
    if (codeLength >= m_cachebits)
    {
        m_cache[lowerValue] = value;
    }

    if (codeLength <= m_cachebits)
    {
        if (codeLength < m_cachebits)
        {
            if (m_startsFromLSB)
            {
                addSymbol(value, code, codeLength + 1);
                addSymbol(value, code | U16(1 << codeLength), codeLength + 1);
            }
            else
            {
                addSymbol(value, code << U16(1), codeLength + 1);
                addSymbol(value, (code << U16(1)) | U16(1), codeLength + 1);
            }
        }
    }
    else
    {
        auto& cacheIndex = m_extraCacheIndices[lowerValue];
        if (cacheIndex == 0xffff)
        {
            cacheIndex = (U16)m_extraCaches.size();
            m_extraCaches.emplace_back(1 << (16 - m_cachebits), 0xffff);
        }
        if (codeLength < 16)
        {
            if (m_startsFromLSB)
            {
                addSymbol(value, code, codeLength + 1);
                addSymbol(value, code | U16(1 << codeLength), codeLength + 1);
            }
            else
            {
                addSymbol(value, code << U16(1), codeLength + 1);
                addSymbol(value, (code << U16(1)) | U16(1), codeLength + 1);
            }
        }
        else
        {
            auto& cache = m_extraCaches[cacheIndex];

            auto insertAt = m_startsFromLSB
                          ? U16(code >> m_cachebits)
                          : code & ((U16(1) << U16(16 - m_cachebits)) - 1);
            cache[insertAt] = value;
        }
    }
}

} // namespace kraken::priv
