#include "kraken/types.hpp"
#include "kraken/utility/utf.hpp"
#include <algorithm>
#include <cstddef>
#include <ios>
#include <istream>
#include <iterator>
#include <kraken/utility/json.hpp>
#include <ostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>

#include <kraken/utility/utf.hpp>

#include <charconv>
#include <limits>
#include <sstream>

namespace kraken
{

[[nodiscard]] std::optional<double>
parseDouble(const char* begin, const char* end) noexcept
{
    double value;
    auto result = std::from_chars(begin, end, value);
    if (result.ec == std::errc{}) return value;
    return std::nullopt;
}

std::string toString(double input)
{
    std::string s;
    s.resize(7+std::numeric_limits<double>::max_digits10);
    auto result = std::to_chars(s.data(), s.data()+s.size(), input);
    if (result.ec != std::errc{})
    {
        throw std::logic_error("Failed to convert double to string.");
    }
    auto written = static_cast<std::size_t>(result.ptr - s.data());
    if (written != s.size()) s.resize(written);
    return s;
}

template <typename Integer>
std::string toString(Integer input) requires std::is_integral_v<Integer>
{
    std::string s;
    s.resize(21);
    auto result = std::to_chars(s.data(), s.data()+s.size(), input);
    if (result.ec != std::errc{})
    {
        throw std::logic_error("Failed to convert integer to string.");
    }
    auto written = static_cast<std::size_t>(result.ptr - s.data());
    if (written != s.size()) s.resize(written);
    return s;
}

bool JSONValue::operator==(const JSONValue& other) const
{
    switch (m_type)
    {
    default:
    case JSONType::Null:
        return other.isNull();

    case JSONType::Boolean:
        return other.isBoolean() && asBoolean() == other.asBoolean();

    case JSONType::Double:
        if (!other.isNumeric()) return false;
        return asDouble() == (F64)other; // Explicit cast for conversion.
    case JSONType::SignedInt:
        if (!other.isNumeric()) return false;
        return asSignedInt() == (S64)other; // Explicit cast for conversion.
    case JSONType::UnsignedInt:
        if (!other.isNumeric()) return false;
        return asUnsignedInt() == (U64)other; // Explicit cast for conversion.

    case JSONType::String:
        return other.isString() && asString() == other.asString();
    case JSONType::Array:
        if (!other.isArray()) return false;
        if (asArray().size() != other.asArray().size()) return false;
        for (size_t i = 0; i < asArray().size(); ++i)
        {
            if (asArray()[i] != other.asArray()[i]) return false;
        }
        return true;

    case JSONType::Object:
        if (!other.isObject()) return false;
        else
        {
            if (asObject().size() != other.asObject().size()) return false;
            const auto& otherObj = other.asObject();
            for (const auto& elem : asObject())
            {
                const auto& found = otherObj.find(elem.first);
                if (found == otherObj.end()) return false;
                if (elem.second != found->second) return false;
            }
            return true;
        }
    }
}

namespace detail
{

template <>
[[nodiscard]] std::optional<double>
tryConvertTo<double>(const std::string& contents) noexcept
{
    return parseDouble(contents.data(),
                       contents.data() + contents.size());
}

} // namespace detail

std::string JSONValue::stringifyNumber() const
{
    if (isDouble()) return toString(asDouble());
    if (isUnsignedInt()) return toString(asUnsignedInt());
    if (isSignedInt()) return toString(asSignedInt());
    throw std::logic_error("Isn't number.");
}

namespace
{

void jsonEncodeCodepoint(std::string& out, U32 codepoint)
{
    out.push_back('\\');
    out.push_back('u');
    U8 chars[4];
    for (size_t i = 0; i < 4; ++i)
    {
        chars[3-i] = codepoint&0xf;
        codepoint>>=4;
    }
    for (auto c : chars)
    {
        out.push_back("0123456789abcdef"[c]);
    }
}

std::string formatJSONString(const std::string& input)
{
    std::string output;
    output.reserve(2+std::max(input.size()+16, input.size() + (input.size() >> 4)));
    output.push_back('"');
    for (auto it = input.begin(); it != input.end();)
    {
        U32 codepoint;
        it = decodeUTF8(it, input.end(), codepoint, 0xfffd);
        if (codepoint <= 0x1f)
        {
            if (codepoint == '\n')
            {
                output.push_back('\\');
                output.push_back('n');
            }
            else
            {
                jsonEncodeCodepoint(output, codepoint);
            }
        }
        else if (codepoint > 0xffff)
        {
            U16 utf16enc[2] = {0xfffd, 0xfffd};
            encodeUTF16(codepoint, utf16enc);
            jsonEncodeCodepoint(output, utf16enc[0]);
            jsonEncodeCodepoint(output, utf16enc[1]);
        }
        else if (codepoint == 0x22 || codepoint == 0x5c)
        {
            output.push_back(0x5c);
            output.push_back(codepoint);
        }
        else
        {
            encodeUTF8(codepoint, std::back_inserter(output));
        }
    }
    output.push_back('"');
    return output;
}

std::string formatJSONBoolean(bool input)
{
    return input ? "true" : "false";
}

bool isSimple(const JSONValue& obj)
{
    return (obj.type() == JSONType::String
         || obj.type() == JSONType::Number
         || obj.type() == JSONType::Boolean
         || obj.type() == JSONType::Null);
}

bool printInline(const JSONValue& obj)
{
    if (isSimple(obj)) return true;
    if (obj.type() == JSONType::Array)
    {
        if (obj.asArray().size() <= 8)
        {
            for (const auto& elem : obj.asArray())
            {
                if (!isSimple(elem)) return false;
            }
            return true;
        }
    }
    if (obj.type() == JSONType::Object)
    {
        if (obj.asObject().size() <= 2)
        {
            for (const auto& elem : obj.asObject())
            {
                if (!isSimple(elem.second)) return false;
            }
            return true;
        }
    }
    return false;
}

using JSONParseError = std::runtime_error;

template <typename It>
It ignoreWhitespace(It begin, It end)
{
    while (begin != end)
    {
        switch (*begin)
        {
        case 0x20: case 0x09: case 0x0a: case 0x0d:
            ++begin;
            break;
        default:
            return begin;
        }
    }
    return begin;
}

inline void throwEscapedEOF()
{
    throw JSONParseError("Premature end of input in escaped character.");
}

U16 hexDigitToNumber(U8 hex)
{
    if (0x30 <= hex && hex <= 0x39)
    {
        return hex - 0x30;
    }
    if (0x41 <= hex && hex <= 0x46)
    {
        return hex - (0x41-10);
    }
    if (0x61 <= hex && hex <= 0x66)
    {
        return hex - (0x61-10);
    }
    throw JSONParseError("Invalid hex digit.");
}

template <typename It>
It parseEscaped(It begin, It end, U16& charOut)
{
    ++begin;
    if (begin == end) throwEscapedEOF();
    switch (*begin)
    {
    case '"': case '\\': case '/':
        charOut = *begin;
        return ++begin;
    case 'b':
        charOut = 0x8;
        return ++begin;
    case 'f':
        charOut = 0xc;
        return ++begin;
    case 'n':
        charOut = 0xa;
        return ++begin;
    case 'r':
        charOut = 0xd;
        return ++begin;
    case 't':
        charOut = 0x9;
        return ++begin;
    case 'u':
        ++begin;
        break;
    default:
        throw JSONParseError("Invalid escape character.");
    }
    // We have parsed "\u" so far -- now we just need to parse the 4 hexes.
    U16 hexdigit = 0;
    for (size_t i = 0; i < 4; ++i)
    {
        if (begin == end) throwEscapedEOF();
        hexdigit = (hexdigit << 4) | hexDigitToNumber(*begin++);
    }
    charOut = hexdigit;
    return begin;
}

template <typename It>
It parseEscaped(It begin, It end, std::string& stringOut)
{
    U16 chars[2];
    begin = parseEscaped(begin, end, chars[0]);
    if (0xd7ff < chars[0] && chars[0] < 0xe000)
    {
        begin = parseEscaped(begin, end, chars[1]);
    }
    U32 codepoint = invalidCodepoint;
    decodeUTF16(chars, chars+2, codepoint);
    encodeUTF8(codepoint, std::back_inserter(stringOut));
    return begin;
}

template <typename It>
It checkLiteral(It begin, It end, const char* lit, bool& result,
                bool noFinalIncrement = false)
{
    while (begin != end && *lit)
    {
        if (*begin != *lit)
        {
            result = false;
            return begin;
        }
        if (noFinalIncrement && !lit[1])
            ;
        else
            ++begin;
        ++lit;
    }
    result = !*lit;
    return begin;
}

template <typename It>
It extractDigits(It begin, It end, std::string& digitsOut)
{
    while (0x30 <= *begin && *begin <= 0x39 && begin != end)
    {
        digitsOut.push_back(*begin++);
    }
    return begin;
}


void writeIndent(std::ostream& ost, size_t indent)
{
    indent *= 4;
    while (indent)
    {
        ost << ' ';
        --indent;
    }
}

std::ostream& prettyPrint(std::ostream& ost, const JSONValue& obj, size_t indent)
{
    switch (obj.type())
    {
    case JSONType::String:
        return ost << formatJSONString(obj.asString());
    case JSONType::Double:
        return ost << toString(obj.asDouble());
    case JSONType::SignedInt:
        return ost << toString(obj.asSignedInt());
    case JSONType::UnsignedInt:
        return ost << toString(obj.asUnsignedInt());
    case JSONType::Boolean:
        return ost << formatJSONBoolean(obj.asBoolean());
    case JSONType::Object:
        {
            const auto& subobj = obj.asObject();

            bool currInline = printInline(obj);
            writeIndent(ost, indent);
            ost << (currInline ? "{" : "{\n");
            for (auto it = subobj.begin(); it != subobj.end(); ++it)
            {
                if (it != subobj.begin()) ost << "," << "\n "[currInline];

                if (!currInline)
                {
                    writeIndent(ost, indent+1);
                }

                ost << formatJSONString(it->first) << ":";

                bool subInline = printInline(it->second);

                ost << "\n "[subInline];

                prettyPrint(ost, it->second, (indent+1)*!subInline);
            }
            if (!currInline)
            {
                ost << "\n";
                writeIndent(ost, indent);
            }
            ost << "}";
            return ost;
        }
    case JSONType::Array:
        {
            const auto& subarr = obj.asArray();

            bool currInline = printInline(obj);
            writeIndent(ost, indent);
            ost << (currInline ? "[" : "[\n");
            for (auto it = subarr.begin(); it != subarr.end(); ++it)
            {
                if (it != subarr.begin()) ost << "," << "\n "[currInline];

                bool subobjInline = printInline(*it);

                if (!currInline && subobjInline)
                {
                    writeIndent(ost, indent+1);
                }

                bool subInline = printInline(*it);

                prettyPrint(ost, *it, (indent+1)*!subInline);
            }
            if (!currInline)
            {
                ost << "\n";
                writeIndent(ost, indent);
            }
            ost << "]";
            return ost;
        }
    case JSONType::Null: default:
        return ost << "null";
    }
}

template <typename It>
It parseJSON(It begin, It end, JSONValue& jsonOut, bool noFinalIncrement = false)
{
    begin = ignoreWhitespace(begin, end);
    if (begin == end) return end;
    jsonOut.clear();
    const U8 initialChar = *begin++;
    switch (initialChar)
    {

    // We ignore the warnings about uninitialised variables since there are
    // false positives with the construction where we call .initType, and then
    // access it directly.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"

    case '{':
        jsonOut.setType(JSONType::Object);
        while ((begin = ignoreWhitespace(begin, end)) != end)
        {
            if (*begin == '}')
            {
                if (!noFinalIncrement) ++begin;
                break;
            }

            JSONValue key;
            begin = parseJSON<It>(begin, end, key);
            if (key.type() != JSONType::String)
            {
                throw JSONParseError("Expected to parse string.");
            }
            begin = ignoreWhitespace(begin, end);
            if (begin == end)
            {
                throw JSONParseError("Premature end of input.");
            }
            if (*begin++ != ':')
            {
                throw JSONParseError("Expected name-separator.");
            }
            begin = ignoreWhitespace(begin, end);
            auto& value = jsonOut[key.asString()] = JSONValue{};
            begin = parseJSON<It>(begin, end, value);
            begin = ignoreWhitespace(begin, end);
            if (begin == end)
            {
                throw JSONParseError("Premature end of input.");
            }
            if (*begin == '}')
            {
                return noFinalIncrement ? begin : ++begin;
            }
            if (*begin != ',')
            {
                throw JSONParseError("Expected value separator or end of object.");
            }
            ++begin; // Ignore the comma.
        }
        break;

    case '[':
        jsonOut.setType(JSONType::Array);
        while ((begin = ignoreWhitespace(begin, end)) != end)
        {
            if (*begin == ']')
            {
                if (!noFinalIncrement) ++begin;
                break;
            }
            jsonOut.asArray().push_back({});
            begin = parseJSON<It>(begin, end, jsonOut.asArray().back());
            begin = ignoreWhitespace(begin, end);
            if (begin == end)
            {
                throw JSONParseError("Premature end of input.");
            }
            if (*begin == ']')
            {
                return noFinalIncrement ? begin : ++begin;
            }
            if (*begin != ',')
            {
                throw JSONParseError("Expected value separator or end of array.");
            }
            ++begin;
        }
        break;

    case '"':
        jsonOut.setType(JSONType::String);
        while (begin != end)
        {
            if (*begin == '"') break;
            if (*begin == '\\')
            {
                begin = parseEscaped(begin, end, jsonOut.asString());
            }
            else
            {
                jsonOut.asString().push_back(*begin);
                ++begin;
            }
        }
        if (begin == end)
        {
            throw JSONParseError("Premature end of input.");
        }
        return noFinalIncrement ? begin : ++begin;
#pragma GCC diagnostic pop

    case '-': case '.': case '+':
    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
        {
            std::string constant;
            constant.push_back(initialChar);
            begin = extractDigits(begin, end, constant);
            if (begin != end && *begin == 0x2e)
            {
                constant.push_back(*begin++);
                begin = extractDigits(begin, end, constant);
                if (begin != end && (*begin == 0x65 || *begin == 0x45))
                {
                    constant.push_back(*begin++);
                    if (begin != end && (*begin == 0x2b || *begin == 0x2d))
                    {
                        constant.push_back(*begin++);
                    }
                    begin = extractDigits(begin, end, constant);
                }
            }
            if (constant.find('.') == std::string::npos
                && constant.find('e') == std::string::npos
                && constant.find('E') == std::string::npos)
            {
                S64 value;
                auto result = std::from_chars(constant.data(),
                                              constant.data()+constant.size(),
                                              value);
                if (result.ec == std::errc{}) jsonOut = value;
                else
                {
                    U64 unsignedValue;
                    result = std::from_chars(constant.data(),
                                             constant.data() + constant.size(),
                                             unsignedValue);
                    if (result.ec == std::errc{}) jsonOut = unsignedValue;
                    else jsonOut = std::stod(constant);
                }
            }
            else jsonOut = std::stod(constant);
        }
        break;

    case 'f':
        {
            bool result;
            begin = checkLiteral(begin, end, "alse", result, noFinalIncrement);
            if (!result)
            {
                throw JSONParseError("Invalid literal.");
            }
            jsonOut = false;
        }
        break;

    case 't':
        {
            bool result;
            begin = checkLiteral(begin, end, "rue", result, noFinalIncrement);
            if (!result)
            {
                throw JSONParseError("Invalid literal.");
            }
            jsonOut = true;
        }
        break;

    case 'n':
        {
            bool result;
            begin = checkLiteral(begin, end, "ull", result, noFinalIncrement);
            if (!result)
            {
                throw JSONParseError("Invalid literal.");
            }
            jsonOut.clear();
        }
        break;

    default:
        throw JSONParseError("Invalid initial character.");

    }
    return begin;
}

} // end anonymous namespace


const U8* parseJSON(const U8* begin, const U8* end, JSONValue& jsonOut)
{
    return parseJSON<const U8*>(begin, end, jsonOut);
}

JSONValue parseJSON(std::string_view view)
{
    const U8* begin = static_cast<const U8*>(static_cast<const void*>(view.data()));
    const U8* end = begin + view.size();
    JSONValue json;
    parseJSON(begin, end, json);
    return json;
}

std::string JSONValue::serialise(bool compact) const
{
    std::stringstream ss;
    if (compact)
    {
        compactPrint(ss, *this);
    }
    else
    {
        prettyPrint(ss, *this);
    }
    return ss.str();
}

JSONValue JSONValue::parse(std::string_view view)
{
    return parseJSON(view);
}

std::ostream& prettyPrint(std::ostream& ost, const JSONValue& obj)
{
    return prettyPrint(ost, obj, 0);
}

std::ostream& compactPrint(std::ostream& ost, const JSONValue& obj)
{
    switch (obj.type())
    {
    case JSONType::String:
        return ost << formatJSONString(obj.asString());
    case JSONType::Double:
        return ost << toString(obj.asDouble());
    case JSONType::SignedInt:
        return ost << toString(obj.asSignedInt());
    case JSONType::UnsignedInt:
        return ost << toString(obj.asUnsignedInt());
    case JSONType::Boolean:
        return ost << formatJSONBoolean(obj.asBoolean());
    case JSONType::Object:
        {
            const auto& subobj = obj.asObject();
            ost << '{';
            for (auto it = subobj.begin(); it != subobj.end(); ++it)
            {
                if (it != subobj.begin()) ost << ',';
                ost << formatJSONString(it->first) << ':';
                compactPrint(ost, it->second);
            }
            ost << '}';
            return ost;
        }
    case JSONType::Array:
        {
            const auto& subarr = obj.asArray();
            ost << '[';
            for (auto it = subarr.begin(); it != subarr.end(); ++it)
            {
                if (it != subarr.begin()) ost << ',';
                compactPrint(ost, *it);
            }
            ost << ']';
            return ost;
        }
    case JSONType::Null: default:
        return ost << "null";
    }
}

std::ostream& operator<<(std::ostream& ost, const JSONValue& obj)
{
    return prettyPrint(ost, obj);
}

JSONValue parseJSON(std::istream& input)
{
    JSONValue obj;

    auto skipws = input.flags(std::ios_base::skipws);
    if (skipws) input.unsetf(std::ios_base::skipws);

    try
    {
        parseJSON(std::istream_iterator<U8>(input),
                  std::istream_iterator<U8>(),
                  obj,
                  true);
    }
    catch (JSONParseError&)
    {
        input.setstate(std::ios_base::failbit);
        obj.clear();
    }

    if (skipws) input.setf(input.flags() | std::ios_base::skipws);

    return obj;
}

std::istream& operator>>(std::istream& input, JSONValue& obj)
{
    obj = parseJSON(input);
    return input;
}


} // namespace kraken
