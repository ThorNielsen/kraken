#include <kraken/utility/memory.hpp>

#include <cstddef>
#include <cstdint>
#include <new>
#include <limits>

#define USE_CUSTOM_ALIGNED_ALLOCATOR

namespace
{

void* getPreviousPointerAddress(void* ptr)
{
    std::size_t ptrAlign = alignof(void*);
    std::size_t ptrSize = sizeof(void*);
    auto pos = reinterpret_cast<std::uintptr_t>(ptr);

    // Note that we move at most (ptrAlign-1) + ptrSize back. Since we have
    // allocated a block before ptr which is ptrAlign + ptrSize, only valid
    // memory is accessed.
    pos -= ptrSize;
    pos -= pos % ptrAlign;
    return reinterpret_cast<void*>(pos);
}

} // end anonymous namespace

namespace kraken
{

void* allocateUntyped(std::size_t bytes, std::size_t alignment)
{
    // We manually create a pointer with the given alignment, since operator new
    // has quite severe restrictions upon its alignment (and therefore we do not
    // use aligned new from C++17).
#if (__cplusplus >= 201703L) && !defined (USE_CUSTOM_ALIGNED_ALLOCATOR)
    return ::operator new(bytes, std::align_val_t{alignment});
#else // __cplusplus >= 201703L
    std::size_t ptrAlign = alignof(void*);
    std::size_t ptrSize = sizeof(void*);
    std::size_t ptrBlockSize = ptrAlign + ptrSize;
    std::size_t extraAlloc = ptrBlockSize + alignment;

    void* alloc = ::operator new(bytes+extraAlloc);
    // Get memory address in a usable format (void* arithmetic is disallowed).
    auto pos = reinterpret_cast<std::uintptr_t>(alloc);
    // Move pointer forward by exactly the amount of extra allocated space.
    pos += extraAlloc;
    // Correct alignment by moving backwards.
    pos -= pos % alignment;
    // Now alignment is correct and there's at least 'bytes' allocated bytes
    // after the pointer, so we can return this.
    void* returnedPointer = reinterpret_cast<void*>(pos);

    // We need to store the original pointer to be able to delete the memory,
    // since the exact same pointer returned from ::operator new must be passed
    // to ::operator delete.
    void* addr = getPreviousPointerAddress(returnedPointer);
    *static_cast<void**>(addr) = alloc;
    return returnedPointer;
#endif // __cplusplus >= 201703L
}

void deleteUntyped(void* pointer)
{
#if __cplusplus >= 201703L && !defined (USE_CUSTOM_ALIGNED_ALLOCATOR)
    ::operator delete(pointer);
#else // __cplusplus >= 201703L
    void* addr = getPreviousPointerAddress(pointer);
    void* origPtr = *static_cast<void**>(addr);
    ::operator delete(origPtr);
#endif // __cplusplus >= 201703L
}

ExternalMemoryAllocation::ExternalMemoryAllocation() noexcept
    : m_memalloc{nullptr}
{
    clear();
}

ExternalMemoryAllocation::ExternalMemoryAllocation(MemoryAllocationFunction func,
                                                   bool isStableOnError) noexcept
{
    clear();
    m_memalloc.swap(func);
    m_isStableOnError = isStableOnError;
}

ExternalMemoryAllocation::ExternalMemoryAllocation(ExternalMemoryAllocation&& other) noexcept
    : m_memalloc{nullptr}
{
    *this = std::move(other);
}

ExternalMemoryAllocation&
ExternalMemoryAllocation::operator=(ExternalMemoryAllocation&& other) noexcept
{
    // std::function's move constructor is not required to be noexcept, so we
    // technically cannot use it, but its swap is noexcept. Thus, we just swap
    // and then clear the other to get rid of our current funciton.
    m_memalloc.swap(other.m_memalloc);
    m_capacity = other.m_capacity;
    m_data = other.m_data;
    m_size = other.m_size;

    other.clear();
    return *this;
}

ExternalMemoryAllocation::operator ExternalMemoryAllocation::MemoryAllocationFunction()
{
    return [this](std::size_t bytes, std::size_t alignment)
    {
        if (!reserve(bytes, alignment))
        {
            throw std::runtime_error("Failed to allocate memory.");
        }
        return data();
    };
}

void ExternalMemoryAllocation::setAllocator(MemoryAllocationFunction func,
                                            bool isStableOnError) noexcept
{
    (m_memalloc = nullptr).swap(func);
    m_isStableOnError = isStableOnError;
}

bool ExternalMemoryAllocation::reserve(std::size_t desiredCapacity,
                                       std::size_t alignment) noexcept
{
    if (m_capacity >= desiredCapacity) return true;
    if (m_capacity < 8)
    {
        return reserveExact(desiredCapacity, alignment);
    }

    if ((desiredCapacity >> 1) >= m_capacity)
    {
        return reserveExact(desiredCapacity, alignment);
    }

    std::size_t toReserve = m_capacity;
    for (std::size_t k = 8*sizeof(toReserve);
         k && toReserve < desiredCapacity;
         --k)
    {
        toReserve <<= 1;
    }
    if (toReserve < desiredCapacity)
    {
        // In this case we have an overflow (which is defined behaviour for
        // unsigned types), but that means the original capacity is more
        // than 50% of the max capacity and doubling is thus wrong.
        // Therefore, we just attempt to reserve all the memory.
        toReserve = std::numeric_limits<decltype(desiredCapacity)>::max();
    }
    return reserveExact(toReserve, alignment);
}

bool ExternalMemoryAllocation::reserveExact(std::size_t bytes, std::size_t alignment) noexcept
{
    if (!m_memalloc) return false;
    if (!bytes) return false;

    void* newAllocation = nullptr;
    try
    {
        newAllocation = m_memalloc(bytes, alignment);
    }
    catch (...)
    {
        newAllocation = nullptr;
    }
    if (newAllocation)
    {
        m_capacity = bytes;
        if (m_size > m_capacity) m_size = m_capacity;
        m_data = newAllocation;
    }
    else if (!m_isStableOnError)
    {
        // ALlocation failed AND we don't have stability under errors.
        m_capacity = m_size = 0;
        m_data = nullptr;
    }
    return newAllocation;
}

void ExternalMemoryAllocation::clear() noexcept
{
    m_memalloc = nullptr;
    m_data = nullptr;
    m_capacity = 0;
    m_size = 0;
    m_isStableOnError = false;
}

} // namespace kraken
