#include <kraken/utility/string.hpp>

#include <cctype>

#ifdef KRAKEN_ENABLE_UNICODE_SUPPORT
#define BOOST_LOCALE_HIDE_AUTO_PTR
#include <boost/locale.hpp>
#include <unicode/uchar.h>
#endif // KRAKEN_ENABLE_UNICODE_SUPPORT

namespace kraken
{

#ifdef KRAKEN_ENABLE_UNICODE_SUPPORT
const std::locale& defaultLocale = boost::locale::generator()("en_US.UTF-8");

std::locale createLocale(std::string name)
{
    return boost::locale::generator()(name);
}

namespace
{

// If Unicode support is enabled then we must set the default locale. To avoid
// burdening any users with this task we set it automatically at startup.
// The following is a quick hack to set the global locale on startup -- make a
// global variable whose value is set using a function that coincidentally calls
// the global locale setting function.
bool dummyFunctionJustForSettingGlobalLocale()
{
    setGlobalLocale(defaultLocale);
    return true;
}

bool stupidGlobalVariableWhoseOnlyPurposeIsToSetTheGlobalLocale
    = dummyFunctionJustForSettingGlobalLocale();

} // ena anonymous namespace

#else
const std::locale& defaultLocale = std::locale::classic();

std::locale createLocale(std::string name)
{
    return std::locale(name);
}
#endif // KRAKEN_ENABLE_UNICODE_SUPPORT

///////////////////////////////////////
// Conversions.
///////////////////////////////////////

String::String(char c, const std::locale& loc)
{
    push_back(toWideChar(c, loc));
}

String::String(U32 codepoint)
{
    push_back(codepoint);
}

String::String(const char* s, const std::locale& loc)
{
    for (size_t i = 0; s[i]; ++i)
    {
        push_back(s[i], loc);
    }
}

String::String(const std::string& s, const std::locale& loc)
    : String(s.c_str(), loc) {}

String::String(const char* ch)
{
    m_str = ch;
}

String::String(const std::string& s)
{
    resize(s.size());
    auto dest = m_str.begin();
    auto src = s.begin();
    while (src != s.end())
    {
        *dest = *src;
        ++dest;
        ++src;
    }
}

std::string String::ansi(const std::locale& loc) const
{
    std::string s;
    auto it = cbegin();
    while (it != cend())
    {
        char c = toNarrowChar(*it, 0u, loc);
        if (c) s.push_back(c);
    }
    return s;
}


template <size_t byteSize>
wchar_t convertToWideImpl(U32 codepoint);

template<>
wchar_t convertToWideImpl<2>(U32 codepoint)
{
    if (codepoint > 0xffff) return 0xfffd;
    return static_cast<wchar_t>(codepoint);
}

template<>
wchar_t convertToWideImpl<4>(U32 codepoint)
{
    return static_cast<wchar_t>(codepoint);
}

wchar_t convertToWide(U32 codepoint)
{
    return convertToWideImpl<sizeof(wchar_t)>(codepoint);
}

std::wstring String::wide() const
{
    std::wstring ws;
    auto it = cbegin();
    while (it != cend())
    {
        wchar_t wc = convertToWide(*it);
        if (wc) ws.push_back(wc);
    }
    return ws;
}

std::basic_string<U8> String::toUTF8() const
{
    std::basic_string<U8> s;
    s.reserve(m_str.size());
    for (size_t i = 0; i < m_str.size(); ++i)
    {
        s.push_back(static_cast<U8>(m_str[i]));
    }
    return s;
}

std::basic_string<U16> String::toUTF16() const
{
    std::basic_string<U16> s;
    auto dest = std::back_inserter(s);
    auto src = begin();
    while (src != end())
    {
        dest = encodeUTF16(*src, dest);
        ++src;
    }
    return s;
}

std::basic_string<U32> String::toUTF32() const
{
    std::basic_string<U32> s;
    auto src = begin();
    while (src != end())
    {
        s.push_back(*src);
        ++src;
    }
    return s;
}

String::operator std::string() const
{
    return str();
}

String::operator std::wstring() const
{
    return wide();
}



///////////////////////////////////////
// Memory functions.
///////////////////////////////////////

size_t String::length() const
{
    size_t len = 0;
    for (auto it = begin(); it != end(); ++it)
    {
        ++len;
    }
    return len;
}

void String::reserve(size_t bytes)
{
    m_str.reserve(std::max(bytes, size()));
}

void String::resize(size_t bytes)
{
    if (bytes == m_str.size()) return;
    if (bytes < m_str.size() && bytes)
    {
        unsigned byteCount = 0;
        size_t idx = bytes;

        while (idx > 0 && ((m_str[idx] & 0xc0) == 0x80))
        {
            --idx;
            ++byteCount;
        }
        m_str.resize(bytes);
        while (byteCount)
        {
            m_str.pop_back();
            --byteCount;
        }
    }
    else
    {
        m_str.resize(bytes);
    }
}



///////////////////////////////////////
// Operations.
///////////////////////////////////////

#ifdef KRAKEN_ENABLE_UNICODE_SUPPORT
int String::compare(const String& other, const std::locale& loc,
                    ComparisonLevel level) const
{
    auto& facet = std::use_facet<boost::locale::collator<char>>(loc);
    boost::locale::collate_level compareLevel;
    switch (level)
    {
    case ComparisonLevel::BaseLetters:
        compareLevel = boost::locale::collate_level::primary;
        break;
    default: case ComparisonLevel::BaseAccents:
        compareLevel = boost::locale::collate_level::secondary;
        break;
    case ComparisonLevel::BaseCaseAccents:
        compareLevel = boost::locale::collate_level::tertiary;
        break;
    case ComparisonLevel::Identical:
        compareLevel = boost::locale::collate_level::quaternary;
        break;
    case ComparisonLevel::IdenticalCodepoints:
        compareLevel = boost::locale::collate_level::identical;
        break;
    }
    return facet.compare(compareLevel, m_str, other.m_str);
}
#endif // KRAKEN_ENABLE_UNICODE_SUPPORT

String& String::operator+=(const String& other)
{
    m_str += other.m_str;
    return *this;
}

void String::insert(const String& ins, const_iterator pos)
{
    m_str.insert(pos.m_pos - m_str.begin(), ins.m_str);
}

void String::erase(const_iterator first, const_iterator last)
{
    // Workaround, as const_iterators doesn't work (g++-4.8.1).
    // https://gcc.gnu.org/bugzilla/show_bug.cgi?id=54577
    m_str.erase(first.pos(), last.m_pos - first.m_pos);
}

void String::replace(const_iterator first, const_iterator last, const String& r)
{
    // Workaround, as const_iterators doesn't work (g++-4.8.1).
    // https://gcc.gnu.org/bugzilla/show_bug.cgi?id=54577
    m_str.replace(first.pos(), last.m_pos - first.m_pos, r);
}

void String::replace(const_iterator first, const_iterator last,
                     const_iterator repf, const_iterator repl)
{
    // Workaround, as const_iterators doesn't work (g++-4.8.1).
    // https://gcc.gnu.org/bugzilla/show_bug.cgi?id=54577
    m_str.replace(first.pos(), last.pos() - first.pos(),
                  &*repf.m_pos, repl.pos() - repf.pos());
}


String String::substr(const_iterator first, const_iterator last) const
{
    return m_str.substr(first.pos(), last.m_pos - first.m_pos);
}

void String::copy(U32* dataOut, const_iterator first, const_iterator last)
{
    while (first != last)
    {
        *dataOut++ = *first++;
    }
}

void String::copy(U8* dataOut, const_iterator first, const_iterator last)
{
    auto f = first.m_pos;
    auto l = last.m_pos;
    while (f != l)
    {
        *dataOut++ = *f++;
    }
}

String::const_iterator String::find(U32 cp, const_iterator first) const
{
    auto it = first;
    for (; it != end(); ++it)
    {
        if (*it == cp) break;
    }
    return it;
}

String::const_iterator String::find(const String& s, const_iterator first) const
{
    return StringIterator(m_str, m_str.begin() + m_str.find(s.m_str,
                                                            first.pos()));
}

U32 String::toWideChar(char c, const std::locale& loc) const
{
    const auto& cvt = std::use_facet<std::ctype<wchar_t>>(loc);
    wchar_t wc = cvt.widen(c);
    return ((wc == static_cast<wchar_t>(-1)) ? invalidCodepoint : wc);
}

char String::toNarrowChar(U32 cp, char repl, const std::locale& loc) const
{
    const auto& cvt = std::use_facet<std::ctype<wchar_t>>(loc);
    wchar_t wc = static_cast<wchar_t>(cp);
    return cvt.narrow(wc, repl);
}

#ifdef KRAKEN_ENABLE_UNICODE_SUPPORT

String uppercase(const String& s)
{
    return boost::locale::to_upper(s.str());
}

String lowercase(const String& s)
{
    return boost::locale::to_lower(s.str());
}

String titlecase(const String& s)
{
    return boost::locale::to_title(s.str());
}

String foldcase(const String& s)
{
    return boost::locale::fold_case(s.str());
}

String normalise(const String& s)
{
    return normaliseNFC(s);
}

String normaliseNFD(const String& s)
{
    return boost::locale::normalize(s.str(), boost::locale::norm_nfd);
}

String normaliseNFC(const String& s)
{
    return boost::locale::normalize(s.str(), boost::locale::norm_nfc);
}

String normaliseNFKD(const String& s)
{
    return boost::locale::normalize(s.str(), boost::locale::norm_nfkd);
}

String normaliseNFKC(const String& s)
{
    return boost::locale::normalize(s.str(), boost::locale::norm_nfkc);
}

bool isAlnum(U32 codepoint) { return u_isalnum(codepoint); }
bool isAlpha(U32 codepoint) { return u_isalpha(codepoint); }
bool isLower(U32 codepoint) { return u_islower(codepoint); }
bool isUpper(U32 codepoint) { return u_isupper(codepoint); }
bool isDigit(U32 codepoint) { return u_isdigit(codepoint); }
bool isHexDigit(U32 codepoint) { return u_isxdigit(codepoint); }
bool isControl(U32 codepoint) { return u_iscntrl(codepoint); }
bool isGraphical(U32 codepoint) { return u_isgraph(codepoint); }
bool isWhitespace(U32 codepoint) { return u_isspace(codepoint); }
bool isBlank(U32 codepoint) { return u_isblank(codepoint); }
bool isPunct(U32 codepoint) { return u_ispunct(codepoint); }

#endif // KRAKEN_ENABLE_UNICODE_SUPPORT

} // namespace core
