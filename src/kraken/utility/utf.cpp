#include "kraken/types.hpp"
#include <kraken/utility/utf.hpp>

namespace kraken
{

namespace
{

unsigned toIndex(UnicodeEncoding ue)
{
    return static_cast<unsigned>(ue);
}

UnicodeEncoding toEncoding(unsigned u)
{
    if (u >= 6 || !u) return UnicodeEncoding::Unknown;
    return static_cast<UnicodeEncoding>(u);
}

} // end anonymous namespace

UnicodeEncoding detectUnicodeEncoding(U8 bytes[16]) noexcept
{
    bool possible[6] =
    {
    // Unknown   8  16LE  16BE  32LE  32BE
        true, true, true, true, true, true,
    };

    unsigned numPossible = 0;

    for (unsigned i = 1; i < 6; ++i)
    {
        auto* begin = bytes;
        auto* end = begin+16;
        while (begin != end)
        {
            U32 cp;
            begin = decode(begin, end, cp, toEncoding(i));
            if (cp == invalidCodepoint)
            {
                if (toEncoding(i) == UnicodeEncoding::UTF8 && end-begin < 4)
                {
                    // Last codepoint may be truncated. Therefore the text may
                    // still be UTF-8 encoded.
                    break;
                }
                if ((toEncoding(i) == UnicodeEncoding::UTF16LE
                     || toEncoding(i) == UnicodeEncoding::UTF16BE)
                    && begin == end)
                {
                    // Last codepoint may be a surrogate pair which was split.
                    // That should not remove UTF-16 from consideration.
                    break;
                }
                // No truncation is possible for UTF-16 and UTF-32 (as we have
                // 16 bytes), so that encoding is definitely wrong if we get a
                // bad codepoint.
                possible[i] = false;
                break;
            }
        }
        numPossible += possible[i];
    }

    if (!numPossible)
    {
        return UnicodeEncoding::Unknown;
    }

    // If just one encoding is valid, then that is probably the one the text is
    // encoded with.
    if (numPossible == 1)
    {
        for (unsigned i = 1; i < 6; ++i)
        {
            if (possible[i]) return toEncoding(i);
        }
    }

    // Otherwise we have several candidates. First, check for a BOM.
    // UTF-8   : 0xEF 0xBB 0xBF
    // UTF-16LE: 0xFF 0xFE
    // UTF-16BE: 0xFE 0xFF
    // UTF-32LE: 0xFF 0xFE 0x00 0x00
    // UTF-32BE: 0x00 0x00 0xFE 0xFF
    switch (bytes[0])
    {
    case 0xFE:
        if (bytes[1] == 0xFF)
        {
            return possible[toIndex(UnicodeEncoding::UTF16BE)]
                   ? UnicodeEncoding::UTF16BE
                   : UnicodeEncoding::Unknown;
        }
        break;
    case 0xFF:
        if (bytes[1] == 0xFE)
        {
            if (!possible[toIndex(UnicodeEncoding::UTF16LE)])
            {
                return possible[toIndex(UnicodeEncoding::UTF32LE)]
                       ? UnicodeEncoding::UTF32LE
                       : UnicodeEncoding::Unknown;
            }
            if (!possible[toIndex(UnicodeEncoding::UTF32LE)])
            {
                return possible[toIndex(UnicodeEncoding::UTF16LE)]
                       ? UnicodeEncoding::UTF16LE
                       : UnicodeEncoding::Unknown;
            }
            // Both are possible. If so, every four bytes look like:
            // 0x?? 0x?? 0b?000000 0x00
            // The most likely encoding is then UTF-32LE, as it is unlikely
            // every second character is either U+0000 or U+0001.
            // Also, if your text is detected as the wrong encoding but you use
            // UTF-X, X != 8, then you have only yourself to blame.
            return UnicodeEncoding::UTF32LE;
        }
        break;
    case 0x00:
        if (bytes[1] == 0x00 && bytes[2] == 0xFE && bytes[3] == 0xFF)
        {
            // The BOM is assumed to be the very first part of the text, so this
            // can only be UTF-32BE.
            return possible[toIndex(UnicodeEncoding::UTF32BE)]
                   ? UnicodeEncoding::UTF32BE
                   : UnicodeEncoding::Unknown;
        }
        break;
    case 0xEF:
        if (bytes[1] == 0xBB && bytes[2] == 0xBF)
        {
            // 0xEF 0xBB 0xBF almost always signifies UTF-8 text, although it
            // may occur as part of UTF-16 text. But this is unlikely, and
            // again using UTF-X, X != 8, you have no-one to blame but yourself.
            if (possible[toIndex(UnicodeEncoding::UTF8)])
            {
                return UnicodeEncoding::UTF8;
            }
            // UTF-32[LB]E is not possible.
            if (!possible[toIndex(UnicodeEncoding::UTF16LE)])
            {
                return possible[toIndex(UnicodeEncoding::UTF16BE)]
                       ? UnicodeEncoding::UTF16BE
                       : UnicodeEncoding::Unknown;
            }
            if (!possible[toIndex(UnicodeEncoding::UTF16BE)])
            {
                return possible[toIndex(UnicodeEncoding::UTF16LE)]
                       ? UnicodeEncoding::UTF16LE
                       : UnicodeEncoding::Unknown;
            }
            // Both UTF-16LE and UTF-16BE are possible, but UTF-8 is not.
            // We could scan for patterns like 0x?? 0x00 0x?? 0x00 ..., but this
            // does not necessarily work.
            return UnicodeEncoding::Unknown;
        }
    }
    // There's no BOM and several possible encodings are valid.
    // If UTF-8 is valid, then that is assumed to be the encoding (as UTF-8 is
    // the most common one), but if not, we don't know which it is.
    return possible[toIndex(UnicodeEncoding::UTF8)]
           ? UnicodeEncoding::UTF8
           : UnicodeEncoding::Unknown;
}

} // namespace kraken
