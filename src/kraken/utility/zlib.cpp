#include "kraken/types.hpp"
#include "kraken/utility/hufftree.hpp"
#include <algorithm>
#include <cstddef>
#include <kraken/utility/zlib.hpp>
#include <vector>

namespace kraken
{

Inflate::Inflate()
{
    clear();
}

void Inflate::setInput(const U8* begin, const U8* end)
{
    m_inputBegin = begin;
    m_inputEnd = end;
    m_bitpos &= 7;
    m_availbits = (m_inputEnd - m_inputBegin) << 3;
    m_status &= ~InflateStatus::RequiresInput;
}

void Inflate::setOutput(U8* begin, const U8* end)
{
    m_outputAt = begin;
    m_outputEnd = end;
}

bool Inflate::inflate()
{
    if (m_state == State::StreamEnd)
    {
        return false;
    }
    if (m_state == State::ExpectChecksum)
    {
        if (!requireBits(32)) return false;
        U32 sum = read(8) << 24;
        sum |= read(8) << 16;
        sum |= read(8) << 8;
        sum |= read(8);
        if (sum == m_checksum.sum())
        {
            m_status = InflateStatus::Finished;
        }
        else
        {
            m_status = InflateStatus::BadChecksum;
        }
        m_state = State::StreamEnd;
        return m_status == InflateStatus::Finished;
    }
    m_status &= ~InflateStatus::RequiresOutput;
    do
    {
        if (!processBlock()) return false;
        if (m_state == State::ExpectChecksum)
        {
            return inflate();
        }
    } while (m_status == InflateStatus::Good && !atOutputEnd());
    return false;
}

bool Inflate::processBlock()
{
    if (m_state == State::ExpectHeader)
    {
        if (!processHeader()) return false;
        m_state = State::ExpectBlock;
    }

    if (m_state == State::ExpectBlock)
    {
        if (!requireBits(3)) return false;
        m_finalBlock = read(1);
        m_blocktype = read(2);
        clearBlockInfo();
        if (m_blocktype == 1) generateFixedTrees();
        m_state = State::InsideBlock;
    }

    if (m_blocktype == 0)
    {
        return processUncompressed();
    }
    if (m_blocktype < 3)
    {
        return processCompressed();
    }
    m_status |= InflateStatus::BadInput;
    return false;
}

bool Inflate::processUncompressed()
{
    if (!m_litblockinfo.lengthRead)
    {
        ensureByteAligned();
        if (!requireBits(32)) return false;
        U16 rlen = 0;
        rlen = readAlignedByte();
        rlen |= readAlignedByte() << 8;
        m_litblockinfo.remainingLength = rlen;
        rlen = readAlignedByte();
        rlen |= readAlignedByte() << 8;
        if ((m_litblockinfo.remainingLength ^ rlen) != 0xffff)
        {
            m_status |= InflateStatus::BadInput;
            return false;
        }
        m_litblockinfo.lengthRead = true;
    }
    while (m_litblockinfo.remainingLength && !atOutputEnd() && requireBits(8))
    {
        appendByte(readAlignedByte());
        --m_litblockinfo.remainingLength;
    }
    if (!m_litblockinfo.remainingLength)
    {
        m_state = State::ExpectBlock;
        if (m_finalBlock)
        {
            m_state = State::ExpectChecksum;
        }
        return true;
    }
    if (requireBits(8)) requireOutput();
    return false;
}

bool Inflate::processCompressed()
{
    constexpr U8 lenExtraBits[31] =
    {
        0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3,
        4, 4, 4, 4, 5, 5, 5, 5, 0,
        0, 0
    };
    constexpr U16 lenExtraBase[31] =
    {
        3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31,
        35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258,
        0, 0
    };
    constexpr U8 distExtraBits[31] =
    {
        0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6,
        7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13,
        0
    };
    constexpr U16 distExtraBase[31] =
    {
        1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97,
        129, 193, 257, 385, 513, 769, 1025, 1537, 2049,
        3073, 4097, 6145, 8193, 12289, 16385, 24577,
        0
    };
    if (!m_cblock.treesRead)
    {
        if (!readDynamicTrees()) return false;
    }

    if (!ensureCopying()) return false;

    while (!atOutputEnd() && m_status == InflateStatus::Good)
    {
        U16 symbol;
        U8 symbits;
        if (!(symbits = readSymbol(m_littree, symbol))) return false;
        if (symbol < 256)
        {
            appendByte(static_cast<U8>(symbol));
        }
        else if (symbol == 256)
        {
            m_state = State::ExpectBlock;
            if (m_finalBlock)
            {
                m_state = State::ExpectChecksum;
                ensureByteAligned();
            }
            return true;
        }
        else if (symbol < 256 + 31)
        {
            auto lebits = lenExtraBits[symbol-257];
            if (!requireBits(lebits))
            {
                // Put back read so far.
                unget(symbits);
                return false;
            }
            auto length = lenExtraBase[symbol-257] + (lebits ? read(lebits) : 0);


            U8 distbits = 0;
            U16 distsym;
            if (m_blocktype == 1)
            {
                if (!requireBits(5))
                {
                    unget(symbits + lebits);
                    return false;
                }
                distsym = 0;
                auto nb = read(5);
                distsym |= nb >> 4;
                distsym |= (nb >> 2) & 2;
                distsym |= nb & 4;
                distsym |= (nb << 2) & 8;
                distsym |= (nb << 4) & 16;
                distbits = 5;
            }
            else
            {
                if (!(distbits = readSymbol(m_disttree, distsym)))
                {
                    unget(symbits + lebits);
                    return false;
                }
            }
            auto debits = distExtraBits[distsym];
            if (!requireBits(debits))
            {
                // Put back read so far.
                unget(symbits + lebits + distbits);
                return false;
            }
            auto dist = distExtraBase[distsym] + longRead(debits);
            m_cblock.dupLen = length;
            m_cblock.dupDist = dist;
            if (!ensureCopying()) return false;
        }
        else
        {
            m_status |= InflateStatus::BadInput;
            return false;
        }
    }

    requireOutput();

    return false;
}

bool Inflate::ensureCopying()
{
    while (m_cblock.dupLen && !atOutputEnd())
    {
        appendByte(getPrevByte(m_cblock.dupDist));
        --m_cblock.dupLen;
    }
    if (m_cblock.dupLen)
    {
        return requireOutput();
    }
    return true;
}

void Inflate::appendByte(U8 byte)
{
    m_checksum.append(&byte, 1);
    m_window[m_windowpos++&0x7fff] = byte;
    m_windowpos &= 0x7fff;
    *m_outputAt++ = byte;
}

U8 Inflate::getPrevByte(U32 dist)
{
    return m_window[(m_windowpos + 0x8000 - dist) & 0x7fff];
}

U8 Inflate::readSymbol(const priv::HufftreeDecoder& reader, U16& output)
{
    if (!requireBits(1)) return 0;
    auto left = bitsLeft();

    auto encoded = longRead(16);
    auto [decoded, symbolLength] = reader.decodeSymbol(encoded);
    if (!symbolLength)
    {
        m_status |= InflateStatus::BadInput;
        unget(16);
        return 0;
    }
    if (symbolLength > left)
    {
        m_status |= InflateStatus::RequiresInput;
        unget(16);
        return 0;
    }
    unget(16-symbolLength);
    output = decoded;
    return symbolLength;
}

void Inflate::clearLitblock()
{
    m_litblockinfo.remainingLength = 0;
    m_litblockinfo.checksumRead = false;
    m_litblockinfo.lengthRead = false;
}

void Inflate::clearDtree()
{
    m_dtree.codelengths.clear();
    m_dtree.cltree.clear();
    m_dtree.litcodes = 0;
    m_dtree.distcodes = 0;
    m_dtree.clcodes = 0;
    m_dtree.currReading = 0;
}

void Inflate::clearCblock()
{
    m_cblock.dupLen = 0;
    m_cblock.dupDist = 0;
    m_cblock.treesRead = false;
}

void Inflate::clearBlockInfo()
{
    clearLitblock();
    clearDtree();
    clearCblock();
}

void Inflate::clear()
{
    //clearBlockInfo();

    m_littree.clear();
    m_disttree.clear();
    m_window.clear();
    m_window.resize(32768, 0);
    m_checksum.clear();

    m_inputBegin = nullptr;
    m_inputEnd = nullptr;
    m_outputAt = nullptr;
    m_outputEnd = nullptr;
    m_bitpos = 0;
    m_availbits = 0;
    m_status = InflateStatus::Good;

    m_state = State::ExpectHeader;
    m_windowpos = 0;
    m_blocktype = 4;
    m_finalBlock = false;
}

bool Inflate::requireBits(U64 count)
{
    if (bitsLeft() >= count) return true;
    m_status |= InflateStatus::RequiresInput;
    return false;
}

bool Inflate::requireOutput()
{
    if (!atOutputEnd()) return true;
    m_status |= InflateStatus::RequiresOutput;
    return false;
}

bool Inflate::readHufftreeLengths(const priv::HufftreeDecoder& decoder,
                                  U16 codeCount,
                                  std::vector<U8>& clens)
{
    while (clens.size() < codeCount)
    {
        U16 symbol;
        U8 symbits;
        if (!(symbits = readSymbol(decoder, symbol))) return false;
        if (symbol < 16) clens.push_back(static_cast<U8>(symbol));
        else
        {
            U8 extraBits = 2;
            U8 times = 3;
            if (symbol == 17) extraBits = 3;
            if (symbol == 18)
            {
                extraBits = 7;
                times = 11;
            }
            if (!requireBits(extraBits))
            {
                unget(symbits);
                return false;
            }
            times += read(extraBits);
            U8 repeat = 0;
            if (!clens.empty() && symbol == 16) repeat = clens.back();
            while (times)
            {
                clens.push_back(repeat);
                --times;
            }
        }
    }
    return true;
}

void Inflate::generateFixedTrees()
{
    std::vector<U8> bitlen(288, 8);
    for (size_t i = 144; i < 256; ++i)
    {
        bitlen[i] = 9;
    }
    for (size_t i = 256; i < 280; ++i)
    {
        bitlen[i] = 7;
    }
    m_littree.create(bitlen);
    m_cblock.treesRead = true;
}

bool Inflate::readDynamicTrees()
{
    auto& clen = m_dtree.codelengths;
    if (!m_dtree.litcodes)
    {
        if (!requireBits(14)) return false;
        m_dtree.litcodes = read(5) + 257;
        m_dtree.distcodes = read(5) + 1;
        m_dtree.clcodes = read(4) + 4;
    }
    if (m_dtree.currReading == 0)
    {
        if (!requireBits(m_dtree.clcodes*3)) return false;
        clen.clear();
        clen.resize(19);

        constexpr U8 clperm[19] =
        {
            16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15
        };

        U8 maxBitlen = 0;
        for (size_t i = 0; i < m_dtree.clcodes; ++i)
        {
            clen[clperm[i]] = read(3);
            maxBitlen = std::max(clen[clperm[i]], maxBitlen);
        }
        m_dtree.cltree.create(clen);
        m_dtree.currReading = 1;
        clen.clear();
    }
    if (m_dtree.currReading == 1)
    {
        if (!readHufftreeLengths(m_dtree.cltree,
                                 m_dtree.litcodes + m_dtree.distcodes, clen))
        {
            return false;
        }
        std::vector<U8> lens;
        for (size_t i = 0; i < m_dtree.litcodes; ++i)
        {
            lens.push_back(clen[i]);
        }
        // Replace the (fixed) number of cache bits with something calculated
        // from the data (e.g. if there is one code of length 15 and the rest
        // are of lengths 6 and 9, then the number of bits should either be 6,
        // which should fit in any cache or 9, which *might* fit. Try auto-
        // detecting cache size, perhaps).
        m_littree.create(lens);
        lens.clear();
        for (size_t i = 0; i < m_dtree.distcodes; ++i)
        {
            lens.push_back(clen[m_dtree.litcodes+i]);
        }
        m_disttree.create(lens);
        m_dtree.currReading = 2;
        m_cblock.treesRead = true;
        clen.clear();
    }
    return true;
}

bool Inflate::processHeader()
{
    if (!requireBits(16)) return false;
    U8 cm = read(4);
    if (cm != 8)
    {
        // Only CM=8 is defined.
        m_status |= InflateStatus::BadInput;
        return false;
    }
    U8 cinfo = read(4);
    if (cinfo >= 8)
    {
        // CINFO must be less than 8.
        m_status |= InflateStatus::BadInput;
        return false;
    }
    U8 fcheck = read(5);
    U8 fdict = read(1);
    if (fdict)
    {
        m_status |= InflateStatus::RequiresDictionary;
        return false;
    }
    U8 flevel = read(2);
    if ((U16(cinfo << 12) | U16(cm << 8) | U16(flevel << 6)
        | U16(fdict << 5) | U16(fcheck)) % 31)
    {
        m_status |= InflateStatus::BadInput;
        return false;
    }
    m_window.clear();
    m_window.resize(256 << cinfo, 0);
    m_windowpos = 0;
    return true;
}

namespace
{

constexpr U16 modPowerTwo[17] =
{
    0x0, 0x1, 0x3, 0x7, 0xf, 0x1f, 0x3f, 0x7f, 0xff,
    0x1ff, 0x3ff, 0x7ff, 0xfff, 0x1fff, 0x3fff, 0x7fff, 0xffff
};

} // end anonymous namespace

U16 Inflate::read(U8 bits)
{
    U8 b0 = *(m_inputBegin+(m_bitpos>>3));
    U8 b1 = bitsLeft() > 8 ? *(m_inputBegin+(m_bitpos>>3)+1) : 0;
    U16 read = (b0 | (b1 << 8)) >> (m_bitpos & 7);
    m_bitpos += bits;
    return modPowerTwo[bits] & read;
}

U16 Inflate::longRead(U8 bits)
{
    U32 result{0};
    auto remainingBitsMinusOne = m_bitpos < m_availbits
                               ? m_availbits - m_bitpos - 1
                               : 0;
    auto extraBytesLeft = remainingBitsMinusOne >> 3;
    switch (extraBytesLeft)
    {
    default:
        result = *(m_inputBegin+(m_bitpos>>3)+2) << 16;
        // Fallthrough
    case 1:
        result |= *(m_inputBegin+(m_bitpos>>3)+1) << 8;
        // Fallthrough
    case 0:
        result |= *(m_inputBegin+(m_bitpos>>3));
    }
    result >>= (m_bitpos & 7);
    m_bitpos += bits;
    return result & modPowerTwo[bits];
}

void Inflate::ensureByteAligned()
{
    if (m_bitpos&7) m_bitpos = ((m_bitpos >> 3) + 1) << 3;
}

void Inflate::unget(U32 bits)
{
    m_bitpos -= bits;
}

U8 Inflate::readAlignedByte()
{
    auto rd = *(m_inputBegin+(m_bitpos>>3));
    m_bitpos += 8;
    return rd;
}

} // namespace kraken
