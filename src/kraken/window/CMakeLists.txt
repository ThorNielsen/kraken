find_package(glfw3 3.3 REQUIRED)

set(INCDIR ${PROJECT_SOURCE_DIR}/include/kraken/window)
set(SRCDIR ${PROJECT_SOURCE_DIR}/src/kraken/window)

set(PUBHEADERS
    ${INCDIR}/display.hpp
    ${INCDIR}/input.hpp
    ${INCDIR}/window.hpp
)

set(SOURCES
    ${SRCDIR}/display.cpp
    ${SRCDIR}/glfw.cpp
    ${SRCDIR}/input.cpp
    ${SRCDIR}/window.cpp
)

add_library(kraken-window SHARED ${SOURCES})
target_sources(kraken-window PUBLIC FILE_SET HEADERS BASE_DIRS ${PROJECT_SOURCE_DIR}/include FILES ${PUBHEADERS})
set_target_properties(kraken-window PROPERTIES LINKER_LANGUAGE CXX)
target_link_libraries(kraken-window kraken-utility kraken-image glfw)
install(TARGETS kraken-window
        LIBRARY DESTINATION ${CMAKE_INSTALL_PREFIX}/lib
        FILE_SET HEADERS)
