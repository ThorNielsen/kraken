#include <kraken/window/display.hpp>
#include <kraken/window/glfw.hpp>

#include <functional>
#include <map>

namespace kraken
{

namespace window
{

DisplayID* g_displayIDhead = nullptr;
std::function<void(DisplayID)> g_displayConnectCallback = [](DisplayID){};
std::function<void(DisplayID)> g_displayDisconnectCallback = [](DisplayID){};
std::map<void*, std::function<void(DisplayID)>> g_disconnectCallbacks;

// To be able to access the properties of DisplayID, we need to use some helper
// functions since we don't want to expose the inner workings to users of the
// library. Creating these and friending them seems to be the least ugly way to
// do so (the best would be to have them in a private namespace so they are
// local to this file, but this doesn't seem to be possible).
void* priv_getID(const DisplayID& did)
{
    if (did.m_disconnected) return nullptr;
    return did.m_display;
}

void priv_setID(DisplayID& did, void* id)
{
    did.m_disconnected = false;
    did.m_display = id;
}

void priv_markDisconnect(DisplayID& did)
{
    did.m_disconnected = true;
}

DisplayID* priv_next(DisplayID* did)
{
    return did->m_next;
}

namespace priv
{

void monitorEventCallback(GLFWmonitor* mon, int event)
{
    DisplayID id;
    priv_setID(id, mon);
    if (event == GLFW_CONNECTED)
    {
        g_disconnectCallbacks[mon] = [](DisplayID){};
        g_displayConnectCallback(id);
    }
    else if (event == GLFW_DISCONNECTED)
    {
        DisplayID* currID = g_displayIDhead;
        while (currID)
        {
            if (static_cast<GLFWmonitor*>(priv_getID(*currID)) == mon)
            {
                priv_markDisconnect(*currID);
            }
            currID = priv_next(currID);
        }
        g_disconnectCallbacks[mon](id);
        g_disconnectCallbacks.erase(mon);
        g_displayDisconnectCallback(id);
    }
}

} // namespace priv


DisplayID::DisplayID()
{
    construct();
}

DisplayID::DisplayID(const DisplayID& other)
{
    construct();
    m_display = other.m_display;
    m_disconnected = other.m_disconnected;
}

DisplayID& DisplayID::operator=(const DisplayID& other)
{
    // Note: There is no need to call construct() since this object has already
    // been constructed.
    m_display = other.m_display;
    m_disconnected = other.m_disconnected;
    return *this;
}

DisplayID::~DisplayID()
{
    destroy();
}

void DisplayID::construct()
{
    priv::registerGLFWDependent();
    m_display = nullptr;
    m_prev = nullptr;
    m_next = g_displayIDhead;
    g_displayIDhead = this;
    if (m_next) m_next->m_prev = this;
}

void DisplayID::destroy()
{
    priv::deregisterGLFWDependent();
    if (m_prev) m_prev->m_next = m_next;
    if (m_next) m_next->m_prev = m_prev;
    if (g_displayIDhead == this) g_displayIDhead = m_next;
}

std::vector<DisplayID> getMonitors()
{
    priv::GLFWGuard guard;

    std::vector<DisplayID> displays;
    int numMonitors = 0;
    GLFWmonitor** monitors = glfwGetMonitors(&numMonitors);
    displays.resize(numMonitors);
    for (int i = 0; i < numMonitors; ++i)
    {
        priv_setID(displays[i], monitors[i]);
    }
    return displays;
}

DisplayID getPrimaryDisplay()
{
    priv::GLFWGuard guard;

    auto* mon = glfwGetPrimaryMonitor();
    DisplayID primary;
    priv_setID(primary, mon);
    return primary;
}

VideoMode fromGLFW(const GLFWvidmode* mode)
{
    VideoMode vm;
    vm.width = mode->width;
    vm.height = mode->height;
    vm.refreshRate = mode->refreshRate;
    vm.redBits = mode->redBits;
    vm.greenBits = mode->greenBits;
    vm.blueBits = mode->blueBits;
    return vm;
}

DisplayInfo getDisplayInfo(DisplayID id)
{
    priv::GLFWGuard guard;

    if (!priv_getID(id)) return DisplayInfo{};
    GLFWmonitor* mon = static_cast<GLFWmonitor*>(priv_getID(id));
    int numVidModes = 0;
    auto* vidmodes = glfwGetVideoModes(mon, &numVidModes);
    DisplayInfo info;
    for (int i = 0; i < numVidModes; ++i)
    {
        info.availableModes.push_back(fromGLFW(&vidmodes[i]));
    }
    const auto* gammaramp = glfwGetGammaRamp(mon);
    info.gammaRampRed.resize(gammaramp->size);
    info.gammaRampGreen.resize(gammaramp->size);
    info.gammaRampBlue.resize(gammaramp->size);
    for (size_t i = 0; i < gammaramp->size; ++i)
    {
        info.gammaRampRed[i] = gammaramp->red[i];
        info.gammaRampGreen[i] = gammaramp->green[i];
        info.gammaRampBlue[i] = gammaramp->blue[i];
    }
    info.name = glfwGetMonitorName(mon);
    int x, y;
    glfwGetMonitorPhysicalSize(mon, &x, &y);
    info.physicalWidth = x;
    info.physicalHeight = y;
    glfwGetMonitorPos(mon, &x, &y);
    info.virtualXPos = x;
    info.virtualYPos = y;
    return info;
}

VideoMode currentVideoMode(DisplayID id)
{
    priv::assertOnMainThread();
    auto* mon = static_cast<GLFWmonitor*>(priv_getID(id));
    return fromGLFW(glfwGetVideoMode(mon));
}

void setDisplayConnectCallback(std::function<void(DisplayID)> cbfun)
{
    // TODO: Synchronise this across several threads.
    g_displayConnectCallback = cbfun;
}

void setDisplayDisconnectCallback(std::function<void(DisplayID)> cbfun)
{
    // TODO: Synchronise this across several threads.
    g_displayDisconnectCallback = cbfun;
}

void setDisplayDisconnectCallback(DisplayID id,
                                  std::function<void(DisplayID)> cbfun)
{
    // TODO: Synchronise this across several threads.
    void* mon = priv_getID(id);
    auto loc = g_disconnectCallbacks.find(mon);
    if (loc == g_disconnectCallbacks.end())
    {
        throw std::runtime_error("Trying to set callback for non-existant "
                                 "display.");
    }
    loc->second = cbfun;
}


} // namespace window

} // namespace kraken
