#include <kraken/window/glfw.hpp>
#include <kraken/window/input.hpp>

#include <iostream>
#include <stdexcept>
#include <thread>

namespace kraken
{

namespace window
{

namespace priv
{

// Unfortunately GLFW both requires that most interesting functions are called
// from the main thread and callbacks can only be given pointers to C functions.
// Therefore we need a few global variables to provide callbacks for individual
// windows and to ensure that everything is called from the main thread.
const std::thread::id g_mainThreadID = std::this_thread::get_id();
size_t g_numGLFWObjectsCreated = 0;
std::atomic<size_t> g_glfwErrorCount{0};

// Manual forward declarations for functions needed here.
Key keyFromGLFW(int key);
void monitorEventCallback(GLFWmonitor* mon, int event);

void errorCallback(int code, const char* desc)
{
    ++g_glfwErrorCount;
    std::cerr << "GLFW error "
              << std::hex << code << std::dec
              << ": " << desc << "\n";
}

WindowCallbacks emptyCallbacks()
{
    WindowCallbacks cbs;
    cbs.close = [](){};
    cbs.focus = [](){};
    cbs.defocus = [](){};
    cbs.minimise = [](){};
    cbs.maximise = [](){};
    cbs.restore = [](bool){};
    cbs.redraw = [](){};

    cbs.position = [](S32, S32){};
    cbs.resize = [](U32, U32){};
    cbs.fbresize = [](U32, U32){};

    cbs.keypress = [](Key, Scancode, KeyModifiers){};
    cbs.keyrepeat = [](Key, Scancode, KeyModifiers){};
    cbs.keyrelease = [](Key, Scancode, KeyModifiers){};
    cbs.utfchar = [](U32){};
    cbs.mousepress = [](Mouse, KeyModifiers){};
    cbs.mouserelease = [](Mouse, KeyModifiers){};
    cbs.scroll = [](F64, F64){};

    cbs.cursorposition = [](F64, F64){};
    cbs.cursorenter = [](){};
    cbs.cursorleave = [](){};

    cbs.stringdrop = [](std::vector<String>){};
    return cbs;
}

void assertOnMainThread()
{
    if (g_mainThreadID != std::this_thread::get_id())
    {
        throw std::runtime_error("Function must be called on main thread.");
    }
}

void initialiseGLFW()
{
    glfwSetErrorCallback(errorCallback);
    if (!glfwInit())
    {
        throw std::runtime_error("glfwInit() failed.");
    }
    glfwSetMonitorCallback(monitorEventCallback);

    /// DANGER: This initialisation is crucial for avoiding possible threading
    /// problems. If this function is changed/moved, please update
    /// window/input.cpp to reflect this.
    // Call keyFromGLFW once in order to perform initialisation once. This
    // avoids any kind of threading problems (since the function uses static
    // variables, it is possible that if two threads each call this function,
    // one thread initialises the variables while the other uses a possibly
    // uninitialised variable).
    (void)keyFromGLFW(GLFW_KEY_A);
}

void registerGLFWDependent()
{
    assertOnMainThread();
    if (!g_numGLFWObjectsCreated)
    {
        initialiseGLFW();
    }
    ++g_numGLFWObjectsCreated;
}

void deregisterGLFWDependent()
{
    assertOnMainThread();
    if (!g_numGLFWObjectsCreated)
    {
        throw std::logic_error("#deregistered > #registered");
    }
    --g_numGLFWObjectsCreated;
    if (!g_numGLFWObjectsCreated)
    {
        glfwTerminate();
    }
}

void setEmptyCallbacks(GLFWwindow* /*window*/, void* callbacks)
{
    *static_cast<priv::WindowCallbacks*>(callbacks) = emptyCallbacks();
}

void closeCallback(GLFWwindow* window)
{
    static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->close();
}

void focusCallback(GLFWwindow* window, int focused)
{
    if (focused)
    {
        static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->focus();
    }
    else
    {
        static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->defocus();
    }
}

void minimiseCallback(GLFWwindow* window, int minimise)
{
    if (minimise)
    {
        static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->minimise();
    }
    else
    {
        static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->restore(true);
    }
}

void maximiseCallback(GLFWwindow* window, int maximise)
{
    if (maximise)
    {
        static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->maximise();
    }
    else
    {
        static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->restore(false);
    }
}

void redrawCallback(GLFWwindow* window)
{
    static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->redraw();
}


void positionCallback(GLFWwindow* window, int xpos, int ypos)
{
    static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->position(xpos, ypos);
}

void resizeCallback(GLFWwindow* window, int width, int height)
{
    static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->resize(width, height);
}

void framebufferResizeCallback(GLFWwindow* window, int width, int height)
{
    static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->fbresize(width, height);
}

void keyCallback(GLFWwindow* window, int key, int scan, int action, int mods)
{
    Key translated = keyFromGLFW(key);
    KeyModifiers keymods;
    keymods.ctrl     = !!(mods & GLFW_MOD_CONTROL);
    keymods.alt      = !!(mods & GLFW_MOD_ALT);
    keymods.shift    = !!(mods & GLFW_MOD_SHIFT);
    keymods.super    = !!(mods & GLFW_MOD_SUPER);
    keymods.numLock  = !!(mods & GLFW_MOD_NUM_LOCK);
    keymods.capsLock = !!(mods & GLFW_MOD_CAPS_LOCK);
    if (action == GLFW_PRESS)
    {
        static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->keypress(translated, scan, keymods);
    }
    else if (action == GLFW_REPEAT)
    {
        static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->keyrepeat(translated, scan, keymods);
    }
    else if (action == GLFW_RELEASE)
    {
        static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->keyrelease(translated, scan, keymods);
    }
}

void utfcharCallback(GLFWwindow* window, U32 codepoint)
{
    static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->utfchar(codepoint);
}

void mouseCallback(GLFWwindow* window, int button, int action, int mods)
{
    KeyModifiers keymods;
    keymods.ctrl  = !!(mods & GLFW_MOD_CONTROL);
    keymods.alt   = !!(mods & GLFW_MOD_ALT);
    keymods.shift = !!(mods & GLFW_MOD_SHIFT);
    keymods.super = !!(mods & GLFW_MOD_SUPER);
    auto trButton = static_cast<Mouse>(button-GLFW_MOUSE_BUTTON_1);
    if (action == GLFW_PRESS)
    {
        static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->mousepress(trButton, keymods);
    }
    else if (action == GLFW_RELEASE)
    {
        static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->mouserelease(trButton, keymods);
    }
}

void scrollCallback(GLFWwindow* window, double x, double y)
{
    static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->scroll(x, y);
}

void cursorPositionCallback(GLFWwindow* window, double x, double y)
{
    static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->cursorposition(x, y);
}

void cursorEnterCallback(GLFWwindow* window, int entered)
{
    if (entered)
    {
        static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->cursorenter();
    }
    else
    {
        static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->cursorleave();
    }
}

void stringDropCallback(GLFWwindow* window, int count, const char** strings)
{
    std::vector<String> readStrings;
    for (int i = 0; i < count; ++i)
    {
        const void* s = static_cast<const void*>(strings[i]);
        readStrings.emplace_back(static_cast<const U8*>(s));
    }
    static_cast<priv::WindowCallbacks*>(glfwGetWindowUserPointer(window))->stringdrop(readStrings);
}

void registerGLFWCallbacks(GLFWwindow* wnd, void* userPointer)
{
    glfwSetWindowUserPointer(wnd, userPointer);
    // Let GLFW know about our callbacks (these will never change from GLFW's
    // point of view -- those we set here will be the bridge between GLFW and
    // our user's callbacks).
    glfwSetWindowCloseCallback(wnd, closeCallback);
    glfwSetWindowFocusCallback(wnd, focusCallback);
    glfwSetWindowIconifyCallback(wnd, minimiseCallback);
    glfwSetWindowMaximizeCallback(wnd, maximiseCallback);
    glfwSetWindowRefreshCallback(wnd, redrawCallback);
    glfwSetWindowPosCallback(wnd, positionCallback);
    glfwSetWindowSizeCallback(wnd, resizeCallback);
    glfwSetFramebufferSizeCallback(wnd, framebufferResizeCallback);
    glfwSetKeyCallback(wnd, keyCallback);
    glfwSetCharCallback(wnd, utfcharCallback);
    glfwSetMouseButtonCallback(wnd, mouseCallback);
    glfwSetScrollCallback(wnd, scrollCallback);
    glfwSetCursorPosCallback(wnd, cursorPositionCallback);
    glfwSetCursorEnterCallback(wnd, cursorEnterCallback);
    glfwSetDropCallback(wnd, stringDropCallback);
}

} // namespace priv

} // namespace window

} // namespace kraken
