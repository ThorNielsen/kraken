#include <kraken/window/input.hpp>
#include <kraken/window/glfw.hpp>

namespace kraken
{

namespace window
{

namespace priv
{

Key keyFromGLFW(int key)
{
    /// WARNING: This is initialised in initialiseGLFW() found in
    /// window/glfw.cpp, and thus, if either the below is changed to non-static
    /// variables, or this function is moved somewhere else, please update
    /// window/glfw.cpp to reflect this.
    static Key keyMappings[GLFW_KEY_LAST+1];
    static bool init = false;
    if (!init)
    {
        init = true;
        for (int i = 0; i <= GLFW_KEY_LAST; ++i)
        {
            keyMappings[i] = Key::Unknown;
        }
        keyMappings[GLFW_KEY_0] = Key::Key0;
        keyMappings[GLFW_KEY_1] = Key::Key1;
        keyMappings[GLFW_KEY_2] = Key::Key2;
        keyMappings[GLFW_KEY_3] = Key::Key3;
        keyMappings[GLFW_KEY_4] = Key::Key4;
        keyMappings[GLFW_KEY_5] = Key::Key5;
        keyMappings[GLFW_KEY_6] = Key::Key6;
        keyMappings[GLFW_KEY_7] = Key::Key7;
        keyMappings[GLFW_KEY_8] = Key::Key8;
        keyMappings[GLFW_KEY_9] = Key::Key9;

        keyMappings[GLFW_KEY_A] = Key::A;
        keyMappings[GLFW_KEY_B] = Key::B;
        keyMappings[GLFW_KEY_C] = Key::C;
        keyMappings[GLFW_KEY_D] = Key::D;
        keyMappings[GLFW_KEY_E] = Key::E;
        keyMappings[GLFW_KEY_F] = Key::F;
        keyMappings[GLFW_KEY_G] = Key::G;
        keyMappings[GLFW_KEY_H] = Key::H;
        keyMappings[GLFW_KEY_I] = Key::I;
        keyMappings[GLFW_KEY_J] = Key::J;
        keyMappings[GLFW_KEY_K] = Key::K;
        keyMappings[GLFW_KEY_L] = Key::L;
        keyMappings[GLFW_KEY_M] = Key::M;
        keyMappings[GLFW_KEY_N] = Key::N;
        keyMappings[GLFW_KEY_O] = Key::O;
        keyMappings[GLFW_KEY_P] = Key::P;
        keyMappings[GLFW_KEY_Q] = Key::Q;
        keyMappings[GLFW_KEY_R] = Key::R;
        keyMappings[GLFW_KEY_S] = Key::S;
        keyMappings[GLFW_KEY_T] = Key::T;
        keyMappings[GLFW_KEY_U] = Key::U;
        keyMappings[GLFW_KEY_V] = Key::V;
        keyMappings[GLFW_KEY_W] = Key::W;
        keyMappings[GLFW_KEY_X] = Key::X;
        keyMappings[GLFW_KEY_Y] = Key::Y;
        keyMappings[GLFW_KEY_Z] = Key::Z;

        keyMappings[GLFW_KEY_MINUS]      = Key::Hyphen;
        keyMappings[GLFW_KEY_COMMA]      = Key::Comma;
        keyMappings[GLFW_KEY_PERIOD]     = Key::Period;
        keyMappings[GLFW_KEY_APOSTROPHE] = Key::Apostrophe;

        keyMappings[GLFW_KEY_SPACE]     = Key::Space;
        keyMappings[GLFW_KEY_ENTER]     = Key::Enter;
        keyMappings[GLFW_KEY_BACKSPACE] = Key::Backspace;
        keyMappings[GLFW_KEY_INSERT]    = Key::Insert;
        keyMappings[GLFW_KEY_DELETE]    = Key::Delete;

        keyMappings[GLFW_KEY_NUM_LOCK]    = Key::NumLock;
        keyMappings[GLFW_KEY_CAPS_LOCK]   = Key::CapsLock;
        keyMappings[GLFW_KEY_SCROLL_LOCK] = Key::ScrollLock;

        keyMappings[GLFW_KEY_ESCAPE]       = Key::Escape;
        keyMappings[GLFW_KEY_PRINT_SCREEN] = Key::PrintScreen;
        keyMappings[GLFW_KEY_PAUSE]        = Key::Pause;
        keyMappings[GLFW_KEY_MENU]         = Key::Menu;

        keyMappings[GLFW_KEY_UP]        = Key::Up;
        keyMappings[GLFW_KEY_DOWN]      = Key::Down;
        keyMappings[GLFW_KEY_LEFT]      = Key::Left;
        keyMappings[GLFW_KEY_RIGHT]     = Key::Right;
        keyMappings[GLFW_KEY_HOME]      = Key::Home;
        keyMappings[GLFW_KEY_END]       = Key::End;
        keyMappings[GLFW_KEY_PAGE_UP]   = Key::PageUp;
        keyMappings[GLFW_KEY_PAGE_DOWN] = Key::PageDown;
        keyMappings[GLFW_KEY_TAB]       = Key::Tab;

        keyMappings[GLFW_KEY_LEFT_CONTROL]  = Key::LeftControl;
        keyMappings[GLFW_KEY_LEFT_ALT]      = Key::LeftAlt;
        keyMappings[GLFW_KEY_LEFT_SHIFT]    = Key::LeftShift;
        keyMappings[GLFW_KEY_LEFT_SUPER]    = Key::LeftSuper;
        keyMappings[GLFW_KEY_RIGHT_CONTROL] = Key::RightControl;
        keyMappings[GLFW_KEY_RIGHT_ALT]     = Key::RightAlt;
        keyMappings[GLFW_KEY_RIGHT_SHIFT]   = Key::RightShift;
        keyMappings[GLFW_KEY_RIGHT_SUPER]   = Key::RightSuper;

        keyMappings[GLFW_KEY_F1]  = Key::F1;
        keyMappings[GLFW_KEY_F2]  = Key::F2;
        keyMappings[GLFW_KEY_F3]  = Key::F3;
        keyMappings[GLFW_KEY_F4]  = Key::F4;
        keyMappings[GLFW_KEY_F5]  = Key::F5;
        keyMappings[GLFW_KEY_F6]  = Key::F6;
        keyMappings[GLFW_KEY_F7]  = Key::F7;
        keyMappings[GLFW_KEY_F8]  = Key::F8;
        keyMappings[GLFW_KEY_F9]  = Key::F9;
        keyMappings[GLFW_KEY_F10] = Key::F10;
        keyMappings[GLFW_KEY_F11] = Key::F11;
        keyMappings[GLFW_KEY_F12] = Key::F12;
        keyMappings[GLFW_KEY_F13] = Key::F13;
        keyMappings[GLFW_KEY_F14] = Key::F14;
        keyMappings[GLFW_KEY_F15] = Key::F15;
        keyMappings[GLFW_KEY_F16] = Key::F16;
        keyMappings[GLFW_KEY_F17] = Key::F17;
        keyMappings[GLFW_KEY_F18] = Key::F18;
        keyMappings[GLFW_KEY_F19] = Key::F19;
        keyMappings[GLFW_KEY_F20] = Key::F20;
        keyMappings[GLFW_KEY_F21] = Key::F21;
        keyMappings[GLFW_KEY_F22] = Key::F22;
        keyMappings[GLFW_KEY_F23] = Key::F23;
        keyMappings[GLFW_KEY_F24] = Key::F24;
        keyMappings[GLFW_KEY_F25] = Key::F25;

        keyMappings[GLFW_KEY_KP_0] = Key::Numpad0;
        keyMappings[GLFW_KEY_KP_1] = Key::Numpad1;
        keyMappings[GLFW_KEY_KP_2] = Key::Numpad2;
        keyMappings[GLFW_KEY_KP_3] = Key::Numpad3;
        keyMappings[GLFW_KEY_KP_4] = Key::Numpad4;
        keyMappings[GLFW_KEY_KP_5] = Key::Numpad5;
        keyMappings[GLFW_KEY_KP_6] = Key::Numpad6;
        keyMappings[GLFW_KEY_KP_7] = Key::Numpad7;
        keyMappings[GLFW_KEY_KP_8] = Key::Numpad8;
        keyMappings[GLFW_KEY_KP_9] = Key::Numpad9;

        keyMappings[GLFW_KEY_KP_ADD]      = Key::NumpadAdd;
        keyMappings[GLFW_KEY_KP_SUBTRACT] = Key::NumpadSubtract;
        keyMappings[GLFW_KEY_KP_MULTIPLY] = Key::NumpadMultiply;
        keyMappings[GLFW_KEY_KP_DIVIDE]   = Key::NumpadDivide;
        keyMappings[GLFW_KEY_KP_DECIMAL]  = Key::NumpadComma;
        keyMappings[GLFW_KEY_KP_ENTER]    = Key::NumpadEnter;
    }

    Key translated = Key::Unknown;
    if (0 <= key && key <= GLFW_KEY_LAST)
    {
        translated = keyMappings[key];
    }
    return translated;
}

int keyToGLFW(Key key)
{
    // Change to an array-like approach as above?
    switch (key)
    {
    case Key::Key0: return GLFW_KEY_0;
    case Key::Key1: return GLFW_KEY_1;
    case Key::Key2: return GLFW_KEY_2;
    case Key::Key3: return GLFW_KEY_3;
    case Key::Key4: return GLFW_KEY_4;
    case Key::Key5: return GLFW_KEY_5;
    case Key::Key6: return GLFW_KEY_6;
    case Key::Key7: return GLFW_KEY_7;
    case Key::Key8: return GLFW_KEY_8;
    case Key::Key9: return GLFW_KEY_9;

    case Key::A: return GLFW_KEY_A;
    case Key::B: return GLFW_KEY_B;
    case Key::C: return GLFW_KEY_C;
    case Key::D: return GLFW_KEY_D;
    case Key::E: return GLFW_KEY_E;
    case Key::F: return GLFW_KEY_F;
    case Key::G: return GLFW_KEY_G;
    case Key::H: return GLFW_KEY_H;
    case Key::I: return GLFW_KEY_I;
    case Key::J: return GLFW_KEY_J;
    case Key::K: return GLFW_KEY_K;
    case Key::L: return GLFW_KEY_L;
    case Key::M: return GLFW_KEY_M;
    case Key::N: return GLFW_KEY_N;
    case Key::O: return GLFW_KEY_O;
    case Key::P: return GLFW_KEY_P;
    case Key::Q: return GLFW_KEY_Q;
    case Key::R: return GLFW_KEY_R;
    case Key::S: return GLFW_KEY_S;
    case Key::T: return GLFW_KEY_T;
    case Key::U: return GLFW_KEY_U;
    case Key::V: return GLFW_KEY_V;
    case Key::W: return GLFW_KEY_W;
    case Key::X: return GLFW_KEY_X;
    case Key::Y: return GLFW_KEY_Y;
    case Key::Z: return GLFW_KEY_Z;

    case Key::Hyphen:     return GLFW_KEY_MINUS;
    case Key::Comma:      return GLFW_KEY_COMMA;
    case Key::Period:     return GLFW_KEY_PERIOD;
    case Key::Apostrophe: return GLFW_KEY_APOSTROPHE;

    case Key::Space:     return GLFW_KEY_SPACE;
    case Key::Enter:     return GLFW_KEY_ENTER;
    case Key::Backspace: return GLFW_KEY_BACKSPACE;
    case Key::Insert:    return GLFW_KEY_INSERT;
    case Key::Delete:    return GLFW_KEY_DELETE;

    case Key::NumLock:    return GLFW_KEY_NUM_LOCK;
    case Key::CapsLock:   return GLFW_KEY_CAPS_LOCK;
    case Key::ScrollLock: return GLFW_KEY_SCROLL_LOCK;

    case Key::Escape:      return GLFW_KEY_ESCAPE;
    case Key::PrintScreen: return GLFW_KEY_PRINT_SCREEN;
    case Key::Pause:       return GLFW_KEY_PAUSE;
    case Key::Menu:        return GLFW_KEY_MENU;

    case Key::Up:       return GLFW_KEY_UP;
    case Key::Down:     return GLFW_KEY_DOWN;
    case Key::Left:     return GLFW_KEY_LEFT;
    case Key::Right:    return GLFW_KEY_RIGHT;
    case Key::Home:     return GLFW_KEY_HOME;
    case Key::End:      return GLFW_KEY_END;
    case Key::PageUp:   return GLFW_KEY_PAGE_UP;
    case Key::PageDown: return GLFW_KEY_PAGE_DOWN;
    case Key::Tab:      return GLFW_KEY_TAB;

    case Key::LeftControl:  return GLFW_KEY_LEFT_CONTROL;
    case Key::LeftAlt:      return GLFW_KEY_LEFT_ALT;
    case Key::LeftShift:    return GLFW_KEY_LEFT_SHIFT;
    case Key::LeftSuper:    return GLFW_KEY_LEFT_SUPER;
    case Key::RightControl: return GLFW_KEY_RIGHT_CONTROL;
    case Key::RightAlt:     return GLFW_KEY_RIGHT_ALT;
    case Key::RightShift:   return GLFW_KEY_RIGHT_SHIFT;
    case Key::RightSuper:   return GLFW_KEY_RIGHT_SUPER;

    case Key::F1:  return GLFW_KEY_F1;
    case Key::F2:  return GLFW_KEY_F2;
    case Key::F3:  return GLFW_KEY_F3;
    case Key::F4:  return GLFW_KEY_F4;
    case Key::F5:  return GLFW_KEY_F5;
    case Key::F6:  return GLFW_KEY_F6;
    case Key::F7:  return GLFW_KEY_F7;
    case Key::F8:  return GLFW_KEY_F8;
    case Key::F9:  return GLFW_KEY_F9;
    case Key::F10: return GLFW_KEY_F10;
    case Key::F11: return GLFW_KEY_F11;
    case Key::F12: return GLFW_KEY_F12;
    case Key::F13: return GLFW_KEY_F13;
    case Key::F14: return GLFW_KEY_F14;
    case Key::F15: return GLFW_KEY_F15;
    case Key::F16: return GLFW_KEY_F16;
    case Key::F17: return GLFW_KEY_F17;
    case Key::F18: return GLFW_KEY_F18;
    case Key::F19: return GLFW_KEY_F19;
    case Key::F20: return GLFW_KEY_F20;
    case Key::F21: return GLFW_KEY_F21;
    case Key::F22: return GLFW_KEY_F22;
    case Key::F23: return GLFW_KEY_F23;
    case Key::F24: return GLFW_KEY_F24;
    case Key::F25: return GLFW_KEY_F25;

    case Key::Numpad0: return GLFW_KEY_KP_0;
    case Key::Numpad1: return GLFW_KEY_KP_1;
    case Key::Numpad2: return GLFW_KEY_KP_2;
    case Key::Numpad3: return GLFW_KEY_KP_3;
    case Key::Numpad4: return GLFW_KEY_KP_4;
    case Key::Numpad5: return GLFW_KEY_KP_5;
    case Key::Numpad6: return GLFW_KEY_KP_6;
    case Key::Numpad7: return GLFW_KEY_KP_7;
    case Key::Numpad8: return GLFW_KEY_KP_8;
    case Key::Numpad9: return GLFW_KEY_KP_9;

    case Key::NumpadAdd:      return GLFW_KEY_KP_ADD;
    case Key::NumpadSubtract: return GLFW_KEY_KP_SUBTRACT;
    case Key::NumpadMultiply: return GLFW_KEY_KP_MULTIPLY;
    case Key::NumpadDivide:   return GLFW_KEY_KP_DIVIDE;
    case Key::NumpadComma:    return GLFW_KEY_KP_DECIMAL;
    case Key::NumpadEnter:    return GLFW_KEY_KP_ENTER;

    case Key::Unknown: default:
        return GLFW_KEY_UNKNOWN;
    }
}

} // namespace priv

String getKeyName(Key k)
{
    priv::GLFWGuard guard;
    if (k == Key::Unknown) return "Unknown";
    return glfwGetKeyName(priv::keyToGLFW(k), 0);
}

String getKeyName(Scancode scancode)
{
    priv::GLFWGuard guard;
    return glfwGetKeyName(0, scancode);
}

Scancode getScancode(Key k)
{
    priv::GLFWGuard guard; // Technically this isn't necessary, as this function
                           // may be called from any thread. However, we still
                           // need to have GLFW initialised, and therefore we
                           // artificially restrict this function to the main
                           // thread.
    return glfwGetKeyScancode(priv::keyToGLFW(k));
}



} // namespace window

} // namespace kraken
