#include <kraken/window/window.hpp>
#include <kraken/window/glfw.hpp>

#include <stdexcept>

namespace kraken
{

namespace window
{

Cursor::CWrap::CWrap()
{
    priv::registerGLFWDependent();
    handle = nullptr;
}

Cursor::CWrap::~CWrap()
{
    if (handle)
    {
        glfwDestroyCursor(static_cast<GLFWcursor*>(handle));
        handle = nullptr;
    }
    priv::deregisterGLFWDependent();
}

Cursor::Cursor(CursorType ct)
    : m_cursor{nullptr}
{
    m_cursor = std::make_shared<CWrap>();
    auto shape = GLFW_ARROW_CURSOR;
    switch (ct)
    {
    case CursorType::Arrow:     shape = GLFW_ARROW_CURSOR;     break;
    case CursorType::IBeam:     shape = GLFW_IBEAM_CURSOR;     break;
    case CursorType::Crosshair: shape = GLFW_CROSSHAIR_CURSOR; break;
    case CursorType::Hand:      shape = GLFW_HAND_CURSOR;      break;
    case CursorType::HResize:   shape = GLFW_HRESIZE_CURSOR;   break;
    case CursorType::VResize:   shape = GLFW_VRESIZE_CURSOR;   break;
    }
    m_cursor->handle = glfwCreateStandardCursor(shape);
}

Cursor::Cursor(image::Image img, math::ivec2 clickAt)
    : m_cursor{nullptr}
{
    m_cursor = std::make_shared<CWrap>();
    if (img.colourType() != image::RGBA8)
    {
        img = std::move(img.convert(image::RGBA8));
    }
    GLFWimage gimg;
    gimg.width = img.width();
    gimg.height = img.height();
    gimg.pixels = img.data();
    m_cursor->handle = glfwCreateCursor(&gimg, clickAt.x, clickAt.y);
}

bool VulkanSurfaceCreationInfo::isPresentationSupported(VkInstance instance,
                                                        VkPhysicalDevice device,
                                                        U32 queueFamily)
{
    return glfwGetPhysicalDevicePresentationSupport(instance, device, queueFamily);
}

std::vector<std::string>
VulkanSurfaceCreationInfo::requiredInstanceExtensions()
{
    priv::GLFWGuard guard;
    U32 count;
    const char** extensionPtr = glfwGetRequiredInstanceExtensions(&count);
    std::vector<std::string> extensions(count);
    for (size_t i = 0; i < count; ++i)
    {
        extensions[i] = extensionPtr[i];
    }
    return extensions;
}

void VulkanSurfaceCreationInfo::beforeWindowOpen()
{
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
}

void* VulkanSurfaceCreationInfo::afterWindowOpen(void* wndHandle)
{
    auto* wnd = static_cast<GLFWwindow*>(wndHandle);
    m_resultOut = glfwCreateWindowSurface(m_instance, wnd,
                                          m_allocator, &m_surfaceOut);
    return nullptr;
}

Window::Window()
    : m_cursor{CursorType::Arrow}
{
    priv::registerGLFWDependent();
    m_width = 0;
    m_height = 0;
    m_windowHandle = nullptr;
    m_windowExtra = nullptr;
    m_callbacks = nullptr;
    m_open = false;
    m_cursorMode = CursorMode::Visible;
}

Window::Window(Window&& other)
    : m_cursor{CursorType::Arrow}
{
    priv::registerGLFWDependent();
    *this = std::move(other);
}

Window& Window::operator=(Window&& other)
{
    m_title = std::move(other.m_title);
    m_sci = std::move(other.m_sci);
    m_width = other.m_width;
    m_height = other.m_height;
    m_cursor = std::move(other.m_cursor);
    m_cursorMode = other.m_cursorMode;
    m_displayMode = other.m_displayMode;
    m_windowHandle = other.m_windowHandle;
    m_windowExtra = other.m_windowExtra;
    m_callbacks = other.m_callbacks;
    m_open = other.m_open;
    other.m_windowHandle = nullptr;
    other.m_windowExtra = nullptr;
    other.m_callbacks = nullptr;
    other.m_open = false;
    return *this;
}

Window::~Window()
{
    close();
    priv::deregisterGLFWDependent();
}

void Window::setTitle(String newTitle)
{
    assertOpen();
    m_title = newTitle;
    auto* window = static_cast<GLFWwindow*>(m_windowHandle);
    glfwSetWindowTitle(window, m_title.c_str());
}

bool Window::open(WindowCreationSettings ws,
                  std::unique_ptr<SurfaceCreationInfo> sci)
{
    if (isOpen())
    {
        throw std::runtime_error("Window already open.");
    }
    // Ensures in particular that setting hints will not be overwritten by
    // other threads.
    priv::assertOnMainThread();
    if (!sci)
    {
        throw std::runtime_error("SurfaceCreationInfo is nullptr.");
    }

    m_sci = std::move(sci);
    size_t errors = priv::g_glfwErrorCount;
    glfwWindowHint(GLFW_DECORATED, ws.decorations);
    glfwWindowHint(GLFW_FOCUSED, ws.focused);
    glfwWindowHint(GLFW_AUTO_ICONIFY, ws.autominimise);
    glfwWindowHint(GLFW_MAXIMIZED, ws.maximised);
    glfwWindowHint(GLFW_RESIZABLE, ws.resizable);
    glfwWindowHint(GLFW_FLOATING, ws.topmost);
    glfwWindowHint(GLFW_VISIBLE, ws.visible);
    glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, ws.transparent);
    glfwWindowHint(GLFW_CENTER_CURSOR, ws.centerMouseFullscreen);

    glfwWindowHint(GLFW_REFRESH_RATE, ws.refreshRate);
    if (priv::g_glfwErrorCount > errors) return false;

    m_sci->beforeWindowOpen();

    if (priv::g_glfwErrorCount > errors) return false;

    auto* fsMon = static_cast<GLFWmonitor*>(priv_getID(ws.fullscreenDisplay));
    if (ws.mode == DisplayMode::Windowed)
    {
        fsMon = nullptr;
    }
    if (ws.mode == DisplayMode::Fullscreen ||
        ws.mode == DisplayMode::BorderlessFullscreen)
    {
        if (!fsMon) fsMon = glfwGetPrimaryMonitor();
        if (ws.mode == DisplayMode::BorderlessFullscreen)
        {
            const auto* vidmode = glfwGetVideoMode(fsMon);
            glfwWindowHint(GLFW_RED_BITS, vidmode->redBits);
            glfwWindowHint(GLFW_GREEN_BITS, vidmode->greenBits);
            glfwWindowHint(GLFW_BLUE_BITS, vidmode->blueBits);
            glfwWindowHint(GLFW_REFRESH_RATE, vidmode->refreshRate);
        }
    }
    m_displayMode = ws.mode;

    m_title = ws.title;

    m_windowHandle = glfwCreateWindow(ws.width,
                                      ws.height,
                                      m_title.c_str(),
                                      fsMon,
                                      NULL);

    m_width = ws.width;
    m_height = ws.height;

    if (priv::g_glfwErrorCount > errors) return false;
    m_open = true;
    m_windowExtra = m_sci->afterWindowOpen(m_windowHandle);
    m_sci->onFramebufferResize(m_width, m_height);

    GLFWwindow* wnd = static_cast<GLFWwindow*>(m_windowHandle);
    glfwSetInputMode(wnd, GLFW_LOCK_KEY_MODS, GLFW_STICKY_KEYS);
    setCursor(m_cursor);

    m_callbacks = new priv::WindowCallbacks;

    priv::setEmptyCallbacks(wnd, m_callbacks);
    setResizeCallback([](U32,U32){});
    setFramebufferResizeCallback([](U32,U32){});

    priv::registerGLFWCallbacks(wnd, m_callbacks);

    return true;
}

bool Window::closeRequested() const
{
    assertOpen();
    auto* wnd = static_cast<GLFWwindow*>(m_windowHandle);
    return glfwWindowShouldClose(wnd);
}

void Window::setCloseFlag()
{
    auto* wnd = static_cast<GLFWwindow*>(m_windowHandle);
    glfwSetWindowShouldClose(wnd, true);
}

void Window::clearCloseFlag()
{
    auto* wnd = static_cast<GLFWwindow*>(m_windowHandle);
    glfwSetWindowShouldClose(wnd, false);
}

void Window::close() noexcept
{
    if (m_callbacks)
    {
        delete static_cast<priv::WindowCallbacks*>(m_callbacks);
        m_callbacks = nullptr;
    }
    if (isOpen())
    {
        m_sci->beforeWindowClose(m_windowHandle);
        glfwDestroyWindow(static_cast<GLFWwindow*>(m_windowHandle));
        m_sci->afterWindowClose();
        m_windowHandle = nullptr;
        m_open = false;
        m_cursorMode = CursorMode::Visible;
    }
}

void Window::pollEvents()
{
    priv::GLFWGuard guard;

    glfwPollEvents();
}

void Window::waitEvents(double timeout)
{
    priv::GLFWGuard guard;

    if (timeout) glfwWaitEventsTimeout(timeout);
    else glfwWaitEvents();
}

void Window::setCloseCallback(std::function<void()> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->close = callback;
}

void Window::setFocusCallback(std::function<void()> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->focus = callback;
}

void Window::setDefocusCallback(std::function<void()> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->defocus = callback;
}

void Window::setMinimiseCallback(std::function<void()> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->minimise = callback;
}

void Window::setMaximiseCallback(std::function<void()> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->maximise = callback;
}

void Window::setRestoreCallback(std::function<void(bool)> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->restore = callback;
}

void Window::setRedrawCallback(std::function<void()> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->redraw = callback;
}


void Window::setPositionCallback(std::function<void(S32, S32)> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->position = callback;
}

void Window::setResizeCallback(std::function<void(U32, U32)> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->resize =
        [this, callback](U32 w, U32 h)
        {
            this->m_width = w;
            this->m_height = h;
            callback(w, h);
        };
}

void Window::setFramebufferResizeCallback(std::function<void(U32, U32)> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->resize =
        [this, callback](U32 w, U32 h)
        {
            this->handleFramebufferResize(w, h);
            callback(w, h);
        };
}


void Window::setKeyPressCallback(std::function<void(Key, Scancode, KeyModifiers)> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->keypress = callback;
}

void Window::setKeyRepeatCallback(std::function<void(Key, Scancode, KeyModifiers)> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->keyrepeat = callback;
}

void Window::setKeyReleaseCallback(std::function<void(Key, Scancode, KeyModifiers)> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->keyrelease = callback;
}

void Window::setUTFCharCallback(std::function<void(U32)> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->utfchar = callback;
}

void Window::setMousePressCallback(std::function<void(Mouse, KeyModifiers)> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->mousepress = callback;
}

void Window::setMouseReleaseCallback(std::function<void(Mouse, KeyModifiers)> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->mouserelease = callback;
}

void Window::setScrollCallback(std::function<void(F64, F64)> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->scroll = callback;
}

void Window::setCursorPositionCallback(std::function<void(F64, F64)> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->cursorposition = callback;
}

void Window::setCursorEnterCallback(std::function<void()> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->cursorenter = callback;
}

void Window::setCursorLeaveCallback(std::function<void()> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->cursorleave = callback;
}

void Window::setStringDropCallback(std::function<void(std::vector<String>)> callback)
{
    auto* callbacks = static_cast<priv::WindowCallbacks*>(m_callbacks);
    callbacks->stringdrop = callback;
}

void Window::handleFramebufferResize(U32 w, U32 h)
{
    if (m_open)
    {
        m_sci->onFramebufferResize(w, h);
    }
}

bool Window::isPressed(Key k) const
{
    assertOpen();
    return glfwGetKey(static_cast<GLFWwindow*>(m_windowHandle),
                      priv::keyToGLFW(k));
}

bool Window::isPressed(Mouse m) const
{
    assertOpen();
    return glfwGetMouseButton(static_cast<GLFWwindow*>(m_windowHandle),
                              GLFW_MOUSE_BUTTON_1 + (int)m);
}

math::dvec2 Window::mousePos() const
{
    assertOpen();
    math::dvec2 pos;
    glfwGetCursorPos(static_cast<GLFWwindow*>(m_windowHandle),
                     &pos.x, &pos.y);
    return pos;
}

void Window::setMousePosition(math::dvec2 pos)
{
    assertOpen();
    glfwSetCursorPos(static_cast<GLFWwindow*>(m_windowHandle), pos.x, pos.y);
}

void Window::setCursorMode(CursorMode newMode)
{
    assertOpen();
    if (newMode == cursorMode()) return;
    auto* wnd = static_cast<GLFWwindow*>(m_windowHandle);
    bool rawSupported = glfwRawMouseMotionSupported();
    if (cursorMode() == CursorMode::LockedUnaccelerated
        && newMode != CursorMode::LockedUnaccelerated)
    {
        glfwSetInputMode(wnd, GLFW_RAW_MOUSE_MOTION, GLFW_FALSE);
    }

    m_cursorMode = newMode;

    if (newMode == CursorMode::LockedUnaccelerated)
    {
        glfwSetInputMode(wnd, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        if (rawSupported)
        {
            glfwSetInputMode(wnd, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
            m_cursorMode = CursorMode::LockedUnaccelerated;
        }
        else
        {
            m_cursorMode = CursorMode::Locked;
        }
    }
    else if (newMode == CursorMode::Locked)
    {
        glfwSetInputMode(wnd, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }
    else if (newMode == CursorMode::Hidden)
    {
        glfwSetInputMode(wnd, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    }
    else if (newMode == CursorMode::Visible)
    {
        glfwSetInputMode(wnd, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
}

void Window::setCursor(Cursor c)
{
    m_cursor = c;
    if (m_open)
    {
        glfwSetCursor(static_cast<GLFWwindow*>(m_windowHandle),
                      static_cast<GLFWcursor*>(m_cursor.m_cursor->handle));
    }
}

void Window::setIcon(const image::Image* iconsBegin,
                     const image::Image* iconsEnd)
{
    assertOpen();
    auto count = (size_t)std::distance(iconsBegin, iconsEnd);
    std::vector<image::Image> converted;
    std::vector<GLFWimage> imgs(count);
    for (size_t i = 0; i < count; ++i)
    {
        const auto& currImg = iconsBegin[i];
        // const_cast is OK to use, since glfwSetWindowIcon takes a
        // const GLFWimage*, and thus, even though we need a non-const pointer,
        // this does, in fact, not throw away constness (effectively).
        image::Image* readFrom = const_cast<image::Image*>(&currImg);
        if (currImg.colourType() != image::RGBA8)
        {
            converted.push_back(currImg.convert(image::RGBA8));
            readFrom = &converted.back();
        }
        imgs[i].width = readFrom->width();
        imgs[i].height = readFrom->height();
        imgs[i].pixels = readFrom->data();
    }
    glfwSetWindowIcon(static_cast<GLFWwindow*>(m_windowHandle),
                      count, imgs.data());
}

void Window::setIcon(const image::Image& img)
{
    assertOpen();
    const image::Image* end = &img;
    ++end;
    setIcon(&img, end);
}

void Window::hide()
{
    assertOpen();
    glfwHideWindow(static_cast<GLFWwindow*>(m_windowHandle));
}

void Window::unhide()
{
    assertOpen();
    glfwShowWindow(static_cast<GLFWwindow*>(m_windowHandle));
}

bool Window::isHidden() const
{
    assertOpen();
    return !glfwGetWindowAttrib(static_cast<GLFWwindow*>(m_windowHandle),
                                GLFW_VISIBLE);
}

void Window::forceFocus()
{
    assertOpen();
    glfwFocusWindow(static_cast<GLFWwindow*>(m_windowHandle));
}

void Window::requestFocus()
{
    assertOpen();
    glfwRequestWindowAttention(static_cast<GLFWwindow*>(m_windowHandle));
}

void Window::setSizeLimits(math::uvec2 minSize, math::uvec2 maxSize)
{
    assertOpen();
    if (minSize.x > maxSize.x) maxSize.x = minSize.x;
    if (minSize.y > maxSize.y) maxSize.y = minSize.y;
    int minX = minSize.x ? minSize.x : GLFW_DONT_CARE;
    int minY = minSize.y ? minSize.y : GLFW_DONT_CARE;
    int maxX = maxSize.x ? maxSize.x : GLFW_DONT_CARE;
    int maxY = maxSize.y ? maxSize.y : GLFW_DONT_CARE;
    glfwSetWindowSizeLimits(static_cast<GLFWwindow*>(m_windowHandle),
                            minX, minY, maxX, maxY);
}

void Window::setSize(math::uvec2 newSize)
{
    assertOpen();
    if (newSize != size() && m_displayMode == DisplayMode::BorderlessFullscreen)
    {
        m_displayMode = DisplayMode::Fullscreen;
    }
    glfwSetWindowSize(static_cast<GLFWwindow*>(m_windowHandle),
                      newSize.x, newSize.y);
}

math::uvec2 Window::framebufferSize() const
{
    assertOpen();
    int x, y;
    glfwGetFramebufferSize(static_cast<GLFWwindow*>(m_windowHandle), &x, &y);
    return {(U32)x, (U32)y};
}

void Window::setAspectRatio(math::uvec2 xy)
{
    assertOpen();
    int x = xy.x, y = xy.y;
    if (!x || !y) x = y = GLFW_DONT_CARE;
    glfwSetWindowAspectRatio(static_cast<GLFWwindow*>(m_windowHandle), x, y);
}

void Window::assertOpen(const char* msg) const
{
    if (!m_open)
    {
        if (msg)
        {
            throw std::runtime_error(msg);
        }
        else
        {
            throw std::runtime_error("Assertion 'Window open' failed.");
        }
    }
}

void Window::setPosition(math::ivec2 pos)
{
    assertOpen();
    if (currentDisplayMode() != DisplayMode::Windowed) return;
    glfwSetWindowPos(static_cast<GLFWwindow*>(m_windowHandle), pos.x, pos.y);
}

math::ivec2 Window::position() const
{
    assertOpen();
    math::ivec2 pos;
    glfwGetWindowPos(static_cast<GLFWwindow*>(m_windowHandle), &pos.x, &pos.y);
    return pos;
}

DisplayID Window::fullscreenDisplay() const
{
    DisplayID did;
    if (m_displayMode == DisplayMode::Windowed) return did;
    priv_setID(did,
               glfwGetWindowMonitor(static_cast<GLFWwindow*>(m_windowHandle)));
    return did;
}

void Window::setDisplayMode(DisplayMode newMode, math::ivec2 newPos,
                            math::uvec2 newSize, F32 refreshRate,
                            DisplayID display)
{
    assertOpen();
    if (newMode == currentDisplayMode()) return;
    auto* wnd = static_cast<GLFWwindow*>(m_windowHandle);
    auto* mon = static_cast<GLFWmonitor*>(priv_getID(display));
    if (newMode == DisplayMode::Windowed)
    {
        glfwSetWindowMonitor(wnd, nullptr, newPos.x, newPos.y,
                             newSize.x, newSize.y, GLFW_DONT_CARE);
    }
    else if (newMode == DisplayMode::Fullscreen)
    {
        glfwSetWindowMonitor(wnd, mon, 0, 0, newSize.x, newSize.y,
                             refreshRate > 0.f ? refreshRate : GLFW_DONT_CARE);
    }
    else if (newMode == DisplayMode::BorderlessFullscreen)
    {
        const auto* mode = glfwGetVideoMode(mon);
        glfwSetWindowMonitor(wnd, mon, 0, 0,
                             mode->width, mode->height, mode->refreshRate);
    }
    m_displayMode = newMode;
}


void Window::setDecorated(bool enable)
{
    assertOpen();
    glfwSetWindowAttrib(static_cast<GLFWwindow*>(m_windowHandle),
                        GLFW_DECORATED, enable);
}

void Window::setAutominimise(bool enable)
{
    assertOpen();
    glfwSetWindowAttrib(static_cast<GLFWwindow*>(m_windowHandle),
                        GLFW_AUTO_ICONIFY, enable);
}

void Window::setTopmost(bool enable)
{
    assertOpen();
    glfwSetWindowAttrib(static_cast<GLFWwindow*>(m_windowHandle),
                        GLFW_FLOATING, enable);
}

void Window::setResizable(bool enable)
{
    assertOpen();
    glfwSetWindowAttrib(static_cast<GLFWwindow*>(m_windowHandle),
                        GLFW_RESIZABLE, enable);
}


bool Window::decorated() const
{
    assertOpen();
    return glfwGetWindowAttrib(static_cast<GLFWwindow*>(m_windowHandle),
                               GLFW_DECORATED);
}

bool Window::autominimise() const
{
    assertOpen();
    return glfwGetWindowAttrib(static_cast<GLFWwindow*>(m_windowHandle),
                               GLFW_AUTO_ICONIFY);
}

bool Window::topmost() const
{
    assertOpen();
    return glfwGetWindowAttrib(static_cast<GLFWwindow*>(m_windowHandle),
                               GLFW_FLOATING);
}

bool Window::resizable() const
{
    assertOpen();
    return glfwGetWindowAttrib(static_cast<GLFWwindow*>(m_windowHandle),
                               GLFW_RESIZABLE);
}



} // namespace window

} // namespace kraken
