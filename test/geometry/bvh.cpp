#include <cstddef>
#include <kraken/geometry/algorithm.hpp>
#include <kraken/geometry/bvh.hpp>
#include <kraken/geometry/bvhconstruction.hpp>
#include <kraken/geometry/distance.hpp>
#include <kraken/geometry/intersection.hpp>
#include <kraken/geometry/primitives.hpp>

#include "common.hpp"
#include "kraken/geometry/boundingvolumes.hpp"
#include "kraken/globaltypes.hpp"
#include "kraken/math/constants.hpp"

#include <random>
#include <set>
#include <utility>

template <typename Prec>
int testBVHTriangleSetDistance(size_t tri1Count, size_t tri2Count,
                               size_t seed = 0xfeeb)
{
    std::mt19937 gen(seed);
    std::uniform_real_distribution<Prec> dist(Prec(-10), Prec(10));

    auto tri1 = generateRandomTriangles(gen, dist, tri1Count, Prec(1)/Prec(8));
    auto tri2 = generateRandomTriangles(gen, dist, tri2Count, Prec(1)/Prec(8));

    // The points above have each coordinate within [-11.25, 11.25]. This means
    // that offsetting them by 22.5 or more in one coordinate ensures that none
    // of the triangles intersect.
    kg::Vector3<Prec> offset{23, 2, 7};

    for (auto& [a, b, c] : tri2)
    {
        a += offset;
        b += offset;
        c += offset;
    }

    auto bvh1 = kg::createTriangleBVH<kg::Aabb<Prec>>(tri1.begin(), tri1.end());
    auto bvh2 = kg::createTriangleBVH<kg::Aabb<Prec>>(tri2.begin(), tri2.end());

    auto [bvhClosest1, bvhClosest2] = kg::closestPoints(bvh1, bvh2);

    auto bvhSqd = squaredLength(bvhClosest2-bvhClosest1);

    auto closestDist = km::Constant<Prec>::maximum;
    for (auto t1 : tri1)
    {
        for (auto t2 : tri2)
        {
            auto [pt1, pt2] = closestPoints(t1, t2);
            closestDist = std::min(closestDist, squaredLength(pt2-pt1));
        }
    }

    return closestDist != bvhSqd;
}

template <typename Prec>
int testBVHClosestPoint(size_t triCount,
                        size_t pointCount,
                        size_t seed = 0xbeef) // Seedy non-vegan.
{
    int err = 0;

    std::mt19937 gen(seed);
    std::uniform_real_distribution<Prec> dist(Prec(-10), Prec(10));
    // The smaller triangles are, the much less likely we are to get a
    // degenerate case where all triangles overlap each other and have roughly
    // identical bounding boxes.
    // For 'real' data, we would actually have an even lower ratio; probably
    // closer to 1/100 or something.
    auto triangles = generateRandomTriangles(gen,
                                             dist,
                                             triCount,
                                             Prec(1)/Prec(13));

    auto aabbBVH = kg::createTriangleBVH<kg::Aabb<Prec>>(triangles.begin(), triangles.end());
    auto sphereBVH = kg::createTriangleBVH<kg::Sphere<Prec>>(triangles.begin(), triangles.end());

    for (size_t i = 0; i < pointCount; ++i)
    {
        auto point = generateRandomVec3(gen, dist);
        auto closestNaive = closestPoint(triangles.begin(),
                                         triangles.end(),
                                         point).second;
        auto closestAABBBVH = kg::closestPoint(point, aabbBVH);
        auto closestSphereBVH = kg::closestPoint(point, sphereBVH);

        auto naiveSqd = kg::squaredDistance(point, closestNaive);
        auto aabbSqd = kg::squaredDistance(point, closestAABBBVH);
        auto sphereSqd = kg::squaredDistance(point, closestSphereBVH);

        err += naiveSqd != aabbSqd;
        err += naiveSqd != sphereSqd;
    }

    return err > 0;
}

template <typename Prec>
int testBVHOverlaps(size_t tri1Count, size_t tri2Count,
                    size_t seed = 0xfeeb)
{
    std::mt19937 gen(seed);
    std::uniform_real_distribution<Prec> dist(Prec(-10), Prec(10));

    auto tri1 = generateRandomTriangles(gen, dist, tri1Count, Prec(1)/Prec(8));
    auto tri2 = generateRandomTriangles(gen, dist, tri2Count, Prec(1)/Prec(8));

    auto bvh1 = kg::createIdentifiableTriangleBVH<kg::Aabb<Prec>, U32>
                                                 (tri1.begin(), tri1.end());

    auto bvh2 = kg::createIdentifiableTriangleBVH<kg::Aabb<Prec>, U32>
                                                 (tri2.begin(), tri2.end());


    std::set<std::pair<size_t, size_t>> overlaps;
    kg::processOverlaps(bvh1, bvh2,
    [&overlaps]
    (const std::pair<kg::Triangle3d<Prec>, U32>& t1,
     const std::pair<kg::Triangle3d<Prec>, U32>& t2)
    {
        if (kg::convexIntersects(reinterpret_cast<const kg::Vector3<Prec>*>(&t1.first),
                                 3,
                                 reinterpret_cast<const kg::Vector3<Prec>*>(&t2.first),
                                 3))
        {
            overlaps.insert({t1.second, t2.second});
        }
    });

    for (size_t i = 0; i < tri1.size(); ++i)
    {
        auto t1 = tri1[i];
        for (size_t j = 0; j < tri2.size(); ++j)
        {
            auto t2 = tri2[j];
            bool hasOverlap = kg::convexIntersects(reinterpret_cast<const kg::Vector3<Prec>*>(&t1),
                                                   3,
                                                   reinterpret_cast<const kg::Vector3<Prec>*>(&t2),
                                                   3);
            bool bvhOverlap = overlaps.find({i, j}) != overlaps.end();

            if (hasOverlap != bvhOverlap)
            {
                return bvhOverlap ? 2 : 1;
            }
        }
    }
    return 0;
}

int main()
{
    int err = 0;

    err += testBVHOverlaps<double>(500, 500, 0xe);
    err += testBVHOverlaps<double>(500, 500, 0xbeef);
    err += testBVHOverlaps<double>(500, 500, 0xaffecafe);

    err += testBVHClosestPoint<double>(5000, 1000);
    err += testBVHClosestPoint<float>(5000, 1000);

    err += testBVHTriangleSetDistance<double>(250, 250, 0xe);
    err += testBVHTriangleSetDistance<double>(250, 250, 0xbeef);
    err += testBVHTriangleSetDistance<double>(314, 159, 0xaffecafe);

    return err;
}
