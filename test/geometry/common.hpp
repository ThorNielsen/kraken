#ifndef KRAKEN_TEST_GEOMETRY_COMMON_HPP_INCLUDED
#define KRAKEN_TEST_GEOMETRY_COMMON_HPP_INCLUDED

#include <kraken/geometry/bvh.hpp>
#include <kraken/geometry/primitives.hpp>
#include <kraken/utility/template_support.hpp>
#include <kraken/globaltypes.hpp>

#include <filesystem>
#include <random>
#include <vector>

namespace km = kraken::math;
namespace kg = kraken::geometry;
namespace fs = std::filesystem;

void setWorkdirToTestDataFolder()
{
    auto currPath = fs::current_path();
    size_t guard = 32;
    while (guard --> 0)
    {
        auto testPath = currPath / "test/data";
        auto status = fs::status(testPath);
        if (fs::is_directory(status))
        {
            fs::current_path(fs::canonical(testPath));
            return;
        }
        else if (fs::is_symlink(status))
        {
            auto ss = fs::symlink_status(testPath);
            if (fs::is_directory(ss))
            {
                fs::current_path(fs::canonical(testPath));
                return;
            }
        }
        else
        {
            currPath = fs::canonical(currPath / "..");
        }
    }
    throw std::runtime_error("Failed to find data directory.");
}

template <typename Prec, typename It1, typename It2>
bool vec_releq(It1 begin1, It1 end1, It2 begin2, It2 end2,
               Prec epsilon = km::Constant<Prec>::epsilon*50)
{
    auto sqEps = epsilon * epsilon;
    while (begin1 != end1 && begin2 != end2)
    {
        if (squaredLength(*begin1++-*begin2++) >= sqEps) return false;
    }
    return begin1 == end1 && begin2 == end2;
}

template <typename Generator, typename Distribution>
auto generateRandomVec3(Generator& generator, Distribution& dist)
-> kg::Vector3<decltype(dist(generator))>
{
    kg::Vector3<decltype(dist(generator))> v;
    v[0] = dist(generator);
    v[1] = dist(generator);
    v[2] = dist(generator);
    return v;
}

template <typename Generator, typename Distribution>
auto generateRandomVec2(Generator& generator, Distribution& dist)
-> kg::Vector2<decltype(dist(generator))>
{
    kg::Vector2<decltype(dist(generator))> v;
    v[0] = dist(generator);
    v[1] = dist(generator);
    return v;
}

template <typename Generator, typename Distribution>
auto generateRandomTriangle(Generator& generator, Distribution& dist,
                            decltype(dist(generator)) vertexFactor = 1)
-> kg::Triangle3d<decltype(dist(generator))>
{
    auto centre = generateRandomVec3(generator, dist);
    kg::Triangle3d<decltype(dist(generator))> triangle;
    triangle.a = centre + vertexFactor * generateRandomVec3(generator, dist);
    triangle.b = centre + vertexFactor * generateRandomVec3(generator, dist);
    triangle.c = centre + vertexFactor * generateRandomVec3(generator, dist);
    return triangle;
}

template <typename Generator, typename Distribution>
auto generateRandomTriangles(Generator& generator,
                             Distribution& dist,
                             size_t count,
                             decltype(dist(generator)) vertexFactor = 1)
-> std::vector<kg::Triangle3d<decltype(dist(generator))>>
{
    std::vector<kg::Triangle3d<decltype(dist(generator))>> triangles;
    triangles.reserve(count);
    while (count --> 0)
    {
        triangles.push_back(generateRandomTriangle(generator,
                                                   dist,
                                                   vertexFactor));
    }
    return triangles;
}

// Convert vector of flat coordinates into vector of Vector2<Prec>.
template <typename Prec>
std::vector<kg::Vector2<Prec>> buildPolygon2d(std::vector<Prec> flat)
{
    if (flat.size() & 1)
    {
        throw std::logic_error("Bad flat vector length.");
    }
    std::vector<kg::Vector2<Prec>> merged;
    for (size_t i = 0; i < (flat.size()>>1); ++i)
    {
        merged.push_back({flat[2*i], flat[2*i+1]});
    }
    return merged;
}

// Convert vector of flat coordinates into vector of Vector3<Prec>.
template <typename Prec>
std::vector<kg::Vector3<Prec>> buildPolygon3d(std::vector<Prec> flat)
{
    if (flat.size() % 3)
    {
        throw std::logic_error("Bad flat vector length.");
    }
    std::vector<kg::Vector3<Prec>> merged;
    for (size_t i = 0; i < flat.size()/3; ++i)
    {
        merged.push_back({flat[3*i], flat[3*i+1], flat[3*i+2]});
    }
    return merged;
}

template <typename Vector, typename Prec>
bool cyclicReleq(std::vector<Vector> a,
                 const std::vector<Vector>& b,
                 Prec epsilon)
{
    if (a.size() != b.size()) return false;
    for (size_t i = 0; i < a.size(); ++i)
    {
        if (vec_releq(a.begin(), a.end(), b.begin(), b.end(), epsilon))
        {
            return true;
        }
        // The following would suffice in C++20-code. But we are not ready to
        // upgrade just yet...
        // std::shift_right(a.begin(), a.end(), 1);
        // So the following is used instead to perform the shift:
        auto tmp = a.back();
        a.pop_back();
        a.insert(a.begin(), tmp);
    }
    return a.empty();
}

#endif // KRAKEN_TEST_GEOMETRY_COMMON_HPP_INCLUDED
