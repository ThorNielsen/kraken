#include <cstddef>
#include <kraken/geometry/convexboolean.hpp>

#include "common.hpp"
#include "kraken/geometry/primitives.hpp"
#include "kraken/math/constants.hpp"

#include <iostream>
#include <limits>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

template <typename Prec>
std::string format(std::vector<kg::Vector2<Prec>> vertices)
{
    std::stringstream ss;
    for (size_t i = 0; i < vertices.size(); ++i)
    {
        if (i) ss << ' ';
        ss << vertices[i];
    }
    return ss.str();
}

template <typename Prec>
int testPolygonBoolean(std::vector<kg::Vector2<Prec>> poly1,
                       std::vector<kg::Vector2<Prec>> poly2,
                       std::vector<kg::Vector2<Prec>> reference,
                       bool intersection,
                       Prec epsilon = 50*km::Constant<Prec>::epsilon)
{
    for (size_t k = 0; k < poly1.size(); ++k)
    {
        for (size_t j = 0; j < poly2.size(); ++j)
        {
            decltype(reference) result;
            kg::performBooleanOperation(poly1.data(),
                                        poly1.size(),
                                        poly2.data(),
                                        poly2.size(),
                                        intersection,
                                        std::back_inserter(result),
                                        ~(size_t)0,
                                        nullptr,
                                        epsilon);

            if (result.size() != reference.size())
            {
                std::cerr << (intersection ? "Intersection" : "Union")
                          << " size mismatch in iteration "
                          << k << ", " << j
                          << " (got " << result.size()
                          << ", expected " << reference.size() << ")\n";
                std::cerr << "Polygons:\n"
                          << "Poly1: " << format(poly1) << "\n"
                          << "Poly2: " << format(poly2) << "\n"
                          << "Result: " << format(result) << "\n"
                          << "Expected: " << format(reference) << "\n";
                return 1;
            }
            if (!cyclicReleq(result, reference, epsilon))
            {
                std::cerr << (intersection ? "Intersection" : "Union")
                          << " sizes matched, but positions didn't in "
                          << "iteration " << k << ", " << j << ".\n";
                std::cerr << "Polygons:\n"
                          << "Poly1: " << format(poly1) << "\n"
                          << "Poly2: " << format(poly2) << "\n"
                          << "Result: " << format(result) << "\n"
                          << "Expected: " << format(reference) << "\n";
                return 1;
            }

            auto tmp = poly2.back();
            poly2.pop_back();
            poly2.insert(poly2.begin(), tmp);
        }
        auto tmp = poly1.back();
        poly1.pop_back();
        poly1.insert(poly1.begin(), tmp);
    }

    return 0;
}

template <typename Prec>
int testPolygonBoolean(std::vector<Prec> poly1Flat,
                       std::vector<Prec> poly2Flat,
                       std::string referenceDesc,
                       bool intersection,
                       Prec epsilon = 50*km::Constant<Prec>::epsilon)
{
    auto poly1 = buildPolygon2d(poly1Flat);
    auto poly2 = buildPolygon2d(poly2Flat);

    decltype(poly1) reference;
    if (!referenceDesc.empty())
    {
        std::stringstream reader(referenceDesc);
        while (reader.good())
        {
            std::string cmd;
            reader >> cmd;
            if (cmd == "c") // Coords
            {
                double x, y;
                reader >> x >> y;
                reference.push_back({x, y});
            }
            else if (cmd == "v" || cmd == "w") // Vertex / "Wertex"
            {
                size_t index = ~(size_t)0;
                reader >> index;
                const auto& src = cmd == "v" ? poly1 : poly2;
                if (src.size() <= index)
                {
                    throw std::logic_error("Bad index!");
                }
                reference.emplace_back(src[index]);
            }
            else if (cmd == "i") // Intersection [i] [j]; of line seg [i, i+1]
            {                    // and [j, j+1], appropriately modded by sizes.
                size_t i, j;
                reader >> i >> j;
                if (i >= poly1.size() || j >= poly2.size())
                {
                    throw std::logic_error("Bad index for intersection.");
                }
                auto ip = (i+1)%poly1.size();
                auto jp = (j+1)%poly1.size();
                auto l1 = kg::Line2d<Prec>{poly1[i],
                                           poly1[ip]-poly1[i]};
                auto l2 = kg::Line2d<Prec>{poly2[j],
                                           poly2[jp]-poly2[j]};
                auto [par1, par2] = kg::intersect(l1, l2, epsilon);
                if (par1 >= std::numeric_limits<Prec>::max())
                {
                    throw std::logic_error("Bad intersection.");
                }
                reference.emplace_back(l1.origin+par1*l1.direction);
            }
            else
            {
                throw std::logic_error("Bad command.");
            }
        }
    }
    return testPolygonBoolean(poly1, poly2, reference, intersection, epsilon);
}

template <typename Prec>
int testPolygonBooleans(std::vector<Prec> poly1Flat,
                        std::vector<Prec> poly2Flat,
                        std::string intersectionDesc,
                        std::string unionDesc,
                        Prec epsilon = 50*km::Constant<Prec>::epsilon)
{
    return testPolygonBoolean(poly1Flat, poly2Flat, intersectionDesc, true, epsilon)
         + testPolygonBoolean(poly1Flat, poly2Flat, unionDesc, false, epsilon);
}

int main()
{
    int err = 0;

    std::vector<double> firstPoly{3, 1, -1, 1, -1, -3};

    // Test triangle against triangle in all non-edge/vertex cases.
    err += testPolygonBooleans(firstPoly,
                               {.9, .7, -.7, -.8, 1.5, -1.5},
                               "w 0 w 1 i 2 1 i 2 2",
                               "v 0 v 1 v 2 i 2 1 w 2 i 2 2");

    err += testPolygonBooleans(firstPoly,
                               {.7, .4, -.4, -.6, .2, -1.7},
                               "w 0 w 1 w 2",
                               "v 0 v 1 v 2");

    err += testPolygonBoolean(firstPoly,
                              {5.7, 5.4, 5.-.4, 5.-.6, 5.2, 5.-1.7},
                              "",
                              true);

    err += testPolygonBooleans(firstPoly,
                               {1.1, 1.1, -1.1, -1.1, .2, -1.7},
                               "i 0 0 i 1 0 i 1 1 w 2 i 0 2",
                               "v 0 i 0 2 w 0 i 0 0 v 1 i 1 0 w 1 i 1 1 v 2");

    err += testPolygonBooleans(firstPoly,
                               {2., 2., -2., -2., 2., -2.},
                               "i 0 0 i 1 0 i 1 1 i 2 1 i 2 2 i 0 2",
                               "w 0 i 0 0 v 1 i 1 0 w 1 i 1 1 v 2 i 2 1 w 2 i 2 2 v 0 i 0 2");

    err += testPolygonBooleans(firstPoly,
                               {2., 0., 0., 1., -1., -2.},
                               "w 0 w 1 w 2",
                               "v 0 v 1 v 2");

    err += testPolygonBooleans(firstPoly,
                               {0., 1., 0., -2., 2., 0.},
                               "w 0 w 1 w 2",
                               "v 0 v 1 v 2");

    err += testPolygonBooleans(firstPoly,
                               {0., 1.1, 0., -2., 2., 0.},
                               "i 0 2 i 0 0 w 1 w 2",
                               "v 0 i 0 2 w 0 i 0 0 v 1 v 2");

    err += testPolygonBooleans(firstPoly,
                               {2.7, 1.2, 2.9, 0.6, 3.4, 1.1},
                               "i 0 0 i 2 0 v 0",
                               "w 0 i 0 0 v 1 v 2 i 2 0 w 1 w 2");

    err += testPolygonBooleans(firstPoly,
                               {1.5, 1.4, 2., -.9, 2.5, -.5},
                               "i 0 0 i 2 0 i 2 2 i 0 2",
                               "v 0 i 0 2 w 0 i 0 0 v 1 v 2 i 2 0 w 1 w 2 i 2 2");

    // Much more complicated polygons.
    err += testPolygonBooleans<double>({-.7, .8, -1.6, -.8, -.5, -1.8, 1.2, -1.4, 1.3, .5},
                                       {1.5, -.4, .5, 1.1, -1.3, .2, -1.1, -1.6, .4, -1.8},
                                       "i 0 1 i 0 2 i 1 2 i 1 3 i 2 3 i 2 4 i 3 4 i 3 0 i 4 0 i 4 1",
                                       "i 0 1 w 2 i 0 2 v 1 i 1 2 w 3 i 1 3 v 2 i 2 3 w 4 i 2 4 v 3 i 3 4 w 0 i 3 0 v 4 i 4 0 w 1 i 4 1 v 0");

    std::cout << "Errors: " << err << "\n";

    return err;
}
