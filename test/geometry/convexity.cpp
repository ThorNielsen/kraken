#include <kraken/geometry/convexity.hpp>

#include <random>
#include <vector>
#include <unordered_set>

namespace km = kraken::math;
namespace kg = kraken::geometry;

bool isMeshClosed(const kg::Mesh& mesh)
{
    std::unordered_set<kg::EdgeIndex> seenEdges;
    for (auto face = mesh.fbegin(); face != mesh.fend(); ++face)
    {
        auto edgeBeginIdx = face->outer;
        size_t loopGuard = mesh.edges() + 1;
        auto currEdgeIndex = edgeBeginIdx;
        do
        {
            if (seenEdges.find(currEdgeIndex) != seenEdges.end())
            {
                // Duplicate edges!
                return false;
            }
            seenEdges.insert(currEdgeIndex);
            if (!mesh.isValidEdge(currEdgeIndex)) return false;
            if (!mesh.isValidEdge(mesh.edge(currEdgeIndex).twin)) return false;
            currEdgeIndex = mesh.edgeAt(currEdgeIndex).next;
        } while (currEdgeIndex != edgeBeginIdx && --loopGuard);
    }
    if (seenEdges.size() != mesh.edges()) return false;

    for (auto vertex = mesh.vbegin(); vertex != mesh.vend(); ++vertex)
    {
        if (!mesh.isValidEdge(vertex->edge)) return false;
    }

    return true;
}

bool isConvex(const kg::Mesh& mesh, float maxAberration = 1e-5)
{
    if (!isMeshClosed(mesh)) return false;
    for (auto face = mesh.fbegin(); face != mesh.fend(); ++face)
    {
        for (auto vertex = mesh.vbegin(); vertex != mesh.vend(); ++vertex)
        {
            if (kg::signedDistance(*face, vertex->pos) >= maxAberration)
            {
                return false;
            }
        }
    }
    return true;
}

void appendIcosahedralVertices(std::vector<km::vec3>& verts, float radius)
{
    constexpr float phi = 0.5f + std::sqrt(1.25f);
    for (size_t i = 0; i < 4; ++i)
    {
        float pmone = i&1 ? 1. : -1.;
        float pmphi = i&2 ? phi : -phi;
        verts.push_back(km::normalise(km::vec3{0, pmone, pmphi})*radius);
        verts.push_back(km::normalise(km::vec3{pmone, pmphi, 0})*radius);
        verts.push_back(km::normalise(km::vec3{pmphi, 0, pmone})*radius);
    }
}

int testIcosahedron(const std::vector<km::vec3>& vertices)
{
    int err = 0;

    auto hull = kg::convexHull(vertices.data(), vertices.size());

    err += !isMeshClosed(hull);
    err += !isConvex(hull);

    err += !(hull.vertices() == 12);
    err += !(hull.faces() == 20);
    err += !(hull.edges() == 2*30);

    std::vector<km::vec3> testDirections =
    {
        {1.f, 0.f, 0.f}, {0.f, 1.f, 0.f}, {0.f, 0.f, 1.f},
        {-1.f, 0.f, 0.f}, {0.f, -1.f, 0.f}, {0.f, 0.f, -1.f},
        {1.f, 3.f, 6.f}, {-1.f, -2.f, -1.f}, {-1.f, -3.f, 2.f},
        {0.1f, 0.1f, -0.1f}, {0.1f, 0.2f, 0.1f}, {.7f, .2f, 0.f},
    };

    for (auto dir : testDirections)
    {
        km::vec3 closestVertex = vertices.at(0);
        for (auto vertex : vertices)
        {
            if (km::dot(vertex, dir) > km::dot(closestVertex, dir))
            {
                closestVertex = vertex;
            }
        }

        auto index = kg::convexExtremalPoint(dir, hull);
        if (index == kg::invalidVertex) ++err;
        else
        {
            auto foundVertex = hull.vertex(index);
            err += !km::flt_releq(km::dot(foundVertex.pos, dir),
                                  km::dot(closestVertex, dir));
        }
        auto newIndex = kg::convexExtremalPoint(dir, hull, index);
        err += !km::flt_releq(km::dot(hull.vertex(newIndex).pos, dir),
                              km::dot(closestVertex, dir));
    }

    km::vec3 innerPoint{0., 0., 0.};

    auto feature = kg::convexClosestFeature(vertices.at(0),
                                            hull,
                                            innerPoint);

    err += !(feature.type == kg::FeatureType::Vertex);

    auto face0 = *hull.fbegin();
    auto face1 = hull.face(hull.edge(hull.edge(face0.outer).twin).face);

    feature = kg::convexClosestFeature(face0.center + face0.normal,
                                       hull, innerPoint);

    err += !(feature.type == kg::FeatureType::Face);
    err += !(feature.face == hull.edge(face0.outer).face);

    auto edge = hull.edge(face0.outer);

    auto twinEdge = hull.edge(edge.twin);

    auto edgeDir = hull.vertexAt(twinEdge.origin).pos
                 - hull.vertexAt(edge.origin).pos;

    feature = kg::convexClosestFeature(hull.vertexAt(edge.origin).pos
                                       + 0.5 * edgeDir
                                       + face0.normal
                                       + face1.normal,
                                       hull,
                                       innerPoint);

    err += !(feature.type == kg::FeatureType::Edge);
    err += !(feature.edge == face0.outer || feature.edge == edge.twin);

    return err;
}

int main()
{
    int err = 0;

    std::vector<km::vec3> vertices;
    appendIcosahedralVertices(vertices, 3.);

    err += testIcosahedron(vertices);

    vertices.push_back(km::vec3{0, 0, 0});
    vertices.push_back(km::vec3{1, 0, 0});
    vertices.push_back(km::vec3{0, 1, 0});
    vertices.push_back(km::vec3{0, 0, 1});
    vertices.push_back(km::vec3{-.5, 0, 0});
    vertices.push_back(km::vec3{0, 0, -.5});
    vertices.push_back(km::vec3{0, -.5, 0});

    err += testIcosahedron(vertices);

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dist(-1., 1.);

    for (size_t i = 0; i < 10000; ++i)
    {
        vertices.push_back({4.f*dist(gen), 3.f*dist(gen), 2.f * dist(gen)});
    }

    auto hull = kg::convexHull(vertices.data(), vertices.size());

    err += !isMeshClosed(hull);

    // Floats can represent ~7 decimals, and the generated points have a maximal
    // distance of ~11. Thus, our epsilon should be at least 11e-7 = 1.1e-6, but
    // the algorithm is currently somewhat coarse and at times allows somewhat
    // large discrepancies in order to merge many triangles to a single face.
    // Since we just want to test that this is *somewhat* convex, we simply test
    // with a very high error tolerance, to prevent bad random configurations
    // from the random data (we really don't want this to fail unless something
    // is *very* much not convex).
    // This can be updated as stability increases. Perhaps add option for using
    // doubles.
    err += !isConvex(hull, 0.025f);

    return err;
}
