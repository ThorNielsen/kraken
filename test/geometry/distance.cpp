#include <kraken/geometry/primitives.hpp>
#include <kraken/geometry/distance.hpp>
#include <kraken/math/comparison.hpp>

namespace kg = kraken::geometry;
namespace km = kraken::math;

int testTriangleDistance()
{
    using Triangle = kg::Triangle3d<double>;

    const Triangle t1{{1., 0., 0.}, {0., 1., 0.}, {0., 0., 1.}};
    const Triangle t2{{-2., 0., 0.}, {-2., 0.5, 0.}, {1., 1., 0.}};
    const Triangle t3{{-2., 0., 0.}, {-2., 0.5, 0.}, {1., 1., 1.}};

    int err = 0;

    err += !km::flt_eq(kg::distance(t1, t2), 0.);
    err += !km::flt_eq(kg::distance(t1, t3), 0.66259, 1e-5);

    return err;
}

int main()
{
    int err = 0;

    err += testTriangleDistance();

    return err;
}
