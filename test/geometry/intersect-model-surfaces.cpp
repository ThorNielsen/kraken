#include <cstdlib>
#include <filesystem>
#include <functional>
#include <kraken/geometry/bvh.hpp>
#include <kraken/geometry/bvhconstruction.hpp>
#include <kraken/geometry/intersection.hpp>
#include <kraken/globaltypes.hpp>
#include <kraken/graphics/import.hpp>

#include "common.hpp"
#include "kraken/geometry/boundingvolumes.hpp"
#include "kraken/geometry/primitives.hpp"
#include "kraken/math/constants.hpp"
#include "kraken/math/matrix.hpp"
#include "kraken/io/read/ply.hpp"

#include <algorithm>
#include <cstring>
#include <fstream>
#include <iostream>
#include <utility>
#include <vector>

namespace graphics = kraken::graphics;

int testSinglePLYIntersection(fs::path path,
                              km::Matrix<3, 3, float> rotation,
                              km::Matrix<3, 1, float> translation,
                              size_t expectedTriangles,
                              size_t expectedSegments,
                              size_t expectedVertices,
                              double expectedTotalArea,
                              double expectedTotalLength)
{
    int err = 0;

    std::ifstream input(path);
    kraken::io::DataSource dataSource(input);

    std::vector<U8> vertexData, indexData;

    kraken::ExternalMemoryAllocation vertAlloc([&vertexData](std::size_t count, std::size_t)
                                               {
                                                   vertexData.resize(count);
                                                   return vertexData.data();
                                               });
    kraken::ExternalMemoryAllocation indexAlloc([&indexData](std::size_t count, std::size_t)
                                                {
                                                    indexData.resize(count);
                                                    return indexData.data();
                                                });

    namespace ply = kraken::io::formats::ply;

    kraken::io::VertexLayout desiredLayout;
    desiredLayout.attributes.push_back
    ({
        .dataType = kraken::io::PrimitiveDataType::Float,
        .usage = kraken::io::VertexAttributeUsageType::Position,
        .componentCount = 3,
        .setIndex = 0,
    });
    ply::TriangleInfo triInfo;
    kraken::io::VertexLayout vertexLayout;
    std::size_t vertexCount;
    if (!ply::read(dataSource,
                   {
                       {ply::getVertexElementName(), ply::createRelocatingVertexParser(vertAlloc, desiredLayout, vertexLayout, vertexCount)},
                       {ply::getFaceElementName(), ply::createTriangulatingFaceParser(indexAlloc, triInfo)},
                   }))
    {
        std::cerr << "Failed to import PLY.\n";
        return 1;
    }

    if (vertexLayout != desiredLayout)
    {
        std::cerr << "Got an undesired layout.\n";
        return 1;
    }

    // Possibly inefficient, but the best we can do right now.
    std::function<size_t(size_t)> index;
    if (triInfo.indexByteWidth == 1)
    {
        index = [&indexData](size_t i){ return (size_t)indexData[i]; };
    }
    else if (triInfo.indexByteWidth == 2)
    {
        index = [&indexData](size_t i)
        {
            return (size_t)reinterpret_cast<const U16*>(indexData.data())[i];
        };
    }
    else if (triInfo.indexByteWidth == 4)
    {
        index = [&indexData](size_t i)
        {
            return (size_t)reinterpret_cast<const U32*>(indexData.data())[i];
        };
    }
    else
    {
        std::cerr << "Bad index byte width (got " << triInfo.indexByteWidth
                  << ", expected 1, 2 or 4).\n";
        return 1;
    }

    const auto* positions = reinterpret_cast<const kg::Vector3<F32>*>(vertexData.data());

    std::vector<kg::Triangle3d<float>> triangles(triInfo.triangleCount);
    // Cast valid due to standard-layout and no padding.
    auto* writeAt = reinterpret_cast<kg::Vector3<float>*>(triangles.data());
    for (size_t i = 0; i < 3*triInfo.triangleCount; ++i)
    {
        std::memcpy(writeAt++,
                    positions + index(i),
                    sizeof(kg::Vector3<float>));
    }

    const auto stride = sizeof(kg::Vector3<F32>);

    decltype(kg::createTriangleBVH<kg::Aabb<float>, float, U32>
                                  (positions,
                                   stride,
                                   (const U32*)nullptr,
                                   (const U32*)nullptr)) bvh;

    if (triInfo.indexByteWidth == 1)
    {
        auto indexBegin = reinterpret_cast<const U8*>(indexData.data());
        auto indexEnd = indexBegin + indexData.size();
        bvh = kg::createTriangleBVH<kg::Aabb<float>, float, U32>(positions,
                                                                 stride,
                                                                 indexBegin,
                                                                 indexEnd);
    }
    else if (triInfo.indexByteWidth == 2)
    {
        auto indexBegin = reinterpret_cast<const U16*>(indexData.data());
        auto indexEnd = indexBegin + indexData.size() / 2;
        bvh = kg::createTriangleBVH<kg::Aabb<float>, float, U32>(positions,
                                                                 stride,
                                                                 indexBegin,
                                                                 indexEnd);
    }
    else // indexByteWidth == 4
    {
        auto indexBegin = reinterpret_cast<const U32*>(indexData.data());
        auto indexEnd = indexBegin + indexData.size() / 4;
        bvh = kg::createTriangleBVH<kg::Aabb<float>, float, U32>(positions,
                                                                 stride,
                                                                 indexBegin,
                                                                 indexEnd);
    }

    std::vector<kg::Triangle3d<float>> triangleResults;
    std::vector<kg::Line3d<float>> segmentResults;
    std::vector<kg::Point3d<float>> pointResults;

    kg::processOverlaps(bvh, bvh,
    [&triangleResults, &segmentResults, &pointResults, rotation, translation]
    (const std::pair<kg::Triangle3d<float>, U32>& tri1,
     std::pair<kg::Triangle3d<float>, U32> tri2)
    {
        tri2.first.a = rotation * tri2.first.a + translation;
        tri2.first.b = rotation * tri2.first.b + translation;
        tri2.first.c = rotation * tri2.first.c + translation;
        auto [points, count] = intersect(tri1.first, tri2.first);
        if (count == 1) pointResults.push_back(points[0]);
        else if (count == 2)
        {
            segmentResults.push_back({points[0], points[1]-points[0]});
        }
        else
        {
            for (size_t i = 2; i < count; ++i)
            {
                // There are other (much nicer) ways of triangulating than this
                // brute-force triangle fan; but this is extremely simple, and
                // even correct for convex polygons. And also, all methods are
                // equal (excepting tendencies to create elongated triangles)
                // whenever we have a polygon in the plane ... which we have!
                triangleResults.push_back({points[0], points[1], points[i]});
            }
        }
    },
    [rotation, translation]
    (const kg::Aabb<float>& a, const kg::Aabb<float>& b)
    {
        return a.overlaps(b, rotation, translation);
    });

    err += !(triangleResults.size() == expectedTriangles);
    err += !(segmentResults.size() == expectedSegments);
    err += !(pointResults.size() == expectedVertices);

    double epsMul = 5. * (double)km::Constant<float>::epsilon;
    auto areaEps = epsMul * std::max(std::abs(expectedTotalArea), 1e-5);
    auto lengthEps = epsMul * std::max(std::abs(expectedTotalLength), 1e-5);

    double totalTriangleArea = 0.;
    for (const auto& triangle : triangleResults)
    {
        totalTriangleArea += .5 * length(cross(triangle.b - triangle.a,
                                               triangle.c - triangle.a));
    }

    err += !(std::abs(totalTriangleArea-expectedTotalArea) < areaEps);

    double totalSegmentLength = 0.;
    for (const auto& segment : segmentResults)
    {
        totalSegmentLength += length(km::highp_cast(segment.direction));
    }
    err += !(std::abs(totalSegmentLength-expectedTotalLength) < lengthEps);

    // Todo: Compute connectivity information, and split to continuous parts.

    return err;
}

int main()
{
    int err = 0;

    setWorkdirToTestDataFolder();
    km::Matrix<3, 3, float> rotation(1.f);
    rotation(0, 0) *= -1.f;
    rotation(1, 1) *= -1.f;
    km::Matrix<3, 1, float> translation(0.f);

    err += testSinglePLYIntersection("complex-model.ply",
                                     rotation,
                                     translation,
                                     0,
                                     1704,
                                     0,
                                     0.,
                                     46.17508075);

    return err;
}
