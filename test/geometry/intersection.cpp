#include <cstddef>
#include <kraken/geometry/intersection.hpp>

#include "common.hpp"
#include "kraken/geometry/primitives.hpp"
#include "kraken/math/constants.hpp"

#include <iostream>
#include <vector>
#include <stdexcept>

template <typename Prec>
kg::Triangle3d<Prec> toTriangle(const std::vector<Prec>& data)
{
    if (data.size() != 9)
    {
        throw std::logic_error("Expected exactly 9 elements.");
    }
    kg::Triangle3d<Prec> k3d;
    k3d.a = {data[0], data[1], data[2]};
    k3d.b = {data[3], data[4], data[5]};
    k3d.c = {data[6], data[7], data[8]};
    return k3d;
}

template <typename Prec>
int testTriangleIntersection(std::vector<Prec> firstTri,
                             std::vector<Prec> secondTri,
                             std::vector<Prec> expectedPolyData,
                             Prec epsilon = 50*km::Constant<Prec>::epsilon)
{
    auto triA = toTriangle(firstTri);
    auto triB = toTriangle(secondTri);
    auto [result, count] = kg::intersect(triA, triB, epsilon);
    auto expectedPoly = buildPolygon3d(expectedPolyData);
    if (count != expectedPoly.size()) return 1;
    std::vector<kg::Vector3<Prec>> resultPolygon;
    for (size_t i = 0; i < count; ++i)
    {
        resultPolygon.push_back(result[i]);
    }
    return !cyclicReleq(expectedPoly, resultPolygon, epsilon);
}

int main()
{
    int err = 0;

    // Test triangle against triangle in all non-edge/vertex cases.

    err += testTriangleIntersection<double>({1, 0, 0, 0, 1, 0, 0, 0, 1},
                                            {0, 0, 0, 1, 1, 0, 1, 1, 1},
                                            {.5, .5, 0, 1./3., 1./3., 1./3.});

    std::cout << "Errors: " << err << "\n";

    return err;
}
