#include "kraken/globaltypes.hpp"
#include "kraken/math/matrix.hpp"
#include <cstddef>
#include <kraken/geometry/meshrepair.hpp>

#include <algorithm>
#include <cstring>
#include <iostream>
#include <vector>

namespace kg = kraken::geometry;

using kraken::pointeradd;

/// \todo Dig up the old code for arbitrary simple polygon triangulation in 2d,
///       add here, and do the appropriate 2d-3d-conversion.

using kraken::math::vec4;
using kraken::math::vec3;
using kraken::math::vec2;

struct SomeVertex
{
    vec2 iAmPadding;
    float morePadding;
    vec3 position;
    float padderAdder;

    bool operator==(const SomeVertex& other) const noexcept
    {
        return !std::memcmp(this, &other, sizeof(*this));
    }
    bool operator!=(const SomeVertex& other) const noexcept
    {
        return !(*this == other);
    }
};

struct WonkyVertex
{
    vec4 aDummy;
    float inPlacePadder;
    vec3 position;
    vec4 bDummy;
};

SomeVertex mkvert(float x, float y, float z)
{
    SomeVertex vert;
    vert.iAmPadding = {2019.f, 666.f};
    vert.morePadding = -1992.f;
    vert.position = {x, y, z};
    vert.padderAdder = 100007.5f;
    return vert;
}

WonkyVertex mkwonky(float x, float y)
{
    WonkyVertex wonky;
    wonky.position = {x, y, 3.f};
    return wonky;
}

int testNonManifoldEdgeSplitting()
{
    int err = 0;

    std::vector<SomeVertex> vertices =
    {
        mkvert(-2, -2, 0), mkvert(-1, -2, 0), mkvert(-1,-1,0), mkvert(-1,-1,1),
        mkvert(-1,1,0), mkvert(0,-2,0), mkvert(0,-1,0), mkvert(0,0,0),
        mkvert(0,0,1), mkvert(0,1,0), mkvert(0,1,1), mkvert(1,-2,0),
        mkvert(1,-1,0), mkvert(1,0,0), mkvert(1,1,0), mkvert(2,-1,0),
    };
    std::vector<U16> indices =
    {
        0,1,2, 1,5,2, 2,5,6, 5,11,6, 6,11,12, 11,15,12, 12,15,13, 13,14,15,
        12,6,13, 7,13,6, 7,6,2, 2,6,3, 2,7,4, 4,7,9, 4,7,8, 8,9,10, 10,9,13,
        9,7,13, 9,13,14,
    };

    // The above has the following non-manifoldness properties:
    // * 3 edges shared by 3 triangles.
    // * 6 edges where triangles have reversed orientation.
    // * 15 edges used only by a single triangle.
    size_t threeEdgeCount = 3, twoEdgeCount = 6, oneEdgeCount = 16;

    for (size_t bits = 0; bits < 8; ++bits)
    {
        size_t countMultiEdges = (bits >> 2) & 1;
        size_t countDualEdges = (bits >> 1) & 1;
        size_t countSingleEdges = bits & 1;

        auto nmEdges = kg::findNonManifoldEdges(indices.size(),
                                                indices.data(),
                                                countMultiEdges,
                                                countDualEdges,
                                                countSingleEdges);
        auto expected = countMultiEdges * threeEdgeCount * (1+3*2)
                      + countDualEdges * twoEdgeCount * (1+2*2)
                      + countSingleEdges * oneEdgeCount * (1+1*2);
        err += !(nmEdges.size() == expected);
    }

    // This is redundant, but in order to actually check that there aren't any
    // issues if/when modifying the above loop, we also manually run on a few
    // important cases.
    auto nmEdges = kg::findNonManifoldEdges(indices.size(), indices.data(),
                                            true, true, false);

    err += !(nmEdges.size() == 3*(1+3*2)+6*(1+2*2));

    nmEdges = kg::findNonManifoldEdges(indices.size(), indices.data(),
                                       true, false, false);

    err += !(nmEdges.size() == 3*(1+3*2));

    const auto origVertices = vertices;
    const auto origIndices = indices;

    auto vertsBefore = vertices.size();

    kg::splitNonManifoldEdges(vertices.size(), vertices.data(), sizeof(vertices[0]),
                              indices.size(), indices.data(),
    [&vertices](size_t newVertexCount)
    {
        vertices.resize(newVertexCount);
        return vertices.data();
    });

    // We have three edges shared thrice so this should split them into three
    // edges each, but we should reuse the space of one of the edges. So in
    // total, we should add 2 vertices per edge, 3 edges and 2 duplicates. I.e.
    // 2*3*(3-1) = 12.
    err += !(vertices.size() == 2*3*(3-1)+vertsBefore);

    vertices = origVertices;
    indices = origIndices;
    kg::splitNonManifoldEdgesContinuously<float>(vertices.size(),
                                                 vertices.data(),
                                                 sizeof(vertices[0]),
                                                 offsetof(SomeVertex, position),
                                                 indices.size(),
                                                 indices.data(),
    [&vertices](size_t newVertexCount)
    {
        vertices.resize(newVertexCount);
        return vertices.data();
    });

    err += vertices.size() != origVertices.size() + 6;
    err += indices.size() != origIndices.size();

    return err;
}

int testTVertexRemoval()
{
    int err = 0;

    // Make 2d vertices for now, since T-vertex stuff should only happen in a
    // plane anyways.

    std::vector<WonkyVertex> vertices =
    {
        mkwonky(-2, 2), mkwonky(-1, 1), mkwonky(1, 1),   mkwonky(3, 1),
        mkwonky(-3, 0), mkwonky(0, -1), mkwonky(-2, -2), mkwonky(2, -2),
        mkwonky(1, -3)
    };
    std::vector<U16> indices =
    {
        0, 1, 2,  0, 2, 3,  0, 1, 4,  1, 4, 6,
        1, 6, 8,  1, 2, 5,  5, 7, 8,  2, 5, 7,
        2, 3, 7,
    };
    auto usagesBefore = kg::countVertexUsages(vertices.size(),
                                              indices.size(),
                                              indices.data());
    err += !(indices.size() == 3*9);

    kg::fixTVertices<float>(vertices.data(),
                            sizeof(vertices[0]),
                            offsetof(WonkyVertex, position),
                            indices.size(),
                            indices.data(),
    [&indices](size_t newCount)
    {
        indices.resize(newCount);
        return indices.data();
    });

    err += !(indices.size() == 3*10);

    auto usagesAfter = kg::countVertexUsages(vertices.size(),
                                             indices.size(),
                                             indices.data());

    // 1 and 8 are special because they are part of the triangle (hopefully)
    // being split. However, it is entirely unreasonable for them to be part of
    // two triangles.
    for (auto index : {0, 2, 3, 4, 7,  1, 8})
    {
        err += !(usagesBefore.at(index) == usagesAfter.at(index));
    }

    // As this is the T-vertex it will be part of *two* new triangles.
    err += !(usagesBefore.at(5)+2 == usagesAfter.at(5));
    // Same situation for number 6 as for 1 and 8, but we *really* should split
    // there.
    err += !(usagesBefore.at(6)+1 == usagesAfter.at(6));

    return err;
}

// Warning: This verification is O(n²) in the number of partitions.
bool isPartitionedCorrectly(const std::vector<size_t>& partition,
                            const std::vector<std::vector<size_t>>& partIndices)
{
    std::vector<bool> seen(partition.size(), false);
    for (const auto& partList : partIndices)
    {
        if (partList.empty()) return false;
        if (partList[0] >= partition.size()) return false;
        auto firstElem = partition[partList[0]];
        for (const auto& p : partList)
        {
            if (p > partition.size() || partition[p] != firstElem) return false;
            seen[p] = true;
        }
    }
    for (size_t i = 1; i < partIndices.size(); ++i)
    {
        for (size_t j = 0; j < i; ++j)
        {
            if (partition[partIndices[i][0]] == partition[partIndices[j][0]])
            {
                return false;
            }
        }
    }
    for (auto s : seen)
    {
        if (!s) return false;
    }
    return true;
}

int testConnectedComponents()
{
    int err = 0;

    std::vector<U16> indices = {0, 1, 2,  2, 3, 4,  5, 6, 7};
    auto conn = kg::getConnectedComponents(8, indices.size(), indices.data());
    err += !isPartitionedCorrectly(conn, {{0, 1}, {2}});

    indices = {0, 1, 2, 3, 4, 5, 6, 7, 8};
    conn = kg::getConnectedComponents(9, indices.size(), indices.data());
    err += !isPartitionedCorrectly(conn, {{0}, {1}, {2}});

    conn = kg::getConnectedComponents(18, indices.size(), indices.data());
    err += !isPartitionedCorrectly(conn, {{0}, {1}, {2}});

    indices = {3, 7, 2,  1, 4, 8,  0, 3, 1,  9, 5, 6,  9, 5, 6};
    conn = kg::getConnectedComponents(10, indices.size(), indices.data());
    err += !isPartitionedCorrectly(conn, {{0, 1, 2}, {3, 4}});

    indices = {1, 4, 8,  3, 7, 2,  0, 3, 7,  9, 5, 6,  9, 5, 6};
    conn = kg::getConnectedComponents(10, indices.size(), indices.data());
    err += !isPartitionedCorrectly(conn, {{0}, {1, 2}, {3, 4}});

    return err;
}

template <typename T>
bool cycleUntilEqualPrefix(std::vector<T>& toCycle,
                           const std::vector<T>& prefix,
                           size_t cycleLength = 1)
{
    if (toCycle.size() < prefix.size()
        || toCycle.size() < cycleLength
        || toCycle.size() % cycleLength != 0)
    {
        return false;
    }
    auto cycleCount = toCycle.size() / cycleLength;

    for (size_t i = 0; i < cycleCount; ++i)
    {
        bool match = true;
        for (size_t j = 0; j < prefix.size(); ++j)
        {
            if (prefix[j] != toCycle[j])
            {
                match = false;
                break;
            }
        }
        if (match) return true;
        std::rotate(toCycle.begin(),
                    toCycle.begin() + cycleLength,
                    toCycle.end());
    }
    return true;
}

template <typename T>
bool areEqual(const std::vector<T>& a, const std::vector<T>& b)
{
    if (a.size() != b.size()) return false;
    for (size_t i = 0; i < a.size(); ++i)
    {
        if (a[i] != b[i]) return false;
    }
    return true;
}

int testCycling()
{
    int err = 0;

    std::vector<U16> toCycle = {0, 1, 2, 3, 4, 5, 6, 7, 8};
    err += !cycleUntilEqualPrefix(toCycle, {3, 4, 5}, 3);
    err += !areEqual(toCycle, {3, 4, 5, 6, 7, 8, 0, 1, 2});
    err += !cycleUntilEqualPrefix(toCycle, {0, 1, 2}, 1);
    err += !areEqual(toCycle, {0, 1, 2, 3, 4, 5, 6, 7, 8});
    err += !cycleUntilEqualPrefix(toCycle, {5, 6}, 1);
    err += !areEqual(toCycle, {5, 6, 7, 8, 0, 1, 2, 3, 4});

    return err;
}

int testContoursAndFilling()
{
    int err = 0;

    std::vector<WonkyVertex> simpleTriVerts =
    {
        mkwonky(0, 0), mkwonky(1, 0), mkwonky(0.5, 1)
    };
    std::vector<U16> simpleTriIndices = {0, 1, 2};

    auto contours = kraken::geometry::extractContours(simpleTriIndices.size(),
                                                      simpleTriIndices.data());

    err += !(contours.size() == 1);
    if (!contours.empty())
    {
        cycleUntilEqualPrefix(contours[0], {0, 1, 2}, 3);
        err += !areEqual(contours[0], {0, 1, 2,  1, 2, 0,  2, 0, 1});
        for (auto& c : contours[0])
        {
            c = simpleTriIndices.at(c);
        }
        err += !areEqual(contours[0], {0, 1, 2,  1, 2, 0,  2, 0, 1});
        auto cavities = kraken::geometry::findContourCavities<float>
                            (simpleTriVerts.data(),
                             sizeof(WonkyVertex),
                             offsetof(WonkyVertex, position),
                             contours[0].size(),
                             contours[0].data());
        err += !cavities.empty();
    }

    std::vector<WonkyVertex> crossBoxVerts =
    {
        mkwonky(0, 2), mkwonky(2, 2), mkwonky(1, 1), mkwonky(0, 0),
        mkwonky(2, 0)
    };
    std::vector<U16> crossBoxIndices =
    {
        0, 3, 2,  3, 4, 2,  2, 4, 1,  0, 2, 1
    };
    // Note: We can cut off the last triangle of the cross box to get another
    // case.
    contours = kraken::geometry::extractContours(crossBoxIndices.size(),
                                                 crossBoxIndices.data());

    err += !(contours.size() == 1);
    if (!contours.empty())
    {
        cycleUntilEqualPrefix(contours[0], {0, 1, 2}, 3);
        err += !areEqual(contours[0],
                         {0, 1, 2,  3, 4, 5,  7, 8, 6,  11, 9, 10});
        for (auto& c : contours[0])
        {
            c = crossBoxIndices.at(c);
        }
        cycleUntilEqualPrefix(contours[0], {1, 0, 2}, 3);
        err += !areEqual(contours[0], {1, 0, 2,  0, 3, 2,  3, 4, 2,  4, 1, 2});

        auto cavities = kraken::geometry::findContourCavities<float>
                            (crossBoxVerts.data(),
                             sizeof(WonkyVertex),
                             offsetof(WonkyVertex, position),
                             contours[0].size(),
                             contours[0].data());
        err += !cavities.empty();
    }

    contours = kraken::geometry::extractContours(crossBoxIndices.size()-3,
                                                 crossBoxIndices.data());

    err += !(contours.size() == 1);
    if (!contours.empty())
    {
        cycleUntilEqualPrefix(contours[0], {0, 1, 2}, 3);
        err += !areEqual(contours[0],
                         {0, 1, 2,  3, 4, 5,  7, 8, 6,  8, 6, 7,  2, 0, 1});

        for (auto& c : contours[0])
        {
            c = crossBoxIndices.at(c);
        }

        auto cavities = kraken::geometry::findContourCavities<float>
                            (crossBoxVerts.data(),
                             sizeof(WonkyVertex),
                             offsetof(WonkyVertex, position),
                             contours[0].size(),
                             contours[0].data());
        err += !(cavities.size() == 1);
        if (!cavities.empty())
        {
            err += !areEqual(cavities[0], {1, 2, 0});
        }
    }

    auto filled = kraken::geometry::fillConcavities<float>
    (
        simpleTriVerts.data(),
        sizeof(WonkyVertex),
        offsetof(WonkyVertex, position),
        simpleTriIndices.size(),
        simpleTriIndices.data()
    );
    err += !filled.empty();
    filled = kraken::geometry::fillConcavities<float>
    (
        crossBoxVerts.data(),
        sizeof(WonkyVertex),
        offsetof(WonkyVertex, position),
        crossBoxIndices.size(),
        crossBoxIndices.data()
    );
    err += !filled.empty();

    filled = kraken::geometry::fillConcavities<float>
    (
        crossBoxVerts.data(),
        sizeof(WonkyVertex),
        offsetof(WonkyVertex, position),
        crossBoxIndices.size()-3,
        crossBoxIndices.data()
    );
    err += !(filled.size() == 3);
    cycleUntilEqualPrefix(filled, {0}, 1);
    err += !areEqual(filled, {0, 2, 1});

    return err;
}

int testUnusedVertexDetectionAndRemoval()
{
    using namespace kraken::geometry;
    int err = 0;

    std::vector<SomeVertex> refVerts =
    {
        mkvert(0, 1, 2), mkvert(3, 4, 5), mkvert(6, 7, 8),
        mkvert(1, 2, 3), mkvert(4, 1, 2), mkvert(6, 6, 6),
        mkvert(19, 10, 12), mkvert(48, -3, 2), mkvert(22, 19, 19),
    };
    std::vector<U8> list0{0, 1, 2, 0, 4, 5};
    auto vc = getVertexCountFromIndices(list0.size(), list0.data());
    err += vc != 6;
    err += !areEqual(getUnusedIndices(vc, list0.size(), list0.data()),
                     {3});

    auto toRemoveFrom = refVerts;

    std::vector<U8> mapping;

    auto remaining = removeUnusedVertices(toRemoveFrom.size(),
                                          toRemoveFrom.data(),
                                          sizeof(toRemoveFrom[0]),
                                          list0.size(),
                                          list0.data(),
                                          &mapping);

    err += !(remaining == 5);
    err += !(mapping.size() == remaining);

    for (size_t i = 0; i < mapping.size(); ++i)
    {
        err += !(toRemoveFrom.at(i) == refVerts.at(mapping.at(i)));
    }

    toRemoveFrom = refVerts;

    std::vector<U16> list1{1, 3, 7};
    vc = getVertexCountFromIndices(list1.size(), list1.data());
    err += vc != 8;
    err += !areEqual(getUnusedIndices(vc, list1.size(), list1.data()),
                     {0, 2, 4, 5, 6});

    std::vector<U16> wideMapping;

    remaining = removeUnusedVertices(toRemoveFrom.size(),
                                     toRemoveFrom.data(),
                                     sizeof(toRemoveFrom[0]),
                                     list1.size(),
                                     list1.data(),
                                     &wideMapping);
    err += !(remaining == 3);
    err += !(wideMapping.size() == remaining);

    for (size_t i = 0; i < wideMapping.size(); ++i)
    {
        err += !(toRemoveFrom.at(i) == refVerts.at(wideMapping.at(i)));
    }

    return err;
}

int main()
{
    int err = 0;

    err += !!kraken::geometry::getVertexCountFromIndices<U8>(0, nullptr);
    err += !!kraken::geometry::getVertexCountFromIndices<U16>(0, nullptr);
    err += !!kraken::geometry::getVertexCountFromIndices<U32>(0, nullptr);

    err += testTVertexRemoval();

    err += testConnectedComponents();

    err += testNonManifoldEdgeSplitting();

    err += testCycling();

    err += testContoursAndFilling();

    err += testUnusedVertexDetectionAndRemoval();

    std::cout << "Errors: " << err << "\n";

    return err;
}
