#include <cstddef>
#include <kraken/geometry/distance.hpp>
#include <kraken/geometry/primitivequeries.hpp>
#include <kraken/geometry/primitives.hpp>

#include "common.hpp"
#include "kraken/math/comparison.hpp"
#include "kraken/math/constants.hpp"
#include "kraken/math/matrix.hpp"

#include <random>
#include <vector>
#include <tuple>

int testPlanes()
{
    int err = 0;

    kg::Plane plane(km::vec3{0., 1., 0.},
                    km::vec3{1., 0., 0.},
                    km::vec3{0., 0., 1.});

    err += !km::parallel(plane.normal, km::vec3{1., 1., 1.});
    plane = kg::Plane(km::vec3{1., 1., 0.},
                      km::vec3{0., 1., 0.},
                      km::vec3{1., 0., 0.});
    err += !km::parallel(plane.normal, km::vec3{0., 0., 1.});

    err += !km::flt_releq(kg::distance(plane, km::vec3{0., 0., 0.}), 0.f);
    err += !km::flt_releq(kg::distance(plane, km::vec3{0., 0., 1.3f}), 1.3f);
    err += !km::flt_releq(kg::signedDistance(plane, km::vec3{0., 0., 1.3f}), 1.3f);
    err += !km::flt_releq(kg::signedDistance(plane, km::vec3{0., 0., -1.3f}), -1.3f);

    err += !(kg::signedDistance(plane, {0., 0., 0.2}) > 0.);
    err += !!(kg::signedDistance(plane, {0., 0., -0.2}) > 0.);

    plane = kg::Plane(km::vec3{0., 1., 0.},
                      km::vec3{1., 0., 0.},
                      km::vec3{0., 0., 1.});

    auto copied = plane;

    err += !(km::length(kg::closestPoint(copied, km::vec3{0.2, 0.2, 0.2})
                        - km::vec3{1.f/3.f, 1.f/3.f, 1.f/3.f}) <= 1e-5f);
    err += !(km::length(kg::closestPoint(copied, km::vec3{1, 1, 1})
                        - km::vec3{1.f/3.f, 1.f/3.f, 1.f/3.f}) <= 1e-5f);

    return err;
}

template <typename Prec>
bool isClosestPointPermutationInvariant(kg::Triangle3d<Prec> t, kg::Vector3<Prec> point)
{
    std::vector<std::tuple<size_t, size_t, size_t>> nonIdentityPermutations
    {
        {0,2,1}, {1,0,2}, {1,2,0}, {2,0,1}, {2,1,0}
    };
    decltype(t.a) vertices[3] = {t.a, t.b, t.c};
    auto closest = kg::closestPoint(t, point);
    for (auto [i0, i1, i2] : nonIdentityPermutations)
    {
        decltype(t) permuted{vertices[i0], vertices[i1], vertices[i2]};
        auto thisClosest = kg::closestPoint(permuted, point);

        auto epsilon = km::Constant<Prec>::epsilon * 200;
        if (km::squaredLength(closest-thisClosest) > epsilon * epsilon)
        {
            return false;
        }
    }
    return true;
}

template <typename Prec>
bool reprojectsToSame(kg::Triangle3d<Prec> t,
                      kg::Vector3<Prec> point,
                      Prec epsilon = km::Constant<Prec>::epsilon * 1250)
{
    auto projected = kg::closestPoint(t, point);
    auto reprojected = kg::closestPoint(t, projected);

    return km::squaredLength(projected-reprojected) < epsilon * epsilon;
}

template <typename Prec>
bool isInsideTriangle(kg::Vector3<Prec> point, kg::Triangle3d<Prec> tri)
{
    auto epsilon = km::Constant<Prec>::epsilon * 200;
    auto squaredEpsilon = epsilon * epsilon;
    auto normal = km::cross(tri.b-tri.a, tri.c-tri.a);
    auto ab = km::cross(point-tri.a, tri.b-tri.a);
    auto bc = km::cross(point-tri.b, tri.c-tri.b);
    auto ca = km::cross(point-tri.c, tri.a-tri.c);
    if (km::squaredLength(ab) <= squaredEpsilon
        || km::squaredLength(bc) <= squaredEpsilon
        || km::squaredLength(ca) <= squaredEpsilon)
    {
        return true;
    }
    return (km::dot(ab, normal) > 0.f) == (km::dot(bc, normal) > 0.f)
        && (km::dot(bc, normal) > 0.f) == (km::dot(ca, normal) > 0.f);
}

// This recursively subdivides the input triangle until a side length becomes
// smaller than the given epsilon, and then determines if the point is located
// in that smaller triangle. Note that not all subdivisions are performed -- in
// each step, the selected subdivided triangle is the one whose vertices are the
// closest to the point.
template <typename Prec>
bool isInTriangle(kg::Vector3<Prec> point, kg::Triangle3d<Prec> tri, Prec epsilon)
{
    auto epsSqr = epsilon * epsilon;
    if (squaredLength(tri.a-tri.b) <= epsSqr
        || squaredLength(tri.a-tri.c) <= epsSqr
        || squaredLength(tri.b-tri.c) <= epsSqr)
    {
        return true;
    }

    auto mab = Prec(.5) * (tri.a + tri.b);
    auto mbc = Prec(.5) * (tri.b + tri.c);
    auto mac = Prec(.5) * (tri.a + tri.c);

    kg::Triangle3d<Prec> t{tri.a, mab, mac};
    if (isInsideTriangle(point, t)) return isInTriangle(point, t, epsilon);
    t = {tri.b, mab, mbc};
    if (isInsideTriangle(point, t)) return isInTriangle(point, t, epsilon);
    t = {tri.c, mac, mbc};
    if (isInsideTriangle(point, t)) return isInTriangle(point, t, epsilon);
    t = {mab, mbc, mac};
    if (isInsideTriangle(point, t)) return isInTriangle(point, t, epsilon);

    return false;
}

template <typename Prec>
int testTCPPermutationAndReprojectionInvariance(size_t iterations,
                                                size_t seed = 0xfeef)
{
    auto epsilon = km::Constant<Prec>::epsilon * 200;
    int err = 0;
    std::mt19937 gen(seed);
    std::uniform_real_distribution<Prec> smalldist(Prec(-5), Prec(5));
    decltype(smalldist) largedist(Prec(-50), Prec(50));
    for (size_t i = 0; i < iterations; ++i)
    {
        auto point = generateRandomVec3(gen, largedist);
        kg::Triangle3d<Prec> tri;
        tri.a = generateRandomVec3(gen, smalldist);
        tri.b = generateRandomVec3(gen, smalldist);
        tri.c = generateRandomVec3(gen, smalldist);
        if (!isClosestPointPermutationInvariant(tri, point)) err |= 1;
        if (!reprojectsToSame(tri, point)) err |= 2;
        if (!isInTriangle(kg::closestPoint(tri, point), tri, epsilon)) err |= 4;
    }
    return err;
}

int testTriangle()
{
    int err = 0;

    kg::Triangle3d<float> triangle;
    triangle.a = {0., 0., 0.};
    triangle.b = {1., 0., 0.};
    triangle.c = {0., 1., 0.};

    err += !isClosestPointPermutationInvariant(triangle, km::vec3{0., 0., 0.});

    err += !(km::length(kg::closestPoint(triangle, km::vec3{0., 0., 0.})
                        - km::vec3{0., 0., 0.}) <= 1e-5f);

    err += !(km::length(kg::closestPoint(triangle, km::vec3{-1., -1., 0.})
                        - km::vec3{0., 0., 0.}) <= 1e-5f);

    err += !(km::length(kg::closestPoint(triangle, km::vec3{1., 1., 0.})
                        - km::vec3{0.5, 0.5, 0.}) <= 1e-5f);

    err += !(km::length(kg::closestPoint(triangle, km::vec3{0.25, 0.25, 0.})
                        - km::vec3{0.25, 0.25, 0.}) <= 1e-5f);

    err += !(km::length(kg::closestPoint(triangle, km::vec3{0.25, 1., 0.})
                        - km::vec3{0.125, 0.875, 0.}) <= 1e-5f);

    err += testTCPPermutationAndReprojectionInvariance<float>(25000);
    err += testTCPPermutationAndReprojectionInvariance<double>(25000);

    return err;
}

int testTetrahedron()
{
    int err = 0;

    kg::Tetrahedron<float> tetrahedron;
    tetrahedron.a = {0., 0., 0.};
    tetrahedron.b = {1., 0., 0.};
    tetrahedron.c = {0., 1., 0.};
    tetrahedron.d = {0., 0., 1.};

    err += !(km::length(kg::closestPoint(tetrahedron, km::vec3{0., 0., 0.})
                        - km::vec3{0., 0., 0.}) <= 1e-5f);

    err += !(km::length(kg::closestPoint(tetrahedron, km::vec3{-1., -1., 0.})
                        - km::vec3{0., 0., 0.}) <= 1e-5f);

    err += !(km::length(kg::closestPoint(tetrahedron, km::vec3{1., 1., 0.})
                        - km::vec3{0.5, 0.5, 0.}) <= 1e-5f);

    err += !(km::length(kg::closestPoint(tetrahedron, km::vec3{0.25, 0.25, 0.})
                        - km::vec3{0.25, 0.25, 0.}) <= 1e-5f);

    err += !(km::length(kg::closestPoint(tetrahedron, km::vec3{0.25, 1., 0.})
                        - km::vec3{0.125, 0.875, 0.}) <= 1e-5f);

    err += !(km::length(kg::closestPoint(tetrahedron,
                                         km::vec3{-0.493894f,
                                                  0.281021f,
                                                  0.940976f})
                        - km::vec3{0.f, 0.17002249f, 0.82997751f}) <= 1e-5f);

    err += !(km::length(kg::closestPoint(tetrahedron,
                                         km::vec3{0.006106f,
                                                  -0.218979f,
                                                  0.440976f})
                        - km::vec3{0.006106f,
                                   0.f,
                                   0.440976f}) <= 1e-5f);

    err += !(km::length(kg::closestPoint(tetrahedron,
                                         km::vec3{0.493894f,
                                                  0.281021f,
                                                  0.940976f})
                        - km::vec3{0.255264f, 0.0423907f, 0.702346f}) <= 1e-5f);
    return err;
}

int main()
{
    int err = 0;

    err += testPlanes();
    err += testTriangle();
    err += testTetrahedron();

    return err;
}
