#include "kraken/math/matrix.hpp"
#include <kraken/image/colour.hpp>


int main()
{
    int err = 0;

    namespace ki = kraken::image;

    err += !(ki::convert(0xf0f, ki::RGB4, ki::RGBA) == 0xff00ffff);
    err += !(ki::convert(0xff00ffff, ki::RGBA, ki::RGB4) == 0xf0f);
    err += !(ki::convert(0x1cafeb0b, ki::RGBA, ki::R4) == 0x1);
    err += !(ki::convert(0x1cafeb0b, ki::RGBA, ki::RG4) == 0x1a);
    err += !(ki::convert(0x1cafeb0b, ki::RGBA, ki::RGB4) == 0x1ae);
    err += !(ki::convert(0x1cafeb0b, ki::RGBA, ki::RGBA4) == 0x1ae0);
    err += !(ki::convert(0x1cafeb  , ki::RGB , ki::RGBA4) == 0x1aef);
    err += !(ki::convert(0x1caf    , ki::RG8 , ki::RGBA4) == 0x1a0f);
    err += !(ki::convert(0x1c      , ki::R8  , ki::RGBA4) == 0x100f);
    err += !(ki::convert(0xf, ki::RGBA1, ki::RGB) == 0xffffff);
    err += !(ki::convert(0x0123456789abcdef, ki::RGBA16, ki::RG4) == 0x04);
    err += !(ki::convert(0x0123456789abcdef, ki::RGBA16, ki::RGBA1) == 0x3);
    err += !(ki::convert(0x0123456789abcdef, ki::RGBA16, ki::RGBA2) == 0x1b);

    kraken::math::vec4 col{0.5f, 0.5f, 0.5f, 1.f};

    err += !(ki::pack(col, ki::RGBA8) == 0x7f7f7fff);
    err += !(ki::pack(col, ki::RGBA16) == 0x7fff7fff7fffffff);
    err += !(ki::pack(col, ki::RGB) == 0x7f7f7f);
    err += !(ki::pack(ki::unpack(0xabcdef, ki::RGB), ki::RGB) == 0xabcdef);
    kraken::math::vec4 expected{1.f, 0.f, 1.f, 0.f};
    err += !(ki::unpack(0xa, ki::RGBA1) == expected);
    err += !(ki::pack(expected, ki::RGBA1) == 0xa);
    err += !(ki::pack(expected, ki::RGB) == 0xff00ff);

    err += !(ki::iunpack(0xabcdefaf, ki::RGBA) ==
             kraken::math::ivec4{0xab, 0xcd, 0xef, 0xaf});

    return err;
}
