#include <cstddef>
#include <kraken/globaltypes.hpp>
#include <kraken/image/dct.hpp>

#include <array>
#include <iomanip>
#include <iostream>
#include <random>
#include <sstream>
#include <string>

namespace ki = kraken::image;

template <typename Type>
std::array<Type, 64>
generateSamples(S64 minVal, S64 maxVal, size_t seed = 0xc0071e5)
{
    std::mt19937 generator(seed);
    std::uniform_int_distribution<S16> dist(minVal, maxVal);
    std::array<Type, 64> values;
    for (auto& v : values)
    {
        v = static_cast<Type>(dist(generator));
    }
    return values;
}

bool areBlocksEqual(const std::array<U8, 64>& blockA,
                    const std::array<U8, 64>& blockB) noexcept
{
    for (size_t i = 0; i < 64; ++i)
    {
        if (blockA[i] != blockB[i]) return false;
    }
    return true;
}

std::string formatBlockDifference(const std::array<U8, 64>& blockA,
                                  const std::array<U8, 64>& blockB)
{
    std::stringstream ss;
    ss << std::hex << std::setw(2) << std::setfill('0');
    for (size_t y = 0; y < 8; ++y)
    {
        for (size_t x = 0; x < 8; ++x)
        {
            ss << std::setw(2) << std::setfill('0')
               << (size_t)blockA[8*y+x] << ' ';
        }
        ss << ' ';
        for (size_t x = 0; x < 8; ++x)
        {
            ss << std::setw(2) << std::setfill('0')
               << (size_t)blockB[8*y+x] << ' ';
        }
        ss << ' ';
        for (size_t x = 0; x < 8; ++x)
        {
            ss << "X."[blockA[8*y+x]==blockB[8*y+x]];
        }

        if (y != 7) ss << '\n';
    }
    return ss.str();
}

int test8Bit8x8IDCT()
{
    int err = 0;

    for (size_t i = 0; i < 64; ++i)
    {
        // For 8-bit data, the JPEG specification allows coefficients with
        // absolute values strictly below 2048 (=2^11).
        auto samples = generateSamples<F32>(-2047, 2047, i+0xc0027a);
        std::array<U8, 64> reference, got;
        ki::jpeg8x8IDCTNaive(samples.data(), reference.data());
        ki::jpeg8x8IDCTFactoredNaive(samples.data(), got.data());

        if (!areBlocksEqual(reference, got))
        {
            ++err;
            std::cerr << "Bad computation of factored-naïve; discrepancy is:\n"
                      << formatBlockDifference(reference, got)
                      << "\n";
        }

        ki::jpeg8x8IDCTBy1DFactorisationNaive(samples.data(), got.data());

        if (!areBlocksEqual(reference, got))
        {
            ++err;
            std::cerr << "Bad computation of 1d-factored; discrepancy is:\n"
                      << formatBlockDifference(reference, got)
                      << "\n";
        }

        ki::jpeg8x8IDCTBy1DFactorisationFast(samples.data(), got.data());

        if (!areBlocksEqual(reference, got))
        {
            ++err;
            std::cerr << "Bad computation of FAST 1d; discrepancy is:\n"
                      << formatBlockDifference(reference, got)
                      << "\n";
        }
    }

    return err;
}

int main()
{
    int err = 0;

    err += test8Bit8x8IDCT();

    std::cout << "Errors: " << err << "\n";
    return !!err;
}
