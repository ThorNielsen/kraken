#include <kraken/image/image.hpp>

#include <string>
#include <sstream>

namespace ki = kraken::image;

int testExportImport(const ki::Image& img, std::string fmt)
{
    std::stringstream stream;
    if (!img.write(stream, fmt)) return 1;
    ki::Image img2;
    if (!img2.read(stream)) return 2;
    if (img2.width() != img.width()) return 3;
    if (img2.height() != img.height()) return 4;

    auto c1type = img.colourType();
    auto c2type = img2.colourType();
    if (img.usesPalette()) c1type = img.paletteColourType();
    if (img2.usesPalette()) c2type = img2.paletteColourType();
    if (c2type.bitDepth < c1type.bitDepth)
    {
        return 5; // Export + import should not throw away precision.
    }
    if (c2type.channels < c1type.channels)
    {
        return 6; // Channels certainly shouldn't be thrown away either.
    }
    for (kraken::U32 y = 0; y < img.height(); ++y)
    {
        for (kraken::U32 x = 0; x < img.width(); ++x)
        {
            ki::Colour c1 = img.getPixel(x, y);
            ki::Colour c2 = img2.getPixel(x, y);
            if (img.usesPalette())
            {
                c1 = img.getPaletteElement(c1);
            }
            if (img2.usesPalette())
            {
                c2 = img2.getPaletteElement(c2);
            }
            if (c1type == c2type && c1 != c2)
            {
                return 7;
            }
            if (c1type != c2type)
            {
                auto expectedCol = ki::convert(c1, c1type, c2type);
                if (c1type.channels == 1)
                {
                    auto col = ki::iunpack(expectedCol, c2type);
                    switch (c2type.channels)
                    {
                    case 4: col.a = col.r; // fallthrough
                    case 3: col.b = col.r; // fallthrough
                    case 2: col.g = col.r; // fallthrough
                    default:
                        ;
                    }
                    expectedCol = ki::pack(col, c2type);
                }
                if (expectedCol != c2)
                {
                    return 8;
                }
            }
        }
    }
    return 0;
}

int main()
{
    int err = 0;

    ki::Image img(125, 127, ki::RGB8);
    img.fill([](kraken::U32 x, kraken::U32 y)
             { return x*y; });
    err += testExportImport(img, "PNM");
    err += testExportImport(img, "PNG");
    err += testExportImport(img, "BMP");
    img.resize(125, 127, ki::RGB16);
    img.fill([](kraken::U32 x, kraken::U32 y)
             { return x*y; });
    err += testExportImport(img, "PNM");
    err += testExportImport(img, "PNG");
    img.resize(27, 15, ki::R1);
    img.fill([](kraken::U32 x, kraken::U32 y)
             { return x*y; });
    err += testExportImport(img, "PNM");
    err += testExportImport(img, "PNG");
    err += testExportImport(img, "BMP");
    img.resize(27, 15, ki::R2);
    img.fill([](kraken::U32 x, kraken::U32 y)
             { return x*y; });
    err += testExportImport(img, "PNM");
    err += testExportImport(img, "PNG");
    img.resize(27, 15, ki::R4);
    img.fill([](kraken::U32 x, kraken::U32 y)
             { return x*y; });
    err += testExportImport(img, "PNM");
    err += testExportImport(img, "PNG");
    err += testExportImport(img, "BMP");
    img.resize(27, 15, ki::R8);
    img.fill([](kraken::U32 x, kraken::U32 y)
             { return x*y; });
    err += testExportImport(img, "PNM");
    err += testExportImport(img, "PNG");
    err += testExportImport(img, "BMP");
    img.resize(27, 15, ki::R16);
    img.fill([](kraken::U32 x, kraken::U32 y)
             { return x*y; });
    err += testExportImport(img, "PNM");
    err += testExportImport(img, "PNG");

    img.resize(53, 17, ki::R4);
    img.createPalette(16);
    err += img.paletteColourType() != ki::RGB8;
    for (kraken::U64 i = 0; i < 16; ++i)
    {
        img.setPaletteElement(i, (i*i+2*i+0xfa982bbc/(i+1))&0xffffff);
    }
    img.fill([](kraken::U32 x, kraken::U32 y)
             { return 0x29f0980f ^ x ^ y; });
    err += testExportImport(img, "BMP");
    err += testExportImport(img, "PNG");

    return err;
}
