#include <kraken/image/image.hpp>
#include <functional>

#include <kraken/math/integer.hpp>
#include <sstream>

#include <cmath>

#include <fstream>

namespace ki = kraken::image;
namespace math = kraken::math;

int testFill(ki::Image& img,
             std::function<ki::Colour(unsigned, unsigned)> fillfunc)
{
    img.fill(fillfunc);
    auto bits = img.colourType().bitDepth * img.colourType().channels;
    kraken::U64 mask = bits >= 64 ? 0xffffffffffffffff
                                  : (kraken::U64(1) << kraken::U64(bits)) - 1;
    if (!mask) return 1;
    for (unsigned y = 0; y < img.height(); ++y)
    {
        for (unsigned x = 0; x < img.width(); ++x)
        {
            ki::Colour col = fillfunc(x, y)&mask;
            if (img.getPixel(x, y) != col)
                return 1;
        }
    }
    if (img.usesPalette()) return 1;
    return 0;
}

int testFill(ki::Image& img,
             std::function<math::ivec4(unsigned, unsigned)> fillfunc)
{
    img.fill(fillfunc);
    auto bits = img.colourType().bitDepth * img.colourType().channels;
    kraken::U64 mask = bits >= 64 ? 0xffffffffffffffff
                                  : (kraken::U64(1) << kraken::U64(bits)) - 1;
    if (!mask) return 1;
    for (unsigned y = 0; y < img.height(); ++y)
    {
        for (unsigned x = 0; x < img.width(); ++x)
        {
            ki::Colour col = ki::pack(fillfunc(x, y), img.colourType())&mask;
            if (img.getPixel(x, y) != col)
                return 1;
        }
    }
    if (img.usesPalette()) return 1;
    return 0;
}

int testFill(ki::Image& img,
             std::function<math::uvec4(unsigned, unsigned)> fillfunc)
{
    img.fill(fillfunc);
    auto bits = img.colourType().bitDepth * img.colourType().channels;
    kraken::U64 mask = bits >= 64 ? 0xffffffffffffffff
                                  : (kraken::U64(1) << kraken::U64(bits)) - 1;
    if (!mask) return 1;
    for (unsigned y = 0; y < img.height(); ++y)
    {
        for (unsigned x = 0; x < img.width(); ++x)
        {
            ki::Colour col = ki::pack(fillfunc(x, y), img.colourType())&mask;
            if (img.getPixel(x, y) != col)
                return 1;
        }
    }
    if (img.usesPalette()) return 1;
    return 0;
}

int testFill(ki::Image& img,
             std::function<math::vec4(unsigned, unsigned)> fillfunc)
{
    img.fill(fillfunc);
    auto bits = img.colourType().bitDepth * img.colourType().channels;
    kraken::U64 mask = bits >= 64 ? 0xffffffffffffffff
                                  : (kraken::U64(1) << kraken::U64(bits)) - 1;
    if (!mask) return 1;
    for (unsigned y = 0; y < img.height(); ++y)
    {
        for (unsigned x = 0; x < img.width(); ++x)
        {
            ki::Colour col = ki::pack(fillfunc(x, y), img.colourType())&mask;
            if (img.getPixel(x, y) != col)
                return 1;
        }
    }
    if (img.usesPalette()) return 1;
    return 0;
}

int testMetadata(ki::Image& img)
{
    int err = 0;

    img.removeMetadata();
    err += img.hasMetadata("Comment");
    img.setMetadata("Comment", "Created by a blob.");
    err += !img.hasMetadata("Comment");
    err += !(img.getMetadata("Comment") == "Created by a blob.");

    img.setMetadata("Copyright", "Yes");
    img.setMetadata("Description", "A universal property of bilinear maps.");;
    img.setMetadata("Creation date", "3456-02-01: 12:34:56");
    img.setMetadata("Copyright", img.getMetadata("Copyright") + " (as if).");
    err += !(img.getMetadata("Comment") == "Created by a blob.");
    err += !(img.getMetadata("Copyright") == "Yes (as if).");

    return err;
}

ki::Image readFromString(const char* data)
{
    ki::Image img;
    std::stringstream ss(data);
    img.read(ss);
    return img;
}

int testPlainPNMReading()
{
    const char* imgStr =
    "P3\n"
    "7 5\n"
    "13\n"
    "13 13 13  13 13 00  13 00 13  13 00 00  00 13 13  00 13 00  00 00 13\n"
    "00 01 02  03 04 05  06 07 08  09 10 11  12 13 00  00 00 00  00 00 00\n"
    "01 02 03  04 05 06  07 08 09  10 11 12  13 00 00  00 00 00  00 00 00\n"
    "02 03 04  05 06 07  08 09 10  11 12 13  00 01 00  00 00 00  00 00 00\n"
    "02 03 05  07 11 13  02 03 05  07 11 13  02 03 05  07 11 13  08 08 08\n"
    ;
    std::vector<kraken::U8> binaryImg =
    {
        0x50, 0x36, 0x0a, 0x37, 0x20, 0x35, 0x0a, 0x31, 0x33, 0x0a, 0x0d, 0x0d,
        0x0d, 0x0d, 0x0d, 0x00, 0x0d, 0x00, 0x0d, 0x0d, 0x00, 0x00, 0x00, 0x0d,
        0x0d, 0x00, 0x0d, 0x00, 0x00, 0x00, 0x0d, 0x00, 0x01, 0x02, 0x03, 0x04,
        0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
        0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c,
        0x0d, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x03,
        0x05, 0x07, 0x0b, 0x0d, 0x02, 0x03, 0x05, 0x07, 0x0b, 0x0d, 0x02, 0x03,
        0x05, 0x07, 0x0b, 0x0d, 0x08, 0x08, 0x08
    };
    auto img = readFromString(imgStr);
    std::stringstream binaryImgStr;
    binaryImgStr.write(reinterpret_cast<char*>(binaryImg.data()),
                       binaryImg.size());
    ki::Image img2;
    img2.read(binaryImgStr);
    // Intensities should be mapped linearly, i.e. we should have:
    // 0  ->   0  (0x0)
    // 1  ->  20  (0x14)
    // 2  ->  39  (0x27)
    // 3  ->  59  (0x3b)
    // 4  ->  78  (0x4e)
    // 5  ->  98  (0x62)
    // 6  -> 118  (0x76)
    // 7  -> 137  (0x89)
    // 8  -> 157  (0x9d)
    // 9  -> 177  (0xb1)
    // 10 -> 196  (0xc4)
    // 11 -> 216  (0xd8)
    // 12 -> 235  (0xeb)
    // 13 -> 255  (0xff)
    // Formula: newValue = round((oldValue / oldMax) * newMax)
    kraken::U32 cols[7*5] =
    {
        0xffffff, 0xffff00, 0xff00ff, 0xff0000, 0x00ffff, 0x00ff00, 0x0000ff,
        0x001427, 0x3b4e62, 0x76899d, 0xb1c4d8, 0xebff00, 0x000000, 0x000000,
        0x14273b, 0x4e6276, 0x899db1, 0xc4d8eb, 0xff0000, 0x000000, 0x000000,
        0x273b4e, 0x627689, 0x9db1c4, 0xd8ebff, 0x001400, 0x000000, 0x000000,
        0x273b62, 0x89d8ff, 0x273b62, 0x89d8ff, 0x273b62, 0x89d8ff, 0x9d9d9d,
    };
    for (kraken::U32 y = 0; y < 5; ++y)
    {
        for (kraken::U32 x = 0; x < 7; ++x)
        {
            if (img.getPixel(x, y) != cols[7*y+x]) return 1;
            if (img2.getPixel(x, y) != cols[7*y+x]) return 1;
        }
    }
    return 0;
}

// Note that two images can be visually identical, but not completely identical,
// as it is possible to permute the palette, or just use a reduced set of
// colours if one has high bit depth and the other doesn't.
bool isIdentical(const ki::Image& a, const ki::Image& b)
{
    if (a.width() != b.width() || a.height() != b.height()) return false;
    if (a.colourType() != b.colourType()) return false;
    if (a.usesPalette() != b.usesPalette()) return false;
    if (a.usesPalette())
    {
        if (a.paletteColourType() != b.paletteColourType()) return false;
        if (a.paletteCount() != b.paletteCount()) return false;
        for (size_t i = 0; i < a.paletteCount(); ++i)
        {
            if (a.getPaletteElement(i) != b.getPaletteElement(i)) return false;
        }
    }
    for (size_t y = 0; y < a.height(); ++y)
    {
        for (size_t x = 0; x < a.width(); ++x)
        {
            if (a.getPixel(x, y) != b.getPixel(x, y)) return false;
        }
    }
    return true;
}

int main()
{
    int err = 0;

    ki::Image img(5, 7, ki::RG1);
    img.setPixel(0, 0, 0x2);
    img.setPixel(3, 0, 0x1);
    img.setPixel(0, 2, 0x3);

    err += !(img.getPixel(0, 0) == 0x2);
    err += !(img.getPixel(0, 1) == 0x0);
    err += !(img.getPixel(1, 0) == 0x0);

    err += !(img.getPixel(3, 0) == 0x1);
    err += !(img.getPixel(3, 1) == 0x0);
    err += !(img.getPixel(2, 0) == 0x0);
    err += !(img.getPixel(4, 0) == 0x0);

    err += !(img.getPixel(0, 2) == 0x3);
    err += !(img.getPixel(1, 2) == 0x0);
    err += !(img.getPixel(0, 1) == 0x0);
    err += !(img.getPixel(0, 3) == 0x0);

    err += testFill(img, [](unsigned x, unsigned y)
                    { return x+3*y; });
    err += testMetadata(img);

    img.resize(71, 23, ki::RG2);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return x+3*y; });

    img.resize(19, 22, ki::RGBA1);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return x+3*y; });

    img.resize(13, 13, ki::R1);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return x+3*y; });

    img.resize(102, 99, ki::RGBA16);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return 0xdeadbeefcafeface ^ ki::Colour(x*y+x+y);});

    img.resize(41, 485, ki::RGB16);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return 0x1deadfeca1ba4157 ^ ki::Colour(x*y+x+y);});

    img.resize(87, 22, ki::RG8);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return 0xb0bbeef465746578 ^ ki::Colour(x*y+x+y);});

    ki::Image img2(img);
    err += !isIdentical(img, img2);
    ki::Image img3(std::move(img2));
    err += !isIdentical(img, img3);

    ki::Image img4;
    img4 = img;
    err += !isIdentical(img, img4);
    ki::Image img5;
    img5 = std::move(img4);
    err += !isIdentical(img, img5);
    img5.setPixel(0, 3, 0x0);
    err += isIdentical(img, img5);

    img.resize(128, 256, ki::RGBA);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return 0x52474241 ^ ki::Colour(x*y+x+y);});


    img.resize(32, 32, ki::R4);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return ki::Colour(x*y); });

    img.resize(128, 128, ki::R1);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return ki::Colour(x+y*x); });
    img.resize(128, 128, ki::R2);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return ki::Colour(x+y); });
    img.resize(128, 128, ki::R4);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return ki::Colour(x+y); });
    img.resize(128, 128, ki::R8);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return ki::Colour(x+y); });
    img.resize(128, 128, ki::R16);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return ki::Colour(x+y+x*y); });

    img.resize(1920, 1080, ki::RGB8);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return ki::Colour(x+y+x*y); });

    img.resize(255, 255, ki::RGB8);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return ki::Colour(x+y); });

    img.resize(254, 254, ki::RGB8);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return ki::Colour(x+y); });
    img.resize(253, 253, ki::RGB8);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return ki::Colour(x+y); });
    img.resize(17, 35, ki::RGB8);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return ki::Colour(x*x*y*y+(43-x)*(84-y)*(37-y)); });
    img.resize(1920, 1080, ki::RGB8);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return ki::Colour(unsigned(std::abs(std::sin((float)x*y))*1000)); });
    img.resize(128, 128, ki::RGB16);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return ki::Colour(x+y+x*y); });

    err += testFill(img, [](unsigned x, unsigned y)
                    { return math::ivec4{(int)x, (int)y, int(x+y), int(x+5)}; });
    err += testFill(img, [](unsigned x, unsigned y)
                    { return math::uvec4{x, y, x+y, x+5}; });
    err += testFill(img, [](unsigned x, unsigned y)
                    { return math::vec4{x/128.f, y/171.f, (x+y)/259.f, x/129.f}; });

    // Test for correct handling of 32-bit pixels.
    img.resize(16, 16, ki::RGBA8);
    err += testFill(img, [](unsigned x, unsigned y)
                    { return math::uvec4{(x+y)&0xff, 253, x&0xff, 255}; });

    err += testPlainPNMReading();

    return err;
}
