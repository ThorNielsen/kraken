#include <kraken/globaltypes.hpp>
#include <kraken/image/image.hpp>
#include <kraken/utility/filesystem.hpp>
#include <kraken/utility/hufftree.hpp>
#include <kraken/utility/memory.hpp>
#include <kraken/utility/timer.hpp>

#include <bitset>
#include <iostream>
#include <map>
#include <regex>

namespace ki = kraken::image;
namespace fs = std::filesystem;

using namespace kraken;

struct JPEGMarkerInfo
{
    std::string name;
    std::string description;
};

JPEGMarkerInfo markerInfo(U8 byte)
{
    std::string number;
    switch (byte)
    {
    case 0x0: return {"Invalid", "Literal/escaped 0xff byte"};
    case 0xff: return {"Invalid", "Padded marker segment"};
    case 0x01: return {"TEM", "Arithmetic coding temporary marker (private use)"};
    case 0xc0: return {"SOF", "Start of frame: Baseline DCT (Huffman)"};
    case 0xc1: return {"SOF", "Start of frame: Extended sequential DCT (Huffman)"};
    case 0xc2: return {"SOF", "Start of frame: Progressive DCT (Huffman)"};
    case 0xc3: return {"SOF", "Start of frame: Lossless (sequential, Huffman)"};
    case 0xc4: return {"DHT", "Define Huffman tables"};
    case 0xc5: return {"SOF", "Start of frame: Differential sequential DCT (Huffman)"};
    case 0xc6: return {"SOF", "Start of frame: Differential progressive DCT (Huffman)"};
    case 0xc7: return {"SOF", "Start of frame: Differential lossless (sequential, Huffman)"};
    case 0xc8: return {"JPG", "Reserved for JPEG extensions"};
    case 0xc9: return {"SOF", "Start of frame: Extended sequential DCT (arithmetic)"};
    case 0xca: return {"SOF", "Start of frame: Progressive DCT (arithmetic)"};
    case 0xcb: return {"SOF", "Start of frame: Lossless (sequential, arithmetic)"};
    case 0xcc: return {"DAC", "Define arithmetic coding conditions"};
    case 0xcd: return {"SOF", "Start of frame: Differential sequential DCT (arithmetic)"};
    case 0xce: return {"SOF", "Start of frame: Differential progressive DCT (arithmetic)"};
    case 0xcf: return {"SOF", "Start of frame: Differential lossless (sequential, arithmetic)"};
    case 0xd0: case 0xd1: case 0xd2: case 0xd3:
    case 0xd4: case 0xd5: case 0xd6: case 0xd7:
        number = (byte-0xd0)["01234567"];
        return {"RST" + number, "Restart with count " + number + " modulo 8"};
    case 0xd8: return {"SOI", "Start of image"};
    case 0xd9: return {"EOI", "End of image"};
    case 0xda: return {"SOS", "Start of scan"};
    case 0xdb: return {"DQT", "Define quantisation tables"};
    case 0xdc: return {"DNL", "Define number of lines"};
    case 0xdd: return {"DRI", "Define restart interval"};
    case 0xde: return {"DHP", "Define hierarchical progression"};
    case 0xdf: return {"EXP", "Expand reference components"};
    case 0xe0: case 0xe1: case 0xe2: case 0xe3:
    case 0xe4: case 0xe5: case 0xe6: case 0xe7:
    case 0xe8: case 0xe9: case 0xea: case 0xeb:
    case 0xec: case 0xed: case 0xee: case 0xef:
        number = std::to_string(size_t(byte - 0xe0));
        return {"APP" + number, "Application segment #" + number};
    case 0xf0: case 0xf1: case 0xf2: case 0xf3:
    case 0xf4: case 0xf5: case 0xf6: case 0xf7:
    case 0xf8: case 0xf9: case 0xfa: case 0xfb:
    case 0xfc: case 0xfd:
        number = std::to_string(size_t(byte - 0xf0));
        return {"JPG" + number, "JPEG extension reserved marker #" + number};
    case 0xfe: return {"COM", "Comment"};
    default:
        number += "0123456789abcdef"[byte>>4];
        number += "0123456789abcdef"[byte&0xf];
        return {"RES", "Reserved marker byte 0x" + number};
    }
}

// Skips over all 0xff bytes and guarantees that the returned pointer is either
// 'end' or points to the second byte of a JPEG marker.
const U8* nextJPEGMarkerByte(const U8* currPos, const U8* end) noexcept
{
    bool wasLastBegin = false;
    while (currPos != end)
    {
        if (*currPos == 0xff) wasLastBegin = true;
        else if (wasLastBegin && *currPos) return currPos;
        else wasLastBegin = false;
        ++currPos;
    }
    return end;
}

void printJPEGMarkerAddresses(const U8* begin, const U8* end)
{
    const auto* curr = begin;
    while ((curr = nextJPEGMarkerByte(curr, end)) != end)
    {
        auto marker = markerInfo(*curr);
        std::cout << "0x"
                  << std::setw(6) << std::setfill('0') << std::hex
                  << static_cast<size_t>(curr-begin) - 1
                  << ": " << marker.name
                  << " (" << marker.description << ").\n";
    }
}

void printJPEGMarkersFromFile(const fs::path& path)
{
    size_t imageDataSize;
    auto rawImage = readEntireFile(path, imageDataSize);

    const auto* curr = rawImage.get();
    const auto* end = rawImage.get() + imageDataSize;
    std::cout << "JPEG markers in " << path.filename() << ":\n";
    printJPEGMarkerAddresses(curr, end);

    std::cout << "\n";
}

std::optional<ki::Image> readImage(const fs::path& path)
{
    std::ifstream input(path);
    if (!input.is_open())
    {
        return std::nullopt;
    }
    ki::Image img;
    if (!img.read(input)) return std::nullopt;
    return img;
}

// If dst is empty, then this will be written to stdout instead.
void decodeSingleImage(const fs::path& src, const fs::path& dst)
{
    std::ifstream input(src, std::ios::binary);

    auto timer = std::make_unique<kraken::ScopeTimer>("JPEG decoding total");
    ki::Image img;
    if (!img.read(input))
    {
        throw std::runtime_error("Failed to read image.");
    }

    timer = nullptr;

    img.transform(ki::ColourSpace::sRGB);

    std::string format = "pnm";
    if (dst.has_extension())
    {
        format = dst.extension().c_str() + 1;
    }

    std::ofstream fileOut;
    if (!dst.empty())
    {
        fileOut.open(dst, std::ios::binary);
        if (!fileOut.is_open())
        {
            throw std::runtime_error("Failed to open file for writing.");
        }
    }
    std::ostream& output = dst.empty() ? std::cout : fileOut;

    if (!img.write(output, format))
    {
        throw std::runtime_error("Failed to write image.");
    }
}

void decodeImages(const fs::path& directory, fs::path dstDir)
{
    if (dstDir.empty() || dstDir == "-") dstDir = directory;

    if (!fs::exists(dstDir) && !fs::create_directories(dstDir))
    {
        throw std::runtime_error("Target directory didn't exist, and it "
                                 "couldn't be created automatically.");
    }

    for (const auto& entry : fs::directory_iterator{directory})
    {
        if (entry.path().extension() != ".jpg") continue;
        ki::Image jpegDecoded;
        {
            try
            {
                auto res = readImage(entry.path());
                if (!res.has_value())
                {
                    std::cerr << "Error: Failed to read JPEG "
                              << entry.path() << ".\n";
                    continue;
                }
                jpegDecoded = std::move(res.value());
                jpegDecoded.transform(ki::ColourSpace::sRGB);
            }
            catch (std::exception& except)
            {
                std::cerr << "\033[1;31mCRITICAL\033[0m: Failed to read JPEG "
                          << entry.path() << " with exception "
                          << except.what() << "\n";
                continue;
            }
        }
        try
        {
            std::ofstream fileOut(dstDir / (entry.path().filename().string() + ".pnm"),
                                  std::ios::binary);
            if (!jpegDecoded.write(fileOut, "pnm"))
            {
                std::cerr << "Error: Failed to write "
                          << entry.path().filename() << "\n";
            }
        }
        catch (std::exception& except)
        {
            std::cerr << "\033[1;31mCRITICAL\033[0m: Writing decoded version "
                      << "of " << entry.path().filename() << " failed with "
                      << "exception " << except.what() << "\n";
        }
    }
}

/// Iterates through a directory and tests whether decoding works or not.
///
/// This considers all .jpg files in the given directory, attempts to decode
/// them and match them to a reference file. To indicate whether a given file
/// should be decodable or not, one can optionally place a dummy file indicating
/// acceptance of failures in the same directory. The naming scheme is as
/// follows:
///     * image.jpg - Image to test decoding of.
///     * image.jpg.reference.pnm - Expected result.
///     * image.jpg.allowfail - Allows decoding of image.jpg to fail.
///     * image.jpg.mustfail - If this exists, the image must not be decodable.
/// If there is no reference image, all that is tested is that the decoding
/// passes / doesn't pass (according to the (allow|must)fail), but otherwise no
/// comparisons are done.
///
/// \param directory The directory to look for the images within.
/// \param allowedPixelError Allowed error averaged over all components of all
///                          pixels; a value of 0 requires bit-perfection, while
///                          a value of 255 allows any image to pass (assuming
///                          8-bit images; for 12-bit images the required value
///                          would be 4095).
///
/// \returns The number of images that fails decoding.
int testDecoding(const fs::path& directory, double allowedPixelError = 4.)
{
    int errorCount = 0;
    for (const auto& entry : fs::directory_iterator{directory})
    {
        if (entry.path().extension() != ".jpg") continue;

        fs::path refPath = entry.path().string() + ".reference.pnm";

        auto hasReference = fs::exists(refPath);
        auto allowFail = fs::exists(entry.path().string() + ".allowfail");
        auto mustFail = fs::exists(entry.path().string() + ".mustfail");

        std::optional<ki::Image> jpegDecoded;
        {
            try
            {
                jpegDecoded = readImage(entry.path());

                if (jpegDecoded.has_value())
                {
                    jpegDecoded->transform(ki::ColourSpace::sRGB);
                }

            }
            catch (std::exception& except)
            {
                std::string severity = "\033[1;31mCRITICAL\033[0m";
                if (!mustFail && allowFail)
                {
                    severity = "\033[93mWarning\033[0m";
                }
                else if (mustFail)
                {
                    severity = "Expected failure";
                }
                std::cerr << severity << ": Failed to read JPEG "
                          << entry.path() << " with exception "
                          << except.what() << "\n";
            }
        }
        if ((mustFail || allowFail) == jpegDecoded.has_value())
        {
            ++errorCount;
            continue;
        }
        if (!hasReference) continue;

        ki::Image refImg;
        {
            auto res = readImage(refPath);
            if (!res.has_value())
            {
                throw std::runtime_error("Failed to read a reference image.");
            }
            refImg = std::move(res.value());
        }

        if (jpegDecoded->width() != refImg.width()
            || jpegDecoded->height() != refImg.height()
            || jpegDecoded->colourType() != refImg.colourType())
        {
            std::cerr << "Validation error: Read and validation image differs "
                         "in basic properties. JPEG was "
                      << jpegDecoded->width() << "x" << jpegDecoded->height()
                      << ":" << jpegDecoded->colourType()
                      << " and reference was "
                      << refImg.width() << "x" << refImg.height()
                      << ":" << refImg.colourType() << "\n";
            ++errorCount;
            continue;
        }

        math::Vector<4, std::intmax_t> diff{0, 0, 0, 0};
        auto maxV = std::numeric_limits<int>::max();
        auto minV = std::numeric_limits<int>::min();
        math::ivec4 minDiff{maxV, maxV, maxV, maxV},
                    maxDiff{minV, minV, minV, minV};

        for (size_t y = 0; y < jpegDecoded->height(); ++y)
        {
            for (size_t x = 0; x < jpegDecoded->width(); ++x)
            {
                auto jd = ki::iunpack(jpegDecoded->getPixel(x, y),
                                      jpegDecoded->colourType());
                auto ri = ki::iunpack(refImg.getPixel(x, y),
                                      refImg.colourType());
                auto currDiff = jd - ri;
                diff += math::prec_cast<std::intmax_t>(currDiff);
                minDiff = math::min(minDiff, currDiff);
                maxDiff = math::max(minDiff, currDiff);
            }
        }

        auto pixelCount = size_t(jpegDecoded->width())
                        * size_t(jpegDecoded->height());
        auto multiplier = 1. / (double)pixelCount;

        double avgDiscrep = .25*multiplier * (diff[0]+diff[1]+diff[2]+diff[3]);
        auto isBad = avgDiscrep > allowedPixelError;
        if (isBad)
        {
            ++errorCount;
            std::cout << entry.path().filename()
                      << " had too great discrepancies: ";
            for (size_t c = 0; c < jpegDecoded->colourType().channels; ++c)
            {
                if (c >= 4) throw std::logic_error("Bad number of channels.");
                std::cout << ' ' << diff[c]*multiplier
                          << '[' << minDiff[c]
                          << '/' << maxDiff[c]
                          << ']';
            }
            std::cout << '\n';
        }
    }

    return errorCount;
}

int main(int argc, char* argv[])
{
    std::string testArg = "--test";
    std::string testDirectoryArg = "jpegtests";
    char* ptrs[4] = {argv[0], testArg.data(), testDirectoryArg.data(), nullptr};
    if (argc < 2)
    {
        // We cheat and put the default arguments if none were given.
        argc = 3;
        argv = ptrs;
    }

    bool didTest = false;
    int errorCount = 0;

    for (int i = 1; i < argc;)
    {
        bool hadRequireFailure = false;

        auto argsLeft = [&i, argc]() noexcept -> size_t
        {
            return i <= argc ? size_t(argc - i) : 0;
        };

        auto requireArgCount = [&argsLeft, &hadRequireFailure]
        (size_t required, std::string_view whatRequired = "") noexcept -> bool
        {
            if (argsLeft() >= required) return true;
            hadRequireFailure = true;
            std::cerr << "Error: ";
            if (!whatRequired.empty()) std::cerr << "Required ";
            else std::cerr << whatRequired << " required ";
            std::cerr << required << " extra arguments, but there were only "
                      << argsLeft() << " extra arguments.\n";
            return false;
        };

        std::string currArg = argv[i++];

        if (currArg == "--print-markers"
            && requireArgCount(1, "Printing markers"))
        {
            printJPEGMarkersFromFile(argv[i++]);
        }
        else if (currArg == "--decode"
                 && requireArgCount(2, "Decoding JPEG image(s)"))
        {
            fs::path path = argv[i++];
            fs::path destination = argv[i++];
            auto pathStatus = fs::status(path);
            if (!fs::exists(pathStatus))
            {
                std::cerr << "Error: " << path << " doesn't exist.\n";
                return 1;
            }
            else if (fs::is_directory(pathStatus))
            {
                decodeImages(path, destination);
            }
            else
            {
                decodeSingleImage(path, destination);
            }
        }
        else if (currArg == "--test")
        {
            std::string_view testDir = testDirectoryArg;
            if (argsLeft()) testDir = argv[i++];
            if (!fs::exists(testDir))
            {
                std::cerr << "Error: Cannot test, either "
                          << testDir
                          << " doesn't exist, it is not a directory, or the "
                             "current working directory is bad.\n";
                return 2;
            }
            errorCount += testDecoding(testDir);
            didTest = true;
        }
        else
        {
            if (hadRequireFailure)
            {
                return 3;
            }
            else
            {
                std::cerr << "Invalid argument '" << currArg << "'\n";
                return 4;
            }
        }
    }

    if (didTest)
    {
        std::cerr << "Errors: " << errorCount << "\n";
    }

    return errorCount;
}
