#include <kraken/image/image.hpp>

#include <fstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>
#include <iostream>

namespace ki = kraken::image;

int testSuite(std::vector<std::pair<std::string, std::string>> names,
              std::string subfolder)
{
    int err = 0;
    std::string basename = "pngsuite/" + subfolder + "/";
    std::string prevname;
    int preverr = 0;
    for (auto& name : names)
    {
        if (err != preverr)
        {
            std::cerr << prevname << "\n";
        }
        preverr = err;
        prevname = name.first;
        auto testName = basename + name.first;
        auto referenceName = basename + name.second;
        ++err; // It is an error if we don't get to the end of the loop.
        std::ifstream reference(referenceName);
        if (!reference.is_open())
        {
            throw std::runtime_error("Couldn't open " + referenceName + ".");
        }
        ki::Image expected;
        if (!expected.read(reference))
        {
            reference.close();
            reference.open(referenceName);
            expected.read(reference);
            throw std::logic_error("Reference image not read correctly.");
        }

        std::ifstream pngimg(testName);
        if (!pngimg.is_open())
        {
            throw std::runtime_error("Couldn't open " + testName + ".");
        }
        ki::Image png;
        if (!png.read(pngimg)) continue;
        expected.applyPalette();
        png.applyPalette();
        if (expected.width() != png.width()) continue;
        if (expected.height() != png.height()) continue;
        if (expected.colourType() != png.colourType()) continue;
        for (kraken::U32 y = 0; y < expected.height(); ++y)
        {
            for (kraken::U32 x = 0; x < expected.width(); ++x)
            {
                if (expected.getPixel(x, y) != png.getPixel(x, y))
                {
                    continue;
                }
            }
        }

        --err; // We got to the end of the loop, so no errors were encountered!
    }
    return err;
}


int main()
{
    int err = 0;

    std::vector<std::pair<std::string, std::string>> basic;
    basic.push_back({"basi0g01.png", "basi0g01.png.pnm"});
    basic.push_back({"basi0g02.png", "basi0g02.png.pnm"});
    basic.push_back({"basi0g04.png", "basi0g04.png.pnm"});
    basic.push_back({"basi0g08.png", "basi0g08.png.pnm"});
    basic.push_back({"basi0g16.png", "basi0g16.png.pnm"});
    basic.push_back({"basi2c08.png", "basi2c08.png.pnm"});
    basic.push_back({"basi2c16.png", "basi2c16.png.pnm"});
    basic.push_back({"basi3p01.png", "basi3p01.png.pnm"});
    basic.push_back({"basi3p02.png", "basi3p02.png.pnm"});
    basic.push_back({"basi3p04.png", "basi3p04.png.pnm"});
    basic.push_back({"basi3p08.png", "basi3p08.png.pnm"});
    basic.push_back({"basi6a08.png", "basi6a08.png.bmp"});
    basic.push_back({"basn0g01.png", "basn0g01.png.pnm"});
    basic.push_back({"basn0g02.png", "basn0g02.png.pnm"});
    basic.push_back({"basn0g04.png", "basn0g04.png.pnm"});
    basic.push_back({"basn0g08.png", "basn0g08.png.pnm"});
    basic.push_back({"basn0g16.png", "basn0g16.png.pnm"});
    basic.push_back({"basn2c08.png", "basn2c08.png.pnm"});
    basic.push_back({"basn2c16.png", "basn2c16.png.pnm"});
    basic.push_back({"basn3p01.png", "basn3p01.png.pnm"});
    basic.push_back({"basn3p02.png", "basn3p02.png.pnm"});
    basic.push_back({"basn3p04.png", "basn3p04.png.pnm"});
    basic.push_back({"basn3p08.png", "basn3p08.png.pnm"});
    basic.push_back({"basn6a08.png", "basn6a08.png.bmp"});
    err += testSuite(basic, "01-basic");

    std::vector<std::pair<std::string, std::string>> sizes;
    sizes.push_back({"s01i3p01.png", "s01i3p01.png.pnm"});
    sizes.push_back({"s01n3p01.png", "s01n3p01.png.pnm"});
    sizes.push_back({"s02i3p01.png", "s02i3p01.png.pnm"});
    sizes.push_back({"s02n3p01.png", "s02n3p01.png.pnm"});
    sizes.push_back({"s03i3p01.png", "s03i3p01.png.pnm"});
    sizes.push_back({"s03n3p01.png", "s03n3p01.png.pnm"});
    sizes.push_back({"s04i3p01.png", "s04i3p01.png.pnm"});
    sizes.push_back({"s04n3p01.png", "s04n3p01.png.pnm"});
    sizes.push_back({"s05i3p02.png", "s05i3p02.png.pnm"});
    sizes.push_back({"s05n3p02.png", "s05n3p02.png.pnm"});
    sizes.push_back({"s06i3p02.png", "s06i3p02.png.pnm"});
    sizes.push_back({"s06n3p02.png", "s06n3p02.png.pnm"});
    sizes.push_back({"s07i3p02.png", "s07i3p02.png.pnm"});
    sizes.push_back({"s07n3p02.png", "s07n3p02.png.pnm"});
    sizes.push_back({"s08i3p02.png", "s08i3p02.png.pnm"});
    sizes.push_back({"s08n3p02.png", "s08n3p02.png.pnm"});
    sizes.push_back({"s09i3p02.png", "s09i3p02.png.pnm"});
    sizes.push_back({"s09n3p02.png", "s09n3p02.png.pnm"});
    sizes.push_back({"s32i3p04.png", "s32i3p04.png.pnm"});
    sizes.push_back({"s32n3p04.png", "s32n3p04.png.pnm"});
    sizes.push_back({"s33i3p04.png", "s33i3p04.png.pnm"});
    sizes.push_back({"s33n3p04.png", "s33n3p04.png.pnm"});
    sizes.push_back({"s34i3p04.png", "s34i3p04.png.pnm"});
    sizes.push_back({"s34n3p04.png", "s34n3p04.png.pnm"});
    sizes.push_back({"s35i3p04.png", "s35i3p04.png.pnm"});
    sizes.push_back({"s35n3p04.png", "s35n3p04.png.pnm"});
    sizes.push_back({"s36i3p04.png", "s36i3p04.png.pnm"});
    sizes.push_back({"s36n3p04.png", "s36n3p04.png.pnm"});
    sizes.push_back({"s37i3p04.png", "s37i3p04.png.pnm"});
    sizes.push_back({"s37n3p04.png", "s37n3p04.png.pnm"});
    sizes.push_back({"s38i3p04.png", "s38i3p04.png.pnm"});
    sizes.push_back({"s38n3p04.png", "s38n3p04.png.pnm"});
    sizes.push_back({"s39i3p04.png", "s39i3p04.png.pnm"});
    sizes.push_back({"s39n3p04.png", "s39n3p04.png.pnm"});
    sizes.push_back({"s40i3p04.png", "s40i3p04.png.pnm"});
    sizes.push_back({"s40n3p04.png", "s40n3p04.png.pnm"});
    err += testSuite(sizes, "02-sizes");

    std::vector<std::pair<std::string, std::string>> gamma;
    gamma.push_back({"g03n0g16.png", "g03n0g16.png.pnm"});
    gamma.push_back({"g03n2c08.png", "g03n2c08.png.pnm"});
    gamma.push_back({"g03n3p04.png", "g03n3p04.png.pnm"});
    gamma.push_back({"g04n0g16.png", "g04n0g16.png.pnm"});
    gamma.push_back({"g04n2c08.png", "g04n2c08.png.pnm"});
    gamma.push_back({"g04n3p04.png", "g04n3p04.png.pnm"});
    gamma.push_back({"g05n0g16.png", "g05n0g16.png.pnm"});
    gamma.push_back({"g05n2c08.png", "g05n2c08.png.pnm"});
    gamma.push_back({"g05n3p04.png", "g05n3p04.png.pnm"});
    gamma.push_back({"g07n0g16.png", "g07n0g16.png.pnm"});
    gamma.push_back({"g07n2c08.png", "g07n2c08.png.pnm"});
    gamma.push_back({"g07n3p04.png", "g07n3p04.png.pnm"});
    gamma.push_back({"g10n0g16.png", "g10n0g16.png.pnm"});
    gamma.push_back({"g10n2c08.png", "g10n2c08.png.pnm"});
    gamma.push_back({"g10n3p04.png", "g10n3p04.png.pnm"});
    gamma.push_back({"g25n0g16.png", "g25n0g16.png.pnm"});
    gamma.push_back({"g25n2c08.png", "g25n2c08.png.pnm"});
    gamma.push_back({"g25n3p04.png", "g25n3p04.png.pnm"});
    err += testSuite(gamma, "05-gamma");

    std::vector<std::pair<std::string, std::string>> filtering;
    filtering.push_back({"f00n0g08.png", "f00n0g08.png.pnm"});
    filtering.push_back({"f00n2c08.png", "f00n2c08.png.pnm"});
    filtering.push_back({"f01n0g08.png", "f01n0g08.png.pnm"});
    filtering.push_back({"f01n2c08.png", "f01n2c08.png.pnm"});
    filtering.push_back({"f02n0g08.png", "f02n0g08.png.pnm"});
    filtering.push_back({"f02n2c08.png", "f02n2c08.png.pnm"});
    filtering.push_back({"f03n0g08.png", "f03n0g08.png.pnm"});
    filtering.push_back({"f03n2c08.png", "f03n2c08.png.pnm"});
    filtering.push_back({"f04n0g08.png", "f04n0g08.png.pnm"});
    filtering.push_back({"f04n2c08.png", "f04n2c08.png.pnm"});
    filtering.push_back({"f99n0g04.png", "f99n0g04.png.pnm"});
    err += testSuite(filtering, "06-filtering");


    std::vector<std::pair<std::string, std::string>> palettes;
    palettes.push_back({"pp0n2c16.png", "pp0n2c16.png.pnm"});
    palettes.push_back({"pp0n6a08.png", "pp0n6a08.png.bmp"});
    palettes.push_back({"ps1n0g08.png", "ps1n0g08.png.pnm"});
    palettes.push_back({"ps1n2c16.png", "ps1n2c16.png.pnm"});
    palettes.push_back({"ps2n0g08.png", "ps2n0g08.png.pnm"});
    palettes.push_back({"ps2n2c16.png", "ps2n2c16.png.pnm"});
    err += testSuite(palettes, "07-palettes");

    std::vector<std::pair<std::string, std::string>> ancillary;
    ancillary.push_back({"ccwn2c08.png", "ccwn2c08.png.pnm"});
    ancillary.push_back({"ccwn3p08.png", "ccwn3p08.png.pnm"});
    ancillary.push_back({"cdfn2c08.png", "cdfn2c08.png.pnm"});
    ancillary.push_back({"cdhn2c08.png", "cdhn2c08.png.pnm"});
    ancillary.push_back({"cdsn2c08.png", "cdsn2c08.png.pnm"});
    ancillary.push_back({"cdun2c08.png", "cdun2c08.png.pnm"});
    ancillary.push_back({"ch1n3p04.png", "ch1n3p04.png.pnm"});
    ancillary.push_back({"ch2n3p08.png", "ch2n3p08.png.pnm"});
    ancillary.push_back({"cm0n0g04.png", "cm0n0g04.png.pnm"});
    ancillary.push_back({"cm7n0g04.png", "cm7n0g04.png.pnm"});
    ancillary.push_back({"cm9n0g04.png", "cm9n0g04.png.pnm"});
    ancillary.push_back({"cs3n2c16.png", "cs3n2c16.png.pnm"});
    ancillary.push_back({"cs3n3p08.png", "cs3n3p08.png.pnm"});
    ancillary.push_back({"cs5n2c08.png", "cs5n2c08.png.pnm"});
    ancillary.push_back({"cs5n3p08.png", "cs5n3p08.png.pnm"});
    ancillary.push_back({"cs8n2c08.png", "cs8n2c08.png.pnm"});
    ancillary.push_back({"cs8n3p08.png", "cs8n3p08.png.pnm"});
    ancillary.push_back({"ct0n0g04.png", "ct0n0g04.png.pnm"});
    ancillary.push_back({"ct1n0g04.png", "ct1n0g04.png.pnm"});
    ancillary.push_back({"cten0g04.png", "cten0g04.png.pnm"});
    ancillary.push_back({"ctfn0g04.png", "ctfn0g04.png.pnm"});
    ancillary.push_back({"ctgn0g04.png", "ctgn0g04.png.pnm"});
    ancillary.push_back({"cthn0g04.png", "cthn0g04.png.pnm"});
    ancillary.push_back({"ctjn0g04.png", "ctjn0g04.png.pnm"});
    ancillary.push_back({"ctzn0g04.png", "ctzn0g04.png.pnm"});
    ancillary.push_back({"exif2c08.png", "exif2c08.png.pnm"});
    err += testSuite(ancillary, "08-ancillary");

    std::vector<std::pair<std::string, std::string>> idatchunks;
    idatchunks.push_back({"oi1n0g16.png", "oi1n0g16.png.pnm"});
    idatchunks.push_back({"oi1n2c16.png", "oi1n2c16.png.pnm"});
    idatchunks.push_back({"oi2n0g16.png", "oi2n0g16.png.pnm"});
    idatchunks.push_back({"oi2n2c16.png", "oi2n2c16.png.pnm"});
    idatchunks.push_back({"oi4n0g16.png", "oi4n0g16.png.pnm"});
    idatchunks.push_back({"oi4n2c16.png", "oi4n2c16.png.pnm"});
    idatchunks.push_back({"oi9n0g16.png", "oi9n0g16.png.pnm"});
    idatchunks.push_back({"oi9n2c16.png", "oi9n2c16.png.pnm"});
    err += testSuite(idatchunks, "09-idatchunks");

    std::vector<std::pair<std::string, std::string>> compression;
    compression.push_back({"z00n2c08.png", "z00n2c08.png.pnm"});
    compression.push_back({"z03n2c08.png", "z03n2c08.png.pnm"});
    compression.push_back({"z06n2c08.png", "z06n2c08.png.pnm"});
    compression.push_back({"z09n2c08.png", "z09n2c08.png.pnm"});
    err += testSuite(compression, "10-compression");

    std::vector<std::string> corrupted;
    corrupted.push_back("pngsuite/11-corrupted/xc1n0g08.png");
    corrupted.push_back("pngsuite/11-corrupted/xc9n2c08.png");
    corrupted.push_back("pngsuite/11-corrupted/xcrn0g04.png");
    corrupted.push_back("pngsuite/11-corrupted/xcsn0g01.png");
    corrupted.push_back("pngsuite/11-corrupted/xd0n2c08.png");
    corrupted.push_back("pngsuite/11-corrupted/xd3n2c08.png");
    corrupted.push_back("pngsuite/11-corrupted/xd9n2c08.png");
    corrupted.push_back("pngsuite/11-corrupted/xdtn0g01.png");
    corrupted.push_back("pngsuite/11-corrupted/xhdn0g08.png");
    corrupted.push_back("pngsuite/11-corrupted/xlfn0g04.png");
    corrupted.push_back("pngsuite/11-corrupted/xs1n0g01.png");
    corrupted.push_back("pngsuite/11-corrupted/xs2n0g01.png");
    corrupted.push_back("pngsuite/11-corrupted/xs4n0g01.png");
    corrupted.push_back("pngsuite/11-corrupted/xs7n0g01.png");
    for (auto& name : corrupted)
    {
        std::ifstream input(name);
        if (!input.is_open())
        {
            throw std::runtime_error("Cannot open " + name + ".");
        }
        ki::Image img;
        if (img.read(input)) ++err;
    }
    return err;
}
