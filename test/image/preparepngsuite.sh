#!/bin/bash
# This is a script for unpacking and setting up Willem van Schaik's png test
# suite for automatic testing. The file can be downloaded from his homepage:
# http://www.schaik.com/pngsuite/
# Or, directly:
# http://www.schaik.com/pngsuite/PngSuite-2017jul19.tgz
# Note that for this script to work, the downloaded file MUST be the one ending
# in '.tgz'.
# The rationale for not simply including the suite directly is twofold - a minor
# reason is that it will *slightly* bloat the size of the repository, but more
# importantly, although the test suite is licensed permissively, it is not under
# a standard free license and thus it is not entirely clear whether it is
# redistributable commercially (since the license states "for any purpose and
# without fee"). Thus, to avoid complicating the distribution terms of this
# library, the test suite has been left out.
# The above URL was found to be correct and working at 2020-02-09, but this is
# not necessarily true in the future. However, a snapshot was saved using the
# Internet Archive's Wayback Machine, so if the above URL is rotten or does not
# work, the following may work instead:
# https://web.archive.org/web/20200209183559/http://www.schaik.com/pngsuite/PngSuite-2017jul19.tgz
FNAME="$1"
if [[ "$FNAME" == "" ]]; then
    FNAME="PngSuite-2017jul19.tgz"
fi
SUM=$(sha256sum "$FNAME" | grep -E "^b9cf4839fa637e8bc0fb9fcffc1f1ad071723b99df28b8f9d42de3effdf77b7e")
if [[ "$SUM" == "" ]]; then
    echo "Error, checksum mismatch."
    exit 1
fi
mkdir pngsuite || exit 2
cd pngsuite
echo "H4sIAIlOQF4AA22OOxLCMAwF+1yDBheaSfhzGkaRTdDEkT2SXYTTE+q43n2zD71/WR09a6CSdD32
A4xoTK7DHTqB8TeY6w57dt5mNE+aqnhKMWnTukBRFMuoQWhtNa4w4bJgC93gzbEEZZla+A4ZYyjl
f3BPH4BCHCNqM/sE9ljoU2VuzYceKC1ZgxknaQnDJqjWXIJ33Q90xf/3VgEAAA==" | base64 -d | zcat > CMakeLists.txt

tar --extract --file "../$FNAME"

mkdir 01-basic
mv bas*.png 01-basic
cd 01-basic
echo "H4sIAJpNQF4AA43UsWrDMBSF4b1P4bFdQqKKoL1rabpmKolJjKH5a1r6/vVQi+MbEp/Bg4QuPwLr
a784993v9+nj3H+eHo+Hn37drTerga6ZLV527/vd2+v+6aG9PTJ+l+ZqY2k0aS1ZtRRrya5lrWWr
lmMt27WitWLVSqwVt7bZSu1/YY1ITTbujqZW7lYX1kitzTYWRuVudWGNSM292/MgL6AurJFam20s
jSatGS9gOqU18wWMJ7PWjBcwndKa+QLGk0Vrxl8yndKa+ZdsD1KrC2tkdbwMzdXGvVHUSSwniU5i
O4k6ieUk0UlsJ1EnsZwkOontJOoklpNEJ7GdRJ3EcpLoJLaTqJNYThKdxHYSdRLLSaKT2E6iTmI5
SXQS20nUSSwniU5iO4k6ieUk0UlsJ1EnsZwkOontJOoklpNEJ7nh5B+IhAOqUAoAAA==" | base64 -d | zcat > CMakeLists.txt
find . -regextype egrep -regex "./bas[in][023]...\.png" -exec bash -c 'pngtopnm {} > "$(basename {}).pnm"' ';'
convert basi6a08.png basi6a08.png.bmp
convert basn6a08.png basn6a08.png.bmp
# Some images left unused (have not yet found a suitable format for storing 16-
# bit 4-channel images).
cd ..

mkdir 02-sizes
mv s*.png 02-sizes
cd 02-sizes
echo "H4sIALdNQF4AA43VvWpbURBF4d5PodJpgnxmzl+d1kRpVbkQthEkJybG7+8kUmBnG4lV3GKGO6z7
Vffwcz0dn99+PT48Hb8/3r5u747xsr37/LKeN/8NX3bf9ruv9/tPN4fLJ7+fH5sPi2unS2sL1ZbX
Fq0VtRVkK24r2FbUVpCtuK1gW6gtkC3cFtgWagtkC7cFtqXaEtnSbYltqbZEtnRbYlv9813lXJMB
nZxrtrh2urS2UG15bdFaU1tDtua2hm1NbQ3ZmtsatnW1dWTrbuvY1tXWka27rWPbUNtAtuG2gW1D
bQPZhtsGtk21TWSbbpvYNtU2kW26bVJb/P035ammAzo51Xxx7XRpbaHa8tqitVBbIFu4LbAt1BbI
Fm4LbEu1JbKl2xLbUm2JbOm2xLaqtops1W0V26raKrJVt1Vsa2pryNbc1rCtqa0hW3Nbw7auto5s
3W0d27raOrJ1t3VsG2obyDbcNrBtqG0g23DbwLaptols020T26baJrJNt01qy63YdEAnp5ovrp0u
rRGbvPWvdsn2DlYIG3l4DwAA" | base64 -d | zcat > CMakeLists.txt
find . -name "*.png" -exec bash -c 'pngtopnm {} > "$(basename {}).pnm"' ';'
cd ..

# These also have the problem that some are two- or four-channel images and as
# such there is no suitable format (considering that some also has bit depth
# 16). Fortunately, neither background colour nor poor-man's transparency is
# required to be supported (although it would be nice to do so).
mkdir 03-backgroundcolour
mv bg*.png 03-backgroundcolour

mkdir 04-transparency
mv t*.png 04-transparency

mkdir 05-gamma
mv g*.png 05-gamma
cd 05-gamma
echo "H4sIAN1NQF4AA43TMQvCQAwF4N1f0VEXuV4T4+4q1rWTQ2lLQWsR/P8WhFwa8XjDDQn3CHzw2ufU
j8P71d368d5th1BNYSgP+3kaitVwqq9NfTk3u037P7K8R/GzyEZjG456TQcootdWi2y0mgPpNR2g
iF5bLTJRspIESZKXJFiSrCRBkuQlCZYkK0mQJHlJgiXZSjIkyV6SYUm2kgxJspdkWJKtJEOS7CUZ
lhQrKZCkeEmBJcVKCiQpXlJgSbGSAkmKlxRUsgxG0g5Q5HvNL7LRJGkHKKLXQMnlZ5K0AxTRa6Bk
tO2OULujb7dfZKNJMkLtjr7dfpGNJskItTv6dvtFin4AzoNuo7wHAAA=" | base64 -d | zcat > CMakeLists.txt
find . -name "*.png" -exec bash -c 'pngtopnm {} > "$(basename {}).pnm"' ';'
cd ..

mkdir 06-filtering
mv f*.png 06-filtering
cd 06-filtering
echo "H4sIAOtNQF4AA43PvQrCQBAE4N6nSGkauf0pTG0rxjaVxZE7AnqK4PurBOJmg2GKK2ZhmPvivaQh
v579JQ3XfptCKCGH/e5RcjULh/bctadjV2/i/8rn3arFYbXK0axNAapMa7PDSpWsjSAbeRvBNrI2
gmzkbQTb2NoYsrG3MWxja2PIxt7GsE2sTSCbeJvANrE2gWzibQLb1NoUsqm3KWxTa1PIpt6mqK1p
vv/Scc0GqDKu+cOv+gYlrvRTugQAAA==" | base64 -d | zcat > CMakeLists.txt
find . -name "*.png" -exec bash -c 'pngtopnm {} > "$(basename {}).pnm"' ';'
cd ..

mkdir 07-palettes
mv p*.png 07-palettes
cd 07-palettes
echo "H4sIAP1NQF4AA0vOz0vLTC8tSo1Py8xJ1SgoMMgzSjY00yvIS1dA4Tj7B0T6+/lEanIl49YCxLkK
GAJ4tZolGljAbYNziNKil5RboIAhgFtrsWGeQTrMNmQOUVogfkMXwKsVEZLIHKK0wG0jMiSLjZD9
ZkSU34zQ/WZEtN+MkP1mRJTfjND9ZoTLbwBypdOKlAIAAA==" | base64 -d | zcat > CMakeLists.txt
find . -regextype egrep -regex "./p..n[02]...\.png" -exec bash -c 'pngtopnm {} > "$(basename {}).pnm"' ';'
convert pp0n6a08.png pp0n6a08.png.bmp
cd ..

mkdir 08-ancillary
mv c*.png exif*.png 08-ancillary
cd 08-ancillary
echo "H4sIAAlOQF4AA43UvU7DMBQF4J2n6EgXFKeEOHNXRFk7MQT/JKKhwomK8vQEXNKTW9U6Q4d7ldNP
R7FTf3a2ccOXebPNh7mv61OX15l+OHZutRi2u9f97uV5v76rb0em32F1tUhGN0fQ5oGKzNpicTv6
bqEbDlQkanKRiHrUPKV5qXlaC6gFSgtSC7Q2oDZQ2iC1gdW8+n3Dj1HDgYpETS4S0RzOJA5U5Kzl
7Jk8ZF3m/rvhQEWiJheJaIlaSWml1Epaq1CrKK2SWsVqYTOdJ/UUNRyoSNTkIhm9nBIcqMiskack
FHDfcKAiZ61g79v0JHQrqG6F7FbQ3TR201Q3LbtpupvGbprqpmU3zXbr8Xb31O3u5e2Wi0RUoaYo
TUlN0ZpBzVCakZqhNYuapTQrNUtrDjVHaU5qjtY8ap7SvNQ8rbWotZTWSq2ltRG1kdJGqY2kZr4b
O38+FgMV+fvzq8Ul+gP7R/8rLAsAAA==" | base64 -d | zcat > CMakeLists.txt
find . -name "*.png" -exec bash -c 'pngtopnm {} > "$(basename {}).pnm"' ';'
cd ..

mkdir 09-idatchunks
mv oi*.png 09-idatchunks
cd 09-idatchunks
echo "H4sIABJOQF4AA43PsQqDQBAE0D5fYRmboMMS2DptiGmtLA49DvSUQP4/VrJuUKbYYhaG4YU5Dyl+
P303pLG/zqnOVazvtyXHYhcezbttXs+2vITjynpT8fc4rSKYtS1QlW1t9zipwtpA2eBtoG2wNlA2
eBtom1ibUDbxNqFtYm1C2cTbhLaptSllU29T2qbWppRNvU2PbD/6j2dCcAMAAA==" | base64 -d | zcat > CMakeLists.txt
find . -name "*.png" -exec bash -c 'pngtopnm {} > "$(basename {}).pnm"' ';'
cd ..

mkdir 10-compression
mv z*.png 10-compression
cd 10-compression
echo "H4sIABxOQF4AA0vOz0vLTC8tSo1Py8xJ1agyMMgzSjaw0CvIS1dA4Tj7B0T6+/lEanIl49YCxLkK
GAJ4tBoj22ZMlG3G6LYZE22bGbJtZkTZZoZumxnRtlki22ZJlG2W6LZZ4rINAFhYXQe4AQAA" | base64 -d | zcat > CMakeLists.txt
find . -name "*.png" -exec bash -c 'pngtopnm {} > "$(basename {}).pnm"' ';'
cd ..

mkdir 11-corrupted
mv x*.png 11-corrupted
cd 11-corrupted
echo "H4sIACVOQF4AA43RsQrCMBRA0d2vcNRFkjRQnV2lde3kkDSxUKK0Cv38UihyFRMyXsgJ7yXmEVzn
30N7c13f7iYjg/DieHgGv/2Kc31t6urS7Dfml5yCMh+CSJBhuVivBJEg43JKrgQRJ1ZgMEaCFCRF
FuH6Nmt9+8IujDi5W/wLI056h0dmxMkoMRgjQRSJyiKaRGeRkqT8R2bUEjirygIAAA==" | base64 -d | zcat > CMakeLists.txt
cd ..
