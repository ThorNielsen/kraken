#include "kraken/math/constants.hpp"
#include "kraken/math/matrix.hpp"
#include <cstddef>
#include <kraken/math/algorithm.hpp>

#include <cmath>

template <size_t Rows, size_t Cols, typename Prec>
bool isEqual(const kraken::math::Matrix<Rows, Cols, Prec>& a,
             const kraken::math::Matrix<Rows, Cols, Prec>& b,
             Prec epsilon = 100*kraken::math::Constant<Prec>::epsilon)
{
    for (size_t i = 0; i < Rows*Cols; ++i)
    {
        if (std::abs(a.data()[i]-b.data()[i]) >= epsilon) return false;
    }
    return true;
}

template <typename Prec>
bool isEqual(Prec a, Prec b,
             Prec epsilon = 100*kraken::math::Constant<Prec>::epsilon)
{
    return std::abs(a-b) <= epsilon;
}

int main()
{
    int err = 0;
    namespace math = kraken::math;

    math::Matrix<3, 4, float> rank2({1,2, 1, 6,
                                     1,1,-1, 1,
                                     3,7, 5,23});
    math::Matrix<3, 4, float> expectedReduced({1,0,-3,-4,
                                               0,1, 2, 5,
                                               0,0, 0, 0});
    auto reduced = math::rowreduce(rank2);
    err += !isEqual(reduced, expectedReduced);
    reduced = math::rowreduce(math::Matrix<3,4,float>({ 1,4,1,2,
                                                       -1,3,2,2,
                                                        3,9,0,2}));
    expectedReduced = math::Matrix<3,4,float>({1,0,0,2.f/3.f,
                                               0,1,0,0,
                                               0,0,1,4.f/3.f});
    err += !isEqual(reduced, expectedReduced);
    math::Matrix<5, 5, float> largeMatrix({ 1, 2, 3, 4, 5,
                                            6, 7, 8, 9,10,
                                           11,12,13, 9,15,
                                           16,17, 1,19,20,
                                           21, 2,23,24,25});
    err += !isEqual(math::determinant(largeMatrix), 34000.f);
    auto rowSwapped = largeMatrix;
    rowSwapped.setRow(2, largeMatrix.row(1));
    rowSwapped.setRow(1, largeMatrix.row(2));
    err += !isEqual(math::determinant(rowSwapped), -34000.f);

    auto orthonormalised = math::orthonormaliseRows(largeMatrix);
    math::Matrix<5, 5, float> iproducts;
    for (size_t i = 0; i < orthonormalised.rowCount(); ++i)
    {
        for (size_t j = 0; j < orthonormalised.rowCount(); ++j)
        {
            iproducts(i, j) = math::dot(orthonormalised.row(i),
                                        orthonormalised.row(j));
        }
    }
    err += !isEqual(iproducts, math::Matrix<5, 5, float>(1.f));
    for (size_t i = 0; i < orthonormalised.colCount(); ++i)
    {
        for (size_t j = 0; j < orthonormalised.colCount(); ++j)
        {
            iproducts(i, j) = math::dot(orthonormalised.col(i),
                                        orthonormalised.col(j));
        }
    }
    err += !isEqual(iproducts, math::Matrix<5, 5, float>(1.f));
    orthonormalised = math::orthonormaliseColumns(largeMatrix);
    for (size_t i = 0; i < orthonormalised.rowCount(); ++i)
    {
        for (size_t j = 0; j < orthonormalised.rowCount(); ++j)
        {
            iproducts(i, j) = math::dot(orthonormalised.row(i),
                                        orthonormalised.row(j));
        }
    }
    err += !isEqual(iproducts, math::Matrix<5, 5, float>(1.f));
    for (size_t i = 0; i < orthonormalised.colCount(); ++i)
    {
        for (size_t j = 0; j < orthonormalised.colCount(); ++j)
        {
            iproducts(i, j) = math::dot(orthonormalised.col(i),
                                        orthonormalised.col(j));
        }
    }
    err += !isEqual(iproducts, math::Matrix<5, 5, float>(1.f));

    auto inv = math::inverse(largeMatrix);
    err += !isEqual(inv*largeMatrix, math::Matrix<5, 5, float>(1.f));
    err += !isEqual(largeMatrix*inv, math::Matrix<5, 5, float>(1.f));

    math::Matrix<6, 10, float> kerim({0,1,1,1,1, 1,1,1,1,1,
                                      0,0,2,2,2, 2,2,2,2,2,
                                      0,0,0,3,3, 3,3,3,3,3,
                                      0,0,0,3,3, 3,3,3,3,3,
                                      0,0,0,0,4, 4,4,4,4,4,
                                      0,0,0,0,0, 0,0,0,5,5});
    math::Vector<6, float> imOut[10];
    auto* imAt = math::computeImageBasis(kerim, imOut);
    err += !(imAt == &imOut[5]);
    math::Matrix<6,6,float> testLinIndep;
    for (size_t i = 0; i < 5; ++i)
    {
        testLinIndep.setCol(i, imOut[i]);
    }
    testLinIndep.setCol(5, math::Vector<6, float>({0,0,1,0,0,0}));
    err += isEqual(math::determinant(testLinIndep), 0.f);
    math::Vector<10, float> kerOut[10];
    auto* kerAt = math::computeKernelBasis(kerim, kerOut);
    err += !(kerAt == &kerOut[5]);
    for (size_t i = 0; i < 5; ++i)
    {
        err += !isEqual(kerim * kerOut[i], math::Vector<6,float>(0.f));
    }
    return err;
}
