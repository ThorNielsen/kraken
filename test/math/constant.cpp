#include <kraken/math/constants.hpp>

#include <cmath>

int main()
{
    int err = 0;
    using cf = kraken::math::Constant<float>;
    using cd = kraken::math::Constant<double>;
    using cl = kraken::math::Constant<long double>;

    err += !(1.f + cf::epsilon > 1.f);
    err += !(1. + cd::epsilon > 1.);
    err += !(1.L + cl::epsilon > 1.L);

    err += cf::maximum >= cf::infinity;
    err += cd::maximum >= cd::infinity;
    err += cl::maximum >= cl::infinity;

    err += std::abs(cf::two_pi-2*cf::pi) > 10*cf::epsilon;
    err += std::abs(cd::two_pi-2*cd::pi) > 10*cd::epsilon;
    err += std::abs(cl::two_pi-2*cl::pi) > 10*cl::epsilon;

    return err;
}
