#include "kraken/types.hpp"
#include <kraken/math/integer.hpp>

int main()
{
    int err = 0;

    // This is from the definition of ilog2.
    err += !(kraken::ilog2(0) == 0);
    err += !(kraken::ilog2(1) == 0);
    err += !(kraken::ilog2(2) == 1);
    err += !(kraken::ilog2(32) == 5);
    err += !(kraken::ilog2(200) == 7);
    err += !(kraken::gcd(200, 32) == kraken::gcd(32, 200));
    err += !(kraken::gcd(200, 32) == 8);
    err += !(kraken::gcd(0, 11) == 11);
    err += !(kraken::gcd(11, 0) == 11);
    err += !(kraken::gcd(2147483647, 715827883) == 1);
    err += !(kraken::lcm(200, 32) == 800);
    err += !(kraken::lcm(32, 200) == 800);
    err += !(kraken::lcm(13, 17) == 13*17);
    err += !(kraken::lcm(4*13, 2*17) == (2*4*13*17)/2);
    err += !kraken::isprime(2147483647);
    err +=  kraken::isprime(2147483648);
    err +=  kraken::isprime(0);
    err += !kraken::isprime(13);
    kraken::U64 p = 47417;
    err += !kraken::isprime(p);
    err +=  kraken::isprime(p*p);
    err += !(kraken::isqrt(p*p) == p);
    err += !(kraken::isqrt(2147483647) == 46340);
    for (kraken::U64 i = 0; i < 1000; ++i)
    {
        auto s = kraken::isqrt(i);
        if (!(s*s <= i && i < (s+1)*(s+1)))
        {
            err += 1;
            break;
        }
    }

    err += !(kraken::ipow(2, 29) == 536870912);
    err += !(kraken::ipow((kraken::U64)5, 21) == 476837158203125);
    err += !(kraken::modpow(3, 8, 26) == 9);
    kraken::U64 prime = 2357;
    for (kraken::U64 i = 1; i < prime; ++i)
    {
        if (kraken::modpow(i, prime-1, prime) != 1
            || kraken::modpow(i, prime, prime) != i)
        {
            err += 1;
            break;
        }
    }
    err += !(kraken::maskpow(13, 2937619432345231ull, (1<<13)-1) == 5637);

    return err;
}
