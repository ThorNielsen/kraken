#include <algorithm>
#include <cwctype>
#include <iterator>
#include <kraken/math/matrix.hpp>

#include <cmath>
#include <sstream>
#include <string>
#include <string_view>

std::string stripWhitespace(std::string_view sv)
{
    std::string s;
    std::copy_if(sv.begin(), sv.end(), std::back_inserter(s),
                 [](char c){ return !std::iswspace(c); });
    return s;
}

int testMatrixIO()
{
    namespace math = kraken::math;

    int err = 0;
    math::Matrix<3,5,int> mat({1,2,3,4,5,
                               6,7,8,9,10,
                               11,12,13,14,15});
    std::stringstream ss;
    ss << mat;

    auto formatted = ss.str();
    auto stripped = stripWhitespace(formatted);

    err += (stripped != "[[1,2,3,4,5],[6,7,8,9,10],[11,12,13,14,15]]");

    decltype(mat) verifier;
    ss = std::stringstream(formatted);
    ss >> verifier;
    err += (mat != verifier);
    err += ss.fail();

    ss = std::stringstream(formatted);
    ss >> verifier;
    err += (mat != verifier);
    err += ss.fail();

    math::Matrix<3, 3, int> floop;
    ss = std::stringstream("[   [3 ,   2,  1],   [ 5, 4, 6 ]\t,\t\n[9,7,8]\n]");
    ss >> floop;
    err += ss.fail();
    err += (floop != decltype(floop)({3,2,1,5,4,6,9,7,8}));

    return err;
}

int main()
{
    int err = 0;
    namespace math = kraken::math;
    math::Matrix<2,3,int> tester({1,2,3,
                                  4,5,6});
    err += !(tester(1,1) == 5);
    err += !(tester(1,2) == 6);
    math::Matrix<4,4,int> wider(tester, 1);
    math::Matrix<4,4,int> expected(1);
    expected(0, 1) = 2;
    expected(0, 2) = 3;
    expected(1, 0) = 4;
    expected(1, 1) = 5;
    expected(1, 2) = 6;
    err += !(wider == expected);

    err += math::prec_cast<long long>(tester).cast<int>() != tester;

    err += testMatrixIO();

    return err;
}
