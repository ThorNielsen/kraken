#include <kraken/math/matrix.hpp>

#include <cmath>

int main()
{
    int err = 0;
    namespace math = kraken::math;
    using mat2 = math::mat2;
    using math::inverse;
    mat2 A = mat2(3);
    mat2 B{0, 1,
           1, 0};
    mat2 C{ 1,  2,
           15,  6};
    mat2 D{-2,   7,
            5,   1};
    mat2 F{3,  0,
           2,  3};
    mat2 G{-16, -8,
           -36, 16};

    err += !(A*inverse(A) == mat2(1));
    err += !(inverse(A)*A == mat2(1));
    err += !(B*inverse(B) == mat2(1));
    err += !(inverse(B)*B == mat2(1));
    err += !(C*inverse(C) == mat2(1));
    err += !(inverse(C)*C == mat2(1));
    err += !(D*inverse(D) == mat2(1));
    err += !(inverse(D)*D == mat2(1));
    err += !(F*inverse(F) == mat2(1));
    err += !(inverse(F)*F == mat2(1));
    err += !(G*inverse(G) == mat2(1));
    err += !(inverse(G)*G == mat2(1));

    return err;
}
