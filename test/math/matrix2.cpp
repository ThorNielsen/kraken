#include <kraken/math/matrix.hpp>

#include <cmath>

int main()
{
    int err = 0;
    namespace math = kraken::math;
    using imat2 = math::imat2;
    using ivec2 = math::ivec2;

    imat2 A = imat2(3);
    err += !(A.col(0) == ivec2{3, 0});
    err += !(A.col(1) == ivec2{0, 3});
    auto B = A;
    B(1, 0) = 2;
    err += !(B.col(0) == ivec2{3, 2});
    err += !(B.col(1) == ivec2{0, 3});
    B.setRow(1, ivec2{9, 2});
    err += !(B.row(0) == ivec2{3, 0});
    err += !(B.row(1) == ivec2{9, 2});
    imat2 C{0, 1,
            1, 0};
    err += !(C.row(0) == ivec2{0, 1});
    err += !(C.row(1) == ivec2{1, 0});
    err += !(A+C == imat2{3, 1,
                          1, 3});
    err += !(A+B+C == imat2{ 6, 1,
                            10, 5});
    err += !(A-C == imat2{ 3, -1,
                          -1,  3});
    err += !(A-B-C == imat2{  0, -1,
                            -10,  1});
    err += !(B+(-B) == imat2(0));
    err += !((A-B)+(-(A-B)) == imat2(0));
    imat2 D{1, 2,
            3, 4};
    // Should be row-major order.
    err += !(D.data()[1] == 2);

    err += !(C*D == imat2{3, 4,
                          1, 2});
    err += !((A-B)*D == imat2{ 0,   0,
                              -6, -14});

    err += !(math::transpose(D) == imat2{1, 3,
                                         2, 4});
    err += !(math::det(D) == -2);
    err += !(math::det(A-B) == 0);
    err += !(math::det(B) == -math::det(imat2({B.col(1), B.col(0)})));
    err += !(math::det(B*D) == math::det(B)*math::det(D));
    err += !(math::abs(A-B) == imat2{0, 0,
                                     9, 1});
    return err;
}
