#include <kraken/math/matrix.hpp>

#include <cmath>

int main()
{
    int err = 0;
    namespace math = kraken::math;
    using mat3 = math::mat3;
    using math::inverse;
    mat3 A = mat3(3);
    mat3 B{0, 0, 1,
           0, 1, 0,
           1, 0, 0};
    mat3 C{ 1,  2,  3,
           15,  6,  7,
            9, 10, 19};
    mat3 D{-2,   7,  0,
            5,   1, -1,
            2, -18, -7};
    mat3 F{3,  0, -1,
           2,  3,  1,
           0,  1,  3};
    mat3 G{-16, -8, -19,
           -36, 16, -35,
           -56, 24, -51};

    err += !math::zero(A*inverse(A) - mat3(1));
    err += !math::zero(inverse(A)*A - mat3(1));
    err += !math::zero(B*inverse(B) - mat3(1));
    err += !math::zero(inverse(B)*B - mat3(1));
    err += !math::zero(C*inverse(C) - mat3(1));
    err += !math::zero(inverse(C)*C - mat3(1));
    err += !math::zero(D*inverse(D) - mat3(1));
    err += !math::zero(inverse(D)*D - mat3(1));
    err += !math::zero(F*inverse(F) - mat3(1));
    err += !math::zero(inverse(F)*F - mat3(1));
    err += !math::zero(G*inverse(G) - mat3(1));
    err += !math::zero(inverse(G)*G - mat3(1));

    return err;
}
