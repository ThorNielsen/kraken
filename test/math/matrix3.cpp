#include <kraken/math/matrix.hpp>

#include <cmath>

int main()
{
    int err = 0;
    namespace math = kraken::math;
    using imat3 = math::imat3;
    using ivec3 = math::ivec3;
    imat3 A = imat3(3);
    err += !(A.col(0) == ivec3{3, 0, 0});
    err += !(A.col(1) == ivec3{0, 3, 0});
    err += !(A.col(2) == ivec3{0, 0, 3});
    auto B = A;
    B(1, 0) = 2;
    B(0, 2) = -1;
    err += !(B.col(0) == ivec3{ 3, 2, 0});
    err += !(B.col(1) == ivec3{ 0, 3, 0});
    err += !(B.col(2) == ivec3{-1, 0, 3});
    B.setRow(2, ivec3{3, -2, 5});
    err += !(B.row(0) == ivec3{3,  0, -1});
    err += !(B.row(1) == ivec3{2,  3,  0});
    err += !(B.row(2) == ivec3{3, -2,  5});
    imat3 C{0, 0, 1,
            0, 1, 0,
            1, 0, 0};
    err += !(A+C == imat3{3, 0, 1,
                          0, 4, 0,
                          1, 0, 3});
    err += !(A+B+C == imat3{6,  0, 0,
                            2,  7, 0,
                            4, -2, 8});
    err += !(A-C == imat3{ 3, 0, -1,
                           0, 2,  0,
                          -1, 0,  3});
    err += !(A-B-C == imat3{ 0,  0,  0,
                            -2, -1,  0,
                            -4,  2, -2});
    err += !(B+(-B) == imat3(0));
    err += !((A-B)+(-(A-B)) == imat3(0));
    imat3 D{1, 2, 3,
            4, 5, 6,
            7, 8, 9};
    // Should be row-major order.
    err += !(D.data()[1] == 2);

    err += !(C*D == imat3{7, 8, 9,
                          4, 5, 6,
                          1, 2, 3});
    err += !(D*(A-B) == imat3{-13,  6,  -5,
                              -28, 12,  -8,
                              -43, 18, -11});

    err += !(math::transpose(D) == imat3{1, 4, 7,
                                         2, 5, 8,
                                         3, 6, 9});
    err += !(math::det(D) == 0);
    err += !(math::det(A-B) == -4);
    err += !(math::det(B) == -math::det(imat3({B.col(1), B.col(0), B.col(2)})));
    err += !(math::det(B) ==  math::det(imat3({B.col(1), B.col(2), B.col(0)})));
    err += !(math::det(B*(A-B)) == math::det(B)*math::det(A-B));
    err += !(math::abs(A-B) == imat3{0, 0, 1,
                                     2, 0, 0,
                                     3, 2, 2});
    return err;
}
