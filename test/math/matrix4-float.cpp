#include <kraken/math/matrix.hpp>

#include <cmath>

int main()
{
    int err = 0;
    namespace math = kraken::math;
    using mat4 = math::mat4;
    using math::inverse;
    mat4 A = mat4(3);
    mat4 B{0, 0, 0, 1,
           0, 0, 1, 0,
           0, 1, 0, 0,
           1, 0, 0, 0};
    mat4 C{ 1,  2,  3,  4,
           15,  6,  7,  8,
            9, 10, 19, 12,
           13, 14, 15, 16};
    mat4 D{-2,   7,  0, 3,
            5,   1, -1, 0,
            2, -18, -7, 4,
            1,   6,  2, 6};
    mat4 F{3,  0, -1, 1,
           2,  3,  1, 0,
           0,  1,  3, 0,
           4, -2,  5, 1};
    mat4 G{-16, -8, -19,   8,
           -36, 16, -35, -16,
           -56, 24, -51,  24,
           -76, 32, -67,  32};

    err += !math::zero(A*inverse(A) - mat4(1));
    err += !math::zero(inverse(A)*A - mat4(1));
    err += !math::zero(B*inverse(B) - mat4(1));
    err += !math::zero(inverse(B)*B - mat4(1));
    err += !math::zero(C*inverse(C) - mat4(1));
    err += !math::zero(inverse(C)*C - mat4(1));
    err += !math::zero(D*inverse(D) - mat4(1));
    err += !math::zero(inverse(D)*D - mat4(1));
    err += !math::zero(F*inverse(F) - mat4(1));
    err += !math::zero(inverse(F)*F - mat4(1));
    err += !math::zero(G*inverse(G) - mat4(1));
    err += !math::zero(inverse(G)*G - mat4(1), 0.000005f); // Numerically unstable

    return err;
}
