#include <kraken/math/matrix.hpp>

#include <cmath>

int main()
{
    int err = 0;
    namespace math = kraken::math;
    using imat4 = math::imat4;
    using ivec4 = math::ivec4;
    imat4 A = imat4(3);
    err += !(A.col(0) == ivec4{3, 0, 0, 0});
    err += !(A.col(1) == ivec4{0, 3, 0, 0});
    err += !(A.col(2) == ivec4{0, 0, 3, 0});
    err += !(A.col(3) == ivec4{0, 0, 0, 3});
    auto B = A;
    B(1, 0) = 2;
    B(0, 2) = -1;
    B(3, 1) = 8;
    err += !(B.col(0) == ivec4{ 3, 2, 0, 0});
    err += !(B.col(1) == ivec4{ 0, 3, 0, 8});
    err += !(B.col(2) == ivec4{-1, 0, 3, 0});
    err += !(B.col(3) == ivec4{ 0, 0, 0, 3});
    B.setRow(3, ivec4{3, -2, 5, 1});
    err += !(B.row(0) == ivec4{3,  0, -1, 0});
    err += !(B.row(1) == ivec4{2,  3,  0, 0});
    err += !(B.row(2) == ivec4{0,  0,  3, 0});
    err += !(B.row(3) == ivec4{3, -2,  5, 1});
    imat4 C{0, 0, 0, 1,
            0, 0, 1, 0,
            0, 1, 0, 0,
            1, 0, 0, 0};
    err += !(A+C == imat4{3, 0, 0, 1,
                          0, 3, 1, 0,
                          0, 1, 3, 0,
                          1, 0, 0, 3});
    err += !(B+C == imat4{3,  0, -1, 1,
                          2,  3,  1, 0,
                          0,  1,  3, 0,
                          4, -2,  5, 1});
    err += !(A-C == imat4{ 3,  0,  0, -1,
                           0,  3, -1,  0,
                           0, -1,  3,  0,
                          -1,  0,  0,  3});
    err += !(A-B-C == imat4{ 0,  0,  1, -1,
                            -2,  0, -1,  0,
                             0, -1,  0,  0,
                            -4,  2, -5,  2});
    err += !(B+(-B) == imat4(0));
    err += !((A-B)+(-(A-B)) == imat4(0));
    imat4 D{ 1,  2,  3,  4,
             5,  6,  7,  8,
             9, 10, 11, 12,
            13, 14, 15, 16};
    // Should be row-major order.
    err += !(D.data()[1] == 2);

    err += !(C*D == imat4{13, 14, 15, 16,
                           9, 10, 11, 12,
                           5,  6,  7,  8,
                           1,  2,  3,  4,});
    err += !(D*(A-B) == imat4{-16,  8, -19,  8,
                              -36, 16, -35, 16,
                              -56, 24, -51, 24,
                              -76, 32, -67, 32});

    err += !(math::transpose(D) == imat4{1, 5,  9, 13,
                                         2, 6, 10, 14,
                                         3, 7, 11, 15,
                                         4, 8, 12, 16});
    err += !(math::det(D) == 0);
    err += !(math::det(B) == 27);
    err += !(math::det(A) == 81);
    err += !(math::det(A+B) == 864);
    err += !(math::det(A)*math::det(B) == math::det(A*B));
    err += !(math::det(A*B) == math::det(B*A));
    err += !(math::det(B*(A+B)) == math::det(B)*math::det(A+B));
    err += !(math::det(B) == -math::det(imat4({B.col(1), B.col(0), B.col(2), B.col(3)})));
    err += !(math::det(B) ==  math::det(imat4({B.col(1), B.col(2), B.col(0), B.col(3)})));
    err += !(math::abs(A-B-C) == imat4{0, 0, 1, 1,
                                       2, 0, 1, 0,
                                       0, 1, 0, 0,
                                       4, 2, 5, 2});
    return err;
}
