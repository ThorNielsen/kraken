#include <kraken/math/matrix.hpp>

#include <cmath>

int main()
{
    int err = 0;
    namespace math = kraken::math;
    using imat2 = math::imat2;
    using ivec2 = math::ivec2;
    imat2 A = imat2(3);
    imat2 B{0, 1,
            1, 0};
    imat2 C{1, 2,
            3, 4};
    imat2 D{-2, 7,
             5, 1};
    ivec2 v{1,-3};
    err += !(A*v == 3*v);
    err += !(B*v == ivec2{-3, 1});
    err += !(C*v == ivec2{-5, -9});
    err += !(B*(D*v) == (B*D)*v);
    err += !(B*D*v == ivec2{2, -23});
    err += !(C*v == ivec2{-5, -9});
    err += !(C*D*v == ivec2{-19, -61});
    err += !(C*D*v != D*C*v);
    err += !((C*D)*v == C*(D*v));
    ivec2 n{0,0};
    err += !(A*n == n);
    err += !(B*n == n);
    err += !(C*n == n);
    err += !(D*n == n);
    return err;
}
