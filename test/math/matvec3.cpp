#include <kraken/math/matrix.hpp>

#include <cmath>

int main()
{
    int err = 0;
    namespace math = kraken::math;
    using imat3 = math::imat3;
    using ivec3 = math::ivec3;
    imat3 A = imat3(3);
    imat3 B{0, 0, 1,
            0, 1, 0,
            1, 0, 0};
    imat3 C{1, 2, 3,
            4, 5, 6,
            7, 8, 9};
    imat3 D{-2,   7,  0,
             5,   1, -1,
             2, -18, -7};
    ivec3 v{1,2,-3};
    err += !(A*v == 3*v);
    err += !(B*v == ivec3{-3, 2, 1});
    err += !(C*v == ivec3{-4, -4, -4});
    err += !(B*(D*v) == (B*D)*v);
    err += !(B*D*v == ivec3{-13, 10, 12});
    err += !(D*v == ivec3{12, 10, -13});
    err += !(C*D*v == ivec3{-7, 20, 47});
    err += !(C*D*v != D*C*v);
    err += !((C*D)*v == C*(D*v));
    ivec3 n{0,0,0};
    err += !(A*n == n);
    err += !(B*n == n);
    err += !(C*n == n);
    err += !(D*n == n);
    return err;
}
