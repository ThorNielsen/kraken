#include <kraken/math/matrix.hpp>

#include <cmath>

int main()
{
    int err = 0;
    namespace math = kraken::math;
    using imat4 = math::imat4;
    using ivec4 = math::ivec4;
    imat4 A = imat4(3);
    imat4 B{0, 0, 0, 1,
            0, 0, 1, 0,
            0, 1, 0, 0,
            1, 0, 0, 0};
    imat4 C{ 1,  2,  3,  4,
             5,  6,  7,  8,
             9, 10, 11, 12,
            13, 14, 15, 16};
    imat4 D{-2,   7,  0, 3,
             5,   1, -1, 0,
             2, -18, -7, 4,
             1,   6,  2, 6};
    ivec4 v{1,2,3,-4};
    err += !(A*v == 3*v);
    err += !(B*v == ivec4{-4, 3, 2, 1});
    err += !(C*v == ivec4{-2, 6, 14, 22});
    err += !(B*(C*v) == (B*C)*v);
    err += !(B*C*v == ivec4{22, 14, 6, -2});
    err += !(D*v == ivec4{0, 4, -71, -5});
    err += !(C*D*v != D*C*v);
    err += !((C*D)*v == C*(D*v));
    ivec4 n{0,0,0,0};
    err += !(A*n == n);
    err += !(B*n == n);
    err += !(C*n == n);
    err += !(D*n == n);
    return err;
}
