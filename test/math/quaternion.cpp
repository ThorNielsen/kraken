#include "kraken/math/comparison.hpp"
#include "kraken/math/constants.hpp"
#include "kraken/math/matrix.hpp"

#include <cmath>

namespace math = kraken::math;
using iquat = math::Quaternion<int>;

int testIntQuat()
{
    int err = 0;
    iquat iq1{-5, 3, -2, 9};
    iquat iq2{7, 2, -8, 3};
    err += !(iq1.x == -5);
    err += !(iq1.y == 3);
    err += !(iq1.z == -2);
    err += !(iq1.w == 9);
    err += !(iq1[1] == 3);
    err += !(iq2[0] == 7);
    err += !(iq2[2] == -8);
    err += !(iq1[3] == 9);

    err += iq1.cast<long>().cast<int>() != iq1;

    const auto& qRef = iq2;

    err += !(iq2[0] == qRef[0]);
    err += !(iq2[1] == qRef[1]);
    err += !(iq2[2] == qRef[2]);
    err += !(iq2[3] == qRef[3]);
    err += !(iq1(0) == iq1[0]);
    err += !(iq1(1) == iq1[1]);
    err += !(iq1(2) == iq1[2]);
    err += !(iq1(3) == iq1[3]);

    err += !(iq1+iq2 == iquat{2, 5, -10, 12});
    err += !(iq1-iq2 == iquat{-12, 1, 6, 6});
    err += !(iq1*3 == iquat{-15, 9, -6, 27});
    err += !(iq1/3 == iquat{-1, 1, 0, 3});

    err += !(math::dot(iq1, qRef) == 14);
    err += !(math::abs(iq1) == iquat{5, 3, 2, 9});
    err += !(math::squaredLength(iq1) == 119);

    err += !(iq1*iq2 == iquat{28, -27, -109, 40});
    err += !(iq2*iq1 == iquat{68, 81, -47, 40});

    err += !(iq1-iq2 == -(iq2-iq1));

    err += !(iq1 == +iq1);

    err += !(iq1.real() == 9);
    err += !(iq1.imag() == math::ivec3{-5, 3, -2});
    err += !(iq1.conjugate() == iquat{5, -3, 2, 9});
    err += !(iq1.real() == math::real(iq1));
    err += !(iq1.imag() == math::imag(iq1));
    err += !(iq1.conjugate() == math::conjugate(iq1));

    err += !(iq1 == iquat{iq1.real(), iq1.imag()});

    err += !(iq1 != iq2);

    iq1.setCol(0, math::ivec4{1,2,3,4});
    err += !(iq1 == iquat{1,2,3,4});

    iq1.setRow(2, 9);
    err += !(iq1 == iquat{1,2,9,4});

    iq1.setRow(0, -17);
    err += !(iq1 == iquat{-17,2,9,4});

    iq1.setRow(3, 12);
    err += !(iq1 == iquat{-17,2,9,12});
    return err;
}

int testFloatQuat()
{
    using math::quat;

    quat q1{.7, .1, .2, .3};
    auto q2 = quat::rotation(math::normalise(math::vec3{1., 1., 0.}),
                             math::Constant<float>::half_pi);

    math::vec3 toRotate{-1.61126f, 2.00924f, 1.31844f};
    math::vec3 expectedRotated{1.13127f, -0.733288f, 2.56008f};

    int err = 0;

    err += !math::flt_releq(math::length(math::normalise(q1)), 1.f);
    err += !math::flt_releq(math::length(math::normalise(q2)), 1.f);

    err += math::length(q2.rotateVector(toRotate) - expectedRotated) > 1e-5;

    return err;
}

int main()
{
    int err = 0;

    err += testIntQuat();

    err += testFloatQuat();

    return err;
}
