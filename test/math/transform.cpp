#include <cstddef>
#include <cstdlib>
#include <kraken/math/constants.hpp>
#include <kraken/math/matrix.hpp>
#include <kraken/math/transform.hpp>

namespace math = kraken::math;

template <size_t N>
bool almostIdentical(math::Vector<N, float> a, math::Vector<N, float> b)
{
    auto diff = a-b;
    return math::dot(diff, diff) < 1e-7f;
}

int main()
{
    int err = 0;
    auto mat = math::rotation(math::normalise(math::vec3{1.f, 2.f, 3.f}), 0.f);
    math::vec3 a{0.2f, 0.6f, -2.3f};
    math::vec3 b{1.f, 2.234f, 0.003f};
    math::vec3 c{-5.2f, 0.96f, -2.2f};
    err += !almostIdentical(mat*a, a);
    err += !almostIdentical(mat*b, b);
    err += !almostIdentical(mat*c, c);
    mat = math::rotation(math::normalise(math::vec3{1.f, 2.f, 3.f}),
                         0.3534f);
    err += !almostIdentical(mat*math::cross(a, b), math::cross(mat*a, mat*b));
    err += !almostIdentical(mat*math::cross(a, c), math::cross(mat*a, mat*c));
    err += !almostIdentical(mat*math::cross(b, c), math::cross(mat*b, mat*c));
    mat = math::rotation(math::normalise(math::vec3{1.f, 2.f, 3.f}),
                         -2.4321643f);
    err += !(std::abs(math::dot(a,b)-math::dot(mat*a, mat*b)) <= 2e-6f);
    err += !(std::abs(math::dot(a,c)-math::dot(mat*a, mat*c)) <= 2e-6f);
    err += !(std::abs(math::dot(b,c)-math::dot(mat*b, mat*c)) <= 2e-6f);

    mat = math::rotation(math::normalise(math::cross(a, b)), math::Constant<float>::half_pi);
    err += !(std::abs(math::dot(mat*a, a)) <= 2e-6f);
    err += !(std::abs(math::dot(mat*b, b)) <= 2e-6f);
    err += !(std::abs(math::dot(mat*c, c)) >  2e-6f);
    mat = math::rotation(math::normalise(math::cross(a, c)), math::Constant<float>::half_pi);
    err += !(std::abs(math::dot(mat*a, a)) <= 2e-6f);
    err += !(std::abs(math::dot(mat*b, b)) >  2e-6f);
    err += !(std::abs(math::dot(mat*c, c)) <= 2e-6f);
    return err;
}
