#include "kraken/math/matrix.hpp"
#include <kraken/math/comparison.hpp>
#include <kraken/math/constants.hpp>

#include <cmath>

int main()
{
    int err = 0;
    namespace math = kraken::math;
    using vec2 = math::vec2;
    using fconst = math::Constant<float>;
    vec2 a{0.7f, -2.3f};
    vec2 b{-8.23f, 0.22f};
    vec2 c{0.54f, 0.16f};
    err += !(math::flt_eq(dot(a, b), -6.267f, 10*fconst::epsilon));
    err +=  math::zero(a);
    err +=  math::zero(b);
    err +=  math::zero(c);
    err += !(math::zero(-0.076463382157123838f*a-0.072117177097203736f*b-c));
    err += !(math::flt_eq(math::length(a), 2.404163056f, 10*fconst::epsilon));
    err += !(math::parallel(a, math::normalise(a)));
    err += !(math::flt_eq(math::length(math::normalise(a)), 1.f, 10*fconst::epsilon));
    err += !(math::parallel(math::project(a, b), b));
    err += !(math::flt_eq(math::length(b), 8.232939937f, 10*fconst::epsilon));

    return err;
}
