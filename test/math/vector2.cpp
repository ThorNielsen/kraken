#include "kraken/math/matrix.hpp"

#include <cmath>

int main()
{
    int err = 0;
    namespace math = kraken::math;
    using ivec2 = math::ivec2;
    ivec2 iv1{-5, 3};
    ivec2 iv2{7, 2};

    err += iv1 != ivec2(-5, 3);

    err += iv1.cast<long>().cast<int>() != iv1;

    err += !(iv1[1] == 3);
    err += !(iv2[0] == 7);
    const auto& vRef = iv2;
    err += !(iv2[0] == vRef[0]);
    err += !(iv2[1] == vRef[1]);
    err += !(iv1(0) == iv1[0]);
    err += !(iv1(1) == iv1[1]);
    err += !(iv1 + (-iv1) == ivec2{0, 0});

    err += !(iv1+iv2 == ivec2{2, 5});
    err += !(iv1-iv2 == ivec2{-12, 1});
    err += !(iv1*3 == ivec2{-15, 9});
    err += !(iv1/3 == ivec2{-1, 1});

    err += !(math::dot(iv1, vRef) == -29);
    err += !(math::dot(math::perp(iv1), iv1) == 0);
    err += !(math::dot(vRef, math::perp(iv2)) == 0);
    err += math::parallel(iv1, iv2);
    err += !math::parallel(math::perp(math::perp(iv1)), iv1);
    err += !math::parallel(iv2, math::perp(math::perp(vRef)));
    err += !(math::abs(iv1) == ivec2{5, 3});
    err += !(math::squaredLength(iv1) == 34);

    return err;
}
