#include "kraken/math/matrix.hpp"
#include <kraken/math/comparison.hpp>
#include <kraken/math/constants.hpp>

#include <cmath>

int main()
{
    int err = 0;
    namespace math = kraken::math;
    using vec3 = math::vec3;
    using fconst = math::Constant<float>;
    vec3 a{0.7f, -2.3f, 2.6f};
    vec3 b{-8.23f, 0.22f, 0.1f};
    vec3 c{0.54f, 0.16f, 5.f};
    vec3 d{1.45f, 0.002f, -2.f};
    err += !(math::flt_eq(dot(a, b), -6.007f, 10*fconst::epsilon));
    err +=  math::zero(a);
    err +=  math::zero(b);
    err +=  math::zero(c);
    err += !(math::zero(-0.046299068495572485f*a-b*0.20452001862845154f-0.37183408400973328f*c-d));
    err += !(math::flt_eq(math::length(a), 3.541186242f, 10*fconst::epsilon));
    err += !(math::parallel(a, math::normalise(a)));
    err += !(math::flt_eq(math::length(math::normalise(a)), 1.f, 10*fconst::epsilon));
    err += !(math::parallel(math::project(a, b), b));
    err += !(math::flt_eq(math::length(b), 8.233547231f, 10*fconst::epsilon));

    return err;
}
