#include "kraken/math/matrix.hpp"

#include <cmath>

int main()
{
    int err = 0;
    namespace math = kraken::math;
    using kraken::math::ivec2;
    using kraken::math::ivec3;
    using kraken::math::ivec4;

    ivec3 iv1{-5, 3, -2};

    err += ivec3(-5, 3, -2) != iv1;
    err += ivec3(ivec4(iv1)) != iv1;
    err += ivec3(ivec2(iv1)) != ivec3(-5, 3, 0);
    err += ivec3(ivec2(iv1), 94) != ivec3(-5, 3, 94);

    err += iv1.cast<long>().cast<int>() != iv1;

    ivec3 iv2{7, 2, -8};

    err += !(iv1[1] == 3);
    err += !(iv2[0] == 7);
    err += !(iv2[2] == -8);

    const auto& vRef = iv2;
    err += !(iv2[0] == vRef[0]);
    err += !(iv2[1] == vRef[1]);
    err += !(iv2[2] == vRef[2]);
    err += !(iv1(0) == iv1[0]);
    err += !(iv1(1) == iv1[1]);
    err += !(iv1(2) == iv1[2]);

    err += !(iv1+iv2 == ivec3{2, 5, -10});
    err += !(iv1-iv2 == ivec3{-12, 1, 6});
    err += !(iv1*3 == ivec3{-15, 9, -6});
    err += !(iv1/3 == ivec3{-1, 1, 0});

    err += !(math::dot(iv1, vRef) == -13);
    err += !(math::dot(math::cross(iv1, iv2), iv1) == 0);
    err += !(math::dot(vRef, math::cross(iv1, iv2)) == 0);
    err += math::parallel(iv1, iv2);
    err += !(math::abs(iv1) == ivec3{5, 3, 2});
    err += !(math::squaredLength(iv1) == 38);

    err += !(math::cross(iv1, iv2) == ivec3{-20, -54, -31});

    return err;
}
