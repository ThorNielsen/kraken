#include "kraken/math/matrix.hpp"
#include <kraken/math/comparison.hpp>
#include <kraken/math/constants.hpp>

#include <cmath>

int main()
{
    int err = 0;
    namespace math = kraken::math;
    using vec4 = math::vec4;
    using fconst = math::Constant<float>;
    vec4 a{0.7f, -2.3f, 2.6f, -2.2f};
    vec4 b{-8.23f, 0.22f, 0.1f, -3.f};
    vec4 c{0.54f, 0.16f, 5.f, 1.645f};
    vec4 d{1.45f, 0.002f, -2.f, -0.371f};
    vec4 e{1.7f, 1.22f, 15.f, -2.119f};
    err += !(math::flt_eq(dot(a, b), 0.593f, 10*fconst::epsilon));
    err +=  math::zero(a);
    err +=  math::zero(b);
    err +=  math::zero(c);
    err += !(math::zero(0.475883480271f*a+b*3.45845000427f+9.49751431293f*c+17.0353568069f*d-e));
    err += !(math::flt_eq(math::length(a), 4.168932717135166f, 10*fconst::epsilon));
    err += !(math::zero(dot(a-math::project(a, b), b)));
    err += !(math::flt_eq(math::length(math::normalise(a)), 1.f, 10*fconst::epsilon));
    err += !(math::flt_eq(math::length(b), 8.763064532456669f, 10*fconst::epsilon));
    err += !(math::zero(dot(a-math::project(a, c), c)));
    err += !(math::zero(dot(b-math::project(b, c), c)));
    err += !(math::zero(dot(b-math::project(b, d), d)));

    return err;
}
