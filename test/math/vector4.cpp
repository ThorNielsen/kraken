#include "kraken/math/matrix.hpp"

#include <cmath>

int main()
{
    int err = 0;
    namespace math = kraken::math;
    using kraken::math::ivec2;
    using kraken::math::ivec3;
    using kraken::math::ivec4;

    ivec4 iv0(-5, 3, -2, 9);
    ivec4 iv0_3(ivec3(-5, 3, -2), 9);
    ivec4 iv0_2(ivec2(-5, 3), -2, 9);
    err += iv0 != iv0_3;
    err += iv0 != iv0_2;

    err += ivec4(ivec3(iv0)) != ivec4(-5, 3, -2, 0);
    err += ivec4(ivec3(iv0), 666) != ivec4(-5, 3, -2, 666);
    err += ivec4(ivec2(iv0)) != ivec4(-5, 3, 0, 0);
    err += ivec4(ivec2(iv0), 15) != ivec4(-5, 3, 15, 0);
    err += ivec4(ivec2(iv0), 31, 19) != ivec4(-5, 3, 31, 19);

    ivec4 iv1{-5, 3, -2, 9};
    err += iv1.cast<long>().cast<int>() != iv1;

    err += iv0 != iv1;

    ivec4 iv2{7, 2, -8, 3};
    err += !(iv1[1] == 3);
    err += !(iv2[0] == 7);
    err += !(iv2[2] == -8);
    err += !(iv1[3] == 9);
    const auto& vRef = iv2;
    err += !(iv2[0] == vRef[0]);
    err += !(iv2[1] == vRef[1]);
    err += !(iv2[2] == vRef[2]);
    err += !(iv2[3] == vRef[3]);
    err += !(iv1(0) == iv1[0]);
    err += !(iv1(1) == iv1[1]);
    err += !(iv1(2) == iv1[2]);
    err += !(iv1(3) == iv1[3]);

    err += !(iv1+iv2 == ivec4{2, 5, -10, 12});
    err += !(iv1-iv2 == ivec4{-12, 1, 6, 6});
    err += !(iv1*3 == ivec4{-15, 9, -6, 27});
    err += !(iv1/3 == ivec4{-1, 1, 0, 3});

    err += !(math::dot(iv1, vRef) == 14);
    err += !(math::abs(iv1) == ivec4{5, 3, 2, 9});
    err += !(math::squaredLength(iv1) == 119);

    return err;
}
