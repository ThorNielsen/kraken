#include <kraken/utility/base64.hpp>

int testDecoding()
{
    int err = 0;

    err += !kraken::base64Decode("").empty();
    err += !(kraken::base64Decode("Zg==") == "f");
    err += !(kraken::base64Decode("Zm8=") == "fo");
    err += !(kraken::base64Decode("Zm9v") == "foo");
    err += !(kraken::base64Decode("Zm9vYg==") == "foob");
    err += !(kraken::base64Decode("Zm9vYmE=") == "fooba");
    err += !(kraken::base64Decode("Zm9vYmFy") == "foobar");

    err += !(kraken::base64Decode("aw==") == "k");
    err += !(kraken::base64Decode("a3I=") == "kr");
    err += !(kraken::base64Decode("a3Jh") == "kra");
    err += !(kraken::base64Decode("a3Jhaw==") == "krak");
    err += !(kraken::base64Decode("a3Jha2U=") == "krake");
    err += !(kraken::base64Decode("a3Jha2Vu") == "kraken");

    return err;
}

int testEncoding()
{
    int err = 0;

    err += !(kraken::base64Encode("").empty());
    err += !(kraken::base64Encode("f") == "Zg==");
    err += !(kraken::base64Encode("fo") == "Zm8=");
    err += !(kraken::base64Encode("foo") == "Zm9v");
    err += !(kraken::base64Encode("foob") == "Zm9vYg==");
    err += !(kraken::base64Encode("fooba") == "Zm9vYmE=");
    err += !(kraken::base64Encode("foobar") == "Zm9vYmFy");

    err += !(kraken::base64Encode("k") == "aw==");
    err += !(kraken::base64Encode("kr") == "a3I=");
    err += !(kraken::base64Encode("kra") == "a3Jh");
    err += !(kraken::base64Encode("krak") == "a3Jhaw==");
    err += !(kraken::base64Encode("krake") == "a3Jha2U=");
    err += !(kraken::base64Encode("kraken") == "a3Jha2Vu");

    return err;
}

int testPrivInvalid()
{
    auto v = kraken::priv::fromBase64Value('?');
    return !(v > 64);
}

int main()
{
    int err = 0;
    err += testDecoding();
    err += testEncoding();
    err += testPrivInvalid();
    return err;
}
