#include <kraken/utility/checksum.hpp>
#include <kraken/types.hpp>

int main()
{
    constexpr static kraken::U8 binData[] =
    {
        0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x0D,
        0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x08, 0x00,
        0x08, 0x02, 0x00, 0x00, 0x00, 0x3D, 0xC5, 0x44, 0x67
    };
    constexpr const kraken::U8* binDataEnd = binData+sizeof(binData);
    int err = 0;

    kraken::CRC32sum crc32;
    err += !(crc32.sum() == 0x00000000);
    crc32.append("abc", 3);
    err += !(crc32.sum() == 0x352441c2);
    crc32.append("def", 3);
    err += !(crc32.sum() == 0x4b8e39ef);
    crc32.clear();
    err += !(crc32.sum() == 0x00000000);
    crc32.append("abcdef", 4);
    err += !(crc32.sum() == 0xed82cd11);
    crc32.clear();
    crc32.append(binData, binDataEnd);
    err += !(crc32.sum() == 0x2939aa2f);
    crc32.clear();
    crc32.append(binData+12, 0x0d+4);
    err += !(crc32.sum() == 0x3dc54467);

    kraken::ADLER32sum adler32;
    err += !(adler32.sum() == 0x00000001);
    adler32.append("abc", 3);
    err += !(adler32.sum() == 0x024d0127);
    adler32.append("def", 3);
    err += !(adler32.sum() == 0x081e0256);
    adler32.clear();
    err += !(adler32.sum() == 0x00000001);
    adler32.append("abcdef", 4);
    err += !(adler32.sum() == 0x03d8018b);
    adler32.clear();
    adler32.append(binData, binDataEnd);
    err += !(adler32.sum() == 0x50c604a5);
    adler32.clear();
    adler32.append(binData+12, 0x0d+4);
    err += !(adler32.sum() == 0x12a20142);

    kraken::SHA256sum sha256;
    err += !(sha256.sum() == "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
    sha256.append("abc", 3);
    err += !(sha256.sum() == "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad");
    sha256.append("def", 3);
    err += !(sha256.sum() == "bef57ec7f53a6d40beb640a780a639c83bc29ac8a9816f1fc6c5c6dcd93c4721");
    sha256.clear();
    err += !(sha256.sum() == "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
    sha256.append("abcdef", 6);
    err += !(sha256.sum() == "bef57ec7f53a6d40beb640a780a639c83bc29ac8a9816f1fc6c5c6dcd93c4721");

    sha256.clear();
    sha256.append(binData, binDataEnd);
    err += !(sha256.sum() == "37a999fde808a350dc2789b632c7866030a02c0cfb1530e5bf596c9e1380e77f");
    sha256.append(binData, binDataEnd);
    err += !(sha256.sum() == "2ed7c228d61d41afb948bf430346bbf8267f748ecac013b48603fb005d68addf");

    sha256.clear();
    sha256.append("0123456789012345678901234567890123456789012345678901234",
                  55);
    err += !(sha256.sum() == "f34d5a0f80c0cbf84c8c0b90218c22637abd199965249da736a20143c8c9c9d9");
    sha256.append("5", 1);
    err += !(sha256.sum() == "83aa034bda83e458a0dc9cbce0d4e354716aa0ff770ed37ac0ed2b292052e4af");
    sha256.append("6", 1);
    err += !(sha256.sum() == "2f599f9dd7fac80e26892a008adcf28c0c9aaa11cf3df4ceacd07d0f1aef4c76");
    sha256.append("7890123", 7);
    err += !(sha256.sum() == "9674d9e078535b7cec43284387a6ee39956188e735a85452b0050b55341cda56");
    sha256.append("4", 1);
    err += !(sha256.sum() == "52774b57c10e45040a61c14d35c1c8ebefe880082313aa0a21ebb077734cd067");

    kraken::SHA512sum sha512;
    err += !(sha512.sum() == "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e");
    sha512.append("abc", 3);
    err += !(sha512.sum() == "ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a2192992a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f");
    sha512.append("def", 3);
    err += !(sha512.sum() == "e32ef19623e8ed9d267f657a81944b3d07adbb768518068e88435745564e8d4150a0a703be2a7d88b61e3d390c2bb97e2d4c311fdc69d6b1267f05f59aa920e7");
    sha512.clear();
    err += !(sha512.sum() == "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e");
    sha512.append("abcdef", 6);
    err += !(sha512.sum() == "e32ef19623e8ed9d267f657a81944b3d07adbb768518068e88435745564e8d4150a0a703be2a7d88b61e3d390c2bb97e2d4c311fdc69d6b1267f05f59aa920e7");

    sha512.clear();
    sha512.append(binData, binDataEnd);
    err += !(sha512.sum() == "cef02e8c7d977ceea8e6ca4ddf938ec9c0e361b1ce7fcd65dd461bb63516be3de71e9f21a50d6d7ca384a5f0d90f56f619e0c81bf5a88f0484a52ba8a1b26743");
    sha512.append(binData, binDataEnd);
    err += !(sha512.sum() == "9aeaf8a0382d7f1428fc76280a3f3811cdbf11b52fd4658f00090ef6e64b76bcb1051f14eae2f273d96bacdbe23259587614741134de438b40bea4dd1f4ad5bd");


    sha512.clear();
    sha512.append("012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890",
                  111);
    err += !(sha512.sum() == "b6545e7db9b73da646208708ae7147ccfd49b39177a1de372eae64ba3332397255c8ab6ce198d379ce1ba90074f02657285fb533a86ec09746cfed6ee4986c02");
    sha512.append("1", 1);
    err += !(sha512.sum() == "d7530de979582347e64f484898f172beb2de592abc93b08ab22f38bc0caf6a3f045d330f8b91555fa955aef07b9ce3435b0b3ecf4af5590eabd4d503d936c588");
    sha512.append("2", 1);
    err += !(sha512.sum() == "c898b9464e508250cac2f9e8e6ab70ca7518f4ff2d40fae7bd4643cd389a88d3dd8d05bb305adae5f4010b47cdae640e56d563f472d5e2797cd6f48f71230b56");
    sha512.append("345678901234567", 15);
    err += !(sha512.sum() == "b6cb700ed6e0eaf4b179b590349afc8e96804164b5efc36af3ee8a35f4b863178e9a089bdd10453330a56ba77b7d83419db048e294be23373208eff448e6c2da");
    sha512.append("8", 1);
    err += !(sha512.sum() == "2a02b63d5dc7623f57e18751f9bc776629120e12b10070c54a0a59fb9a574f6347c91813f16f6673e4d730d1977da180395c2e842cbc0c46732a65dbe8f69c4f");
    return err;
}
