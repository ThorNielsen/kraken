#include "kraken/types.hpp"
#include <cstddef>
#include <stdexcept>
#define KRAKEN_UTILITY_REDBLACK_INTERNAL_TESTING
#include <kraken/utility/containers.hpp>

#include <random>
#include <vector>

int testPriorityQueue()
{
    int err = 0;
    kraken::PriorityQueue<int> lq;
    lq.push(2);
    err += lq.empty();
    lq.push(5);
    lq.push(3);
    err += !(lq.top() == 2);
    lq.pop();
    err += lq.empty();
    err += !(lq.top() == 3);
    lq.pop();
    err += !(lq.top() == 5);
    err += lq.empty();
    lq.pop();
    err += !lq.empty();

    auto compare = [](int& a, int& b)
    {
        if (a % 2 == b % 2) return a < b;
        return a % 2 == 0;
    };
    kraken::PriorityQueue<int, decltype(compare)> cq(compare);
    for (int i = 0; i < 10; ++i)
    {
        cq.push(i);
    }
    constexpr int expected[10] = {0, 2, 4, 6, 8, 1, 3, 5, 7, 9};
    for (size_t i = 0; i < 10; ++i)
    {
        err += !(cq.top() == expected[i]);
        err += cq.empty();
        cq.pop();
    }
    err += !cq.empty();
    return err;
}

int testRedBlackTree()
{
    int err = 0;
    kraken::RedBlackTree<int> rbt;
    err += !rbt.empty();
    rbt.insert(5);
    err += rbt.empty();
    rbt.insert(3);
    err += !(rbt.get(rbt.minimum())->data == 3);
    err += !(rbt.get(rbt.maximum())->data == 5);
    return err;
}

std::vector<int> randseq(std::mt19937& gen, int min, int max, size_t num)
{
    std::uniform_int_distribution<int> dist(min, max);
    std::vector<int> nums;
    while (num)
    {
        nums.push_back(dist(gen));
        --num;
    }
    return nums;
}

int randomisedTestRedBlackTree(size_t maxTrees = 20,
                               kraken::U64 seed = 0xdeadbeef)
{
    std::mt19937 gen(seed);
    for (size_t i = 0; i < maxTrees; ++i)
    {
        kraken::RedBlackTree<int> rbt;
        auto minVal = -randseq(gen, 0, 1000, 1)[0];
        auto maxVal =  randseq(gen, 0, 10000, 1)[0];
        auto treeSize = randseq(gen, 200, 2500, 1)[0];
        auto nums = randseq(gen, minVal, maxVal, treeSize);
        for (auto n : nums)
        {
            rbt.insert(n);
            try
            {
                rbt.performConsistencyCheck();
            }
            catch (std::logic_error&)
            {
                return 1;
            }
            if (!rbt.checkRBCorrect()) return 2;
            auto* node = rbt.get(rbt.minimum());
            int prev = -2147483647;
            while (node)
            {
                if (node->data < prev)
                {
                    return 3; // Unsorted!
                }
                prev = node->data;
                node = rbt.get(rbt.successor(rbt.id(node)));
            }
        }
        // Test erasure by erasing it again.
        while (!rbt.empty())
        {
            auto n = randseq(gen, 0, int(rbt.size()-1), 1)[0];
            rbt.erase(n);
            try
            {
                rbt.performConsistencyCheck();
            }
            catch (std::logic_error&)
            {
                return 4;
            }
            auto* node = rbt.get(rbt.minimum());
            int prev = -2147483647;
            size_t it = 0;
            while (node)
            {
                if (node->data < prev)
                {
                    return 5; // Unsorted!
                }
                prev = node->data;
                node = rbt.get(rbt.successor(rbt.id(node)));
                ++it;
                if (it > rbt.size()+10)
                {
                    return 6; // Loop detected.
                }
            }
        }
    }
    return 0;
}

int main()
{
    return testPriorityQueue()
           + testRedBlackTree()
           + randomisedTestRedBlackTree();
}
