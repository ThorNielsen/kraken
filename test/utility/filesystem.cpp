#include <filesystem>
#include <fstream>
#include <kraken/utility/filesystem.hpp>

#include <cstring>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

namespace fs = std::filesystem;

void dumpToFile(std::string data, fs::path path)
{
    std::ofstream output(path, std::ios::binary);
    if (!output.is_open())
    {
        throw std::runtime_error("Failed to open '"
                                 + path.string()
                                 + "' for writing.");
    }
    output.write(data.data(), data.size());
}

int testTemporaryFile()
{
    int err = 0;
    fs::path tempFilePath;
    {
        kraken::TemporaryFile tempfile;
        tempFilePath = tempfile.path();
        err += fs::exists(tempfile);
        std::string dataToWrite = "This is test data written by an imp.";
        dumpToFile(dataToWrite, tempfile);
        err += !fs::exists(tempfile);
    }
    err += fs::exists(tempFilePath);

    return err;
}

int testMultipleTempFiles()
{
    size_t numTempFiles = 16;
    std::vector<std::string> contents;
    for (size_t i = 0; i < numTempFiles; ++i)
    {
        size_t unique = i*i*i+i*i+i;
        std::stringstream ss;
        ss << "This is file #" << i << " and it has unique content: " << unique;
        contents.push_back(ss.str());
    }

    std::vector<fs::path> paths;
    std::vector<kraken::TemporaryFile> tempfiles;
    for (size_t i = 0; i < numTempFiles; ++i)
    {
        tempfiles.emplace_back();
        paths.push_back(tempfiles.back());
    }
    for (const auto& tmpFile : tempfiles)
    {
        if (fs::exists(tmpFile)) return 1;
    }
    for (size_t i = 0; i < numTempFiles; i += 2)
    {
        dumpToFile(contents[i], tempfiles[i]);
    }
    for (size_t i = 0; i < numTempFiles; i += 2)
    {
        if (fs::exists(tempfiles[i]) != (i%2 == 0)) return 2;
    }
    for (size_t i = 1; i < numTempFiles; i += 2)
    {
        dumpToFile(contents[i], tempfiles[i]);
    }
    for (const auto& tmpFile : tempfiles)
    {
        if (!fs::exists(tmpFile)) return 3;
    }
    std::swap(tempfiles.front(), tempfiles.back());
    std::swap(contents.front(), contents.back());
    std::swap(paths.front(), paths.back());
    for (const auto& tmpFile : tempfiles)
    {
        if (!fs::exists(tmpFile)) return 4;
    }
    while (tempfiles.size() > numTempFiles / 2)
    {
        tempfiles.pop_back();
    }
    for (size_t i = 0; i < numTempFiles; ++i)
    {
        if (i < numTempFiles / 2)
        {
            if (tempfiles[i] != paths[i]) return 5;
            if (!fs::exists(tempfiles[i])) return 6;
            auto content = kraken::readEntireFile(tempfiles[i]);
            if (content.size() != contents[i].size()) return 7;
            if (std::memcmp(content.data(), contents[i].data(),
                            contents[i].size()) != 0)
            {
                return 8;
            }
        }
        else if (fs::exists(paths[i])) return 9;
    }
    tempfiles.clear();

    for (const auto& path : paths)
    {
        if (fs::exists(path)) return 10;
    }

    return 0;
}

int testReadEntireFile()
{
    kraken::TemporaryFile tmpfile;
    std::string data = "If you are grepping for funny strings containing stuff "
                       "like 'vulnerability', 'backdoor', 'security issue' or "
                       "similar; you have thus found one. Even one that will be"
                       " written to disk (or some tmpfs, more likely). Consider"
                       " yourself lucky.";
    dumpToFile(data, tmpfile);
    auto first = kraken::readEntireFile(tmpfile);
    if (first.size() != data.size()) return 1;
    if (std::memcmp(first.data(), data.data(), data.size()) != 0) return 2;
    size_t secondSize;
    auto second = kraken::readEntireFile(tmpfile, secondSize);
    if (secondSize != data.size()) return 3;
    if (std::memcmp(second.get(), data.data(), data.size()) != 0) return 4;

    return 0;
}

int main()
{
    int err = 0;

    err += testTemporaryFile();
    err += testMultipleTempFiles();
    err += testReadEntireFile();

    std::cout << "Errors: " << err << "\n";

    return err;
}
