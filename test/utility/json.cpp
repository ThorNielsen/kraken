#include "kraken/types.hpp"
#include "kraken/utility/utf.hpp"
#include "kraken/utility/json.hpp"
#include "kraken/utility/testsummariser.hpp"

#include <sstream>
#include <stdexcept>

using kraken::TestSummariser;
using kraken::JSONType;
using kraken::parseJSON;

TestSummariser testBasicParsing()
{
    TestSummariser test;

    auto value = parseJSON(R"*({})*");
    if (test.check(value.type() == JSONType::Object))
    {
        test.expect(value.asObject().empty());
        test.expect(value.isObject());
    }

    value = parseJSON(R"*([])*");
    if (test.check(value.type() == JSONType::Array))
    {
        test.expect(value.asArray().empty());
        test.expect(value.isArray());
    }

    value = parseJSON(R"*(-.3e3)*");
    if (test.check(value.type() == JSONType::Double))
    {
        test.expect(value == -.3e3);
        test.expect(value.isDouble());
    }

    test.expect((double)value == -.3e3);
    test.expect((int)value == -300);

    value = parseJSON(R"*(2.7)*");
    if (test.check(value.type() == JSONType::Double))
    {
        test.expect(value == 2.7);
        test.expect(value.isDouble());
    }

    value = parseJSON(R"*(3)*");
    if (test.check(value.type() == JSONType::SignedInt))
    {
        test.expect(value.asSignedInt() == 3);
        test.expect(value.isSignedInt());
    }

    // Value here is 2^64 - 1.
    value = parseJSON(R"*(18446744073709551615)*");
    if (test.check(value.type() == JSONType::UnsignedInt))
    {
        test.expect(value.asUnsignedInt() == 18446744073709551615ull);
        test.expect(value.isUnsignedInt());
    }

    // Too large to represent as a 64-bit integer, so it MUST be converted into
    // a double.
    value = parseJSON(R"*(184467440737095516151)*");
    if (test.check(value.type() == JSONType::Double))
    {
        test.expect(std::abs(value.asDouble() - 184467440737095516151.) < 1e+9);
    }

    test.expect(parseJSON("").type() == JSONType::Null);
    test.expect(parseJSON("null").type() == JSONType::Null);
    test.expect(parseJSON("false").type() == JSONType::Boolean);
    test.expect(parseJSON("true").type() == JSONType::Boolean);
    test.expect(parseJSON("\"\"").type() == JSONType::String);

    test.expect(parseJSON("false") == false);
    test.expect(parseJSON("true") == true);

    if (test.check((value = parseJSON("true")).isBoolean()))
    {
        test.expect(value.isBoolean());
        test.expect(value.asBoolean() == true);
    }

    if (test.check((value = parseJSON("\"2.7\"")).type() == JSONType::String))
    {
        test.expect(value.isString());
        test.expect(value.asString() == "2.7");
    }

    if (test.check((value = parseJSON(R"*("\ud83D\uDC09")*")).isString()))
    {
        kraken::U32 codepoint = kraken::invalidCodepoint;
        auto it = kraken::decodeUTF8(value.asString().begin(),
                                     value.asString().end(),
                                     codepoint);
        test.expect(codepoint == 0x1f409);
        test.expect(it == value.asString().end());
    }

    test.expect(value != parseJSON("\"2.7\""));
    return test;
}

TestSummariser testNestedParsing()
{
    TestSummariser test;
    auto object = kraken::parseJSON(R"***({"string": "value", "str": "\n",
                                "moreescape": "tru\/\/",
                                "bless": ["you", "them", "it", true, false, null],
                                "xml": [0., 0.02E3, -4.33e+7, -0.2e-2,
                                        3.141592653589793238462643383279, {"s":"v"}]})***");
    if (test.check(object.isObject()))
    {
        test.expect(object.size() == 5);
        test.expect(object.asObject().size() == 5);
        test.expect(object["string"] == "value");
        test.expect(object["str"] == "\n");
        test.expect(object["moreescape"] == "tru//");

        test.expect(object["bless"].isArray());
        test.expect(object["xml"].isArray());

        test.expect(object("bless").isArray());
        test.expect(object("xml").isArray());
        test.expect(object("xml")(5).isObject());
        test.expect(object("xml")(6).isNull());

        test.expect(object("xml").size() == 6);
        test.expect(object("xml")(5)("s") == "v");
    }

    if (test.check(object["bless"].isArray()))
    {
        const auto& bless = object["bless"].asArray();
        test.assert(bless.size() == 6);
        test.expect(bless[0] == "you");
        test.expect(bless[1] == "them");
        test.expect(bless[2] == "it");
        test.expect(bless[3] == true);
        test.expect(bless[4] == false);
        test.expect(bless[5] == nullptr);
    }
    if (test.check(object["xml"].isArray()))
    {
        auto& xml = object["xml"].asArray();
        test.assert(xml.size() == 6);
        test.expect(xml[0] == 0);
        test.expect(xml[1] == 20.);
        test.expect(xml[1] == 20);
        test.expect(xml[1] == kraken::U64(20));
        test.expect(xml[2] == -4.33e+7);
        test.expect(xml[3] == -.2e-2);
        test.expect(xml[4] == 3.141592653589793238462643383279);
        test.expect(xml[5] == kraken::parseJSON(R"({"s" : "v"})"));
        test.expect(xml[5]["s"] == "v");
    }

    return test;
}

TestSummariser testSerialisation()
{
    TestSummariser test;
    kraken::JSONValue object;
    object["modules"][0]["stage"] = "vertex";
    object["modules"][0]["location"] = "file:./shaders/vert2.spv";
    object["modules"][0]["entrypoint"] = "main";
    object["modules"][1]["stage"] = "fragment";
    object["modules"][1]["location"] = "file:./shaders/frag2.spv";
    object["modules"][1]["entrypoint"] = "main";

    object["descriptors"][0]["stages"].push_back({});
    object["descriptors"][0]["stages"][0] = "vertex";
    object["descriptors"][0]["stages"].push_back({});
    object["descriptors"][0]["stages"][1] = "fragment";

    object["atest"]["simple"] = "boop";
    object["atest"]["sample"] = u8"bææp";
    object["zzz"][0] = u8"ææ\\\"🐉";

    object["ell"] = {"string", "bob", 4.f};
    object["smell"] = {{"string", "bring"},
                       {std::string("thing"), "i-y"},
                       {"fatman", 5}};
    object["smell"]["thing"] = 4;

    test.expect(parseJSON(object.serialise(true)) == object,
                "Round-trip with compact printing.");
    test.expect(parseJSON(object.serialise(false)) == object,
                "Round-trip with pretty printing.");

    std::stringstream ss;
    ss << object << object;
    kraken::JSONValue objectReread, reread2;

    ss >> objectReread;
    test.expect(object == objectReread,
                "Correct reading when another valid JSON immediately follows.");

    ss >> reread2;
    test.expect(object == reread2,
                "Correct termination / start of reading when concatenated.");

    return test;
}

TestSummariser testPrecision()
{
    TestSummariser test;
    kraken::F64 num = 0.30000000000001;
    if (num <= .3)
    {
        throw std::logic_error("Precision of numbers is too low.");
    }
    kraken::JSONValue jsonObj = num;
    if (test.check(jsonObj.isDouble()))
    {
        test.expect(jsonObj.asDouble() == num);
        test.expect(jsonObj == num);
        test.expectFalse(jsonObj != num);
        test.expect(jsonObj != num + std::numeric_limits<double>::epsilon());
        test.expectFalse(jsonObj == num + std::numeric_limits<double>::epsilon());
    }
    test.expect(parseJSON(jsonObj.serialise()) == num);
    return test;
}

TestSummariser testStreamReading()
{
    TestSummariser test;
    std::stringstream ss("truefalse");
    kraken::JSONValue reader;

    ss >> reader;
    test.expect(reader == true);
    test.expect(!!reader);
    test.expectFalse(!reader);

    ss >> reader;
    test.expect(!reader);
    test.expectFalse(!!reader);

    ss.str("{}[]");
    ss >> reader;
    test.assert(reader.isObject());
    test.check(reader.asObject().empty());
    ss >> reader;
    test.assert(reader.isArray());
    test.check(reader.asArray().empty());

    ss.str("[]{}");
    ss >> reader;
    test.assert(reader.isArray());
    test.check(reader.asArray().empty());
    ss >> reader;
    test.assert(reader.isObject());
    test.check(reader.asObject().empty());

    ss.str(R"("a  b""c   d")");
    ss >> reader;
    test.check(reader == "a  b");
    ss >> reader;
    test.check(reader == "c   d");

    return test;
}

TestSummariser testPrettyPrint()
{
    TestSummariser test;
    kraken::JSONValue json;
    json[0][0][0]["test"] = "toast";

    kraken::JSONValue json2;
    json2[0][0]["test"] = "west";
    json2[0][0]["test2"] = "west";
    json2[0][0]["test3"] = "west";
    std::stringstream ss;
    ss << json;
    test.expect(ss.str() ==
R"([
    [
        [
            {"test": "toast"}
        ]
    ]
])");
    ss = {};
    ss << json2;
    test.expect(ss.str() ==
R"([
    [
        {
            "test": "west",
            "test2": "west",
            "test3": "west"
        }
    ]
])");

    return test;
}

TestSummariser testCreation()
{
    TestSummariser test;
    test.expect(kraken::JSONValue::Array({}).type() == kraken::JSONType::Array);
    test.expect(kraken::JSONValue::Array({}).isArray());
    test.expect(kraken::JSONValue::Object({}).type() == kraken::JSONType::Object);
    test.expect(kraken::JSONValue::Object({}).isObject());
    test.expect(kraken::JSONValue::Array({1,2,3,4,5})
                == kraken::JSONValue::parse("[1,2,3,4,5]"));

    test.expect(kraken::JSONValue::Object({"jens", "bob"})
                == kraken::JSONValue::parse(R"*({"jens": "bob"})*"));
    test.expect(kraken::JSONValue::Object({{"jens", "bob"}, {"claus", 5}})
                == kraken::JSONValue::parse(R"*({"jens": "bob", "claus": 5})*"));

    return test;
}

TestSummariser testTyping()
{
    TestSummariser test;

    kraken::JSONValue json;
    json["expectNull"] = nullptr;
    json["expectFalse"] = false;
    json["expectTrue"] = true;
    json["expectNumeric"] = 42;
    json["expectNumericString"] = "42";
    json["expectNonNumericString"] = "not a number";
    json["expectArray"] = kraken::JSONValue::Array({1,2,3,4});
    json["expectObject"] = kraken::JSONValue::Object({"ob", "ject"});


    test.expect(json["expectNull"].type() == kraken::JSONType::Null);
    test.expect(json["expectFalse"].type() == kraken::JSONType::Boolean);
    test.expect(json["expectTrue"].type() == kraken::JSONType::Boolean);
    test.expect(json["expectNumeric"].isNumeric());
    test.expect(json["expectNumericString"].type() == kraken::JSONType::String);
    test.expect(json["expectNonNumericString"].type() == kraken::JSONType::String);
    test.expect(json["expectArray"].type() == kraken::JSONType::Array);
    test.expect(json["expectObject"].type() == kraken::JSONType::Object);

    test.expect((bool)json["expectNull"] == false);
    test.expect((bool)json["expectFalse"] == false);
    test.expect((bool)json["expectTrue"] == true);

#define EXPECT_EXCEPTION(expr) \
    do {try { (void)(expr); test.expect(false, \
    "An exception should have been thrown"); } catch (...) {} } while (false)

    EXPECT_EXCEPTION((bool)json["expectNumeric"]);
    EXPECT_EXCEPTION((bool)json["expectNumericString"]);
    EXPECT_EXCEPTION((bool)json["expectNonNumericString"]);
    EXPECT_EXCEPTION((bool)json["expectArray"]);
    EXPECT_EXCEPTION((bool)json["expectObject"]);

    test.expect((int)json["expectNull"] == 0);
    test.expect((int)json["expectFalse"] == 0);
    test.expect((int)json["expectTrue"] == 1);
    test.expect((int)json["expectNumeric"] == 42);
    test.expect((int)json["expectNumericString"] == 42);

    EXPECT_EXCEPTION((int)json["expectNonNumericString"]);
    EXPECT_EXCEPTION((int)json["expectArray"]);
    EXPECT_EXCEPTION((int)json["expectObject"]);

    EXPECT_EXCEPTION(json["expectObject"][7] == 3);
    EXPECT_EXCEPTION(json["expectArray"]["ob"] == 2);

    test.expect(json("doesn't exist") == nullptr);

    EXPECT_EXCEPTION(json["expectFalse"].push_back(5));
    EXPECT_EXCEPTION(json["expectTrue"].push_back(5));
    EXPECT_EXCEPTION(json["expectNumeric"].push_back(5));
    EXPECT_EXCEPTION(json["expectNumericString"].push_back(5));
    EXPECT_EXCEPTION(json["expectNonNumericString"].push_back(5));
    EXPECT_EXCEPTION(json["expectObject"].push_back(5));

#undef EXPECT_EXCEPTION

    kraken::JSONValue copied;
    copied["expectNull"] = json["expectNull"];
    copied["expectFalse"] = json["expectFalse"];
    copied["expectTrue"] = json["expectTrue"];
    copied["expectNumeric"] = json["expectNumeric"];
    copied["expectNumericString"] = json["expectNumericString"];
    copied["expectNonNumericString"] = json["expectNonNumericString"];
    copied["expectArray"] = json["expectArray"];
    copied["expectObject"] = json["expectObject"];

    test.expect(copied == json);

    copied["expectTrue"] = 9;

    test.expect(copied != json);

    json["expectTrue"] = 9;

    test.expect(copied == json);

    return test;
}

TestSummariser testConversion()
{
    TestSummariser test;

    test.expect((std::string)parseJSON("0.25") == "0.25");
    test.expect((std::string)parseJSON("125") == "125");
    test.expect((std::string)parseJSON("-19") == "-19");
    test.expect((std::string)parseJSON("18446744073709551615") == "18446744073709551615");

    return test;
}

int main()
{
    TestSummariser tests;
    tests += testBasicParsing();
    tests += testNestedParsing();
    tests += testSerialisation();
    tests += testPrecision();
    tests += testStreamReading();
    tests += testPrettyPrint();
    tests += testCreation();
    tests += testTyping();
    tests += testConversion();
    return tests.returnCode();
}
