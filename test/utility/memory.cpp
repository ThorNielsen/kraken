#include <cstddef>
#include <cstring>
#include <kraken/types.hpp>
#include <kraken/utility/memory.hpp>

#include <cstdint>
#include <vector>

int testEndianswaps()
{
    using kraken::swapEndian;
    int err = 0;
    const kraken::U8 b1 = 0xab;
    const kraken::U16 b2 = 0xbeef;
    const kraken::U32 b4 = 0xab00cafe;
    const kraken::U64 b8 = 0x1deadb0b99adfeed;
    const kraken::S64 sgn = -4352987142349ll;
    const kraken::F32 flt = 1.207e+3f;
    const kraken::F64 dbl = 1.05982312379e+102;
    err += !(swapEndian(b1) == 0xab);
    err += !(swapEndian(b2) == 0xefbe);
    err += !(swapEndian(b4) == 0xfeca00ab);
    err += !(swapEndian(b8) == 0xedfead990bdbea1d);
    err += !(swapEndian(swapEndian(sgn)) == sgn);
    err += !(swapEndian(swapEndian(flt)) == flt);
    err += !(swapEndian(swapEndian(dbl)) == dbl);

    kraken::U64 inplace = 0x1001facadebeefad;
    kraken::swapEndianIP(inplace);
    err += !(inplace == 0xadefbedecafa0110);
    kraken::U32 ip4 = 0xbeeffeed;
    kraken::swapEndianIP(ip4);
    err += !(ip4 == 0xedfeefbe);
    kraken::U16 ip2 = 0x12fe;
    kraken::swapEndianIP(ip2);
    err += !(ip2 == 0xfe12);
    kraken::U8 ip1 = 0x0a;
    kraken::swapEndianIP(ip1);
    err += !(ip1 == 0x0a);
    return err;
}

class MemTest
{
public:
    MemTest(int& alloc, int& destroy)
        : m_alloc{alloc}, m_destroy{destroy}
    {
        ++m_alloc;
    }
    MemTest(MemTest&) = delete;
    MemTest(MemTest&&) = delete;
    ~MemTest()
    {
        ++m_destroy;
    }
    int& m_alloc;
    int& m_destroy;
};

int testAlloc()
{
    int err = 0;

    int alloc = 0;
    int destroy = 0;
    {
        auto ptr1 = kraken::make_unique<MemTest>(alloc, destroy);
        err += !(alloc == 1 && destroy == 0);
        {
            auto ptr2 = kraken::make_unique<MemTest>(alloc, destroy);
            err += !(alloc == 2 && destroy == 0);
        }
        err += !(alloc == 2 && destroy == 1);
    }
    err += !(alloc == 2 && destroy == 2);

    size_t size = 5307;
    void* ptr = kraken::allocateUntyped(size, 29);
    err += !(reinterpret_cast<std::uintptr_t>(ptr) % 29 == 0);
    kraken::deleteUntyped(ptr);

    return err;
}

int testRemoveUntypedArrayElements()
{
    int err = 0;

    constexpr size_t size = 10;
    const char* teststring = "ABCDEFGHIJ";
    char data[size];
    std::memcpy(data, teststring, size);
    std::vector<kraken::U32> toRemove{0, 1, 5, 7};
    kraken::removeUntypedArrayElements(toRemove.begin(),
                                       toRemove.end(),
                                       10,
                                       data,
                                       sizeof(data[0]));

    err += !!strncmp(data, "CDEGIJ", 6);
    std::memcpy(data, teststring, size);
    toRemove = {0, 1, 3, 5, 7, 8, 9};
    kraken::removeUntypedArrayElements(toRemove.begin(),
                                       toRemove.end(),
                                       10,
                                       data,
                                       sizeof(data[0]));
    err += !!strncmp(data, "CEG", 3);
    std::memcpy(data, teststring, size);
    toRemove = {};
    kraken::removeUntypedArrayElements(toRemove.begin(),
                                       toRemove.end(),
                                       10,
                                       data,
                                       sizeof(data[0]));
    err += !!strncmp(data, "ABCDEFGHIJ", 3);

    return err;
}

int main()
{
    int err = 0;
    err += testEndianswaps();
    err += testAlloc();
    err += testRemoveUntypedArrayElements();
    return err;
}
