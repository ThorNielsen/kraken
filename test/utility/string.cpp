#include <kraken/utility/string.hpp>
#include <kraken/types.hpp>

#include <iostream>
#include <string>
#include <vector>

int testCases()
{
    auto clvl = kraken::ComparisonLevel::Identical;
    int err = 0;
    err += !!kraken::compare(kraken::uppercase(u8"stRaße"), u8"STRASSE", std::locale(), clvl);
    err += !!kraken::compare(kraken::lowercase(u8"stRaße"), u8"straße", std::locale(), clvl);
    err += !!kraken::compare(kraken::titlecase(u8"stRaße"), u8"Straße", std::locale(), clvl);
    err += !!kraken::compare(kraken::foldcase(u8"stRaße"), u8"strasse", std::locale(), clvl);
    err += !!kraken::compare(kraken::uppercase(u8"ὈΔΥΣΣΕΎΣ"), u8"ὈΔΥΣΣΕΎΣ", std::locale(), clvl);
    err += !!kraken::compare(kraken::lowercase(u8"ὈΔΥΣΣΕΎΣ"), u8"ὀδυσσεύς", std::locale(), clvl);
    err += !!kraken::compare(kraken::titlecase(u8"ὈΔΥΣΣΕΎΣ"), u8"Ὀδυσσεύς", std::locale(), clvl);
    err += !!kraken::compare(kraken::foldcase(u8"ὈΔΥΣΣΕΎΣ"), u8"ὀδυσσεύσ", std::locale());
    return err;
}

int testCategories()
{
    int err = 0;
    err += !kraken::isAlnum('M');
    err += !kraken::isAlpha('M');
    err +=  kraken::isDigit('M');
    err += !kraken::isUpper('M');
    err +=  kraken::isLower('M');
    err +=  kraken::isHexDigit('M');
    err +=  kraken::isControl('M');
    err += !kraken::isGraphical('M');
    err +=  kraken::isWhitespace('M');
    err +=  kraken::isBlank('M');
    err +=  kraken::isPunct('M');

    err += !kraken::isAlnum('0');
    err += !kraken::isDigit('0');
    err +=  kraken::isLower('0');
    err += !kraken::isHexDigit('0');

    err +=  kraken::isAlnum('#');
    err += !kraken::isGraphical('#');
    err += !kraken::isPunct('#');
    err +=  kraken::isBlank('#');

    err += !kraken::isAlnum(0x416); // Ж
    err += !kraken::isUpper(0x416);
    err +=  kraken::isLower(0x416);
    err += !kraken::isGraphical(0x416);
    err +=  kraken::isHexDigit(0x416);

    err += !kraken::isAlnum(0x6f4); // ۴
    err +=  kraken::isAlpha(0x6f4);
    err += !kraken::isDigit(0x6f4);
    err +=  kraken::isControl(0x6f4);
    err += !kraken::isHexDigit(0x6f4);

    err += !kraken::isDigit(0x1d7e0); // 𝟠
    err +=  kraken::isUpper(0x1d7e0);
    err += !kraken::isHexDigit(0x1d7e0);

    return err;
}

int testComparison()
{
    auto dan = kraken::createLocale("da_DK.UTF-8");
    auto sve = kraken::createLocale("sv_SE.UTF-8");
    auto ger = kraken::createLocale("de_DE.UTF-8");
    auto clvl = kraken::ComparisonLevel::BaseCaseAccents;

    int err = 0;
    err += !(kraken::compare(u8"Latin",   u8"Opossum", dan, clvl) < 0);
    err += !(kraken::compare(u8"Opossum", u8"Æsel",    dan, clvl) < 0);
    err += !(kraken::compare(u8"Æsel",    u8"Øst",     dan, clvl) < 0);
    err += !(kraken::compare(u8"Øst",     u8"Åkande",  dan, clvl) < 0);
    err += !(kraken::compare(u8"Latin",   u8"Æsel",    dan, clvl) < 0);
    err += !(kraken::compare(u8"Opossum", u8"Øst",     dan, clvl) < 0);
    err += !(kraken::compare(u8"Æsel",    u8"Latin",   dan, clvl) > 0);

    err += !(kraken::compare(u8"Latin",   u8"Öst",    sve, clvl) < 0);
    err += !(kraken::compare(u8"Latin",   u8"Ält",    sve, clvl) < 0);
    err += !(kraken::compare(u8"Opossum", u8"Åkande", sve, clvl) < 0);
    err += !(kraken::compare(u8"Åkande",  u8"Ält",    sve, clvl) < 0);
    err += !(kraken::compare(u8"Ält",     u8"Öst",    sve, clvl) < 0);

    err += !(kraken::compare(u8"Latin", u8"Ält", ger, clvl) > 0);
    err += !(kraken::compare(u8"Ält",   u8"Öst", ger, clvl) < 0);
    err += !(kraken::compare(u8"Ält",   u8"B",   ger, clvl) < 0);
    err += !(kraken::compare(u8"Öst",   u8"O",   ger, clvl) > 0);
    err += !(kraken::compare(u8"Öst",   u8"P",   ger, clvl) < 0);
    return err;
}

int testIteration()
{
    using kraken::String;
    using std::basic_string;
    constexpr kraken::U32 codepoints[] =
    {
        0x01d465, 0x0000b2, 0x000020, 0x00002b, 0x000020, 0x01d466, 0x0000b3,
        0x000020, 0x002209, 0x000020, 0x00211d, 0x000020, 0x0021d2, 0x000020,
        0x000028, 0x01d465, 0x00002c, 0x000020, 0x01d466, 0x000029, 0x000020,
        0x002209, 0x000020, 0x00211d, 0x0000d7, 0x00211d
    };
    int err = 0;
    String str(u8"𝑥² + 𝑦³ ∉ ℝ ⇒ (𝑥, 𝑦) ∉ ℝ×ℝ");

    std::vector<std::string> toTest;
    toTest.push_back(String("𝑥² + 𝑦³ ∉ ℝ ⇒ (𝑥, 𝑦) ∉ ℝ×ℝ")); // char*
    toTest.push_back(String(u"𝑥² + 𝑦³ ∉ ℝ ⇒ (𝑥, 𝑦) ∉ ℝ×ℝ")); // U16*
    toTest.push_back(String(U"𝑥² + 𝑦³ ∉ ℝ ⇒ (𝑥, 𝑦) ∉ ℝ×ℝ")); // U32*
    toTest.push_back(String(L"𝑥² + 𝑦³ ∉ ℝ ⇒ (𝑥, 𝑦) ∉ ℝ×ℝ")); // wchar_t*
    toTest.push_back(String(basic_string<char>("𝑥² + 𝑦³ ∉ ℝ ⇒ (𝑥, 𝑦) ∉ ℝ×ℝ")));
    toTest.push_back(String(basic_string<char16_t>(u"𝑥² + 𝑦³ ∉ ℝ ⇒ (𝑥, 𝑦) ∉ ℝ×ℝ")));
    toTest.push_back(String(basic_string<char32_t>(U"𝑥² + 𝑦³ ∉ ℝ ⇒ (𝑥, 𝑦) ∉ ℝ×ℝ")));
    toTest.push_back(String(basic_string<wchar_t>(L"𝑥² + 𝑦³ ∉ ℝ ⇒ (𝑥, 𝑦) ∉ ℝ×ℝ")));
    for (auto& test : toTest)
    {
        err += !(str == test);
    }

    auto* start = codepoints;
    for (auto c : str)
    {
        err += c != *start++;
    }
    return err;
}

int main()
{
    int err = 0;
    err += testCases();
    err += testCategories();
    err += testComparison();
    err += testIteration();
    return err;
}
