#include <cstddef>
#include <iterator>
#include <kraken/types.hpp>
#include <kraken/utility/utf.hpp>

#include <list>
#include <string>

constexpr kraken::U8 complexUTF8[] =
{
    0xf0, 0x9d, 0x91, 0xa5, 0xc2, 0xb2, 0x20, 0x2b, 0x20, 0xf0, 0x9d, 0x91,
    0xa6, 0xc2, 0xb3, 0x20, 0xe2, 0x88, 0x89, 0x20, 0xe2, 0x84, 0x9d, 0x20,
    0xe2, 0x87, 0x92, 0x20, 0x28, 0xf0, 0x9d, 0x91, 0xa5, 0x2c, 0x20, 0xf0,
    0x9d, 0x91, 0xa6, 0x29, 0x20, 0xe2, 0x88, 0x89, 0x20, 0xe2, 0x84, 0x9d,
    0xc3, 0x97, 0xe2, 0x84, 0x9d, 0x0a
};

constexpr kraken::U8 complexUTF16LE[] =
{
    0xff, 0xfe, // BOM
    0x35, 0xd8, 0x65, 0xdc, 0xb2, 0x00, 0x20, 0x00, 0x2b, 0x00, 0x20, 0x00,
    0x35, 0xd8, 0x66, 0xdc, 0xb3, 0x00, 0x20, 0x00, 0x09, 0x22, 0x20, 0x00,
    0x1d, 0x21, 0x20, 0x00, 0xd2, 0x21, 0x20, 0x00, 0x28, 0x00, 0x35, 0xd8,
    0x65, 0xdc, 0x2c, 0x00, 0x20, 0x00, 0x35, 0xd8, 0x66, 0xdc, 0x29, 0x00,
    0x20, 0x00, 0x09, 0x22, 0x20, 0x00, 0x1d, 0x21, 0xd7, 0x00, 0x1d, 0x21,
    0x0a, 0x00
};

constexpr kraken::U8 complexUTF16BE[] =
{
    0xfe, 0xff, // BOM
    0xd8, 0x35, 0xdc, 0x65, 0x00, 0xb2, 0x00, 0x20, 0x00, 0x2b, 0x00, 0x20,
    0xd8, 0x35, 0xdc, 0x66, 0x00, 0xb3, 0x00, 0x20, 0x22, 0x09, 0x00, 0x20,
    0x21, 0x1d, 0x00, 0x20, 0x21, 0xd2, 0x00, 0x20, 0x00, 0x28, 0xd8, 0x35,
    0xdc, 0x65, 0x00, 0x2c, 0x00, 0x20, 0xd8, 0x35, 0xdc, 0x66, 0x00, 0x29,
    0x00, 0x20, 0x22, 0x09, 0x00, 0x20, 0x21, 0x1d, 0x00, 0xd7, 0x21, 0x1d,
    0x00, 0x0a
};

constexpr kraken::U16 complexUTF16[] =
{
    0xfeff, // BOM
    0xd835, 0xdc65, 0x00b2, 0x0020, 0x002b, 0x0020, 0xd835, 0xdc66, 0x00b3,
    0x0020, 0x2209, 0x0020, 0x211d, 0x0020, 0x21d2, 0x0020, 0x0028, 0xd835,
    0xdc65, 0x002c, 0x0020, 0xd835, 0xdc66, 0x0029, 0x0020, 0x2209, 0x0020,
    0x211d, 0x00d7, 0x211d, 0x000a
};

constexpr kraken::U8 complexUTF32LE[] =
{
    0xff, 0xfe, 0x00, 0x00, // BOM
    0x65, 0xd4, 0x01, 0x00, 0xb2, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00,
    0x2b, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x66, 0xd4, 0x01, 0x00,
    0xb3, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x09, 0x22, 0x00, 0x00,
    0x20, 0x00, 0x00, 0x00, 0x1d, 0x21, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00,
    0xd2, 0x21, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00,
    0x65, 0xd4, 0x01, 0x00, 0x2c, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00,
    0x66, 0xd4, 0x01, 0x00, 0x29, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00,
    0x09, 0x22, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x1d, 0x21, 0x00, 0x00,
    0xd7, 0x00, 0x00, 0x00, 0x1d, 0x21, 0x00, 0x00, 0x0a, 0x00, 0x00, 0x00
};

constexpr kraken::U8 complexUTF32BE[] =
{
    0x00, 0x00, 0xfe, 0xff, // BOM
    0x00, 0x01, 0xd4, 0x65, 0x00, 0x00, 0x00, 0xb2, 0x00, 0x00, 0x00, 0x20,
    0x00, 0x00, 0x00, 0x2b, 0x00, 0x00, 0x00, 0x20, 0x00, 0x01, 0xd4, 0x66,
    0x00, 0x00, 0x00, 0xb3, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x22, 0x09,
    0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x21, 0x1d, 0x00, 0x00, 0x00, 0x20,
    0x00, 0x00, 0x21, 0xd2, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x28,
    0x00, 0x01, 0xd4, 0x65, 0x00, 0x00, 0x00, 0x2c, 0x00, 0x00, 0x00, 0x20,
    0x00, 0x01, 0xd4, 0x66, 0x00, 0x00, 0x00, 0x29, 0x00, 0x00, 0x00, 0x20,
    0x00, 0x00, 0x22, 0x09, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x21, 0x1d,
    0x00, 0x00, 0x00, 0xd7, 0x00, 0x00, 0x21, 0x1d, 0x00, 0x00, 0x00, 0x0a
};

constexpr kraken::U32 decoded[] =
{
    0x01d465, 0x0000b2, 0x000020, 0x00002b, 0x000020, 0x01d466, 0x0000b3,
    0x000020, 0x002209, 0x000020, 0x00211d, 0x000020, 0x0021d2, 0x000020,
    0x000028, 0x01d465, 0x00002c, 0x000020, 0x01d466, 0x000029, 0x000020,
    0x002209, 0x000020, 0x00211d, 0x0000d7, 0x00211d, 0x00000a
};

// Leading 4-bit sequence:
// 0-7: Standard ascii character.
// 8-b: Continuation character.
// c-d: Leading byte of 2-byte sequence.
// e: Leading byte of 3-byte sequence.
// f: Leading byte of 4-byte sequence.

constexpr kraken::U8 brokenUTF8[] =
{
    // 0xbf invalid here; the next two starts with 0b10, and so all of this
    // should give replacement characters.
    0xbf, 0x9d, 0x91,

    // Normal.
    0xc2, 0xb2, 0x20,

    // Broken: 0xcf begins a two-byte sequence, but then 0xcf is yet another
    // sequence starting character. Finally, 0x20 is not valid as a continuation
    // character.
    0xcf, 0xcf, 0x20,

    // Continue with some valid characters.
    0x2b, 0x20,

    // Create a valid 4-byte character.
    0xf0, 0x9d, 0x91, 0xa6,
    // Create a 4-byte character where byte 4 is not an extended one.
    0xf0, 0x9d, 0x91, 0x41,
    // Create a 4-byte character where byte 3 is not an extended one.
    0xf0, 0x9d, 0x42,
    // Create a 4-byte character where byte 2 is not an extended one.
    0xf0, 0x43,

    // Create a valid 3-byte character.
    0xe2, 0x88, 0x89,
    // Create a 3-byte character where byte 3 is not an extended one.
    0xe2, 0x88, 0x61,
    // Create a 3-byte character where byte 2 is not an extended one.
    0xe2, 0x62,

    // Create a valid 2-byte character.
    0xc3, 0x97,
    // Create a 2-byte character where byte 2 is not an extended one.
    0xc3, 0x63,

    // Now we just start with a continuation character.
    0x82, 0xb3, 0x20,

    // Normal
    0xe2, 0x88, 0x89, 0x20, 0xe2, 0x84, 0x9d, 0x20,

    // We end with a continuation character missing its final 3 bytes.
    0xf0,
};

using kraken::invalidCodepoint;

constexpr kraken::U32 brokenUTF8Decoded[] =
{
    invalidCodepoint, invalidCodepoint, invalidCodepoint,
    0xb2, 0x20,
    invalidCodepoint, invalidCodepoint, 0x20,
    0x2b, 0x20,
    0x1d466,
    invalidCodepoint, 0x41,
    invalidCodepoint, 0x42,
    invalidCodepoint, 0x43,
    0x2209,
    invalidCodepoint, 0x61,
    invalidCodepoint, 0x62,
    0xd7,
    invalidCodepoint, 0x63,
    invalidCodepoint, invalidCodepoint, 0x20,
    0x2209, 0x20, 0x211d, 0x20,
    invalidCodepoint
};

#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof((arr)[0]))

using U8string = std::basic_string<kraken::U8>;
using U16string = std::basic_string<kraken::U16>;
using U32string = std::basic_string<kraken::U32>;

#define createDecoder(name, array, func)    \
U32string name()                            \
{                                           \
    U32string str;                          \
    auto* begin = array;                    \
    auto* end = begin+ARRAY_SIZE(array);    \
    kraken::U32 codepoint;                  \
    while (begin != end)                    \
    {                                       \
        begin = func(begin, end, codepoint);\
        str.push_back(codepoint);           \
    }                                       \
    return str;                             \
}

createDecoder(decodeUTF8, complexUTF8, kraken::decodeUTF8);
createDecoder(decodeUTF16LE, complexUTF16LE, kraken::decodeUTF16LE);
createDecoder(decodeUTF16BE, complexUTF16BE, kraken::decodeUTF16BE);
createDecoder(decodeUTF16, complexUTF16, kraken::decodeUTF16);
createDecoder(decodeUTF32LE, complexUTF32LE, kraken::decodeUTF32LE);
createDecoder(decodeUTF32BE, complexUTF32BE, kraken::decodeUTF32BE);
createDecoder(decodeUTF32, decoded, kraken::decodeUTF32);

U32string decodeUTF8ForwardIterator(const kraken::U8* array, size_t arraySize)
{
    std::list<kraken::U8> liststr;
    {
        auto* begin = array;
        auto* end = begin+arraySize;
        while (begin != end)
        {
            liststr.push_back(*begin++);
        }
    }
    auto begin = liststr.begin();
    auto end = liststr.end();
    U32string str;
    kraken::U32 codepoint;
    while (begin != end)
    {
        begin = kraken::decodeUTF8(begin, end, codepoint);
        str.push_back(codepoint);
    }
    return str;
}

bool isCorrect(U32string str)
{
    if (str.size() != ARRAY_SIZE(decoded)) return false;
    for (size_t i = 0; i < ARRAY_SIZE(decoded); ++i)
    {
        if (str[i] != decoded[i]) return false;
    }
    return true;
}

bool isCorrectBOM(U32string str)
{
    if (str.empty()) return false;
    if (str[0] != 0xfeff) return false;
    str.erase(0, 1);
    return isCorrect(str);
}

template <typename T>
bool isSame(const T* a, const T* b, size_t n)
{
    while (n--)
    {
        if (*a++ != *b++) return false;
    }
    return true;
}

#define createEncoder(name, compareTo, encFunc, offset) \
bool name()                                             \
{                                                       \
    U8string str;                                       \
    auto encoder = std::back_inserter(str);             \
    auto* begin = decoded;                              \
    auto* end = begin+ARRAY_SIZE(decoded);              \
    while (begin != end)                                \
    {                                                   \
        encoder = encFunc(*begin++, encoder);           \
    }                                                   \
    return isSame(str.data(), (compareTo)+(offset), ARRAY_SIZE(compareTo)-(offset));\
}

createEncoder(testUTF8Enc, complexUTF8, kraken::encodeUTF8, 0);
createEncoder(testUTF16LEEnc, complexUTF16LE, kraken::encodeUTF16LE, 2);
createEncoder(testUTF16BEEnc, complexUTF16BE, kraken::encodeUTF16BE, 2);
createEncoder(testUTF32LEEnc, complexUTF32LE, kraken::encodeUTF32LE, 4);
createEncoder(testUTF32BEEnc, complexUTF32BE, kraken::encodeUTF32BE, 4);

bool testUTF16Enc()
{
    U16string str;
    auto encoder = std::back_inserter(str);
    auto* begin = decoded;
    auto* end = begin+ARRAY_SIZE(decoded);
    while (begin != end)
    {
        encoder = kraken::encodeUTF16(*begin++, encoder);
    }
    return isSame(str.data(), complexUTF16+1, ARRAY_SIZE(complexUTF16)-1);
}

bool testUTF32Enc()
{
    U32string str;
    auto encoder = std::back_inserter(str);
    auto* begin = decoded;
    auto* end = begin+ARRAY_SIZE(decoded);
    while (begin != end)
    {
        encoder = kraken::encodeUTF32(*begin++, encoder);
    }
    return isSame(str.data(), decoded, ARRAY_SIZE(decoded));
}

int testDecoding()
{
    int err = 0;
    err += !isCorrect(decodeUTF8());
    err +=  isCorrect(decodeUTF16LE());
    err += !isCorrectBOM(decodeUTF16LE());
    err +=  isCorrect(decodeUTF16BE());
    err += !isCorrectBOM(decodeUTF16BE());
    err +=  isCorrect(decodeUTF16());
    err += !isCorrectBOM(decodeUTF16());
    err +=  isCorrect(decodeUTF32LE());
    err += !isCorrectBOM(decodeUTF32LE());
    err +=  isCorrect(decodeUTF32BE());
    err += !isCorrectBOM(decodeUTF32BE());
    err += !isCorrect(decodeUTF32());
    err += !isCorrect(decodeUTF8ForwardIterator(complexUTF8,
                                                ARRAY_SIZE(complexUTF8)));
    return err;
}

int testEncoding()
{
    int err = 0;
    err += !testUTF8Enc();
    err += !testUTF16LEEnc();
    err += !testUTF16BEEnc();
    err += !testUTF32LEEnc();
    err += !testUTF32BEEnc();
    err += !testUTF16Enc();
    err += !testUTF32Enc();
    return err;
}

int testInvalidCodes()
{
    kraken::U32 cp;
    auto invalid = kraken::invalidCodepoint;
    int err = 0;
    kraken::U8 testArr[4] = {0xc0, 0x80, 0, 0};
    kraken::decodeUTF8(testArr, testArr + 4, cp);
    err += !(cp == invalid);
    return err;
}

int testAllEncodeDecode()
{
    int errorFound = 0;
    kraken::U8 testArr[4] = {0, 0, 0, 0};
    for (kraken::U32 cp = 0; cp <= 0x10ffff + 0x100; ++cp)
    {
        kraken::U32 check;
        kraken::encodeUTF8(cp, testArr);
        kraken::decodeUTF8(testArr, testArr+4, check);
        if (kraken::isValidCodepoint(cp) != (cp == check)) errorFound |= 1;
        kraken::encodeUTF16LE(cp, testArr);
        kraken::decodeUTF16LE(testArr, testArr+4, check);
        if (kraken::isValidCodepoint(cp) != (cp == check)) errorFound |= 2;
        kraken::encodeUTF16BE(cp, testArr);
        kraken::decodeUTF16BE(testArr, testArr+4, check);
        if (kraken::isValidCodepoint(cp) != (cp == check)) errorFound |= 4;
        kraken::encodeUTF32LE(cp, testArr);
        kraken::decodeUTF32LE(testArr, testArr+4, check);
        if (kraken::isValidCodepoint(cp) != (cp == check)) errorFound |= 8;
        kraken::encodeUTF32BE(cp, testArr);
        kraken::decodeUTF32BE(testArr, testArr+4, check);
        if (kraken::isValidCodepoint(cp) != (cp == check)) errorFound |= 16;
    }
    return errorFound;
}

int testEncodingAutodetect()
{
    using Encoding = kraken::UnicodeEncoding;
    int err = 0;
    err += !(kraken::detectUnicodeEncoding(complexUTF8,
                                    complexUTF8+ARRAY_SIZE(complexUTF8))
             == Encoding::UTF8);
    err += !(kraken::detectUnicodeEncoding(complexUTF16LE,
                                    complexUTF16LE+ARRAY_SIZE(complexUTF16LE))
             == Encoding::UTF16LE);
    err += !(kraken::detectUnicodeEncoding(complexUTF16BE,
                                    complexUTF16BE+ARRAY_SIZE(complexUTF16BE))
             == Encoding::UTF16BE);
    err += !(kraken::detectUnicodeEncoding(complexUTF32LE,
                                    complexUTF32LE+ARRAY_SIZE(complexUTF32LE))
             == Encoding::UTF32LE);
    err += !(kraken::detectUnicodeEncoding(complexUTF32BE,
                                    complexUTF32BE+ARRAY_SIZE(complexUTF32BE))
             == Encoding::UTF32BE);
    return err;
}

int testStrDecoding()
{
    int err = 0;
    U32string str;
    auto* begin = complexUTF8;
    auto* end = begin+ARRAY_SIZE(complexUTF8);
    err += !(end == kraken::decodeUTFStr(begin, end, std::back_inserter(str)));
    err += !isCorrect(str);
    begin = complexUTF16LE;
    end = begin+ARRAY_SIZE(complexUTF16LE);
    str.clear();
    err += !(end == kraken::decodeUTFStr(begin, end, std::back_inserter(str)));
    err += !isCorrectBOM(str);
    begin = complexUTF16BE;
    end = begin+ARRAY_SIZE(complexUTF16BE);
    str.clear();
    err += !(end == kraken::decodeUTFStr(begin, end, std::back_inserter(str)));
    err += !isCorrectBOM(str);
    begin = complexUTF32LE;
    end = begin+ARRAY_SIZE(complexUTF32LE);
    str.clear();
    err += !(end == kraken::decodeUTFStr(begin, end, std::back_inserter(str)));
    err += !isCorrectBOM(str);
    begin = complexUTF32BE;
    end = begin+ARRAY_SIZE(complexUTF32BE);
    str.clear();
    err += !(end == kraken::decodeUTFStr(begin, end, std::back_inserter(str)));
    err += !isCorrectBOM(str);
    return err;
}

int testBrokenStringDecoding()
{
    int err = 0;

    U32string str;
    auto* begin = brokenUTF8;
    auto* end = begin+ARRAY_SIZE(brokenUTF8);
    err += !(end == kraken::decodeUTF8Str(begin, end, std::back_inserter(str)));

    if (str.size() != ARRAY_SIZE(brokenUTF8Decoded)) ++err;
    else
    {
        for (size_t i = 0; i < ARRAY_SIZE(brokenUTF8Decoded); ++i)
        {
            if (str[i] != brokenUTF8Decoded[i])
            {
                ++err;
                break;
            }
        }
    }

    U32string str2;

    str2 = decodeUTF8ForwardIterator(brokenUTF8,
                                     ARRAY_SIZE(brokenUTF8));

    err += !(str == str2);

    return err;
}

int main()
{
    int err = 0;
    err += testDecoding();
    err += testEncoding();
    err += testInvalidCodes();
    err += testAllEncodeDecode();
    err += testEncodingAutodetect();
    err += testStrDecoding();
    err += testBrokenStringDecoding();
    return err;
}
